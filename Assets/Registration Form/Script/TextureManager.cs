using UnityEngine;
using System.Collections;

public class TextureManager : MonoBehaviour {

	// Font array
	public Texture[] textures;
	private TextureManager instance;
	
	public TextureManager get {
		get {
			return instance;
		}
	}
	
	public void Awake () {
		instance = this;
	}
	
	public Texture GetTextureByName (string textureName) {
		foreach (Texture curTexture in textures)
			if ((curTexture != null) && (curTexture.name == textureName))
				return curTexture;
		return null;
	}	
}
