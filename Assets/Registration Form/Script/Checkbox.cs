using UnityEngine;
using System.Collections;
 
public class Checkbox : MonoBehaviour
{
	public bool isChecked = false;
	public bool readOnly = true;

	// Flag indicating if user pressed the button down on this control or not
	private bool buttonDown = false;
	private TextureManager textureManager = null;
	
	// On start, make a copy of the texture manager
	void Start () {
		textureManager = (TextureManager) GameObject.FindWithTag ("TextureManager").GetComponent ("TextureManager");
	}
	
	// If mouse entry or exit, reset the button down flag
	void OnMouseEnter ()
	{
		buttonDown = false;
	}	
	void OnMouseExit ()
	{
		buttonDown = false;
	}
	
	// If mouse clicked and released on the same control, toggle checked flag
	void OnMouseDown ()
	{
		buttonDown = true;
	}
	void OnMouseUp ()
	{
		// No need to do anything if this control is not enabled
		if ((!readOnly) && (buttonDown))
		{
			isChecked = !isChecked;
		}
	}
	
	// If accessed this asset by code, trigger this function
	private void SetEnabled (bool setChecked)
	{
		isChecked = setChecked;
	}
	
	// On refresh
	void Update ()
	{
		// Update the displayed image
		ChangeDisplay ();
	}
	
	// Helper function
	private void ChangeDisplay ()
	{
		// Change the graphics accordingly
		if (isChecked)
		{
			GetComponent<GUITexture>().texture = textureManager.get.GetTextureByName ("Checkbox-Checked");
		}
		else
		{
			GetComponent<GUITexture>().texture = textureManager.get.GetTextureByName ("Checkbox-Unchecked");
		}
	}	
}
