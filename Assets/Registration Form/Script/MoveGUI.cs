using UnityEngine;
using System.Collections;

public class MoveGUI : MonoBehaviour {

	// Use enumerator to create GUI drop down option
	public enum MoveType
	{
		Keyboard,
		Mouse,
		Both,
		NotMove
	}
	
	// Member variables
	public float verMoveSpeed = -0.400f;
	public float horMoveSpeed = 0.200f;
	public MoveType moveType = MoveType.Keyboard; 
	public bool unlock = true;

	// Private variables
	private Vector3 moveDirections = Vector3.zero;
	private Vector3 oldMousePosition = Vector3.zero;
	public bool dragged = false;	

	// Modify dragged flag
	private void OnMouseDown ()
	{
		dragged = true;
	}	
	private void OnMouseUp ()
	{
		dragged = false;
	}
		
	// Function to move the form according to inputs	
	void FixedUpdate() 
	{
		// If not moving is selected, do nothing
		if ((moveType == MoveType.NotMove) || (!unlock))
			return;
		
		// If keyboard is enabled, inching toward the destination
		if ((moveType == MoveType.Keyboard) || (moveType == MoveType.Both))
		{
			// If the position of this form is allowed to be moved, calculate the moving axes (X, Y)
			moveDirections = new Vector3(Input.GetAxis("Horizontal") * verMoveSpeed, 
				Input.GetAxis("Vertical") * horMoveSpeed, 0);
			moveDirections = transform.TransformDirection(moveDirections);

			// Move the object
			this.transform.Translate (moveDirections * Time.deltaTime);
		}
		
		dragged = (Input.GetMouseButton (0)) ;
		
		// If mouse is enabled, instantly jump to the destination
		Vector3 newMousePosition = new Vector3 (Input.mousePosition.x / Screen.width, 
			Input.mousePosition.y / Screen.height, 0);
		if ((dragged) && ((moveType == MoveType.Mouse) || (moveType == MoveType.Both)))
		{
			moveDirections = transform.TransformDirection(newMousePosition - oldMousePosition);

			// Move the object
			this.transform.Translate (moveDirections);
		}		
		// Update current mouse position
		oldMousePosition = newMousePosition;
	}
}
