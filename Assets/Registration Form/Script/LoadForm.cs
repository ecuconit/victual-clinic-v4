// using UnityEngine;
// using System.Collections;
// using FNP.Definitions; // Yanhao's FNP DLL file
// using System;

// public class LoadForm : MonoBehaviour {

	// private GameObject [] guiTexts;
	// private GameObject [] checkBoxes;
	// private Patient testPatient = null;
	// private DateTime formDate;	
	
	// // Use this for initialization
	// void Start () {
		// // Initialize the form
		// guiTexts = GameObject.FindGameObjectsWithTag ("DisplayField");
		// checkBoxes = GameObject.FindGameObjectsWithTag ("DisplayCheckbox");
		
		// // Debugger
		// // print ("1: " + guiTexts.ToString() + " 2: " + checkBoxes.ToString ());
			
 		// if (guiTexts != null)
			// InitializeGuiTexts ();
		
		// if (checkBoxes != null)
			// InitializeCheckBoxes ();
		
		// // Debugger
		// //    Testing display of FNP.Definitions.Patient object
		// testPatient = new Patient ();
		// testPatient.FirstName = "Monkey";
		// testPatient.LastName = "Luffy";
		// testPatient.Initial = "Dee";
		// testPatient.Sex = Sex.Male;		
		// testPatient.DateOfBirth = new DateTime (1993, 3, 3);
		// testPatient.SSN = new SSN ("123-45-6789"); 
		// testPatient.MartialStatus = new MartialStatus ();
		// testPatient.MartialStatus.Status = MaritalStatusToString (1);
		// testPatient.HomePhone = "555-555-5555";
		// testPatient.Address = new Address ("Address Line 1", "Address Line 2", "City", "State", "Zip Code", "Country");
		// testPatient.Employer = new Company ();
		// testPatient.Employer.Name = "Employer Name";
		// testPatient.Employer.Phone = "555-555-5555";
		// testPatient.Employer.Address = new Address ("Address Line 1", "Address Line 2", 
			// "City", "State", "Zip Code", "Country");
		// testPatient.Occupation = new Occupation ();
		// testPatient.Occupation.Name = "Poison Food Tester";
		// testPatient.EmergencyContact = new EmergencyContact ();
		// testPatient.EmergencyContact.FirstName = "Firstname";
		// testPatient.EmergencyContact.LastName = "Lastname";
		// testPatient.EmergencyContact.Phone = "555-555-5555";
		// testPatient.EmergencyContact.Relationship = "None";
		// testPatient.EmergencyContact.Address = 	new Address ("Address Line 1", "Address Line 2", 
			// "City", "State", "Zip Code", "Country");
		// testPatient.InsurancePolicy = new InsurancePolicy ();
		// testPatient.InsurancePolicy.Company = new Company ();
		// testPatient.InsurancePolicy.Company.Name = "Insurance Company Name";
		// testPatient.InsurancePolicy.Company.Phone = "555-555-5555";
		// testPatient.InsurancePolicy.Company.Address = new Address ("Address Line 1", "Address Line 2", 
			// "City", "State", "Zip Code", "Country");	
		// testPatient.InsurancePolicy.ContractNo = "123456789";
		// testPatient.InsurancePolicy.GroupNo = "987654321";
		// testPatient.InsurancePolicy.SubscriberNo = "Giggly";
		
		// SetPatient (testPatient, DateTime.Now);
	// }
	
	// // Initialize objects
	// private void InitializeGuiTexts ()
	// {
		// // Initialize text to be empty
		// foreach (GameObject display in guiTexts)
			// display.guiText.text = "";
	// }	
	// private void InitializeCheckBoxes ()
	// {
		// // Initialize boxes to be unchecked
		// foreach (GameObject box in checkBoxes)
			// ((Checkbox) box.GetComponent ("Checkbox")).isChecked = false;
	// }
	// // Populate the form with given Patient data
	// private void PopulateGuiTexts (Patient patient, DateTime formDate)
	// {
		// foreach (GameObject display in guiTexts)
		// {
			// // Temp object
			// ObjectKeyword objectName;
			// try
			// {
				// objectName = (ObjectKeyword)Enum.Parse(typeof(ObjectKeyword), display.name, true);
			// }
			// catch
			// {
				// // Object name does not exist in enumerator, ignore it
				// continue;
			// }
            	
			// switch (objectName)
			// {
				// // Header Section
                // case ObjectKeyword.Header_Date_Input:
                	// if (formDate != new DateTime ())
						// display.guiText.text = DateToString (formDate);
					// break;
                // case ObjectKeyword.Header_Home_Phone_Input:
					// if (patient.HomePhone != null)
						// display.guiText.text = patient.HomePhone;
					// break;
					
				// // Patient Information
                // case ObjectKeyword.Patient_First_Name_Input:
					// if (patient.FirstName != null)
						// display.guiText.text = patient.FirstName;
					// break;
                // case ObjectKeyword.Patient_Last_Name_Input:
					// if (patient.FirstName != null)
						// display.guiText.text = patient.LastName;
					// break;
                // case ObjectKeyword.Patient_Birthdate_Input:
					// if (patient.DateOfBirth != new DateTime ())
						// display.guiText.text = DateToString (patient.DateOfBirth);
					// break;
                // case ObjectKeyword.Patient_Initial_Input:
					// if (patient.Initial != null)
						// display.guiText.text = patient.Initial [0] + ".";
					// break;
                // case ObjectKeyword.Patient_Soc_Sec_Num_Input:
					// if (patient.SSN != null)
						// display.guiText.text = patient.SSN.ToString ();
					// break;
                // case ObjectKeyword.Patient_Address_Input:
					// if ((patient.Address != null) && (patient.Address.Line1 != null))
					// {
						// display.guiText.text = patient.Address.Line1;
						// if (patient.Address.Line2 != null)
							// display.guiText.text += " " + patient.Address.Line2;
					// }
					// break;
                // case ObjectKeyword.Patient_City_Input:
					// if ((patient.Address != null) && (patient.Address.City != null))
						// display.guiText.text = patient.Address.City;
					// break;
                // case ObjectKeyword.Patient_State_Input:
					// if ((patient.Address != null) && (patient.Address.State != null))
						// display.guiText.text = patient.Address.State;
					// break;
                // case ObjectKeyword.Patient_Zip_Input:
					// if ((patient.Address != null) && (patient.Address.Zip != null))
						// display.guiText.text = patient.Address.Zip;
					// break;
                // case ObjectKeyword.Patient_Employeed_Input:
					// if ((patient.Employer != null) && (patient.Employer.Name != null))
						// display.guiText.text = patient.Employer.Name;
					// break;
                // case ObjectKeyword.Patient_Business_Address_Input:
					// if ((patient.Employer != null) && (patient.Employer.Address != null))
					// {
						// display.guiText.text = patient.Address.Line1;
						// if (patient.Address.Line2 != null)
							// display.guiText.text += " " + patient.Address.Line2;
						// if (patient.Address.City != null)
							// display.guiText.text += " " + patient.Address.City;
						// if (patient.Address.State != null)
							// display.guiText.text += ", " + patient.Address.State;
						// if (patient.Address.Zip != null)
							// display.guiText.text += "  " + patient.Address.Zip;
					// }
					// break;
                // case ObjectKeyword.Patient_Business_Phone_Input:
					// if ((patient.Employer != null) && (patient.Employer.Phone != null))
						// display.guiText.text = patient.Employer.Phone;
					// break;
                // case ObjectKeyword.Patient_Occupation_Input:
					// if ((patient.Occupation != null) && (patient.Occupation.Name != null))
						// display.guiText.text = patient.Occupation.Name;
					// break;
                // case ObjectKeyword.Patient_Emergency_Notified_Name_Input:
					// if ((patient.EmergencyContact != null) && (patient.EmergencyContact.FirstName != null))
						// display.guiText.text = patient.EmergencyContact.FirstName;
					// if ((patient.EmergencyContact != null) && (patient.EmergencyContact.LastName != null))
					// {
						// if (display.guiText.text.Trim() != "")
							// display.guiText.text += " " + patient.EmergencyContact.LastName;
						// else
							// display.guiText.text = patient.EmergencyContact.LastName;
					// }
					// break;
                // case ObjectKeyword.Patient_Emergency_Notified_Phone_Input:
					// if ((patient.EmergencyContact != null) && (patient.EmergencyContact.Phone != null))
						// display.guiText.text = patient.EmergencyContact.Phone;
					// break;
				
				// // Primary Insurance
                // case ObjectKeyword.Insurance_Person_Responsible_First_Name_Input:
					// if (patient.FirstName != null)
						// display.guiText.text = patient.FirstName;
					// break;
                // case ObjectKeyword.Insurance_Person_Responsible_Last_Name_Input:
					// if (patient.FirstName != null)
						// display.guiText.text = patient.LastName;
					// break;
                // case ObjectKeyword.Insurance_Person_Responsible_Birthdate_Input:
					// if (patient.DateOfBirth != new DateTime ())
						// display.guiText.text = DateToString (patient.DateOfBirth);
					// break;
                // case ObjectKeyword.Insurance_Person_Responsible_Initial_Input:
					// if (patient.Initial != null)
						// display.guiText.text = patient.Initial [0] + ".";
					// break;
                // case ObjectKeyword.Insurance_Person_Responsible_Soc_Sec_Num_Input:
					// if (patient.SSN != null)
						// display.guiText.text = patient.SSN.ToString ();
					// break;
                // case ObjectKeyword.Insurance_Person_Responsible_Phone_Input:
					// if (patient.HomePhone != null)
						// display.guiText.text = patient.HomePhone;
					// break;
                // case ObjectKeyword.Insurance_Person_Responsible_Employed_Input:
					// if ((patient.Employer != null) && (patient.Employer.Name != null))
						// display.guiText.text = patient.Employer.Name;
					// break;
                // case ObjectKeyword.Insurance_Person_Responsible_Business_Address_Input:
					// if ((patient.Employer != null) && (patient.Employer.Address != null))
					// {
						// display.guiText.text = patient.Address.Line1;
						// if (patient.Address.Line2 != null)
							// display.guiText.text += " " + patient.Address.Line2;
						// if (patient.Address.City != null)
							// display.guiText.text += " " + patient.Address.City;
						// if (patient.Address.State != null)
							// display.guiText.text += ", " + patient.Address.State;
						// if (patient.Address.Zip != null)
							// display.guiText.text += "  " + patient.Address.Zip;
					// }
					// break;
                // case ObjectKeyword.Insurance_Person_Responsible_Business_Phone_Input:
					// if ((patient.Employer != null) && (patient.Employer.Phone != null))
						// display.guiText.text = patient.Employer.Phone;
					// break;
                // case ObjectKeyword.Insurance_Person_Responsible_Occupation_Input:
                    // if ((patient.Occupation != null) && (patient.Occupation.Name != null))
						// display.guiText.text = patient.Occupation.Name;
					// break;
                // case ObjectKeyword.Insurance_Company_Name_Input:
                    // if ((patient.InsurancePolicy != null) && (patient.InsurancePolicy.Company != null) &&
						// (patient.InsurancePolicy.Company.Name != null))
						// display.guiText.text = patient.InsurancePolicy.Company.Name;
					// break;
                // case ObjectKeyword.Insurance_Contract_Num_Input:
                    // if ((patient.InsurancePolicy != null) && (patient.InsurancePolicy.ContractNo != null))
						// display.guiText.text = patient.InsurancePolicy.ContractNo;
					// break;
                // case ObjectKeyword.Insurance_Group_Num_Input:
                    // if ((patient.InsurancePolicy != null) && (patient.InsurancePolicy.GroupNo != null))
						// display.guiText.text = patient.InsurancePolicy.GroupNo;
					// break;
                // case ObjectKeyword.Insurance_Subscriber_Num_Input:
                    // if ((patient.InsurancePolicy != null) && (patient.InsurancePolicy.SubscriberNo != null))
						// display.guiText.text = patient.InsurancePolicy.SubscriberNo;
					// break;
				
				// // Additional Insurance

				// // Assignment and Release
                // case ObjectKeyword.Assign_Insurance_Company_Name_Input:
                    // if ((patient.InsurancePolicy != null) && (patient.InsurancePolicy.Company != null) &&
						// (patient.InsurancePolicy.Company.Name != null))
						// display.guiText.text = patient.InsurancePolicy.Company.Name;
					// break;
                // case ObjectKeyword.Assign_Signature_Input:
                    // if (patient.FirstName != null)
						// display.guiText.text = patient.FirstName;
					// if (patient.Initial != null)
					// {
						// if (display.guiText.text.Trim ().Equals (""))
							// display.guiText.text = patient.Initial [0] + ".";
						// else
							// display.guiText.text += " " + patient.Initial [0] + ".";
					// }
					// if (patient.LastName != null)
					// {
						// if (display.guiText.text.Trim ().Equals (""))
							// display.guiText.text = patient.LastName;
						// else
							// display.guiText.text += " " + patient.LastName;
					// }
					// break;
                // case ObjectKeyword.Assign_Date_Input:
                    // if (formDate != new DateTime())
						// display.guiText.text = DateToString (formDate);
					// break;				
				// default:
					// // Don't know what to do, nothing to do
					// break;
			// }
		// }
	// }
	// private void PopulateCheckBoxes (Patient patient)
	// {
		// foreach (GameObject box in checkBoxes)
		// {
			// // Temp object
			// ObjectKeyword objectName;
			// try
			// {
				// objectName = (ObjectKeyword)Enum.Parse(typeof(ObjectKeyword), box.name, true);
			// }
			// catch
			// {
				// // Object name does not exist in enumerator, ignore it
				// continue;
			// }

			// switch (objectName)
            // {
				// // Patient Information
                // case ObjectKeyword.Patient_Sex_Male_Checkbox:
                    // // Due to the fact that the Sex parameter is an enumerator, null catch
					// //    will not work.  So Try and Catch is used instead
					// try
					// {
						// if (patient.Sex == Sex.Male)
							// ((Checkbox) box.GetComponent ("Checkbox")).isChecked = true;
					// }
					// catch
					// { // Do nothing 
					// }
					// break;
                // case ObjectKeyword.Patient_Sex_Female_Checkbox:
                    // // Due to the fact that the Sex parameter is an enumerator, null catch
					// //    will not work.  So Try and Catch is used instead
					// try
					// {
						// if (patient.Sex == Sex.Female)
							// ((Checkbox) box.GetComponent ("Checkbox")).isChecked = true;
					// }
					// catch
					// { // Do nothing
					// }
					// break;
                // case ObjectKeyword.Patient_Marital_Status_Single_Checkbox:
                    // if ((patient.MartialStatus != null) && (patient.MartialStatus.Status.Equals 
						// (MaritalStatusToString(1))))
						// ((Checkbox) box.GetComponent ("Checkbox")).isChecked = true;
					// break;
                // case ObjectKeyword.Patient_Marital_Status_Married_Checkbox:
                    // if ((patient.MartialStatus != null) && (patient.MartialStatus.Status.Equals 
						// (MaritalStatusToString(2))))
						// ((Checkbox) box.GetComponent ("Checkbox")).isChecked = true;
					// break;
                // case ObjectKeyword.Patient_Marital_Status_Widowed_Checkbox:
                    // if ((patient.MartialStatus != null) && (patient.MartialStatus.Status.Equals 
						// (MaritalStatusToString(3))))
						// ((Checkbox) box.GetComponent ("Checkbox")).isChecked = true;
					// break;
                // case ObjectKeyword.Patient_Marital_Status_Separated_Checkbox:
                    // if ((patient.MartialStatus != null) && (patient.MartialStatus.Status.Equals 
						// (MaritalStatusToString(4))))
						// ((Checkbox) box.GetComponent ("Checkbox")).isChecked = true;
					// break;
                // case ObjectKeyword.Patient_Marital_Status_Divorced_Checkbox:
                    // if ((patient.MartialStatus != null) && (patient.MartialStatus.Status.Equals 
						// (MaritalStatusToString(5))))
						// ((Checkbox) box.GetComponent ("Checkbox")).isChecked = true;
					// break;
					
				// // Additional Insurance
				// // For this project, assume no addtional insurance information is used
				// case ObjectKeyword.Add_Ins_Covered_No_Checkbox:
					// ((Checkbox) box.GetComponent ("Checkbox")).isChecked = true;
					// break;
					
				// default:
					// // Don't know what to do, nothing to do
					// break;
			// }
		// }
	// }
	
	// // Convert DateTime object into a string
	// private string DateToString (DateTime date)
	// {
		// return MonthToString (date.Month) + " " + date.Day.ToString () + ", " + 
			// date.Year.ToString ();
	// }
	// // Convert month from integer to string
	// private string MonthToString (int month)
	// {
		// switch (month)
		// {
			// case 1:
				// return "January";
			// case 2:
				// return "Feburary";
			// case 3:
				// return "March";
			// case 4:
				// return "April";
			// case 5:
				// return "May";
			// case 6:
				// return "June";
			// case 7:
				// return "July";
			// case 8:
				// return "August";
			// case 9:
				// return "September";
			// case 10:
				// return "October";
			// case 11:
				// return "November";
			// case 12:
				// return "December";
			// default:
				// // Return nothing
				// return "";
		// }
	// }
	// // Translate the marital status code into string
	// private string MaritalStatusToString (int statusCode)
	// {
		// switch (statusCode)
		// {
			// case 1:
				// return "Single";
			// case 2:
				// return "Married";
			// case 3:
				// return "Widowed";
			// case 4:
				// return "Separated";
			// case 5:
				// return "Divorced";
			// default:
				// return "";				
		// }
	// }
    // // Keyword enumerator, used for switch statement
    // enum ObjectKeyword
    // {
        // Header_Date_Input,
        // Header_Home_Phone_Input,
        // Patient_First_Name_Input,
        // Patient_Last_Name_Input,
        // Patient_Birthdate_Input,
        // Patient_Initial_Input,
        // Patient_Soc_Sec_Num_Input,
        // Patient_Address_Input,
        // Patient_City_Input,
        // Patient_State_Input,
        // Patient_Zip_Input,
        // Patient_Employeed_Input,
        // Patient_Business_Address_Input,
        // Patient_Business_Phone_Input,
        // Patient_Occupation_Input,
        // Patient_Emergency_Notified_Name_Input,
        // Patient_Emergency_Notified_Phone_Input,
        // Insurance_Person_Responsible_First_Name_Input,
        // Insurance_Person_Responsible_Last_Name_Input,
        // Insurance_Person_Responsible_Birthdate_Input,
        // Insurance_Person_Responsible_Initial_Input,
        // Insurance_Person_Responsible_Soc_Sec_Num_Input,
        // Insurance_Person_Responsible_Phone_Input,
        // Insurance_Person_Responsible_Employed_Input,
        // Insurance_Person_Responsible_Business_Address_Input,
        // Insurance_Person_Responsible_Business_Phone_Input,
        // Insurance_Person_Responsible_Occupation_Input,
        // Insurance_Company_Name_Input,
        // Insurance_Contract_Num_Input,
        // Insurance_Group_Num_Input,
        // Insurance_Subscriber_Num_Input,
        // Assign_Insurance_Company_Name_Input,
        // Assign_Signature_Input,
        // Assign_Date_Input,
        // Patient_Sex_Male_Checkbox,
        // Patient_Sex_Female_Checkbox,
        // Patient_Marital_Status_Single_Checkbox,
        // Patient_Marital_Status_Married_Checkbox,
        // Patient_Marital_Status_Widowed_Checkbox,
        // Patient_Marital_Status_Separated_Checkbox,
        // Patient_Marital_Status_Divorced_Checkbox,
        // Add_Ins_Covered_No_Checkbox
    // }
	
	// // Interface function
	// public void SetPatient (Patient patient)
	// {
		// // Calling worker functions to perform the tasks
		// PopulateGuiTexts (patient, new DateTime ());
		// PopulateCheckBoxes (patient);
	// }
	// public void SetPatient (Patient patient, DateTime formDate)
	// {
		// // Calling worker functions to perform the tasks
		// PopulateGuiTexts (patient, formDate);
		// PopulateCheckBoxes (patient);
	// }	
	
// }
