using UnityEngine;
using System.Collections;

public class GUITextStyle : MonoBehaviour {

	// Type enumerator
	public enum fontStyle {
		Title,
		Header,
		Description,
		FormField,
		UserInput,
		Custom
	}
	
	// Public variable used for GUI editing
	public fontStyle style = fontStyle.FormField;
	public float componentRed = 1;
	public float componentGreen = 1;
	public float componentBlue = 1;
	public float componentAlpha = 1;
	public Font customFont = null;
	
	// Private variable for this class only
	// private FontManager fontManager = null;
	
	// On construction, fetch the font manager
	void Start () {
		// fontManager = (FontManager) GameObject.FindWithTag ("FontManager").GetComponent ("FontManager");
	}
	
	// Update is called once per frame
	void Update () {
		// Change the GUIText's color to the set RGB by style
		switch (style) {
			case fontStyle.Title:
				GetComponent<GUIText>().material.color = new Color (0.000f, 0.403f, 0.090f, 1f);
				break;
			case fontStyle.Header:
				GetComponent<GUIText>().material.color = new Color (0.000f, 0.403f, 0.090f, 1f);
				break;
			case fontStyle.Description:
				GetComponent<GUIText>().material.color = new Color (0.188f, 0.433f, 0.295f, 1f);
				break;
			case fontStyle.FormField:
				GetComponent<GUIText>().material.color = new Color (0.188f, 0.433f, 0.295f, 1f);
				break;
			case fontStyle.UserInput:
				GetComponent<GUIText>().material.color = new Color (0.000f, 0.000f, 0.000f, 1f);
				break;
			default:
				// By default, use the color described by the component values
				GetComponent<GUIText>().material.color = new Color (componentRed, componentGreen, 
					componentBlue, componentAlpha);
				if (customFont != null)
				 	GetComponent<GUIText>().font = customFont;
				break;
		}
	}
}
