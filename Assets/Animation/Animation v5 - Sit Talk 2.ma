//Maya ASCII 2014 scene
//Name: Animation v5 - Sit Talk 2.ma
//Last modified: Tue, Apr 14, 2015 07:28:30 AM
//Codeset: 1252
requires maya "2014";
currentUnit -l centimeter -a degree -t film;
fileInfo "application" "maya";
fileInfo "product" "Maya 2014";
fileInfo "version" "2014 x64";
fileInfo "cutIdentifier" "201303010241-864206";
fileInfo "osv" "Microsoft Windows 7 Enterprise Edition, 64-bit Windows 7 Service Pack 1 (Build 7601)\n";
fileInfo "license" "education";
createNode transform -n "Sit_Talk_2";
	setAttr ".v" no;
createNode joint -n "Bip01_Spine" -p "Sit_Talk_2";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".s" -type "double3" 0.99999999999999956 0.99999999999999989 0.99999999999999989 ;
	setAttr ".jo" -type "double3" 90.000171 0 89.999999999999986 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" -0.0081722831436215043 0.99872757760529951 0.04976383746125676 0
		 -0.031468606931617168 -0.049997711134416305 0.99825345261566845 0 0.99947133053730142 0.006592011223395697 0.031837161023350762 0
		 -6.7139604917915256e-006 99.443700000000007 -2.2495999999899592 1;
	setAttr -k on ".liw";
	setAttr ".fbxID" 5;
createNode joint -n "Bip01_Spine1" -p "|Sit_Talk_2|Bip01_Spine";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".t" -type "double3" 13.3984 -0.0107 0 ;
	setAttr ".s" -type "double3" 1 1 0.99999999999999989 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" -0.0087295896663812361 0.015254963731834248 0.99984552824213668 0
		 -0.0050586545493994011 -0.99987150655113555 0.015211193290218554 0 0.99994910084384092 -0.0049250856543791108 0.0088056375629807955 0
		 9.9999999999999995e-007 2.2603360000000001 112.84212599999999 1;
	setAttr -k on ".liw";
	setAttr ".fbxID" 5;
createNode joint -n "Bip01_Spine2" -p "|Sit_Talk_2|Bip01_Spine|Bip01_Spine1";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".t" -type "double3" 13.3983 -0.0107 0 ;
	setAttr ".s" -type "double3" 1 0.99999999999999989 0.99999999999999989 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" 0 0 1 0 0 -1 0 0 1 0 0 0 -3.9999999999999998e-006 -9.6239790000000003 -19.074804 1;
	setAttr -k on ".liw";
	setAttr ".fbxID" 5;
createNode joint -n "Bip01_Neck" -p "|Sit_Talk_2|Bip01_Spine|Bip01_Spine1|Bip01_Spine2";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".t" -type "double3" 13.3985 -0.007899999999999999 0 ;
	setAttr ".s" -type "double3" 0.99999999999999956 0.99999999999999956 0.99999999999999989 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" 0 -0.020830999999999999 0.99978299999999998 0 0 -0.99978299999999998 -0.020830999999999999 0
		 1 0 0 0 -1.0000000000000001e-005 -9.6266999999999996 -5.6762839999999999 1;
	setAttr -k on ".liw";
	setAttr ".fbxID" 5;
createNode joint -n "Bip01_Head" -p "|Sit_Talk_2|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".t" -type "double3" 9.9829 0 0 ;
	setAttr ".s" -type "double3" 0.99999999999999989 1 1 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" 0 -0.10133399999999999 0.99485299999999999 0 0 -0.99485199999999996 -0.10133399999999999 0
		 1 0 0 0 -4.4000000000000012e-005 -9.8346470000000004 4.3044190000000002 1;
	setAttr -k on ".liw";
	setAttr ".fbxID" 5;
createNode joint -n "_notused_Bip01_HeadNub" -p "|Sit_Talk_2|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_Head";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".t" -type "double3" 18.3537 0 0 ;
	setAttr ".ssc" no;
	setAttr ".fbxID" 5;
createNode joint -n "Bip01_R_Clavicle" -p "|Sit_Talk_2|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".t" -type "double3" -0.8268 -0.6612 -3.9165 ;
	setAttr ".s" -type "double3" 1 1 0.99999999999999989 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" -0.99454799999999999 -9.9454800000000002e-007 -0.104286 0
		 -9.8906500000000008e-007 1 -1.0428599999999999e-007 0 0.104286 -5.7179000000000015e-010 -0.99454799999999999 0
		 -3.9165049999999999 2.9251839999999998 138.82619800000001 1;
	setAttr -k on ".liw";
	setAttr ".fbxID" 5;
createNode joint -n "Bip01_R_UpperArm" -p "|Sit_Talk_2|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_R_Clavicle";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".t" -type "double3" 10.6745 0 0 ;
	setAttr ".s" -type "double3" 0.99999999999999989 0.99999999999999989 1 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" -0.75332600000000005 0.10000199999999999 -0.65000000000000002 0
		 0.071782399999999996 0.99496899999999999 0.0698825 0 0.65371900000000005 0.0059857399999999998 -0.75671500000000003 0
		 -14.532831 2.9251239999999998 137.712976 1;
	setAttr -k on ".liw";
	setAttr ".fbxID" 5;
createNode joint -n "Bip01_R_Forearm" -p "|Sit_Talk_2|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_R_Clavicle|Bip01_R_UpperArm";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".t" -type "double3" 25.3375 0 0 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" -0.752861 -0.095945600000000006 -0.65114899999999998 0
		 -0.076500899999999997 0.99536800000000003 -0.058215000000000003 0 0.65371900000000005 0.0059856600000000003 -0.756714 0
		 -33.620182 5.4589869999999996 121.243574 1;
	setAttr -k on ".liw";
	setAttr ".fbxID" 5;
createNode joint -n "Bip01_R_Hand" -p "|Sit_Talk_2|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_R_Clavicle|Bip01_R_UpperArm|Bip01_R_Forearm";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".t" -type "double3" 24.9881 0 0 ;
	setAttr ".r" -type "double3" 90.133325000000013 -6.3004020000000009 2.5653330000000008 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" -0.67922700000000003 -0.050331500000000001 -0.73219999999999996 0
		 0.73278100000000002 0.0092545500000000003 -0.68040299999999998 0 0.0410219 -0.99868900000000005 0.030596000000000002 0
		 -52.432737000000003 3.0614050000000002 104.972679 1;
	setAttr -k on ".liw";
	setAttr ".fbxID" 5;
createNode joint -n "Bip01_R_Finger0" -p "|Sit_Talk_2|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_R_Clavicle|Bip01_R_UpperArm|Bip01_R_Forearm|Bip01_R_Hand";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".t" -type "double3" 2.3036000000000003 1.3941 2.9222 ;
	setAttr ".r" -type "double3" -81.93018 -23.314181 18.295908 ;
	setAttr ".s" -type "double3" 0.99999999999999989 1 1 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" -0.36474400000000001 -0.436471 -0.82246900000000001 0
		 -0.072261699999999998 0.89392799999999994 -0.44234699999999999 0 0.92829899999999999 -0.10191 -0.357595 0
		 -52.855969000000002 0.0400224 102.426821 1;
	setAttr -k on ".liw";
	setAttr ".fbxID" 5;
createNode joint -n "Bip01_R_Finger01" -p "|Sit_Talk_2|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_R_Clavicle|Bip01_R_UpperArm|Bip01_R_Forearm|Bip01_R_Hand|Bip01_R_Finger0";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".t" -type "double3" 4.0806 0 0 ;
	setAttr ".r" -type "double3" 9.4787915988669464e-023 3.0000000000000005e-006 24.064549 ;
	setAttr ".s" -type "double3" 1 1 0.99999999999999989 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" -0.36250900000000003 -0.034022900000000002 -0.93135900000000005 0
		 0.082749500000000004 0.99421099999999996 -0.068527199999999996 0 0.92830000000000001 -0.101911 -0.357595 0
		 -54.344414999999998 -1.7410159999999999 99.070615000000004 1;
	setAttr -k on ".liw";
	setAttr ".fbxID" 5;
createNode joint -n "_notused_Bip01_R_Finger0Nub" -p "|Sit_Talk_2|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_R_Clavicle|Bip01_R_UpperArm|Bip01_R_Forearm|Bip01_R_Hand|Bip01_R_Finger0|Bip01_R_Finger01";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".t" -type "double3" 3.3413 0 0 ;
	setAttr ".r" -type "double3" 1e-006 0 -180 ;
	setAttr ".ssc" no;
	setAttr ".fbxID" 5;
createNode joint -n "Bip01_R_Finger2" -p "|Sit_Talk_2|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_R_Clavicle|Bip01_R_UpperArm|Bip01_R_Forearm|Bip01_R_Hand";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".t" -type "double3" 7.3709 -0.4816 0.6341 ;
	setAttr ".r" -type "double3" 0.043615000000000029 -0.013407000000000007 17.086613999999997 ;
	setAttr ".s" -type "double3" 0.99999999999999989 0.99999999999999989 0.99999999999999989 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" -0.43393399999999999 -0.045623999999999998 -0.89978899999999995 0
		 0.90003599999999995 0.022875199999999998 -0.43521399999999999 0 0.040439000000000003 -0.99869600000000003 0.031137000000000001 0
		 -57.766043000000003 2.0527600000000001 99.922762000000006 1;
	setAttr -k on ".liw";
	setAttr ".fbxID" 5;
createNode joint -n "Bip01_R_Finger21" -p "|Sit_Talk_2|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_R_Clavicle|Bip01_R_UpperArm|Bip01_R_Forearm|Bip01_R_Hand|Bip01_R_Finger2";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".t" -type "double3" 3.8208 0 0 ;
	setAttr ".r" -type "double3" 0 0 29.946389999999987 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" 0.0732873 -0.028113900000000001 -0.99691399999999997 0
		 0.99648999999999999 0.042598999999999998 0.072054900000000005 0 0.0404418 -0.99869600000000003 0.0311372 0
		 -59.424045999999997 1.8784400000000001 96.484806000000006 1;
	setAttr -k on ".liw";
	setAttr ".fbxID" 5;
createNode joint -n "_notused_Bip01_R_Finger2Nub" -p "|Sit_Talk_2|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_R_Clavicle|Bip01_R_UpperArm|Bip01_R_Forearm|Bip01_R_Hand|Bip01_R_Finger2|Bip01_R_Finger21";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".t" -type "double3" 4.7174000000000005 0 0 ;
	setAttr ".r" -type "double3" 0 0 180 ;
	setAttr ".ssc" no;
	setAttr ".fbxID" 5;
createNode joint -n "Bip01_R_Finger1" -p "|Sit_Talk_2|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_R_Clavicle|Bip01_R_UpperArm|Bip01_R_Forearm|Bip01_R_Hand";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".t" -type "double3" 7.3263 -0.211 2.2784 ;
	setAttr ".r" -type "double3" -0.73310399999999953 -5.3145149999999983 22.646732999999994 ;
	setAttr ".s" -type "double3" 1 1 0.99999999999999989 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" -0.33941900000000003 -0.13520499999999999 -0.93086800000000003 0
		 0.93680799999999997 0.040593299999999999 -0.34748099999999998 0 0.084767999999999996 -0.98998600000000003 0.112883 0
		 -57.470132999999997 0.41539700000000002 99.821579 1;
	setAttr -k on ".liw";
	setAttr ".fbxID" 5;
createNode joint -n "Bip01_R_Finger11" -p "|Sit_Talk_2|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_R_Clavicle|Bip01_R_UpperArm|Bip01_R_Forearm|Bip01_R_Hand|Bip01_R_Finger1";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".t" -type "double3" 3.5597999999999996 0 0 ;
	setAttr ".r" -type "double3" -2e-006 3.999999999999999e-006 22.627677999999996 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" 0.047135799999999999 -0.10918 -0.99290400000000001 0
		 0.995286 0.089482699999999998 0.037409400000000002 0 0.084763400000000003 -0.98998600000000003 0.112883 0
		 -58.678344000000003 -0.065903900000000001 96.507863999999998 1;
	setAttr -k on ".liw";
	setAttr ".fbxID" 5;
createNode joint -n "_notused_Bip01_R_Finger1Nub" -p "|Sit_Talk_2|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_R_Clavicle|Bip01_R_UpperArm|Bip01_R_Forearm|Bip01_R_Hand|Bip01_R_Finger1|Bip01_R_Finger11";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".t" -type "double3" 4.1404000000000005 0 0 ;
	setAttr ".r" -type "double3" 0 0 180 ;
	setAttr ".ssc" no;
	setAttr ".fbxID" 5;
createNode joint -n "Bip01_R_Finger3" -p "|Sit_Talk_2|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_R_Clavicle|Bip01_R_UpperArm|Bip01_R_Forearm|Bip01_R_Hand";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".t" -type "double3" 7.2345 -0.5997 -0.9332 ;
	setAttr ".r" -type "double3" 0.043363999999999805 3.826455 18.509659999999997 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" -0.41327900000000001 0.0219578 -0.91034000000000004 0
		 0.91051400000000005 0.0239971 -0.41277999999999998 0 0.0127817 -0.999471 -0.0299104 0
		 -57.824306 3.6237469999999998 100.05504999999999 1;
	setAttr -k on ".liw";
	setAttr ".fbxID" 5;
createNode joint -n "Bip01_R_Finger31" -p "|Sit_Talk_2|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_R_Clavicle|Bip01_R_UpperArm|Bip01_R_Forearm|Bip01_R_Hand|Bip01_R_Finger3";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".t" -type "double3" 3.7241 0 0 ;
	setAttr ".r" -type "double3" 3.0000000000000013e-006 3.0000000000000005e-006 27.984287000000009 ;
	setAttr ".s" -type "double3" 1 0.99999999999999989 0.99999999999999989 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" 0.0622821 0.0306512 -0.99758800000000003 0 0.99797599999999997 0.0108983 0.062641199999999994 0
		 0.012792100000000001 -0.999471 -0.0299104 0 -59.363360999999998 3.705527 96.664748000000003 1;
	setAttr -k on ".liw";
	setAttr ".fbxID" 5;
createNode joint -n "_notused_Bip01_R_Finger3Nub" -p "|Sit_Talk_2|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_R_Clavicle|Bip01_R_UpperArm|Bip01_R_Forearm|Bip01_R_Hand|Bip01_R_Finger3|Bip01_R_Finger31";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".t" -type "double3" 4.2397 0 0 ;
	setAttr ".r" -type "double3" 3e-006 -5e-006 180 ;
	setAttr ".ssc" no;
	setAttr ".fbxID" 5;
createNode joint -n "Bip01_R_Finger4" -p "|Sit_Talk_2|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_R_Clavicle|Bip01_R_UpperArm|Bip01_R_Forearm|Bip01_R_Hand";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".t" -type "double3" 7.2292 -0.4187 -2.3639 ;
	setAttr ".r" -type "double3" -0.17713400000000007 5.5619560000000012 16.615208 ;
	setAttr ".s" -type "double3" 0.99999999999999989 0.99999999999999989 1 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" -0.44323200000000001 0.051426199999999998 -0.89493100000000003 0
		 0.89640699999999995 0.026346700000000001 -0.44244899999999998 0 0.00082491599999999997 -0.99832900000000002 -0.057776500000000001 0
		 -57.746822000000002 5.0544450000000003 99.892002000000005 1;
	setAttr -k on ".liw";
	setAttr ".fbxID" 5;
createNode joint -n "Bip01_R_Finger41" -p "|Sit_Talk_2|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_R_Clavicle|Bip01_R_UpperArm|Bip01_R_Forearm|Bip01_R_Hand|Bip01_R_Finger4";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".t" -type "double3" 3.4791000000000003 0 0 ;
	setAttr ".r" -type "double3" -1.9999999999999995e-006 -3.0000000000000005e-006 29.444368 ;
	setAttr ".s" -type "double3" 1 0.99999999999999989 1 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" 0.054672699999999998 0.057735700000000001 -0.996834 0
		 0.99850399999999995 -0.0023359700000000001 0.054628999999999997 0 0.00082546799999999999 -0.99832900000000002 -0.057777000000000002 0
		 -59.288860999999997 5.2333790000000002 96.778379000000001 1;
	setAttr -k on ".liw";
	setAttr ".fbxID" 5;
createNode joint -n "_notused_Bip01_R_Finger4Nub" -p "|Sit_Talk_2|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_R_Clavicle|Bip01_R_UpperArm|Bip01_R_Forearm|Bip01_R_Hand|Bip01_R_Finger4|Bip01_R_Finger41";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".t" -type "double3" 3.3974 0 0 ;
	setAttr ".r" -type "double3" 0 0 -180 ;
	setAttr ".ssc" no;
	setAttr ".fbxID" 5;
createNode joint -n "Bip01_L_Clavicle" -p "|Sit_Talk_2|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".t" -type "double3" -0.8268 -0.6612 3.9165 ;
	setAttr ".s" -type "double3" 0.99999999999999989 0.99999999999999956 0.99999999999999989 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" 0.99454799999999999 9.9454800000000002e-007 -0.104286 0
		 -9.8906500000000008e-007 1 1.0428599999999999e-007 0 0.104286 -5.7179000000000015e-010 0.99454799999999999 0
		 3.9165070000000002 2.9252039999999999 138.82619700000001 1;
	setAttr -k on ".liw";
	setAttr ".fbxID" 5;
createNode joint -n "Bip01_L_UpperArm" -p "|Sit_Talk_2|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_L_Clavicle";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".t" -type "double3" 10.6745 0 0 ;
	setAttr ".s" -type "double3" 1 1 0.99999999999999989 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" 0.75332500000000002 0.100004 -0.65000000000000002 0
		 -0.071785600000000005 0.99496899999999999 0.069882299999999994 0 0.65371900000000005 -0.0059834299999999997 0.756714 0
		 14.532825000000001 2.925173 137.712974 1;
	setAttr -k on ".liw";
	setAttr ".fbxID" 5;
createNode joint -n "Bip01_L_Forearm" -p "|Sit_Talk_2|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_L_Clavicle|Bip01_L_UpperArm";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".t" -type "double3" 25.3374 0 0 ;
	setAttr ".s" -type "double3" 1 1 0.99999999999999989 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" 0.752861 -0.095943299999999995 -0.65114899999999998 0
		 0.076497700000000002 0.99536899999999995 -0.058215299999999998 0 0.65371900000000005 -0.0059834199999999997 0.756714 0
		 33.620189000000003 5.4590990000000001 121.243576 1;
	setAttr -k on ".liw";
	setAttr ".fbxID" 5;
createNode joint -n "Bip01_L_Hand" -p "|Sit_Talk_2|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_L_Clavicle|Bip01_L_UpperArm|Bip01_L_Forearm";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".t" -type "double3" 24.9881 0 0 ;
	setAttr ".r" -type "double3" -90.133325 6.300397000000002 2.565332999999999 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" 0.67922800000000005 -0.050329499999999999 -0.73219999999999996 0
		 -0.73278100000000002 0.0092525000000000003 -0.68040299999999998 0 0.041019 0.99868999999999997 -0.030595899999999999 0
		 52.432744999999997 3.0615610000000002 104.972662 1;
	setAttr -k on ".liw";
	setAttr ".fbxID" 5;
createNode joint -n "Bip01_L_Finger0" -p "|Sit_Talk_2|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_L_Clavicle|Bip01_L_UpperArm|Bip01_L_Forearm|Bip01_L_Hand";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".t" -type "double3" 2.3036000000000003 1.3941 -2.9222 ;
	setAttr ".r" -type "double3" 81.930180000000021 23.314181000000019 18.295909 ;
	setAttr ".s" -type "double3" 0.99999999999999989 1 1.0000000000000002 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" 0.36474600000000001 -0.43647000000000002 -0.82246900000000001 0
		 0.072258799999999998 0.89392799999999994 -0.44234699999999999 0 0.92830000000000001 0.101913 0.357595 0
		 52.856034999999999 0.0402022 102.42681 1;
	setAttr -k on ".liw";
	setAttr ".fbxID" 5;
createNode joint -n "Bip01_L_Finger01" -p "|Sit_Talk_2|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_L_Clavicle|Bip01_L_UpperArm|Bip01_L_Forearm|Bip01_L_Hand|Bip01_L_Finger0";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".t" -type "double3" 4.0806 0 0 ;
	setAttr ".r" -type "double3" 0 0 24.06455200000001 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" 0.36250900000000003 -0.034021900000000001 -0.93135900000000005 0
		 -0.082751900000000003 0.99421199999999998 -0.068527000000000005 0 0.92829899999999999 0.101913 0.357595 0
		 54.344436000000002 -1.7408459999999999 99.070606999999995 1;
	setAttr -k on ".liw";
	setAttr ".fbxID" 5;
createNode joint -n "_notused_Bip01_L_Finger0Nub" -p "|Sit_Talk_2|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_L_Clavicle|Bip01_L_UpperArm|Bip01_L_Forearm|Bip01_L_Hand|Bip01_L_Finger0|Bip01_L_Finger01";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".t" -type "double3" 3.3413 0 0 ;
	setAttr ".ssc" no;
	setAttr ".fbxID" 5;
createNode joint -n "Bip01_L_Finger2" -p "|Sit_Talk_2|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_L_Clavicle|Bip01_L_UpperArm|Bip01_L_Forearm|Bip01_L_Hand";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".t" -type "double3" 7.3709 -0.4816 -0.6341 ;
	setAttr ".r" -type "double3" -0.043610000000000031 0.01341 17.086619000000006 ;
	setAttr ".s" -type "double3" 1 0.99999999999999989 0.99999999999999989 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" 0.43393500000000002 -0.045622700000000002 -0.89978899999999995 0
		 -0.90003599999999995 0.022871599999999999 -0.43521399999999999 0 0.040435199999999998 0.99869699999999995 -0.031137399999999999 0
		 57.766060000000003 2.0529269999999999 99.922754999999995 1;
	setAttr -k on ".liw";
	setAttr ".fbxID" 5;
createNode joint -n "Bip01_L_Finger21" -p "|Sit_Talk_2|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_L_Clavicle|Bip01_L_UpperArm|Bip01_L_Forearm|Bip01_L_Hand|Bip01_L_Finger2";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".t" -type "double3" 3.8208 0 0 ;
	setAttr ".r" -type "double3" -4.0000000000000024e-006 -4.0000000000000015e-006 29.946390000000019 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" -0.0732873 -0.0281141 -0.99691399999999997 0 -0.99649100000000002 0.042592999999999999 0.072054999999999994 0
		 0.040435899999999997 0.99869699999999995 -0.031137000000000001 0 59.424058000000002 1.878614 96.484800000000007 1;
	setAttr -k on ".liw";
	setAttr ".fbxID" 5;
createNode joint -n "_notused_Bip01_L_Finger2Nub" -p "|Sit_Talk_2|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_L_Clavicle|Bip01_L_UpperArm|Bip01_L_Forearm|Bip01_L_Hand|Bip01_L_Finger2|Bip01_L_Finger21";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".t" -type "double3" 4.7174000000000005 0 0 ;
	setAttr ".ssc" no;
	setAttr ".fbxID" 5;
createNode joint -n "Bip01_L_Finger1" -p "|Sit_Talk_2|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_L_Clavicle|Bip01_L_UpperArm|Bip01_L_Forearm|Bip01_L_Hand";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".t" -type "double3" 7.3263 -0.211 -2.2784 ;
	setAttr ".r" -type "double3" 0.73310799999999965 5.3145129999999989 22.646742999999997 ;
	setAttr ".s" -type "double3" 1 0.99999999999999989 0.99999999999999989 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" 0.33942 -0.13520399999999999 -0.93086800000000003 0
		 -0.93680799999999997 0.040590800000000003 -0.34748099999999998 0 0.084765400000000005 0.98998600000000003 -0.112883 0
		 57.470143999999998 0.415574 99.821611000000004 1;
	setAttr -k on ".liw";
	setAttr ".fbxID" 5;
createNode joint -n "Bip01_L_Finger11" -p "|Sit_Talk_2|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_L_Clavicle|Bip01_L_UpperArm|Bip01_L_Forearm|Bip01_L_Hand|Bip01_L_Finger1";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".t" -type "double3" 3.5597999999999996 0 0 ;
	setAttr ".r" -type "double3" -2.0000000000000012e-006 3.0000000000000005e-006 22.627677000000009 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" -0.047135499999999997 -0.10918 -0.99290400000000001 0
		 -0.99528499999999998 0.089479600000000006 0.037409299999999999 0 0.0847604 0.98998699999999995 -0.112883 0
		 58.678319000000002 -0.065726099999999996 96.507845000000003 1;
	setAttr -k on ".liw";
	setAttr ".fbxID" 5;
createNode joint -n "_notused_Bip01_L_Finger1Nub" -p "|Sit_Talk_2|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_L_Clavicle|Bip01_L_UpperArm|Bip01_L_Forearm|Bip01_L_Hand|Bip01_L_Finger1|Bip01_L_Finger11";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".t" -type "double3" 4.1404000000000005 0 0 ;
	setAttr ".ssc" no;
	setAttr ".fbxID" 5;
createNode joint -n "Bip01_L_Finger3" -p "|Sit_Talk_2|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_L_Clavicle|Bip01_L_UpperArm|Bip01_L_Forearm|Bip01_L_Hand";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".t" -type "double3" 7.2345 -0.5997 0.9332 ;
	setAttr ".r" -type "double3" -0.043362000000000123 -3.826458 18.509666999999997 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" 0.41327900000000001 0.021958700000000001 -0.91034000000000004 0
		 -0.91051400000000005 0.0239966 -0.41277999999999998 0 0.012781000000000001 0.999471 0.029911 0
		 57.824314000000001 3.6238670000000002 100.055049 1;
	setAttr -k on ".liw";
	setAttr ".fbxID" 5;
createNode joint -n "Bip01_L_Finger31" -p "|Sit_Talk_2|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_L_Clavicle|Bip01_L_UpperArm|Bip01_L_Forearm|Bip01_L_Hand|Bip01_L_Finger3";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".t" -type "double3" 3.7241 0 0 ;
	setAttr ".r" -type "double3" 2e-006 -9.4787915988669346e-023 27.984288 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" -0.062282200000000003 0.030651100000000001 -0.99758800000000003 0
		 -0.99797599999999997 0.0108933 0.062641199999999994 0 0.012787099999999999 0.999471 0.029910599999999999 0
		 59.363363 3.7056490000000002 96.664751999999993 1;
	setAttr -k on ".liw";
	setAttr ".fbxID" 5;
createNode joint -n "_notused_Bip01_L_Finger3Nub" -p "|Sit_Talk_2|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_L_Clavicle|Bip01_L_UpperArm|Bip01_L_Forearm|Bip01_L_Hand|Bip01_L_Finger3|Bip01_L_Finger31";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".t" -type "double3" 4.2397 0 0 ;
	setAttr ".ssc" no;
	setAttr ".fbxID" 5;
createNode joint -n "Bip01_L_Finger4" -p "|Sit_Talk_2|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_L_Clavicle|Bip01_L_UpperArm|Bip01_L_Forearm|Bip01_L_Hand";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".t" -type "double3" 7.2292 -0.4187 2.3639 ;
	setAttr ".r" -type "double3" 0.17713199999999998 -5.5619539999999983 16.615218000000002 ;
	setAttr ".s" -type "double3" 1 0.99999999999999989 1 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" 0.44323200000000001 0.051427599999999997 -0.89493100000000003 0
		 -0.89640600000000004 0.026343800000000001 -0.44244899999999998 0 0.00082181400000000003 0.99832900000000002 0.057776399999999999 0
		 57.746775 5.054627 99.891990000000007 1;
	setAttr -k on ".liw";
	setAttr ".fbxID" 5;
createNode joint -n "Bip01_L_Finger41" -p "|Sit_Talk_2|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_L_Clavicle|Bip01_L_UpperArm|Bip01_L_Forearm|Bip01_L_Hand|Bip01_L_Finger4";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".t" -type "double3" 3.4791000000000003 0 0 ;
	setAttr ".r" -type "double3" 2e-006 -3.0000000000000009e-006 29.444363999999997 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" -0.054672800000000001 0.057735500000000002 -0.996834 0
		 -0.99850399999999995 -0.00234096 0.054628799999999998 0 0.00082047800000000003 0.99832900000000002 0.057777200000000001 0
		 59.288862000000002 5.2335589999999996 96.778389000000004 1;
	setAttr -k on ".liw";
	setAttr ".fbxID" 5;
createNode joint -n "_notused_Bip01_L_Finger4Nub" -p "|Sit_Talk_2|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_L_Clavicle|Bip01_L_UpperArm|Bip01_L_Forearm|Bip01_L_Hand|Bip01_L_Finger4|Bip01_L_Finger41";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".t" -type "double3" 3.3974 0 0 ;
	setAttr ".ssc" no;
	setAttr ".fbxID" 5;
createNode joint -n "Bip01_L_Thigh" -p "|Sit_Talk_2|Bip01_Spine";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".t" -type "double3" -11.0456 2.2584 8.5201999999999991 ;
	setAttr ".s" -type "double3" 1 0.99999999999999989 1.0000000000000002 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" 0.059694200000000003 0.0067868399999999997 -0.998193 0
		 0.0013515700000000001 -0.99997599999999998 -0.00671814 0 -0.99821599999999999 -0.00094809699999999996 -0.059701999999999998 0
		 8.5202019999999994 4.3064900000000002e-005 88.396292000000003 1;
	setAttr -k on ".liw";
	setAttr ".fbxID" 5;
createNode joint -n "Bip01_L_Calf" -p "|Sit_Talk_2|Bip01_Spine|Bip01_L_Thigh";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".t" -type "double3" 36.1527 0 0 ;
	setAttr ".s" -type "double3" 0.99999999999999989 0.99999999999999989 0.99999999999999989 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" 0.059157700000000001 0.119882 -0.99102400000000002 0
		 0.0080950799999999993 -0.99278699999999998 -0.119612 0 -0.99821599999999999 -0.00094644599999999998 -0.059701400000000002 0
		 10.678299000000001 0.245397 52.308965999999998 1;
	setAttr -k on ".liw";
	setAttr ".fbxID" 5;
createNode joint -n "Bip01_L_Foot" -p "|Sit_Talk_2|Bip01_Spine|Bip01_L_Thigh|Bip01_L_Calf";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".t" -type "double3" 43.2543 0 0 ;
	setAttr ".s" -type "double3" 1 1 0.99999999999999989 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" 0 0 -1 0 0.123471 -0.99234800000000001 0 0 -0.99234800000000001 -0.123471 0 0
		 13.237131 5.4308170000000002 9.4429110000000005 1;
	setAttr -k on ".liw";
	setAttr ".fbxID" 5;
createNode joint -n "Bip01_L_Toe0" -p "|Sit_Talk_2|Bip01_Spine|Bip01_L_Thigh|Bip01_L_Calf|Bip01_L_Foot";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".t" -type "double3" 9.4714999999999989 11.2377 0 ;
	setAttr ".r" -type "double3" -5.000000000000003e-006 1.9999999999999995e-006 89.999979999999979 ;
	setAttr ".s" -type "double3" 0.99999999999999989 1 1 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" 0.123471 -0.99234800000000001 0 0 0 0 1 0 -0.99234800000000001 -0.123471 0 0
		 14.624665999999999 -5.7209120000000002 -0.028559000000000001 1;
	setAttr -k on ".liw";
	setAttr ".fbxID" 5;
createNode joint -n "_notused_Bip01_L_Toe0Nub" -p "|Sit_Talk_2|Bip01_Spine|Bip01_L_Thigh|Bip01_L_Calf|Bip01_L_Foot|Bip01_L_Toe0";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".t" -type "double3" 7.2685 0 0 ;
	setAttr ".r" -type "double3" 0 0 180 ;
	setAttr ".ssc" no;
	setAttr ".fbxID" 5;
createNode joint -n "Bip01_R_Thigh" -p "|Sit_Talk_2|Bip01_Spine";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".t" -type "double3" -11.0456 2.2585 -8.5201999999999991 ;
	setAttr ".s" -type "double3" 1 0.99999999999999989 1 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" -0.059694200000000003 0.0067826700000000002 -0.998193 0
		 -0.00134545 -0.99997599999999998 -0.0067143300000000001 0 -0.99821599999999999 0.00094221699999999997 0.059701900000000002 0
		 -8.5201879999999992 1.40303e-005 88.396298000000002 1;
	setAttr -k on ".liw";
	setAttr ".fbxID" 5;
createNode joint -n "Bip01_R_Calf" -p "|Sit_Talk_2|Bip01_Spine|Bip01_R_Thigh";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".t" -type "double3" 36.1527 0 0 ;
	setAttr ".s" -type "double3" 0.99999999999999989 1 1 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" -0.059158000000000002 0.11988699999999999 -0.99102400000000002 0
		 -0.0080972100000000005 -0.99278699999999998 -0.119617 0 -0.99821599999999999 0.00094822899999999998 0.059701999999999998 0
		 -10.678288 0.24518599999999999 52.308996999999998 1;
	setAttr -k on ".liw";
	setAttr ".fbxID" 5;
createNode joint -n "Bip01_R_Foot" -p "|Sit_Talk_2|Bip01_Spine|Bip01_R_Thigh|Bip01_R_Calf";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".t" -type "double3" 43.2543 0 0 ;
	setAttr ".s" -type "double3" 0.99999999999999989 1 1 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" 0 0 -1 0 -0.123469 -0.99234800000000001 0 0 -0.99234800000000001 0.123469 0 0
		 -13.237124 5.4308209999999999 9.442933 1;
	setAttr -k on ".liw";
	setAttr ".fbxID" 5;
createNode joint -n "Bip01_R_Toe0" -p "|Sit_Talk_2|Bip01_Spine|Bip01_R_Thigh|Bip01_R_Calf|Bip01_R_Foot";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".t" -type "double3" 9.4714999999999989 11.2377 0 ;
	setAttr ".r" -type "double3" 1.9999999999999991e-006 -1.9999999999999995e-006 89.999986 ;
	setAttr ".s" -type "double3" 0.99999999999999989 0.99999999999999989 1 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" -0.12346799999999999 -0.99234800000000001 0 0 0 0 1 0
		 -0.99234800000000001 0.12346799999999999 0 0 -14.624625999999999 -5.7209019999999997 -0.028538999999999998 1;
	setAttr -k on ".liw";
	setAttr ".fbxID" 5;
createNode joint -n "_notused_Bip01_R_Toe0Nub" -p "|Sit_Talk_2|Bip01_Spine|Bip01_R_Thigh|Bip01_R_Calf|Bip01_R_Foot|Bip01_R_Toe0";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".t" -type "double3" 7.2685 0 0 ;
	setAttr ".ssc" no;
	setAttr ".fbxID" 5;
createNode animCurveTL -n "Bip01_Spine_translateX3";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 73 ".ktv[0:72]"  0.824 -2.5010421276092529 1.86 -2.5032835006713867
		 2.892 -2.5071196556091309 3.928 -2.5124204158782959 4.964 -2.5277657508850098 5.996 -2.5520801544189453
		 7.032 -2.5749504566192627 8.064 -2.5919463634490967 9.1 -2.6042795181274414 10.136 -2.6180727481842041
		 11.168 -2.6436104774475098 12.204 -2.6828503608703613 13.24 -2.725011825561523 14.272 -2.7597305774688721
		 15.308 -2.7895481586456299 16.34 -2.8165233135223389 17.376 -2.8327078819274902 18.412 -2.8485512733459473
		 19.444 -2.8887274265289307 20.48 -2.9472253322601318 21.516 -2.9935340881347656 22.548 -3.0158891677856445
		 23.584 -3.0330564975738525 24.616 -3.0615272521972656 25.652 -3.0924131870269775
		 26.688 -3.1157302856445313 27.72 -3.1340367794036865 28.756 -3.1501865386962891 29.792 -3.1628131866455078
		 30.824 -3.1705610752105713 31.86 -3.1758112907409668 32.892 -3.1771194934844971 33.928 -3.1716172695159912
		 34.964 -3.1631360054016113 35.996 -3.1526579856872559 37.032 -3.1383488178253174
		 38.064 -3.1247012615203857 39.1 -3.1129779815673828 40.136 -3.0956575870513916 41.168 -3.0707752704620361
		 42.204 -3.0368521213531494 43.24 -2.9863839149475098 44.272 -2.9197962284088135 45.308 -2.8439435958862305
		 46.34 -2.7686808109283447 47.376 -2.700514554977417 48.412 -2.6369035243988037 49.444 -2.5787498950958252
		 50.48 -2.5262076854705811 51.516 -2.4731681346893311 52.548 -2.4176156520843506 53.584 -2.3615572452545166
		 54.616 -2.3146047592163086 55.652 -2.2882862091064453 56.688 -2.274160623550415 57.72 -2.2587466239929199
		 58.756 -2.2462015151977539 59.788 -2.243255615234375 60.824 -2.2446324825286865 61.86 -2.247233629226685
		 62.892 -2.2627613544464111 63.928 -2.2950565814971924 64.964 -2.3320555686950684
		 65.996 -2.3680858612060547 67.032 -2.4068536758422852 68.064 -2.4487872123718266
		 69.1 -2.479163646697998 70.136 -2.4830026626586914 71.168 -2.4694211483001709 72.204 -2.4543476104736328
		 73.24 -2.4407575130462646 74.272 -2.4311251640319824 75.308 -2.4299111366271973;
createNode animCurveTL -n "Bip01_Spine_translateY3";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 73 ".ktv[0:72]"  0.824 65.441375732421875 1.86 65.389602661132813
		 2.892 65.350662231445312 3.928 65.319633483886719 4.964 65.286994934082031 5.996 65.249786376953125
		 7.032 65.214851379394531 8.064 65.187141418457031 9.1 65.161460876464844 10.136 65.130477905273438
		 11.168 65.093940734863281 12.204 65.056991577148438 13.24 65.019325256347656 14.272 64.975074768066406
		 15.308 64.925636291503906 16.34 64.878837585449219 17.376 64.834358215332031 18.412 64.779975891113281
		 19.444 64.707290649414062 20.48 64.628173828125 21.516 64.561531066894531 22.548 64.509452819824219
		 23.584 64.461051940917969 24.616 64.41009521484375 25.652 64.361625671386719 26.688 64.323936462402344
		 27.72 64.299240112304688 28.756 64.283470153808594 29.792 64.275779724121094 30.824 64.28460693359375
		 31.86 64.314163208007813 32.892 64.361160278320313 33.928 64.42327880859375 34.964 64.491737365722656
		 35.996 64.556495666503906 37.032 64.618896484375 38.064 64.679862976074219 39.1 64.739044189453125
		 40.136 64.804306030273437 41.168 64.8818359375 42.204 64.969703674316406 43.24 65.059089660644531
		 44.272 65.145523071289062 45.308 65.239898681640625 46.34 65.353286743164063 47.376 65.479019165039063
		 48.412 65.598968505859375 49.444 65.710502624511719 50.48 65.82513427734375 51.516 65.93524169921875
		 52.548 66.029457092285156 53.584 66.118423461914063 54.616 66.207115173339844 55.652 66.288459777832031
		 56.688 66.365447998046875 57.72 66.438888549804687 58.756 66.502372741699219 59.788 66.556221008300781
		 60.824 66.603202819824219 61.86 66.654197692871094 62.892 66.723342895507813 63.928 66.803916931152344
		 64.964 66.893394470214844 65.996 67.011207580566406 67.032 67.147872924804688 68.064 67.255577087402344
		 69.1 67.316505432128906 70.136 67.359207153320312 71.168 67.404075622558594 72.204 67.438362121582031
		 73.24 67.4375 74.272 67.397239685058594 75.308 67.330345153808594;
createNode animCurveTL -n "Bip01_Spine_translateZ3";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 73 ".ktv[0:72]"  0.824 0.051158182322978973 1.86 0.05161820724606514
		 2.892 0.055421985685825341 3.928 0.061771977692842477 4.964 0.067581862211227417
		 5.996 0.071368731558322906 7.032 0.073706969618797302 8.064 0.078979715704917908
		 9.1 0.089578837156295776 10.136 0.10080839693546297 11.168 0.11130654811859132 12.204 0.12487834692001344
		 13.24 0.14093153178691864 14.272 0.15820504724979401 15.308 0.18050411343574524 16.34 0.20708769559860227
		 17.376 0.2328021228313446 18.412 0.26184436678886414 19.444 0.29629242420196533 20.48 0.32780078053474426
		 21.516 0.35775107145309448 22.548 0.39216366410255432 23.584 0.42559719085693359
		 24.616 0.44936922192573542 25.652 0.45886689424514771 26.688 0.45733743906021118
		 27.72 0.45544663071632385 28.756 0.45929694175720215 29.792 0.46527144312858582 30.824 0.46976163983345032
		 31.86 0.47467389702796936 32.892 0.47836929559707642 33.928 0.47321519255638123 34.964 0.4599472582340241
		 35.996 0.44625267386436462 37.032 0.42974653840065002 38.064 0.40782257914543152
		 39.1 0.38493484258651733 40.136 0.35912108421325684 41.168 0.32987958192825317 42.204 0.29795351624488831
		 43.24 0.25566855072975159 44.272 0.20126408338546753 45.308 0.13909873366355896 46.34 0.071572341024875641
		 47.376 0.0033355876803398132 48.412 -0.064450748264789581 49.444 -0.1348864734172821
		 50.48 -0.20404279232025144 51.516 -0.2637496292591095 52.548 -0.31426167488098145
		 53.584 -0.3649777472019195 54.616 -0.41949999332427979 55.652 -0.46962669491767878
		 56.688 -0.51160502433776855 57.72 -0.54554766416549683 58.756 -0.56530630588531494
		 59.788 -0.57336068153381348 60.824 -0.57701307535171509 61.86 -0.56615853309631348
		 62.892 -0.52750110626220703 63.928 -0.46293780207633967 64.964 -0.3579978346824646
		 65.996 -0.17515860497951508 67.032 0.068665914237499237 68.064 0.31443598866462708
		 69.1 0.56517070531845093 70.136 0.83404839038848877 71.168 1.0696237087249756 72.204 1.2364645004272461
		 73.24 1.3522754907608032 74.272 1.4418962001800537 75.308 1.5207104682922363;
createNode animCurveTA -n "Bip01_Spine_rotateX3";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 73 ".ktv[0:72]"  0.824 -1.0492358207702637 1.86 -1.0373420715332031
		 2.892 -1.0235679149627686 3.928 -1.0043052434921265 4.964 -0.98129302263259899 5.996 -0.95883905887603749
		 7.032 -0.93706250190734885 8.064 -0.91287189722061157 9.1 -0.88450318574905396 10.136 -0.85514265298843406
		 11.168 -0.83283305168151855 12.204 -0.82311016321182251 13.24 -0.81802701950073264
		 14.272 -0.80748748779296875 15.308 -0.79734343290328979 16.34 -0.79337191581726085
		 17.376 -0.78536403179168701 18.412 -0.77128976583480835 19.444 -0.76981163024902333
		 20.48 -0.79064017534255981 21.516 -0.81727069616317749 22.548 -0.8375236988067627
		 23.584 -0.86306619644165039 24.616 -0.89987695217132579 25.652 -0.9421994686126709
		 26.688 -0.9890959858894347 27.72 -1.0387455224990845 28.756 -1.0926254987716677 29.792 -1.1538077592849731
		 30.824 -1.2151767015457151 31.86 -1.2704257965087893 32.892 -1.3238993883132939 33.928 -1.3866324424743652
		 34.964 -1.4619483947753906 35.996 -1.5421782732009888 37.032 -1.627448558807373 38.064 -1.7193619012832642
		 39.1 -1.8077917098999023 40.136 -1.8870329856872559 41.168 -1.9675308465957644 42.204 -2.0579292774200439
		 43.24 -2.1527657508850098 44.272 -2.2456650733947754 45.308 -2.3328452110290527 46.34 -2.4170017242431641
		 47.376 -2.5046699047088623 48.412 -2.5863149166107178 49.444 -2.6483638286590576
		 50.48 -2.6985800266265869 51.516 -2.7551968097686768 52.548 -2.8189640045166016 53.584 -2.875314474105835
		 54.616 -2.9177472591400142 55.652 -2.95052170753479 56.688 -2.9736981391906734 57.72 -2.9711582660675049
		 58.756 -2.9248545169830318 59.788 -2.8420743942260742 60.824 -2.744920015335083 61.86 -2.6411056518554687
		 62.892 -2.5284998416900635 63.928 -2.4032356739044189 64.964 -2.2479205131530762
		 65.996 -2.0615806579589844 67.032 -1.8860003948211672 68.064 -1.7537462711334229
		 69.1 -1.6543781757354736 70.136 -1.5663491487503052 71.168 -1.479059100151062 72.204 -1.3958578109741211
		 73.24 -1.3340393304824829 74.272 -1.2940773963928225 75.308 -1.2491773366928101;
createNode animCurveTA -n "Bip01_Spine_rotateY3";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 73 ".ktv[0:72]"  0.824 -0.70795607566833496 1.86 -0.57559329271316528
		 2.892 -0.44761884212493902 3.928 -0.31993651390075684 4.964 -0.20235086977481839
		 5.996 -0.10156154632568359 7.032 -0.0033318495843559504 8.064 0.10887300223112106
		 9.1 0.22805210947990415 10.136 0.33439671993255615 11.168 0.42381376028060919 12.204 0.50571811199188232
		 13.24 0.58788883686065674 14.272 0.67685598134994507 15.308 0.77183502912521362 16.34 0.86715704202651989
		 17.376 0.96686202287673961 18.412 1.0712887048721311 19.444 1.1695427894592283 20.48 1.2598345279693604
		 21.516 1.353379487991333 22.548 1.4544278383255005 23.584 1.5506861209869385 24.616 1.6377658843994141
		 25.652 1.7229560613632202 26.688 1.7996563911437993 27.72 1.8622163534164429 28.756 1.9178000688552863
		 29.792 1.9685525894165041 30.824 2.0116314888000488 31.86 2.0423593521118164 32.892 2.0628535747528076
		 33.928 2.0833053588867187 34.964 2.1012940406799316 35.996 2.1098577976226807 37.032 2.1133542060852051
		 38.064 2.1186926364898682 39.1 2.1340765953063965 40.136 2.1626718044281006 41.168 2.1929686069488525
		 42.204 2.2204179763793945 43.24 2.2574481964111328 44.272 2.3130061626434326 45.308 2.388314962387085
		 46.34 2.4763398170471191 47.376 2.5628287792205811 48.412 2.6460134983062744 49.444 2.736478328704834
		 50.48 2.8353815078735352 51.516 2.9291841983795166 52.548 3.0054218769073486 53.584 3.0684034824371338
		 54.616 3.1323635578155522 55.652 3.20188307762146 56.688 3.2712059020996098 57.72 3.3427295684814453
		 58.756 3.4208767414093018 59.788 3.4889066219329834 60.824 3.5319459438323975 61.86 3.5601160526275635
		 62.892 3.5792903900146484 63.928 3.5828096866607666 64.964 3.5617067813873291 65.996 3.5075016021728516
		 67.032 3.4327070713043213 68.064 3.3584046363830566 69.1 3.2861557006835938 70.136 3.2097933292388916
		 71.168 3.126910924911499 72.204 3.0414385795593266 73.24 2.9663221836090088 74.272 2.89969801902771
		 75.308 2.8186089992523193;
createNode animCurveTA -n "Bip01_Spine_rotateZ3";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 73 ".ktv[0:72]"  0.824 4.9377903938293457 1.86 4.9224028587341309
		 2.892 4.9074630737304687 3.928 4.8951878547668457 4.964 4.8897294998168945 5.996 4.8910956382751465
		 7.032 4.8947529792785645 8.064 4.8998823165893555 9.1 4.903355598449707 10.136 4.904937744140625
		 11.168 4.909207820892334 12.204 4.9116435050964355 13.24 4.9076571464538574 14.272 4.8987565040588379
		 15.308 4.8810772895812988 16.34 4.8524937629699707 17.376 4.8209757804870605 18.412 4.7901124954223633
		 19.444 4.751396656036377 20.48 4.6981987953186035 21.516 4.6342487335205087 22.548 4.5671124458312988
		 23.584 4.5018553733825684 24.616 4.4411892890930176 25.652 4.3887877464294434 26.688 4.3461589813232422
		 27.72 4.3085079193115234 28.756 4.2698736190795907 29.792 4.2270984649658203 30.824 4.179661750793457
		 31.86 4.131129264831543 32.892 4.0857152938842773 33.928 4.0417337417602539 34.964 3.9949138164520281
		 35.996 3.9451045989990234 37.032 3.89543604850769 38.064 3.8468155860900879 39.1 3.7964169979095459
		 40.136 3.7450730800628667 41.168 3.6963748931884775 42.204 3.6488180160522456 43.24 3.6021938323974605
		 44.272 3.5628554821014404 45.308 3.534883975982666 46.34 3.5107681751251221 47.376 3.484663724899292
		 48.412 3.4720709323883057 49.444 3.4846000671386723 50.48 3.5045244693756104 51.516 3.5052390098571777
		 52.548 3.4724235534667973 53.584 3.432698011398315 54.616 3.446860790252686 55.652 3.525964736938477
		 56.688 3.6420857906341553 57.72 3.8206205368041992 58.756 4.0970497131347656 59.788 4.4589543342590332
		 60.824 4.8981208801269531 61.86 5.4298148155212402 62.892 6.0392146110534668 63.928 6.6653466224670419
		 64.964 7.3089141845703125 65.996 8.028529167175293 67.032 8.7504978179931641 68.064 9.3254327774047852
		 69.1 9.734370231628418 70.136 10.04743766784668 71.168 10.306934356689451 72.204 10.531306266784668
		 73.24 10.737400054931641 74.272 10.922403335571287 75.308 11.076207160949709;
createNode animCurveTA -n "Bip01_Spine1_rotateX3";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 73 ".ktv[0:72]"  0.824 -4.9681711196899414 1.86 -5.0381393432617187
		 2.892 -5.1041274070739746 3.928 -5.1664371490478516 4.964 -5.2253637313842773 5.996 -5.2811684608459473
		 7.032 -5.3340268135070801 8.064 -5.3840999603271484 9.1 -5.4315061569213876 10.136 -5.4763569831848145
		 11.168 -5.5188069343566903 12.204 -5.5590076446533203 13.24 -5.5972065925598145 14.272 -5.6336479187011728
		 15.308 -5.66864013671875 16.34 -5.7025446891784668 17.376 -5.7357420921325684 18.412 -5.768622875213623
		 19.444 -5.8015131950378418 20.48 -5.8347177505493164 21.516 -5.8684296607971191 22.548 -5.9027194976806641
		 23.584 -5.9375734329223633 24.616 -5.9728469848632821 25.652 -6.0083489418029785
		 26.688 -6.0438108444213867 27.72 -6.0789055824279785 28.756 -6.1112809181213379 29.792 -6.1365814208984375
		 30.824 -6.1884961128234863 31.86 -6.2090358734130859 32.892 -6.2287297248840332 33.928 -6.269951343536377
		 34.964 -6.2819113731384277 35.996 -6.3153047561645508 37.032 -6.3288211822509766
		 38.064 -6.3429422378540039 39.1 -6.3533906936645517 40.136 -6.3681097030639648 41.168 -6.3706154823303223
		 42.204 -6.3707871437072763 43.24 -6.359438419342041 44.272 -6.3466110229492187 45.308 -6.3241653442382821
		 46.34 -6.2918562889099121 47.376 -6.2492117881774902 48.412 -6.1955499649047852 49.444 -6.1304826736450195
		 50.48 -6.0539641380310059 51.516 -5.966313362121582 52.548 -5.8682713508605957 53.584 -5.7609171867370605
		 54.616 -5.6456656455993652 55.652 -5.5241436958312988 56.688 -5.3981285095214844
		 57.72 -5.269524097442627 58.756 -5.1401557922363281 59.788 -5.0117459297180176 60.824 -4.885828971862793
		 61.86 -4.7637014389038086 62.892 -4.6463541984558105 63.928 -4.5344758033752441 64.964 -4.4284248352050781
		 65.996 -4.3283257484436035 67.032 -4.2340455055236816 68.064 -4.1452956199645996
		 69.1 -4.0616641044616699 70.136 -3.9826993942260747 71.168 -3.9078800678253174 72.204 -3.8366451263427734
		 73.24 -3.7683956623077393 74.272 -3.702506542205811 75.308 -3.6383368968963623;
createNode animCurveTA -n "Bip01_Spine1_rotateY3";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 73 ".ktv[0:72]"  0.824 0.6229630708694458 1.86 0.55340617895126343
		 2.892 0.48434078693389898 3.928 0.41616845130920416 4.964 0.34906134009361267 5.996 0.2829509973526001
		 7.032 0.2176351398229599 8.064 0.15276108682155609 9.1 0.087877489626407609 10.136 0.022464495152235031
		 11.168 -0.044015184044837952 12.204 -0.11209399998188015 13.24 -0.18227089941501615
		 14.272 -0.25502303242683411 15.308 -0.330710768699646 16.34 -0.40966805815696722
		 17.376 -0.49205154180526733 18.412 -0.577922523021698 19.444 -0.66717690229415894
		 20.48 -0.75950688123703003 21.516 -0.85445713996887196 22.548 -0.95139384269714322
		 23.584 -1.049486517906189 24.616 -1.1477694511413576 25.652 -1.2451441287994385 26.688 -1.3405153751373291
		 27.72 -1.4328111410140993 28.756 -1.5161798000335691 29.792 -1.5800511837005615 30.824 -1.7069199085235596
		 31.86 -1.7559217214584353 32.892 -1.802514314651489 33.928 -1.8990706205368038 34.964 -1.9276359081268315
		 35.996 -2.010573148727417 37.032 -2.0485222339630127 38.064 -2.0920336246490479 39.1 -2.1293473243713379
		 40.136 -2.2119705677032471 41.168 -2.2486684322357178 42.204 -2.286462783813477 43.24 -2.3815524578094478
		 44.272 -2.4283394813537602 45.308 -2.4886465072631836 46.34 -2.5545406341552734 47.376 -2.621530294418335
		 48.412 -2.6886236667633057 49.444 -2.7547481060028076 50.48 -2.8188307285308838 51.516 -2.8798661231994629
		 52.548 -2.9368884563446045 53.584 -2.9890925884246826 54.616 -3.0357842445373535
		 55.652 -3.0764412879943848 56.688 -3.1106913089752197 57.72 -3.1382744312286377 58.756 -3.1590127944946289
		 59.788 -3.1727766990661621 60.824 -3.1794397830963135 61.86 -3.1788525581359863 62.892 -3.1708505153656006
		 63.928 -3.1551482677459717 64.964 -3.1314487457275391 65.996 -3.099351167678833 67.032 -3.0584175586700439
		 68.064 -3.0082097053527832 69.1 -2.9483301639556885 70.136 -2.8784756660461426 71.168 -2.7985160350799561
		 72.204 -2.7085347175598145 73.24 -2.6088559627532959 74.272 -2.5000758171081543 75.308 -2.3831071853637695;
createNode animCurveTA -n "Bip01_Spine1_rotateZ3";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 73 ".ktv[0:72]"  0.824 0.96925461292266846 1.86 0.95727914571762096
		 2.892 0.94509398937225342 3.928 0.93242728710174572 4.964 0.91916793584823631 5.996 0.90525072813034047
		 7.032 0.89065724611282349 8.064 0.87543594837188721 9.1 0.85970175266265869 10.136 0.843564212322235
		 11.168 0.82715386152267467 12.204 0.810577392578125 13.24 0.79396289587020896 14.272 0.77735131978988636
		 15.308 0.76079350709915172 16.34 0.74429208040237427 17.376 0.72782915830612172 18.412 0.71143686771392822
		 19.444 0.69509297609329224 20.48 0.67881560325622559 21.516 0.66266810894012451 22.548 0.64673614501953125
		 23.584 0.63113147020339966 24.616 0.61606830358505249 25.652 0.60178256034851074
		 26.688 0.58853799104690552 27.72 0.57667833566665649 28.756 0.56687849760055542 29.792 0.56024289131164551
		 30.824 0.55079561471939087 31.86 0.54900050163269043 32.892 0.5484626293182373 33.928 0.55251258611679077
		 34.964 0.55551838874816895 35.996 0.57026129961013794 37.032 0.58011257648468018
		 38.064 0.59325063228607178 39.1 0.60576117038726807 40.136 0.63772523403167725 41.168 0.65259355306625366
		 42.204 0.66802304983139038 43.24 0.70613521337509155 44.272 0.72394567728042603 45.308 0.74611902236938477
		 46.34 0.76932817697525024 47.376 0.79183238744735707 48.412 0.8134438991546632 49.444 0.83407515287399292
		 50.48 0.85375863313674927 51.516 0.87260037660598755 52.548 0.89084863662719727 53.584 0.90879034996032726
		 54.616 0.92680591344833363 55.652 0.94528412818908691 56.688 0.96461606025695823
		 57.72 0.985149085521698 58.756 1.007143497467041 59.788 1.0306991338729858 60.824 1.0557786226272583
		 61.86 1.0821428298950195 62.892 1.1093560457229614 63.928 1.1368374824523926 64.964 1.1638410091400146
		 65.996 1.1894681453704834 67.032 1.2128056287765503 68.064 1.2329301834106443 69.1 1.2489681243896484
		 70.136 1.2601739168167114 71.168 1.2660167217254641 72.204 1.2661111354827881 73.24 1.2603381872177124
		 74.272 1.2488389015197754 75.308 1.2319320440292358;
createNode animCurveTA -n "Bip01_Spine2_rotateX3";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 73 ".ktv[0:72]"  0.824 -2.4853651523590088 1.86 -2.5201983451843266
		 2.892 -2.5530388355255127 3.928 -2.5840432643890381 4.964 -2.613370418548584 5.996 -2.6411373615264893
		 7.032 -2.667431116104126 8.064 -2.6923372745513916 9.1 -2.7159113883972168 10.136 -2.7382268905639648
		 11.168 -2.7593252658843994 12.204 -2.7793195247650142 13.24 -2.7982764244079594 14.272 -2.8163774013519287
		 15.308 -2.8337595462799072 16.34 -2.8505935668945313 17.376 -2.8670670986175537 18.412 -2.8833713531494141
		 19.444 -2.8997042179107666 20.48 -2.9161674976348877 21.516 -2.932900190353394 22.548 -2.9499189853668213
		 23.584 -2.9672174453735352 24.616 -2.9847426414489746 25.652 -3.0023705959320068
		 26.688 -3.0199811458587646 27.72 -3.0374188423156734 28.756 -3.053494930267334 29.792 -3.0660667419433594
		 30.824 -3.0918560028076172 31.86 -3.1020505428314213 32.892 -3.1118271350860596 33.928 -3.1322572231292725
		 34.964 -3.1381783485412602 35.996 -3.1546554565429687 37.032 -3.1613061428070068
		 38.064 -3.1682360172271729 39.1 -3.1733198165893555 40.136 -3.1803789138793945 41.168 -3.1814980506896973
		 42.204 -3.181427001953125 43.24 -3.1753666400909424 44.272 -3.1687483787536621 45.308 -3.1572844982147217
		 46.34 -3.1408634185791016 47.376 -3.1192498207092285 48.412 -3.092148065567017 49.444 -3.0593371391296387
		 50.48 -3.02081298828125 51.516 -2.9767310619354248 52.548 -2.9274623394012451 53.584 -2.8735439777374268
		 54.616 -2.8157069683074951 55.652 -2.7547190189361572 56.688 -2.6915197372436523
		 57.72 -2.6270201206207275 58.756 -2.5621523857116699 59.788 -2.4977645874023437 60.824 -2.4346466064453125
		 61.86 -2.3734323978424072 62.892 -2.3146162033081055 63.928 -2.2585492134094238 64.964 -2.205430269241333
		 65.996 -2.1553196907043457 67.032 -2.1081714630126953 68.064 -2.0638372898101807
		 69.1 -2.0221254825592041 70.136 -1.9828038215637207 71.168 -1.9456228017807005 72.204 -1.9103026390075684
		 73.24 -1.8765294551849363 74.272 -1.8439921140670776 75.308 -1.8123658895492556;
createNode animCurveTA -n "Bip01_Spine2_rotateY3";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 73 ".ktv[0:72]"  0.824 0.30097445845603943 1.86 0.2661813497543335
		 2.892 0.23165051639080048 3.928 0.19757856428623199 4.964 0.16405066847801208 5.996 0.13104753196239471
		 7.032 0.098456405103206635 8.064 0.066100046038627625 9.1 0.033754512667655945 10.136 0.0011512873461470008
		 11.168 -0.031970981508493423 12.204 -0.06587684154510498 13.24 -0.10082653164863586
		 14.272 -0.13706074655056 15.308 -0.17477305233478546 16.34 -0.21409282088279727 17.376 -0.25513729453086853
		 18.412 -0.29792198538780212 19.444 -0.34238743782043457 20.48 -0.38839495182037354
		 21.516 -0.43572100996971136 22.548 -0.48402777314186096 23.584 -0.53292566537857056
		 24.616 -0.58191084861755371 25.652 -0.63046044111251831 26.688 -0.67801517248153687
		 27.72 -0.72404515743255615 28.756 -0.76564908027648926 29.792 -0.797529697418213
		 30.824 -0.86089926958084118 31.86 -0.88538640737533569 32.892 -0.90871423482894864
		 33.928 -0.95710420608520497 34.964 -0.97143429517745983 35.996 -1.0131433010101318
		 37.032 -1.0322693586349487 38.064 -1.0542253255844116 39.1 -1.0730688571929932 40.136 -1.1148455142974854
		 41.168 -1.1334013938903809 42.204 -1.1525167226791382 43.24 -1.2005738019943235 44.272 -1.2241934537887571
		 45.308 -1.2546156644821167 46.34 -1.2878302335739136 47.376 -1.3215526342391968 48.412 -1.3553038835525513
		 49.444 -1.3885263204574585 50.48 -1.4206905364990234 51.516 -1.45128333568573 52.548 -1.4798467159271242
		 53.584 -1.5059665441513064 54.616 -1.529308557510376 55.652 -1.5496153831481934 56.688 -1.5667093992233276
		 57.72 -1.5804591178894043 58.756 -1.5907994508743286 59.788 -1.5976480245590212 60.824 -1.6009649038314819
		 61.86 -1.600674033164978 62.892 -1.5966570377349854 63.928 -1.588817358016968 64.964 -1.5769608020782473
		 65.996 -1.560899019241333 67.032 -1.540401816368103 68.064 -1.515247106552124 69.1 -1.485221266746521
		 70.136 -1.4501795768737793 71.168 -1.4100513458251951 72.204 -1.3648589849472046
		 73.24 -1.3147828578948977 74.272 -1.2601271867752075 75.308 -1.2013300657272341;
createNode animCurveTA -n "Bip01_Spine2_rotateZ3";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 73 ".ktv[0:72]"  0.824 0.49136489629745489 1.86 0.48471811413764959
		 2.892 0.47793194651603699 3.928 0.47090220451354975 4.964 0.46356496214866644 5.996 0.45588141679763794
		 7.032 0.44785547256469727 8.064 0.43951866030693054 9.1 0.43089845776557922 10.136 0.42204770445823669
		 11.168 0.4130423665046693 12.204 0.40393388271331782 13.24 0.39476490020751953 14.272 0.38555178046226501
		 15.308 0.37630981206893921 16.34 0.36704364418983454 17.376 0.35776087641716003 18.412 0.34843552112579346
		 19.444 0.33909246325492859 20.48 0.32973846793174744 21.516 0.32038640975952148 22.548 0.31110337376594543
		 23.584 0.30195668339729309 24.616 0.29306259751319885 25.652 0.2845461368560791 26.688 0.27657404541969299
		 27.72 0.2693055272102356 28.756 0.26319199800491339 29.792 0.25893190503120428 30.824 0.2523188591003418
		 31.86 0.25068584084510803 32.892 0.24969004094600677 33.928 0.25022616982460022 34.964 0.25128597021102905
		 35.996 0.25736477971076965 37.032 0.26170766353607178 38.064 0.26761302351951599
		 39.1 0.27330115437507635 40.136 0.28805556893348694 41.168 0.29496762156486511 42.204 0.30214950442314148
		 43.24 0.31993216276168823 44.272 0.32825011014938354 45.308 0.33861202001571655 46.34 0.34948712587356567
		 47.376 0.36006009578704834 48.412 0.37024366855621343 49.444 0.38004955649375921
		 50.48 0.3894985020160675 51.516 0.3986591100692749 52.548 0.40765330195426946 53.584 0.41664248704910278
		 54.616 0.4258195161819458 55.652 0.43536680936813354 56.688 0.44547459483146662 57.72 0.45628789067268377
		 58.756 0.46792802214622503 59.788 0.4804500043392182 60.824 0.49377542734146118 61.86 0.50779855251312256
		 62.892 0.52230888605117798 63.928 0.53698098659515381 64.964 0.55142420530319214
		 65.996 0.5652158260345459 67.032 0.57791280746459961 68.064 0.58902215957641602 69.1 0.59813386201858521
		 70.136 0.60487806797027588 71.168 0.60894870758056641 72.204 0.61019349098205566
		 73.24 0.60854935646057129 74.272 0.6040763258934021 75.308 0.59691226482391357;
createNode animCurveTA -n "Bip01_Neck_rotateX2";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 73 ".ktv[0:72]"  0.824 0.16074052453041077 1.86 0.16091273725032806
		 2.892 0.16076527535915375 3.928 0.1605420708656311 4.964 0.16047438979148865 5.996 0.16076241433620453
		 7.032 0.16153189539909363 8.064 0.16292771697044373 9.1 0.16498821973800659 10.136 0.16773627698421478
		 11.168 0.17106686532497406 12.204 0.17491726577281952 13.24 0.17884784936904907 14.272 0.18187975883483887
		 15.308 0.18813610076904297 16.34 0.19388400018215179 17.376 0.19560278952121737 18.412 0.20109190046787265
		 19.444 0.20306748151779175 20.48 0.20495134592056272 21.516 0.20616458356380463 22.548 0.20642499625682831
		 23.584 0.20563970506191256 24.616 0.20378348231315613 25.652 0.20080374181270602
		 26.688 0.19669000804424289 27.72 0.19140996038913727 28.756 0.18496344983577728 29.792 0.17733187973499301
		 30.824 0.16851410269737244 31.86 0.15852762758731845 32.892 0.14739084243774414 33.928 0.13512031733989716
		 34.964 0.12172866612672809 35.996 0.10727708786725998 37.032 0.091814495623111725
		 38.064 0.07544340193271637 39.1 0.058240760117769241 40.136 0.040390297770500183
		 41.168 0.022038178518414497 42.204 0.0034145549871027474 43.24 -0.015215425752103329
		 44.272 -0.033564146608114243 45.308 -0.051227733492851257 46.34 -0.067859679460525513
		 47.376 -0.083055257797241211 48.412 -0.096378646790981293 49.444 -0.1074199080467224
		 50.48 -0.11579268425703046 51.516 -0.12114054709672928 52.548 -0.12317193299531937
		 53.584 -0.12164852023124696 54.616 -0.1163798272609711 55.652 -0.10734399408102036
		 56.688 -0.094556458294391632 57.72 -0.078147634863853455 58.756 -0.058333799242973335
		 59.788 -0.035434499382972717 60.824 -0.0098244817927479744 61.86 0.018055632710456848
		 62.892 0.047691412270069129 63.928 0.078553527593612671 64.964 0.110099695622921
		 65.996 0.14181715250015259 67.032 0.17324675619602203 68.064 0.20396941900253296
		 69.1 0.23362976312637329 70.136 0.26193958520889282 71.168 0.28870439529418951 72.204 0.31377747654914856
		 73.24 0.33712196350097656 74.272 0.35874781012535095 75.308 0.3787321150302887;
createNode animCurveTA -n "Bip01_Neck_rotateY2";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 73 ".ktv[0:72]"  0.824 -0.59430474042892456 1.86 -0.59323543310165405
		 2.892 -0.590923011302948 3.928 -0.58829569816589355 4.964 -0.58622020483016968 5.996 -0.58542877435684204
		 7.032 -0.58650612831115723 8.064 -0.58985477685928345 9.1 -0.59569418430328369 10.136 -0.60402172803878784
		 11.168 -0.61466139554977417 12.204 -0.62728190422058105 13.24 -0.64046239852905273
		 14.272 -0.65069025754928589 15.308 -0.67232537269592285 16.34 -0.69266659021377563
		 17.376 -0.69891947507858276 18.412 -0.71925950050354004 19.444 -0.7269471287727356
		 20.48 -0.73466002941131581 21.516 -0.7403220534324646 22.548 -0.7427496314048766
		 23.584 -0.74164420366287231 24.616 -0.73676550388336171 25.652 -0.72793447971344005
		 26.688 -0.71497702598571777 27.72 -0.69778037071228027 28.756 -0.6761857271194458
		 29.792 -0.65013587474822998 30.824 -0.61955070495605469 31.86 -0.58446061611175548
		 32.892 -0.54484987258911133 33.928 -0.50080031156539917 34.964 -0.45238187909126282
		 35.996 -0.39971363544464111 37.032 -0.34299474954605103 38.064 -0.28251546621322632
		 39.1 -0.2186987847089768 40.136 -0.15205249190330505 41.168 -0.0832352414727211 42.204 -0.013030542992055416
		 43.24 0.057573523372411728 44.272 0.12743215262889862 45.308 0.19520150125026703
		 46.34 0.25938645005226141 47.376 0.31839281320571899 48.412 0.37058708071708679 49.444 0.41427960991859442
		 50.48 0.447887122631073 51.516 0.46995082497596735 52.548 0.47917914390563959 53.584 0.4745169579982757
		 54.616 0.45521265268325811 55.652 0.42091295123100281 56.688 0.3716522753238678 57.72 0.30786564946174622
		 58.756 0.23036845028400416 59.788 0.14026737213134766 60.824 0.039037331938743591
		 61.86 -0.071591697633266449 62.892 -0.18964581191539764 63.928 -0.31302934885025024
		 64.964 -0.439596027135849 65.996 -0.56727761030197144 67.032 -0.69414001703262329
		 68.064 -0.81842762231826782 69.1 -0.9385952353477478 70.136 -1.0533243417739868 71.168 -1.1615728139877319
		 72.204 -1.2626131772994997 73.24 -1.3559836149215698 74.272 -1.4415417909622192 75.308 -1.519362211227417;
createNode animCurveTA -n "Bip01_Neck_rotateZ2";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 73 ".ktv[0:72]"  0.824 -1.0827604532241819 1.86 -1.1661128997802734
		 2.892 -1.25246262550354 3.928 -1.3410104513168335 4.964 -1.4307482242584229 5.996 -1.5204228162765503
		 7.032 -1.6086190938949585 8.064 -1.6937630176544187 9.1 -1.7741988897323608 10.136 -1.848231315612793
		 11.168 -1.9142485857009888 12.204 -1.9707248210906989 13.24 -2.0150434970855713 14.272 -2.0429441928863525
		 15.308 -2.0752456188201904 16.34 -2.0839252471923828 17.376 -2.0799422264099121 18.412 -2.049602746963501
		 19.444 -2.0236749649047856 20.48 -1.9828454256057739 21.516 -1.9305859804153442 22.548 -1.8693948984146118
		 23.584 -1.8007251024246216 24.616 -1.7261110544204712 25.652 -1.647058367729187 26.688 -1.5649830102920534
		 27.72 -1.481203556060791 28.756 -1.3968589305877686 29.792 -1.312933087348938 30.824 -1.2301863431930542
		 31.86 -1.1491905450820925 32.892 -1.0702160596847534 33.928 -0.99337399005889881
		 34.964 -0.91850978136062611 35.996 -0.84534645080566395 37.032 -0.77346408367156971
		 38.064 -0.7023700475692749 39.1 -0.63151007890701294 40.136 -0.56031298637390137
		 41.168 -0.48829889297485363 42.204 -0.4150689840316773 43.24 -0.34038454294204712
		 44.272 -0.26411142945289612 45.308 -0.18632735311985016 46.34 -0.10726804286241531
		 47.376 -0.02734378352761269 48.412 0.052903890609741218 49.444 0.13284452259540558
		 50.48 0.21183723211288452 51.516 0.28926888108253479 52.548 0.36457756161689758 53.584 0.43735146522521978
		 54.616 0.50728249549865723 55.652 0.57428997755050659 56.688 0.63840705156326294
		 57.72 0.69982224702835083 58.756 0.75882399082183838 59.788 0.81575632095336914 60.824 0.87097567319869995
		 61.86 0.9247174859046936 62.892 0.97715497016906716 63.928 1.028223991394043 64.964 1.0776205062866211
		 65.996 1.1246875524520874 67.032 1.1684077978134155 68.064 1.2074984312057495 69.1 1.2403954267501831
		 70.136 1.2652771472930908 71.168 1.2802459001541138 72.204 1.2832198143005371 73.24 1.272180438041687
		 74.272 1.2451866865158081 75.308 1.2005463838577273;
createNode animCurveTA -n "Bip01_Head_rotateX2";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 73 ".ktv[0:72]"  0.824 -0.047628022730350494 1.86 -0.047202099114656448
		 2.892 -0.046630430966615677 3.928 -0.045998059213161475 4.964 -0.043931368738412857
		 5.996 -0.042577926069498062 7.032 -0.040562365204095847 8.064 -0.038074027746915817
		 9.1 -0.035187013447284698 10.136 -0.031950823962688446 11.168 -0.02843369543552399
		 12.204 -0.024688916280865669 13.24 -0.020847825333476067 14.272 -0.016985246911644936
		 15.308 -0.013241611421108246 16.34 -0.0097226072102785128 17.376 -0.0067001827992498875
		 18.412 -0.0046353843063116074 19.444 -0.0014714231947436929 20.48 0.00050057831685990095
		 21.516 0.00083100906340405345 22.548 0.001225681509822607 23.584 0.00059635716024786234
		 24.616 -0.001374997547827661 25.652 -0.0030212975107133389 26.688 -0.0056180772371590137
		 27.72 -0.0089383572340011597 28.756 -0.012811588123440742 29.792 -0.017187263816595078
		 30.824 -0.021964747458696365 31.86 -0.02704760804772377 32.892 -0.032325658947229385
		 33.928 -0.037703871726989746 34.964 -0.043053481727838509 35.996 -0.048291619867086411
		 37.032 -0.053292106837034225 38.064 -0.057984769344329834 39.1 -0.062297780066728592
		 40.136 -0.06613989919424057 41.168 -0.069476976990699768 42.204 -0.07219347357749939
		 43.24 -0.073956385254859924 44.272 -0.076393149793148055 45.308 -0.077769026160240173
		 46.34 -0.077961094677448273 47.376 -0.078056007623672485 48.412 -0.07738622277975081
		 49.444 -0.075745768845081329 50.48 -0.074464239180088043 51.516 -0.072696208953857422
		 52.548 -0.07099025696516037 53.584 -0.066598422825336456 54.616 -0.064401373267173767
		 55.652 -0.061592146754264832 56.688 -0.058630753308534622 57.72 -0.055789731442928314
		 58.756 -0.05316944420337677 59.788 -0.050872359424829483 60.824 -0.048938509076833725
		 61.86 -0.04746701568365097 62.892 -0.046489093452692032 63.928 -0.046017784625291824
		 64.964 -0.046035584062337875 65.996 -0.046448361128568649 67.032 -0.04703647643327713
		 68.064 -0.049158647656440742 69.1 -0.050421979278326035 70.136 -0.052139434963464737
		 71.168 -0.054077699780464179 72.204 -0.056049630045890801 73.24 -0.057970065623521798
		 74.272 -0.059748776257038116 75.308 -0.061310574412345879;
createNode animCurveTA -n "Bip01_Head_rotateY2";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 73 ".ktv[0:72]"  0.824 -0.83651238679885853 1.86 -0.83237665891647339
		 2.892 -0.82453733682632446 3.928 -0.81468147039413452 4.964 -0.77849304676055908
		 5.996 -0.75400620698928833 7.032 -0.71749031543731701 8.064 -0.67186206579208385
		 9.1 -0.61904942989349365 10.136 -0.56002539396286011 11.168 -0.49611482024192816
		 12.204 -0.42894339561462402 13.24 -0.36030337214469915 14.272 -0.2921423614025116
		 15.308 -0.22647553682327273 16.34 -0.16533146798610687 17.376 -0.112966388463974
		 18.412 -0.077439166605472565 19.444 -0.023650897666811943 20.48 0.0096337888389825821
		 21.516 0.015177176333963874 22.548 0.021317904815077785 23.584 0.0099826995283365232
		 24.616 -0.024046756327152252 25.652 -0.051924653351306915 26.688 -0.095702797174453721
		 27.72 -0.15082785487174988 28.756 -0.21454218029975891 29.792 -0.28528320789337164
		 30.824 -0.36137473583221441 31.86 -0.441048264503479 32.892 -0.52252554893493652
		 33.928 -0.60405534505844116 34.964 -0.68396848440170288 35.996 -0.76075232028961182
		 37.032 -0.83309978246688843 38.064 -0.8999021053314209 39.1 -0.96026295423507702
		 40.136 -1.0135061740875244 41.168 -1.0591564178466797 42.204 -1.0955559015274048
		 43.24 -1.1189990043640139 44.272 -1.1505807638168337 45.308 -1.1670987606048584 46.34 -1.169100284576416
		 47.376 -1.1680641174316406 48.412 -1.1571593284606934 49.444 -1.132663369178772 50.48 -1.1144838333129885
		 51.516 -1.0892146825790403 52.548 -1.0651698112487793 53.584 -1.0042920112609863
		 54.616 -0.97406208515167236 55.652 -0.93566060066223133 56.688 -0.89530307054519664
		 57.72 -0.85688126087188721 58.756 -0.82184642553329468 59.788 -0.79155182838439941
		 60.824 -0.76717352867126465 61.86 -0.74962329864501953 62.892 -0.73954886198043823
		 63.928 -0.73726880550384533 64.964 -0.74279528856277477 65.996 -0.75414007902145397
		 67.032 -0.76739054918289185 68.064 -0.80954593420028687 69.1 -0.83350288867950428
		 70.136 -0.86597132682800293 71.168 -0.90236353874206532 72.204 -0.93967121839523315
		 73.24 -0.97652351856231701 74.272 -1.0116257667541504 75.308 -1.0438026189804075;
createNode animCurveTA -n "Bip01_Head_rotateZ2";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 73 ".ktv[0:72]"  0.824 2.9197874069213867 1.86 2.8948256969451904
		 2.892 2.8768880367279053 3.928 2.866585493087769 4.964 2.858875036239624 5.996 2.8615629673004155
		 7.032 2.86922287940979 8.064 2.8817672729492187 9.1 2.8986968994140625 10.136 2.9190819263458252
		 11.168 2.9419043064117432 12.204 2.9661073684692383 13.24 2.9906966686248779 14.272 3.0147805213928223
		 15.308 3.037677526473999 16.34 3.0588862895965576 17.376 3.0772194862365723 18.412 3.0898761749267578
		 19.444 3.1120939254760742 20.48 3.1317994594573975 21.516 3.1374080181121826 22.548 3.1572964191436768
		 23.584 3.1783692836761475 24.616 3.2088050842285156 25.652 3.2288014888763428 26.688 3.2589964866638184
		 27.72 3.2958672046661377 28.756 3.3380703926086426 29.792 3.3852565288543701 30.824 3.4367485046386723
		 31.86 3.491661548614502 32.892 3.5488574504852295 33.928 3.6071004867553715 34.964 3.6651065349578857
		 35.996 3.7216429710388192 37.032 3.7756450176239018 38.064 3.8262298107147217 39.1 3.872774600982666
		 40.136 3.9148387908935538 41.168 3.9521863460540767 42.204 3.983281135559082 43.24 4.0041255950927734
		 44.272 4.0367021560668945 45.308 4.0596513748168945 46.34 4.064544677734375 47.376 4.0758538246154785
		 48.412 4.0789289474487305 49.444 4.0730338096618652 50.48 4.0647540092468262 51.516 4.0517449378967285
		 52.548 4.0374746322631836 53.584 3.9928936958312993 54.616 3.966227769851685 55.652 3.9286379814147949
		 56.688 3.8841888904571533 57.72 3.8354582786560054 58.756 3.7834832668304448 59.788 3.7294321060180673
		 60.824 3.6745443344116206 61.86 3.6201131343841553 62.892 3.5672857761383057 63.928 3.5171208381652836
		 64.964 3.4703757762908936 65.996 3.4296715259552006 67.032 3.3994944095611572 68.064 3.3425633907318115
		 69.1 3.3193504810333252 70.136 3.291959285736084 71.168 3.2636785507202148 72.204 3.235492467880249
		 73.24 3.2061078548431396 74.272 3.1744129657745361 75.308 3.1394925117492676;
createNode animCurveTA -n "Bip01_R_Clavicle_rotateX3";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 73 ".ktv[0:72]"  0.824 0.8579590916633606 1.86 0.84698432683944702
		 2.892 0.8351406455039978 3.928 0.82364916801452637 4.964 0.81358796358108532 5.996 0.80522561073303212
		 7.032 0.79828953742980957 8.064 0.79954004287719727 9.1 0.79800546169281006 10.136 0.79900276660919189
		 11.168 0.79822194576263428 12.204 0.80642563104629517 13.24 0.81056094169616699 14.272 0.80697423219680786
		 15.308 0.81972289085388195 16.34 0.82921278476715088 17.376 0.81486481428146373 18.412 0.82378607988357533
		 19.444 0.81218618154525757 20.48 0.80209463834762573 21.516 0.79072463512420654 22.548 0.77762883901596069
		 23.584 0.7644914984703064 24.616 0.73774063587188721 25.652 0.72081249952316295 26.688 0.69487535953521729
		 27.72 0.66861271858215332 28.756 0.64267122745513916 29.792 0.6073189377784729 30.824 0.56566953659057617
		 31.86 0.53615236282348633 32.892 0.48561629652976984 33.928 0.43309080600738525 34.964 0.37578994035720825
		 35.996 0.31216850876808167 37.032 0.24165546894073492 38.064 0.16458429396152496
		 39.1 0.080636471509933472 40.136 -0.0097084566950798035 41.168 -0.10582996159791946
		 42.204 -0.20656365156173703 43.24 -0.31055662035942078 44.272 -0.41588953137397766
		 45.308 -0.52129834890365601 46.34 -0.62699717283248901 47.376 -0.70673292875289917
		 48.412 -0.79930615425109852 49.444 -0.85572153329849243 50.48 -0.91100138425827037
		 51.516 -0.94412899017333984 52.548 -0.9384886622428894 53.584 -0.92302989959716786
		 54.616 -0.86395466327667236 55.652 -0.78841334581375122 56.688 -0.6850578784942627
		 57.72 -0.5469861626625061 58.756 -0.38801798224449158 59.788 -0.20896314084529879
		 60.824 -0.01284880191087723 61.86 0.19620157778263092 62.892 0.41381755471229553
		 63.928 0.63556522130966187 64.964 0.8573036789894104 65.996 1.0751456022262571 67.032 1.2856454849243164
		 68.064 1.4871847629547119 69.1 1.6742242574691772 70.136 1.852463364601135 71.168 2.0095908641815186
		 72.204 2.1479241847991943 73.24 2.2773361206054687 74.272 2.3874533176422119 75.308 2.4804689884185791;
createNode animCurveTA -n "Bip01_R_Clavicle_rotateY3";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 73 ".ktv[0:72]"  0.824 78.373794555664063 1.86 78.390922546386719
		 2.892 78.406539916992188 3.928 78.420425415039063 4.964 78.432632446289063 5.996 78.444389343261719
		 7.032 78.45794677734375 8.064 78.459518432617188 9.1 78.474113464355469 10.136 78.490119934082031
		 11.168 78.516792297363281 12.204 78.528884887695312 13.24 78.552742004394531 14.272 78.586982727050781
		 15.308 78.615242004394531 16.34 78.649276733398438 17.376 78.700393676757813 18.412 78.739372253417969
		 19.444 78.790596008300781 20.48 78.839927673339844 21.516 78.887344360351563 22.548 78.93017578125
		 23.584 78.963188171386719 24.616 79.017494201660156 25.652 79.036750793457017 26.688 79.064697265625014
		 27.72 79.080177307128906 28.756 79.080978393554687 29.792 79.089836120605469 30.824 79.098739624023438
		 31.86 79.066154479980469 32.892 79.067314147949219 33.928 79.059112548828125 34.964 79.048301696777358
		 35.996 79.038673400878906 37.032 79.031600952148438 38.064 79.027671813964844 39.1 79.027824401855469
		 40.136 79.032302856445313 41.168 79.041236877441406 42.204 79.054306030273438 43.24 79.070884704589844
		 44.272 79.089996337890625 45.308 79.112884521484375 46.34 79.142822265625 47.376 79.139358520507813
		 48.412 79.171897888183594 49.444 79.16336822509767 50.48 79.175155639648438 51.516 79.176788330078125
		 52.548 79.144699096679673 53.584 79.127883911132813 54.616 79.073554992675781 55.652 79.025711059570327
		 56.688 78.965072631835938 57.72 78.877738952636719 58.756 78.783378601074219 59.788 78.679275512695313
		 60.824 78.565650939941406 61.86 78.444374084472656 62.892 78.317573547363281 63.928 78.18768310546875
		 64.964 78.057197570800781 65.996 77.928909301757813 67.032 77.805671691894531 68.064 77.687263488769531
		 69.1 77.583572387695313 70.136 77.477676391601591 71.168 77.399703979492188 72.204 77.343620300292983
		 73.24 77.281929016113281 74.272 77.243827819824219 75.308 77.224998474121094;
createNode animCurveTA -n "Bip01_R_Clavicle_rotateZ3";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 73 ".ktv[0:72]"  0.824 176.4693603515625 1.86 176.548828125
		 2.892 176.62930297851562 3.928 176.71145629882812 4.964 176.79573059082034 5.996 176.88192749023435
		 7.032 176.96957397460935 8.064 177.05809020996094 9.1 177.14566040039065 10.136 177.23091125488281
		 11.168 177.31182861328125 12.204 177.38668823242187 13.24 177.4505615234375 14.272 177.49359130859375
		 15.308 177.55921936035156 16.34 177.59986877441406 17.376 177.60585021972656 18.412 177.60897827148437
		 19.444 177.59632873535156 20.48 177.56941223144531 21.516 177.52816772460935 22.548 177.47303771972656
		 23.584 177.40476989746094 24.616 177.3245849609375 25.652 177.23326110839844 26.688 177.13201904296875
		 27.72 177.02177429199219 28.756 176.90333557128906 29.792 176.77760314941406 30.824 176.64509582519531
		 31.86 176.50697326660159 32.892 176.36257934570312 33.928 176.21266174316406 34.964 176.05714416503906
		 35.996 175.89593505859375 37.032 175.72866821289062 38.064 175.55555725097656 39.1 175.3763427734375
		 40.136 175.19119262695312 41.168 175.00053405761722 42.204 174.80534362792969 43.24 174.60682678222656
		 44.272 174.40681457519531 45.308 174.2076416015625 46.34 174.01130676269531 47.376 173.82781982421878
		 48.412 173.64921569824219 49.444 173.49275207519531 50.48 173.35090637207031 51.516 173.23280334472656
		 52.548 173.14605712890625 53.584 173.08340454101562 54.616 173.05673217773435 55.652 173.05781555175784
		 56.688 173.08979797363281 57.72 173.1531982421875 58.756 173.24203491210935 59.788 173.35359191894531
		 60.824 173.48399353027344 61.86 173.62904357910156 62.892 173.78436279296875 63.928 173.94581604003906
		 64.964 174.10984802246094 65.996 174.27363586425781 67.032 174.43524169921878 68.064 174.5936279296875
		 69.1 174.74848937988281 70.136 174.8995361328125 71.168 175.04902648925781 72.204 175.19850158691406
		 73.24 175.3477783203125 74.272 175.50105285644531 75.308 175.6603698730469;
createNode animCurveTA -n "Bip01_R_UpperArm_rotateX3";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 73 ".ktv[0:72]"  0.824 4.3675298690795898 1.86 4.4145126342773437
		 2.892 4.457427978515625 3.928 4.5236926078796387 4.964 4.6084737777709961 5.996 4.7138242721557617
		 7.032 4.8806676864624023 8.064 5.0403475761413574 9.1 5.1440839767456055 10.136 5.290092945098877
		 11.168 5.4942831993103027 12.204 5.703681468963623 13.24 5.9698019027709961 14.272 6.366905689239502
		 15.308 6.8103137016296387 16.34 7.2242336273193395 17.376 7.9827175140380868 18.412 9.7029809951782227
		 19.444 12.488667488098146 20.48 15.74100875854492 21.516 18.726190567016602 22.548 20.91758918762207
		 23.584 21.987005233764648 24.616 22.208440780639648 25.652 22.165172576904297 26.688 21.760492324829105
		 27.72 20.598356246948239 28.756 18.870166778564453 29.792 17.042409896850586 30.824 15.279889106750487
		 31.86 13.511205673217772 32.892 11.659849166870115 33.928 9.8272285461425781 34.964 8.1391515731811523
		 35.996 6.5389394760131836 37.032 5.0300459861755371 38.064 3.6652009487152104 39.1 2.2441627979278564
		 40.136 0.4473468959331513 41.168 -1.7283318042755127 42.204 -3.9014370441436768 43.24 -5.7732963562011719
		 44.272 -7.4497041702270499 45.308 -9.2059650421142578 46.34 -11.263961791992188 47.376 -13.737573623657228
		 48.412 -16.493312835693359 49.444 -19.314586639404297 50.48 -21.974454879760746 51.516 -24.129451751708984
		 52.548 -25.794448852539063 53.584 -27.328521728515625 54.616 -28.820568084716797
		 55.652 -30.118844985961914 56.688 -31.089008331298828 57.72 -31.722057342529297 58.756 -32.156661987304687
		 59.788 -32.553302764892578 60.824 -33.047565460205078 61.86 -33.674266815185554 62.892 -34.381248474121094
		 63.928 -35.215427398681641 64.964 -36.248603820800781 65.996 -37.44158935546875 67.032 -38.66998291015625
		 68.064 -39.800983428955078 69.1 -40.839599609375 70.136 -41.755741119384766 71.168 -42.394134521484375
		 72.204 -42.823772430419922 73.24 -43.213626861572266 74.272 -43.503486633300781 75.308 -43.613388061523437;
createNode animCurveTA -n "Bip01_R_UpperArm_rotateY3";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 73 ".ktv[0:72]"  0.824 -68.078475952148438 1.86 -68.388832092285156
		 2.892 -68.648719787597656 3.928 -68.849021911621094 4.964 -69.029808044433594 5.996 -69.201469421386719
		 7.032 -69.362236022949219 8.064 -69.537117004394531 9.1 -69.718299865722656 10.136 -69.880447387695313
		 11.168 -70.034324645996094 12.204 -70.200447082519531 13.24 -70.352943420410156 14.272 -70.434494018554687
		 15.308 -70.536643981933594 16.34 -70.759811401367188 17.376 -70.743675231933594 18.412 -69.966346740722656
		 19.444 -68.351982116699219 20.48 -66.14654541015625 21.516 -63.66424560546875 22.548 -61.419017791748047
		 23.584 -60.214881896972663 24.616 -60.277332305908203 25.652 -60.989192962646484
		 26.688 -62.131870269775391 27.72 -63.684299468994148 28.756 -65.106544494628906 29.792 -66.081214904785156
		 30.824 -66.705245971679688 31.86 -67.115966796875 32.892 -67.445930480957031 33.928 -67.721405029296875
		 34.964 -67.880683898925781 35.996 -67.924728393554688 37.032 -67.922142028808594
		 38.064 -67.904029846191406 39.1 -67.873603820800781 40.136 -67.872047424316406 41.168 -67.948028564453125
		 42.204 -68.068046569824219 43.24 -68.133285522460938 44.272 -68.11859130859375 45.308 -68.099899291992188
		 46.34 -68.159461975097656 47.376 -68.344146728515625 48.412 -68.694618225097656 49.444 -69.223045349121094
		 50.48 -69.75640869140625 51.516 -70.041091918945312 52.548 -70.104194641113281 53.584 -70.126304626464844
		 54.616 -70.144477844238281 55.652 -70.133689880371094 56.688 -70.094635009765625
		 57.72 -70.030494689941406 58.756 -69.943702697753906 59.788 -69.830268859863281 60.824 -69.675529479980469
		 61.86 -69.50225830078125 62.892 -69.382308959960937 63.928 -69.334503173828125 64.964 -69.30828857421875
		 65.996 -69.292182922363281 67.032 -69.316207885742187 68.064 -69.369285583496094
		 69.1 -69.407539367675781 70.136 -69.429298400878906 71.168 -69.467369079589844 72.204 -69.509910583496094
		 73.24 -69.538337707519531 74.272 -69.599899291992188 75.308 -69.710624694824219;
createNode animCurveTA -n "Bip01_R_UpperArm_rotateZ3";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 73 ".ktv[0:72]"  0.824 -7.1289496421813974 1.86 -7.5181937217712393
		 2.892 -7.8556637763977051 3.928 -8.1622705459594727 4.964 -8.4458494186401367 5.996 -8.7103986740112305
		 7.032 -9.013422966003418 8.064 -9.2728223800659197 9.1 -9.4092073440551758 10.136 -9.5410375595092773
		 11.168 -9.6996517181396484 12.204 -9.8311462402343768 13.24 -9.9992685317993164 14.272 -10.264272689819336
		 15.308 -10.541569709777832 16.34 -10.789201736450195 17.376 -11.384246826171877 18.412 -13.015454292297363
		 19.444 -15.91006374359131 20.48 -19.446619033813477 21.516 -22.801530838012695 22.548 -25.376249313354492
		 23.584 -26.78282356262207 24.616 -27.37685585021973 25.652 -27.830770492553714 26.688 -27.88538932800293
		 27.72 -27.004987716674805 28.756 -25.496164321899418 29.792 -23.91070556640625 30.824 -22.388399124145508
		 31.86 -20.860258102416992 32.892 -19.279106140136719 33.928 -17.796554565429688 34.964 -16.555267333984375
		 35.996 -15.460625648498532 37.032 -14.53938674926758 38.064 -13.877575874328612 39.1 -13.187535285949709
		 40.136 -12.037394523620604 41.168 -10.451801300048828 42.204 -8.9478311538696289
		 43.24 -7.8708348274230957 44.272 -7.0413641929626465 45.308 -6.1145153045654297 46.34 -4.8286261558532715
		 47.376 -3.0631906986236572 48.412 -1.0417544841766355 49.444 0.88086163997650146
		 50.48 2.503131628036499 51.516 3.6997091770172119 52.548 4.5980319976806641 53.584 5.4470458030700684
		 54.616 6.2119097709655762 55.652 6.755279541015625 56.688 7.1086220741271973 57.72 7.4307665824890172
		 58.756 7.8164339065551758 59.788 8.244389533996582 60.824 8.8092632293701172 61.86 9.6045513153076172
		 62.892 10.54678440093994 63.928 11.672921180725098 64.964 13.117693901062012 65.996 14.837886810302734
		 67.032 16.639492034912109 68.064 18.360013961791992 69.1 20.045854568481445 70.136 21.665012359619141
		 71.168 22.972997665405273 72.204 23.990955352783203 73.24 24.898416519165039 74.272 25.672773361206055
		 75.308 26.310466766357425;
createNode animCurveTA -n "Bip01_R_Forearm_rotateY3";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 73 ".ktv[0:72]"  0.824 2.0361413955688477 1.86 1.9976980686187742
		 2.892 1.9664791822433474 3.928 1.9425684213638299 4.964 1.9242614507675171 5.996 1.9110622406005853
		 7.032 1.9005371332168579 8.064 1.8901369571685793 9.1 1.8804335594177248 10.136 1.8710727691650395
		 11.168 1.8602945804595945 12.204 1.8482949733734133 13.24 1.8361054658889775 14.272 1.8239268064498901
		 15.308 1.8122087717056274 16.34 1.8026038408279421 17.376 1.7970432043075559 18.412 1.7977371215820313
		 19.444 1.803366899490356 20.48 1.8197656869888308 21.516 1.854600787162781 22.548 1.9073579311370852
		 23.584 1.9675140380859375 24.616 2.0290853977203369 25.652 2.0861139297485352 26.688 2.1262459754943848
		 27.72 2.1462681293487553 28.756 2.1534264087677002 29.792 2.1545591354370117 30.824 2.1534509658813477
		 31.86 2.151814222335815 32.892 2.1500179767608643 33.928 2.1481435298919682 34.964 2.1465740203857422
		 35.996 2.1458163261413574 37.032 2.1462578773498535 38.064 2.1470761299133301 39.1 2.1464054584503174
		 40.136 2.1434700489044189 41.168 2.1393148899078369 42.204 2.1357593536376953 43.24 2.1336913108825684
		 44.272 2.1321415901184082 45.308 2.129758358001709 46.34 2.1261091232299805 47.376 2.1211183071136475
		 48.412 2.115243673324585 49.444 2.1105227470397949 50.48 2.109529972076416 51.516 2.1127679347991943
		 52.548 2.1180999279022217 53.584 2.1228008270263672 54.616 2.125622034072876 55.652 2.1265804767608643
		 56.688 2.1264443397521977 57.72 2.1260876655578613 58.756 2.124798059463501 59.788 2.1238734722137451
		 60.824 2.1212813854217529 61.86 2.1168639659881596 62.892 2.1121475696563721 63.928 2.1087543964385982
		 64.964 2.1075968742370605 65.996 2.1098434925079346 67.032 2.1161055564880371 68.064 2.1235220432281494
		 69.1 2.1279454231262207 70.136 2.1286673545837398 71.168 2.12477707862854 72.204 2.1219613552093506
		 73.24 2.117024183273315 74.272 2.1081945896148682 75.308 2.0917377471923828;
createNode animCurveTA -n "Bip01_R_Forearm_rotateX3";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 73 ".ktv[0:72]"  0.824 -2.8802027702331543 1.86 -2.9770183563232422
		 2.892 -3.0470187664031982 3.928 -3.0969624519348145 4.964 -3.1330022811889648 5.996 -3.1566636562347412
		 7.032 -3.1745514869689941 8.064 -3.1922922134399414 9.1 -3.2077717781066899 10.136 -3.2216377258300781
		 11.168 -3.237396240234375 12.204 -3.2533905506134033 13.24 -3.2674458026885982 14.272 -3.2808375358581543
		 15.308 -3.2942843437194824 16.34 -3.305586576461792 17.376 -3.3122673034667969 18.412 -3.3143250942230225
		 19.444 -3.3080024719238281 20.48 -3.2861998081207275 21.516 -3.234366655349731 22.548 -3.1479113101959229
		 23.584 -3.0382089614868164 24.616 -2.9053049087524414 25.652 -2.7453641891479492
		 26.688 -2.5900542736053467 27.72 -2.4830503463745117 28.756 -2.4334306716918945 29.792 -2.4227168560028076
		 30.824 -2.4298417568206787 31.86 -2.4421215057373047 32.892 -2.4554464817047119 33.928 -2.4667210578918457
		 34.964 -2.4738345146179199 35.996 -2.4780256748199467 37.032 -2.4778037071228027
		 38.064 -2.4733915328979492 39.1 -2.4722659587860107 40.136 -2.4811651706695557 41.168 -2.4992353916168213
		 42.204 -2.518733024597168 43.24 -2.5336167812347412 44.272 -2.5448331832885742 45.308 -2.5554163455963135
		 46.34 -2.5679450035095215 47.376 -2.5842022895812988 48.412 -2.6019840240478516 49.444 -2.6154215335845947
		 50.48 -2.6206715106964111 51.516 -2.6181330680847168 52.548 -2.6108558177947998 53.584 -2.6022815704345703
		 54.616 -2.5933780670166016 55.652 -2.5826694965362549 56.688 -2.5697596073150639
		 57.72 -2.5586719512939453 58.756 -2.5550985336303715 59.788 -2.5582451820373535 60.824 -2.5671167373657227
		 61.86 -2.5792906284332275 62.892 -2.592127799987793 63.928 -2.6054725646972656 64.964 -2.6202793121337891
		 65.996 -2.6332175731658936 67.032 -2.6396539211273193 68.064 -2.6427476406097412
		 69.1 -2.6487977504730225 70.136 -2.6559135913848877 71.168 -2.6587300300598145 72.204 -2.6614139080047607
		 73.24 -2.6702654361724858 74.272 -2.6938836574554443 75.308 -2.7396092414855961;
createNode animCurveTA -n "Bip01_R_Forearm_rotateZ3";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 73 ".ktv[0:72]"  0.824 -101.02196502685548 1.86 -103.76438903808594
		 2.892 -105.781494140625 3.928 -107.23879241943359 4.964 -108.30238342285156 5.996 -109.01371002197266
		 7.032 -109.55807495117187 8.064 -110.09796142578124 9.1 -110.57706451416016 10.136 -111.014892578125
		 11.168 -111.51472473144533 12.204 -112.03672027587892 13.24 -112.51884460449222 14.272 -112.98787689208984
		 15.308 -113.45413970947266 16.34 -113.84376525878908 17.376 -114.07262420654295 18.412 -114.10698699951172
		 19.444 -113.8857421875 20.48 -113.16605377197266 21.516 -111.51876831054687 22.548 -108.85044097900392
		 23.584 -105.56034088134766 24.616 -101.71338653564452 25.652 -97.249778747558594
		 26.688 -93.029655456542983 27.72 -90.165214538574219 28.756 -88.845260620117187 29.792 -88.561233520507813
		 30.824 -88.7508544921875 31.86 -89.077178955078125 32.892 -89.431510925292983 33.928 -89.732170104980469
		 34.964 -89.923126220703125 35.996 -90.035133361816406 37.032 -90.028022766113281
		 38.064 -89.909835815429702 39.1 -89.88250732421875 40.136 -90.124328613281236 41.168 -90.610877990722685
		 42.204 -91.133140563964844 43.24 -91.5296630859375 44.272 -91.828285217285156 45.308 -92.1136474609375
		 46.34 -92.455230712890625 47.376 -92.900413513183594 48.412 -93.390258789062514 49.444 -93.762786865234375
		 50.48 -93.903968811035156 51.516 -93.821624755859375 52.548 -93.607513427734375 53.584 -93.363609313964844
		 54.616 -93.120018005371094 55.652 -92.837165832519545 56.688 -92.501548767089844
		 57.72 -92.213905334472656 58.756 -92.126655578613281 59.788 -92.212898254394531 60.824 -92.455894470214844
		 61.86 -92.794639587402315 62.892 -93.152381896972656 63.928 -93.516311645507813 64.964 -93.905525207519531
		 65.996 -94.229537963867188 67.032 -94.370223999023452 68.064 -94.424125671386719
		 69.1 -94.570007324218764 70.136 -94.755653381347656 71.168 -94.837692260742187 72.204 -94.915557861328125
		 73.24 -95.162223815917969 74.272 -95.811149597167969 75.308 -97.075645446777344;
createNode animCurveTA -n "Bip01_L_Clavicle_rotateX3";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 73 ".ktv[0:72]"  0.824 2.1308090686798096 1.86 2.1262953281402588
		 2.892 2.119831800460815 3.928 2.1135585308074951 4.964 2.109541654586792 5.996 2.1093454360961914
		 7.032 2.1149272918701172 8.064 2.1284158229827881 9.1 2.1423604488372803 10.136 2.1701617240905762
		 11.168 2.2043511867523193 12.204 2.2466964721679687 13.24 2.2818348407745361 14.272 2.3270041942596436
		 15.308 2.3881111145019531 16.34 2.4495611190795894 17.376 2.4764795303344727 18.412 2.5507080554962158
		 19.444 2.5774102210998535 20.48 2.617762565612793 21.516 2.6386499404907227 22.548 2.6585566997528076
		 23.584 2.6646673679351807 24.616 2.6570794582366943 25.652 2.6377348899841309 26.688 2.6027827262878418
		 27.72 2.5539219379425049 28.756 2.489405632019043 29.792 2.4055793285369873 30.824 2.321660041809082
		 31.86 2.2254774570465088 32.892 2.0943777561187744 33.928 1.9741740226745603 34.964 1.8394174575805664
		 35.996 1.6946009397506714 37.032 1.5424209833145142 38.064 1.3841127157211304 39.1 1.2214417457580566
		 40.136 1.0558944940567017 41.168 0.89049369096755981 42.204 0.72967624664306641 43.24 0.55381494760513306
		 44.272 0.40160900354385376 45.308 0.25612246990203857 46.34 0.10573186725378036 47.376 -0.01753564178943634
		 48.412 -0.1263812929391861 49.444 -0.22742952406406405 50.48 -0.29591181874275208
		 51.516 -0.34966453909873962 52.548 -0.3766777515411377 53.584 -0.37640449404716497
		 54.616 -0.34796491265296936 55.652 -0.29048201441764832 56.688 -0.20348922908306122
		 57.72 -0.087144128978252411 58.756 0.057983051985502236 59.788 0.23062817752361298
		 60.824 0.43072152137756353 61.86 0.65354925394058228 62.892 0.89806938171386719 63.928 1.1611132621765137
		 64.964 1.4374635219573979 65.996 1.7260724306106567 67.032 2.0216448307037354 68.064 2.3213684558868408
		 69.1 2.6233212947845459 70.136 2.9121100902557373 71.168 3.2065083980560303 72.204 3.4954335689544682
		 73.24 3.7544445991516109 74.272 4.0360836982727051 75.308 4.2809844017028817;
createNode animCurveTA -n "Bip01_L_Clavicle_rotateY3";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 73 ".ktv[0:72]"  0.824 -80.744529724121094 1.86 -80.744194030761719
		 2.892 -80.74407958984375 3.928 -80.744972229003906 4.964 -80.747734069824219 5.996 -80.7530517578125
		 7.032 -80.762321472167969 8.064 -80.777374267578125 9.1 -80.787315368652358 10.136 -80.810775756835938
		 11.168 -80.837562561035156 12.204 -80.870895385742188 13.24 -80.891914367675781 14.272 -80.932693481445313
		 15.308 -80.969505310058594 16.34 -81.008033752441406 17.376 -81.030181884765625 18.412 -81.082786560058594
		 19.444 -81.09991455078125 20.48 -81.133865356445312 21.516 -81.146095275878906 22.548 -81.163528442382812
		 23.584 -81.170433044433594 24.616 -81.167495727539063 25.652 -81.15775299072267 26.688 -81.13653564453125
		 27.72 -81.106346130371094 28.756 -81.064987182617188 29.792 -81.007064819335938 30.824 -80.958671569824219
		 31.86 -80.902976989746094 32.892 -80.807052612304688 33.928 -80.735137939453125 34.964 -80.650230407714844
		 35.996 -80.557670593261719 37.032 -80.460487365722656 38.064 -80.359306335449219
		 39.1 -80.255302429199219 40.136 -80.149337768554688 41.168 -80.044792175292983 42.204 -79.947280883789063
		 43.24 -79.818748474121094 44.272 -79.727096557617188 45.308 -79.63836669921875 46.34 -79.526039123535156
		 47.376 -79.449378967285156 48.412 -79.380363464355469 49.444 -79.300148010253906
		 50.48 -79.257789611816406 51.516 -79.211585998535156 52.548 -79.184165954589844 53.584 -79.173805236816406
		 54.616 -79.179832458496094 55.652 -79.202346801757813 56.688 -79.241249084472656
		 57.72 -79.2958984375 58.756 -79.365188598632812 59.788 -79.447471618652358 60.824 -79.544181823730469
		 61.86 -79.648612976074219 62.892 -79.761917114257813 63.928 -79.8819580078125 64.964 -80.004074096679673
		 65.996 -80.1307373046875 67.032 -80.257728576660156 68.064 -80.385055541992188 69.1 -80.513626098632813
		 70.136 -80.627548217773437 71.168 -80.753250122070298 72.204 -80.87921142578125 73.24 -80.980293273925781
		 74.272 -81.115455627441406 75.308 -81.221839904785156;
createNode animCurveTA -n "Bip01_L_Clavicle_rotateZ3";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 73 ".ktv[0:72]"  0.824 174.03514099121094 1.86 174.12312316894531
		 2.892 174.21661376953125 3.928 174.31266784667969 4.964 174.40821838378906 5.996 174.50047302246094
		 7.032 174.58644104003906 8.064 174.66304016113281 9.1 174.73135375976562 10.136 174.7841796875
		 11.168 174.82310485839847 12.204 174.84637451171875 13.24 174.85929870605469 14.272 174.85528564453125
		 15.308 174.83364868164065 16.34 174.789306640625 17.376 174.76536560058594 18.412 174.67527770996094
		 19.444 174.62698364257812 20.48 174.55728149414062 21.516 174.48722839355469 22.548 174.41270446777344
		 23.584 174.34141540527344 24.616 174.27519226074219 25.652 174.21492004394531 26.688 174.16400146484375
		 27.72 174.12309265136719 28.756 174.09423828125 29.792 174.07977294921875 30.824 174.07234191894531
		 31.86 174.07754516601565 32.892 174.1043701171875 33.928 174.13424682617187 34.964 174.17680358886719
		 35.996 174.22943115234375 37.032 174.290283203125 38.064 174.35774230957031 39.1 174.42984008789062
		 40.136 174.50468444824219 41.168 174.57994079589844 42.204 174.65289306640625 43.24 174.726318359375
		 44.272 174.78988647460938 45.308 174.84562683105469 46.34 174.89350891113284 47.376 174.92662048339844
		 48.412 174.94560241699219 49.444 174.94903564453125 50.48 174.93315124511719 51.516 174.89805603027344
		 52.548 174.84146118164062 53.584 174.76213073730469 54.616 174.65887451171875 55.652 174.5308837890625
		 56.688 174.37760925292969 57.72 174.19892883300781 58.756 173.99493408203125 59.788 173.76631164550781
		 60.824 173.51396179199219 61.86 173.24009704589844 62.892 172.9468994140625 63.928 172.637451171875
		 64.964 172.31611633300781 65.996 171.98641967773435 67.032 171.65361022949219 68.064 171.32244873046875
		 69.1 170.99737548828125 70.136 170.68930053710935 71.168 170.39295959472656 72.204 170.11686706542969
		 73.24 169.87696838378906 74.272 169.64767456054687 75.308 169.46333312988284;
createNode animCurveTA -n "Bip01_L_UpperArm_rotateX3";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 73 ".ktv[0:72]"  0.824 -191.30534362792969 1.86 -191.56166076660156
		 2.892 -191.72500610351562 3.928 -191.80007934570312 4.964 -191.88523864746097 5.996 -192.02316284179687
		 7.032 -192.19195556640625 8.064 -192.37966918945312 9.1 -192.59346008300781 10.136 -192.82492065429688
		 11.168 -193.04646301269531 12.204 -193.28874206542969 13.24 -193.65266418457031 14.272 -194.19554138183591
		 15.308 -194.83062744140625 16.34 -195.39447021484375 17.376 -195.85662841796875 18.412 -196.2919006347656
		 19.444 -196.73919677734375 20.48 -197.40193176269531 21.516 -198.5877685546875 22.548 -199.75408935546875
		 23.584 -199.77618408203125 24.616 -198.74916076660159 25.652 -197.52584838867187
		 26.688 -196.21635437011719 27.72 -194.81083679199224 28.756 -193.57766723632812 29.792 -192.41365051269531
		 30.824 -191.17143249511719 31.86 -190.02174377441409 32.892 -189.08567810058591 33.928 -188.31248474121097
		 34.964 -187.52459716796875 35.996 -186.51443481445312 37.032 -185.31378173828125
		 38.064 -184.08430480957031 39.1 -182.86610412597656 40.136 -181.66470336914065 41.168 -180.41645812988281
		 42.204 -179.00448608398435 43.24 -177.50157165527347 44.272 -176.01483154296875 45.308 -174.3712158203125
		 46.34 -172.35250854492187 47.376 -169.99160766601562 48.412 -167.46653747558594 49.444 -164.9266357421875
		 50.48 -162.39361572265625 51.516 -160.04244995117187 52.548 -158.44036865234375 53.584 -157.76322937011719
		 54.616 -157.35585021972656 55.652 -156.62503051757812 56.688 -155.60256958007812
		 57.72 -154.55526733398435 58.756 -153.56892395019531 59.788 -152.6763916015625 60.824 -151.9725341796875
		 61.86 -151.40481567382812 62.892 -150.82722473144531 63.928 -150.17828369140625 64.964 -149.40476989746094
		 65.996 -148.400634765625 67.032 -147.22337341308594 68.064 -146.10505676269531 69.1 -145.08950805664062
		 70.136 -144.10533142089844 71.168 -143.23912048339844 72.204 -142.52374267578125
		 73.24 -141.87055969238281 74.272 -141.26544189453125 75.308 -140.73768615722656;
createNode animCurveTA -n "Bip01_L_UpperArm_rotateY3";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 73 ".ktv[0:72]"  0.824 117.39365386962893 1.86 117.29781341552734
		 2.892 117.24365997314452 3.928 117.19226837158205 4.964 117.15511322021484 5.996 117.1399383544922
		 7.032 117.12160491943361 8.064 117.06988525390625 9.1 116.99602508544922 10.136 116.94463348388672
		 11.168 116.95674133300781 12.204 117.00971221923828 13.24 117.05316925048828 14.272 117.1815185546875
		 15.308 117.53021240234376 16.34 117.95951843261722 17.376 118.30332183837892 18.412 118.79164123535158
		 19.444 119.61885833740236 20.48 120.56075286865232 21.516 121.46201324462892 22.548 122.25360107421876
		 23.584 122.70639038085936 24.616 122.77687835693359 25.652 122.38370513916014 26.688 121.28424835205078
		 27.72 119.88811492919922 28.756 118.96686553955077 29.792 118.55838775634764 30.824 118.30638122558594
		 31.86 118.16564178466794 32.892 118.18109130859374 33.928 118.30558776855467 34.964 118.46779632568361
		 35.996 118.56388092041016 37.032 118.58297729492186 38.064 118.60630798339842 39.1 118.62488555908206
		 40.136 118.59906768798828 41.168 118.56934356689452 42.204 118.52983856201172 43.24 118.43241119384766
		 44.272 118.23927307128903 45.308 117.83561706542967 46.34 117.22677612304686 47.376 116.6281661987305
		 48.412 116.15697479248044 49.444 115.83421325683594 50.48 115.69869995117187 51.516 115.66676330566406
		 52.548 115.605224609375 53.584 115.51132202148437 54.616 115.4490966796875 55.652 115.43389129638672
		 56.688 115.45984649658206 57.72 115.52091979980469 58.756 115.58048248291017 59.788 115.62602233886722
		 60.824 115.69728088378906 61.86 115.80413055419923 62.892 115.92309570312501 63.928 116.0319747924805
		 64.964 116.09532928466794 65.996 116.06971740722658 67.032 115.96624755859378 68.064 115.88475036621097
		 69.1 115.86888122558592 70.136 115.86226654052736 71.168 115.86680603027345 72.204 115.93925476074222
		 73.24 116.09083557128906 74.272 116.31419372558592 75.308 116.64059448242187;
createNode animCurveTA -n "Bip01_L_UpperArm_rotateZ3";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 73 ".ktv[0:72]"  0.824 -192.49374389648435 1.86 -192.64463806152344
		 2.892 -192.69834899902344 3.928 -192.66856384277344 4.964 -192.62710571289065 5.996 -192.59886169433591
		 7.032 -192.55970764160156 8.064 -192.5037841796875 9.1 -192.44784545898437 10.136 -192.40896606445312
		 11.168 -192.35600280761719 12.204 -192.24885559082031 13.24 -192.12179565429688 14.272 -192.0130615234375
		 15.308 -191.88128662109375 16.34 -191.71458435058591 17.376 -191.55514526367187 18.412 -191.30256652832031
		 19.444 -190.92417907714844 20.48 -190.69967651367187 21.516 -190.82032775878903 22.548 -190.98182678222656
		 23.584 -190.75288391113281 24.616 -190.18243408203125 25.652 -189.46694946289068
		 26.688 -188.61227416992187 27.72 -187.78428649902344 28.756 -187.07772827148435 29.792 -186.25614929199224
		 30.824 -185.31666564941409 31.86 -184.53863525390625 32.892 -184.05841064453125 33.928 -183.82331848144531
		 34.964 -183.6107177734375 35.996 -183.17710876464844 37.032 -182.57879638671875 38.064 -182.00537109375
		 39.1 -181.52786254882812 40.136 -181.18565368652344 41.168 -180.853271484375 42.204 -180.35408020019531
		 43.24 -179.83290100097656 44.272 -179.4835205078125 45.308 -179.14041137695312 46.34 -178.4744873046875
		 47.376 -177.42233276367187 48.412 -176.30015563964844 49.444 -175.34344482421875
		 50.48 -174.39881896972656 51.516 -173.52850341796875 52.548 -173.26541137695315 53.584 -173.6788330078125
		 54.616 -174.13949584960935 55.652 -174.25262451171875 56.688 -174.13401794433594
		 57.72 -173.98258972167969 58.756 -173.87263488769531 59.788 -173.85923767089844 60.824 -173.99723815917969
		 61.86 -174.19204711914062 62.892 -174.2781982421875 63.928 -174.19261169433594 64.964 -173.88906860351562
		 65.996 -173.26229858398438 67.032 -172.38722229003906 68.064 -171.48106384277344
		 69.1 -170.5557861328125 70.136 -169.62852478027344 71.168 -168.9556884765625 72.204 -168.65548706054687
		 73.24 -168.56819152832031 74.272 -168.52958679199222 75.308 -168.50392150878906;
createNode animCurveTA -n "Bip01_L_Forearm_rotateY3";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 73 ".ktv[0:72]"  0.824 -2.0055649280548096 1.86 -1.9806853532791138
		 2.892 -1.963611364364624 3.928 -1.9523509740829468 4.964 -1.9462213516235354 5.996 -1.944247841835022
		 7.032 -1.9430181980133063 8.064 -1.9397646188735964 9.1 -1.9345984458923335 10.136 -1.9282552003860476
		 11.168 -1.9207218885421753 12.204 -1.9114799499511721 13.24 -1.9004757404327393 14.272 -1.8885320425033565
		 15.308 -1.8772231340408327 16.34 -1.8676677942275999 17.376 -1.8574092388153072 18.412 -1.8430048227310181
		 19.444 -1.8296908140182495 20.48 -1.8319770097732544 21.516 -1.8594992160797119 22.548 -1.9097239971160891
		 23.584 -1.974106431007385 24.616 -2.0398619174957275 25.652 -2.0920150279998779 26.688 -2.1248373985290527
		 27.72 -2.1422059535980225 28.756 -2.1485626697540283 29.792 -2.1478762626647949 30.824 -2.1447443962097168
		 31.86 -2.1426656246185303 32.892 -2.1427817344665532 33.928 -2.1445686817169189 34.964 -2.1460928916931152
		 35.996 -2.1454036235809326 37.032 -2.1422615051269531 38.064 -2.1376078128814697
		 39.1 -2.132533073425293 40.136 -2.1282062530517578 41.168 -2.1242465972900391 42.204 -2.1190464496612549
		 43.24 -2.1125841140747075 44.272 -2.1058251857757568 45.308 -2.0987663269042969 46.34 -2.090996265411377
		 47.376 -2.0837147235870361 48.412 -2.0795032978057861 49.444 -2.0797302722930908
		 50.48 -2.0831298828125 51.516 -2.0882329940795894 52.548 -2.0954380035400391 53.584 -2.103642463684082
		 54.616 -2.1095914840698242 55.652 -2.1122047901153564 56.688 -2.1130952835083008
		 57.72 -2.1143507957458496 58.756 -2.1176564693450928 59.788 -2.1225581169128418 60.824 -2.1265265941619873
		 61.86 -2.1280887126922607 62.892 -2.1252908706665039 63.928 -2.1242904663085938 64.964 -2.1250903606414795
		 65.996 -2.1283473968505859 67.032 -2.1431114673614502 68.064 -2.1500146389007568
		 69.1 -2.1557812690734863 70.136 -2.1585679054260254 71.168 -2.1577918529510498 72.204 -2.1534907817840576
		 73.24 -2.14599609375 74.272 -2.1352496147155762 75.308 -2.1202743053436279;
createNode animCurveTA -n "Bip01_L_Forearm_rotateZ3";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 73 ".ktv[0:72]"  0.824 -103.30278778076172 1.86 -104.82211303710936
		 2.892 -105.8179168701172 3.928 -106.45916748046876 4.964 -106.81568908691406 5.996 -106.97212982177736
		 7.032 -107.11794281005859 8.064 -107.35666656494141 9.1 -107.63623809814452 10.136 -107.92010498046876
		 11.168 -108.24494934082033 12.204 -108.68212890625 13.24 -109.25084686279295 14.272 -109.87654876708984
		 15.308 -110.41954040527344 16.34 -110.78944396972656 17.376 -111.1569747924805 18.412 -111.81441497802734
		 19.444 -112.56063079833984 20.48 -112.634033203125 21.516 -111.4683837890625 22.548 -109.01773071289064
		 23.584 -105.41558837890624 24.616 -100.98943328857422 25.652 -96.583045959472656
		 26.688 -93.09586334228517 27.72 -90.842735290527344 28.756 -89.771697998046889 29.792 -89.604629516601563
		 30.824 -89.845008850097685 31.86 -90.137313842773438 32.892 -90.331611633300781 33.928 -90.381851196289063
		 34.964 -90.430381774902315 35.996 -90.665916442871094 37.032 -91.063125610351562
		 38.064 -91.524497985839844 39.1 -91.9765625 40.136 -92.311180114746094 41.168 -92.632415771484375
		 42.204 -93.118957519531236 43.24 -93.670967102050795 44.272 -94.176162719726577 45.308 -94.727165222167969
		 46.34 -95.373023986816406 47.376 -95.971488952636733 48.412 -96.359077453613281 49.444 -96.513946533203125
		 50.48 -96.533538818359375 51.516 -96.422393798828125 52.548 -96.03271484375 53.584 -95.426818847656236
		 54.616 -94.893203735351563 55.652 -94.521171569824219 56.688 -94.224426269531236
		 57.72 -93.895751953125 58.756 -93.346527099609375 59.788 -92.617584228515625 60.824 -92.079078674316406
		 61.86 -91.894309997558594 62.892 -91.939491271972656 63.928 -91.996772766113281 64.964 -92.067779541015625
		 65.996 -92.030776977539063 67.032 -91.656410217285156 68.064 -91.094062805175781
		 69.1 -90.8372802734375 70.136 -90.968605041503906 71.168 -91.227722167968764 72.204 -91.478721618652372
		 73.24 -91.782043457031236 74.272 -92.350822448730469 75.308 -93.424331665039063;
createNode animCurveTA -n "Bip01_L_Forearm_rotateX3";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 73 ".ktv[0:72]"  0.824 2.9611368179321289 1.86 3.0134334564208984
		 2.892 3.047313928604126 3.928 3.0689737796783447 4.964 3.0810542106628418 5.996 3.0866503715515137
		 7.032 3.0921463966369629 8.064 3.1005861759185791 9.1 3.1099064350128174 10.136 3.1188766956329346
		 11.168 3.1289875507354736 12.204 3.1429414749145508 13.24 3.1615502834320068 14.272 3.182063102722168
		 15.308 3.1993474960327148 16.34 3.210007905960083 17.376 3.2201054096221924 18.412 3.2404146194458008
		 19.444 3.265328168869019 20.48 3.2699663639068608 21.516 3.2347941398620605 22.548 3.1560521125793457
		 23.584 3.0349624156951904 24.616 2.8799669742584229 25.652 2.7207787036895752 26.688 2.5923163890838623
		 27.72 2.5083980560302734 28.756 2.46822190284729 29.792 2.4617736339569092 30.824 2.4706184864044194
		 31.86 2.4815223217010498 32.892 2.4889569282531734 33.928 2.4910645484924316 34.964 2.4930713176727295
		 35.996 2.5019862651824951 37.032 2.516808032989502 38.064 2.5338833332061768 39.1 2.5505146980285645
		 40.136 2.5627200603485107 41.168 2.5744028091430664 42.204 2.5922117233276367 43.24 2.6122593879699707
		 44.272 2.630373477935791 45.308 2.6501259803771973 46.34 2.6733369827270508 47.376 2.6947875022888184
		 48.412 2.7088065147399902 49.444 2.7149505615234375 50.48 2.716651439666748 51.516 2.7136106491088867
		 52.548 2.7001090049743652 53.584 2.6783316135406494 54.616 2.6588742733001709 55.652 2.645000696182251
		 56.688 2.6337318420410156 57.72 2.6212997436523437 58.756 2.6007368564605713 59.788 2.5735580921173096
		 60.824 2.553539514541626 61.86 2.5466923713684082 62.892 2.5479474067687988 63.928 2.5499589443206787
		 64.964 2.5528173446655273 65.996 2.5519409179687504 67.032 2.5394036769866943 68.064 2.5185527801513672
		 69.1 2.5090560913085937 70.136 2.5140910148620605 71.168 2.5238733291625977 72.204 2.5332217216491699
		 73.24 2.544306755065918 74.272 2.5649957656860352 75.308 2.604012250900269;
createNode animCurveTA -n "Bip01_L_Thigh_rotateX3";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 73 ".ktv[0:72]"  0.824 -7.6640453338623047 1.86 -7.773219108581543
		 2.892 -7.9238386154174796 3.928 -8.083582878112793 4.964 -8.2080564498901367 5.996 -8.278325080871582
		 7.032 -8.3472833633422852 8.064 -8.4558553695678711 9.1 -8.5958747863769531 10.136 -8.7116050720214844
		 11.168 -8.7950429916381836 12.204 -8.8483457565307617 13.24 -8.8720874786376953 14.272 -8.9115657806396484
		 15.308 -9.0297012329101562 16.34 -9.1605129241943359 17.376 -9.3052577972412109 18.412 -9.4400863647460937
		 19.444 -9.5396156311035156 20.48 -9.5974531173706072 21.516 -9.6728315353393555 22.548 -9.7800655364990234
		 23.584 -9.8685569763183594 24.616 -9.936039924621582 25.652 -9.9852714538574219 26.688 -10.038847923278809
		 27.72 -10.107559204101562 28.756 -10.181516647338867 29.792 -10.239163398742676 30.824 -10.276401519775392
		 31.86 -10.30547046661377 32.892 -10.364101409912108 33.928 -10.459204673767092 34.964 -10.526686668395996
		 35.996 -10.55179500579834 37.032 -10.572066307067873 38.064 -10.586164474487305 39.1 -10.607802391052246
		 40.136 -10.623292922973633 41.168 -10.649029731750488 42.204 -10.713369369506836
		 43.24 -10.802736282348633 44.272 -10.883304595947266 45.308 -10.967561721801758 46.34 -11.083347320556641
		 47.376 -11.210989952087402 48.412 -11.333660125732422 49.444 -11.45671272277832 50.48 -11.587552070617676
		 51.516 -11.694239616394045 52.548 -11.78087329864502 53.584 -11.861606597900392 54.616 -11.92703914642334
		 55.652 -11.966791152954102 56.688 -11.999494552612305 57.72 -12.02443790435791 58.756 -12.044546127319336
		 59.788 -12.064217567443848 60.824 -12.087603569030762 61.86 -12.101592063903809 62.892 -12.066840171813965
		 63.928 -12.003510475158691 64.964 -11.949579238891602 65.996 -11.919881820678713
		 67.032 -11.849421501159668 68.064 -11.720303535461426 69.1 -11.582361221313477 70.136 -11.502256393432615
		 71.168 -11.475427627563477 72.204 -11.413655281066896 73.24 -11.318852424621582 74.272 -11.185144424438478
		 75.308 -11.041874885559082;
createNode animCurveTA -n "Bip01_L_Thigh_rotateY3";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 73 ".ktv[0:72]"  0.824 -173.18515014648438 1.86 -173.21900939941406
		 2.892 -173.28050231933594 3.928 -173.36640930175781 4.964 -173.41651916503906 5.996 -173.433837890625
		 7.032 -173.44416809082031 8.064 -173.48826599121094 9.1 -173.55732727050781 10.136 -173.60868835449219
		 11.168 -173.63279724121094 12.204 -173.62246704101562 13.24 -173.58404541015625 14.272 -173.56832885742187
		 15.308 -173.58981323242187 16.34 -173.61993408203125 17.376 -173.6568603515625 18.412 -173.68875122070312
		 19.444 -173.67604064941409 20.48 -173.62445068359375 21.516 -173.5887451171875 22.548 -173.56849670410156
		 23.584 -173.53291320800781 24.616 -173.46005249023435 25.652 -173.39846801757813
		 26.688 -173.31417846679687 27.72 -173.25933837890625 28.756 -173.20509338378906 29.792 -173.13006591796875
		 30.824 -173.04885864257812 31.86 -172.942626953125 32.892 -172.864501953125 33.928 -172.82115173339844
		 34.964 -172.75756835937503 35.996 -172.65696716308594 37.032 -172.559814453125 38.064 -172.46842956542969
		 39.1 -172.37814331054687 40.136 -172.27822875976562 41.168 -172.19625854492187 42.204 -172.13755798339844
		 43.24 -172.10556030273437 44.272 -172.08027648925781 45.308 -172.05238342285156 46.34 -172.04330444335938
		 47.376 -172.04783630371094 48.412 -172.06022644042969 49.444 -172.08583068847656
		 50.48 -172.1243896484375 51.516 -172.1605224609375 52.548 -172.17002868652347 53.584 -172.18618774414062
		 54.616 -172.21525573730469 55.652 -172.23788452148435 56.688 -172.27052307128906
		 57.72 -172.31999206542969 58.756 -172.39649963378906 59.788 -172.49784851074219 60.824 -172.63090515136719
		 61.86 -172.77066040039062 62.892 -172.87442016601562 63.928 -172.9573974609375 64.964 -173.05860900878906
		 65.996 -173.19947814941406 67.032 -173.298095703125 68.064 -173.33596801757812 69.1 -173.36994934082031
		 70.136 -173.44868469238281 71.168 -173.56553649902344 72.204 -173.66984558105469
		 73.24 -173.75918579101562 74.272 -173.82589721679687 75.308 -173.90554809570312;
createNode animCurveTA -n "Bip01_L_Thigh_rotateZ3";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 73 ".ktv[0:72]"  0.824 -95.856887817382813 1.86 -95.873039245605469
		 2.892 -95.835037231445312 3.928 -95.780799865722656 4.964 -95.748100280761733 5.996 -95.762908935546875
		 7.032 -95.775909423828125 8.064 -95.732635498046875 9.1 -95.677665710449219 10.136 -95.7115478515625
		 11.168 -95.775909423828125 12.204 -95.789161682128906 13.24 -95.824623107910156 14.272 -95.909759521484375
		 15.308 -95.949867248535156 16.34 -95.952461242675781 17.376 -95.938934326171889 18.412 -95.956504821777344
		 19.444 -95.972648620605469 20.48 -95.992294311523438 21.516 -96.006362915039063 22.548 -95.974723815917969
		 23.584 -95.934890747070298 24.616 -95.902214050292983 25.652 -95.899398803710937
		 26.688 -95.901229858398438 27.72 -95.878501892089844 28.756 -95.841590881347642 29.792 -95.789794921875
		 30.824 -95.716438293457017 31.86 -95.60693359375 32.892 -95.472709655761719 33.928 -95.298591613769545
		 34.964 -95.129119873046875 35.996 -94.973091125488281 37.032 -94.844299316406236
		 38.064 -94.718650817871094 39.1 -94.579483032226563 40.136 -94.446540832519531 41.168 -94.266059875488295
		 42.204 -94.076095581054687 43.24 -93.894195556640625 44.272 -93.747650146484375 45.308 -93.581779479980483
		 46.34 -93.341606140136719 47.376 -93.076019287109375 48.412 -92.8699951171875 49.444 -92.692680358886733
		 50.48 -92.525695800781236 51.516 -92.371063232421875 52.548 -92.194381713867187 53.584 -92.010536193847642
		 54.616 -91.889892578125 55.652 -91.855194091796875 56.688 -91.846611022949219 57.72 -91.922676086425781
		 58.756 -92.108047485351563 59.788 -92.429992675781236 60.824 -92.859123229980469
		 61.86 -93.357833862304688 62.892 -93.871353149414063 63.928 -94.376625061035156 64.964 -94.917823791503906
		 65.996 -95.491394042968764 67.032 -96.021636962890625 68.064 -96.462677001953139
		 69.1 -96.806312561035156 70.136 -97.055313110351563 71.168 -97.231094360351563 72.204 -97.411521911621094
		 73.24 -97.624732971191406 74.272 -97.865646362304688 75.308 -98.115226745605469;
createNode animCurveTA -n "Bip01_L_Calf_rotateX3";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 73 ".ktv[0:72]"  0.824 0.93477779626846313 1.86 0.93481314182281494
		 2.892 0.93478924036026001 3.928 0.93481957912445068 4.964 0.93485617637634277 5.996 0.93487399816513073
		 7.032 0.93490982055664096 8.064 0.93499326705932606 9.1 0.93501418828964245 10.136 0.93497794866561856
		 11.168 0.934958815574646 12.204 0.93494379520416271 13.24 0.93495517969131481 14.272 0.93491059541702271
		 15.308 0.9347817897796632 16.34 0.93470621109008789 17.376 0.9346177577972411 18.412 0.93455660343170166
		 19.444 0.93445891141891468 20.48 0.9343023896217344 21.516 0.93415373563766479 22.548 0.93401324748992909
		 23.584 0.93392723798751842 24.616 0.93382811546325684 25.652 0.93370676040649414
		 26.688 0.93356496095657349 27.72 0.93347936868667603 28.756 0.93338155746459972 29.792 0.93330913782119751
		 30.824 0.93325877189636219 31.86 0.93327164649963379 32.892 0.93332201242446899 33.928 0.93342596292495739
		 34.964 0.93353366851806641 35.996 0.93360567092895519 37.032 0.93369382619857788
		 38.064 0.9337688684463501 39.1 0.93382245302200306 40.136 0.93395948410034169 41.168 0.93410569429397583
		 42.204 0.93427348136901844 43.24 0.93440443277358987 44.272 0.93454974889755249 45.308 0.93475896120071411
		 46.34 0.93496608734130859 47.376 0.93515211343765248 48.412 0.93525815010070812 49.444 0.93531876802444458
		 50.48 0.93536555767059326 51.516 0.93539881706237793 52.548 0.93536573648452759 53.584 0.93532049655914307
		 54.616 0.93523728847503695 55.652 0.93514877557754505 56.688 0.93500906229019165
		 57.72 0.93489754199981712 58.756 0.93472701311111428 59.788 0.93455165624618497 60.824 0.93432360887527466
		 61.86 0.93408924341201771 62.892 0.93389809131622314 63.928 0.9336700439453125 64.964 0.93346726894378651
		 65.996 0.93326097726821888 67.032 0.93313556909561168 68.064 0.93314743041992199
		 69.1 0.93337601423263561 70.136 0.9336414337158202 71.168 0.93380486965179443 72.204 0.93392300605773915
		 73.24 0.93403643369674683 74.272 0.93422669172286987 75.308 0.93440490961074829;
createNode animCurveTA -n "Bip01_L_Calf_rotateY3";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 73 ".ktv[0:72]"  0.824 0.022442895919084549 1.86 0.023031849414110184
		 2.892 0.02302614226937294 3.928 0.022245327010750771 4.964 0.021600641310214996 5.996 0.022035591304302216
		 7.032 0.022026941180229187 8.064 0.021355684846639633 9.1 0.020747750997543335 10.136 0.021450787782669067
		 11.168 0.022151056677103043 12.204 0.023156974464654919 13.24 0.024533990770578384
		 14.272 0.026694010943174359 15.308 0.029008828103542328 16.34 0.031320545822381973
		 17.376 0.033635642379522324 18.412 0.036098565906286247 19.444 0.039130024611949921
		 20.48 0.042841300368309021 21.516 0.046496599912643433 22.548 0.049662183970212936
		 23.584 0.052545793354511268 24.616 0.055460415780544281 25.652 0.058260031044483185
		 26.688 0.060576584190130234 27.72 0.062285732477903373 28.756 0.063899993896484375
		 29.792 0.065198957920074463 30.824 0.066135823726654053 31.86 0.066060185432434082
		 32.892 0.065351828932762146 33.928 0.063753426074981689 34.964 0.061857759952545159
		 35.996 0.060424204915761955 37.032 0.059075288474559791 38.064 0.057530380785465247
		 39.1 0.056106451898813255 40.136 0.053918816149234765 41.168 0.051125615835189826
		 42.204 0.047793187201023109 43.24 0.044164352118968957 44.272 0.040322478860616691
		 45.308 0.03554428368806839 46.34 0.02943641692399979 47.376 0.022837566211819649
		 48.412 0.016607051715254784 49.444 0.010480816476047041 50.48 0.0041899126954376698
		 51.516 -0.0014191940426826477 52.548 -0.0062411967664957047 53.584 -0.011267124675214292
		 54.616 -0.016552913933992386 55.652 -0.021657025441527367 56.688 -0.026686849072575569
		 57.72 -0.031637772917747498 58.756 -0.036268498748540878 59.788 -0.040561482310295105
		 60.824 -0.044410768896341331 61.86 -0.048361573368310928 62.892 -0.052978128194808967
		 63.928 -0.05734290927648545 64.964 -0.060716044157743454 65.996 -0.063502036035060883
		 67.032 -0.065389834344387054 68.064 -0.06570008397102356 69.1 -0.062930241227149963
		 70.136 -0.05896754190325737 71.168 -0.055938165634870529 72.204 -0.054110206663608551
		 73.24 -0.051939506083726883 74.272 -0.048872474581003189 75.308 -0.045337792485952377;
createNode animCurveTA -n "Bip01_L_Calf_rotateZ3";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 73 ".ktv[0:72]"  0.824 -89.087646484375 1.86 -89.153404235839844
		 2.892 -89.143424987792983 3.928 -89.110755920410156 4.964 -89.090652465820327 5.996 -89.135139465332031
		 7.032 -89.172866821289063 8.064 -89.188888549804687 9.1 -89.167045593261719 10.136 -89.186332702636719
		 11.168 -89.222885131835938 12.204 -89.2813720703125 13.24 -89.391708374023438 14.272 -89.516891479492188
		 15.308 -89.585319519042983 16.34 -89.69952392578125 17.376 -89.806098937988281 18.412 -89.963623046875
		 19.444 -90.132942199707031 20.48 -90.329910278320327 21.516 -90.526382446289063 22.548 -90.703605651855469
		 23.584 -90.905433654785156 24.616 -91.102180480957017 25.652 -91.276580810546875
		 26.688 -91.399971008300781 27.72 -91.496871948242188 28.756 -91.587860107421875 29.792 -91.6668701171875
		 30.824 -91.724632263183594 31.86 -91.723579406738281 32.892 -91.699546813964858 33.928 -91.609519958496108
		 34.964 -91.510833740234375 35.996 -91.417053222656236 37.032 -91.354881286621094
		 38.064 -91.263725280761719 39.1 -91.163475036621108 40.136 -91.068397521972656 41.168 -90.924903869628906
		 42.204 -90.752113342285156 43.24 -90.537696838378906 44.272 -90.321525573730469 45.308 -90.086242675781236
		 46.34 -89.776466369628906 47.376 -89.432098388671875 48.412 -89.091239929199219 49.444 -88.745948791503906
		 50.48 -88.4071044921875 51.516 -88.123161315917969 52.548 -87.846633911132813 53.584 -87.569740295410156
		 54.616 -87.28250885009767 55.652 -87.000526428222656 56.688 -86.708335876464844 57.72 -86.459190368652358
		 58.756 -86.191291809082031 59.788 -85.94073486328125 60.824 -85.670104980468764 61.86 -85.408584594726563
		 62.892 -85.175392150878906 63.928 -84.944442749023437 64.964 -84.751419067382827
		 65.996 -84.560111999511733 67.032 -84.450141906738281 68.064 -84.452476501464844
		 69.1 -84.653221130371094 70.136 -84.903823852539077 71.168 -85.072883605957017 72.204 -85.187774658203125
		 73.24 -85.316764831542983 74.272 -85.513236999511719 75.308 -85.722969055175781;
createNode animCurveTA -n "Bip01_L_Foot_rotateX3";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 73 ".ktv[0:72]"  0.824 -10.56098461151123 1.86 -10.475405693054199
		 2.892 -10.655247688293455 3.928 -10.93208694458008 4.964 -11.033595085144045 5.996 -10.929497718811035
		 7.032 -10.803649902343752 8.064 -10.819746971130373 9.1 -10.938218116760254 10.136 -11.166906356811523
		 11.168 -11.241664886474608 12.204 -11.13872241973877 13.24 -10.923622131347656 14.272 -10.87347412109375
		 15.308 -11.200814247131348 16.34 -11.560938835144045 17.376 -11.939990043640137 18.412 -12.290285110473633
		 19.444 -12.490653991699221 20.48 -12.548955917358398 21.516 -12.717900276184084 22.548 -12.834604263305664
		 23.584 -12.752695083618164 24.616 -12.603837013244627 25.652 -12.55152416229248 26.688 -12.491814613342283
		 27.72 -12.567302703857422 28.756 -12.665685653686523 29.792 -12.66150951385498 30.824 -12.570720672607422
		 31.86 -12.411186218261721 32.892 -12.390072822570801 33.928 -12.611011505126951 34.964 -12.763017654418944
		 35.996 -12.693596839904783 37.032 -12.618044853210447 38.064 -12.62547779083252 39.1 -12.590048789978027
		 40.136 -12.342695236206056 41.168 -12.087130546569824 42.204 -12.069516181945801
		 43.24 -12.16973876953125 44.272 -12.095302581787108 45.308 -11.860355377197266 46.34 -11.653718948364258
		 47.376 -11.625006675720217 48.412 -11.629852294921877 49.444 -11.533487319946287
		 50.48 -11.489345550537108 51.516 -11.405231475830078 52.548 -11.322978019714355 53.584 -11.247418403625488
		 54.616 -11.208137512207031 55.652 -11.081518173217772 56.688 -11.001343727111816
		 57.72 -10.87721061706543 58.756 -10.701349258422852 59.788 -10.643471717834473 60.824 -10.936211585998535
		 61.86 -11.240517616271973 62.892 -11.179173469543455 63.928 -11.035623550415041 64.964 -11.045380592346191
		 65.996 -11.260907173156738 67.032 -11.315515518188477 68.064 -11.071750640869141
		 69.1 -10.845611572265623 70.136 -10.93294620513916 71.168 -11.246537208557129 72.204 -11.479169845581056
		 73.24 -11.522919654846191 74.272 -11.457485198974608 75.308 -11.424405097961426;
createNode animCurveTA -n "Bip01_L_Foot_rotateY3";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 73 ".ktv[0:72]"  0.824 1.0489388704299929 1.86 0.99012756347656261
		 2.892 1.0823287963867187 3.928 1.2663697004318235 4.964 1.312442421913147 5.996 1.2422268390655518
		 7.032 1.1532596349716189 8.064 1.174491286277771 9.1 1.2680631875991819 10.136 1.3678264617919922
		 11.168 1.4011017084121704 12.204 1.3340984582901001 13.24 1.1823617219924929 14.272 1.1223964691162107
		 15.308 1.3185673952102659 16.34 1.558146595954895 17.376 1.8285096883773797 18.412 2.0090420246124268
		 19.444 2.1379742622375488 20.48 2.1957597732543945 21.516 2.2921175956726074 22.548 2.3902606964111328
		 23.584 2.3081943988800049 24.616 2.2376649379730225 25.652 2.1955363750457764 26.688 2.1869809627532959
		 27.72 2.1986792087554932 28.756 2.2585041522979736 29.792 2.2720654010772705 30.824 2.1934175491333008
		 31.86 2.0817742347717285 32.892 2.0861098766326904 33.928 2.2399599552154541 34.964 2.3351197242736816
		 35.996 2.2876977920532227 37.032 2.2345895767211914 38.064 2.2325534820556641 39.1 2.2015979290008545
		 40.136 2.0172069072723389 41.168 1.8595412969589233 42.204 1.857205867767334 43.24 1.9101186990737915
		 44.272 1.8264110088348389 45.308 1.6110378503799438 46.34 1.558756947517395 47.376 1.5034784078598022
		 48.412 1.4612494707107544 49.444 1.4033997058868408 50.48 1.3609271049499512 51.516 1.3008668422698977
		 52.548 1.2218629121780396 53.584 1.1371467113494873 54.616 1.1003807783126831 55.652 1.0174858570098877
		 56.688 0.95681673288345359 57.72 0.87580883502960205 58.756 0.75086444616317749 59.788 0.71102172136306763
		 60.824 0.84024465084075939 61.86 1.0111974477767944 62.892 0.97482043504714977 63.928 0.85922366380691517
		 64.964 0.86706697940826416 65.996 0.96646672487258911 67.032 1.0207943916320801 68.064 0.87654805183410645
		 69.1 0.73651450872421265 70.136 0.84337460994720459 71.168 1.0502763986587529 72.204 1.2006486654281616
		 73.24 1.2392044067382813 74.272 1.2348586320877075 75.308 1.2355853319168093;
createNode animCurveTA -n "Bip01_L_Foot_rotateZ3";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 73 ".ktv[0:72]"  0.824 3.730555295944213 1.86 3.7577428817749023
		 2.892 3.8642458915710449 3.928 4.0425810813903809 4.964 4.2243480682373047 5.996 4.3179616928100586
		 7.032 4.5048093795776367 8.064 4.9489483833312988 9.1 5.2413849830627441 10.136 5.0628237724304199
		 11.168 4.8642377853393555 12.204 4.9966907501220703 13.24 5.1125888824462891 14.272 4.8922538757324219
		 15.308 4.7085542678833008 16.34 4.84710693359375 17.376 5.101442813873291 18.412 5.3366875648498535
		 19.444 5.5592436790466309 20.48 5.7182021141052246 21.516 5.8138418197631836 22.548 5.9968562126159668
		 23.584 6.3071203231811523 24.616 6.6235237121582031 25.652 6.7849140167236328 26.688 6.8446254730224618
		 27.72 6.9427337646484375 28.756 7.0972251892089844 29.792 7.1899375915527344 30.824 7.1856045722961417
		 31.86 7.1811590194702157 32.892 7.2700471878051758 33.928 7.4335780143737811 34.964 7.4974374771118164
		 35.996 7.4310531616210937 37.032 7.352774143218995 38.064 7.270823001861574 39.1 7.1868348121643066
		 40.136 7.1217188835144043 41.168 7.0928573608398438 42.204 7.0967411994934082 43.24 6.9657521247863778
		 44.272 6.6431355476379395 45.308 6.3933901786804199 46.34 6.3437652587890625 47.376 6.3063254356384277
		 48.412 6.1212091445922852 49.444 5.8894715309143066 50.48 5.7292346954345703 51.516 5.5623173713684082
		 52.548 5.3386898040771484 53.584 5.1311259269714355 54.616 4.8641519546508789 55.652 4.5822033882141122
		 56.688 4.3742537498474121 57.72 4.201906681060791 58.756 3.9421424865722656 59.788 3.4911859035491943
		 60.824 2.938704252243042 61.86 2.6146152019500732 62.892 2.5785179138183594 63.928 2.5158131122589111
		 64.964 2.3391647338867187 65.996 2.2239105701446533 67.032 2.1808211803436279 68.064 2.2124254703521729
		 69.1 2.5356831550598145 70.136 3.0864989757537842 71.168 3.5371906757354736 72.204 3.7942287921905522
		 73.24 4.034663200378418 74.272 4.429527759552002 75.308 4.8190417289733887;
createNode animCurveTA -n "Bip01_R_Thigh_rotateX3";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 73 ".ktv[0:72]"  0.824 8.203125 1.86 8.0585298538208008 2.892 7.9243712425231934
		 3.928 7.7762107849121076 4.964 7.638009071350095 5.996 7.5344223976135263 7.032 7.4282946586608851
		 8.064 7.2780714035034189 9.1 7.1211004257202148 10.136 6.9858212471008301 11.168 6.8845434188842773
		 12.204 6.8179354667663574 13.24 6.7674741744995117 14.272 6.7011528015136719 15.308 6.6219635009765625
		 16.34 6.5333895683288574 17.376 6.4219484329223633 18.412 6.3060674667358398 19.444 6.2317414283752441
		 20.48 6.2312221527099609 21.516 6.2099528312683105 22.548 6.1256685256958008 23.584 6.0476951599121094
		 24.616 6.0032167434692392 25.652 5.9637112617492676 26.688 5.926445484161377 27.72 5.8979635238647461
		 28.756 5.8604249954223633 29.792 5.8053736686706543 30.824 5.7509918212890625 31.86 5.7015137672424316
		 32.892 5.6744661331176758 33.928 5.6595215797424316 34.964 5.6257667541503906 35.996 5.5891981124877939
		 37.032 5.5382857322692871 38.064 5.4984383583068848 39.1 5.4487419128417969 40.136 5.4013404846191406
		 41.168 5.370877742767334 42.204 5.3427510261535645 43.24 5.2824134826660156 44.272 5.1638822555541992
		 45.308 4.9864749908447266 46.34 4.8089447021484375 47.376 4.6796355247497559 48.412 4.5709128379821777
		 49.444 4.436767578125 50.48 4.2969536781311035 51.516 4.1688332557678223 52.548 4.0507664680480957
		 53.584 3.9364426136016846 54.616 3.8297960758209224 55.652 3.7379574775695801 56.688 3.6672785282135014
		 57.72 3.5799751281738281 58.756 3.4550237655639653 59.788 3.3204689025878902 60.824 3.2235348224639893
		 61.86 3.1427884101867676 62.892 3.073544025421143 63.928 3.0383274555206299 64.964 3.0138618946075439
		 65.996 2.9912264347076416 67.032 2.9997506141662607 68.064 3.0246672630310059 69.1 3.0535588264465332
		 70.136 3.088543176651001 71.168 3.1206450462341309 72.204 3.1699178218841553 73.24 3.2333157062530522
		 74.272 3.3471748828887939 75.308 3.5042009353637695;
createNode animCurveTA -n "Bip01_R_Thigh_rotateY3";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 73 ".ktv[0:72]"  0.824 170.77993774414062 1.86 170.80242919921875
		 2.892 170.81533813476562 3.928 170.81173706054687 4.964 170.82026672363281 5.996 170.84820556640625
		 7.032 170.86730957031253 8.064 170.84648132324222 9.1 170.80130004882812 10.136 170.77008056640625
		 11.168 170.76539611816406 12.204 170.783203125 13.24 170.8111572265625 14.272 170.81948852539062
		 15.308 170.82466125488281 16.34 170.81137084960935 17.376 170.77084350585935 18.412 170.7398681640625
		 19.444 170.75697326660156 20.48 170.82302856445315 21.516 170.85884094238281 22.548 170.85453796386719
		 23.584 170.85096740722656 24.616 170.89343261718753 25.652 170.94905090332031 26.688 171.00741577148435
		 27.72 171.07464599609378 28.756 171.13583374023435 29.792 171.19796752929687 30.824 171.25239562988281
		 31.86 171.31248474121094 32.892 171.38859558105469 33.928 171.46986389160156 34.964 171.5545654296875
		 35.996 171.64019775390628 37.032 171.72357177734375 38.064 171.81460571289062 39.1 171.90669250488281
		 40.136 171.99729919433594 41.168 172.09898376464844 42.204 172.21168518066409 43.24 172.30111694335935
		 44.272 172.33924865722656 45.308 172.3385925292969 46.34 172.337158203125 47.376 172.37150573730469
		 48.412 172.4130859375 49.444 172.44306945800781 50.48 172.46633911132812 51.516 172.48751831054687
		 52.548 172.51068115234375 53.584 172.52825927734375 54.616 172.54598999023435 55.652 172.57981872558594
		 56.688 172.62547302246094 57.72 172.645751953125 58.756 172.63693237304687 59.788 172.61923217773435
		 60.824 172.618896484375 61.86 172.62176513671875 62.892 172.63198852539062 63.928 172.64581298828125
		 64.964 172.65156555175781 65.996 172.63990783691406 67.032 172.62632751464844 68.064 172.60321044921875
		 69.1 172.5562744140625 70.136 172.4880065917969 71.168 172.39933776855469 72.204 172.29728698730469
		 73.24 172.20741271972656 74.272 172.14291381835937 75.308 172.10122680664062;
createNode animCurveTA -n "Bip01_R_Thigh_rotateZ3";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 73 ".ktv[0:72]"  0.824 -95.660484313964858 1.86 -95.739387512207017
		 2.892 -95.783889770507813 3.928 -95.828887939453125 4.964 -95.891670227050781 5.996 -95.950714111328125
		 7.032 -96.036415100097685 8.064 -96.150398254394531 9.1 -96.246925354003906 10.136 -96.328582763671875
		 11.168 -96.417594909667969 12.204 -96.499092102050781 13.24 -96.574279785156236 14.272 -96.681625366210937
		 15.308 -96.782844543457017 16.34 -96.866744995117188 17.376 -96.908279418945313 18.412 -97.018051147460938
		 19.444 -97.174407958984375 20.48 -97.271804809570327 21.516 -97.268798828125 22.548 -97.259017944335952
		 23.584 -97.271202087402315 24.616 -97.310768127441406 25.652 -97.360382080078125
		 26.688 -97.391525268554687 27.72 -97.378601074218764 28.756 -97.357917785644531 29.792 -97.345985412597685
		 30.824 -97.294731140136719 31.86 -97.210197448730469 32.892 -97.079170227050781 33.928 -96.910240173339844
		 34.964 -96.752273559570327 35.996 -96.625312805175781 37.032 -96.504013061523438
		 38.064 -96.387763977050781 39.1 -96.266265869140625 40.136 -96.116409301757813 41.168 -95.947624206542983
		 42.204 -95.760528564453125 43.24 -95.579177856445313 44.272 -95.43896484375 45.308 -95.354187011718764
		 46.34 -95.249908447265625 47.376 -95.097785949707017 48.412 -94.941123962402315 49.444 -94.825889587402315
		 50.48 -94.721580505371094 51.516 -94.606613159179673 52.548 -94.484893798828125 53.584 -94.366569519042983
		 54.616 -94.286354064941406 55.652 -94.288902282714844 56.688 -94.344360351562514
		 57.72 -94.495910644531236 58.756 -94.759986877441406 59.788 -95.135650634765625 60.824 -95.587043762207031
		 61.86 -96.121795654296875 62.892 -96.695938110351563 63.928 -97.269020080566406 64.964 -97.828193664550781
		 65.996 -98.394195556640625 67.032 -98.912879943847642 68.064 -99.332443237304688
		 69.1 -99.686958312988281 70.136 -99.972076416015625 71.168 -100.17586517333984 72.204 -100.33908081054687
		 73.24 -100.53543090820312 74.272 -100.73916625976562 75.308 -100.9341278076172;
createNode animCurveTA -n "Bip01_R_Calf_rotateX3";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 73 ".ktv[0:72]"  0.824 -0.93488615751266468 1.86 -0.93487399816513073
		 2.892 -0.93486982583999634 3.928 -0.93486595153808594 4.964 -0.93485265970230103
		 5.996 -0.93481552600860596 7.032 -0.93479651212692261 8.064 -0.93478071689605724
		 9.1 -0.93474483489990223 10.136 -0.93471604585647572 11.168 -0.93467015027999878
		 12.204 -0.93463724851608276 13.24 -0.93459457159042347 14.272 -0.93448954820632935
		 15.308 -0.93436437845230091 16.34 -0.93433290719985984 17.376 -0.93423587083816539
		 18.412 -0.9341123104095459 19.444 -0.93389075994491588 20.48 -0.93368434906005882
		 21.516 -0.93359929323196411 22.548 -0.9335361123085022 23.584 -0.93339568376541138
		 24.616 -0.93323177099227883 25.652 -0.93307369947433461 26.688 -0.93297016620635986
		 27.72 -0.93292975425720215 28.756 -0.93292653560638439 29.792 -0.93288332223892212
		 30.824 -0.93289470672607411 31.86 -0.93294960260391246 32.892 -0.93307232856750522
		 33.928 -0.93324553966522217 34.964 -0.93341821432113647 35.996 -0.93353784084320079
		 37.032 -0.933646559715271 38.064 -0.93377047777175903 39.1 -0.93390482664108265 40.136 -0.93403023481369007
		 41.168 -0.93415486812591553 42.204 -0.93429982662200928 43.24 -0.93443834781646751
		 44.272 -0.93454921245574951 45.308 -0.93465560674667358 46.34 -0.93470716476440419
		 47.376 -0.93473374843597401 48.412 -0.9347681999206543 49.444 -0.93477022647857666
		 50.48 -0.93474316596984852 51.516 -0.93467974662780784 52.548 -0.9345933198928833
		 53.584 -0.93450427055358865 54.616 -0.93440920114517212 55.652 -0.93428826332092274
		 56.688 -0.9341312050819397 57.72 -0.93399864435195901 58.756 -0.93383741378784169
		 59.788 -0.93371981382370006 60.824 -0.93358325958251931 61.86 -0.93346238136291504
		 62.892 -0.93332767486572277 63.928 -0.93319386243820213 64.964 -0.93308126926422108
		 65.996 -0.93302428722381614 67.032 -0.93301928043365467 68.064 -0.93311583995819103
		 69.1 -0.93330103158950795 70.136 -0.93351978063583396 71.168 -0.93369597196578991
		 72.204 -0.93378233909606934 73.24 -0.93387293815612804 74.272 -0.93396931886673007
		 75.308 -0.93406397104263328;
createNode animCurveTA -n "Bip01_R_Calf_rotateY3";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 73 ".ktv[0:72]"  0.824 -0.0049698185175657272 1.86 -0.0066738300956785679
		 2.892 -0.0079083861783146858 3.928 -0.0089938249439001083 4.964 -0.010256894864141939
		 5.996 -0.011624532751739023 7.032 -0.013174936175346376 8.064 -0.01504921168088913
		 9.1 -0.017033817246556282 10.136 -0.018753807991743088 11.168 -0.020796975120902061
		 12.204 -0.022796599194407463 13.24 -0.024811586365103722 14.272 -0.02744658850133419
		 15.308 -0.030750352889299393 16.34 -0.03373304009437561 17.376 -0.036211583763360977
		 18.412 -0.039503175765275955 19.444 -0.043894018977880478 20.48 -0.047588985413312912
		 21.516 -0.050034523010253906 22.548 -0.052520398050546646 23.584 -0.055387064814567566
		 24.616 -0.05813637375831604 25.652 -0.060495283454656601 26.688 -0.062067586928606033
		 27.72 -0.062750130891799927 28.756 -0.063156269490718842 29.792 -0.064136050641536727
		 30.824 -0.064026087522506714 31.86 -0.063257455825805664 32.892 -0.061563327908515937
		 33.928 -0.0589405857026577 34.964 -0.05600032955408097 35.996 -0.053400952368974686
		 37.032 -0.050953168421983719 38.064 -0.048295740038156509 39.1 -0.045526031404733658
		 40.136 -0.042416825890541077 41.168 -0.038685820996761322 42.204 -0.034461278468370438
		 43.24 -0.029991514980792999 44.272 -0.025512363761663437 45.308 -0.021005140617489815
		 46.34 -0.016022233292460442 47.376 -0.01040254533290863 48.412 -0.0046246782876551151
		 49.444 0.00098310341127216816 50.48 0.0065004667267203331 51.516 0.011668811552226543
		 52.548 0.016018610447645191 53.584 0.020079202950000763 54.616 0.024446317926049232
		 55.652 0.028642732650041577 56.688 0.032381895929574966 57.72 0.035890959203243256
		 58.756 0.038886371999979019 59.788 0.041070867329835885 60.824 0.042891349643468857
		 61.86 0.044997505843639374 62.892 0.04740583524107933 63.928 0.049439314752817154
		 64.964 0.051050201058387763 65.996 0.052061058580875397 67.032 0.052445955574512482
		 68.064 0.051412824541330338 69.1 0.047639630734920502 70.136 0.042620085179805763
		 71.168 0.038889259099960327 72.204 0.03653746098279953 73.24 0.034234285354614258
		 74.272 0.03144361823797226 75.308 0.028159301728010178;
createNode animCurveTA -n "Bip01_R_Calf_rotateZ3";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 73 ".ktv[0:72]"  0.824 -87.982734680175781 1.86 -88.075035095214844
		 2.892 -88.143524169921875 3.928 -88.206550598144531 4.964 -88.265754699707017 5.996 -88.3289794921875
		 7.032 -88.40667724609375 8.064 -88.520126342773438 9.1 -88.618659973144531 10.136 -88.715171813964844
		 11.168 -88.82523345947267 12.204 -88.936195373535156 13.24 -89.03656005859375 14.272 -89.157623291015625
		 15.308 -89.304115295410156 16.34 -89.512306213378906 17.376 -89.64642333984375 18.412 -89.814140319824219
		 19.444 -90.005821228027344 20.48 -90.168952941894531 21.516 -90.325027465820327 22.548 -90.505409240722656
		 23.584 -90.667297363281236 24.616 -90.806632995605469 25.652 -90.912818908691406
		 26.688 -90.992195129394531 27.72 -91.031822204589844 28.756 -91.0638427734375 29.792 -91.130302429199219
		 30.824 -91.132553100585937 31.86 -91.095779418945313 32.892 -91.024795532226562 33.928 -90.900344848632827
		 34.964 -90.749214172363281 35.996 -90.602096557617188 37.032 -90.454948425292983
		 38.064 -90.308502197265625 39.1 -90.175064086914062 40.136 -89.997230529785156 41.168 -89.789871215820327
		 42.204 -89.548736572265625 43.24 -89.304161071777344 44.272 -89.060699462890625 45.308 -88.81695556640625
		 46.34 -88.514656066894531 47.376 -88.181137084960937 48.412 -87.858001708984375 49.444 -87.549896240234375
		 50.48 -87.243377685546875 51.516 -86.949386596679673 52.548 -86.688888549804688 53.584 -86.45440673828125
		 54.616 -86.210525512695313 55.652 -85.971260070800781 56.688 -85.730033874511719
		 57.72 -85.510231018066406 58.756 -85.31170654296875 59.788 -85.158203125 60.824 -85.006919860839844
		 61.86 -84.871185302734375 62.892 -84.705696105957017 63.928 -84.561943054199219 64.964 -84.439521789550781
		 65.996 -84.378982543945327 67.032 -84.378089904785156 68.064 -84.462890625 69.1 -84.688453674316406
		 70.136 -84.959220886230469 71.168 -85.183586120605483 72.204 -85.318191528320327
		 73.24 -85.455551147460938 74.272 -85.608413696289063 75.308 -85.794158935546875;
createNode animCurveTA -n "Bip01_R_Foot_rotateX3";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 73 ".ktv[0:72]"  0.824 9.1096010208129883 1.86 9.3944730758666992
		 2.892 9.5292139053344727 3.928 9.5101528167724609 4.964 9.5060510635375977 5.996 9.6737327575683594
		 7.032 9.7704105377197266 8.064 9.5955591201782227 9.1 9.3619890213012695 10.136 9.2245016098022461
		 11.168 9.1680517196655273 12.204 9.2032136917114258 13.24 9.2954845428466797 14.272 9.4039430618286133
		 15.308 9.4668426513671875 16.34 9.4143638610839844 17.376 9.2615203857421875 18.412 9.1788339614868182
		 19.444 9.3124704360961914 20.48 9.6050071716308594 21.516 9.7205905914306641 22.548 9.5809173583984393
		 23.584 9.5145711898803711 24.616 9.6250019073486328 25.652 9.8609943389892578 26.688 10.031801223754885
		 27.72 10.135614395141602 28.756 10.096875190734863 29.792 9.9556961059570312 30.824 9.8194751739501971
		 31.86 9.7691020965576172 32.892 9.7383832931518555 33.928 9.714360237121582 34.964 9.6435747146606445
		 35.996 9.5164661407470703 37.032 9.4389343261718768 38.064 9.3110990524291992 39.1 9.2336854934692383
		 40.136 9.1981687545776367 41.168 9.3258085250854492 42.204 9.484858512878418 43.24 9.5668811798095703
		 44.272 9.4175901412963867 45.308 9.0923786163330078 46.34 8.7426033020019531 47.376 8.6233510971069354
		 48.412 8.5889682769775391 49.444 8.4804086685180664 50.48 8.4353694915771484 51.516 8.3821697235107422
		 52.548 8.3205766677856445 53.584 8.2263650894165039 54.616 8.1054725646972656 55.652 8.1075553894042969
		 56.688 8.1643295288085938 57.72 8.2203912734985352 58.756 8.1261987686157227 59.788 8.0088033676147461
		 60.824 8.028656005859375 61.86 8.0314264297485352 62.892 7.9672751426696786 63.928 8.050267219543457
		 64.964 8.1564970016479492 65.996 8.1987123489379883 67.032 8.1761388778686523 68.064 8.1196002960205096
		 69.1 8.0776557922363299 70.136 8.0630607604980469 71.168 8.0274581909179687 72.204 7.847884178161622
		 73.24 7.7613143920898429 74.272 7.9023399353027344 75.308 8.28057861328125;
createNode animCurveTA -n "Bip01_R_Foot_rotateY3";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 73 ".ktv[0:72]"  0.824 -0.01225854828953743 1.86 -0.1877090185880661
		 2.892 -0.30017241835594177 3.928 -0.30827990174293518 4.964 -0.30967351794242859
		 5.996 -0.42959693074226374 7.032 -0.50396811962127686 8.064 -0.41982164978981018
		 9.1 -0.28142198920249939 10.136 -0.21221888065338135 11.168 -0.19045259058475497
		 12.204 -0.22056236863136292 13.24 -0.27694854140281677 14.272 -0.36275258660316467
		 15.308 -0.38676926493644714 16.34 -0.36630314588546753 17.376 -0.28052878379821777
		 18.412 -0.24390602111816406 19.444 -0.33012634515762335 20.48 -0.49316370487213135
		 21.516 -0.58913218975067139 22.548 -0.53179842233657837 23.584 -0.4925721287727356
		 24.616 -0.61064529418945313 25.652 -0.70900064706802368 26.688 -0.82049143314361572
		 27.72 -0.89081954956054688 28.756 -0.87384176254272461 29.792 -0.79364269971847534
		 30.824 -0.7184409499168396 31.86 -0.69622588157653809 32.892 -0.69348496198654175
		 33.928 -0.72411841154098511 34.964 -0.65394300222396851 35.996 -0.61854302883148193
		 37.032 -0.53651833534240723 38.064 -0.47423639893531794 39.1 -0.40983191132545471
		 40.136 -0.42818477749824529 41.168 -0.52223140001296997 42.204 -0.64690577983856201
		 43.24 -0.69997864961624146 44.272 -0.6353154182434082 45.308 -0.40487697720527649
		 46.34 -0.21433544158935547 47.376 -0.15845417976379397 48.412 -0.15851928293704989
		 49.444 -0.11159071326255798 50.48 -0.11575270444154739 51.516 -0.092220298945903778
		 52.548 -0.06282883882522583 53.584 -0.025330493226647377 54.616 0.0066971150226891041
		 55.652 0.05503261461853981 56.688 -0.030632061883807179 57.72 -0.059685230255126953
		 58.756 -0.011573992669582369 59.788 0.052651070058345795 60.824 0.037985969334840781
		 61.86 0.019780240952968597 62.892 0.080300562083721175 63.928 0.049016617238521576
		 64.964 -0.0024660925846546893 65.996 -0.015382373705506325 67.032 -0.0039912727661430836
		 68.064 0.034435462206602097 69.1 0.090443238615989685 70.136 0.08109709620475769
		 71.168 0.12660548090934751 72.204 0.20761415362358093 73.24 0.31042817234992981 74.272 0.24512875080108645
		 75.308 0.043956484645605087;
createNode animCurveTA -n "Bip01_R_Foot_rotateZ3";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 73 ".ktv[0:72]"  0.824 1.4329707622528076 1.86 1.6174228191375732
		 2.892 1.8869442939758301 3.928 2.0447165966033936 4.964 2.1137900352478027 5.996 2.1944947242736816
		 7.032 2.2418756484985352 8.064 2.200739860534668 9.1 2.1904482841491699 10.136 2.2836785316467285
		 11.168 2.3842086791992192 12.204 2.4699795246124268 13.24 2.5079536437988281 14.272 2.4216904640197754
		 15.308 2.3867096900939941 16.34 2.55615234375 17.376 2.733142614364624 18.412 2.6047253608703613
		 19.444 2.2294528484344478 20.48 2.1182363033294682 21.516 2.4945075511932373 22.548 2.8923981189727783
		 23.584 3.0360138416290283 24.616 3.0604739189147949 25.652 3.0442700386047363 26.688 3.0966365337371826
		 27.72 3.2429409027099609 28.756 3.3443360328674316 29.792 3.3621857166290283 30.824 3.3530688285827637
		 31.86 3.379436731338501 32.892 3.5030527114868164 33.928 3.647258996963501 34.964 3.63326096534729
		 35.996 3.4303023815155029 37.032 3.2184908390045166 38.064 3.085376501083374 39.1 3.0199604034423828
		 40.136 2.9833226203918457 41.168 2.9210419654846191 42.204 2.8889462947845459 43.24 2.941942691802979
		 44.272 2.8949713706970215 45.308 2.5280907154083252 46.34 1.9790978431701665 47.376 1.5880982875823977
		 48.412 1.4684765338897705 49.444 1.4283215999603271 50.48 1.3181107044219973 51.516 1.1686878204345703
		 52.548 0.95553624629974365 53.584 0.70857161283493042 54.616 0.54329597949981689
		 55.652 0.3743862509727478 56.688 0.1056705117225647 57.72 -0.21607983112335205 58.756 -0.52592730522155762
		 59.788 -0.91143631935119618 60.824 -1.2920349836349487 61.86 -1.5614266395568848
		 62.892 -1.8261954784393313 63.928 -2.1495990753173828 64.964 -2.4010419845581055
		 65.996 -2.4118573665618896 67.032 -2.1819183826446533 68.064 -1.90848171710968 69.1 -1.7619495391845703
		 70.136 -1.6455725431442263 71.168 -1.5120735168457031 72.204 -1.4975440502166748
		 73.24 -1.4816201925277712 74.272 -1.2770806550979614 75.308 -1.0013345479965212;
select -ne :time1;
	setAttr ".o" 24.2;
	setAttr ".unw" 24.2;
select -ne :renderPartition;
	setAttr -s 126 ".st";
select -ne :initialShadingGroup;
	setAttr ".ro" yes;
select -ne :initialParticleSE;
	setAttr ".ro" yes;
select -ne :defaultShaderList1;
	setAttr -s 64 ".s";
select -ne :defaultTextureList1;
	setAttr -s 31 ".tx";
select -ne :postProcessList1;
	setAttr -s 2 ".p";
select -ne :defaultRenderUtilityList1;
	setAttr -s 31 ".u";
select -ne :defaultRenderingList1;
select -ne :renderGlobalsList1;
select -ne :defaultResolution;
	setAttr ".pa" 1;
select -ne :hardwareRenderGlobals;
	setAttr ".ctrs" 256;
	setAttr ".btrs" 512;
select -ne :hardwareRenderingGlobals;
	setAttr ".otfna" -type "stringArray" 18 "NURBS Curves" "NURBS Surfaces" "Polygons" "Subdiv Surfaces" "Particles" "Fluids" "Image Planes" "UI:" "Lights" "Cameras" "Locators" "Joints" "IK Handles" "Deformers" "Motion Trails" "Components" "Misc. UI" "Ornaments"  ;
	setAttr ".otfva" -type "Int32Array" 18 0 1 1 1 1 1
		 1 0 0 0 0 0 0 0 0 0 0 0 ;
select -ne :defaultHardwareRenderGlobals;
	setAttr ".fn" -type "string" "im";
	setAttr ".res" -type "string" "ntsc_4d 646 485 1.333";
select -ne :hyperGraphLayout;
	setAttr -s 4 ".hyp";
	setAttr ".hyp[1].isc" yes;
	setAttr ".hyp[2].isc" yes;
	setAttr ".hyp[3].isc" yes;
	setAttr ".hyp[4].isc" yes;
connectAttr "Bip01_Spine_translateX3.o" "|Sit_Talk_2|Bip01_Spine.tx";
connectAttr "Bip01_Spine_translateY3.o" "|Sit_Talk_2|Bip01_Spine.ty";
connectAttr "Bip01_Spine_translateZ3.o" "|Sit_Talk_2|Bip01_Spine.tz";
connectAttr "Bip01_Spine_rotateX3.o" "|Sit_Talk_2|Bip01_Spine.rx";
connectAttr "Bip01_Spine_rotateY3.o" "|Sit_Talk_2|Bip01_Spine.ry";
connectAttr "Bip01_Spine_rotateZ3.o" "|Sit_Talk_2|Bip01_Spine.rz";
connectAttr "Bip01_Spine1_rotateX3.o" "|Sit_Talk_2|Bip01_Spine|Bip01_Spine1.rx";
connectAttr "Bip01_Spine1_rotateY3.o" "|Sit_Talk_2|Bip01_Spine|Bip01_Spine1.ry";
connectAttr "Bip01_Spine1_rotateZ3.o" "|Sit_Talk_2|Bip01_Spine|Bip01_Spine1.rz";
connectAttr "|Sit_Talk_2|Bip01_Spine.s" "|Sit_Talk_2|Bip01_Spine|Bip01_Spine1.is"
		;
connectAttr "Bip01_Spine2_rotateX3.o" "|Sit_Talk_2|Bip01_Spine|Bip01_Spine1|Bip01_Spine2.rx"
		;
connectAttr "Bip01_Spine2_rotateY3.o" "|Sit_Talk_2|Bip01_Spine|Bip01_Spine1|Bip01_Spine2.ry"
		;
connectAttr "Bip01_Spine2_rotateZ3.o" "|Sit_Talk_2|Bip01_Spine|Bip01_Spine1|Bip01_Spine2.rz"
		;
connectAttr "|Sit_Talk_2|Bip01_Spine|Bip01_Spine1.s" "|Sit_Talk_2|Bip01_Spine|Bip01_Spine1|Bip01_Spine2.is"
		;
connectAttr "Bip01_Neck_rotateX2.o" "|Sit_Talk_2|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck.rx"
		;
connectAttr "Bip01_Neck_rotateY2.o" "|Sit_Talk_2|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck.ry"
		;
connectAttr "Bip01_Neck_rotateZ2.o" "|Sit_Talk_2|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck.rz"
		;
connectAttr "|Sit_Talk_2|Bip01_Spine|Bip01_Spine1|Bip01_Spine2.s" "|Sit_Talk_2|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck.is"
		;
connectAttr "Bip01_Head_rotateX2.o" "|Sit_Talk_2|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_Head.rx"
		;
connectAttr "Bip01_Head_rotateY2.o" "|Sit_Talk_2|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_Head.ry"
		;
connectAttr "Bip01_Head_rotateZ2.o" "|Sit_Talk_2|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_Head.rz"
		;
connectAttr "|Sit_Talk_2|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck.s" "|Sit_Talk_2|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_Head.is"
		;
connectAttr "|Sit_Talk_2|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_Head.s" "|Sit_Talk_2|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_Head|_notused_Bip01_HeadNub.is"
		;
connectAttr "Bip01_R_Clavicle_rotateX3.o" "|Sit_Talk_2|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_R_Clavicle.rx"
		;
connectAttr "Bip01_R_Clavicle_rotateY3.o" "|Sit_Talk_2|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_R_Clavicle.ry"
		;
connectAttr "Bip01_R_Clavicle_rotateZ3.o" "|Sit_Talk_2|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_R_Clavicle.rz"
		;
connectAttr "|Sit_Talk_2|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck.s" "|Sit_Talk_2|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_R_Clavicle.is"
		;
connectAttr "Bip01_R_UpperArm_rotateX3.o" "|Sit_Talk_2|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_R_Clavicle|Bip01_R_UpperArm.rx"
		;
connectAttr "Bip01_R_UpperArm_rotateY3.o" "|Sit_Talk_2|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_R_Clavicle|Bip01_R_UpperArm.ry"
		;
connectAttr "Bip01_R_UpperArm_rotateZ3.o" "|Sit_Talk_2|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_R_Clavicle|Bip01_R_UpperArm.rz"
		;
connectAttr "|Sit_Talk_2|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_R_Clavicle.s" "|Sit_Talk_2|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_R_Clavicle|Bip01_R_UpperArm.is"
		;
connectAttr "Bip01_R_Forearm_rotateY3.o" "|Sit_Talk_2|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_R_Clavicle|Bip01_R_UpperArm|Bip01_R_Forearm.ry"
		;
connectAttr "Bip01_R_Forearm_rotateX3.o" "|Sit_Talk_2|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_R_Clavicle|Bip01_R_UpperArm|Bip01_R_Forearm.rx"
		;
connectAttr "Bip01_R_Forearm_rotateZ3.o" "|Sit_Talk_2|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_R_Clavicle|Bip01_R_UpperArm|Bip01_R_Forearm.rz"
		;
connectAttr "|Sit_Talk_2|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_R_Clavicle|Bip01_R_UpperArm.s" "|Sit_Talk_2|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_R_Clavicle|Bip01_R_UpperArm|Bip01_R_Forearm.is"
		;
connectAttr "|Sit_Talk_2|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_R_Clavicle|Bip01_R_UpperArm|Bip01_R_Forearm.s" "|Sit_Talk_2|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_R_Clavicle|Bip01_R_UpperArm|Bip01_R_Forearm|Bip01_R_Hand.is"
		;
connectAttr "|Sit_Talk_2|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_R_Clavicle|Bip01_R_UpperArm|Bip01_R_Forearm|Bip01_R_Hand.s" "|Sit_Talk_2|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_R_Clavicle|Bip01_R_UpperArm|Bip01_R_Forearm|Bip01_R_Hand|Bip01_R_Finger0.is"
		;
connectAttr "|Sit_Talk_2|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_R_Clavicle|Bip01_R_UpperArm|Bip01_R_Forearm|Bip01_R_Hand|Bip01_R_Finger0.s" "|Sit_Talk_2|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_R_Clavicle|Bip01_R_UpperArm|Bip01_R_Forearm|Bip01_R_Hand|Bip01_R_Finger0|Bip01_R_Finger01.is"
		;
connectAttr "|Sit_Talk_2|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_R_Clavicle|Bip01_R_UpperArm|Bip01_R_Forearm|Bip01_R_Hand|Bip01_R_Finger0|Bip01_R_Finger01.s" "|Sit_Talk_2|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_R_Clavicle|Bip01_R_UpperArm|Bip01_R_Forearm|Bip01_R_Hand|Bip01_R_Finger0|Bip01_R_Finger01|_notused_Bip01_R_Finger0Nub.is"
		;
connectAttr "|Sit_Talk_2|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_R_Clavicle|Bip01_R_UpperArm|Bip01_R_Forearm|Bip01_R_Hand.s" "|Sit_Talk_2|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_R_Clavicle|Bip01_R_UpperArm|Bip01_R_Forearm|Bip01_R_Hand|Bip01_R_Finger2.is"
		;
connectAttr "|Sit_Talk_2|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_R_Clavicle|Bip01_R_UpperArm|Bip01_R_Forearm|Bip01_R_Hand|Bip01_R_Finger2.s" "|Sit_Talk_2|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_R_Clavicle|Bip01_R_UpperArm|Bip01_R_Forearm|Bip01_R_Hand|Bip01_R_Finger2|Bip01_R_Finger21.is"
		;
connectAttr "|Sit_Talk_2|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_R_Clavicle|Bip01_R_UpperArm|Bip01_R_Forearm|Bip01_R_Hand|Bip01_R_Finger2|Bip01_R_Finger21.s" "|Sit_Talk_2|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_R_Clavicle|Bip01_R_UpperArm|Bip01_R_Forearm|Bip01_R_Hand|Bip01_R_Finger2|Bip01_R_Finger21|_notused_Bip01_R_Finger2Nub.is"
		;
connectAttr "|Sit_Talk_2|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_R_Clavicle|Bip01_R_UpperArm|Bip01_R_Forearm|Bip01_R_Hand.s" "|Sit_Talk_2|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_R_Clavicle|Bip01_R_UpperArm|Bip01_R_Forearm|Bip01_R_Hand|Bip01_R_Finger1.is"
		;
connectAttr "|Sit_Talk_2|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_R_Clavicle|Bip01_R_UpperArm|Bip01_R_Forearm|Bip01_R_Hand|Bip01_R_Finger1.s" "|Sit_Talk_2|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_R_Clavicle|Bip01_R_UpperArm|Bip01_R_Forearm|Bip01_R_Hand|Bip01_R_Finger1|Bip01_R_Finger11.is"
		;
connectAttr "|Sit_Talk_2|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_R_Clavicle|Bip01_R_UpperArm|Bip01_R_Forearm|Bip01_R_Hand|Bip01_R_Finger1|Bip01_R_Finger11.s" "|Sit_Talk_2|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_R_Clavicle|Bip01_R_UpperArm|Bip01_R_Forearm|Bip01_R_Hand|Bip01_R_Finger1|Bip01_R_Finger11|_notused_Bip01_R_Finger1Nub.is"
		;
connectAttr "|Sit_Talk_2|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_R_Clavicle|Bip01_R_UpperArm|Bip01_R_Forearm|Bip01_R_Hand.s" "|Sit_Talk_2|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_R_Clavicle|Bip01_R_UpperArm|Bip01_R_Forearm|Bip01_R_Hand|Bip01_R_Finger3.is"
		;
connectAttr "|Sit_Talk_2|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_R_Clavicle|Bip01_R_UpperArm|Bip01_R_Forearm|Bip01_R_Hand|Bip01_R_Finger3.s" "|Sit_Talk_2|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_R_Clavicle|Bip01_R_UpperArm|Bip01_R_Forearm|Bip01_R_Hand|Bip01_R_Finger3|Bip01_R_Finger31.is"
		;
connectAttr "|Sit_Talk_2|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_R_Clavicle|Bip01_R_UpperArm|Bip01_R_Forearm|Bip01_R_Hand|Bip01_R_Finger3|Bip01_R_Finger31.s" "|Sit_Talk_2|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_R_Clavicle|Bip01_R_UpperArm|Bip01_R_Forearm|Bip01_R_Hand|Bip01_R_Finger3|Bip01_R_Finger31|_notused_Bip01_R_Finger3Nub.is"
		;
connectAttr "|Sit_Talk_2|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_R_Clavicle|Bip01_R_UpperArm|Bip01_R_Forearm|Bip01_R_Hand.s" "|Sit_Talk_2|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_R_Clavicle|Bip01_R_UpperArm|Bip01_R_Forearm|Bip01_R_Hand|Bip01_R_Finger4.is"
		;
connectAttr "|Sit_Talk_2|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_R_Clavicle|Bip01_R_UpperArm|Bip01_R_Forearm|Bip01_R_Hand|Bip01_R_Finger4.s" "|Sit_Talk_2|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_R_Clavicle|Bip01_R_UpperArm|Bip01_R_Forearm|Bip01_R_Hand|Bip01_R_Finger4|Bip01_R_Finger41.is"
		;
connectAttr "|Sit_Talk_2|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_R_Clavicle|Bip01_R_UpperArm|Bip01_R_Forearm|Bip01_R_Hand|Bip01_R_Finger4|Bip01_R_Finger41.s" "|Sit_Talk_2|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_R_Clavicle|Bip01_R_UpperArm|Bip01_R_Forearm|Bip01_R_Hand|Bip01_R_Finger4|Bip01_R_Finger41|_notused_Bip01_R_Finger4Nub.is"
		;
connectAttr "Bip01_L_Clavicle_rotateX3.o" "|Sit_Talk_2|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_L_Clavicle.rx"
		;
connectAttr "Bip01_L_Clavicle_rotateY3.o" "|Sit_Talk_2|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_L_Clavicle.ry"
		;
connectAttr "Bip01_L_Clavicle_rotateZ3.o" "|Sit_Talk_2|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_L_Clavicle.rz"
		;
connectAttr "|Sit_Talk_2|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck.s" "|Sit_Talk_2|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_L_Clavicle.is"
		;
connectAttr "Bip01_L_UpperArm_rotateX3.o" "|Sit_Talk_2|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_L_Clavicle|Bip01_L_UpperArm.rx"
		;
connectAttr "Bip01_L_UpperArm_rotateY3.o" "|Sit_Talk_2|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_L_Clavicle|Bip01_L_UpperArm.ry"
		;
connectAttr "Bip01_L_UpperArm_rotateZ3.o" "|Sit_Talk_2|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_L_Clavicle|Bip01_L_UpperArm.rz"
		;
connectAttr "|Sit_Talk_2|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_L_Clavicle.s" "|Sit_Talk_2|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_L_Clavicle|Bip01_L_UpperArm.is"
		;
connectAttr "Bip01_L_Forearm_rotateY3.o" "|Sit_Talk_2|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_L_Clavicle|Bip01_L_UpperArm|Bip01_L_Forearm.ry"
		;
connectAttr "Bip01_L_Forearm_rotateZ3.o" "|Sit_Talk_2|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_L_Clavicle|Bip01_L_UpperArm|Bip01_L_Forearm.rz"
		;
connectAttr "Bip01_L_Forearm_rotateX3.o" "|Sit_Talk_2|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_L_Clavicle|Bip01_L_UpperArm|Bip01_L_Forearm.rx"
		;
connectAttr "|Sit_Talk_2|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_L_Clavicle|Bip01_L_UpperArm.s" "|Sit_Talk_2|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_L_Clavicle|Bip01_L_UpperArm|Bip01_L_Forearm.is"
		;
connectAttr "|Sit_Talk_2|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_L_Clavicle|Bip01_L_UpperArm|Bip01_L_Forearm.s" "|Sit_Talk_2|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_L_Clavicle|Bip01_L_UpperArm|Bip01_L_Forearm|Bip01_L_Hand.is"
		;
connectAttr "|Sit_Talk_2|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_L_Clavicle|Bip01_L_UpperArm|Bip01_L_Forearm|Bip01_L_Hand.s" "|Sit_Talk_2|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_L_Clavicle|Bip01_L_UpperArm|Bip01_L_Forearm|Bip01_L_Hand|Bip01_L_Finger0.is"
		;
connectAttr "|Sit_Talk_2|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_L_Clavicle|Bip01_L_UpperArm|Bip01_L_Forearm|Bip01_L_Hand|Bip01_L_Finger0.s" "|Sit_Talk_2|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_L_Clavicle|Bip01_L_UpperArm|Bip01_L_Forearm|Bip01_L_Hand|Bip01_L_Finger0|Bip01_L_Finger01.is"
		;
connectAttr "|Sit_Talk_2|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_L_Clavicle|Bip01_L_UpperArm|Bip01_L_Forearm|Bip01_L_Hand|Bip01_L_Finger0|Bip01_L_Finger01.s" "|Sit_Talk_2|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_L_Clavicle|Bip01_L_UpperArm|Bip01_L_Forearm|Bip01_L_Hand|Bip01_L_Finger0|Bip01_L_Finger01|_notused_Bip01_L_Finger0Nub.is"
		;
connectAttr "|Sit_Talk_2|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_L_Clavicle|Bip01_L_UpperArm|Bip01_L_Forearm|Bip01_L_Hand.s" "|Sit_Talk_2|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_L_Clavicle|Bip01_L_UpperArm|Bip01_L_Forearm|Bip01_L_Hand|Bip01_L_Finger2.is"
		;
connectAttr "|Sit_Talk_2|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_L_Clavicle|Bip01_L_UpperArm|Bip01_L_Forearm|Bip01_L_Hand|Bip01_L_Finger2.s" "|Sit_Talk_2|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_L_Clavicle|Bip01_L_UpperArm|Bip01_L_Forearm|Bip01_L_Hand|Bip01_L_Finger2|Bip01_L_Finger21.is"
		;
connectAttr "|Sit_Talk_2|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_L_Clavicle|Bip01_L_UpperArm|Bip01_L_Forearm|Bip01_L_Hand|Bip01_L_Finger2|Bip01_L_Finger21.s" "|Sit_Talk_2|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_L_Clavicle|Bip01_L_UpperArm|Bip01_L_Forearm|Bip01_L_Hand|Bip01_L_Finger2|Bip01_L_Finger21|_notused_Bip01_L_Finger2Nub.is"
		;
connectAttr "|Sit_Talk_2|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_L_Clavicle|Bip01_L_UpperArm|Bip01_L_Forearm|Bip01_L_Hand.s" "|Sit_Talk_2|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_L_Clavicle|Bip01_L_UpperArm|Bip01_L_Forearm|Bip01_L_Hand|Bip01_L_Finger1.is"
		;
connectAttr "|Sit_Talk_2|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_L_Clavicle|Bip01_L_UpperArm|Bip01_L_Forearm|Bip01_L_Hand|Bip01_L_Finger1.s" "|Sit_Talk_2|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_L_Clavicle|Bip01_L_UpperArm|Bip01_L_Forearm|Bip01_L_Hand|Bip01_L_Finger1|Bip01_L_Finger11.is"
		;
connectAttr "|Sit_Talk_2|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_L_Clavicle|Bip01_L_UpperArm|Bip01_L_Forearm|Bip01_L_Hand|Bip01_L_Finger1|Bip01_L_Finger11.s" "|Sit_Talk_2|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_L_Clavicle|Bip01_L_UpperArm|Bip01_L_Forearm|Bip01_L_Hand|Bip01_L_Finger1|Bip01_L_Finger11|_notused_Bip01_L_Finger1Nub.is"
		;
connectAttr "|Sit_Talk_2|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_L_Clavicle|Bip01_L_UpperArm|Bip01_L_Forearm|Bip01_L_Hand.s" "|Sit_Talk_2|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_L_Clavicle|Bip01_L_UpperArm|Bip01_L_Forearm|Bip01_L_Hand|Bip01_L_Finger3.is"
		;
connectAttr "|Sit_Talk_2|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_L_Clavicle|Bip01_L_UpperArm|Bip01_L_Forearm|Bip01_L_Hand|Bip01_L_Finger3.s" "|Sit_Talk_2|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_L_Clavicle|Bip01_L_UpperArm|Bip01_L_Forearm|Bip01_L_Hand|Bip01_L_Finger3|Bip01_L_Finger31.is"
		;
connectAttr "|Sit_Talk_2|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_L_Clavicle|Bip01_L_UpperArm|Bip01_L_Forearm|Bip01_L_Hand|Bip01_L_Finger3|Bip01_L_Finger31.s" "|Sit_Talk_2|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_L_Clavicle|Bip01_L_UpperArm|Bip01_L_Forearm|Bip01_L_Hand|Bip01_L_Finger3|Bip01_L_Finger31|_notused_Bip01_L_Finger3Nub.is"
		;
connectAttr "|Sit_Talk_2|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_L_Clavicle|Bip01_L_UpperArm|Bip01_L_Forearm|Bip01_L_Hand.s" "|Sit_Talk_2|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_L_Clavicle|Bip01_L_UpperArm|Bip01_L_Forearm|Bip01_L_Hand|Bip01_L_Finger4.is"
		;
connectAttr "|Sit_Talk_2|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_L_Clavicle|Bip01_L_UpperArm|Bip01_L_Forearm|Bip01_L_Hand|Bip01_L_Finger4.s" "|Sit_Talk_2|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_L_Clavicle|Bip01_L_UpperArm|Bip01_L_Forearm|Bip01_L_Hand|Bip01_L_Finger4|Bip01_L_Finger41.is"
		;
connectAttr "|Sit_Talk_2|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_L_Clavicle|Bip01_L_UpperArm|Bip01_L_Forearm|Bip01_L_Hand|Bip01_L_Finger4|Bip01_L_Finger41.s" "|Sit_Talk_2|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_L_Clavicle|Bip01_L_UpperArm|Bip01_L_Forearm|Bip01_L_Hand|Bip01_L_Finger4|Bip01_L_Finger41|_notused_Bip01_L_Finger4Nub.is"
		;
connectAttr "Bip01_L_Thigh_rotateX3.o" "|Sit_Talk_2|Bip01_Spine|Bip01_L_Thigh.rx"
		;
connectAttr "Bip01_L_Thigh_rotateY3.o" "|Sit_Talk_2|Bip01_Spine|Bip01_L_Thigh.ry"
		;
connectAttr "Bip01_L_Thigh_rotateZ3.o" "|Sit_Talk_2|Bip01_Spine|Bip01_L_Thigh.rz"
		;
connectAttr "|Sit_Talk_2|Bip01_Spine.s" "|Sit_Talk_2|Bip01_Spine|Bip01_L_Thigh.is"
		;
connectAttr "Bip01_L_Calf_rotateX3.o" "|Sit_Talk_2|Bip01_Spine|Bip01_L_Thigh|Bip01_L_Calf.rx"
		;
connectAttr "Bip01_L_Calf_rotateY3.o" "|Sit_Talk_2|Bip01_Spine|Bip01_L_Thigh|Bip01_L_Calf.ry"
		;
connectAttr "Bip01_L_Calf_rotateZ3.o" "|Sit_Talk_2|Bip01_Spine|Bip01_L_Thigh|Bip01_L_Calf.rz"
		;
connectAttr "|Sit_Talk_2|Bip01_Spine|Bip01_L_Thigh.s" "|Sit_Talk_2|Bip01_Spine|Bip01_L_Thigh|Bip01_L_Calf.is"
		;
connectAttr "Bip01_L_Foot_rotateX3.o" "|Sit_Talk_2|Bip01_Spine|Bip01_L_Thigh|Bip01_L_Calf|Bip01_L_Foot.rx"
		;
connectAttr "Bip01_L_Foot_rotateY3.o" "|Sit_Talk_2|Bip01_Spine|Bip01_L_Thigh|Bip01_L_Calf|Bip01_L_Foot.ry"
		;
connectAttr "Bip01_L_Foot_rotateZ3.o" "|Sit_Talk_2|Bip01_Spine|Bip01_L_Thigh|Bip01_L_Calf|Bip01_L_Foot.rz"
		;
connectAttr "|Sit_Talk_2|Bip01_Spine|Bip01_L_Thigh|Bip01_L_Calf.s" "|Sit_Talk_2|Bip01_Spine|Bip01_L_Thigh|Bip01_L_Calf|Bip01_L_Foot.is"
		;
connectAttr "|Sit_Talk_2|Bip01_Spine|Bip01_L_Thigh|Bip01_L_Calf|Bip01_L_Foot.s" "|Sit_Talk_2|Bip01_Spine|Bip01_L_Thigh|Bip01_L_Calf|Bip01_L_Foot|Bip01_L_Toe0.is"
		;
connectAttr "|Sit_Talk_2|Bip01_Spine|Bip01_L_Thigh|Bip01_L_Calf|Bip01_L_Foot|Bip01_L_Toe0.s" "|Sit_Talk_2|Bip01_Spine|Bip01_L_Thigh|Bip01_L_Calf|Bip01_L_Foot|Bip01_L_Toe0|_notused_Bip01_L_Toe0Nub.is"
		;
connectAttr "Bip01_R_Thigh_rotateX3.o" "|Sit_Talk_2|Bip01_Spine|Bip01_R_Thigh.rx"
		;
connectAttr "Bip01_R_Thigh_rotateY3.o" "|Sit_Talk_2|Bip01_Spine|Bip01_R_Thigh.ry"
		;
connectAttr "Bip01_R_Thigh_rotateZ3.o" "|Sit_Talk_2|Bip01_Spine|Bip01_R_Thigh.rz"
		;
connectAttr "|Sit_Talk_2|Bip01_Spine.s" "|Sit_Talk_2|Bip01_Spine|Bip01_R_Thigh.is"
		;
connectAttr "Bip01_R_Calf_rotateX3.o" "|Sit_Talk_2|Bip01_Spine|Bip01_R_Thigh|Bip01_R_Calf.rx"
		;
connectAttr "Bip01_R_Calf_rotateY3.o" "|Sit_Talk_2|Bip01_Spine|Bip01_R_Thigh|Bip01_R_Calf.ry"
		;
connectAttr "Bip01_R_Calf_rotateZ3.o" "|Sit_Talk_2|Bip01_Spine|Bip01_R_Thigh|Bip01_R_Calf.rz"
		;
connectAttr "|Sit_Talk_2|Bip01_Spine|Bip01_R_Thigh.s" "|Sit_Talk_2|Bip01_Spine|Bip01_R_Thigh|Bip01_R_Calf.is"
		;
connectAttr "Bip01_R_Foot_rotateX3.o" "|Sit_Talk_2|Bip01_Spine|Bip01_R_Thigh|Bip01_R_Calf|Bip01_R_Foot.rx"
		;
connectAttr "Bip01_R_Foot_rotateY3.o" "|Sit_Talk_2|Bip01_Spine|Bip01_R_Thigh|Bip01_R_Calf|Bip01_R_Foot.ry"
		;
connectAttr "Bip01_R_Foot_rotateZ3.o" "|Sit_Talk_2|Bip01_Spine|Bip01_R_Thigh|Bip01_R_Calf|Bip01_R_Foot.rz"
		;
connectAttr "|Sit_Talk_2|Bip01_Spine|Bip01_R_Thigh|Bip01_R_Calf.s" "|Sit_Talk_2|Bip01_Spine|Bip01_R_Thigh|Bip01_R_Calf|Bip01_R_Foot.is"
		;
connectAttr "|Sit_Talk_2|Bip01_Spine|Bip01_R_Thigh|Bip01_R_Calf|Bip01_R_Foot.s" "|Sit_Talk_2|Bip01_Spine|Bip01_R_Thigh|Bip01_R_Calf|Bip01_R_Foot|Bip01_R_Toe0.is"
		;
connectAttr "|Sit_Talk_2|Bip01_Spine|Bip01_R_Thigh|Bip01_R_Calf|Bip01_R_Foot|Bip01_R_Toe0.s" "|Sit_Talk_2|Bip01_Spine|Bip01_R_Thigh|Bip01_R_Calf|Bip01_R_Foot|Bip01_R_Toe0|_notused_Bip01_R_Toe0Nub.is"
		;
// End of Animation v5 - Sit Talk 2.ma
