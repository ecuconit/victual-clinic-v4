//Maya ASCII 2014 scene
//Name: Animation v5 - Obese Sit Idle 1.ma
//Last modified: Tue, Apr 14, 2015 07:39:32 AM
//Codeset: 1252
requires maya "2014";
currentUnit -l centimeter -a degree -t film;
fileInfo "application" "maya";
fileInfo "product" "Maya 2014";
fileInfo "version" "2014 x64";
fileInfo "cutIdentifier" "201303010241-864206";
fileInfo "osv" "Microsoft Windows 7 Enterprise Edition, 64-bit Windows 7 Service Pack 1 (Build 7601)\n";
fileInfo "license" "education";
createNode transform -n "Obese_Sit_Idle_1";
	setAttr ".v" no;
createNode joint -n "Bip01_Spine" -p "Obese_Sit_Idle_1";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".jo" -type "double3" 90.000171 0 89.999999999999986 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" 0.0058944645619291025 0.99957100188420478 -0.028689152649306493 0
		 -0.00095053300202146973 0.028695238773347783 0.99958775490636764 0 0.99998217572841774 -0.0058647646114406982 0.0011192682932001929 0
		 -6.7139604917915256e-006 99.443700000000007 -2.2495999999899592 1;
	setAttr -k on ".liw";
	setAttr ".fbxID" 5;
createNode joint -n "Bip01_Spine1" -p "|Obese_Sit_Idle_1|Bip01_Spine";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".t" -type "double3" 13.3984 -0.0107 0 ;
	setAttr ".s" -type "double3" 1 0.99999999999999989 1 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" -0.013016029413255978 -0.008122775381498382 0.99988229482195301 0
		 0.029728798363151423 -0.9995280890415984 -0.0077329014434088985 0 0.99947325203138693 0.029624647457009743 0.013251367357806723 0
		 9.9999999999999995e-007 2.2603360000000001 112.84212599999999 1;
	setAttr -k on ".liw";
	setAttr ".fbxID" 5;
createNode joint -n "Bip01_Spine2" -p "|Obese_Sit_Idle_1|Bip01_Spine|Bip01_Spine1";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".t" -type "double3" 13.3983 -0.0107 0 ;
	setAttr ".s" -type "double3" 1 0.99999999999999956 1 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" 0 0 1 0 0 -1 0 0 1 0 0 0 -3.9999999999999998e-006 -9.6239790000000003 -19.074804 1;
	setAttr -k on ".liw";
	setAttr ".fbxID" 5;
createNode joint -n "Bip01_Neck" -p "|Obese_Sit_Idle_1|Bip01_Spine|Bip01_Spine1|Bip01_Spine2";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".t" -type "double3" 13.3985 -0.007899999999999999 0 ;
	setAttr ".r" -type "double3" 1.1848489498583726e-023 -5.9999999999999985e-006 1.1479959999999994 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" 0 -0.020830999999999999 0.99978299999999998 0 0 -0.99978299999999998 -0.020830999999999999 0
		 1 0 0 0 -1.0000000000000001e-005 -9.6266999999999996 -5.6762839999999999 1;
	setAttr -k on ".liw";
	setAttr ".fbxID" 5;
createNode joint -n "Bip01_Head" -p "|Obese_Sit_Idle_1|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".t" -type "double3" 9.9829 0 0 ;
	setAttr ".r" -type "double3" 9.4787915988671744e-023 -1.3000000000000004e-005 4.622390999999995 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" 0 -0.10133399999999999 0.99485299999999999 0 0 -0.99485199999999996 -0.10133399999999999 0
		 1 0 0 0 -4.4000000000000012e-005 -9.8346470000000004 4.3044190000000002 1;
	setAttr -k on ".liw";
	setAttr ".fbxID" 5;
createNode joint -n "_notused_Bip01_HeadNub" -p "|Obese_Sit_Idle_1|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_Head";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".t" -type "double3" 18.3537 0 0 ;
	setAttr ".ssc" no;
	setAttr ".fbxID" 5;
createNode joint -n "Bip01_R_Clavicle" -p "|Obese_Sit_Idle_1|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".t" -type "double3" -0.8268 -0.6612 -3.9165 ;
	setAttr ".s" -type "double3" 0.99999999999999989 0.99999999999999989 0.99999999999999989 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" -0.99454799999999999 -9.9454800000000002e-007 -0.104286 0
		 -9.8906500000000008e-007 1 -1.0428599999999999e-007 0 0.104286 -5.7179000000000015e-010 -0.99454799999999999 0
		 -3.9165049999999999 2.9251839999999998 138.82619800000001 1;
	setAttr -k on ".liw";
	setAttr ".fbxID" 5;
createNode joint -n "Bip01_R_UpperArm" -p "|Obese_Sit_Idle_1|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_R_Clavicle";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".t" -type "double3" 10.6745 0 0 ;
	setAttr ".s" -type "double3" 0.99999999999999989 1 1 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" -0.75332600000000005 0.10000199999999999 -0.65000000000000002 0
		 0.071782399999999996 0.99496899999999999 0.0698825 0 0.65371900000000005 0.0059857399999999998 -0.75671500000000003 0
		 -14.532831 2.9251239999999998 137.712976 1;
	setAttr -k on ".liw";
	setAttr ".fbxID" 5;
createNode joint -n "Bip01_R_Forearm" -p "|Obese_Sit_Idle_1|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_R_Clavicle|Bip01_R_UpperArm";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".t" -type "double3" 25.3375 0 0 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" -0.752861 -0.095945600000000006 -0.65114899999999998 0
		 -0.076500899999999997 0.99536800000000003 -0.058215000000000003 0 0.65371900000000005 0.0059856600000000003 -0.756714 0
		 -33.620182 5.4589869999999996 121.243574 1;
	setAttr -k on ".liw";
	setAttr ".fbxID" 5;
createNode joint -n "Bip01_R_Hand" -p "|Obese_Sit_Idle_1|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_R_Clavicle|Bip01_R_UpperArm|Bip01_R_Forearm";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".t" -type "double3" 24.9881 0 0 ;
	setAttr ".s" -type "double3" 1 0.99999999999999989 1 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" -0.67922700000000003 -0.050331500000000001 -0.73219999999999996 0
		 0.73278100000000002 0.0092545500000000003 -0.68040299999999998 0 0.0410219 -0.99868900000000005 0.030596000000000002 0
		 -52.432737000000003 3.0614050000000002 104.972679 1;
	setAttr -k on ".liw";
	setAttr ".fbxID" 5;
createNode joint -n "Bip01_R_Finger0" -p "|Obese_Sit_Idle_1|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_R_Clavicle|Bip01_R_UpperArm|Bip01_R_Forearm|Bip01_R_Hand";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".t" -type "double3" 2.3036000000000003 1.3941 2.9222 ;
	setAttr ".r" -type "double3" -81.93018 -23.314181 18.295908 ;
	setAttr ".s" -type "double3" 0.99999999999999989 1 1 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" -0.36474400000000001 -0.436471 -0.82246900000000001 0
		 -0.072261699999999998 0.89392799999999994 -0.44234699999999999 0 0.92829899999999999 -0.10191 -0.357595 0
		 -52.855969000000002 0.0400224 102.426821 1;
	setAttr -k on ".liw";
	setAttr ".fbxID" 5;
createNode joint -n "Bip01_R_Finger01" -p "|Obese_Sit_Idle_1|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_R_Clavicle|Bip01_R_UpperArm|Bip01_R_Forearm|Bip01_R_Hand|Bip01_R_Finger0";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".t" -type "double3" 4.0806 0 0 ;
	setAttr ".r" -type "double3" 9.4787915988669452e-023 3.0000000000000005e-006 24.064549 ;
	setAttr ".s" -type "double3" 1 1 0.99999999999999989 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" -0.36250900000000003 -0.034022900000000002 -0.93135900000000005 0
		 0.082749500000000004 0.99421099999999996 -0.068527199999999996 0 0.92830000000000001 -0.101911 -0.357595 0
		 -54.344414999999998 -1.7410159999999999 99.070615000000004 1;
	setAttr -k on ".liw";
	setAttr ".fbxID" 5;
createNode joint -n "_notused_Bip01_R_Finger0Nub" -p "|Obese_Sit_Idle_1|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_R_Clavicle|Bip01_R_UpperArm|Bip01_R_Forearm|Bip01_R_Hand|Bip01_R_Finger0|Bip01_R_Finger01";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".t" -type "double3" 3.3413 0 0 ;
	setAttr ".r" -type "double3" 1e-006 0 -180 ;
	setAttr ".ssc" no;
	setAttr ".fbxID" 5;
createNode joint -n "Bip01_R_Finger2" -p "|Obese_Sit_Idle_1|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_R_Clavicle|Bip01_R_UpperArm|Bip01_R_Forearm|Bip01_R_Hand";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".t" -type "double3" 7.3709 -0.4816 0.6341 ;
	setAttr ".r" -type "double3" 0.043615000000000029 -0.013407000000000007 17.086613999999997 ;
	setAttr ".s" -type "double3" 0.99999999999999989 0.99999999999999989 0.99999999999999989 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" -0.43393399999999999 -0.045623999999999998 -0.89978899999999995 0
		 0.90003599999999995 0.022875199999999998 -0.43521399999999999 0 0.040439000000000003 -0.99869600000000003 0.031137000000000001 0
		 -57.766043000000003 2.0527600000000001 99.922762000000006 1;
	setAttr -k on ".liw";
	setAttr ".fbxID" 5;
createNode joint -n "Bip01_R_Finger21" -p "|Obese_Sit_Idle_1|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_R_Clavicle|Bip01_R_UpperArm|Bip01_R_Forearm|Bip01_R_Hand|Bip01_R_Finger2";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".t" -type "double3" 3.8208 0 0 ;
	setAttr ".r" -type "double3" 0 0 29.946389999999987 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" 0.0732873 -0.028113900000000001 -0.99691399999999997 0
		 0.99648999999999999 0.042598999999999998 0.072054900000000005 0 0.0404418 -0.99869600000000003 0.0311372 0
		 -59.424045999999997 1.8784400000000001 96.484806000000006 1;
	setAttr -k on ".liw";
	setAttr ".fbxID" 5;
createNode joint -n "_notused_Bip01_R_Finger2Nub" -p "|Obese_Sit_Idle_1|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_R_Clavicle|Bip01_R_UpperArm|Bip01_R_Forearm|Bip01_R_Hand|Bip01_R_Finger2|Bip01_R_Finger21";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".t" -type "double3" 4.7174000000000005 0 0 ;
	setAttr ".r" -type "double3" 0 0 180 ;
	setAttr ".ssc" no;
	setAttr ".fbxID" 5;
createNode joint -n "Bip01_R_Finger1" -p "|Obese_Sit_Idle_1|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_R_Clavicle|Bip01_R_UpperArm|Bip01_R_Forearm|Bip01_R_Hand";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".t" -type "double3" 7.3263 -0.211 2.2784 ;
	setAttr ".r" -type "double3" -0.73310399999999953 -5.3145149999999983 22.646732999999994 ;
	setAttr ".s" -type "double3" 1 1 0.99999999999999989 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" -0.33941900000000003 -0.13520499999999999 -0.93086800000000003 0
		 0.93680799999999997 0.040593299999999999 -0.34748099999999998 0 0.084767999999999996 -0.98998600000000003 0.112883 0
		 -57.470132999999997 0.41539700000000002 99.821579 1;
	setAttr -k on ".liw";
	setAttr ".fbxID" 5;
createNode joint -n "Bip01_R_Finger11" -p "|Obese_Sit_Idle_1|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_R_Clavicle|Bip01_R_UpperArm|Bip01_R_Forearm|Bip01_R_Hand|Bip01_R_Finger1";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".t" -type "double3" 3.5597999999999996 0 0 ;
	setAttr ".r" -type "double3" -2e-006 3.999999999999999e-006 22.627677999999996 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" 0.047135799999999999 -0.10918 -0.99290400000000001 0
		 0.995286 0.089482699999999998 0.037409400000000002 0 0.084763400000000003 -0.98998600000000003 0.112883 0
		 -58.678344000000003 -0.065903900000000001 96.507863999999998 1;
	setAttr -k on ".liw";
	setAttr ".fbxID" 5;
createNode joint -n "_notused_Bip01_R_Finger1Nub" -p "|Obese_Sit_Idle_1|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_R_Clavicle|Bip01_R_UpperArm|Bip01_R_Forearm|Bip01_R_Hand|Bip01_R_Finger1|Bip01_R_Finger11";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".t" -type "double3" 4.1404000000000005 0 0 ;
	setAttr ".r" -type "double3" 0 0 180 ;
	setAttr ".ssc" no;
	setAttr ".fbxID" 5;
createNode joint -n "Bip01_R_Finger3" -p "|Obese_Sit_Idle_1|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_R_Clavicle|Bip01_R_UpperArm|Bip01_R_Forearm|Bip01_R_Hand";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".t" -type "double3" 7.2345 -0.5997 -0.9332 ;
	setAttr ".r" -type "double3" 0.043363999999999805 3.826455 18.509659999999997 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" -0.41327900000000001 0.0219578 -0.91034000000000004 0
		 0.91051400000000005 0.0239971 -0.41277999999999998 0 0.0127817 -0.999471 -0.0299104 0
		 -57.824306 3.6237469999999998 100.05504999999999 1;
	setAttr -k on ".liw";
	setAttr ".fbxID" 5;
createNode joint -n "Bip01_R_Finger31" -p "|Obese_Sit_Idle_1|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_R_Clavicle|Bip01_R_UpperArm|Bip01_R_Forearm|Bip01_R_Hand|Bip01_R_Finger3";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".t" -type "double3" 3.7241 0 0 ;
	setAttr ".r" -type "double3" 3.0000000000000013e-006 3.0000000000000005e-006 27.984287000000009 ;
	setAttr ".s" -type "double3" 1 0.99999999999999989 0.99999999999999989 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" 0.0622821 0.0306512 -0.99758800000000003 0 0.99797599999999997 0.0108983 0.062641199999999994 0
		 0.012792100000000001 -0.999471 -0.0299104 0 -59.363360999999998 3.705527 96.664748000000003 1;
	setAttr -k on ".liw";
	setAttr ".fbxID" 5;
createNode joint -n "_notused_Bip01_R_Finger3Nub" -p "|Obese_Sit_Idle_1|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_R_Clavicle|Bip01_R_UpperArm|Bip01_R_Forearm|Bip01_R_Hand|Bip01_R_Finger3|Bip01_R_Finger31";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".t" -type "double3" 4.2397 0 0 ;
	setAttr ".r" -type "double3" 3e-006 -5e-006 180 ;
	setAttr ".ssc" no;
	setAttr ".fbxID" 5;
createNode joint -n "Bip01_R_Finger4" -p "|Obese_Sit_Idle_1|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_R_Clavicle|Bip01_R_UpperArm|Bip01_R_Forearm|Bip01_R_Hand";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".t" -type "double3" 7.2292 -0.4187 -2.3639 ;
	setAttr ".r" -type "double3" -0.17713400000000007 5.5619560000000012 16.615208 ;
	setAttr ".s" -type "double3" 0.99999999999999989 0.99999999999999989 1 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" -0.44323200000000001 0.051426199999999998 -0.89493100000000003 0
		 0.89640699999999995 0.026346700000000001 -0.44244899999999998 0 0.00082491599999999997 -0.99832900000000002 -0.057776500000000001 0
		 -57.746822000000002 5.0544450000000003 99.892002000000005 1;
	setAttr -k on ".liw";
	setAttr ".fbxID" 5;
createNode joint -n "Bip01_R_Finger41" -p "|Obese_Sit_Idle_1|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_R_Clavicle|Bip01_R_UpperArm|Bip01_R_Forearm|Bip01_R_Hand|Bip01_R_Finger4";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".t" -type "double3" 3.4791000000000003 0 0 ;
	setAttr ".r" -type "double3" -1.9999999999999995e-006 -3.0000000000000005e-006 29.444368 ;
	setAttr ".s" -type "double3" 1 0.99999999999999989 1 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" 0.054672699999999998 0.057735700000000001 -0.996834 0
		 0.99850399999999995 -0.0023359700000000001 0.054628999999999997 0 0.00082546799999999999 -0.99832900000000002 -0.057777000000000002 0
		 -59.288860999999997 5.2333790000000002 96.778379000000001 1;
	setAttr -k on ".liw";
	setAttr ".fbxID" 5;
createNode joint -n "_notused_Bip01_R_Finger4Nub" -p "|Obese_Sit_Idle_1|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_R_Clavicle|Bip01_R_UpperArm|Bip01_R_Forearm|Bip01_R_Hand|Bip01_R_Finger4|Bip01_R_Finger41";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".t" -type "double3" 3.3974 0 0 ;
	setAttr ".r" -type "double3" 0 0 -180 ;
	setAttr ".ssc" no;
	setAttr ".fbxID" 5;
createNode joint -n "Bip01_L_Clavicle" -p "|Obese_Sit_Idle_1|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".t" -type "double3" -0.8268 -0.6612 3.9165 ;
	setAttr ".s" -type "double3" 1 0.99999999999999989 0.99999999999999989 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" 0.99454799999999999 9.9454800000000002e-007 -0.104286 0
		 -9.8906500000000008e-007 1 1.0428599999999999e-007 0 0.104286 -5.7179000000000015e-010 0.99454799999999999 0
		 3.9165070000000002 2.9252039999999999 138.82619700000001 1;
	setAttr -k on ".liw";
	setAttr ".fbxID" 5;
createNode joint -n "Bip01_L_UpperArm" -p "|Obese_Sit_Idle_1|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_L_Clavicle";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".t" -type "double3" 10.6745 0 0 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" 0.75332500000000002 0.100004 -0.65000000000000002 0
		 -0.071785600000000005 0.99496899999999999 0.069882299999999994 0 0.65371900000000005 -0.0059834299999999997 0.756714 0
		 14.532825000000001 2.925173 137.712974 1;
	setAttr -k on ".liw";
	setAttr ".fbxID" 5;
createNode joint -n "Bip01_L_Forearm" -p "|Obese_Sit_Idle_1|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_L_Clavicle|Bip01_L_UpperArm";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".t" -type "double3" 25.3374 0 0 ;
	setAttr ".s" -type "double3" 1 1 0.99999999999999989 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" 0.752861 -0.095943299999999995 -0.65114899999999998 0
		 0.076497700000000002 0.99536899999999995 -0.058215299999999998 0 0.65371900000000005 -0.0059834199999999997 0.756714 0
		 33.620189000000003 5.4590990000000001 121.243576 1;
	setAttr -k on ".liw";
	setAttr ".fbxID" 5;
createNode joint -n "Bip01_L_Hand" -p "|Obese_Sit_Idle_1|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_L_Clavicle|Bip01_L_UpperArm|Bip01_L_Forearm";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".t" -type "double3" 24.9881 0 0 ;
	setAttr ".s" -type "double3" 0.99999999999999989 1 0.99999999999999989 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" 0.67922800000000005 -0.050329499999999999 -0.73219999999999996 0
		 -0.73278100000000002 0.0092525000000000003 -0.68040299999999998 0 0.041019 0.99868999999999997 -0.030595899999999999 0
		 52.432744999999997 3.0615610000000002 104.972662 1;
	setAttr -k on ".liw";
	setAttr ".fbxID" 5;
createNode joint -n "Bip01_L_Finger0" -p "|Obese_Sit_Idle_1|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_L_Clavicle|Bip01_L_UpperArm|Bip01_L_Forearm|Bip01_L_Hand";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".t" -type "double3" 2.3036000000000003 1.3941 -2.9222 ;
	setAttr ".r" -type "double3" 81.930180000000021 23.314181000000019 18.295909 ;
	setAttr ".s" -type "double3" 0.99999999999999989 1 1.0000000000000002 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" 0.36474600000000001 -0.43647000000000002 -0.82246900000000001 0
		 0.072258799999999998 0.89392799999999994 -0.44234699999999999 0 0.92830000000000001 0.101913 0.357595 0
		 52.856034999999999 0.0402022 102.42681 1;
	setAttr -k on ".liw";
	setAttr ".fbxID" 5;
createNode joint -n "Bip01_L_Finger01" -p "|Obese_Sit_Idle_1|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_L_Clavicle|Bip01_L_UpperArm|Bip01_L_Forearm|Bip01_L_Hand|Bip01_L_Finger0";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".t" -type "double3" 4.0806 0 0 ;
	setAttr ".r" -type "double3" 0 0 24.06455200000001 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" 0.36250900000000003 -0.034021900000000001 -0.93135900000000005 0
		 -0.082751900000000003 0.99421199999999998 -0.068527000000000005 0 0.92829899999999999 0.101913 0.357595 0
		 54.344436000000002 -1.7408459999999999 99.070606999999995 1;
	setAttr -k on ".liw";
	setAttr ".fbxID" 5;
createNode joint -n "_notused_Bip01_L_Finger0Nub" -p "|Obese_Sit_Idle_1|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_L_Clavicle|Bip01_L_UpperArm|Bip01_L_Forearm|Bip01_L_Hand|Bip01_L_Finger0|Bip01_L_Finger01";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".t" -type "double3" 3.3413 0 0 ;
	setAttr ".ssc" no;
	setAttr ".fbxID" 5;
createNode joint -n "Bip01_L_Finger2" -p "|Obese_Sit_Idle_1|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_L_Clavicle|Bip01_L_UpperArm|Bip01_L_Forearm|Bip01_L_Hand";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".t" -type "double3" 7.3709 -0.4816 -0.6341 ;
	setAttr ".r" -type "double3" -0.043610000000000031 0.01341 17.086619000000006 ;
	setAttr ".s" -type "double3" 1 0.99999999999999989 0.99999999999999989 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" 0.43393500000000002 -0.045622700000000002 -0.89978899999999995 0
		 -0.90003599999999995 0.022871599999999999 -0.43521399999999999 0 0.040435199999999998 0.99869699999999995 -0.031137399999999999 0
		 57.766060000000003 2.0529269999999999 99.922754999999995 1;
	setAttr -k on ".liw";
	setAttr ".fbxID" 5;
createNode joint -n "Bip01_L_Finger21" -p "|Obese_Sit_Idle_1|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_L_Clavicle|Bip01_L_UpperArm|Bip01_L_Forearm|Bip01_L_Hand|Bip01_L_Finger2";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".t" -type "double3" 3.8208 0 0 ;
	setAttr ".r" -type "double3" -4.0000000000000024e-006 -4.0000000000000015e-006 29.946390000000019 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" -0.0732873 -0.0281141 -0.99691399999999997 0 -0.99649100000000002 0.042592999999999999 0.072054999999999994 0
		 0.040435899999999997 0.99869699999999995 -0.031137000000000001 0 59.424058000000002 1.878614 96.484800000000007 1;
	setAttr -k on ".liw";
	setAttr ".fbxID" 5;
createNode joint -n "_notused_Bip01_L_Finger2Nub" -p "|Obese_Sit_Idle_1|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_L_Clavicle|Bip01_L_UpperArm|Bip01_L_Forearm|Bip01_L_Hand|Bip01_L_Finger2|Bip01_L_Finger21";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".t" -type "double3" 4.7174000000000005 0 0 ;
	setAttr ".ssc" no;
	setAttr ".fbxID" 5;
createNode joint -n "Bip01_L_Finger1" -p "|Obese_Sit_Idle_1|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_L_Clavicle|Bip01_L_UpperArm|Bip01_L_Forearm|Bip01_L_Hand";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".t" -type "double3" 7.3263 -0.211 -2.2784 ;
	setAttr ".r" -type "double3" 0.73310799999999965 5.3145129999999989 22.646742999999997 ;
	setAttr ".s" -type "double3" 1 0.99999999999999989 0.99999999999999989 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" 0.33942 -0.13520399999999999 -0.93086800000000003 0
		 -0.93680799999999997 0.040590800000000003 -0.34748099999999998 0 0.084765400000000005 0.98998600000000003 -0.112883 0
		 57.470143999999998 0.415574 99.821611000000004 1;
	setAttr -k on ".liw";
	setAttr ".fbxID" 5;
createNode joint -n "Bip01_L_Finger11" -p "|Obese_Sit_Idle_1|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_L_Clavicle|Bip01_L_UpperArm|Bip01_L_Forearm|Bip01_L_Hand|Bip01_L_Finger1";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".t" -type "double3" 3.5597999999999996 0 0 ;
	setAttr ".r" -type "double3" -2.0000000000000012e-006 3.0000000000000005e-006 22.627677000000009 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" -0.047135499999999997 -0.10918 -0.99290400000000001 0
		 -0.99528499999999998 0.089479600000000006 0.037409299999999999 0 0.0847604 0.98998699999999995 -0.112883 0
		 58.678319000000002 -0.065726099999999996 96.507845000000003 1;
	setAttr -k on ".liw";
	setAttr ".fbxID" 5;
createNode joint -n "_notused_Bip01_L_Finger1Nub" -p "|Obese_Sit_Idle_1|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_L_Clavicle|Bip01_L_UpperArm|Bip01_L_Forearm|Bip01_L_Hand|Bip01_L_Finger1|Bip01_L_Finger11";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".t" -type "double3" 4.1404000000000005 0 0 ;
	setAttr ".ssc" no;
	setAttr ".fbxID" 5;
createNode joint -n "Bip01_L_Finger3" -p "|Obese_Sit_Idle_1|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_L_Clavicle|Bip01_L_UpperArm|Bip01_L_Forearm|Bip01_L_Hand";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".t" -type "double3" 7.2345 -0.5997 0.9332 ;
	setAttr ".r" -type "double3" -0.043362000000000123 -3.826458 18.509666999999997 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" 0.41327900000000001 0.021958700000000001 -0.91034000000000004 0
		 -0.91051400000000005 0.0239966 -0.41277999999999998 0 0.012781000000000001 0.999471 0.029911 0
		 57.824314000000001 3.6238670000000002 100.055049 1;
	setAttr -k on ".liw";
	setAttr ".fbxID" 5;
createNode joint -n "Bip01_L_Finger31" -p "|Obese_Sit_Idle_1|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_L_Clavicle|Bip01_L_UpperArm|Bip01_L_Forearm|Bip01_L_Hand|Bip01_L_Finger3";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".t" -type "double3" 3.7241 0 0 ;
	setAttr ".r" -type "double3" 2e-006 -9.4787915988669323e-023 27.984288 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" -0.062282200000000003 0.030651100000000001 -0.99758800000000003 0
		 -0.99797599999999997 0.0108933 0.062641199999999994 0 0.012787099999999999 0.999471 0.029910599999999999 0
		 59.363363 3.7056490000000002 96.664751999999993 1;
	setAttr -k on ".liw";
	setAttr ".fbxID" 5;
createNode joint -n "_notused_Bip01_L_Finger3Nub" -p "|Obese_Sit_Idle_1|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_L_Clavicle|Bip01_L_UpperArm|Bip01_L_Forearm|Bip01_L_Hand|Bip01_L_Finger3|Bip01_L_Finger31";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".t" -type "double3" 4.2397 0 0 ;
	setAttr ".ssc" no;
	setAttr ".fbxID" 5;
createNode joint -n "Bip01_L_Finger4" -p "|Obese_Sit_Idle_1|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_L_Clavicle|Bip01_L_UpperArm|Bip01_L_Forearm|Bip01_L_Hand";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".t" -type "double3" 7.2292 -0.4187 2.3639 ;
	setAttr ".r" -type "double3" 0.17713199999999998 -5.5619539999999983 16.615218000000002 ;
	setAttr ".s" -type "double3" 1 0.99999999999999989 1 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" 0.44323200000000001 0.051427599999999997 -0.89493100000000003 0
		 -0.89640600000000004 0.026343800000000001 -0.44244899999999998 0 0.00082181400000000003 0.99832900000000002 0.057776399999999999 0
		 57.746775 5.054627 99.891990000000007 1;
	setAttr -k on ".liw";
	setAttr ".fbxID" 5;
createNode joint -n "Bip01_L_Finger41" -p "|Obese_Sit_Idle_1|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_L_Clavicle|Bip01_L_UpperArm|Bip01_L_Forearm|Bip01_L_Hand|Bip01_L_Finger4";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".t" -type "double3" 3.4791000000000003 0 0 ;
	setAttr ".r" -type "double3" 2e-006 -3.0000000000000009e-006 29.444363999999997 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" -0.054672800000000001 0.057735500000000002 -0.996834 0
		 -0.99850399999999995 -0.00234096 0.054628799999999998 0 0.00082047800000000003 0.99832900000000002 0.057777200000000001 0
		 59.288862000000002 5.2335589999999996 96.778389000000004 1;
	setAttr -k on ".liw";
	setAttr ".fbxID" 5;
createNode joint -n "_notused_Bip01_L_Finger4Nub" -p "|Obese_Sit_Idle_1|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_L_Clavicle|Bip01_L_UpperArm|Bip01_L_Forearm|Bip01_L_Hand|Bip01_L_Finger4|Bip01_L_Finger41";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".t" -type "double3" 3.3974 0 0 ;
	setAttr ".ssc" no;
	setAttr ".fbxID" 5;
createNode joint -n "Bip01_L_Thigh" -p "|Obese_Sit_Idle_1|Bip01_Spine";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".t" -type "double3" -11.0456 2.2584 8.5201999999999991 ;
	setAttr ".s" -type "double3" 1 1 0.99999999999999989 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" 0.059694200000000003 0.0067868399999999997 -0.998193 0
		 0.0013515700000000001 -0.99997599999999998 -0.00671814 0 -0.99821599999999999 -0.00094809699999999996 -0.059701999999999998 0
		 8.5202019999999994 4.3064900000000002e-005 88.396292000000003 1;
	setAttr -k on ".liw";
	setAttr ".fbxID" 5;
createNode joint -n "Bip01_L_Calf" -p "|Obese_Sit_Idle_1|Bip01_Spine|Bip01_L_Thigh";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".t" -type "double3" 36.1527 0 0 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" 0.059157700000000001 0.119882 -0.99102400000000002 0
		 0.0080950799999999993 -0.99278699999999998 -0.119612 0 -0.99821599999999999 -0.00094644599999999998 -0.059701400000000002 0
		 10.678299000000001 0.245397 52.308965999999998 1;
	setAttr -k on ".liw";
	setAttr ".fbxID" 5;
createNode joint -n "Bip01_L_Foot" -p "|Obese_Sit_Idle_1|Bip01_Spine|Bip01_L_Thigh|Bip01_L_Calf";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".t" -type "double3" 43.2543 0 0 ;
	setAttr ".s" -type "double3" 1 0.99999999999999989 1 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" 0 0 -1 0 0.123471 -0.99234800000000001 0 0 -0.99234800000000001 -0.123471 0 0
		 13.237131 5.4308170000000002 9.4429110000000005 1;
	setAttr -k on ".liw";
	setAttr ".fbxID" 5;
createNode joint -n "Bip01_L_Toe0" -p "|Obese_Sit_Idle_1|Bip01_Spine|Bip01_L_Thigh|Bip01_L_Calf|Bip01_L_Foot";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".t" -type "double3" 9.4714999999999989 11.2377 0 ;
	setAttr ".s" -type "double3" 0.99999999999999989 0.99999999999999989 1 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" 0.123471 -0.99234800000000001 0 0 0 0 1 0 -0.99234800000000001 -0.123471 0 0
		 14.624665999999999 -5.7209120000000002 -0.028559000000000001 1;
	setAttr -k on ".liw";
	setAttr ".fbxID" 5;
createNode joint -n "_notused_Bip01_L_Toe0Nub" -p "|Obese_Sit_Idle_1|Bip01_Spine|Bip01_L_Thigh|Bip01_L_Calf|Bip01_L_Foot|Bip01_L_Toe0";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".t" -type "double3" 7.2685 0 0 ;
	setAttr ".r" -type "double3" 0 0 180 ;
	setAttr ".ssc" no;
	setAttr ".fbxID" 5;
createNode joint -n "Bip01_R_Thigh" -p "|Obese_Sit_Idle_1|Bip01_Spine";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".t" -type "double3" -11.0456 2.2585 -8.5201999999999991 ;
	setAttr ".s" -type "double3" 1.0000000000000002 1 1.0000000000000002 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" -0.059694200000000003 0.0067826700000000002 -0.998193 0
		 -0.00134545 -0.99997599999999998 -0.0067143300000000001 0 -0.99821599999999999 0.00094221699999999997 0.059701900000000002 0
		 -8.5201879999999992 1.40303e-005 88.396298000000002 1;
	setAttr -k on ".liw";
	setAttr ".fbxID" 5;
createNode joint -n "Bip01_R_Calf" -p "|Obese_Sit_Idle_1|Bip01_Spine|Bip01_R_Thigh";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".t" -type "double3" 36.1527 0 0 ;
	setAttr ".s" -type "double3" 0.99999999999999989 1 0.99999999999999989 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" -0.059158000000000002 0.11988699999999999 -0.99102400000000002 0
		 -0.0080972100000000005 -0.99278699999999998 -0.119617 0 -0.99821599999999999 0.00094822899999999998 0.059701999999999998 0
		 -10.678288 0.24518599999999999 52.308996999999998 1;
	setAttr -k on ".liw";
	setAttr ".fbxID" 5;
createNode joint -n "Bip01_R_Foot" -p "|Obese_Sit_Idle_1|Bip01_Spine|Bip01_R_Thigh|Bip01_R_Calf";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".t" -type "double3" 43.2543 0 0 ;
	setAttr ".s" -type "double3" 1 1 0.99999999999999989 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" 0 0 -1 0 -0.123469 -0.99234800000000001 0 0 -0.99234800000000001 0.123469 0 0
		 -13.237124 5.4308209999999999 9.442933 1;
	setAttr -k on ".liw";
	setAttr ".fbxID" 5;
createNode joint -n "Bip01_R_Toe0" -p "|Obese_Sit_Idle_1|Bip01_Spine|Bip01_R_Thigh|Bip01_R_Calf|Bip01_R_Foot";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".t" -type "double3" 9.4714999999999989 11.2377 0 ;
	setAttr ".s" -type "double3" 0.99999999999999989 0.99999999999999989 1 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" -0.12346799999999999 -0.99234800000000001 0 0 0 0 1 0
		 -0.99234800000000001 0.12346799999999999 0 0 -14.624625999999999 -5.7209019999999997 -0.028538999999999998 1;
	setAttr -k on ".liw";
	setAttr ".fbxID" 5;
createNode joint -n "_notused_Bip01_R_Toe0Nub" -p "|Obese_Sit_Idle_1|Bip01_Spine|Bip01_R_Thigh|Bip01_R_Calf|Bip01_R_Foot|Bip01_R_Toe0";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".t" -type "double3" 7.2685 0 0 ;
	setAttr ".ssc" no;
	setAttr ".fbxID" 5;
createNode animCurveTL -n "Bip01_Spine_translateX11";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 92 ".ktv[0:91]"  0 -8.0409564971923828 0.8 -8.0071372985839844
		 1.6 -7.9769582748413086 2.4 -7.9467644691467276 3.2 -7.9161362648010254 4 -7.888725757598877
		 4.8 -7.8657870292663565 5.6 -7.8421335220336914 6.4 -7.8188014030456543 7.2 -7.8028879165649405
		 8 -7.791254997253418 8.8 -7.7782993316650382 9.6 -7.76627492904663 10.4 -7.7502241134643555
		 11.2 -7.7246160507202148 12 -7.7000079154968253 12.8 -7.6842970848083487 13.6 -7.67230224609375
		 14.4 -7.6582064628601074 15.2 -7.6416354179382324 16 -7.6247081756591788 16.8 -7.6092805862426749
		 17.6 -7.604633331298829 18.4 -7.5961241722106925 19.2 -7.5810184478759757 20 -7.5658292770385742
		 20.8 -7.5616145133972159 21.6 -7.5595588684082031 22.4 -7.5503482818603516 23.2 -7.5406427383422852
		 24 -7.5321025848388672 24.8 -7.5181550979614258 25.6 -7.4995222091674796 26.4 -7.4823164939880371
		 27.2 -7.4688854217529297 28 -7.4651751518249503 28.8 -7.4497628211975107 29.6 -7.4355306625366211
		 30.4 -7.4243650436401358 31.2 -7.4140429496765137 32 -7.4062066078186035 32.8 -7.4021492004394531
		 33.6 -7.3955984115600595 34.4 -7.3855533599853516 35.2 -7.3790912628173828 36 -7.3760271072387695
		 36.8 -7.3731551170349121 37.6 -7.3719897270202637 38.4 -7.3716521263122559 39.2 -7.376746177673339
		 40 -7.395352840423584 40.8 -7.4194722175598145 41.6 -7.4395689964294434 42.4 -7.4605650901794434
		 43.2 -7.4872183799743652 44 -7.518064022064209 44.8 -7.5502443313598642 45.6 -7.580285072326661
		 46.4 -7.6087865829467773 47.2 -7.6403646469116211 48 -7.6777634620666495 48.8 -7.7256464958190909
		 49.6 -7.7836432456970206 50.4 -7.8379735946655282 51.2 -7.8837356567382812 52 -7.9282970428466797
		 52.8 -7.9705266952514657 53.6 -8.011570930480957 54.4 -8.0616931915283203 55.2 -8.1218223571777344
		 56 -8.1845626831054687 56.8 -8.2482099533081055 57.6 -8.3146762847900391 58.4 -8.3833150863647461
		 59.2 -8.453282356262207 60 -8.5334939956665039 60.8 -8.5961570739746094 61.6 -8.6507472991943359
		 62.4 -8.7111005783081055 63.2 -8.7677545547485352 64 -8.8282432556152344 64.8 -8.8904428482055664
		 65.6 -8.9487810134887695 66.4 -9.0015153884887695 67.2 -9.0554342269897461 68 -9.1047687530517578
		 68.8 -9.1541576385498047 69.6 -9.2051973342895508 70.4 -9.2438611984252912 71.2 -9.2668952941894531
		 72 -9.2823486328125 72.8 -9.3074350357055664;
createNode animCurveTL -n "Bip01_Spine_translateY11";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 92 ".ktv[0:91]"  0 65.614463806152344 0.8 65.601104736328125
		 1.6 65.589630126953125 2.4 65.574378967285156 3.2 65.562644958496094 4 65.555244445800781
		 4.8 65.544425964355469 5.6 65.528060913085938 6.4 65.507781982421875 7.2 65.486228942871094
		 8 65.467903137207031 8.8 65.455093383789063 9.6 65.444511413574219 10.4 65.432876586914063
		 11.2 65.420761108398438 12 65.405426025390625 12.8 65.385459899902344 13.6 65.367660522460938
		 14.4 65.349983215332031 15.2 65.32373046875 16 65.294349670410156 16.8 65.270477294921875
		 17.6 65.251380920410156 18.4 65.235374450683594 19.2 65.215347290039063 20 65.189300537109375
		 20.8 65.160934448242188 21.6 65.132743835449219 22.4 65.106437683105469 23.2 65.085296630859375
		 24 65.069488525390625 24.8 65.056694030761719 25.6 65.047660827636719 26.4 65.038581848144531
		 27.2 65.024383544921875 28 65.010368347167969 28.8 65.000289916992188 29.6 64.992805480957031
		 30.4 64.983963012695312 31.2 64.969757080078125 32 64.961456298828125 32.8 64.967529296875
		 33.6 64.968490600585938 34.4 64.952674865722656 35.2 64.935226440429687 36 64.925224304199219
		 36.8 64.918411254882813 37.6 64.918586730957031 38.4 64.931037902832031 39.2 64.944168090820313
		 40 64.948158264160156 40.8 64.949111938476563 41.6 64.952560424804688 42.4 64.958320617675781
		 43.2 64.965965270996094 44 64.975593566894531 44.8 64.985481262207031 45.6 64.993522644042969
		 46.4 65.001953125 47.2 65.014801025390625 48 65.029914855957031 48.8 65.040122985839844
		 49.6 65.047752380371094 50.4 65.060111999511719 51.2 65.073013305664063 52 65.082221984863281
		 52.8 65.091194152832031 53.6 65.102622985839844 54.4 65.118545532226562 55.2 65.135490417480469
		 56 65.146720886230469 56.8 65.155029296875 57.6 65.167434692382812 58.4 65.185234069824219
		 59.2 65.203887939453125 60 65.213829040527344 60.8 65.215713500976562 61.6 65.219306945800781
		 62.4 65.234062194824219 63.2 65.250938415527344 64 65.255905151367188 64.8 65.25885009765625
		 65.6 65.270530700683594 66.4 65.283210754394531 67.2 65.28814697265625 68 65.29095458984375
		 68.8 65.297203063964844 69.6 65.305679321289063 70.4 65.311759948730469 71.2 65.316352844238281
		 72 65.324440002441406 72.8 65.3333740234375;
createNode animCurveTL -n "Bip01_Spine_translateZ11";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 92 ".ktv[0:91]"  0 -48.586235046386719 0.8 -48.583766937255859
		 1.6 -48.584285736083984 2.4 -48.591167449951165 3.2 -48.603694915771491 4 -48.617942810058594
		 4.8 -48.629371643066406 5.6 -48.632888793945313 6.4 -48.632389068603523 7.2 -48.636978149414063
		 8 -48.647651672363281 8.8 -48.660541534423821 9.6 -48.675041198730469 10.4 -48.689678192138672
		 11.2 -48.702426910400391 12 -48.715274810791016 12.8 -48.731391906738281 13.6 -48.752460479736328
		 14.4 -48.769992828369141 15.2 -48.775840759277344 16 -48.781288146972656 16.8 -48.795886993408203
		 17.6 -48.814128875732422 18.4 -48.826126098632812 19.2 -48.837039947509766 20 -48.852649688720703
		 20.8 -48.867931365966797 21.6 -48.874134063720703 22.4 -48.875602722167969 23.2 -48.879997253417969
		 24 -48.887718200683594 24.8 -48.897697448730469 25.6 -48.908992767333984 26.4 -48.921760559082031
		 27.2 -48.936660766601563 28 -48.946491241455078 28.8 -48.956912994384766 29.6 -48.966228485107422
		 30.4 -48.976467132568359 31.2 -48.983558654785149 32 -48.98468017578125 32.8 -48.983871459960938
		 33.6 -48.984901428222656 34.4 -48.988758087158203 35.2 -48.994190216064453 36 -48.995388031005859
		 36.8 -48.993484497070313 37.6 -48.997821807861328 38.4 -49.00653076171875 39.2 -49.008968353271491
		 40 -49.002628326416016 40.8 -48.993534088134766 41.6 -48.987659454345703 42.4 -48.985557556152344
		 43.2 -48.984333038330078 44 -48.983718872070312 44.8 -48.985050201416016 45.6 -48.986118316650391
		 46.4 -48.983287811279297 47.2 -48.977550506591797 48 -48.972385406494141 48.8 -48.967018127441406
		 49.6 -48.961765289306641 50.4 -48.959300994873047 51.2 -48.956233978271491 52 -48.949417114257813
		 52.8 -48.941669464111328 53.6 -48.934078216552734 54.4 -48.924686431884766 55.2 -48.915367126464851
		 56 -48.910320281982422 56.8 -48.906982421875 57.6 -48.900547027587891 58.4 -48.892402648925781
		 59.2 -48.883083343505859 60 -48.870090484619141 60.8 -48.8524169921875 61.6 -48.834373474121094
		 62.4 -48.821277618408203 63.2 -48.809043884277344 64 -48.791358947753906 64.8 -48.770687103271491
		 65.6 -48.752834320068359 66.4 -48.736404418945313 67.2 -48.715980529785149 68 -48.693515777587891
		 68.8 -48.675594329833984 69.6 -48.660442352294922 70.4 -48.643692016601563 71.2 -48.616504669189453
		 72 -48.586532592773438 72.8 -48.555809020996094;
createNode animCurveTA -n "Bip01_Spine_rotateX11";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 92 ".ktv[0:91]"  0 7.1810674667358398 0.8 7.1739225387573242
		 1.6 7.1792073249816895 2.4 7.1902003288269052 3.2 7.1984257698059073 4 7.2051048278808603
		 4.8 7.2123231887817365 5.6 7.2212743759155273 6.4 7.2295627593994141 7.2 7.2373013496398899
		 8 7.2563729286193857 8.8 7.2870659828186035 9.6 7.3172812461853027 10.4 7.3501439094543466
		 11.2 7.3908705711364755 12 7.4334635734558123 12.8 7.4769845008850098 13.6 7.5234479904174796
		 14.4 7.568396568298339 15.2 7.6079568862915039 16 7.6412940025329599 16.8 7.6655883789062509
		 17.6 7.6887059211730957 18.4 7.705172538757326 19.2 7.7301678657531747 20 7.7595963478088388
		 20.8 7.780141830444335 21.6 7.7891755104064941 22.4 7.7954249382019052 23.2 7.7952876091003427
		 24 7.787571907043457 24.8 7.787177562713623 25.6 7.7906193733215323 26.4 7.7792725563049316
		 27.2 7.7591133117675772 28 7.749742031097413 28.8 7.7410745620727548 29.6 7.7276959419250479
		 30.4 7.7156410217285156 31.2 7.711329460144043 32 7.7050795555114764 32.8 7.6913723945617667
		 33.6 7.6818952560424814 34.4 7.6810979843139648 35.2 7.6845445632934579 36 7.6951289176940909
		 36.8 7.7138881683349618 37.6 7.7320103645324707 38.4 7.744966983795166 39.2 7.7554736137390146
		 40 7.7653493881225586 40.8 7.7825136184692392 41.6 7.8121361732482892 42.4 7.8431930541992205
		 43.2 7.8687644004821777 44 7.8943400382995588 44.8 7.9243903160095215 45.6 7.959895133972168
		 46.4 7.9966330528259277 47.2 8.0280961990356445 48 8.0536203384399414 48.8 8.0750150680541992
		 49.6 8.0952463150024414 50.4 8.1178503036499023 51.2 8.1421308517456055 52 8.1681957244873047
		 52.8 8.2002229690551758 53.6 8.2376899719238281 54.4 8.2743873596191406 55.2 8.3019685745239258
		 56 8.3126211166381836 56.8 8.3152399063110369 57.6 8.3280982971191406 58.4 8.3497982025146484
		 59.2 8.367706298828125 60 8.3874387741088867 60.8 8.3909416198730469 61.6 8.3882894515991211
		 62.4 8.3606023788452148 63.2 8.3587913513183594 64 8.3627586364746094 64.8 8.3713865280151367
		 65.6 8.3827619552612305 66.4 8.3901567459106445 67.2 8.4059286117553711 68 8.39971923828125
		 68.8 8.391265869140625 69.6 8.3884754180908203 70.4 8.3941278457641602 71.2 8.4080877304077148
		 72 8.4201974868774414 72.8 8.4304447174072266;
createNode animCurveTA -n "Bip01_Spine_rotateY11";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 92 ".ktv[0:91]"  0 4.8403286933898926 0.8 4.7146987915039062
		 1.6 4.5920577049255371 2.4 4.4739255905151367 3.2 4.361140251159668 4 4.250885009765625
		 4.8 4.1436028480529785 5.6 4.0455007553100586 6.4 3.9519524574279785 7.2 3.8550550937652592
		 8 3.7629020214080802 8.8 3.6832377910614009 9.6 3.6150064468383789 10.4 3.560908317565918
		 11.2 3.5192325115203857 12 3.4793024063110352 12.8 3.4396688938140869 13.6 3.4071938991546631
		 14.4 3.386082649230957 15.2 3.3784146308898926 16 3.3777153491973877 16.8 3.3726367950439453
		 17.6 3.3816344738006592 18.4 3.3769774436950684 19.2 3.3975260257720947 20 3.4351205825805664
		 20.8 3.4654567241668701 21.6 3.4882841110229492 22.4 3.5162031650543213 23.2 3.5504708290100098
		 24 3.5912671089172359 24.8 3.6419074535369882 25.6 3.7024588584899902 26.4 3.7649080753326416
		 27.2 3.814692497253418 28 3.8898799419403067 28.8 3.9390280246734615 29.6 4.0020427703857422
		 30.4 4.0686178207397461 31.2 4.1327400207519531 32 4.1944489479064941 32.8 4.2580075263977051
		 33.6 4.3244514465332031 34.4 4.3868861198425293 35.2 4.4425621032714844 36 4.4997611045837402
		 36.8 4.563117504119873 37.6 4.6281571388244629 38.4 4.6934852600097656 39.2 4.7554678916931152
		 40 4.8095688819885254 40.8 4.862215518951416 41.6 4.9170293807983398 42.4 4.9686846733093262
		 43.2 5.0170817375183105 44 5.0672006607055664 44.8 5.120391845703125 45.6 5.1773777008056641
		 46.4 5.2398571968078613 47.2 5.3053836822509766 48 5.3714323043823251 48.8 5.4317436218261719
		 49.6 5.4817519187927246 50.4 5.5325250625610352 51.2 5.5913662910461426 52 5.6530313491821289
		 52.8 5.7169380187988281 53.6 5.7857871055603027 54.4 5.8575754165649414 55.2 5.9259614944458008
		 56 5.9876871109008789 56.8 6.0476880073547363 57.6 6.112541675567627 58.4 6.1833224296569824
		 59.2 6.2489852905273437 60 6.3414354324340829 60.8 6.3895893096923828 61.6 6.4377293586730957
		 62.4 6.5152182579040527 63.2 6.5652523040771484 64 6.6208481788635254 64.8 6.6759309768676758
		 65.6 6.7324447631835938 66.4 6.7841839790344238 67.2 6.8344025611877441 68 6.8711686134338379
		 68.8 6.9038710594177246 69.6 6.9416847229003906 70.4 6.9810943603515625 71.2 7.0276446342468262
		 72 7.0599651336669931 72.8 7.1072554588317871;
createNode animCurveTA -n "Bip01_Spine_rotateZ11";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 92 ".ktv[0:91]"  0 -3.2140631675720215 0.8 -3.2073101997375488
		 1.6 -3.1967241764068604 2.4 -3.1871531009674072 3.2 -3.178687572479248 4 -3.168149471282959
		 4.8 -3.1535599231719971 5.6 -3.1373622417449951 6.4 -3.1245131492614746 7.2 -3.1164042949676514
		 8 -3.1052203178405762 8.8 -3.0824804306030273 9.6 -3.055708646774292 10.4 -3.0416409969329834
		 11.2 -3.041511058807373 12 -3.0367114543914795 12.8 -3.0177140235900879 13.6 -2.9991159439086914
		 14.4 -2.9913053512573242 15.2 -2.9896483421325688 16 -2.9883427619934082 16.8 -2.9855830669403076
		 17.6 -2.9935550689697266 18.4 -3.0006053447723389 19.2 -3.0091428756713867 20 -3.0196356773376465
		 20.8 -3.0315930843353271 21.6 -3.0406229496002197 22.4 -3.0474720001220703 23.2 -3.0586197376251221
		 24 -3.0752644538879395 24.8 -3.0959017276763916 25.6 -3.119586706161499 26.4 -3.1339571475982666
		 27.2 -3.1330466270446777 28 -3.1168892383575439 28.8 -3.1259968280792241 29.6 -3.1349184513092041
		 30.4 -3.1360528469085693 31.2 -3.1287107467651367 32 -3.1196942329406734 32.8 -3.1145930290222168
		 33.6 -3.1051890850067139 34.4 -3.0870954990386963 35.2 -3.0675227642059326 36 -3.0459904670715332
		 36.8 -3.0176427364349365 37.6 -2.9879357814788814 38.4 -2.9643900394439697 39.2 -2.9431924819946289
		 40 -2.9189026355743408 40.8 -2.8924601078033447 41.6 -2.864493608474731 42.4 -2.8351531028747559
		 43.2 -2.8043653964996338 44 -2.7734296321868896 44.8 -2.7456462383270264 45.6 -2.7186546325683594
		 46.4 -2.6893675327301025 47.2 -2.661796092987061 48 -2.638131856918335 48.8 -2.6149559020996103
		 49.6 -2.5915162563323975 50.4 -2.5701866149902344 51.2 -2.5519628524780273 52 -2.5384914875030522
		 52.8 -2.5275218486785889 53.6 -2.508825302124023 54.4 -2.4828813076019287 55.2 -2.4627752304077148
		 56 -2.4494996070861816 56.8 -2.4349484443664551 57.6 -2.4211254119873047 58.4 -2.4163987636566162
		 59.2 -2.418036937713623 60 -2.4132437705993652 60.8 -2.3910260200500488 61.6 -2.367727518081665
		 62.4 -2.3577253818511963 63.2 -2.3512639999389648 64 -2.3406052589416504 64.8 -2.322998046875
		 65.6 -2.303851842880249 66.4 -2.2904245853424077 67.2 -2.2765035629272461 68 -2.2594575881958008
		 68.8 -2.2406289577484131 69.6 -2.2093446254730225 70.4 -2.1814706325531006 71.2 -2.1498861312866211
		 72 -2.1253871917724609 72.8 -2.0805351734161377;
createNode animCurveTA -n "Bip01_Spine1_rotateX11";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 92 ".ktv[0:91]"  0 0.16194331645965576 0.8 0.1603233814239502
		 1.6 0.1594451367855072 2.4 0.15580978989601135 3.2 0.15462177991867063 4 0.15053494274616239
		 4.8 0.14611408114433289 5.6 0.14480939507484436 6.4 0.14042077958583832 7.2 0.13650064170360565
		 8 0.13550788164138794 8.8 0.13262417912483215 9.6 0.13078434765338898 10.4 0.12983356416225433
		 11.2 0.12978427112102509 12 0.13001427054405212 12.8 0.13156463205814362 13.6 0.13378360867500305
		 14.4 0.13678257167339325 15.2 0.14063610136508942 16 0.14529402554035187 16.8 0.15100868046283722
		 17.6 0.15846918523311615 18.4 0.16085721552371979 19.2 0.16925376653671265 20 0.17737710475921631
		 20.8 0.18692316114902499 21.6 0.19032979011535645 22.4 0.20207251608371735 23.2 0.20547419786453247
		 24 0.21548934280872345 24.8 0.22522032260894773 25.6 0.22790563106536868 26.4 0.23691640794277191
		 27.2 0.24538354575634003 28 0.24800986051559448 28.8 0.25687095522880554 29.6 0.26067522168159485
		 30.4 0.26478579640388489 31.2 0.26783880591392523 32 0.27250146865844727 32.8 0.2754109799861908
		 33.6 0.27743890881538391 34.4 0.27886864542961121 35.2 0.27976608276367188 36 0.28014957904815679
		 36.8 0.28000819683074951 37.6 0.27985754609107971 38.4 0.27908521890640259 39.2 0.27824947237968445
		 40 0.27732035517692566 40.8 0.27633127570152283 41.6 0.27535814046859741 42.4 0.27512845396995544
		 43.2 0.2746712863445282 44 0.27473348379135132 44.8 0.27535480260848999 45.6 0.2766968309879303
		 46.4 0.27723723649978638 47.2 0.27960395812988281 48 0.28236386179924011 48.8 0.28602814674377447
		 49.6 0.28724348545074463 50.4 0.29198905825614929 51.2 0.29732763767242432 52 0.29893198609352112
		 52.8 0.30446234345436096 53.6 0.3093528151512146 54.4 0.31412136554718018 55.2 0.31922078132629395
		 56 0.32515925168991089 56.8 0.32722398638725281 57.6 0.33422327041625977 58.4 0.33648309111595154
		 59.2 0.3432641327381134 60 0.3463098406791687 60.8 0.34993276000022888 61.6 0.35359656810760498
		 62.4 0.35680988430976868 63.2 0.35918480157852173 64 0.3635537326335907 64.8 0.3652410507202149
		 65.6 0.36701497435569758 66.4 0.3683761358261109 67.2 0.37050560116767878 68 0.37097671627998352
		 68.8 0.37119981646537786 69.6 0.37106087803840643 70.4 0.36931979656219482 71.2 0.36777395009994501
		 72 0.36551216244697565 72.8 0.36347338557243342;
createNode animCurveTA -n "Bip01_Spine1_rotateY11";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 92 ".ktv[0:91]"  0 -0.2495304346084595 0.8 -0.23884342610836029
		 1.6 -0.23530222475528717 2.4 -0.22372525930404663 3.2 -0.22052405774593356 4 -0.21198378503322604
		 4.8 -0.20470964908599856 5.6 -0.20291891694068909 6.4 -0.19761696457862857 7.2 -0.1934700608253479
		 8 -0.19252121448516851 8.8 -0.18993102014064789 9.6 -0.188187375664711 10.4 -0.18674591183662417
		 11.2 -0.18514581024646759 12 -0.18464429676532745 12.8 -0.1827625036239624 13.6 -0.18073716759681704
		 14.4 -0.17829509079456329 15.2 -0.17519402503967285 16 -0.17127703130245209 16.8 -0.16596519947052002
		 17.6 -0.1582719087600708 18.4 -0.15562902390956881 19.2 -0.14562076330184937 20 -0.13479386270046237
		 20.8 -0.12057232111692429 21.6 -0.1149308681488037 22.4 -0.094209030270576477 23.2 -0.087786830961704254
		 24 -0.06655707955360414 24.8 -0.04333992674946785 25.6 -0.0362679623067379 26.4 -0.010046170093119144
		 27.2 0.017829243093729019 28 0.027587829157710075 28.8 0.063370078802108779 29.6 0.08114488422870636
		 30.4 0.10221075266599657 31.2 0.11890660971403122 32 0.15137720108032229 32.8 0.1778438538312912
		 33.6 0.20266093313694 34.4 0.22717852890491486 35.2 0.25120913982391357 36 0.27593028545379639
		 36.8 0.30386039614677429 37.6 0.31195405125617981 38.4 0.33782044053077698 39.2 0.35836640000343323
		 40 0.37617301940917969 40.8 0.39342421293258661 41.6 0.41175514459609985 42.4 0.41678044199943542
		 43.2 0.43187546730041504 44 0.44269451498985291 44.8 0.45110562443733215 45.6 0.45849493145942682
		 46.4 0.46016570925712585 47.2 0.46402588486671448 48 0.46518760919570917 48.8 0.46388426423072815
		 49.6 0.46297550201416016 50.4 0.45775637030601501 51.2 0.44988885521888727 52 0.44707590341567993
		 52.8 0.43606078624725342 53.6 0.42485147714614863 54.4 0.41257062554359442 55.2 0.39780312776565552
		 56 0.3785586953163147 56.8 0.37119132280349726 57.6 0.34452041983604431 58.4 0.33501303195953369
		 59.2 0.3033510148525238 60 0.28727912902832031 60.8 0.266498863697052 61.6 0.24363233149051669
		 62.4 0.22158262133598328 63.2 0.20354324579238892 64 0.16316547989845276 64.8 0.14412516355514526
		 65.6 0.12174716591835022 66.4 0.10248497873544692 67.2 0.060486499220132842 68 0.040998801589012146
		 68.8 0.018218241631984711 69.6 -0.0012966492213308811 70.4 -0.043358907103538513
		 71.2 -0.062554553151130676 72 -0.084856249392032623 72.8 -0.10232252627611162;
createNode animCurveTA -n "Bip01_Spine1_rotateZ11";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 92 ".ktv[0:91]"  0 0.76914036273956299 0.8 0.76256829500198364
		 1.6 0.76006835699081421 2.4 0.7510913610458374 3.2 0.74835562705993663 4 0.73967397212982178
		 4.8 0.73065155744552612 5.6 0.7280353307723999 6.4 0.71898370981216431 7.2 0.710246741771698
		 8 0.70783162117004395 8.8 0.70001441240310669 9.6 0.69374829530715942 10.4 0.68811696767807007
		 11.2 0.68234401941299438 12 0.68082731962203979 12.8 0.67662376165390015 13.6 0.67403727769851685
		 14.4 0.67262828350067139 15.2 0.67221879959106445 16 0.67288863658905029 16.8 0.67487555742263794
		 17.6 0.67866843938827515 18.4 0.68013203144073486 19.2 0.68608850240707397 20 0.69283753633499146
		 20.8 0.70177686214447021 21.6 0.70526993274688721 22.4 0.71791452169418335 23.2 0.72177654504776001
		 24 0.73414355516433727 24.8 0.74713695049285889 25.6 0.75095075368881237 26.4 0.76435136795043956
		 27.2 0.77762556076049805 28 0.78190779685974132 28.8 0.79678255319595348 29.6 0.80346381664276123
		 30.4 0.81092244386672974 31.2 0.8165861964225769 32 0.82624685764312733 32.8 0.83312869071960449
		 33.6 0.83860051631927501 34.4 0.84314972162246704 35.2 0.84681051969528187 36 0.84963458776473999
		 36.8 0.85189217329025269 37.6 0.85235893726348877 38.4 0.85332971811294556 39.2 0.85349345207214355
		 40 0.8531230092048645 40.8 0.85228270292282116 41.6 0.85088074207305908 42.4 0.85040968656539917
		 43.2 0.84883272647857666 44 0.84772264957427979 44.8 0.84711563587188721 45.6 0.84708410501480091
		 46.4 0.84723192453384399 47.2 0.84821444749832164 48 0.84995204210281372 48.8 0.85317724943161011
		 49.6 0.85445702075958252 50.4 0.8602297306060791 51.2 0.86764657497405995 52 0.87013214826583862
		 52.8 0.87963128089904785 53.6 0.88918411731719971 54.4 0.89965260028839089 55.2 0.91239959001541149
		 56 0.92937260866165183 56.8 0.93601441383361839 57.6 0.96031022071838368 58.4 0.96910548210144043
		 59.2 0.99892073869705211 60 1.0144959688186646 60.8 1.0349525213241575 61.6 1.0578300952911377
		 62.4 1.0802075862884519 63.2 1.0986905097961426 64 1.1404205560684204 64.8 1.1600979566574099
		 65.6 1.1832231283187866 66.4 1.2030586004257202 67.2 1.2458316087722778 68 1.2650429010391238
		 68.8 1.2870409488677983 69.6 1.3053915500640867 70.4 1.3426532745361328 71.2 1.3583555221557615
		 72 1.375696063041687 72.8 1.3887434005737305;
createNode animCurveTA -n "Bip01_Spine2_rotateX11";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 92 ".ktv[0:91]"  0 0.52684211730957031 0.8 0.52153891324996948
		 1.6 0.51743960380554199 2.4 0.51313376426696777 3.2 0.5017356276512146 4 0.49044495820999146
		 4.8 0.47610956430435186 5.6 0.46916779875755305 6.4 0.46227329969406128 7.2 0.44896805286407465
		 8 0.43931153416633606 8.8 0.43071359395980829 9.6 0.42864552140235906 10.4 0.42402040958404547
		 11.2 0.42347666621208191 12 0.42544540762901306 12.8 0.43081104755401606 13.6 0.44155272841453552
		 14.4 0.44959676265716536 15.2 0.46091753244400024 16 0.47067785263061512 16.8 0.49468657374382019
		 17.6 0.52228879928588867 18.4 0.5376286506652832 19.2 0.55937588214874268 20 0.58376574516296387
		 20.8 0.60928988456726074 21.6 0.63556414842605591 22.4 0.66211438179016113 23.2 0.68856477737426758
		 24 0.71467477083206177 24.8 0.740184485912323 25.6 0.76491588354110718 26.4 0.78868424892425537
		 27.2 0.8113858699798584 28 0.83288830518722523 28.8 0.85312867164611805 29.6 0.8718687891960144
		 30.4 0.88885217905044556 31.2 0.9037522077560427 32 0.91642659902572632 32.8 0.92683899402618419
		 33.6 0.93515527248382579 34.4 0.94147789478301969 35.2 0.94597369432449352 36 0.94882547855377197
		 36.8 0.95017200708389282 37.6 0.95026594400405884 38.4 0.9494112730026244 39.2 0.94789934158325184
		 40 0.94598209857940674 40.8 0.94398415088653564 41.6 0.94224566221237172 42.4 0.94106882810592662
		 43.2 0.94050365686416626 44 0.94140696525573697 44.8 0.94452238082885742 45.6 0.94586837291717518
		 46.4 0.95193696022033691 47.2 0.95899170637130737 48 0.96797990798950206 48.8 0.98021841049194325
		 49.6 0.98509997129440308 50.4 1.0041434764862061 51.2 1.0142427682876587 52 1.0274298191070557
		 52.8 1.0419467687606812 53.6 1.0569076538085935 54.4 1.0721875429153442 55.2 1.0875536203384399
		 56 1.1028212308883667 56.8 1.1178528070449829 57.6 1.1325749158859253 58.4 1.1467829942703247
		 59.2 1.1602848768234255 60 1.1730024814605713 60.8 1.1848856210708618 61.6 1.1958338022232056
		 62.4 1.2057465314865112 63.2 1.2144263982772827 64 1.221802830696106 64.8 1.2279232740402222
		 65.6 1.2328726053237915 66.4 1.2365608215332031 67.2 1.2388272285461426 68 1.2393829822540283
		 68.8 1.2379355430603027 69.6 1.2343131303787231 70.4 1.2283258438110352 71.2 1.2198596000671389
		 72 1.208717942237854 72.8 1.194693922996521;
createNode animCurveTA -n "Bip01_Spine2_rotateY11";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 92 ".ktv[0:91]"  0 -0.85115987062454224 0.8 -0.80449408292770386
		 1.6 -0.78580683469772339 2.4 -0.76916837692260731 3.2 -0.73691654205322277 4 -0.71255886554718018
		 4.8 -0.68866336345672607 5.6 -0.67926323413848877 6.4 -0.67019349336624146 7.2 -0.65490990877151489
		 8 -0.64514386653900146 8.8 -0.63709062337875366 9.6 -0.63479238748550415 10.4 -0.6277562975883485
		 11.2 -0.62580883502960205 12 -0.62030774354934692 12.8 -0.61440330743789673 13.6 -0.60538226366043102
		 14.4 -0.59903323650360107 15.2 -0.59001272916793823 16 -0.58216273784637451 16.8 -0.56090712547302246
		 17.6 -0.53315603733062744 18.4 -0.5157204270362854 19.2 -0.48957687616348267 20 -0.45780119299888611
		 20.8 -0.42134392261505127 21.6 -0.38026377558708191 22.4 -0.33478784561157227 23.2 -0.28512877225875854
		 24 -0.23132075369358063 24.8 -0.17328514158725741 25.6 -0.11102154105901718 26.4 -0.044772546738386154
		 27.2 0.025052644312381744 28 0.097950942814350128 28.8 0.17353011667728424 29.6 0.25138235092163086
		 30.4 0.33110001683235168 31.2 0.41226533055305481 32 0.49437466263771063 32.8 0.57684940099716187
		 33.6 0.65903180837631226 34.4 0.74025559425354004 35.2 0.81992870569229126 36 0.89745575189590443
		 36.8 0.97231024503707886 37.6 1.0439822673797607 38.4 1.1119576692581177 39.2 1.1757864952087402
		 40 1.2351595163345337 40.8 1.2898494005203247 41.6 1.3396282196044922 42.4 1.3821209669113159
		 43.2 1.4111264944076538 44 1.4582784175872805 44.8 1.4930306673049929 45.6 1.5008496046066284
		 46.4 1.5208667516708374 47.2 1.5311658382415771 48 1.5339089632034302 48.8 1.5293020009994511
		 49.6 1.5249491930007939 50.4 1.5022364854812622 51.2 1.4860078096389773 52 1.4615480899810791
		 52.8 1.4307370185852053 53.6 1.3945866823196411 54.4 1.3533002138137815 55.2 1.3070946931838987
		 56 1.2561948299407959 56.8 1.200749397277832 57.6 1.1408991813659668 58.4 1.0768793821334841
		 59.2 1.0090893507003784 60 0.9379165172576901 60.8 0.86369466781616211 61.6 0.78671258687973022
		 62.4 0.70725107192993164 63.2 0.62559902667999268 64 0.54205667972564697 64.8 0.45706444978713995
		 65.6 0.37112674117088318 66.4 0.28463122248649597 67.2 0.19775360822677612 68 0.11054958403110504
		 68.8 0.023223679512739185 69.6 -0.063939660787582397 70.4 -0.15059787034988403 71.2 -0.23652951419353485
		 72 -0.32172331213951111 72.8 -0.40638947486877441;
createNode animCurveTA -n "Bip01_Spine2_rotateZ11";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 92 ".ktv[0:91]"  0 2.5668683052062988 0.8 2.5391523838043213
		 1.6 2.5262601375579834 2.4 2.5141663551330566 3.2 2.4869887828826904 4 2.4622175693511963
		 4.8 2.4321599006652832 5.6 2.4177343845367432 6.4 2.4033308029174805 7.2 2.3739323616027832
		 8 2.3498392105102539 8.8 2.3243086338043213 9.6 2.3160014152526855 10.4 2.2892658710479736
		 11.2 2.2820897102355957 12 2.2646822929382324 12.8 2.2527210712432861 13.6 2.243671178817749
		 14.4 2.2416887283325195 15.2 2.240541934967041 16 2.2407333850860596 16.8 2.2478201389312744
		 17.6 2.261610746383667 18.4 2.2719039916992187 19.2 2.2878646850585938 20 2.3078486919403076
		 20.8 2.3309645652770996 21.6 2.3567876815795894 22.4 2.3848955631256104 23.2 2.4149291515350342
		 24 2.4465916156768799 24.8 2.4795653820037842 25.6 2.513390064239502 26.4 2.5475640296936035
		 27.2 2.5815169811248779 28 2.6147711277008057 28.8 2.6469216346740723 29.6 2.6776049137115479
		 30.4 2.7064437866210937 31.2 2.7331080436706543 32 2.7573716640472412 32.8 2.778980016708374
		 33.6 2.797694206237793 34.4 2.8134081363677979 35.2 2.8261086940765381 36 2.8358979225158691
		 36.8 2.8430073261260982 37.6 2.8477528095245361 38.4 2.8504416942596436 39.2 2.8512742519378662
		 40 2.8504917621612549 40.8 2.8483922481536865 41.6 2.8452978134155273 42.4 2.8418455123901367
		 43.2 2.8391547203063965 44 2.8346874713897705 44.8 2.8324506282806401 45.6 2.8323409557342529
		 46.4 2.8336951732635498 47.2 2.836763858795166 48 2.8425784111022949 48.8 2.8534047603607178
		 49.6 2.8588521480560303 50.4 2.8827230930328369 51.2 2.8975136280059814 52 2.9188237190246582
		 52.8 2.9450178146362305 53.6 2.9754271507263184 54.4 3.0102248191833496 55.2 3.0495805740356445
		 56 3.0936498641967773 56.8 3.1425297260284424 57.6 3.1961536407470703 58.4 3.2543938159942627
		 59.2 3.3171713352203369 60 3.3844630718231201 60.8 3.4561076164245605 61.6 3.5318286418914795
		 62.4 3.6112101078033447 63.2 3.693670511245728 64 3.7785103321075444 64.8 3.8649458885192862
		 65.6 3.9522371292114262 66.4 4.0397162437438965 67.2 4.1266007423400879 68 4.2120137214660645
		 68.8 4.2949743270874023 69.6 4.3746528625488281 70.4 4.4503078460693359 71.2 4.5212082862854004
		 72 4.586524486541748 72.8 4.6455121040344238;
createNode animCurveTA -n "Bip01_R_Clavicle_rotateX11";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 92 ".ktv[0:91]"  0 -0.98229819536209106 0.8 -0.98039144277572643
		 1.6 -0.97842997312545765 2.4 -0.97633427381515503 3.2 -0.97402644157409668 4 -0.97160726785659812
		 4.8 -0.96920764446258545 5.6 -0.96668249368667603 6.4 -0.9641978144645692 7.2 -0.96188473701477051
		 8 -0.95967024564743053 8.8 -0.95763993263244629 9.6 -0.955802321434021 10.4 -0.95421332120895397
		 11.2 -0.95264685153961171 12 -0.95133656263351429 12.8 -0.95019006729125999 13.6 -0.94924247264862049
		 14.4 -0.94843149185180675 15.2 -0.94780266284942627 16 -0.94733554124832176 16.8 -0.9472927451133728
		 17.6 -0.94709032773971547 18.4 -0.94722604751586914 19.2 -0.94737237691879261 20 -0.94784599542617809
		 20.8 -0.94805753231048562 21.6 -0.94894033670425426 22.4 -0.94981563091278087 23.2 -0.95082300901412964
		 24 -0.95211273431777965 24.8 -0.95408356189727783 25.6 -0.95469826459884644 26.4 -0.95703977346420299
		 27.2 -0.95926940441131581 28 -0.96148085594177279 28.8 -0.9637787938117981 29.6 -0.96644228696823131
		 30.4 -0.96967273950576782 31.2 -0.97078508138656605 32 -0.97419399023056052 32.8 -0.97742789983749412
		 33.6 -0.98058748245239269 34.4 -0.98455142974853516 35.2 -0.985773265361786 36 -0.98995047807693481
		 36.8 -0.99437379837036144 37.6 -0.99563974142074596 38.4 -0.99993515014648438 39.2 -1.0038889646530151
		 40 -1.0083010196685791 40.8 -1.0098189115524292 41.6 -1.0154519081115725 42.4 -1.0173044204711914
		 43.2 -1.0230669975280762 44 -1.0247360467910769 44.8 -1.0295360088348389 45.6 -1.0336160659790039
		 46.4 -1.0375759601593018 47.2 -1.0422841310501101 48 -1.043667197227478 48.8 -1.0480754375457764
		 49.6 -1.0517972707748413 50.4 -1.0558754205703735 51.2 -1.0576289892196655 52 -1.0598562955856323
		 52.8 -1.0614087581634519 53.6 -1.0639851093292236 54.4 -1.065521240234375 55.2 -1.0658066272735596
		 56 -1.0662842988967898 56.8 -1.0661236047744751 57.6 -1.0658221244812012 58.4 -1.0643327236175537
		 59.2 -1.0621010065078735 60 -1.0612598657608032 60.8 -1.0581157207489014 61.6 -1.0545153617858889
		 62.4 -1.0530949831008911 63.2 -1.0482115745544434 64 -1.0466756820678713 64.8 -1.0422934293746948
		 65.6 -1.0385732650756836 66.4 -1.0352240800857544 67.2 -1.0321626663208008 68 -1.0293287038803101
		 68.8 -1.0267819166183472 69.6 -1.0246390104293823 70.4 -1.0228339433670044 71.2 -1.0215210914611819
		 72 -1.0208140611648562 72.8 -1.0203207731246948;
createNode animCurveTA -n "Bip01_R_Clavicle_rotateY11";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 92 ".ktv[0:91]"  0 81.722557067871094 0.8 81.718894958496094
		 1.6 81.71512603759767 2.4 81.711097717285156 3.2 81.706703186035156 4 81.702064514160156
		 4.8 81.697372436523437 5.6 81.692649841308594 6.4 81.687957763671875 7.2 81.683456420898437
		 8 81.679267883300781 8.8 81.675376892089844 9.6 81.671890258789063 10.4 81.668708801269531
		 11.2 81.665847778320327 12 81.663307189941406 12.8 81.661064147949219 13.6 81.659225463867187
		 14.4 81.657745361328125 15.2 81.656600952148452 16 81.655670166015625 16.8 81.655479431152358
		 17.6 81.655258178710938 18.4 81.655357360839844 19.2 81.655685424804688 20 81.656570434570327
		 20.8 81.657028198242188 21.6 81.658668518066406 22.4 81.660385131835938 23.2 81.662307739257813
		 24 81.664871215820327 24.8 81.668487548828125 25.6 81.669685363769531 26.4 81.674224853515625
		 27.2 81.678413391113281 28 81.682685852050781 28.8 81.687171936035156 29.6 81.692214965820327
		 30.4 81.698455810546875 31.2 81.700408935546875 32 81.707183837890625 32.8 81.713218688964844
		 33.6 81.719383239746094 34.4 81.726882934570327 35.2 81.729141235351562 36 81.737258911132813
		 36.8 81.745674133300781 37.6 81.748092651367188 38.4 81.75628662109375 39.2 81.76373291015625
		 40 81.772186279296875 40.8 81.775169372558594 41.6 81.785766601562514 42.4 81.789375305175781
		 43.2 81.800300598144531 44 81.803497314453139 44.8 81.812660217285156 45.6 81.820358276367188
		 46.4 81.8280029296875 47.2 81.836837768554688 48 81.839469909667969 48.8 81.847923278808594
		 49.6 81.855049133300781 50.4 81.862655639648437 51.2 81.866073608398438 52 81.870254516601563
		 52.8 81.873298645019531 53.6 81.877944946289077 54.4 81.880935668945313 55.2 81.88153076171875
		 56 81.882423400878906 56.8 81.882102966308594 57.6 81.881561279296875 58.4 81.878715515136719
		 59.2 81.874404907226562 60 81.872940063476563 60.8 81.866912841796875 61.6 81.859939575195313
		 62.4 81.857322692871094 63.2 81.848045349121094 64 81.845260620117188 64.8 81.83689117431642
		 65.6 81.829826354980469 66.4 81.823486328125014 67.2 81.817558288574219 68 81.812171936035156
		 68.8 81.807342529296875 69.6 81.803306579589844 70.4 81.799972534179673 71.2 81.797470092773438
		 72 81.795906066894531 72.8 81.795089721679673;
createNode animCurveTA -n "Bip01_R_Clavicle_rotateZ11";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 92 ".ktv[0:91]"  0 173.98049926757812 0.8 173.98100280761719
		 1.6 173.98153686523435 2.4 173.98208618164062 3.2 173.98274230957034 4 173.9833984375
		 4.8 173.98402404785156 5.6 173.9847412109375 6.4 173.98544311523435 7.2 173.98605346679687
		 8 173.98664855957031 8.8 173.98721313476562 9.6 173.98771667480469 10.4 173.98809814453125
		 11.2 173.98858642578125 12 173.98890686035156 12.8 173.98922729492188 13.6 173.98948669433594
		 14.4 173.98973083496094 15.2 173.98991394042969 16 173.99002075195312 16.8 173.989990234375
		 17.6 173.9901123046875 18.4 173.99002075195312 19.2 173.989990234375 20 173.9898681640625
		 20.8 173.98980712890625 21.6 173.98956298828125 22.4 173.98933410644531 23.2 173.98905944824219
		 24 173.98873901367187 24.8 173.98814392089844 25.6 173.98797607421875 26.4 173.98736572265625
		 27.2 173.98674011230469 28 173.98614501953125 28.8 173.98554992675781 29.6 173.98481750488281
		 30.4 173.98394775390625 31.2 173.98358154296875 32 173.98274230957034 32.8 173.9818115234375
		 33.6 173.98100280761719 34.4 173.97987365722656 35.2 173.97950744628906 36 173.97843933105472
		 36.8 173.97718811035156 37.6 173.97685241699219 38.4 173.97567749023437 39.2 173.97454833984375
		 40 173.97334289550781 40.8 173.97296142578125 41.6 173.97135925292969 42.4 173.97087097167969
		 43.2 173.96926879882813 44 173.96879577636719 44.8 173.96748352050781 45.6 173.96632385253906
		 46.4 173.96527099609378 47.2 173.96392822265625 48 173.96354675292969 48.8 173.96234130859375
		 49.6 173.96133422851562 50.4 173.96012878417969 51.2 173.95967102050781 52 173.95904541015625
		 52.8 173.95864868164062 53.6 173.95783996582031 54.4 173.95742797851565 55.2 173.95735168457034
		 56 173.95722961425781 56.8 173.957275390625 57.6 173.95736694335935 58.4 173.95777893066406
		 59.2 173.95835876464844 60 173.95864868164062 60.8 173.95950317382815 61.6 173.96044921875003
		 62.4 173.96090698242187 63.2 173.96224975585935 64 173.96272277832031 64.8 173.96392822265625
		 65.6 173.96498107910156 66.4 173.96591186523438 67.2 173.96672058105469 68 173.96749877929687
		 68.8 173.96823120117187 69.6 173.96881103515625 70.4 173.9693603515625 71.2 173.9697265625
		 72 173.96983337402344 72.8 173.97001647949219;
createNode animCurveTA -n "Bip01_R_UpperArm_rotateX11";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 92 ".ktv[0:91]"  0 -28.196292877197266 0.8 -28.261491775512695
		 1.6 -28.304719924926761 2.4 -28.320432662963867 3.2 -28.307741165161136 4 -28.270940780639648
		 4.8 -28.229314804077148 5.6 -28.202812194824219 6.4 -28.18634033203125 7.2 -28.162555694580082
		 8 -28.126310348510746 8.8 -28.079961776733398 9.6 -28.014568328857425 10.4 -27.956045150756836
		 11.2 -27.943252563476563 12 -27.953922271728516 12.8 -27.945764541625977 13.6 -27.889852523803711
		 14.4 -27.829702377319336 15.2 -27.825313568115231 16 -27.804046630859379 16.8 -27.687284469604492
		 17.6 -27.513959884643555 18.4 -27.354040145874023 19.2 -27.219879150390629 20 -27.044549942016602
		 20.8 -26.812772750854492 21.6 -26.640615463256836 22.4 -26.550260543823239 23.2 -26.387407302856445
		 24 -26.099239349365231 24.8 -25.7766227722168 25.6 -25.448844909667969 26.4 -25.112274169921875
		 27.2 -24.808364868164063 28 -24.539968490600582 28.8 -24.247947692871097 29.6 -23.912143707275391
		 30.4 -23.588384628295895 31.2 -23.306783676147461 32 -23.021997451782227 32.8 -22.731870651245117
		 33.6 -22.496454238891602 34.4 -22.303501129150391 35.2 -22.117483139038089 36 -21.95635986328125
		 36.8 -21.798994064331055 37.6 -21.607967376708984 38.4 -21.420211791992188 39.2 -21.283422470092773
		 40 -21.201807022094727 40.8 -21.190193176269531 41.6 -21.239816665649418 42.4 -21.273523330688477
		 43.2 -21.262214660644531 44 -21.250654220581055 44.8 -21.265537261962891 45.6 -21.311790466308594
		 46.4 -21.38416862487793 47.2 -21.462591171264648 48 -21.531370162963867 48.8 -21.590904235839844
		 49.6 -21.646085739135746 50.4 -21.709747314453125 51.2 -21.806327819824219 52 -21.927936553955082
		 52.8 -22.050531387329105 53.6 -22.181196212768555 54.4 -22.316780090332031 55.2 -22.415864944458008
		 56 -22.45294189453125 56.8 -22.468000411987305 57.6 -22.511487960815433 58.4 -22.572357177734375
		 59.2 -22.612443923950195 60 -22.625980377197266 60.8 -22.673088073730469 61.6 -22.710050582885746
		 62.4 -22.723587036132812 63.2 -22.711711883544918 64 -22.729290008544918 64.8 -22.785699844360352
		 65.6 -22.852617263793945 66.4 -22.91900634765625 67.2 -22.980609893798832 68 -23.017969131469727
		 68.8 -23.004453659057617 69.6 -22.969161987304688 70.4 -23.015649795532227 71.2 -23.18077278137207
		 72 -23.359066009521484 72.8 -23.482006072998047;
createNode animCurveTA -n "Bip01_R_UpperArm_rotateY11";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 92 ".ktv[0:91]"  0 -52.944313049316413 0.8 -52.923679351806641
		 1.6 -52.926315307617188 2.4 -52.937618255615234 3.2 -52.946865081787109 4 -52.966289520263672
		 4.8 -52.997081756591797 5.6 -53.023036956787109 6.4 -53.044708251953125 7.2 -53.071220397949219
		 8 -53.099403381347656 8.8 -53.134933471679687 9.6 -53.187564849853523 10.4 -53.227581024169922
		 11.2 -53.229225158691406 12 -53.213020324707031 12.8 -53.196010589599609 13.6 -53.187705993652344
		 14.4 -53.179443359375 15.2 -53.154296875 16 -53.135658264160163 16.8 -53.13859939575196
		 17.6 -53.150157928466797 18.4 -53.153831481933594 19.2 -53.141578674316406 20 -53.148826599121094
		 20.8 -53.190452575683594 21.6 -53.206504821777344 22.4 -53.177864074707031 23.2 -53.166259765625007
		 24 -53.187992095947266 24.8 -53.199970245361328 25.6 -53.195751190185547 26.4 -53.191547393798835
		 27.2 -53.183925628662109 28 -53.171699523925781 28.8 -53.160003662109375 29.6 -53.149890899658203
		 30.4 -53.131984710693359 31.2 -53.099071502685547 32 -53.078178405761719 32.8 -53.078346252441406
		 33.6 -53.055118560791016 34.4 -53.006294250488281 35.2 -52.963703155517578 36 -52.91773986816407
		 36.8 -52.863239288330078 37.6 -52.811721801757805 38.4 -52.754344940185547 39.2 -52.706409454345703
		 40 -52.696258544921875 40.8 -52.684478759765625 41.6 -52.632770538330078 42.4 -52.573760986328125
		 43.2 -52.532947540283203 44 -52.505893707275391 44.8 -52.489665985107422 45.6 -52.463081359863281
		 46.4 -52.407093048095703 47.2 -52.337272644042969 48 -52.277156829833984 48.8 -52.248462677001953
		 49.6 -52.253925323486328 50.4 -52.256744384765625 51.2 -52.238380432128906 52 -52.207378387451165
		 52.8 -52.152816772460938 53.6 -52.087585449218757 54.4 -52.054855346679688 55.2 -52.054897308349609
		 56 -52.05804443359375 56.8 -52.056121826171875 57.6 -52.062602996826179 58.4 -52.087944030761719
		 59.2 -52.120033264160163 60 -52.138355255126953 60.8 -52.139625549316406 61.6 -52.147682189941413
		 62.4 -52.184333801269531 63.2 -52.220626831054688 64 -52.236743927001953 64.8 -52.261417388916016
		 65.6 -52.29461669921875 66.4 -52.313503265380859 67.2 -52.328384399414063 68 -52.354087829589837
		 68.8 -52.391544342041016 69.6 -52.428199768066406 70.4 -52.434955596923821 71.2 -52.414024353027344
		 72 -52.406314849853523 72.8 -52.424121856689453;
createNode animCurveTA -n "Bip01_R_UpperArm_rotateZ11";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 92 ".ktv[0:91]"  0 -17.801919937133789 0.8 -17.615226745605469
		 1.6 -17.47076416015625 2.4 -17.369676589965824 3.2 -17.317132949829102 4 -17.316911697387695
		 4.8 -17.331966400146484 5.6 -17.316030502319336 6.4 -17.277730941772461 7.2 -17.252702713012695
		 8 -17.249843597412113 8.8 -17.269060134887695 9.6 -17.331537246704102 10.4 -17.370660781860352
		 11.2 -17.29730224609375 12 -17.16351318359375 12.8 -17.057308197021484 13.6 -17.036666870117188
		 14.4 -17.013387680053711 15.2 -16.864524841308594 16 -16.739997863769531 16.8 -16.793720245361328
		 17.6 -16.940401077270508 18.4 -17.037012100219727 19.2 -17.055301666259766 20 -17.138547897338867
		 20.8 -17.321676254272461 21.6 -17.354530334472656 22.4 -17.186374664306641 23.2 -17.140657424926758
		 24 -17.324886322021484 24.8 -17.545949935913086 25.6 -17.748905181884766 26.4 -17.951889038085938
		 27.2 -18.0765495300293 28 -18.118719100952148 28.8 -18.193656921386719 29.6 -18.342342376708988
		 30.4 -18.461685180664062 31.2 -18.498044967651367 32 -18.554315567016602 32.8 -18.640731811523441
		 33.6 -18.625478744506836 34.4 -18.538179397583008 35.2 -18.467617034912109 36 -18.378936767578125
		 36.8 -18.314714431762695 37.6 -18.351316452026367 38.4 -18.410713195800781 39.2 -18.415643692016602
		 40 -18.378854751586911 40.8 -18.250127792358398 41.6 -18.023967742919918 42.4 -17.863954544067383
		 43.2 -17.837369918823242 44 -17.85395622253418 44.8 -17.858957290649414 45.6 -17.827425003051758
		 46.4 -17.751989364624023 47.2 -17.674985885620117 48 -17.639297485351563 48.8 -17.652799606323242
		 49.6 -17.699365615844727 50.4 -17.730939865112308 51.2 -17.691448211669922 52 -17.592983245849609
		 52.8 -17.464075088500977 53.6 -17.298177719116211 54.4 -17.130527496337891 55.2 -17.041549682617188
		 56 -17.061563491821289 56.8 -17.105735778808594 57.6 -17.082096099853516 58.4 -17.016565322875977
		 59.2 -16.976739883422852 60 -16.967617034912109 60.8 -16.861948013305664 61.6 -16.782957077026367
		 62.4 -16.751222610473633 63.2 -16.762334823608398 64 -16.708086013793949 64.8 -16.586021423339844
		 65.6 -16.45246696472168 66.4 -16.322956085205078 67.2 -16.215082168579102 68 -16.176607131958008
		 68.8 -16.263408660888672 69.6 -16.414737701416016 70.4 -16.415338516235352 71.2 -16.196609497070312
		 72 -15.984951972961426 72.8 -15.919349670410156;
createNode animCurveTA -n "Bip01_R_Forearm_rotateX11";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 92 ".ktv[0:91]"  0 -0.88651049137115467 0.8 -0.89116108417511009
		 1.6 -0.89481055736541748 2.4 -0.89729148149490345 3.2 -0.8976847529411317 4 -0.89601457118988037
		 4.8 -0.89474946260452271 5.6 -0.89472109079360962 6.4 -0.8945692777633667 7.2 -0.89392882585525524
		 8 -0.89285731315612804 8.8 -0.89107733964920044 9.6 -0.88850241899490356 10.4 -0.8868219256401062
		 11.2 -0.88708662986755371 12 -0.88874208927154541 12.8 -0.89108806848526001 13.6 -0.89071643352508545
		 14.4 -0.89052724838256836 15.2 -0.89688748121261597 16 -0.90289407968521118 16.8 -0.90068161487579346
		 17.6 -0.89439898729324341 18.4 -0.89110743999481212 19.2 -0.89189493656158447 20 -0.89029306173324596
		 20.8 -0.88463598489761364 21.6 -0.88464421033859253 22.4 -0.89147329330444336 23.2 -0.89402472972869873
		 24 -0.88844442367553711 24.8 -0.88115608692169189 25.6 -0.8745293617248534 26.4 -0.86932897567749023
		 27.2 -0.86774015426635742 28 -0.86734706163406372 28.8 -0.86591637134552013 29.6 -0.8626481294631958
		 30.4 -0.86192589998245228 31.2 -0.86634433269500732 32 -0.87074267864227295 32.8 -0.87203466892242432
		 33.6 -0.87553894519805908 34.4 -0.88195890188217174 35.2 -0.88797414302825917 36 -0.89476311206817627
		 36.8 -0.90137565135955811 37.6 -0.9043889045715332 38.4 -0.90465980768203713 39.2 -0.90666842460632324
		 40 -0.9101271629333495 40.8 -0.91647857427597046 41.6 -0.92592537403106689 42.4 -0.93320131301879872
		 43.2 -0.93538355827331554 44 -0.93453991413116433 44.8 -0.93271714448928833 45.6 -0.9323866367340089
		 46.4 -0.93446302413940463 47.2 -0.93671143054962158 48 -0.93802815675735474 48.8 -0.93850964307785034
		 49.6 -0.93710148334503185 50.4 -0.93438625335693359 51.2 -0.93355137109756481 52 -0.93583160638809193
		 52.8 -0.94107598066329967 53.6 -0.94894361495971669 54.4 -0.95646536350250255 55.2 -0.96076029539108276
		 56 -0.96251833438873302 56.8 -0.96448892354965199 57.6 -0.96810722351074219 58.4 -0.97230458259582508
		 59.2 -0.97546064853668213 60 -0.97827446460723888 60.8 -0.98341369628906239 61.6 -0.988883376121521
		 62.4 -0.99024480581283547 63.2 -0.9894958734512328 64 -0.99393141269683827 64.8 -1.0009173154830935
		 65.6 -1.0065286159515381 66.4 -1.012316107749939 67.2 -1.0189599990844729 68 -1.0234224796295166
		 68.8 -1.0230836868286133 69.6 -1.0208370685577393 70.4 -1.023720383644104 71.2 -1.0327162742614746
		 72 -1.0402522087097168 72.8 -1.0405447483062744;
createNode animCurveTA -n "Bip01_R_Forearm_rotateY11";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 92 ".ktv[0:91]"  0 1.7992802858352659 0.8 1.8028985261917112
		 1.6 1.8050456047058103 2.4 1.8066916465759275 3.2 1.8076080083847044 4 1.807585597038269
		 4.8 1.8079590797424316 5.6 1.8086638450622561 6.4 1.8085906505584719 7.2 1.8082350492477417
		 8 1.8081909418106081 8.8 1.8075052499771116 9.6 1.804928183555603 10.4 1.8018057346343994
		 11.2 1.8001948595046995 12 1.8008501529693604 12.8 1.80332350730896 13.6 1.8037627935409546
		 14.4 1.8031339645385742 15.2 1.8066265583038332 16 1.8101551532745357 16.8 1.8084486722946167
		 17.6 1.8040539026260376 18.4 1.8012778759002686 19.2 1.8007893562316897 20 1.7982769012451172
		 20.8 1.7930976152420042 21.6 1.7927001714706423 22.4 1.7988893985748289 23.2 1.8027380704879761
		 24 1.8011280298233034 24.8 1.7984145879745483 25.6 1.7956944704055784 26.4 1.7932776212692263
		 27.2 1.7932522296905518 28 1.7975511550903327 28.8 1.7988712787628174 29.6 1.7981579303741455
		 30.4 1.7972992658615117 31.2 1.7979105710983283 32 1.7967351675033572 32.8 1.7931774854660036
		 33.6 1.7933843135833738 34.4 1.7981089353561404 35.2 1.8029186725616455 36 1.8070632219314577
		 36.8 1.8100348711013794 37.6 1.8105965852737427 38.4 1.809449315071106 39.2 1.8082423210144045
		 40 1.8097149133682251 40.8 1.813077449798584 41.6 1.8185343742370605 42.4 1.8225343227386472
		 43.2 1.8242454528808596 44 1.825960636138916 44.8 1.8279685974121096 45.6 1.8302154541015625
		 46.4 1.8328802585601811 47.2 1.8350557088851929 48 1.8364834785461426 48.8 1.8375959396362305
		 49.6 1.8381249904632568 50.4 1.8385548591613772 51.2 1.8400706052780151 52 1.8423365354537964
		 52.8 1.8451223373413088 53.6 1.848741292953491 54.4 1.8515483140945432 55.2 1.8521220684051511
		 56 1.8513926267623899 56.8 1.8510270118713379 57.6 1.8516147136688232 58.4 1.8525789976119993
		 59.2 1.8535196781158447 60 1.855413556098938 60.8 1.8598383665084837 61.6 1.8645108938217161
		 62.4 1.8662030696868896 63.2 1.8659907579421999 64 1.8694294691085811 64.8 1.8743307590484621
		 65.6 1.8774652481079106 66.4 1.8800261020660407 67.2 1.8834058046340945 68 1.8863430023193359
		 68.8 1.88658607006073 69.6 1.8849087953567507 70.4 1.8855732679367065 71.2 1.8898577690124512
		 72 1.89374840259552 72.8 1.896377325057983;
createNode animCurveTA -n "Bip01_R_Forearm_rotateZ11";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 92 ".ktv[0:91]"  0 -46.060760498046875 0.8 -46.216114044189453
		 1.6 -46.327125549316413 2.4 -46.405483245849609 3.2 -46.428226470947266 4 -46.392738342285149
		 4.8 -46.371967315673821 5.6 -46.382686614990234 6.4 -46.378402709960938 7.2 -46.359123229980469
		 8 -46.336105346679688 8.8 -46.287937164306641 9.6 -46.192867279052734 10.4 -46.107555389404297
		 11.2 -46.087322235107422 12 -46.132545471191406 12.8 -46.221378326416016 13.6 -46.220287322998047
		 14.4 -46.206470489501953 15.2 -46.396022796630866 16 -46.578662872314453 16.8 -46.504966735839837
		 17.6 -46.302700042724609 18.4 -46.189540863037109 19.2 -46.198772430419922 20 -46.125556945800781
		 20.8 -45.924293518066406 21.6 -45.918529510498047 22.4 -46.160388946533203 23.2 -46.274574279785149
		 24 -46.130985260009766 24.8 -45.934406280517578 25.6 -45.752582550048842 26.4 -45.60528564453125
		 27.2 -45.572536468505859 28 -45.637321472167969 28.8 -45.631366729736328 29.6 -45.55413818359375
		 30.4 -45.524520874023438 31.2 -45.623928070068359 32 -45.693115234375 32.8 -45.660533905029297
		 33.6 -45.736263275146477 34.4 -45.946029663085937 35.2 -46.149299621582038 36 -46.358272552490234
		 36.8 -46.544654846191406 37.6 -46.617626190185547 38.4 -46.605674743652344 39.2 -46.630550384521491
		 40 -46.727741241455078 40.8 -46.916465759277344 41.6 -47.204086303710937 42.4 -47.423145294189453
		 43.2 -47.496273040771491 44 -47.502994537353523 44.8 -47.4930419921875 45.6 -47.519660949707031
		 46.4 -47.604927062988281 47.2 -47.6866455078125 48 -47.736896514892578 48.8 -47.764480590820313
		 49.6 -47.743114471435547 50.4 -47.692897796630859 51.2 -47.699436187744141 52 -47.783462524414063
		 52.8 -47.937995910644531 53.6 -48.160861968994141 54.4 -48.364238739013672 55.2 -48.464515686035156
		 56 -48.490917205810547 56.8 -48.528232574462891 57.6 -48.615825653076179 58.4 -48.721958160400391
		 59.2 -48.805179595947266 60 -48.894744873046875 60.8 -49.072166442871094 61.6 -49.260498046875
		 62.4 -49.315139770507813 63.2 -49.295543670654297 64 -49.443347930908203 64.8 -49.668354034423828
		 65.6 -49.837360382080078 66.4 -50.001903533935547 67.2 -50.19769287109375 68 -50.338283538818366
		 68.8 -50.334171295166016 69.6 -50.260631561279297 70.4 -50.334091186523437 71.2 -50.59503173828125
		 72 -50.818210601806641 72.8 -50.861476898193359;
createNode animCurveTA -n "Bip01_R_Hand_rotateX1";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 92 ".ktv[0:91]"  0 90.133331298828125 0.8 90.133316040039063
		 1.6 90.133316040039063 2.4 90.133316040039063 3.2 90.133316040039063 4 90.133316040039063
		 4.8 90.133308410644531 5.6 90.133316040039063 6.4 90.133316040039063 7.2 90.133316040039063
		 8 90.133316040039063 8.8 90.133316040039063 9.6 90.133308410644531 10.4 90.133316040039063
		 11.2 90.133316040039063 12 90.133316040039063 12.8 90.133316040039063 13.6 90.133308410644531
		 14.4 90.133308410644531 15.2 90.133308410644531 16 90.133308410644531 16.8 90.133316040039063
		 17.6 90.133316040039063 18.4 90.133316040039063 19.2 90.133316040039063 20 90.133316040039063
		 20.8 90.133316040039063 21.6 90.133316040039063 22.4 90.133316040039063 23.2 90.133308410644531
		 24 90.133308410644531 24.8 90.133316040039063 25.6 90.133308410644531 26.4 90.133316040039063
		 27.2 90.133308410644531 28 90.133316040039063 28.8 90.133316040039063 29.6 90.133316040039063
		 30.4 90.133316040039063 31.2 90.133308410644531 32 90.133316040039063 32.8 90.133316040039063
		 33.6 90.133308410644531 34.4 90.133316040039063 35.2 90.133316040039063 36 90.133316040039063
		 36.8 90.133308410644531 37.6 90.133308410644531 38.4 90.133316040039063 39.2 90.133316040039063
		 40 90.133316040039063 40.8 90.133316040039063 41.6 90.133316040039063 42.4 90.133316040039063
		 43.2 90.133331298828125 44 90.133308410644531 44.8 90.133308410644531 45.6 90.133316040039063
		 46.4 90.133316040039063 47.2 90.133308410644531 48 90.133308410644531 48.8 90.133316040039063
		 49.6 90.133316040039063 50.4 90.133316040039063 51.2 90.133308410644531 52 90.133308410644531
		 52.8 90.133316040039063 53.6 90.133316040039063 54.4 90.133308410644531 55.2 90.133316040039063
		 56 90.133316040039063 56.8 90.133308410644531 57.6 90.133316040039063 58.4 90.133316040039063
		 59.2 90.133308410644531 60 90.133316040039063 60.8 90.133316040039063 61.6 90.133316040039063
		 62.4 90.133308410644531 63.2 90.133316040039063 64 90.133316040039063 64.8 90.133308410644531
		 65.6 90.133331298828125 66.4 90.133308410644531 67.2 90.133316040039063 68 90.133316040039063
		 68.8 90.133316040039063 69.6 90.133308410644531 70.4 90.133316040039063 71.2 90.133316040039063
		 72 90.133316040039063 72.8 90.133308410644531;
createNode animCurveTA -n "Bip01_R_Hand_rotateY1";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 92 ".ktv[0:91]"  0 -6.3004002571105957 0.8 -6.3004035949707031
		 1.6 -6.3003969192504883 2.4 -6.3004007339477539 3.2 -6.3004040718078622 4 -6.3004002571105957
		 4.8 -6.3003988265991211 5.6 -6.3004012107849121 6.4 -6.3004045486450195 7.2 -6.3004012107849121
		 8 -6.3004026412963876 8.8 -6.3004016876220703 9.6 -6.3004035949707031 10.4 -6.3003964424133301
		 11.2 -6.3003973960876465 12 -6.3004007339477539 12.8 -6.3004007339477539 13.6 -6.3003988265991211
		 14.4 -6.3004045486450195 15.2 -6.3003988265991211 16 -6.3004016876220703 16.8 -6.3004045486450195
		 17.6 -6.300407886505127 18.4 -6.3004050254821777 19.2 -6.3004016876220703 20 -6.3004045486450195
		 20.8 -6.3004007339477539 21.6 -6.3004031181335449 22.4 -6.3004031181335449 23.2 -6.3004007339477539
		 24 -6.3003964424133301 24.8 -6.3004031181335449 25.6 -6.3004007339477539 26.4 -6.3003993034362793
		 27.2 -6.3003983497619629 28 -6.3004002571105957 28.8 -6.3004040718078622 29.6 -6.3004026412963876
		 30.4 -6.3004007339477539 31.2 -6.3004074096679687 32 -6.3004035949707031 32.8 -6.3004031181335449
		 33.6 -6.3003964424133301 34.4 -6.3004074096679687 35.2 -6.3004064559936523 36 -6.3003983497619629
		 36.8 -6.3004035949707031 37.6 -6.3003997802734375 38.4 -6.3004026412963876 39.2 -6.3004012107849121
		 40 -6.3004007339477539 40.8 -6.3004026412963876 41.6 -6.3004016876220703 42.4 -6.3004026412963876
		 43.2 -6.3004064559936523 44 -6.3004021644592285 44.8 -6.3004012107849121 45.6 -6.3004026412963876
		 46.4 -6.3004045486450195 47.2 -6.3004016876220703 48 -6.3004002571105957 48.8 -6.3003983497619629
		 49.6 -6.3004002571105957 50.4 -6.3004045486450195 51.2 -6.3003993034362793 52 -6.3003964424133301
		 52.8 -6.3004031181335449 53.6 -6.3004031181335449 54.4 -6.3003988265991211 55.2 -6.3004045486450195
		 56 -6.3003973960876465 56.8 -6.3003983497619629 57.6 -6.3004069328308105 58.4 -6.3004055023193359
		 59.2 -6.3004035949707031 60 -6.3003969192504883 60.8 -6.3004107475280771 61.6 -6.3004035949707031
		 62.4 -6.3003988265991211 63.2 -6.3004007339477539 64 -6.3004035949707031 64.8 -6.3004002571105957
		 65.6 -6.3004040718078622 66.4 -6.3004007339477539 67.2 -6.3004021644592285 68 -6.3004035949707031
		 68.8 -6.3004035949707031 69.6 -6.3004016876220703 70.4 -6.3004040718078622 71.2 -6.3004007339477539
		 72 -6.3004045486450195 72.8 -6.3003983497619629;
createNode animCurveTA -n "Bip01_R_Hand_rotateZ1";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 92 ".ktv[0:91]"  0 2.5653319358825684 0.8 2.5653336048126221
		 1.6 2.565335750579834 2.4 2.5653319358825684 3.2 2.5653264522552495 4 2.5653338432312016
		 4.8 2.5653367042541504 5.6 2.5653347969055176 6.4 2.5653352737426758 7.2 2.5653319358825684
		 8 2.5653319358825684 8.8 2.5653252601623535 9.6 2.5653316974639893 10.4 2.5653328895568848
		 11.2 2.5653395652770996 12 2.5653350353240967 12.8 2.565330982208252 13.6 2.5653319358825684
		 14.4 2.5653367042541504 15.2 2.5653257369995117 16 2.5653269290924072 16.8 2.5653293132781982
		 17.6 2.56532883644104 18.4 2.5653290748596191 19.2 2.5653345584869385 20 2.5653283596038814
		 20.8 2.5653271675109863 21.6 2.5653278827667236 22.4 2.565330982208252 23.2 2.5653254985809326
		 24 2.5653302669525142 24.8 2.5653300285339355 25.6 2.5653350353240967 26.4 2.5653331279754639
		 27.2 2.5653319358825684 28 2.5653424263000488 28.8 2.5653371810913086 29.6 2.5653359889984131
		 30.4 2.5653302669525142 31.2 2.5653262138366699 32 2.5653297901153564 32.8 2.5653362274169922
		 33.6 2.5653343200683594 34.4 2.5653285980224609 35.2 2.5653226375579834 36 2.5653305053710938
		 36.8 2.565333366394043 37.6 2.5653347969055176 38.4 2.5653338432312016 39.2 2.5653326511383057
		 40 2.5653340816497803 40.8 2.5653319358825684 41.6 2.5653328895568848 42.4 2.5653338432312016
		 43.2 2.565333366394043 44 2.5653321743011475 44.8 2.5653295516967773 45.6 2.5653345584869385
		 46.4 2.5653350353240967 47.2 2.5653297901153564 48 2.5653257369995117 48.8 2.5653295516967773
		 49.6 2.5653376579284668 50.4 2.5653278827667236 51.2 2.5653259754180908 52 2.5653347969055176
		 52.8 2.5653300285339355 53.6 2.5653247833251953 54.4 2.5653305053710938 55.2 2.5653245449066162
		 56 2.5653386116027832 56.8 2.5653347969055176 57.6 2.5653278827667236 58.4 2.5653269290924072
		 59.2 2.5653319358825684 60 2.5653307437896729 60.8 2.5653321743011475 61.6 2.565330982208252
		 62.4 2.5653295516967773 63.2 2.5653321743011475 64 2.565338134765625 64.8 2.5653324127197266
		 65.6 2.5653326511383057 66.4 2.5653338432312016 67.2 2.5653376579284668 68 2.5653262138366699
		 68.8 2.565330982208252 69.6 2.565335750579834 70.4 2.5653398036956787 71.2 2.5653355121612549
		 72 2.565324068069458 72.8 2.5653276443481445;
createNode animCurveTA -n "Bip01_L_Clavicle_rotateX11";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 92 ".ktv[0:91]"  0 -0.04923555999994278 0.8 -0.051368426531553261
		 1.6 -0.053926948457956314 2.4 -0.057423572987318039 3.2 -0.058539241552352898 4 -0.062464691698551178
		 4.8 -0.066191717982292175 5.6 -0.070279508829116821 6.4 -0.071461200714111328 7.2 -0.075334355235099792
		 8 -0.07829797267913817 8.8 -0.080775916576385498 9.6 -0.08310997486114502 10.4 -0.083730459213256836
		 11.2 -0.085002310574054732 12 -0.085455484688282013 12.8 -0.085182115435600295 13.6 -0.084238782525062561
		 14.4 -0.082658171653747559 15.2 -0.080439813435077667 16 -0.077630795538425446 16.8 -0.074286848306655884
		 17.6 -0.070024900138378143 18.4 -0.064485356211662292 19.2 -0.062653586268424988
		 20 -0.056134548038244254 20.8 -0.050290793180465698 21.6 -0.044101200997829444 22.4 -0.036589439958333969
		 23.2 -0.034293334931135178 24 -0.026610186323523521 24.8 -0.020143236964941025 25.6 -0.013593249954283236
		 26.4 -0.0059836213476955891 27.2 -0.0037530185654759411 28 0.0037315809167921539
		 28.8 0.010365894995629787 29.6 0.017482472583651543 30.4 0.019891237840056419 31.2 0.027741057798266411
		 32 0.030065011233091351 32.8 0.036033611744642258 33.6 0.041229706257581711 34.4 0.046539213508367538
		 35.2 0.048056486994028091 36 0.053034987300634384 36.8 0.057016741484403603 37.6 0.060645952820777893
		 38.4 0.064371570944786072 39.2 0.068566836416721344 40 0.069817446172237396 40.8 0.074044540524482713
		 41.6 0.07798449695110321 42.4 0.079783357679843903 43.2 0.081448368728160858 44 0.084837079048156738
		 44.8 0.087333172559738159 45.6 0.089405521750450134 46.4 0.09123558551073073 47.2 0.092578820884227767
		 48 0.093487672507762909 48.8 0.093985579907894135 49.6 0.093965008854866028 50.4 0.093277379870414734
		 51.2 0.091843627393245697 52 0.091217741370201111 52.8 0.088624373078346252 53.6 0.085481360554695129
		 54.4 0.08150026947259903 55.2 0.08019062876701355 56 0.07520785927772522 56.8 0.06972040981054306
		 57.6 0.068149179220199585 58.4 0.062537252902984619 59.2 0.056934420019388199 60 0.054599069058895111
		 60.8 0.052244648337364197 61.6 0.047729853540658951 62.4 0.044677641242742538 63.2 0.042170312255620963
		 64 0.04002661257982254 64.8 0.039539054036140442 65.6 0.038478318601846695 66.4 0.038170851767063141
		 67.2 0.038370057940483093 68 0.039014127105474472 68.8 0.040026459842920303 69.6 0.041311420500278473
		 70.4 0.042877901345491409 71.2 0.044501248747110367 72 0.046012226492166519 72.8 0.047771308571100235;
createNode animCurveTA -n "Bip01_L_Clavicle_rotateY11";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 92 ".ktv[0:91]"  0 -79.663825988769531 0.8 -79.659431457519531
		 1.6 -79.654067993164062 2.4 -79.646896362304688 3.2 -79.644561767578125 4 -79.6363525390625
		 4.8 -79.628753662109375 5.6 -79.620178222656264 6.4 -79.61767578125 7.2 -79.609634399414077
		 8 -79.603485107421875 8.8 -79.598289489746094 9.6 -79.59344482421875 10.4 -79.592269897460952
		 11.2 -79.589530944824219 12 -79.588546752929702 12.8 -79.589187622070298 13.6 -79.591117858886719
		 14.4 -79.594375610351563 15.2 -79.598953247070327 16 -79.604843139648437 16.8 -79.611839294433594
		 17.6 -79.620574951171875 18.4 -79.632171630859375 19.2 -79.635955810546875 20 -79.649429321289063
		 20.8 -79.661613464355469 21.6 -79.674514770507813 22.4 -79.690017700195313 23.2 -79.694755554199233
		 24 -79.710670471191406 24.8 -79.724212646484375 25.6 -79.737709045410156 26.4 -79.753448486328125
		 27.2 -79.758125305175781 28 -79.773582458496094 28.8 -79.787216186523438 29.6 -79.801918029785156
		 30.4 -79.806900024414063 31.2 -79.823280334472656 32 -79.827857971191406 32.8 -79.840354919433594
		 33.6 -79.850914001464844 34.4 -79.861961364746094 35.2 -79.865119934082031 36 -79.875274658203125
		 36.8 -79.883491516113281 37.6 -79.890960693359375 38.4 -79.898666381835938 39.2 -79.907356262207017
		 40 -79.909919738769531 40.8 -79.918464660644531 41.6 -79.926803588867187 42.4 -79.930442810058594
		 43.2 -79.933883666992188 44 -79.940818786621094 44.8 -79.945968627929702 45.6 -79.950271606445313
		 46.4 -79.95391845703125 47.2 -79.956787109375 48 -79.958648681640625 48.8 -79.95953369140625
		 49.6 -79.959510803222656 50.4 -79.958175659179673 51.2 -79.955169677734375 52 -79.953903198242188
		 52.8 -79.948455810546875 53.6 -79.942207336425781 54.4 -79.93393707275392 55.2 -79.931243896484375
		 56 -79.920989990234375 56.8 -79.909721374511719 57.6 -79.906425476074219 58.4 -79.894859313964844
		 59.2 -79.883438110351563 60 -79.878524780273437 60.8 -79.873710632324219 61.6 -79.86454010009767
		 62.4 -79.858146667480469 63.2 -79.852989196777344 64 -79.84844970703125 64.8 -79.847465515136719
		 65.6 -79.845268249511719 66.4 -79.84466552734375 67.2 -79.845123291015625 68 -79.846466064453125
		 68.8 -79.848533630371094 69.6 -79.851165771484375 70.4 -79.854263305664063 71.2 -79.857612609863281
		 72 -79.860977172851562 72.8 -79.864471435546875;
createNode animCurveTA -n "Bip01_L_Clavicle_rotateZ11";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 92 ".ktv[0:91]"  0 174.22489929199219 0.8 174.22532653808594
		 1.6 174.22584533691406 2.4 174.22657775878906 3.2 174.22679138183594 4 174.22756958007812
		 4.8 174.22836303710935 5.6 174.22915649414062 6.4 174.22935485839844 7.2 174.23016357421875
		 8 174.23075866699219 8.8 174.23123168945313 9.6 174.23170471191406 10.4 174.23187255859375
		 11.2 174.23207092285156 12 174.23216247558594 12.8 174.23213195800781 13.6 174.23193359375
		 14.4 174.23161315917969 15.2 174.23117065429687 16 174.23060607910156 16.8 174.22996520996094
		 17.6 174.22906494140625 18.4 174.22798156738281 19.2 174.22761535644531 20 174.22625732421875
		 20.8 174.22511291503906 21.6 174.22386169433594 22.4 174.22233581542969 23.2 174.22186279296875
		 24 174.22027587890625 24.8 174.21902465820313 25.6 174.21766662597656 26.4 174.21611022949219
		 27.2 174.21566772460935 28 174.21412658691406 28.8 174.21273803710935 29.6 174.21128845214844
		 30.4 174.21076965332031 31.2 174.20921325683594 32 174.20864868164062 32.8 174.20747375488281
		 33.6 174.20635986328125 34.4 174.20527648925781 35.2 174.2049560546875 36 174.20388793945312
		 36.8 174.20307922363281 37.6 174.20231628417969 38.4 174.20155334472656 39.2 174.20068359375
		 40 174.20040893554687 40.8 174.19947814941406 41.6 174.19873046875 42.4 174.19833374023435
		 43.2 174.197998046875 44 174.19725036621094 44.8 174.19673156738281 45.6 174.19633483886719
		 46.4 174.19590759277344 47.2 174.19566345214844 48 174.19546508789062 48.8 174.1953125
		 49.6 174.19529724121094 50.4 174.19549560546875 51.2 174.19577026367187 52 174.19590759277344
		 52.8 174.19642639160156 53.6 174.19715881347656 54.4 174.19795227050784 55.2 174.1982421875
		 56 174.19929504394531 56.8 174.200439453125 57.6 174.20074462890625 58.4 174.20193481445312
		 59.2 174.20310974121094 60 174.20358276367187 60.8 174.20407104492187 61.6 174.205078125
		 62.4 174.20567321777344 63.2 174.20620727539062 64 174.20660400390628 64.8 174.20671081542969
		 65.6 174.20692443847656 66.4 174.20700073242187 67.2 174.20698547363281 68 174.20684814453125
		 68.8 174.20663452148435 69.6 174.20637512207031 70.4 174.20599365234375 71.2 174.20564270019531
		 72 174.20542907714844 72.8 174.20501708984375;
createNode animCurveTA -n "Bip01_L_UpperArm_rotateX11";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 92 ".ktv[0:91]"  0 10.395947456359863 0.8 10.45625686645508
		 1.6 10.478641510009766 2.4 10.465179443359377 3.2 10.392980575561523 4 10.268735885620115
		 4.8 10.134049415588381 5.6 10.064658164978027 6.4 10.053619384765623 7.2 10.004134178161619
		 8 9.8568649291992187 8.8 9.6811742782592773 9.6 9.5325860977172852 10.4 9.3199825286865234
		 11.2 9.0179519653320312 12 8.7559652328491211 12.8 8.5885410308837891 13.6 8.4375953674316406
		 14.4 8.2964944839477539 15.2 8.2239112854003906 16 8.1899175643920898 16.8 8.132206916809082
		 17.6 8.0694761276245117 18.4 8.1025114059448242 19.2 8.2229757308959961 20 8.2921075820922852
		 20.8 8.3798341751098633 21.6 8.6431541442871094 22.4 9.0044832229614258 23.2 9.3779010772705078
		 24 9.7397279739379883 24.8 10.04358959197998 25.6 10.32639217376709 26.4 10.63150691986084
		 27.2 10.982260704040527 28 11.406082153320313 28.8 11.886484146118164 29.6 12.40023136138916
		 30.4 12.890157699584959 31.2 13.339788436889648 32 13.867956161499023 32.8 14.502882957458496
		 33.6 15.08434009552002 34.4 15.528925895690916 35.2 15.936975479125977 36 16.404340744018555
		 36.8 16.886800765991211 37.6 17.272832870483398 38.4 17.560527801513672 39.2 17.880151748657227
		 40 18.29633903503418 40.8 18.71116828918457 41.6 19.030147552490231 42.4 19.281528472900391
		 43.2 19.518617630004883 44 19.716484069824219 44.8 19.819728851318359 45.6 19.856725692749023
		 46.4 19.916574478149418 47.2 20.004886627197266 48 20.065032958984375 48.8 20.014196395874023
		 49.6 19.952840805053711 50.4 19.81059455871582 51.2 19.66258430480957 52 19.446590423583984
		 52.8 19.083118438720703 53.6 18.674383163452148 54.4 18.274787902832031 55.2 17.835390090942383
		 56 17.337833404541016 56.8 16.746522903442383 57.6 15.999037742614744 58.4 15.071091651916504
		 59.2 14.104901313781738 60 13.34559440612793 60.8 12.852714538574221 61.6 12.471567153930664
		 62.4 11.959493637084959 63.2 11.244565010070801 64 10.512954711914062 64.8 9.8545942306518555
		 65.6 9.2157726287841797 66.4 8.6281061172485352 67.2 8.1487979888916016 68 7.7376141548156756
		 68.8 7.2977561950683594 69.6 6.819420337677002 70.4 6.4253745079040527 71.2 6.1579618453979492
		 72 5.9250268936157227 72.8 5.7290821075439462;
createNode animCurveTA -n "Bip01_L_UpperArm_rotateY11";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 92 ".ktv[0:91]"  0 64.368270874023437 0.8 64.38299560546875
		 1.6 64.409278869628906 2.4 64.441848754882813 3.2 64.476661682128906 4 64.510818481445313
		 4.8 64.541656494140625 5.6 64.577499389648437 6.4 64.615890502929688 7.2 64.63812255859375
		 8 64.6453857421875 8.8 64.655342102050781 9.6 64.673049926757812 10.4 64.706604003906264
		 11.2 64.758583068847656 12 64.798896789550781 12.8 64.813369750976563 13.6 64.822479248046875
		 14.4 64.829437255859375 15.2 64.822799682617188 16 64.813133239746094 16.8 64.812393188476562
		 17.6 64.810775756835938 18.4 64.797431945800781 19.2 64.769577026367188 20 64.721519470214844
		 20.8 64.654685974121094 21.6 64.5894775390625 22.4 64.531723022460937 23.2 64.465751647949219
		 24 64.402336120605469 24.8 64.363265991210938 25.6 64.331260681152344 26.4 64.266899108886719
		 27.2 64.16607666015625 28 64.066108703613281 28.8 63.986907958984368 29.6 63.917350769042969
		 30.4 63.839241027832031 31.2 63.738685607910163 32 63.633106231689453 32.8 63.551845550537102
		 33.6 63.488399505615234 34.4 63.420127868652351 35.2 63.342063903808601 36 63.26533508300782
		 36.8 63.190311431884766 37.6 63.112419128417962 38.4 63.051235198974602 39.2 63.004241943359375
		 40 62.943073272705078 40.8 62.87969970703125 41.6 62.832866668701172 42.4 62.786666870117195
		 43.2 62.729026794433601 44 62.675102233886726 44.8 62.640964508056641 45.6 62.624908447265618
		 46.4 62.606891632080078 47.2 62.567283630371101 48 62.519081115722656 48.8 62.478290557861328
		 49.6 62.458271026611321 50.4 62.461639404296861 51.2 62.472240447998047 52 62.480422973632805
		 52.8 62.504848480224609 53.6 62.557861328124993 54.4 62.65665435791017 55.2 62.787425994873047
		 56 62.901149749755866 56.8 63.014999389648437 57.6 63.193084716796861 58.4 63.447849273681648
		 59.2 63.701496124267571 60 63.854526519775384 60.8 63.893478393554695 61.6 63.876705169677741
		 62.4 63.889915466308594 63.2 63.970169067382805 64 64.055740356445312 64.8 64.109375
		 65.6 64.148391723632813 66.4 64.167045593261719 67.2 64.157981872558594 68 64.136695861816406
		 68.8 64.113014221191406 69.6 64.087425231933594 70.4 64.069877624511719 71.2 64.062095642089844
		 72 64.050796508789063 72.8 64.026817321777344;
createNode animCurveTA -n "Bip01_L_UpperArm_rotateZ11";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 92 ".ktv[0:91]"  0 -22.133827209472656 0.8 -22.023403167724609
		 1.6 -21.934003829956055 2.4 -21.849884033203125 3.2 -21.815160751342773 4 -21.833259582519531
		 4.8 -21.841892242431641 5.6 -21.729982376098633 6.4 -21.514171600341797 7.2 -21.341615676879883
		 8 -21.310110092163089 8.8 -21.329885482788089 9.6 -21.325607299804688 10.4 -21.444040298461911
		 11.2 -21.731052398681641 12 -21.984848022460937 12.8 -22.120878219604492 13.6 -22.267833709716797
		 14.4 -22.441644668579109 15.2 -22.553472518920895 16 -22.653678894042969 16.8 -22.840530395507813
		 17.6 -23.081304550170895 18.4 -23.217870712280273 19.2 -23.26177978515625 20 -23.422389984130859
		 20.8 -23.591962814331055 21.6 -23.536352157592773 22.4 -23.374279022216797 23.2 -23.227754592895508
		 24 -23.133544921875 24.8 -23.168638229370117 25.6 -23.267917633056641 26.4 -23.347436904907227
		 27.2 -23.366046905517578 28 -23.292835235595703 28.8 -23.157943725585938 29.6 -22.98835563659668
		 30.4 -22.859865188598633 31.2 -22.784059524536133 32 -22.574661254882813 32.8 -22.195524215698239
		 33.6 -21.894069671630859 34.4 -21.789386749267575 35.2 -21.716627120971683 36 -21.527050018310547
		 36.8 -21.290349960327148 37.6 -21.178110122680664 38.4 -21.200101852416992 39.2 -21.14863395690918
		 40 -20.905231475830082 40.8 -20.624416351318359 41.6 -20.462301254272461 42.4 -20.369388580322266
		 43.2 -20.255161285400391 44 -20.161802291870117 44.8 -20.182090759277344 45.6 -20.270084381103516
		 46.4 -20.272529602050781 47.2 -20.170680999755859 48 -20.065309524536133 48.8 -20.033489227294918
		 49.6 -20.020784378051765 50.4 -20.077913284301761 51.2 -20.087324142456055 52 -20.136699676513672
		 52.8 -20.348642349243164 53.6 -20.577016830444336 54.4 -20.757461547851559 55.2 -20.962547302246097
		 56 -21.198049545288089 56.8 -21.530363082885746 57.6 -22.08997917175293 58.4 -22.924379348754883
		 59.2 -23.787593841552734 60 -24.269416809082031 60.8 -24.279348373413089 61.6 -24.087265014648437
		 62.4 -24.112346649169918 63.2 -24.488321304321289 64 -24.909330368041992 64.8 -25.227165222167969
		 65.6 -25.534965515136719 66.4 -25.786962509155273 67.2 -25.896652221679687 68 -25.934993743896484
		 68.8 -26.058074951171875 69.6 -26.283294677734375 70.4 -26.423080444335937 71.2 -26.409934997558597
		 72 -26.381227493286133 72.8 -26.329624176025391;
createNode animCurveTA -n "Bip01_L_Forearm_rotateX11";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 92 ".ktv[0:91]"  0 0.99736988544464111 0.8 1.0008258819580078
		 1.6 1.0055408477783203 2.4 1.011250376701355 3.2 1.0145622491836548 4 1.0162976980209353
		 4.8 1.0188220739364624 5.6 1.0239883661270142 6.4 1.0312520265579224 7.2 1.0383428335189819
		 8 1.0421037673950195 8.8 1.0433220863342283 9.6 1.0446521043777466 10.4 1.043521523475647
		 11.2 1.0387569665908811 12 1.0355184078216553 12.8 1.0359828472137451 13.6 1.0358481407165527
		 14.4 1.0342506170272827 15.2 1.0348707437515261 16 1.0374337434768677 16.8 1.0382248163223269
		 17.6 1.0362764596939087 18.4 1.0360449552536013 19.2 1.0386748313903809 20 1.0390238761901855
		 20.8 1.0395605564117432 21.6 1.0460038185119629 22.4 1.0563591718673706 23.2 1.0648652315139775
		 24 1.0712110996246338 24.8 1.0730005502700806 25.6 1.0722596645355225 26.4 1.0722193717956543
		 27.2 1.0744988918304443 28 1.0805960893630979 28.8 1.0871551036834717 29.6 1.0937798023223877
		 30.4 1.1000936031341553 31.2 1.1061767339706421 32 1.1141204833984375 32.8 1.126983642578125
		 33.6 1.1357536315917969 34.4 1.1418750286102295 35.2 1.146897554397583 36 1.1538848876953125
		 36.8 1.162299633026123 37.6 1.1678086519241333 38.4 1.1690866947174072 39.2 1.169888973236084
		 40 1.1748977899551392 40.8 1.1816275119781494 41.6 1.1864387989044187 42.4 1.1902309656143188
		 43.2 1.1936725378036499 44 1.1958481073379517 44.8 1.1943056583404541 45.6 1.1913309097290039
		 46.4 1.1916390657424931 47.2 1.1945468187332151 48 1.1963421106338501 48.8 1.1973580121994021
		 49.6 1.1976786851882937 50.4 1.1965619325637815 51.2 1.196052074432373 52 1.1932199001312256
		 52.8 1.1847671270370483 53.6 1.1757880449295044 54.4 1.1685343980789185 55.2 1.1614121198654177
		 56 1.154808521270752 56.8 1.1455661058425903 57.6 1.1287993192672727 58.4 1.1031694412231443
		 59.2 1.0760840177536013 60 1.0613096952438354 60.8 1.0630828142166138 61.6 1.0716105699539185
		 62.4 1.0730770826339722 63.2 1.0647855997085571 64 1.055493950843811 64.8 1.0479693412780762
		 65.6 1.0397881269454956 66.4 1.0342348814010622 67.2 1.0346170663833618 68 1.0375156402587893
		 68.8 1.0376577377319336 69.6 1.0344802141189575 70.4 1.0334104299545288 71.2 1.0365179777145386
		 72 1.0403558015823364 72.8 1.0454541444778442;
createNode animCurveTA -n "Bip01_L_Forearm_rotateY11";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 92 ".ktv[0:91]"  0 -1.8708659410476685 0.8 -1.8722697496414182
		 1.6 -1.8754339218139648 2.4 -1.879781126976013 3.2 -1.8821084499359133 4 -1.880861759185791
		 4.8 -1.8798617124557493 5.6 -1.8821364641189573 6.4 -1.8872783184051514 7.2 -1.892689108848572
		 8 -1.8959197998046873 8.8 -1.8974183797836304 9.6 -1.8985792398452759 10.4 -1.8974938392639162
		 11.2 -1.8937503099441526 12 -1.8913007974624634 12.8 -1.8916920423507688 13.6 -1.8919339179992676
		 14.4 -1.8917063474655151 15.2 -1.8935364484786985 16 -1.8962736129760747 16.8 -1.8967248201370241
		 17.6 -1.8951519727706907 18.4 -1.8953126668930054 19.2 -1.8974734544754028 20 -1.8971923589706419
		 20.8 -1.8962593078613279 21.6 -1.8993847370147703 22.4 -1.9054251909255979 23.2 -1.9110519886016848
		 24 -1.9153680801391599 24.8 -1.9171053171157839 25.6 -1.9177738428115847 26.4 -1.9187743663787837
		 27.2 -1.9196566343307493 28 -1.9215273857116701 28.8 -1.9238667488098151 29.6 -1.927935004234314
		 30.4 -1.9323745965957642 31.2 -1.9349619150161741 32 -1.937174916267395 32.8 -1.9358739852905271
		 33.6 -1.941030740737915 34.4 -1.9462454319000244 35.2 -1.9510596990585329 36 -1.9552514553070068
		 36.8 -1.9580307006835935 37.6 -1.9587116241455083 38.4 -1.9564932584762571 39.2 -1.957743167877197
		 40 -1.9619817733764648 40.8 -1.9666824340820312 41.6 -1.9693968296051025 42.4 -1.9684096574783327
		 43.2 -1.9697902202606201 44 -1.971670389175415 44.8 -1.9728338718414304 45.6 -1.9730339050292969
		 46.4 -1.9734681844711306 47.2 -1.9740167856216431 48 -1.9734554290771484 48.8 -1.9723327159881592
		 49.6 -1.9709318876266482 50.4 -1.9692238569259644 51.2 -1.9687069654464724 52 -1.9685767889022827
		 52.8 -1.9676781892776491 53.6 -1.9676021337509157 54.4 -1.9665855169296271 55.2 -1.9622398614883421
		 56 -1.9564805030822754 56.8 -1.9499999284744265 57.6 -1.9407727718353271 58.4 -1.9275388717651365
		 59.2 -1.9139723777770989 60 -1.9078414440155027 60.8 -1.9108942747116089 61.6 -1.916014552116394
		 62.4 -1.914575934410095 63.2 -1.9065592288970945 64 -1.8998121023178101 64.8 -1.8970369100570676
		 65.6 -1.8954567909240727 66.4 -1.8951622247695925 67.2 -1.897224545478821 68 -1.8996193408966062
		 68.8 -1.898629426956177 69.6 -1.8968265056610107 70.4 -1.8971457481384277 71.2 -1.8999143838882451
		 72 -1.9011062383651736 72.8 -1.9009739160537718;
createNode animCurveTA -n "Bip01_L_Forearm_rotateZ11";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 92 ".ktv[0:91]"  0 -49.540122985839837 0.8 -49.636470794677734
		 1.6 -49.786411285400391 2.4 -49.975162506103523 3.2 -50.082164764404297 4 -50.103115081787109
		 4.8 -50.145015716552734 5.6 -50.292198181152344 6.4 -50.526588439941413 7.2 -50.760959625244141
		 8 -50.890316009521491 8.8 -50.938358306884766 9.6 -50.984458923339837 10.4 -50.944061279296875
		 11.2 -50.784778594970703 12 -50.678195953369141 12.8 -50.694293975830078 13.6 -50.694507598876953
		 14.4 -50.655593872070313 15.2 -50.695415496826179 16 -50.791362762451165 16.8 -50.815296173095703
		 17.6 -50.749454498291016 18.4 -50.7467041015625 19.2 -50.835884094238281 20 -50.83978271484375
		 20.8 -50.838336944580078 21.6 -51.025814056396477 22.4 -51.341903686523438 23.2 -51.610755920410163
		 24 -51.813018798828132 24.8 -51.877651214599609 25.6 -51.870620727539063 26.4 -51.884082794189453
		 27.2 -51.947250366210937 28 -52.109416961669922 28.8 -52.288833618164062 29.6 -52.493762969970703
		 30.4 -52.697162628173821 31.2 -52.869842529296882 32 -53.079231262207031 32.8 -53.358734130859375
		 33.6 -53.626827239990234 34.4 -53.83428955078125 35.2 -54.011707305908203 36 -54.226421356201165
		 36.8 -54.456638336181648 37.6 -54.593441009521491 38.4 -54.596385955810547 39.2 -54.630355834960938
		 40 -54.799186706542969 40.8 -55.013607025146477 41.6 -55.159366607666016 42.4 -55.236663818359375
		 43.2 -55.334255218505859 44 -55.408031463623047 44.8 -55.38579177856446 45.6 -55.318397521972656
		 46.4 -55.331005096435547 47.2 -55.405593872070313 48 -55.440887451171875 48.8 -55.451446533203125
		 49.6 -55.442440032958984 50.4 -55.396167755126953 51.2 -55.378353118896477 52 -55.309776306152344
		 52.8 -55.100826263427734 53.6 -54.890819549560547 54.4 -54.709873199462891 55.2 -54.489959716796875
		 56 -54.263603210449219 56.8 -53.966869354248047 57.6 -53.461986541748047 58.4 -52.701393127441406
		 59.2 -51.903827667236328 60 -51.486763000488281 60.8 -51.569057464599609 61.6 -51.831325531005859
		 62.4 -51.8447265625 63.2 -51.547840118408203 64 -51.245807647705078 64.8 -51.038219451904297
		 65.6 -50.832626342773438 66.4 -50.704936981201165 67.2 -50.743301391601562 68 -50.842025756835938
		 68.8 -50.830387115478509 69.6 -50.734371185302734 70.4 -50.715599060058594 71.2 -50.824432373046875
		 72 -50.926204681396477 72.8 -51.037006378173821;
createNode animCurveTA -n "Bip01_L_Hand_rotateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 92 ".ktv[0:91]"  0 -90.133308410644531 0.8 -90.133316040039063
		 1.6 -90.133316040039063 2.4 -90.133316040039063 3.2 -90.133316040039063 4 -90.133316040039063
		 4.8 -90.133316040039063 5.6 -90.133308410644531 6.4 -90.133308410644531 7.2 -90.133316040039063
		 8 -90.133316040039063 8.8 -90.133316040039063 9.6 -90.133316040039063 10.4 -90.133316040039063
		 11.2 -90.133316040039063 12 -90.133316040039063 12.8 -90.133316040039063 13.6 -90.133316040039063
		 14.4 -90.133308410644531 15.2 -90.133316040039063 16 -90.133316040039063 16.8 -90.133316040039063
		 17.6 -90.133316040039063 18.4 -90.133316040039063 19.2 -90.133316040039063 20 -90.133316040039063
		 20.8 -90.133316040039063 21.6 -90.133308410644531 22.4 -90.133316040039063 23.2 -90.133316040039063
		 24 -90.133316040039063 24.8 -90.133316040039063 25.6 -90.133316040039063 26.4 -90.133316040039063
		 27.2 -90.133316040039063 28 -90.133308410644531 28.8 -90.133316040039063 29.6 -90.133316040039063
		 30.4 -90.133316040039063 31.2 -90.133316040039063 32 -90.133316040039063 32.8 -90.133308410644531
		 33.6 -90.133316040039063 34.4 -90.133316040039063 35.2 -90.133316040039063 36 -90.133316040039063
		 36.8 -90.133308410644531 37.6 -90.133316040039063 38.4 -90.133316040039063 39.2 -90.133308410644531
		 40 -90.133308410644531 40.8 -90.133316040039063 41.6 -90.133316040039063 42.4 -90.133316040039063
		 43.2 -90.133308410644531 44 -90.133308410644531 44.8 -90.133316040039063 45.6 -90.133316040039063
		 46.4 -90.133316040039063 47.2 -90.133308410644531 48 -90.133308410644531 48.8 -90.133316040039063
		 49.6 -90.133316040039063 50.4 -90.133308410644531 51.2 -90.133316040039063 52 -90.133316040039063
		 52.8 -90.133316040039063 53.6 -90.133308410644531 54.4 -90.133316040039063 55.2 -90.133308410644531
		 56 -90.133316040039063 56.8 -90.133308410644531 57.6 -90.133316040039063 58.4 -90.133316040039063
		 59.2 -90.133316040039063 60 -90.133308410644531 60.8 -90.133308410644531 61.6 -90.133316040039063
		 62.4 -90.133316040039063 63.2 -90.133316040039063 64 -90.133316040039063 64.8 -90.133308410644531
		 65.6 -90.133316040039063 66.4 -90.133316040039063 67.2 -90.133316040039063 68 -90.133316040039063
		 68.8 -90.133308410644531 69.6 -90.133316040039063 70.4 -90.133316040039063 71.2 -90.133308410644531
		 72 -90.133316040039063 72.8 -90.133316040039063;
createNode animCurveTA -n "Bip01_L_Hand_rotateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 92 ".ktv[0:91]"  0 6.3003835678100586 0.8 6.3003978729248047
		 1.6 6.3003911972045898 2.4 6.3004007339477539 3.2 6.3003964424133301 4 6.3003988265991211
		 4.8 6.3003988265991211 5.6 6.3003969192504883 6.4 6.3003983497619629 7.2 6.3003954887390137
		 8 6.3004074096679687 8.8 6.3003988265991211 9.6 6.3003959655761728 10.4 6.3003969192504883
		 11.2 6.3003921508789063 12 6.3004045486450195 12.8 6.3003945350646973 13.6 6.3003935813903809
		 14.4 6.3004021644592285 15.2 6.300407886505127 16 6.3003950119018555 16.8 6.3003964424133301
		 17.6 6.3003931045532227 18.4 6.3003964424133301 19.2 6.3003926277160645 20 6.3003926277160645
		 20.8 6.3003945350646973 21.6 6.3003993034362793 22.4 6.3004002571105957 23.2 6.300410270690918
		 24 6.3003907203674316 24.8 6.3003983497619629 25.6 6.3003964424133301 26.4 6.3003954887390137
		 27.2 6.3004007339477539 28 6.3003945350646973 28.8 6.3004016876220703 29.6 6.3003926277160645
		 30.4 6.3003964424133301 31.2 6.3003907203674316 32 6.3003940582275391 32.8 6.3004002571105957
		 33.6 6.3004035949707031 34.4 6.3003950119018555 35.2 6.3003864288330078 36 6.3003964424133301
		 36.8 6.3004002571105957 37.6 6.3003954887390137 38.4 6.3003926277160645 39.2 6.3003954887390137
		 40 6.3003997802734375 40.8 6.3003978729248047 41.6 6.3003940582275391 42.4 6.3003964424133301
		 43.2 6.3004007339477539 44 6.3004021644592285 44.8 6.3003926277160645 45.6 6.3003964424133301
		 46.4 6.3003926277160645 47.2 6.3004007339477539 48 6.3003988265991211 48.8 6.3003978729248047
		 49.6 6.3003926277160645 50.4 6.3003964424133301 51.2 6.3004012107849121 52 6.3003931045532227
		 52.8 6.3003897666931152 53.6 6.3003883361816406 54.4 6.3003969192504883 55.2 6.3004007339477539
		 56 6.3003945350646973 56.8 6.3003926277160645 57.6 6.3003964424133301 58.4 6.3003897666931152
		 59.2 6.3004002571105957 60 6.3004012107849121 60.8 6.3003935813903809 61.6 6.3004007339477539
		 62.4 6.3003969192504883 63.2 6.3003973960876465 64 6.300389289855957 64.8 6.3004007339477539
		 65.6 6.3004021644592285 66.4 6.3003964424133301 67.2 6.3003921508789063 68 6.3003997802734375
		 68.8 6.3003964424133301 69.6 6.3004012107849121 70.4 6.3003964424133301 71.2 6.3003969192504883
		 72 6.3003907203674316 72.8 6.3003978729248047;
createNode animCurveTA -n "Bip01_L_Hand_rotateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 92 ".ktv[0:91]"  0 2.5653407573699951 0.8 2.5653266906738281
		 1.6 2.565338134765625 2.4 2.565333366394043 3.2 2.5653312206268311 4 2.5653376579284668
		 4.8 2.5653345584869385 5.6 2.5653297901153564 6.4 2.5653371810913086 7.2 2.5653355121612549
		 8 2.5653252601623535 8.8 2.5653314590454106 9.6 2.5653395652770996 10.4 2.5653393268585205
		 11.2 2.5653359889984131 12 2.5653336048126221 12.8 2.5653378963470463 13.6 2.5653364658355713
		 14.4 2.5653278827667236 15.2 2.5653250217437744 16 2.5653336048126221 16.8 2.5653364658355713
		 17.6 2.5653314590454106 18.4 2.5653328895568848 19.2 2.5653362274169922 20 2.5653352737426758
		 20.8 2.5653316974639893 21.6 2.5653345584869385 22.4 2.5653285980224609 23.2 2.5653257369995117
		 24 2.5653319358825684 24.8 2.5653312206268311 25.6 2.5653350353240967 26.4 2.5653383731842041
		 27.2 2.5653321743011475 28 2.5653290748596191 28.8 2.5653362274169922 29.6 2.5653345584869385
		 30.4 2.5653376579284668 31.2 2.5653359889984131 32 2.565335750579834 32.8 2.5653319358825684
		 33.6 2.5653290748596191 34.4 2.5653393268585205 35.2 2.5653328895568848 36 2.5653328895568848
		 36.8 2.5653307437896729 37.6 2.5653319358825684 38.4 2.5653321743011475 39.2 2.5653328895568848
		 40 2.565333366394043 40.8 2.5653312206268311 41.6 2.5653297901153564 42.4 2.5653336048126221
		 43.2 2.5653386116027832 44 2.5653319358825684 44.8 2.5653338432312016 45.6 2.5653345584869385
		 46.4 2.5653297901153564 47.2 2.5653302669525142 48 2.5653259754180908 48.8 2.5653269290924072
		 49.6 2.5653343200683594 50.4 2.5653321743011475 51.2 2.565333366394043 52 2.5653436183929443
		 52.8 2.5653362274169922 53.6 2.5653376579284668 54.4 2.5653331279754639 55.2 2.565333366394043
		 56 2.5653338432312016 56.8 2.565333366394043 57.6 2.5653300285339355 58.4 2.5653319358825684
		 59.2 2.5653369426727295 60 2.5653345584869385 60.8 2.5653350353240967 61.6 2.5653283596038814
		 62.4 2.5653359889984131 63.2 2.5653305053710938 64 2.5653359889984131 64.8 2.5653319358825684
		 65.6 2.5653314590454106 66.4 2.5653276443481445 67.2 2.5653326511383057 68 2.5653355121612549
		 68.8 2.5653369426727295 69.6 2.5653278827667236 70.4 2.5653312206268311 71.2 2.5653407573699951
		 72 2.5653326511383057 72.8 2.5653338432312016;
createNode animCurveTA -n "Bip01_L_Thigh_rotateX11";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 92 ".ktv[0:91]"  0 -4.5387148857116699 0.8 -4.4134769439697266
		 1.6 -4.3393282890319824 2.4 -4.2851786613464355 3.2 -4.2055110931396484 4 -4.1076207160949707
		 4.8 -4.0179815292358398 5.6 -3.931429386138916 6.4 -3.8428280353546143 7.2 -3.7469048500061026
		 8 -3.6648333072662354 8.8 -3.5975422859191895 9.6 -3.5236396789550781 10.4 -3.474079847335815
		 11.2 -3.4394917488098145 12 -3.4258861541748047 12.8 -3.4045486450195313 13.6 -3.372391939163208
		 14.4 -3.3367931842803955 15.2 -3.3133792877197266 16 -3.3075599670410156 16.8 -3.300128698348999
		 17.6 -3.273327112197876 18.4 -3.2524540424346924 19.2 -3.2679994106292725 20 -3.3028061389923096
		 20.8 -3.2930526733398442 21.6 -3.287369966506958 22.4 -3.3056473731994629 23.2 -3.3462734222412109
		 24 -3.3692774772644043 24.8 -3.4158866405487061 25.6 -3.4732604026794434 26.4 -3.5318360328674316
		 27.2 -3.5778441429138184 28 -3.635600328445435 28.8 -3.6901869773864751 29.6 -3.7504019737243648
		 30.4 -3.7970659732818608 31.2 -3.8331840038299561 32 -3.8790555000305162 32.8 -3.9545426368713379
		 33.6 -4.0345377922058105 34.4 -4.0774316787719727 35.2 -4.1051621437072754 36 -4.1563339233398437
		 36.8 -4.2133793830871582 37.6 -4.2781295776367187 38.4 -4.3455572128295898 39.2 -4.3856096267700195
		 40 -4.413750171661377 40.8 -4.4428191184997559 41.6 -4.4792652130126953 42.4 -4.5437421798706055
		 43.2 -4.5637955665588379 44 -4.6154589653015137 44.8 -4.6653742790222177 45.6 -4.7317090034484863
		 46.4 -4.804901123046875 47.2 -4.8717551231384277 48 -4.9327621459960937 48.8 -4.9630608558654785
		 49.6 -4.9787707328796387 50.4 -5.0245327949523926 51.2 -5.0854582786560059 52 -5.1704254150390625
		 52.8 -5.2414870262145996 53.6 -5.3170289993286133 54.4 -5.3896474838256836 55.2 -5.4612278938293457
		 56 -5.5431351661682129 56.8 -5.5970664024353027 57.6 -5.6682915687561035 58.4 -5.7394623756408691
		 59.2 -5.8088021278381348 60 -5.8804101943969727 60.8 -5.9381389617919922 61.6 -5.9934906959533691
		 62.4 -6.0552353858947754 63.2 -6.1296572685241699 64 -6.2115378379821777 64.8 -6.2709875106811523
		 65.6 -6.3277325630187988 66.4 -6.3882756233215332 67.2 -6.4377536773681641 68 -6.484717845916748
		 68.8 -6.5149345397949219 69.6 -6.5414905548095703 70.4 -6.5858044624328613 71.2 -6.6317033767700195
		 72 -6.6791048049926758 72.8 -6.7108244895935059;
createNode animCurveTA -n "Bip01_L_Thigh_rotateY11";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 92 ".ktv[0:91]"  0 -177.28048706054687 0.8 -177.23451232910156
		 1.6 -177.23757934570312 2.4 -177.26187133789065 3.2 -177.26846313476562 4 -177.26351928710935
		 4.8 -177.24551391601562 5.6 -177.24250793457034 6.4 -177.24664306640625 7.2 -177.24435424804687
		 8 -177.23768615722656 8.8 -177.26295471191406 9.6 -177.28504943847656 10.4 -177.31657409667969
		 11.2 -177.37928771972656 12 -177.45863342285156 12.8 -177.53402709960935 13.6 -177.59637451171878
		 14.4 -177.64100646972656 15.2 -177.70515441894531 16 -177.76679992675781 16.8 -177.82937622070312
		 17.6 -177.86546325683594 18.4 -177.8895263671875 19.2 -177.94819641113281 20 -178.01712036132812
		 20.8 -178.04786682128906 21.6 -178.04576110839844 22.4 -178.08152770996094 23.2 -178.11457824707031
		 24 -178.13710021972656 24.8 -178.17874145507812 25.6 -178.22285461425781 26.4 -178.26089477539062
		 27.2 -178.290771484375 28 -178.32388305664062 28.8 -178.35699462890625 29.6 -178.38812255859375
		 30.4 -178.42314147949219 31.2 -178.43907165527344 32 -178.46525573730469 32.8 -178.52593994140625
		 33.6 -178.58673095703125 34.4 -178.6209716796875 35.2 -178.64419555664062 36 -178.68540954589844
		 36.8 -178.72926330566406 37.6 -178.7861328125 38.4 -178.8521728515625 39.2 -178.88987731933594
		 40 -178.89964294433594 40.8 -178.91265869140625 41.6 -178.94439697265625 42.4 -178.97685241699219
		 43.2 -178.99374389648435 44 -179.00297546386719 44.8 -179.03807067871094 45.6 -179.05288696289062
		 46.4 -179.095458984375 47.2 -179.11820983886719 48 -179.12544250488281 48.8 -179.10879516601562
		 49.6 -179.06410217285156 50.4 -179.03462219238281 51.2 -179.03788757324219 52 -179.03623962402344
		 52.8 -179.02749633789062 53.6 -179.01776123046875 54.4 -179.00642395019531 55.2 -178.99284362792969
		 56 -178.95025634765625 56.8 -178.90957641601562 57.6 -178.86907958984375 58.4 -178.826416015625
		 59.2 -178.78031921386722 60 -178.71023559570312 60.8 -178.65534973144534 61.6 -178.59365844726562
		 62.4 -178.53648376464844 63.2 -178.50276184082031 64 -178.44859313964844 64.8 -178.403076171875
		 65.6 -178.35121154785156 66.4 -178.31231689453125 67.2 -178.26016235351562 68 -178.21524047851562
		 68.8 -178.15852355957034 69.6 -178.09701538085935 70.4 -178.0614013671875 71.2 -178.05751037597656
		 72 -178.05484008789065 72.8 -178.04577636718753;
createNode animCurveTA -n "Bip01_L_Thigh_rotateZ11";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 92 ".ktv[0:91]"  0 -83.580169677734375 0.8 -83.657440185546875
		 1.6 -83.733856201171875 2.4 -83.800979614257813 3.2 -83.867782592773438 4 -83.928321838378906
		 4.8 -83.997718811035156 5.6 -84.076675415039063 6.4 -84.151969909667969 7.2 -84.235191345214844
		 8 -84.312477111816406 8.8 -84.381462097167969 9.6 -84.426925659179673 10.4 -84.441268920898438
		 11.2 -84.464111328125 12 -84.496841430664063 12.8 -84.560882568359375 13.6 -84.6119384765625
		 14.4 -84.6463623046875 15.2 -84.677810668945313 16 -84.730293273925781 16.8 -84.765457153320327
		 17.6 -84.789962768554687 18.4 -84.799720764160156 19.2 -84.80245208740233 20 -84.813377380371094
		 20.8 -84.839279174804687 21.6 -84.874382019042983 22.4 -84.88968658447267 23.2 -84.902664184570341
		 24 -84.903839111328125 24.8 -84.8868408203125 25.6 -84.832229614257813 26.4 -84.777641296386719
		 27.2 -84.778465270996108 28 -84.803215026855469 28.8 -84.8135986328125 29.6 -84.801109313964844
		 30.4 -84.793106079101563 31.2 -84.82157135009767 32 -84.837417602539063 32.8 -84.783760070800781
		 33.6 -84.742042541503906 34.4 -84.752342224121094 35.2 -84.797775268554687 36 -84.81854248046875
		 36.8 -84.837310791015625 37.6 -84.835235595703125 38.4 -84.803352355957017 39.2 -84.781219482421889
		 40 -84.777778625488281 40.8 -84.7723388671875 41.6 -84.763053894042983 42.4 -84.751144409179673
		 43.2 -84.760398864746094 44 -84.748924255371094 44.8 -84.727508544921875 45.6 -84.72332763671875
		 46.4 -84.707427978515625 47.2 -84.691421508789063 48 -84.63797760009767 48.8 -84.598770141601563
		 49.6 -84.601806640625 50.4 -84.587600708007812 51.2 -84.54888916015625 52 -84.510337829589844
		 52.8 -84.467361450195313 53.6 -84.436790466308594 54.4 -84.399635314941406 55.2 -84.360183715820327
		 56 -84.331550598144531 56.8 -84.303024291992188 57.6 -84.27606201171875 58.4 -84.213249206542983
		 59.2 -84.150192260742188 60 -84.097412109375 60.8 -84.084487915039063 61.6 -84.055747985839844
		 62.4 -84.020446777343764 63.2 -83.969551086425781 64 -83.94873046875 64.8 -83.949302673339844
		 65.6 -83.927833557128906 66.4 -83.897315979003906 67.2 -83.875732421875 68 -83.863433837890625
		 68.8 -83.85089111328125 69.6 -83.844635009765625 70.4 -83.845024108886719 71.2 -83.856765747070327
		 72 -83.845130920410156 72.8 -83.849174499511719;
createNode animCurveTA -n "Bip01_L_Calf_rotateX11";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 92 ".ktv[0:91]"  0 0.91347563266754161 0.8 0.91366744041442871
		 1.6 0.91381275653839089 2.4 0.91392129659652721 3.2 0.91395270824432384 4 0.91402441263198853
		 4.8 0.91406142711639404 5.6 0.91414040327072144 6.4 0.91425013542175293 7.2 0.91446667909622181
		 8 0.91450357437133789 8.8 0.91447114944458019 9.6 0.91431236267089844 10.4 0.91411787271499634
		 11.2 0.91415888071060181 12 0.91406422853469838 12.8 0.91399556398391735 13.6 0.91389888525009155
		 14.4 0.91381973028182995 15.2 0.91383850574493408 16 0.9138864278793335 16.8 0.91388684511184692
		 17.6 0.91385811567306507 18.4 0.91390079259872448 19.2 0.91386091709136974 20 0.91385501623153698
		 20.8 0.91400319337844849 21.6 0.91416567564010631 22.4 0.91425639390945435 23.2 0.91431832313537598
		 24 0.91451615095138561 24.8 0.91455888748168956 25.6 0.91449612379074097 26.4 0.91433113813400257
		 27.2 0.91427242755889904 28 0.91433089971542358 28.8 0.91438019275665283 29.6 0.91441422700881947
		 30.4 0.91441690921783425 31.2 0.91438204050064087 32 0.91443055868148804 32.8 0.9142931103706361
		 33.6 0.91418075561523426 34.4 0.91423326730728161 35.2 0.91418856382370006 36 0.91423410177230835
		 36.8 0.91409844160079967 37.6 0.91396719217300415 38.4 0.91372847557067904 39.2 0.91351920366287243
		 40 0.9134594202041626 40.8 0.91331565380096402 41.6 0.91312921047210693 42.4 0.91295635700225852
		 43.2 0.91276007890701283 44 0.9125293493270874 44.8 0.91225045919418335 45.6 0.91201567649841309
		 46.4 0.91178208589553822 47.2 0.91157084703445446 48 0.91126596927642822 48.8 0.91105860471725453
		 49.6 0.91097396612167358 50.4 0.91077876091003429 51.2 0.9104437828063966 52 0.91016131639480591
		 52.8 0.91000282764434814 53.6 0.90984785556793213 54.4 0.90965533256530762 55.2 0.9093608856201173
		 56 0.90926271677017212 56.8 0.90908545255661 57.6 0.90892863273620605 58.4 0.90870559215545665
		 59.2 0.90851604938507069 60 0.90838909149169922 60.8 0.90838181972503684 61.6 0.90832644701004028
		 62.4 0.90819919109344482 63.2 0.90804487466812123 64 0.90802192687988303 64.8 0.90816700458526622
		 65.6 0.90809673070907615 66.4 0.9079761505126952 67.2 0.90794646739959728 68 0.90801370143890403
		 68.8 0.90801554918289196 69.6 0.907975614070892 70.4 0.90794807672500588 71.2 0.90808671712875344
		 72 0.90806764364242565 72.8 0.90806835889816295;
createNode animCurveTA -n "Bip01_L_Calf_rotateY11";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 92 ".ktv[0:91]"  0 -0.20596584677696228 0.8 -0.20500276982784271
		 1.6 -0.20410607755184176 2.4 -0.20340089499950409 3.2 -0.20302997529506683 4 -0.20265001058578491
		 4.8 -0.20234832167625427 5.6 -0.20186147093772888 6.4 -0.20131239295005801 7.2 -0.20016142725944519
		 8 -0.19984391331672668 8.8 -0.19994118809700012 9.6 -0.20073303580284119 10.4 -0.20180974900722504
		 11.2 -0.20165348052978516 12 -0.20195092260837555 12.8 -0.20217011868953705 13.6 -0.20256201922893524
		 14.4 -0.20296996831893921 15.2 -0.20295010507106781 16 -0.20273587107658383 16.8 -0.20264708995819092
		 17.6 -0.20271185040473938 18.4 -0.20258861780166623 19.2 -0.20281265676021576 20 -0.2028052806854248
		 20.8 -0.20219248533248901 21.6 -0.20140708982944489 22.4 -0.20085428655147552 23.2 -0.20056344568729401
		 24 -0.1995137482881546 24.8 -0.19931034743785855 25.6 -0.19987866282463071 26.4 -0.20075668394565585
		 27.2 -0.20097503066062927 28 -0.20061653852462769 28.8 -0.20023299753665927 29.6 -0.2000633031129837
		 30.4 -0.19992537796497345 31.2 -0.19991412758827207 32 -0.1996183842420578 32.8 -0.20031203329563141
		 33.6 -0.20100018382072449 34.4 -0.20076307654380801 35.2 -0.20104540884494779 36 -0.20076672732830048
		 36.8 -0.20133344829082489 37.6 -0.20193316042423248 38.4 -0.20320022106170657 39.2 -0.20443263649940491
		 40 -0.20470567047595975 40.8 -0.20552255213260651 41.6 -0.20645603537559509 42.4 -0.20734141767024991
		 43.2 -0.20824199914932251 44 -0.20937927067279816 44.8 -0.21072496473789218 45.6 -0.2119036465883255
		 46.4 -0.21284842491149905 47.2 -0.21404659748077393 48 -0.21560290455818176 48.8 -0.21678620576858521
		 49.6 -0.21734306216239929 50.4 -0.21823678910732269 51.2 -0.2196635901927948 52 -0.22087569534778595
		 52.8 -0.22177350521087649 53.6 -0.22268043458461759 54.4 -0.22356702387332916 55.2 -0.22501885890960693
		 56 -0.22547848522663111 56.8 -0.22616256773471827 57.6 -0.22694478929042816 58.4 -0.22802247107028961
		 59.2 -0.22909969091415405 60 -0.22975490987300873 60.8 -0.22983936965465546 61.6 -0.23010687530040733
		 62.4 -0.23066280782222748 63.2 -0.23131990432739255 64 -0.2313986271619797 64.8 -0.2307423800230026
		 65.6 -0.23108901083469388 66.4 -0.23164518177509308 67.2 -0.23194208741188049 68 -0.2316609472036362
		 68.8 -0.23171599209308624 69.6 -0.2319491505622864 70.4 -0.23212756216526031 71.2 -0.23146335780620572
		 72 -0.23160251975059507 72.8 -0.23171433806419373;
createNode animCurveTA -n "Bip01_L_Calf_rotateZ11";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 92 ".ktv[0:91]"  0 -75.541923522949219 0.8 -75.596145629882812
		 1.6 -75.626472473144531 2.4 -75.62664794921875 3.2 -75.618865966796875 4 -75.63519287109375
		 4.8 -75.631057739257812 5.6 -75.653633117675781 6.4 -75.674407958984375 7.2 -75.73468017578125
		 8 -75.7322998046875 8.8 -75.715385437011719 9.6 -75.673576354980469 10.4 -75.622390747070298
		 11.2 -75.641944885253906 12 -75.587188720703125 12.8 -75.55706787109375 13.6 -75.512939453125
		 14.4 -75.498443603515625 15.2 -75.503425598144531 16 -75.517868041992188 16.8 -75.507537841796875
		 17.6 -75.504234313964844 18.4 -75.525009155273437 19.2 -75.507659912109375 20 -75.505683898925781
		 20.8 -75.557723999023452 21.6 -75.615211486816406 22.4 -75.637992858886719 23.2 -75.652900695800781
		 24 -75.699790954589844 24.8 -75.720077514648438 25.6 -75.720848083496094 26.4 -75.694107055664062
		 27.2 -75.662857055664063 28 -75.662384033203125 28.8 -75.673789978027344 29.6 -75.680709838867188
		 30.4 -75.671302795410156 31.2 -75.639289855957017 32 -75.6448974609375 32.8 -75.604339599609375
		 33.6 -75.585159301757813 34.4 -75.599037170410156 35.2 -75.597450256347642 36 -75.601760864257813
		 36.8 -75.55218505859375 37.6 -75.49969482421875 38.4 -75.43991851806642 39.2 -75.398170471191406
		 40 -75.383476257324219 40.8 -75.348640441894531 41.6 -75.301643371582031 42.4 -75.254646301269531
		 43.2 -75.189247131347642 44 -75.133407592773438 44.8 -75.052574157714844 45.6 -74.983566284179673
		 46.4 -74.902420043945313 47.2 -74.864990234375 48 -74.805351257324219 48.8 -74.766868591308594
		 49.6 -74.761894226074219 50.4 -74.708099365234375 51.2 -74.59637451171875 52 -74.514480590820327
		 52.8 -74.488059997558594 53.6 -74.472808837890625 54.4 -74.426116943359375 55.2 -74.359390258789063
		 56 -74.333656311035156 56.8 -74.282630920410156 57.6 -74.244087219238281 58.4 -74.202224731445313
		 59.2 -74.17745208740233 60 -74.154762268066406 60.8 -74.168403625488281 61.6 -74.153099060058594
		 62.4 -74.119964599609389 63.2 -74.073310852050781 64 -74.072799682617202 64.8 -74.10736083984375
		 65.6 -74.084747314453125 66.4 -74.0660400390625 67.2 -74.078041076660156 68 -74.095024108886719
		 68.8 -74.106346130371094 69.6 -74.109970092773438 70.4 -74.111129760742188 71.2 -74.140060424804688
		 72 -74.139114379882813 72.8 -74.146629333496094;
createNode animCurveTA -n "Bip01_L_Foot_rotateX11";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 92 ".ktv[0:91]"  0 -11.353065490722656 0.8 -11.240264892578125
		 1.6 -11.646015167236328 2.4 -12.159531593322756 3.2 -12.372209548950195 4 -12.43639087677002
		 4.8 -12.495802879333498 5.6 -12.606395721435549 6.4 -12.69443416595459 7.2 -12.781049728393556
		 8 -12.834177017211914 8.8 -12.929619789123535 9.6 -12.949661254882812 10.4 -12.971149444580078
		 11.2 -13.089632034301758 12 -13.389676094055176 12.8 -13.676964759826662 13.6 -13.701383590698244
		 14.4 -13.541358947753906 15.2 -13.485325813293455 16 -13.53006076812744 16.8 -13.594841957092283
		 17.6 -13.530298233032228 18.4 -13.444580078125 19.2 -13.507084846496582 20 -13.513195991516112
		 20.8 -13.234445571899416 21.6 -12.930710792541504 22.4 -12.922183990478516 23.2 -13.02302360534668
		 24 -13.011482238769533 24.8 -12.992180824279783 25.6 -13.000146865844728 26.4 -13.021570205688477
		 27.2 -13.073073387145996 28 -13.148069381713867 28.8 -13.186662673950195 29.6 -13.202264785766602
		 30.4 -13.221710205078123 31.2 -13.083301544189451 32 -13.048495292663574 32.8 -13.439836502075195
		 33.6 -13.80612087249756 34.4 -13.761889457702637 35.2 -13.64560604095459 36 -13.666970252990724
		 36.8 -13.705918312072756 37.6 -13.849364280700684 38.4 -14.021805763244627 39.2 -14.043870925903322
		 40 -13.982488632202148 40.8 -13.95628833770752 41.6 -13.990302085876465 42.4 -13.970280647277832
		 43.2 -13.891086578369141 44 -13.894744873046877 44.8 -13.945821762084959 45.6 -14.065042495727541
		 46.4 -14.184682846069338 47.2 -14.200993537902832 48 -14.131075859069824 48.8 -13.892434120178224
		 49.6 -13.559826850891112 50.4 -13.398398399353027 51.2 -13.445091247558594 52 -13.500419616699221
		 52.8 -13.471391677856444 53.6 -13.392191886901855 54.4 -13.250818252563477 55.2 -13.215076446533203
		 56 -13.170015335083008 56.8 -13.164219856262209 57.6 -13.103474617004396 58.4 -12.975478172302246
		 59.2 -12.795105934143066 60 -12.577872276306152 60.8 -12.477683067321776 61.6 -12.335672378540041
		 62.4 -12.374512672424316 63.2 -12.527661323547365 64 -12.638424873352053 64.8 -12.613478660583496
		 65.6 -12.509239196777344 66.4 -12.46494770050049 67.2 -12.380232810974119 68 -12.346977233886721
		 68.8 -12.278584480285646 69.6 -12.153285980224609 70.4 -12.061421394348145 71.2 -12.03084659576416
		 72 -12.022536277770996 72.8 -12.012774467468262;
createNode animCurveTA -n "Bip01_L_Foot_rotateY11";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 92 ".ktv[0:91]"  0 1.5662143230438232 0.8 1.5168560743331909
		 1.6 1.6898577213287354 2.4 1.9541913270950317 3.2 2.041980504989624 4 2.0616166591644287
		 4.8 2.1234526634216309 5.6 2.1741480827331543 6.4 2.2211589813232422 7.2 2.2575614452362061
		 8 2.3005404472351074 8.8 2.3292543888092041 9.6 2.357153177261353 10.4 2.4074447154998779
		 11.2 2.4538300037384033 12 2.5881795883178711 12.8 2.7030375003814697 13.6 2.6850078105926514
		 14.4 2.6502981185913086 15.2 2.6151838302612305 16 2.6363489627838135 16.8 2.6687633991241455
		 17.6 2.6026277542114258 18.4 2.5892119407653809 19.2 2.6084845066070557 20 2.6293880939483643
		 20.8 2.4808003902435303 21.6 2.3319780826568604 22.4 2.3540563583374023 23.2 2.3815629482269287
		 24 2.3694798946380615 24.8 2.3673257827758789 25.6 2.368863582611084 26.4 2.3901522159576416
		 27.2 2.3866961002349858 28 2.4120781421661377 28.8 2.4189403057098393 29.6 2.4227044582366943
		 30.4 2.3866510391235352 31.2 2.3153185844421387 32 2.3041071891784668 32.8 2.477226734161377
		 33.6 2.6662642955780029 34.4 2.6353223323822021 35.2 2.5735838413238525 36 2.586051225662231
		 36.8 2.5827095508575439 37.6 2.6295115947723389 38.4 2.7141015529632568 39.2 2.7294299602508545
		 40 2.6919336318969727 40.8 2.6923563480377197 41.6 2.6986575126647949 42.4 2.6749062538146973
		 43.2 2.6335740089416508 44 2.6216185092926025 44.8 2.6477437019348145 45.6 2.6862118244171143
		 46.4 2.738008975982666 47.2 2.7460732460021973 48 2.706598281860352 48.8 2.6178789138793945
		 49.6 2.4661643505096436 50.4 2.3425700664520264 51.2 2.3748538494110107 52 2.4048304557800293
		 52.8 2.3872456550598145 53.6 2.3495283126831055 54.4 2.2904419898986816 55.2 2.245866060256958
		 56 2.2678544521331787 56.8 2.2390151023864746 57.6 2.20011305809021 58.4 2.1296803951263428
		 59.2 2.0403320789337158 60 1.9468532800674436 60.8 1.9175989627838139 61.6 1.8662322759628296
		 62.4 1.8369152545928955 63.2 1.8983073234558108 64 1.9664764404296875 64.8 1.9524374008178711
		 65.6 1.8878518342971804 66.4 1.8598228693008421 67.2 1.8074816465377803 68 1.8037555217742918
		 68.8 1.7882668972015381 69.6 1.731357216835022 70.4 1.6823031902313232 71.2 1.6622577905654907
		 72 1.6583430767059326 72.8 1.6504274606704712;
createNode animCurveTA -n "Bip01_L_Foot_rotateZ11";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 92 ".ktv[0:91]"  0 -9.2375745773315447 0.8 -9.1893005371093768
		 1.6 -9.2078075408935547 2.4 -9.2589263916015625 3.2 -9.3035745620727539 4 -9.3664884567260742
		 4.8 -9.4360246658325195 5.6 -9.4640312194824219 6.4 -9.4404668807983398 7.2 -9.4458265304565447
		 8 -9.5360450744628906 8.8 -9.6245355606079102 9.6 -9.5796861648559553 10.4 -9.4340381622314453
		 11.2 -9.3469114303588867 12 -9.4464082717895508 12.8 -9.6247615814208984 13.6 -9.7224712371826172
		 14.4 -9.7478017807006836 15.2 -9.7866573333740234 16 -9.8483352661132812 16.8 -9.8957357406616211
		 17.6 -9.8766326904296875 18.4 -9.8769836425781232 19.2 -9.9109506607055664 20 -9.9309349060058594
		 20.8 -9.9405794143676758 21.6 -9.9429779052734375 22.4 -9.9441843032836914 23.2 -9.9806623458862305
		 24 -10.019528388977053 24.8 -9.9516935348510742 25.6 -9.7565584182739258 26.4 -9.6378602981567383
		 27.2 -9.75946044921875 28 -9.9718589782714844 28.8 -10.098372459411619 29.6 -10.145593643188477
		 30.4 -10.264425277709959 31.2 -10.566605567932127 32 -10.783607482910156 32.8 -10.632389068603516
		 33.6 -10.389039993286133 34.4 -10.323017120361328 35.2 -10.384159088134766 36 -10.49350643157959
		 36.8 -10.64673900604248 37.6 -10.762418746948242 38.4 -10.689208984375 39.2 -10.533849716186523
		 40 -10.493618011474609 40.8 -10.472148895263672 41.6 -10.424001693725586 42.4 -10.461170196533203
		 43.2 -10.563143730163574 44 -10.62934398651123 44.8 -10.680012702941896 45.6 -10.763869285583496
		 46.4 -10.828792572021484 47.2 -10.769632339477541 48 -10.583654403686523 48.8 -10.413884162902832
		 49.6 -10.366176605224608 50.4 -10.477353096008301 51.2 -10.749301910400392 52 -10.887515068054199
		 52.8 -10.696393013000488 53.6 -10.461827278137209 54.4 -10.423078536987305 55.2 -10.456766128540041
		 56 -10.482321739196776 56.8 -10.518291473388672 57.6 -10.562015533447266 58.4 -10.539644241333008
		 59.2 -10.44062614440918 60 -10.325300216674805 60.8 -10.227584838867187 61.6 -10.183559417724608
		 62.4 -10.264691352844238 63.2 -10.353347778320312 64 -10.305877685546877 64.8 -10.253169059753418
		 65.6 -10.270867347717283 66.4 -10.255255699157717 67.2 -10.159016609191896 68 -10.057390213012697
		 68.8 -9.9557714462280273 69.6 -9.8601369857788086 70.4 -9.8174476623535156 71.2 -9.8097896575927734
		 72 -9.7329845428466797 72.8 -9.6859245300292969;
createNode animCurveTA -n "Bip01_L_Toe0_rotateX1";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 92 ".ktv[0:91]"  0 -7.0968026193440892e-006 0.8 -2.5611102500988636e-006
		 1.6 -1.0719567399064545e-005 2.4 -2.4213450160459615e-006 3.2 -2.6045688628073549e-006
		 4 -5.8840696510742418e-006 4.8 -5.8350474319013301e-006 5.6 -7.1860158641356975e-006
		 6.4 -2.1405253392003947e-006 7.2 -5.7148276937368792e-006 8 -1.2997832527616993e-005
		 8.8 -7.810652277839834e-006 9.6 -3.9807841858419116e-006 10.4 -7.9590508903493156e-006
		 11.2 -2.0123268313909648e-006 12 -6.9932016231177849e-006 12.8 -3.735024620254991e-006
		 13.6 -3.3741839615686331e-006 14.4 -4.8192659960477613e-006 15.2 -3.493362783046905e-006
		 16 -7.126542641344713e-006 16.8 -6.3066358961805236e-006 17.6 -5.5002356020850129e-006
		 18.4 -5.5475170483987313e-006 19.2 -6.8204553826944903e-006 20 -4.1370522012584843e-006
		 20.8 -1.0180016261074345e-006 21.6 -8.8400529421051033e-006 22.4 -7.7405766205629317e-006
		 23.2 -7.4587728704500478e-006 24 -4.0746508602751419e-006 24.8 -3.5322186704433989e-006
		 25.6 -9.7673928394215181e-006 26.4 -2.9292696126503874e-006 27.2 -3.8785160541010546e-006
		 28 -8.8933229562826455e-006 28.8 -5.5622276704525575e-006 29.6 -2.7081916869065026e-006
		 30.4 -7.0166552177397526e-006 31.2 -2.5718691176734865e-006 32 -1.0467287211213261e-005
		 32.8 -1.4743827705387957e-006 33.6 -6.4018572629720438e-006 34.4 -9.4168190116761252e-006
		 35.2 -6.2503363551513758e-006 36 -7.3396840889472514e-006 36.8 -5.4559945965593215e-006
		 37.6 -4.4154326133138966e-006 38.4 -9.9515063993749209e-006 39.2 -5.5291279750235844e-006
		 40 -6.4513487814110704e-006 40.8 -6.492236025223975e-006 41.6 -8.4612483988166787e-006
		 42.4 -3.4794766179402359e-006 43.2 -3.1305053198593669e-006 44 -5.1915890253440011e-006
		 44.8 -6.4837104218895547e-006 45.6 -5.1196216190874119e-006 46.4 -8.3313007053220645e-006
		 47.2 -2.062937028313172e-006 48 -8.7857852122397162e-006 48.8 -5.4244178500084672e-006
		 49.6 -3.3436456305935285e-006 50.4 -3.7391876048786798e-006 51.2 -2.9010197977186181e-006
		 52 -6.7488040258467663e-006 52.8 -4.5145898184273392e-006 53.6 -2.579881765996106e-006
		 54.4 -5.6105709518305957e-006 55.2 -1.3319587424120982e-006 56 -7.4341101026220704e-006
		 56.8 -2.6255527245666599e-006 57.6 -7.9359951996593719e-006 58.4 -8.9499271780368872e-006
		 59.2 -6.6449274527258248e-006 60 -1.5475462760150549e-006 60.8 8.8124710373449489e-007
		 61.6 -6.6223660724062938e-006 62.4 -2.3986137875908753e-006 63.2 -6.6915736169903539e-006
		 64 -3.8825519368401729e-006 64.8 -2.6305449409846915e-006 65.6 -1.2017531844321638e-005
		 66.4 -4.2192596083623357e-006 67.2 -9.256979865313042e-006 68 -6.3626448536524549e-006
		 68.8 -3.4732956919469875e-006 69.6 -2.1215689685050165e-006 70.4 -5.3143958211876452e-006
		 71.2 -8.9421864686300978e-006 72 -3.7544693896052191e-006 72.8 -2.7620251330517931e-006;
createNode animCurveTA -n "Bip01_L_Toe0_rotateY1";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 92 ".ktv[0:91]"  0 4.0526911107008345e-006 0.8 2.6712134513218189e-006
		 1.6 3.9649745531278313e-007 2.4 -3.8177884675860696e-007 3.2 3.8187208701856426e-006
		 4 4.23199935539742e-006 4.8 1.8721824517342611e-006 5.6 1.1600781363085844e-005 6.4 3.4883128137153103e-006
		 7.2 9.5780069386819378e-006 8 9.0481225925032049e-006 8.8 4.8484525905223563e-006
		 9.6 7.1447807385993664e-006 10.4 1.1343265668983804e-006 11.2 4.2600509004842024e-006
		 12 -3.8406524254241958e-006 12.8 5.1436945796012878e-006 13.6 1.4942921211513749e-007
		 14.4 -1.7304939774476225e-006 15.2 4.3269219531794079e-006 16 8.266804798040539e-006
		 16.8 1.5516387747993578e-006 17.6 -3.962565642723348e-006 18.4 -7.1447502705268562e-006
		 19.2 6.4956188907672185e-006 20 -6.5640074353723321e-007 20.8 1.068536403181497e-005
		 21.6 -3.972394551965408e-006 22.4 9.5920256626413902e-007 23.2 -1.2000981541859801e-006
		 24 2.315444589839899e-006 24.8 -8.568736689085199e-007 25.6 2.8191295768920099e-006
		 26.4 -7.6587903095060028e-006 27.2 7.8439225035253929e-006 28 5.1009419621550478e-006
		 28.8 7.6546592708837114e-007 29.6 5.7136403484037154e-006 30.4 1.1010549314960372e-005
		 31.2 4.6738077799091116e-006 32 7.1577795779376174e-007 32.8 -5.2043301366211381e-006
		 33.6 3.302859795439872e-006 34.4 8.3575440612548846e-007 35.2 4.6538680180674419e-006
		 36 8.9140928594133584e-007 36.8 2.9945229584882327e-007 37.6 -1.4092898936723941e-006
		 38.4 7.9004776125657372e-006 39.2 5.3059379752085079e-006 40 3.5696837130672066e-006
		 40.8 6.6160714595753234e-006 41.6 -4.8245615289488342e-006 42.4 4.7749285840836819e-006
		 43.2 6.451498393289512e-006 44 9.9265635071787983e-006 44.8 1.2041342642987731e-006
		 45.6 3.4421489090163959e-006 46.4 -2.0346310520835686e-006 47.2 6.5262142925348599e-006
		 48 1.34125375552685e-005 48.8 1.0106257832376286e-005 49.6 6.4205100898107048e-006
		 50.4 1.5676248494855829e-006 51.2 6.7904152274422813e-006 52 -7.207393991848221e-006
		 52.8 1.3789535842079204e-005 53.6 7.4111931098741479e-006 54.4 -6.4752480284369093e-006
		 55.2 9.3493536041933112e-006 56 -3.2949337764875963e-006 56.8 8.3237301851113443e-007
		 57.6 3.681192310978077e-007 58.4 3.1825834412302356e-006 59.2 -2.0221509657858405e-006
		 60 1.342351060884539e-005 60.8 9.15057444217382e-006 61.6 3.5781399674306158e-006
		 62.4 5.3523456244874978e-007 63.2 2.8854726679128362e-006 64 -3.8166712101883599e-007
		 64.8 9.9109793154639192e-006 65.6 1.2055061233695596e-006 66.4 1.44174725846824e-006
		 67.2 -3.4425153216943727e-007 68 3.3802209600253263e-006 68.8 4.50827019449207e-006
		 69.6 1.3002094419789501e-005 70.4 6.4775258579174988e-006 71.2 1.9508129298628778e-006
		 72 1.6329149730154311e-006 72.8 -4.4393855205271393e-006;
createNode animCurveTA -n "Bip01_L_Toe0_rotateZ1";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 92 ".ktv[0:91]"  0 89.999961853027344 0.8 89.999977111816406
		 1.6 89.999961853027344 2.4 89.999977111816406 3.2 89.999977111816406 4 89.999977111816406
		 4.8 89.999977111816406 5.6 89.999977111816406 6.4 89.999984741210937 7.2 89.999977111816406
		 8 89.999954223632813 8.8 89.999977111816406 9.6 89.999977111816406 10.4 89.999961853027344
		 11.2 89.999977111816406 12 89.999977111816406 12.8 89.999977111816406 13.6 89.999977111816406
		 14.4 89.999977111816406 15.2 89.999977111816406 16 89.999961853027344 16.8 89.999977111816406
		 17.6 89.999977111816406 18.4 89.999977111816406 19.2 89.999977111816406 20 89.999961853027344
		 20.8 89.999984741210937 21.6 89.999961853027344 22.4 89.999977111816406 23.2 89.999977111816406
		 24 89.999977111816406 24.8 89.999977111816406 25.6 89.999961853027344 26.4 89.999977111816406
		 27.2 89.999961853027344 28 89.999977111816406 28.8 89.999977111816406 29.6 89.999977111816406
		 30.4 89.999977111816406 31.2 89.999977111816406 32 89.999977111816406 32.8 89.999977111816406
		 33.6 89.999977111816406 34.4 89.999977111816406 35.2 89.999977111816406 36 89.999977111816406
		 36.8 89.999977111816406 37.6 89.999977111816406 38.4 89.999961853027344 39.2 89.999977111816406
		 40 89.999961853027344 40.8 89.999977111816406 41.6 89.999961853027344 42.4 89.999961853027344
		 43.2 89.999977111816406 44 89.999977111816406 44.8 89.999961853027344 45.6 89.999977111816406
		 46.4 89.999977111816406 47.2 89.999977111816406 48 89.999977111816406 48.8 89.999977111816406
		 49.6 89.999977111816406 50.4 89.999961853027344 51.2 89.999977111816406 52 89.999977111816406
		 52.8 89.999977111816406 53.6 89.999977111816406 54.4 89.999977111816406 55.2 89.999977111816406
		 56 89.999977111816406 56.8 89.999977111816406 57.6 89.999961853027344 58.4 89.999977111816406
		 59.2 89.999977111816406 60 89.999977111816406 60.8 89.999977111816406 61.6 89.999977111816406
		 62.4 89.999977111816406 63.2 89.999977111816406 64 89.999977111816406 64.8 89.999977111816406
		 65.6 89.999961853027344 66.4 89.999977111816406 67.2 89.999977111816406 68 89.999977111816406
		 68.8 89.999961853027344 69.6 89.999977111816406 70.4 89.999961853027344 71.2 89.999977111816406
		 72 89.999977111816406 72.8 89.999977111816406;
createNode animCurveTA -n "Bip01_R_Thigh_rotateX11";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 92 ".ktv[0:91]"  0 1.0010324716567991 0.8 1.1300137042999268
		 1.6 1.2542275190353394 2.4 1.3789058923721313 3.2 1.4950053691864016 4 1.6062145233154297
		 4.8 1.7121098041534424 5.6 1.7855342626571655 6.4 1.8678517341613767 7.2 1.9796346426010132
		 8 2.0890951156616211 8.8 2.1631617546081543 9.6 2.2175984382629395 10.4 2.2681963443756104
		 11.2 2.2971017360687256 12 2.3364710807800293 12.8 2.3808398246765137 13.6 2.4028604030609131
		 14.4 2.4026827812194824 15.2 2.3999097347259521 16 2.3938310146331787 16.8 2.3858258724212646
		 17.6 2.3997595310211186 18.4 2.4246213436126709 19.2 2.4069311618804932 20 2.3567020893096924
		 20.8 2.2978529930114746 21.6 2.285517692565918 22.4 2.2555468082427979 23.2 2.2180628776550293
		 24 2.1744179725646973 24.8 2.1197490692138672 25.6 2.0489199161529541 26.4 1.9837872982025144
		 27.2 1.9284764528274538 28 1.8650513887405389 28.8 1.8001235723495479 29.6 1.7275732755661013
		 30.4 1.6538072824478147 31.2 1.5857648849487305 32 1.5268750190734863 32.8 1.4621521234512329
		 33.6 1.3915826082229614 34.4 1.303036093711853 35.2 1.2146261930465698 36 1.1271040439605713
		 36.8 1.0301973819732666 37.6 0.94766139984130859 38.4 0.86595863103866577 39.2 0.77089607715606689
		 40 0.68224024772644043 40.8 0.61197125911712646 41.6 0.56991094350814819 42.4 0.5136985182762146
		 43.2 0.45517745614051813 44 0.38805034756660461 44.8 0.32656499743461609 45.6 0.26318082213401794
		 46.4 0.18604068458080292 47.2 0.11379094421863556 48 0.042470112442970269 48.8 -0.013523774221539496
		 49.6 -0.058111246675252894 50.4 -0.11089494824409485 51.2 -0.16176618635654447 52 -0.20739185810089111
		 52.8 -0.29621163010597229 53.6 -0.4169966876506806 54.4 -0.52988606691360474 55.2 -0.60679399967193604
		 56 -0.6282954216003418 56.8 -0.64567142724990845 57.6 -0.68222659826278687 58.4 -0.74184048175811779
		 59.2 -0.79101783037185669 60 -0.83128863573074352 60.8 -0.8774060606956483 61.6 -0.93090009689331055
		 62.4 -0.95816618204116821 63.2 -0.96849346160888672 64 -1.000185489654541 64.8 -1.0467127561569214
		 65.6 -1.0906991958618164 66.4 -1.1311339139938354 67.2 -1.1673885583877563 68 -1.1925783157348633
		 68.8 -1.2006652355194092 69.6 -1.1943541765213013 70.4 -1.2101728916168213 71.2 -1.2461270093917849
		 72 -1.2765896320343018 72.8 -1.2896761894226074;
createNode animCurveTA -n "Bip01_R_Thigh_rotateY11";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 92 ".ktv[0:91]"  0 167.40240478515625 0.8 167.42623901367188
		 1.6 167.44667053222656 2.4 167.45838928222659 3.2 167.46640014648435 4 167.47238159179687
		 4.8 167.47760009765625 5.6 167.47334289550781 6.4 167.45356750488281 7.2 167.47492980957031
		 8 167.497802734375 8.8 167.48081970214844 9.6 167.45985412597656 10.4 167.42259216308594
		 11.2 167.36686706542969 12 167.32437133789062 12.8 167.29539489746094 13.6 167.25006103515625
		 14.4 167.18898010253906 15.2 167.13844299316406 16 167.08601379394531 16.8 167.04981994628906
		 17.6 167.03042602539062 18.4 167.01580810546875 19.2 166.98036193847656 20 166.9203796386719
		 20.8 166.87356567382812 21.6 166.86990356445312 22.4 166.85296630859375 23.2 166.83039855957031
		 24 166.81086730957031 24.8 166.77584838867187 25.6 166.72761535644531 26.4 166.70396423339847
		 27.2 166.69963073730469 28 166.68266296386722 28.8 166.65998840332031 29.6 166.64276123046875
		 30.4 166.61827087402344 31.2 166.59506225585938 32 166.58123779296875 32.8 166.57679748535156
		 33.6 166.55378723144531 34.4 166.51995849609375 35.2 166.49378967285156 36 166.46034240722656
		 36.8 166.43173217773435 37.6 166.40539550781253 38.4 166.36509704589844 39.2 166.31405639648435
		 40 166.28257751464847 40.8 166.2725830078125 41.6 166.27220153808594 42.4 166.28933715820312
		 43.2 166.28187561035156 44 166.2769775390625 44.8 166.28022766113281 45.6 166.27850341796875
		 46.4 166.26858520507812 47.2 166.25950622558594 48 166.267333984375 48.8 166.2845458984375
		 49.6 166.31082153320312 50.4 166.33586120605469 51.2 166.363037109375 52 166.38000488281253
		 52.8 166.38070678710935 53.6 166.343505859375 54.4 166.3076171875 55.2 166.31706237792969
		 56 166.37074279785156 56.8 166.43821716308594 57.6 166.48405456542969 58.4 166.50895690917969
		 59.2 166.53877258300781 60 166.58131408691406 60.8 166.62281799316406 61.6 166.66719055175781
		 62.4 166.7196044921875 63.2 166.77261352539062 64 166.81796264648435 64.8 166.85578918457031
		 65.6 166.88858032226562 66.4 166.90644836425781 67.2 166.93278503417969 68 166.96153259277344
		 68.8 166.99710083007812 69.6 167.02551269531253 70.4 167.04803466796875 71.2 167.03324890136719
		 72 167.01261901855469 72.8 167.01040649414065;
createNode animCurveTA -n "Bip01_R_Thigh_rotateZ11";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 92 ".ktv[0:91]"  0 -85.684471130371094 0.8 -85.67218017578125
		 1.6 -85.653656005859375 2.4 -85.639091491699219 3.2 -85.617401123046875 4 -85.583168029785156
		 4.8 -85.580978393554688 5.6 -85.612945556640625 6.4 -85.650566101074219 7.2 -85.655349731445313
		 8 -85.644203186035156 8.8 -85.665512084960938 9.6 -85.690757751464844 10.4 -85.695533752441406
		 11.2 -85.691246032714844 12 -85.687286376953125 12.8 -85.716598510742188 13.6 -85.743789672851563
		 14.4 -85.769851684570298 15.2 -85.809219360351563 16 -85.855552673339844 16.8 -85.881782531738281
		 17.6 -85.889244079589844 18.4 -85.883010864257813 19.2 -85.894981384277344 20 -85.936912536621094
		 20.8 -86.000190734863281 21.6 -86.03570556640625 22.4 -86.077178955078125 23.2 -86.107421875
		 24 -86.119804382324219 24.8 -86.118217468261719 25.6 -86.114227294921875 26.4 -86.124114990234375
		 27.2 -86.170074462890625 28 -86.223075866699219 28.8 -86.255691528320327 29.6 -86.27020263671875
		 30.4 -86.298477172851562 31.2 -86.350494384765625 32 -86.3834228515625 32.8 -86.404808044433594
		 33.6 -86.450653076171875 34.4 -86.5250244140625 35.2 -86.603378295898452 36 -86.664031982421875
		 36.8 -86.758041381835952 37.6 -86.825523376464844 38.4 -86.844131469726563 39.2 -86.859321594238295
		 40 -86.90582275390625 40.8 -86.966583251953139 41.6 -87.0087890625 42.4 -87.012016296386719
		 43.2 -87.0560302734375 44 -87.097640991210938 44.8 -87.129386901855469 45.6 -87.158836364746108
		 46.4 -87.186134338378906 47.2 -87.215446472167969 48 -87.238288879394531 48.8 -87.266616821289063
		 49.6 -87.299896240234375 50.4 -87.31396484375 51.2 -87.310195922851563 52 -87.33349609375
		 52.8 -87.349510192871094 53.6 -87.378875732421875 54.4 -87.414337158203125 55.2 -87.424041748046875
		 56 -87.41961669921875 56.8 -87.433029174804688 57.6 -87.444038391113281 58.4 -87.434097290039063
		 59.2 -87.418167114257813 60 -87.413436889648437 60.8 -87.46417236328125 61.6 -87.504852294921875
		 62.4 -87.50321197509767 63.2 -87.476333618164063 64 -87.497123718261719 64.8 -87.536140441894531
		 65.6 -87.56512451171875 66.4 -87.564224243164063 67.2 -87.58258056640625 68 -87.61260986328125
		 68.8 -87.624794006347642 69.6 -87.634765625 70.4 -87.6754150390625 71.2 -87.737625122070327
		 72 -87.785331726074219 72.8 -87.792320251464844;
createNode animCurveTA -n "Bip01_R_Calf_rotateX11";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 92 ".ktv[0:91]"  0 -0.90439128875732422 0.8 -0.9042704701423645
		 1.6 -0.90410375595092762 2.4 -0.90399628877639782 3.2 -0.90384238958358754 4 -0.90358382463455189
		 4.8 -0.90342587232589733 5.6 -0.90343260765075695 6.4 -0.90356832742691029 7.2 -0.90346580743789662
		 8 -0.90336954593658447 8.8 -0.90326321125030529 9.6 -0.9031488299369812 10.4 -0.9030078649520874
		 11.2 -0.90298968553543102 12 -0.90294337272644043 12.8 -0.90288776159286499 13.6 -0.90278542041778576
		 14.4 -0.90276271104812644 15.2 -0.90288585424423184 16 -0.90321105718612671 16.8 -0.90320700407028198
		 17.6 -0.90317451953887928 18.4 -0.90313643217086792 19.2 -0.90320771932601951 20 -0.90343195199966431
		 20.8 -0.90355575084686268 21.6 -0.90374016761779785 22.4 -0.90395885705947876 23.2 -0.9041097164154055
		 24 -0.90417706966400124 24.8 -0.90420728921890259 25.6 -0.90419352054595936 26.4 -0.90418559312820401
		 27.2 -0.90424305200576816 28 -0.90437090396881104 28.8 -0.90445572137832642 29.6 -0.90442895889282227
		 30.4 -0.90440523624420177 31.2 -0.90453654527664196 32 -0.90464258193969749 32.8 -0.90464806556701649
		 33.6 -0.90480953454971325 34.4 -0.90493714809417736 35.2 -0.90511226654052757 36 -0.90528005361557007
		 36.8 -0.90546405315399148 37.6 -0.90552884340286255 38.4 -0.90548259019851696 39.2 -0.90539979934692372
		 40 -0.90562689304351807 40.8 -0.90573960542678833 41.6 -0.90576469898223877 42.4 -0.90565729141235363
		 43.2 -0.90567654371261597 44 -0.90571057796478271 44.8 -0.90568888187408481 45.6 -0.90580374002456654
		 46.4 -0.90570312738418601 47.2 -0.90574485063552868 48 -0.90576434135437012 48.8 -0.90573823451995839
		 49.6 -0.90583968162536621 50.4 -0.90583914518356323 51.2 -0.90573054552078247 52 -0.90579903125762939
		 52.8 -0.90595120191574097 53.6 -0.90612667798995972 54.4 -0.90624344348907471 55.2 -0.90614789724349976
		 56 -0.90609198808670044 56.8 -0.90610021352767955 57.6 -0.90617054700851429 58.4 -0.90622270107269276
		 59.2 -0.90623968839645386 60 -0.90622830390930176 60.8 -0.90649795532226563 61.6 -0.9067302942276001
		 62.4 -0.90667831897735607 63.2 -0.90663927793502819 64 -0.90679413080215476 64.8 -0.90703308582305897
		 65.6 -0.90717524290084839 66.4 -0.90724915266036987 67.2 -0.90739655494689941 68 -0.90757977962493896
		 68.8 -0.90765821933746338 69.6 -0.90763431787490856 70.4 -0.90766090154647794 71.2 -0.90792578458786022
		 72 -0.90816015005111728 72.8 -0.90824550390243541;
createNode animCurveTA -n "Bip01_R_Calf_rotateY11";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 92 ".ktv[0:91]"  0 0.24417799711227417 0.8 0.24462719261646271
		 1.6 0.24519278109073639 2.4 0.24560227990150449 3.2 0.24634359776973724 4 0.24748802185058591
		 4.8 0.24807222187519071 5.6 0.24794842302799222 6.4 0.24739514291286469 7.2 0.24773548543453217
		 8 0.24809163808822632 8.8 0.24859696626663211 9.6 0.24893012642860413 10.4 0.2493920773267746
		 11.2 0.24953269958496097 12 0.24973557889461515 12.8 0.24987007677555084 13.6 0.25015914440155029
		 14.4 0.25029343366622925 15.2 0.24977840483188629 16 0.24843010306358332 16.8 0.24836067855358121
		 17.6 0.24852916598320005 18.4 0.24870945513248444 19.2 0.24838435649871826 20 0.24743509292602539
		 20.8 0.24678093194961548 21.6 0.24595005810260773 22.4 0.24494932591915128 23.2 0.2442220151424408
		 24 0.24396738409996033 24.8 0.24388298392295832 25.6 0.24399250745773315 26.4 0.24402534961700439
		 27.2 0.24369758367538455 28 0.24314698576927188 28.8 0.24277153611183164 29.6 0.24282573163509369
		 30.4 0.2429659515619278 31.2 0.2423679381608963 32 0.24187202751636505 32.8 0.24186716973781583
		 33.6 0.24121268093585971 34.4 0.24065420031547544 35.2 0.23998273909091949 36 0.23925511538982391
		 36.8 0.23844289779663091 37.6 0.23803664743900299 38.4 0.23836278915405273 39.2 0.23894822597503662
		 40 0.23805478215217593 40.8 0.2375141829252243 41.6 0.23740778863430023 42.4 0.23780842125415799
		 43.2 0.23772789537906644 44 0.23749618232250216 44.8 0.23761725425720215 45.6 0.23723889887332916
		 46.4 0.23769307136535645 47.2 0.23747272789478305 48 0.23738270998001099 48.8 0.23749540746212003
		 49.6 0.2370815426111221 50.4 0.23711512982845304 51.2 0.23753108084201813 52 0.23728750646114349
		 52.8 0.23663517832756045 53.6 0.23601578176021573 54.4 0.23562462627887729 55.2 0.23607295751571658
		 56 0.23609013855457303 56.8 0.23598814010620117 57.6 0.23556600511074069 58.4 0.23534880578517908
		 59.2 0.23529163002967837 60 0.23528701066970828 60.8 0.23404191434383392 61.6 0.23293606936931613
		 62.4 0.23316717147827151 63.2 0.23320315778255463 64 0.23239010572433472 64.8 0.2312735319137573
		 65.6 0.23060862720012665 66.4 0.23018650710582733 67.2 0.22954738140106204 68 0.22875721752643585
		 68.8 0.22836141288280487 69.6 0.22832445800304413 70.4 0.22811681032180781 71.2 0.22685442864894867
		 72 0.2258151173591614 72.8 0.22540047764778137;
createNode animCurveTA -n "Bip01_R_Calf_rotateZ11";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 92 ".ktv[0:91]"  0 -72.722381591796875 0.8 -72.690223693847642
		 1.6 -72.632064819335938 2.4 -72.596809387207017 3.2 -72.585136413574219 4 -72.535385131835938
		 4.8 -72.48760986328125 5.6 -72.474296569824219 6.4 -72.499618530273438 7.2 -72.466705322265625
		 8 -72.433677673339844 8.8 -72.41900634765625 9.6 -72.378524780273438 10.4 -72.331764221191406
		 11.2 -72.337867736816406 12 -72.330657958984375 12.8 -72.298828125 13.6 -72.258842468261719
		 14.4 -72.253463745117188 15.2 -72.278404235839844 16 -72.357978820800781 16.8 -72.34954833984375
		 17.6 -72.333526611328125 18.4 -72.330940246582031 19.2 -72.344528198242188 20 -72.390724182128906
		 20.8 -72.404502868652358 21.6 -72.430961608886719 22.4 -72.47540283203125 23.2 -72.501144409179673
		 24 -72.520164489746094 24.8 -72.529083251953125 25.6 -72.532058715820327 26.4 -72.525909423828125
		 27.2 -72.533828735351563 28 -72.56451416015625 28.8 -72.576728820800781 29.6 -72.570602416992188
		 30.4 -72.560569763183594 31.2 -72.589508056640625 32 -72.605422973632812 32.8 -72.611564636230469
		 33.6 -72.65008544921875 34.4 -72.679229736328125 35.2 -72.729408264160156 36 -72.772567749023438
		 36.8 -72.808586120605469 37.6 -72.815254211425781 38.4 -72.815292358398438 39.2 -72.841201782226562
		 40 -72.89984130859375 40.8 -72.923812866210938 41.6 -72.924606323242188 42.4 -72.887794494628906
		 43.2 -72.890144348144531 44 -72.891563415527344 44.8 -72.891746520996094 45.6 -72.930137634277344
		 46.4 -72.91412353515625 47.2 -72.918876647949219 48 -72.919998168945313 48.8 -72.9139404296875
		 49.6 -72.939720153808608 50.4 -72.945220947265625 51.2 -72.914520263671875 52 -72.935684204101562
		 52.8 -72.96978759765625 53.6 -73.036018371582031 54.4 -73.084693908691406 55.2 -73.058441162109375
		 56 -73.015823364257813 56.8 -73.008888244628906 57.6 -73.017021179199219 58.4 -73.028533935546875
		 59.2 -73.027099609375 60 -73.018684387207031 60.8 -73.083900451660156 61.6 -73.125526428222656
		 62.4 -73.109832763671889 63.2 -73.08172607421875 64 -73.104110717773437 64.8 -73.153984069824219
		 65.6 -73.186607360839844 66.4 -73.189857482910156 67.2 -73.229965209960938 68 -73.287200927734375
		 68.8 -73.292617797851563 69.6 -73.269660949707017 70.4 -73.270408630371094 71.2 -73.328254699707017
		 72 -73.386123657226562 72.8 -73.404747009277344;
createNode animCurveTA -n "Bip01_R_Foot_rotateX11";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 92 ".ktv[0:91]"  0 13.11998176574707 0.8 13.188881874084473
		 1.6 13.337484359741213 2.4 13.510872840881348 3.2 13.591322898864746 4 13.66426944732666
		 4.8 13.674599647521973 5.6 13.608237266540527 6.4 13.557198524475098 7.2 13.73447322845459
		 8 13.924219131469728 8.8 13.948291778564451 9.6 13.922819137573242 10.4 13.913309097290041
		 11.2 13.924139022827148 12 14.090551376342773 12.8 14.263463973999023 13.6 14.255050659179687
		 14.4 14.180646896362305 15.2 14.178841590881348 16 14.245050430297852 16.8 14.282193183898926
		 17.6 14.408937454223633 18.4 14.615808486938477 19.2 14.747756958007814 20 14.723481178283691
		 20.8 14.788880348205566 21.6 14.968242645263672 22.4 15.012787818908693 23.2 15.010540008544922
		 24 15.026388168334959 24.8 14.918170928955076 25.6 14.801156997680662 26.4 14.882762908935547
		 27.2 15.03231143951416 28 15.00770378112793 28.8 14.931181907653809 29.6 14.867944717407225
		 30.4 14.822872161865233 31.2 14.79261589050293 32 14.859917640686035 32.8 14.895526885986328
		 33.6 14.85976600646973 34.4 14.688535690307621 35.2 14.43153667449951 36 14.225929260253906
		 36.8 14.173160552978517 37.6 14.084039688110352 38.4 13.841172218322754 39.2 13.387677192687988
		 40 13.008726119995115 40.8 12.92351245880127 41.6 13.146320343017578 42.4 13.347090721130373
		 43.2 13.298356056213381 44 13.179216384887695 44.8 13.207768440246582 45.6 13.25803279876709
		 46.4 13.249550819396973 47.2 13.225231170654297 48 13.216306686401367 48.8 13.189314842224119
		 49.6 13.16016674041748 50.4 13.232296943664554 51.2 13.338665008544922 52 13.445048332214355
		 52.8 13.360483169555664 53.6 12.96830940246582 54.4 12.533745765686035 55.2 12.474291801452637
		 56 12.856429100036619 56.8 13.28596305847168 57.6 13.477468490600586 58.4 13.562942504882812
		 59.2 13.75560188293457 60 13.963329315185549 60.8 14.101360321044922 61.6 14.240923881530762
		 62.4 14.459627151489258 63.2 14.694683074951172 64 14.865586280822752 64.8 15.014993667602543
		 65.6 15.127833366394045 66.4 15.198073387145996 67.2 15.277422904968262 68 15.391842842102054
		 68.8 15.496174812316895 69.6 15.688636779785154 70.4 15.891252517700195 71.2 15.950159072875975
		 72 15.987539291381834 72.8 16.060115814208984;
createNode animCurveTA -n "Bip01_R_Foot_rotateY11";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 92 ".ktv[0:91]"  0 -2.6483540534973145 0.8 -2.6647100448608398
		 1.6 -2.7012753486633301 2.4 -2.7609765529632568 3.2 -2.8238697052001953 4 -2.8341658115386963
		 4.8 -2.8270387649536133 5.6 -2.7812438011169434 6.4 -2.7475831508636475 7.2 -2.797698974609375
		 8 -2.8790147304534912 8.8 -2.8936069011688232 9.6 -2.8581233024597168 10.4 -2.852766752243042
		 11.2 -2.8740851879119873 12 -2.9366512298583984 12.8 -3.0041296482086186 13.6 -2.9803917407989502
		 14.4 -2.9444139003753662 15.2 -2.9547436237335205 16 -2.9938862323760982 16.8 -3.012444019317627
		 17.6 -3.0412783622741699 18.4 -3.173261404037476 19.2 -3.2383716106414795 20 -3.2285676002502441
		 20.8 -3.2440884113311768 21.6 -3.3167405128479004 22.4 -3.3160996437072754 23.2 -3.3221509456634521
		 24 -3.3117079734802246 24.8 -3.3036842346191406 25.6 -3.2677099704742432 26.4 -3.3157854080200195
		 27.2 -3.3735001087188721 28 -3.3642833232879639 28.8 -3.3152029514312749 29.6 -3.2971591949462891
		 30.4 -3.2726485729217529 31.2 -3.2961065769195557 32 -3.3130977153778076 32.8 -3.3383479118347168
		 33.6 -3.339249849319458 34.4 -3.2563600540161133 35.2 -3.1516470909118652 36 -3.0328657627105713
		 36.8 -3.0403416156768799 37.6 -2.9959719181060791 38.4 -2.8927321434020996 39.2 -2.729123592376709
		 40 -2.561016321182251 40.8 -2.5221438407897949 41.6 -2.6156489849090576 42.4 -2.7284975051879883
		 43.2 -2.7087528705596924 44 -2.6616644859313965 44.8 -2.6676805019378662 45.6 -2.7074527740478516
		 46.4 -2.7171189785003662 47.2 -2.7020530700683594 48 -2.6804735660552979 48.8 -2.700525283813477
		 49.6 -2.6976399421691895 50.4 -2.7285177707672119 51.2 -2.7739739418029785 52 -2.8079102039337158
		 52.8 -2.8120784759521484 53.6 -2.6307003498077393 54.4 -2.4684016704559326 55.2 -2.4404833316802979
		 56 -2.5882396697998047 56.8 -2.8069717884063721 57.6 -2.9033300876617432 58.4 -2.9484329223632812
		 59.2 -3.0231227874755859 60 -3.1317520141601563 60.8 -3.2000141143798828 61.6 -3.2600162029266357
		 62.4 -3.3357088565826416 63.2 -3.4672296047210693 64 -3.5504682064056396 64.8 -3.5735552310943604
		 65.6 -3.651309251785277 66.4 -3.6917848587036137 67.2 -3.7129640579223628 68 -3.7768950462341317
		 68.8 -3.8333206176757821 69.6 -3.8992648124694824 70.4 -3.98890233039856 71.2 -4.031242847442627
		 72 -4.0138645172119141 72.8 -4.0729403495788574;
createNode animCurveTA -n "Bip01_R_Foot_rotateZ11";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 92 ".ktv[0:91]"  0 -10.811043739318848 0.8 -10.903816223144531
		 1.6 -11.028416633605955 2.4 -11.095693588256836 3.2 -10.932538032531738 4 -10.78469181060791
		 4.8 -10.902980804443359 5.6 -11.136425971984863 6.4 -11.275382041931152 7.2 -11.290741920471191
		 8 -11.257928848266602 8.8 -11.329482078552246 9.6 -11.574839591979982 10.4 -11.718217849731444
		 11.2 -11.601546287536619 12 -11.489317893981935 12.8 -11.576254844665527 13.6 -11.731901168823242
		 14.4 -11.806879997253418 15.2 -11.766053199768066 16 -11.724260330200195 16.8 -11.75980281829834
		 17.6 -11.779285430908203 18.4 -11.656436920166017 19.2 -11.549184799194336 20 -11.646544456481934
		 20.8 -11.809245109558104 21.6 -11.889670372009276 22.4 -11.932415008544922 23.2 -11.990416526794434
		 24 -12.009007453918455 24.8 -11.944355010986328 25.6 -11.863652229309082 26.4 -11.847476005554199
		 27.2 -11.867236137390137 28 -11.903813362121582 28.8 -11.958754539489746 29.6 -11.988961219787598
		 30.4 -12.019648551940918 31.2 -12.033014297485352 32 -11.977635383605957 32.8 -11.928251266479492
		 33.6 -11.975503921508787 34.4 -12.010826110839844 35.2 -11.95192813873291 36 -11.927184104919435
		 36.8 -12.066243171691896 37.6 -12.178001403808594 38.4 -12.04222583770752 39.2 -11.801734924316406
		 40 -11.754240036010742 40.8 -11.83753490447998 41.6 -11.853752136230469 42.4 -11.826817512512209
		 43.2 -11.844443321228027 44 -11.874144554138184 44.8 -11.833434104919434 45.6 -11.701778411865236
		 46.4 -11.594351768493652 47.2 -11.610471725463867 48 -11.655078887939451 48.8 -11.637372970581056
		 49.6 -11.605207443237305 50.4 -11.556612968444824 51.2 -11.548403739929199 52 -11.553873062133787
		 52.8 -11.449166297912598 53.6 -11.267088890075684 54.4 -11.19815158843994 55.2 -11.249444007873535
		 56 -11.341207504272459 56.8 -11.389583587646484 57.6 -11.368829727172852 58.4 -11.346057891845703
		 59.2 -11.334383010864258 60 -11.31489372253418 60.8 -11.345679283142092 61.6 -11.390089988708496
		 62.4 -11.421880722045898 63.2 -11.458516120910645 64 -11.513113975524902 64.8 -11.560752868652344
		 65.6 -11.609679222106934 66.4 -11.631543159484863 67.2 -11.581290245056152 68 -11.527362823486328
		 68.8 -11.563468933105469 69.6 -11.659122467041016 70.4 -11.753785133361816 71.2 -11.811235427856444
		 72 -11.756649971008301 72.8 -11.638298034667969;
createNode animCurveTA -n "Bip01_R_Toe0_rotateX1";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 92 ".ktv[0:91]"  0 2.1770044895674801e-006 0.8 5.6787683888614993e-007
		 1.6 -4.9091340770246461e-007 2.4 1.6899987258511828e-006 3.2 -3.6961089335818538e-006
		 4 4.5928691179142342e-006 4.8 -1.1263103942837915e-006 5.6 3.86645797334495e-006
		 6.4 4.6103500608296599e-006 7.2 3.5412654142419342e-006 8 1.7281661257584344e-006
		 8.8 8.0948811955749989e-006 9.6 2.4203634438890731e-006 10.4 3.3286510188190732e-006
		 11.2 -6.766534852431505e-007 12 1.8794630705087911e-006 12.8 -3.7496806726267096e-006
		 13.6 -2.9373854886216577e-006 14.4 5.0236285460414373e-006 15.2 8.8209918658321872e-007
		 16 2.2288945729087577e-006 16.8 -9.3653864041698398e-007 17.6 3.0210396744223544e-006
		 18.4 4.889136107522063e-006 19.2 3.0133296604617499e-006 20 1.7494136272944163e-006
		 20.8 -1.9402959878789261e-006 21.6 5.6840019624360139e-007 22.4 2.175720965169603e-006
		 23.2 -3.9485998968302738e-006 24 3.6999751955590909e-006 24.8 4.2448465364941512e-007
		 25.6 4.6915513962630939e-007 26.4 3.3628127766860416e-006 27.2 2.6086838715855265e-006
		 28 3.3476360385975568e-006 28.8 7.327741968765622e-006 29.6 1.4650890989287293e-006
		 30.4 2.7297485871713434e-007 31.2 4.9074578782892786e-006 32 6.4208722960756859e-007
		 32.8 3.1628646866010968e-006 33.6 9.9679368759097997e-007 34.4 6.8733793341380087e-006
		 35.2 1.2275753533685927e-006 36 3.2310952065017773e-006 36.8 4.592590983065749e-007
		 37.6 -7.9267618957601382e-007 38.4 -1.9916913061024388e-006 39.2 1.561253156978637e-006
		 40 1.7634115465625655e-006 40.8 -5.2712272236021828e-007 41.6 5.6686951666051755e-007
		 42.4 6.2363678807741962e-006 43.2 5.9020412663812749e-006 44 1.4159040802042e-006
		 44.8 3.4736669931589863e-006 45.6 1.6297724414471304e-006 46.4 2.4108358047669753e-006
		 47.2 1.5159237136685988e-006 48 3.8314519770210609e-006 48.8 -1.1494975069581417e-006
		 49.6 6.4732121245469898e-006 50.4 -6.5825497586047277e-007 51.2 -1.765915953910735e-006
		 52 1.3149900723874453e-006 52.8 2.7982696337858215e-006 53.6 1.8442166265231206e-006
		 54.4 1.2286936907912605e-006 55.2 9.3665630629402585e-006 56 7.7076902016415261e-007
		 56.8 2.1674163690477144e-006 57.6 2.1990666709825748e-007 58.4 2.8181349875922024e-007
		 59.2 4.3539973404449483e-008 60 8.2654190691755502e-007 60.8 -3.2852423714757606e-007
		 61.6 3.5576968002715148e-006 62.4 3.7457743928825953e-006 63.2 4.273113972885767e-006
		 64 2.0546556243061787e-006 64.8 5.8994446590077132e-006 65.6 4.0235045162262395e-006
		 66.4 8.5441519104278996e-007 67.2 2.7635398964775959e-006 68 1.2677135146077487e-006
		 68.8 -6.7350384824749199e-007 69.6 1.2893668781543963e-006 70.4 6.2080580391921103e-006
		 71.2 -1.4278076605478418e-006 72 -8.7934159864744288e-007 72.8 5.8082582654606085e-006;
createNode animCurveTA -n "Bip01_R_Toe0_rotateY1";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 92 ".ktv[0:91]"  0 -1.5689813153585419e-005 0.8 -2.4935002329584677e-006
		 1.6 2.8313681923464173e-006 2.4 -8.6975533122313209e-006 3.2 -1.5525540675298544e-006
		 4 -1.3035078154643998e-005 4.8 1.2929979220643872e-006 5.6 -6.3027637224877253e-006
		 6.4 -3.139374484817381e-006 7.2 -1.1207869647478219e-005 8 -1.2445915672287812e-005
		 8.8 -1.0601499525364488e-005 9.6 -3.4829381547751836e-006 10.4 1.9856420294672716e-006
		 11.2 -3.8149037209223025e-006 12 -3.5818689525513037e-007 12.8 -4.1969619815063197e-006
		 13.6 -4.1241636949962412e-007 14.4 -1.2284673175599892e-005 15.2 -2.9518992050725501e-006
		 16 -9.4777551566949114e-006 16.8 1.9033826674785818e-006 17.6 7.317171821341617e-006
		 18.4 -6.015848157403525e-006 19.2 -6.990017482166877e-006 20 -5.018155206926167e-006
		 20.8 1.7689126607933758e-008 21.6 -1.653757749409124e-006 22.4 -1.3047872471361188e-006
		 23.2 -7.0493612724931154e-008 24 -7.0364335442718584e-006 24.8 -1.259186092283926e-006
		 25.6 -3.1684196528658504e-006 26.4 2.8698823371087201e-006 27.2 -7.080986961227608e-006
		 28 -6.9962638917786543e-006 28.8 -5.331182819645619e-006 29.6 -8.2528049460961483e-006
		 30.4 -9.1589099611155689e-006 31.2 -5.9290787248755805e-006 32 -7.2883035500126434e-006
		 32.8 -1.7278604218518014e-006 33.6 -1.136438277171692e-005 34.4 -1.2059754226356745e-006
		 35.2 -7.9916408139979484e-006 36 -1.0287454642821103e-005 36.8 -1.4844290490145793e-005
		 37.6 -8.4402463471633382e-006 38.4 -2.7098637929157121e-006 39.2 -1.5786956282681786e-005
		 40 -8.7873004304128699e-006 40.8 -1.4029943486093545e-005 41.6 2.4651367311889771e-006
		 42.4 -2.1146590825082967e-006 43.2 -5.9755166148534053e-006 44 -1.8101923160429574e-006
		 44.8 -6.9196977392493864e-007 45.6 -3.2375794489780674e-006 46.4 -2.8351282708172221e-006
		 47.2 -4.5743927330477163e-006 48 -8.2955175457755104e-006 48.8 -3.534765937729389e-006
		 49.6 -1.4538830328092446e-005 50.4 -1.6375004634028301e-005 51.2 -6.7095339773004525e-007
		 52 -1.0262650903314352e-005 52.8 -1.3342219062906224e-006 53.6 -9.3360304163070396e-006
		 54.4 -7.6014239311916779e-006 55.2 -4.8494107431906741e-006 56 6.5166555032192264e-006
		 56.8 -6.6131110543210534e-006 57.6 -8.8244478320120834e-006 58.4 -6.4419973568874411e-006
		 59.2 -1.4094022844801659e-005 60 -1.3588352885562928e-005 60.8 -4.145772891206434e-006
		 61.6 -3.7876518490520535e-006 62.4 -7.8399989433819428e-006 63.2 -3.0495036753563909e-006
		 64 -9.7242536867270246e-006 64.8 -3.2818509225762682e-006 65.6 -6.9117959355935454e-006
		 66.4 -7.6987280408502556e-006 67.2 -1.2768568012688776e-005 68 -6.2099829847284127e-006
		 68.8 3.0749347956771089e-007 69.6 -6.7664888092622277e-007 70.4 -8.8491005953983404e-006
		 71.2 -9.1944975793012418e-006 72 -3.1074278012965806e-006 72.8 -1.014510780805722e-005;
createNode animCurveTA -n "Bip01_R_Toe0_rotateZ1";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 92 ".ktv[0:91]"  0 89.999984741210937 0.8 89.999984741210937
		 1.6 89.999977111816406 2.4 89.999977111816406 3.2 89.999984741210937 4 89.999984741210937
		 4.8 89.999977111816406 5.6 89.999984741210937 6.4 89.999977111816406 7.2 89.999977111816406
		 8 89.999984741210937 8.8 89.999984741210937 9.6 89.999977111816406 10.4 89.999977111816406
		 11.2 89.999977111816406 12 89.999977111816406 12.8 89.999977111816406 13.6 89.999984741210937
		 14.4 89.999977111816406 15.2 89.999977111816406 16 89.999977111816406 16.8 89.999977111816406
		 17.6 89.999977111816406 18.4 89.999977111816406 19.2 89.999977111816406 20 89.999977111816406
		 20.8 89.999977111816406 21.6 89.999977111816406 22.4 89.999977111816406 23.2 89.999984741210937
		 24 89.999961853027344 24.8 89.999984741210937 25.6 89.999977111816406 26.4 89.999977111816406
		 27.2 89.999977111816406 28 89.999977111816406 28.8 89.999977111816406 29.6 89.999977111816406
		 30.4 89.999977111816406 31.2 89.999977111816406 32 89.999977111816406 32.8 89.999977111816406
		 33.6 89.999984741210937 34.4 89.999977111816406 35.2 89.999977111816406 36 89.999977111816406
		 36.8 89.999977111816406 37.6 89.999977111816406 38.4 89.999977111816406 39.2 89.999984741210937
		 40 89.999977111816406 40.8 89.999977111816406 41.6 89.999977111816406 42.4 89.999977111816406
		 43.2 89.999977111816406 44 89.999977111816406 44.8 89.999977111816406 45.6 89.999984741210937
		 46.4 89.999977111816406 47.2 89.999977111816406 48 89.999977111816406 48.8 89.999984741210937
		 49.6 89.999977111816406 50.4 89.999977111816406 51.2 89.999977111816406 52 89.999977111816406
		 52.8 89.999977111816406 53.6 89.999977111816406 54.4 89.999977111816406 55.2 89.999977111816406
		 56 89.999977111816406 56.8 89.999977111816406 57.6 89.999977111816406 58.4 89.999977111816406
		 59.2 89.999977111816406 60 89.999977111816406 60.8 89.999977111816406 61.6 89.999977111816406
		 62.4 89.999977111816406 63.2 89.999977111816406 64 89.999977111816406 64.8 89.999977111816406
		 65.6 89.999984741210937 66.4 89.999977111816406 67.2 89.999977111816406 68 89.999984741210937
		 68.8 89.999984741210937 69.6 89.999977111816406 70.4 89.999977111816406 71.2 89.999977111816406
		 72 89.999984741210937 72.8 89.999977111816406;
select -ne :time1;
	setAttr ".o" 24.2;
	setAttr ".unw" 24.2;
select -ne :renderPartition;
	setAttr -s 126 ".st";
select -ne :initialShadingGroup;
	setAttr ".ro" yes;
select -ne :initialParticleSE;
	setAttr ".ro" yes;
select -ne :defaultShaderList1;
	setAttr -s 64 ".s";
select -ne :defaultTextureList1;
	setAttr -s 31 ".tx";
select -ne :postProcessList1;
	setAttr -s 2 ".p";
select -ne :defaultRenderUtilityList1;
	setAttr -s 31 ".u";
select -ne :defaultRenderingList1;
select -ne :renderGlobalsList1;
select -ne :defaultResolution;
	setAttr ".pa" 1;
select -ne :hardwareRenderGlobals;
	setAttr ".ctrs" 256;
	setAttr ".btrs" 512;
select -ne :hardwareRenderingGlobals;
	setAttr ".otfna" -type "stringArray" 18 "NURBS Curves" "NURBS Surfaces" "Polygons" "Subdiv Surfaces" "Particles" "Fluids" "Image Planes" "UI:" "Lights" "Cameras" "Locators" "Joints" "IK Handles" "Deformers" "Motion Trails" "Components" "Misc. UI" "Ornaments"  ;
	setAttr ".otfva" -type "Int32Array" 18 0 1 1 1 1 1
		 1 0 0 0 0 0 0 0 0 0 0 0 ;
select -ne :defaultHardwareRenderGlobals;
	setAttr ".fn" -type "string" "im";
	setAttr ".res" -type "string" "ntsc_4d 646 485 1.333";
select -ne :hyperGraphLayout;
	setAttr -s 4 ".hyp";
	setAttr ".hyp[1].isc" yes;
	setAttr ".hyp[2].isc" yes;
	setAttr ".hyp[3].isc" yes;
	setAttr ".hyp[4].isc" yes;
connectAttr "Bip01_Spine_translateX11.o" "|Obese_Sit_Idle_1|Bip01_Spine.tx";
connectAttr "Bip01_Spine_translateY11.o" "|Obese_Sit_Idle_1|Bip01_Spine.ty";
connectAttr "Bip01_Spine_translateZ11.o" "|Obese_Sit_Idle_1|Bip01_Spine.tz";
connectAttr "Bip01_Spine_rotateX11.o" "|Obese_Sit_Idle_1|Bip01_Spine.rx";
connectAttr "Bip01_Spine_rotateY11.o" "|Obese_Sit_Idle_1|Bip01_Spine.ry";
connectAttr "Bip01_Spine_rotateZ11.o" "|Obese_Sit_Idle_1|Bip01_Spine.rz";
connectAttr "|Obese_Sit_Idle_1|Bip01_Spine.s" "|Obese_Sit_Idle_1|Bip01_Spine|Bip01_Spine1.is"
		;
connectAttr "Bip01_Spine1_rotateX11.o" "|Obese_Sit_Idle_1|Bip01_Spine|Bip01_Spine1.rx"
		;
connectAttr "Bip01_Spine1_rotateY11.o" "|Obese_Sit_Idle_1|Bip01_Spine|Bip01_Spine1.ry"
		;
connectAttr "Bip01_Spine1_rotateZ11.o" "|Obese_Sit_Idle_1|Bip01_Spine|Bip01_Spine1.rz"
		;
connectAttr "|Obese_Sit_Idle_1|Bip01_Spine|Bip01_Spine1.s" "|Obese_Sit_Idle_1|Bip01_Spine|Bip01_Spine1|Bip01_Spine2.is"
		;
connectAttr "Bip01_Spine2_rotateX11.o" "|Obese_Sit_Idle_1|Bip01_Spine|Bip01_Spine1|Bip01_Spine2.rx"
		;
connectAttr "Bip01_Spine2_rotateY11.o" "|Obese_Sit_Idle_1|Bip01_Spine|Bip01_Spine1|Bip01_Spine2.ry"
		;
connectAttr "Bip01_Spine2_rotateZ11.o" "|Obese_Sit_Idle_1|Bip01_Spine|Bip01_Spine1|Bip01_Spine2.rz"
		;
connectAttr "|Obese_Sit_Idle_1|Bip01_Spine|Bip01_Spine1|Bip01_Spine2.s" "|Obese_Sit_Idle_1|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck.is"
		;
connectAttr "|Obese_Sit_Idle_1|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck.s" "|Obese_Sit_Idle_1|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_Head.is"
		;
connectAttr "|Obese_Sit_Idle_1|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_Head.s" "|Obese_Sit_Idle_1|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_Head|_notused_Bip01_HeadNub.is"
		;
connectAttr "|Obese_Sit_Idle_1|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck.s" "|Obese_Sit_Idle_1|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_R_Clavicle.is"
		;
connectAttr "Bip01_R_Clavicle_rotateX11.o" "|Obese_Sit_Idle_1|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_R_Clavicle.rx"
		;
connectAttr "Bip01_R_Clavicle_rotateY11.o" "|Obese_Sit_Idle_1|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_R_Clavicle.ry"
		;
connectAttr "Bip01_R_Clavicle_rotateZ11.o" "|Obese_Sit_Idle_1|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_R_Clavicle.rz"
		;
connectAttr "|Obese_Sit_Idle_1|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_R_Clavicle.s" "|Obese_Sit_Idle_1|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_R_Clavicle|Bip01_R_UpperArm.is"
		;
connectAttr "Bip01_R_UpperArm_rotateX11.o" "|Obese_Sit_Idle_1|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_R_Clavicle|Bip01_R_UpperArm.rx"
		;
connectAttr "Bip01_R_UpperArm_rotateY11.o" "|Obese_Sit_Idle_1|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_R_Clavicle|Bip01_R_UpperArm.ry"
		;
connectAttr "Bip01_R_UpperArm_rotateZ11.o" "|Obese_Sit_Idle_1|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_R_Clavicle|Bip01_R_UpperArm.rz"
		;
connectAttr "|Obese_Sit_Idle_1|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_R_Clavicle|Bip01_R_UpperArm.s" "|Obese_Sit_Idle_1|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_R_Clavicle|Bip01_R_UpperArm|Bip01_R_Forearm.is"
		;
connectAttr "Bip01_R_Forearm_rotateX11.o" "|Obese_Sit_Idle_1|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_R_Clavicle|Bip01_R_UpperArm|Bip01_R_Forearm.rx"
		;
connectAttr "Bip01_R_Forearm_rotateY11.o" "|Obese_Sit_Idle_1|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_R_Clavicle|Bip01_R_UpperArm|Bip01_R_Forearm.ry"
		;
connectAttr "Bip01_R_Forearm_rotateZ11.o" "|Obese_Sit_Idle_1|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_R_Clavicle|Bip01_R_UpperArm|Bip01_R_Forearm.rz"
		;
connectAttr "|Obese_Sit_Idle_1|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_R_Clavicle|Bip01_R_UpperArm|Bip01_R_Forearm.s" "|Obese_Sit_Idle_1|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_R_Clavicle|Bip01_R_UpperArm|Bip01_R_Forearm|Bip01_R_Hand.is"
		;
connectAttr "Bip01_R_Hand_rotateX1.o" "|Obese_Sit_Idle_1|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_R_Clavicle|Bip01_R_UpperArm|Bip01_R_Forearm|Bip01_R_Hand.rx"
		;
connectAttr "Bip01_R_Hand_rotateY1.o" "|Obese_Sit_Idle_1|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_R_Clavicle|Bip01_R_UpperArm|Bip01_R_Forearm|Bip01_R_Hand.ry"
		;
connectAttr "Bip01_R_Hand_rotateZ1.o" "|Obese_Sit_Idle_1|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_R_Clavicle|Bip01_R_UpperArm|Bip01_R_Forearm|Bip01_R_Hand.rz"
		;
connectAttr "|Obese_Sit_Idle_1|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_R_Clavicle|Bip01_R_UpperArm|Bip01_R_Forearm|Bip01_R_Hand.s" "|Obese_Sit_Idle_1|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_R_Clavicle|Bip01_R_UpperArm|Bip01_R_Forearm|Bip01_R_Hand|Bip01_R_Finger0.is"
		;
connectAttr "|Obese_Sit_Idle_1|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_R_Clavicle|Bip01_R_UpperArm|Bip01_R_Forearm|Bip01_R_Hand|Bip01_R_Finger0.s" "|Obese_Sit_Idle_1|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_R_Clavicle|Bip01_R_UpperArm|Bip01_R_Forearm|Bip01_R_Hand|Bip01_R_Finger0|Bip01_R_Finger01.is"
		;
connectAttr "|Obese_Sit_Idle_1|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_R_Clavicle|Bip01_R_UpperArm|Bip01_R_Forearm|Bip01_R_Hand|Bip01_R_Finger0|Bip01_R_Finger01.s" "|Obese_Sit_Idle_1|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_R_Clavicle|Bip01_R_UpperArm|Bip01_R_Forearm|Bip01_R_Hand|Bip01_R_Finger0|Bip01_R_Finger01|_notused_Bip01_R_Finger0Nub.is"
		;
connectAttr "|Obese_Sit_Idle_1|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_R_Clavicle|Bip01_R_UpperArm|Bip01_R_Forearm|Bip01_R_Hand.s" "|Obese_Sit_Idle_1|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_R_Clavicle|Bip01_R_UpperArm|Bip01_R_Forearm|Bip01_R_Hand|Bip01_R_Finger2.is"
		;
connectAttr "|Obese_Sit_Idle_1|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_R_Clavicle|Bip01_R_UpperArm|Bip01_R_Forearm|Bip01_R_Hand|Bip01_R_Finger2.s" "|Obese_Sit_Idle_1|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_R_Clavicle|Bip01_R_UpperArm|Bip01_R_Forearm|Bip01_R_Hand|Bip01_R_Finger2|Bip01_R_Finger21.is"
		;
connectAttr "|Obese_Sit_Idle_1|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_R_Clavicle|Bip01_R_UpperArm|Bip01_R_Forearm|Bip01_R_Hand|Bip01_R_Finger2|Bip01_R_Finger21.s" "|Obese_Sit_Idle_1|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_R_Clavicle|Bip01_R_UpperArm|Bip01_R_Forearm|Bip01_R_Hand|Bip01_R_Finger2|Bip01_R_Finger21|_notused_Bip01_R_Finger2Nub.is"
		;
connectAttr "|Obese_Sit_Idle_1|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_R_Clavicle|Bip01_R_UpperArm|Bip01_R_Forearm|Bip01_R_Hand.s" "|Obese_Sit_Idle_1|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_R_Clavicle|Bip01_R_UpperArm|Bip01_R_Forearm|Bip01_R_Hand|Bip01_R_Finger1.is"
		;
connectAttr "|Obese_Sit_Idle_1|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_R_Clavicle|Bip01_R_UpperArm|Bip01_R_Forearm|Bip01_R_Hand|Bip01_R_Finger1.s" "|Obese_Sit_Idle_1|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_R_Clavicle|Bip01_R_UpperArm|Bip01_R_Forearm|Bip01_R_Hand|Bip01_R_Finger1|Bip01_R_Finger11.is"
		;
connectAttr "|Obese_Sit_Idle_1|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_R_Clavicle|Bip01_R_UpperArm|Bip01_R_Forearm|Bip01_R_Hand|Bip01_R_Finger1|Bip01_R_Finger11.s" "|Obese_Sit_Idle_1|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_R_Clavicle|Bip01_R_UpperArm|Bip01_R_Forearm|Bip01_R_Hand|Bip01_R_Finger1|Bip01_R_Finger11|_notused_Bip01_R_Finger1Nub.is"
		;
connectAttr "|Obese_Sit_Idle_1|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_R_Clavicle|Bip01_R_UpperArm|Bip01_R_Forearm|Bip01_R_Hand.s" "|Obese_Sit_Idle_1|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_R_Clavicle|Bip01_R_UpperArm|Bip01_R_Forearm|Bip01_R_Hand|Bip01_R_Finger3.is"
		;
connectAttr "|Obese_Sit_Idle_1|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_R_Clavicle|Bip01_R_UpperArm|Bip01_R_Forearm|Bip01_R_Hand|Bip01_R_Finger3.s" "|Obese_Sit_Idle_1|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_R_Clavicle|Bip01_R_UpperArm|Bip01_R_Forearm|Bip01_R_Hand|Bip01_R_Finger3|Bip01_R_Finger31.is"
		;
connectAttr "|Obese_Sit_Idle_1|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_R_Clavicle|Bip01_R_UpperArm|Bip01_R_Forearm|Bip01_R_Hand|Bip01_R_Finger3|Bip01_R_Finger31.s" "|Obese_Sit_Idle_1|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_R_Clavicle|Bip01_R_UpperArm|Bip01_R_Forearm|Bip01_R_Hand|Bip01_R_Finger3|Bip01_R_Finger31|_notused_Bip01_R_Finger3Nub.is"
		;
connectAttr "|Obese_Sit_Idle_1|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_R_Clavicle|Bip01_R_UpperArm|Bip01_R_Forearm|Bip01_R_Hand.s" "|Obese_Sit_Idle_1|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_R_Clavicle|Bip01_R_UpperArm|Bip01_R_Forearm|Bip01_R_Hand|Bip01_R_Finger4.is"
		;
connectAttr "|Obese_Sit_Idle_1|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_R_Clavicle|Bip01_R_UpperArm|Bip01_R_Forearm|Bip01_R_Hand|Bip01_R_Finger4.s" "|Obese_Sit_Idle_1|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_R_Clavicle|Bip01_R_UpperArm|Bip01_R_Forearm|Bip01_R_Hand|Bip01_R_Finger4|Bip01_R_Finger41.is"
		;
connectAttr "|Obese_Sit_Idle_1|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_R_Clavicle|Bip01_R_UpperArm|Bip01_R_Forearm|Bip01_R_Hand|Bip01_R_Finger4|Bip01_R_Finger41.s" "|Obese_Sit_Idle_1|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_R_Clavicle|Bip01_R_UpperArm|Bip01_R_Forearm|Bip01_R_Hand|Bip01_R_Finger4|Bip01_R_Finger41|_notused_Bip01_R_Finger4Nub.is"
		;
connectAttr "|Obese_Sit_Idle_1|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck.s" "|Obese_Sit_Idle_1|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_L_Clavicle.is"
		;
connectAttr "Bip01_L_Clavicle_rotateX11.o" "|Obese_Sit_Idle_1|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_L_Clavicle.rx"
		;
connectAttr "Bip01_L_Clavicle_rotateY11.o" "|Obese_Sit_Idle_1|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_L_Clavicle.ry"
		;
connectAttr "Bip01_L_Clavicle_rotateZ11.o" "|Obese_Sit_Idle_1|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_L_Clavicle.rz"
		;
connectAttr "|Obese_Sit_Idle_1|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_L_Clavicle.s" "|Obese_Sit_Idle_1|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_L_Clavicle|Bip01_L_UpperArm.is"
		;
connectAttr "Bip01_L_UpperArm_rotateX11.o" "|Obese_Sit_Idle_1|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_L_Clavicle|Bip01_L_UpperArm.rx"
		;
connectAttr "Bip01_L_UpperArm_rotateY11.o" "|Obese_Sit_Idle_1|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_L_Clavicle|Bip01_L_UpperArm.ry"
		;
connectAttr "Bip01_L_UpperArm_rotateZ11.o" "|Obese_Sit_Idle_1|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_L_Clavicle|Bip01_L_UpperArm.rz"
		;
connectAttr "|Obese_Sit_Idle_1|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_L_Clavicle|Bip01_L_UpperArm.s" "|Obese_Sit_Idle_1|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_L_Clavicle|Bip01_L_UpperArm|Bip01_L_Forearm.is"
		;
connectAttr "Bip01_L_Forearm_rotateX11.o" "|Obese_Sit_Idle_1|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_L_Clavicle|Bip01_L_UpperArm|Bip01_L_Forearm.rx"
		;
connectAttr "Bip01_L_Forearm_rotateY11.o" "|Obese_Sit_Idle_1|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_L_Clavicle|Bip01_L_UpperArm|Bip01_L_Forearm.ry"
		;
connectAttr "Bip01_L_Forearm_rotateZ11.o" "|Obese_Sit_Idle_1|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_L_Clavicle|Bip01_L_UpperArm|Bip01_L_Forearm.rz"
		;
connectAttr "|Obese_Sit_Idle_1|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_L_Clavicle|Bip01_L_UpperArm|Bip01_L_Forearm.s" "|Obese_Sit_Idle_1|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_L_Clavicle|Bip01_L_UpperArm|Bip01_L_Forearm|Bip01_L_Hand.is"
		;
connectAttr "Bip01_L_Hand_rotateX.o" "|Obese_Sit_Idle_1|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_L_Clavicle|Bip01_L_UpperArm|Bip01_L_Forearm|Bip01_L_Hand.rx"
		;
connectAttr "Bip01_L_Hand_rotateY.o" "|Obese_Sit_Idle_1|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_L_Clavicle|Bip01_L_UpperArm|Bip01_L_Forearm|Bip01_L_Hand.ry"
		;
connectAttr "Bip01_L_Hand_rotateZ.o" "|Obese_Sit_Idle_1|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_L_Clavicle|Bip01_L_UpperArm|Bip01_L_Forearm|Bip01_L_Hand.rz"
		;
connectAttr "|Obese_Sit_Idle_1|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_L_Clavicle|Bip01_L_UpperArm|Bip01_L_Forearm|Bip01_L_Hand.s" "|Obese_Sit_Idle_1|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_L_Clavicle|Bip01_L_UpperArm|Bip01_L_Forearm|Bip01_L_Hand|Bip01_L_Finger0.is"
		;
connectAttr "|Obese_Sit_Idle_1|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_L_Clavicle|Bip01_L_UpperArm|Bip01_L_Forearm|Bip01_L_Hand|Bip01_L_Finger0.s" "|Obese_Sit_Idle_1|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_L_Clavicle|Bip01_L_UpperArm|Bip01_L_Forearm|Bip01_L_Hand|Bip01_L_Finger0|Bip01_L_Finger01.is"
		;
connectAttr "|Obese_Sit_Idle_1|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_L_Clavicle|Bip01_L_UpperArm|Bip01_L_Forearm|Bip01_L_Hand|Bip01_L_Finger0|Bip01_L_Finger01.s" "|Obese_Sit_Idle_1|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_L_Clavicle|Bip01_L_UpperArm|Bip01_L_Forearm|Bip01_L_Hand|Bip01_L_Finger0|Bip01_L_Finger01|_notused_Bip01_L_Finger0Nub.is"
		;
connectAttr "|Obese_Sit_Idle_1|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_L_Clavicle|Bip01_L_UpperArm|Bip01_L_Forearm|Bip01_L_Hand.s" "|Obese_Sit_Idle_1|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_L_Clavicle|Bip01_L_UpperArm|Bip01_L_Forearm|Bip01_L_Hand|Bip01_L_Finger2.is"
		;
connectAttr "|Obese_Sit_Idle_1|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_L_Clavicle|Bip01_L_UpperArm|Bip01_L_Forearm|Bip01_L_Hand|Bip01_L_Finger2.s" "|Obese_Sit_Idle_1|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_L_Clavicle|Bip01_L_UpperArm|Bip01_L_Forearm|Bip01_L_Hand|Bip01_L_Finger2|Bip01_L_Finger21.is"
		;
connectAttr "|Obese_Sit_Idle_1|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_L_Clavicle|Bip01_L_UpperArm|Bip01_L_Forearm|Bip01_L_Hand|Bip01_L_Finger2|Bip01_L_Finger21.s" "|Obese_Sit_Idle_1|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_L_Clavicle|Bip01_L_UpperArm|Bip01_L_Forearm|Bip01_L_Hand|Bip01_L_Finger2|Bip01_L_Finger21|_notused_Bip01_L_Finger2Nub.is"
		;
connectAttr "|Obese_Sit_Idle_1|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_L_Clavicle|Bip01_L_UpperArm|Bip01_L_Forearm|Bip01_L_Hand.s" "|Obese_Sit_Idle_1|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_L_Clavicle|Bip01_L_UpperArm|Bip01_L_Forearm|Bip01_L_Hand|Bip01_L_Finger1.is"
		;
connectAttr "|Obese_Sit_Idle_1|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_L_Clavicle|Bip01_L_UpperArm|Bip01_L_Forearm|Bip01_L_Hand|Bip01_L_Finger1.s" "|Obese_Sit_Idle_1|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_L_Clavicle|Bip01_L_UpperArm|Bip01_L_Forearm|Bip01_L_Hand|Bip01_L_Finger1|Bip01_L_Finger11.is"
		;
connectAttr "|Obese_Sit_Idle_1|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_L_Clavicle|Bip01_L_UpperArm|Bip01_L_Forearm|Bip01_L_Hand|Bip01_L_Finger1|Bip01_L_Finger11.s" "|Obese_Sit_Idle_1|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_L_Clavicle|Bip01_L_UpperArm|Bip01_L_Forearm|Bip01_L_Hand|Bip01_L_Finger1|Bip01_L_Finger11|_notused_Bip01_L_Finger1Nub.is"
		;
connectAttr "|Obese_Sit_Idle_1|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_L_Clavicle|Bip01_L_UpperArm|Bip01_L_Forearm|Bip01_L_Hand.s" "|Obese_Sit_Idle_1|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_L_Clavicle|Bip01_L_UpperArm|Bip01_L_Forearm|Bip01_L_Hand|Bip01_L_Finger3.is"
		;
connectAttr "|Obese_Sit_Idle_1|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_L_Clavicle|Bip01_L_UpperArm|Bip01_L_Forearm|Bip01_L_Hand|Bip01_L_Finger3.s" "|Obese_Sit_Idle_1|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_L_Clavicle|Bip01_L_UpperArm|Bip01_L_Forearm|Bip01_L_Hand|Bip01_L_Finger3|Bip01_L_Finger31.is"
		;
connectAttr "|Obese_Sit_Idle_1|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_L_Clavicle|Bip01_L_UpperArm|Bip01_L_Forearm|Bip01_L_Hand|Bip01_L_Finger3|Bip01_L_Finger31.s" "|Obese_Sit_Idle_1|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_L_Clavicle|Bip01_L_UpperArm|Bip01_L_Forearm|Bip01_L_Hand|Bip01_L_Finger3|Bip01_L_Finger31|_notused_Bip01_L_Finger3Nub.is"
		;
connectAttr "|Obese_Sit_Idle_1|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_L_Clavicle|Bip01_L_UpperArm|Bip01_L_Forearm|Bip01_L_Hand.s" "|Obese_Sit_Idle_1|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_L_Clavicle|Bip01_L_UpperArm|Bip01_L_Forearm|Bip01_L_Hand|Bip01_L_Finger4.is"
		;
connectAttr "|Obese_Sit_Idle_1|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_L_Clavicle|Bip01_L_UpperArm|Bip01_L_Forearm|Bip01_L_Hand|Bip01_L_Finger4.s" "|Obese_Sit_Idle_1|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_L_Clavicle|Bip01_L_UpperArm|Bip01_L_Forearm|Bip01_L_Hand|Bip01_L_Finger4|Bip01_L_Finger41.is"
		;
connectAttr "|Obese_Sit_Idle_1|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_L_Clavicle|Bip01_L_UpperArm|Bip01_L_Forearm|Bip01_L_Hand|Bip01_L_Finger4|Bip01_L_Finger41.s" "|Obese_Sit_Idle_1|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_L_Clavicle|Bip01_L_UpperArm|Bip01_L_Forearm|Bip01_L_Hand|Bip01_L_Finger4|Bip01_L_Finger41|_notused_Bip01_L_Finger4Nub.is"
		;
connectAttr "|Obese_Sit_Idle_1|Bip01_Spine.s" "|Obese_Sit_Idle_1|Bip01_Spine|Bip01_L_Thigh.is"
		;
connectAttr "Bip01_L_Thigh_rotateX11.o" "|Obese_Sit_Idle_1|Bip01_Spine|Bip01_L_Thigh.rx"
		;
connectAttr "Bip01_L_Thigh_rotateY11.o" "|Obese_Sit_Idle_1|Bip01_Spine|Bip01_L_Thigh.ry"
		;
connectAttr "Bip01_L_Thigh_rotateZ11.o" "|Obese_Sit_Idle_1|Bip01_Spine|Bip01_L_Thigh.rz"
		;
connectAttr "|Obese_Sit_Idle_1|Bip01_Spine|Bip01_L_Thigh.s" "|Obese_Sit_Idle_1|Bip01_Spine|Bip01_L_Thigh|Bip01_L_Calf.is"
		;
connectAttr "Bip01_L_Calf_rotateX11.o" "|Obese_Sit_Idle_1|Bip01_Spine|Bip01_L_Thigh|Bip01_L_Calf.rx"
		;
connectAttr "Bip01_L_Calf_rotateY11.o" "|Obese_Sit_Idle_1|Bip01_Spine|Bip01_L_Thigh|Bip01_L_Calf.ry"
		;
connectAttr "Bip01_L_Calf_rotateZ11.o" "|Obese_Sit_Idle_1|Bip01_Spine|Bip01_L_Thigh|Bip01_L_Calf.rz"
		;
connectAttr "|Obese_Sit_Idle_1|Bip01_Spine|Bip01_L_Thigh|Bip01_L_Calf.s" "|Obese_Sit_Idle_1|Bip01_Spine|Bip01_L_Thigh|Bip01_L_Calf|Bip01_L_Foot.is"
		;
connectAttr "Bip01_L_Foot_rotateX11.o" "|Obese_Sit_Idle_1|Bip01_Spine|Bip01_L_Thigh|Bip01_L_Calf|Bip01_L_Foot.rx"
		;
connectAttr "Bip01_L_Foot_rotateY11.o" "|Obese_Sit_Idle_1|Bip01_Spine|Bip01_L_Thigh|Bip01_L_Calf|Bip01_L_Foot.ry"
		;
connectAttr "Bip01_L_Foot_rotateZ11.o" "|Obese_Sit_Idle_1|Bip01_Spine|Bip01_L_Thigh|Bip01_L_Calf|Bip01_L_Foot.rz"
		;
connectAttr "|Obese_Sit_Idle_1|Bip01_Spine|Bip01_L_Thigh|Bip01_L_Calf|Bip01_L_Foot.s" "|Obese_Sit_Idle_1|Bip01_Spine|Bip01_L_Thigh|Bip01_L_Calf|Bip01_L_Foot|Bip01_L_Toe0.is"
		;
connectAttr "Bip01_L_Toe0_rotateX1.o" "|Obese_Sit_Idle_1|Bip01_Spine|Bip01_L_Thigh|Bip01_L_Calf|Bip01_L_Foot|Bip01_L_Toe0.rx"
		;
connectAttr "Bip01_L_Toe0_rotateY1.o" "|Obese_Sit_Idle_1|Bip01_Spine|Bip01_L_Thigh|Bip01_L_Calf|Bip01_L_Foot|Bip01_L_Toe0.ry"
		;
connectAttr "Bip01_L_Toe0_rotateZ1.o" "|Obese_Sit_Idle_1|Bip01_Spine|Bip01_L_Thigh|Bip01_L_Calf|Bip01_L_Foot|Bip01_L_Toe0.rz"
		;
connectAttr "|Obese_Sit_Idle_1|Bip01_Spine|Bip01_L_Thigh|Bip01_L_Calf|Bip01_L_Foot|Bip01_L_Toe0.s" "|Obese_Sit_Idle_1|Bip01_Spine|Bip01_L_Thigh|Bip01_L_Calf|Bip01_L_Foot|Bip01_L_Toe0|_notused_Bip01_L_Toe0Nub.is"
		;
connectAttr "|Obese_Sit_Idle_1|Bip01_Spine.s" "|Obese_Sit_Idle_1|Bip01_Spine|Bip01_R_Thigh.is"
		;
connectAttr "Bip01_R_Thigh_rotateX11.o" "|Obese_Sit_Idle_1|Bip01_Spine|Bip01_R_Thigh.rx"
		;
connectAttr "Bip01_R_Thigh_rotateY11.o" "|Obese_Sit_Idle_1|Bip01_Spine|Bip01_R_Thigh.ry"
		;
connectAttr "Bip01_R_Thigh_rotateZ11.o" "|Obese_Sit_Idle_1|Bip01_Spine|Bip01_R_Thigh.rz"
		;
connectAttr "|Obese_Sit_Idle_1|Bip01_Spine|Bip01_R_Thigh.s" "|Obese_Sit_Idle_1|Bip01_Spine|Bip01_R_Thigh|Bip01_R_Calf.is"
		;
connectAttr "Bip01_R_Calf_rotateX11.o" "|Obese_Sit_Idle_1|Bip01_Spine|Bip01_R_Thigh|Bip01_R_Calf.rx"
		;
connectAttr "Bip01_R_Calf_rotateY11.o" "|Obese_Sit_Idle_1|Bip01_Spine|Bip01_R_Thigh|Bip01_R_Calf.ry"
		;
connectAttr "Bip01_R_Calf_rotateZ11.o" "|Obese_Sit_Idle_1|Bip01_Spine|Bip01_R_Thigh|Bip01_R_Calf.rz"
		;
connectAttr "|Obese_Sit_Idle_1|Bip01_Spine|Bip01_R_Thigh|Bip01_R_Calf.s" "|Obese_Sit_Idle_1|Bip01_Spine|Bip01_R_Thigh|Bip01_R_Calf|Bip01_R_Foot.is"
		;
connectAttr "Bip01_R_Foot_rotateX11.o" "|Obese_Sit_Idle_1|Bip01_Spine|Bip01_R_Thigh|Bip01_R_Calf|Bip01_R_Foot.rx"
		;
connectAttr "Bip01_R_Foot_rotateY11.o" "|Obese_Sit_Idle_1|Bip01_Spine|Bip01_R_Thigh|Bip01_R_Calf|Bip01_R_Foot.ry"
		;
connectAttr "Bip01_R_Foot_rotateZ11.o" "|Obese_Sit_Idle_1|Bip01_Spine|Bip01_R_Thigh|Bip01_R_Calf|Bip01_R_Foot.rz"
		;
connectAttr "|Obese_Sit_Idle_1|Bip01_Spine|Bip01_R_Thigh|Bip01_R_Calf|Bip01_R_Foot.s" "|Obese_Sit_Idle_1|Bip01_Spine|Bip01_R_Thigh|Bip01_R_Calf|Bip01_R_Foot|Bip01_R_Toe0.is"
		;
connectAttr "Bip01_R_Toe0_rotateX1.o" "|Obese_Sit_Idle_1|Bip01_Spine|Bip01_R_Thigh|Bip01_R_Calf|Bip01_R_Foot|Bip01_R_Toe0.rx"
		;
connectAttr "Bip01_R_Toe0_rotateY1.o" "|Obese_Sit_Idle_1|Bip01_Spine|Bip01_R_Thigh|Bip01_R_Calf|Bip01_R_Foot|Bip01_R_Toe0.ry"
		;
connectAttr "Bip01_R_Toe0_rotateZ1.o" "|Obese_Sit_Idle_1|Bip01_Spine|Bip01_R_Thigh|Bip01_R_Calf|Bip01_R_Foot|Bip01_R_Toe0.rz"
		;
connectAttr "|Obese_Sit_Idle_1|Bip01_Spine|Bip01_R_Thigh|Bip01_R_Calf|Bip01_R_Foot|Bip01_R_Toe0.s" "|Obese_Sit_Idle_1|Bip01_Spine|Bip01_R_Thigh|Bip01_R_Calf|Bip01_R_Foot|Bip01_R_Toe0|_notused_Bip01_R_Toe0Nub.is"
		;
// End of Animation v5 - Obese Sit Idle 1.ma
