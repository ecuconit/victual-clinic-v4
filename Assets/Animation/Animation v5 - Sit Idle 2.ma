//Maya ASCII 2014 scene
//Name: Animation v5 - Sit Idle 2.ma
//Last modified: Tue, Apr 14, 2015 07:28:39 AM
//Codeset: 1252
requires maya "2014";
currentUnit -l centimeter -a degree -t film;
fileInfo "application" "maya";
fileInfo "product" "Maya 2014";
fileInfo "version" "2014 x64";
fileInfo "cutIdentifier" "201303010241-864206";
fileInfo "osv" "Microsoft Windows 7 Enterprise Edition, 64-bit Windows 7 Service Pack 1 (Build 7601)\n";
fileInfo "license" "education";
createNode transform -n "Sit_Idle_2";
	setAttr ".v" no;
createNode joint -n "Bip01_Spine" -p "Sit_Idle_2";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".s" -type "double3" 0.99999999999999956 0.99999999999999989 0.99999999999999989 ;
	setAttr ".jo" -type "double3" 90.000171 0 89.999999999999986 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" -0.0081722831436215043 0.99872757760529951 0.04976383746125676 0
		 -0.031468606931617168 -0.049997711134416305 0.99825345261566845 0 0.99947133053730142 0.006592011223395697 0.031837161023350762 0
		 -6.7139604917915256e-006 99.443700000000007 -2.2495999999899592 1;
	setAttr -k on ".liw";
	setAttr ".fbxID" 5;
createNode joint -n "Bip01_Spine1" -p "|Sit_Idle_2|Bip01_Spine";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".t" -type "double3" 13.3984 -0.0107 0 ;
	setAttr ".s" -type "double3" 1 1 0.99999999999999989 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" -0.0087295896663812361 0.015254963731834248 0.99984552824213668 0
		 -0.0050586545493994011 -0.99987150655113555 0.015211193290218554 0 0.99994910084384092 -0.0049250856543791108 0.0088056375629807955 0
		 9.9999999999999995e-007 2.2603360000000001 112.84212599999999 1;
	setAttr -k on ".liw";
	setAttr ".fbxID" 5;
createNode joint -n "Bip01_Spine2" -p "|Sit_Idle_2|Bip01_Spine|Bip01_Spine1";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".t" -type "double3" 13.3983 -0.0107 0 ;
	setAttr ".s" -type "double3" 1 0.99999999999999989 0.99999999999999989 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" 0 0 1 0 0 -1 0 0 1 0 0 0 -3.9999999999999998e-006 -9.6239790000000003 -19.074804 1;
	setAttr -k on ".liw";
	setAttr ".fbxID" 5;
createNode joint -n "Bip01_Neck" -p "|Sit_Idle_2|Bip01_Spine|Bip01_Spine1|Bip01_Spine2";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".t" -type "double3" 13.3985 -0.007899999999999999 0 ;
	setAttr ".s" -type "double3" 0.99999999999999956 0.99999999999999956 0.99999999999999989 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" 0 -0.020830999999999999 0.99978299999999998 0 0 -0.99978299999999998 -0.020830999999999999 0
		 1 0 0 0 -1.0000000000000001e-005 -9.6266999999999996 -5.6762839999999999 1;
	setAttr -k on ".liw";
	setAttr ".fbxID" 5;
createNode joint -n "Bip01_Head" -p "|Sit_Idle_2|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".t" -type "double3" 9.9829 0 0 ;
	setAttr ".s" -type "double3" 0.99999999999999989 1 1 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" 0 -0.10133399999999999 0.99485299999999999 0 0 -0.99485199999999996 -0.10133399999999999 0
		 1 0 0 0 -4.4000000000000012e-005 -9.8346470000000004 4.3044190000000002 1;
	setAttr -k on ".liw";
	setAttr ".fbxID" 5;
createNode joint -n "_notused_Bip01_HeadNub" -p "|Sit_Idle_2|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_Head";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".t" -type "double3" 18.3537 0 0 ;
	setAttr ".ssc" no;
	setAttr ".fbxID" 5;
createNode joint -n "Bip01_R_Clavicle" -p "|Sit_Idle_2|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".t" -type "double3" -0.8268 -0.6612 -3.9165 ;
	setAttr ".s" -type "double3" 1 1 0.99999999999999989 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" -0.99454799999999999 -9.9454800000000002e-007 -0.104286 0
		 -9.8906500000000008e-007 1 -1.0428599999999999e-007 0 0.104286 -5.7179000000000015e-010 -0.99454799999999999 0
		 -3.9165049999999999 2.9251839999999998 138.82619800000001 1;
	setAttr -k on ".liw";
	setAttr ".fbxID" 5;
createNode joint -n "Bip01_R_UpperArm" -p "|Sit_Idle_2|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_R_Clavicle";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".t" -type "double3" 10.6745 0 0 ;
	setAttr ".s" -type "double3" 0.99999999999999989 0.99999999999999989 1 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" -0.75332600000000005 0.10000199999999999 -0.65000000000000002 0
		 0.071782399999999996 0.99496899999999999 0.0698825 0 0.65371900000000005 0.0059857399999999998 -0.75671500000000003 0
		 -14.532831 2.9251239999999998 137.712976 1;
	setAttr -k on ".liw";
	setAttr ".fbxID" 5;
createNode joint -n "Bip01_R_Forearm" -p "|Sit_Idle_2|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_R_Clavicle|Bip01_R_UpperArm";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".t" -type "double3" 25.3375 0 0 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" -0.752861 -0.095945600000000006 -0.65114899999999998 0
		 -0.076500899999999997 0.99536800000000003 -0.058215000000000003 0 0.65371900000000005 0.0059856600000000003 -0.756714 0
		 -33.620182 5.4589869999999996 121.243574 1;
	setAttr -k on ".liw";
	setAttr ".fbxID" 5;
createNode joint -n "Bip01_R_Hand" -p "|Sit_Idle_2|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_R_Clavicle|Bip01_R_UpperArm|Bip01_R_Forearm";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".t" -type "double3" 24.9881 0 0 ;
	setAttr ".r" -type "double3" 90.133325000000013 -6.3004020000000009 2.5653330000000008 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" -0.67922700000000003 -0.050331500000000001 -0.73219999999999996 0
		 0.73278100000000002 0.0092545500000000003 -0.68040299999999998 0 0.0410219 -0.99868900000000005 0.030596000000000002 0
		 -52.432737000000003 3.0614050000000002 104.972679 1;
	setAttr -k on ".liw";
	setAttr ".fbxID" 5;
createNode joint -n "Bip01_R_Finger0" -p "|Sit_Idle_2|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_R_Clavicle|Bip01_R_UpperArm|Bip01_R_Forearm|Bip01_R_Hand";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".t" -type "double3" 2.3036000000000003 1.3941 2.9222 ;
	setAttr ".r" -type "double3" -81.93018 -23.314181 18.295908 ;
	setAttr ".s" -type "double3" 0.99999999999999989 1 1 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" -0.36474400000000001 -0.436471 -0.82246900000000001 0
		 -0.072261699999999998 0.89392799999999994 -0.44234699999999999 0 0.92829899999999999 -0.10191 -0.357595 0
		 -52.855969000000002 0.0400224 102.426821 1;
	setAttr -k on ".liw";
	setAttr ".fbxID" 5;
createNode joint -n "Bip01_R_Finger01" -p "|Sit_Idle_2|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_R_Clavicle|Bip01_R_UpperArm|Bip01_R_Forearm|Bip01_R_Hand|Bip01_R_Finger0";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".t" -type "double3" 4.0806 0 0 ;
	setAttr ".r" -type "double3" 9.4787915988669464e-023 3.0000000000000005e-006 24.064549 ;
	setAttr ".s" -type "double3" 1 1 0.99999999999999989 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" -0.36250900000000003 -0.034022900000000002 -0.93135900000000005 0
		 0.082749500000000004 0.99421099999999996 -0.068527199999999996 0 0.92830000000000001 -0.101911 -0.357595 0
		 -54.344414999999998 -1.7410159999999999 99.070615000000004 1;
	setAttr -k on ".liw";
	setAttr ".fbxID" 5;
createNode joint -n "_notused_Bip01_R_Finger0Nub" -p "|Sit_Idle_2|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_R_Clavicle|Bip01_R_UpperArm|Bip01_R_Forearm|Bip01_R_Hand|Bip01_R_Finger0|Bip01_R_Finger01";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".t" -type "double3" 3.3413 0 0 ;
	setAttr ".r" -type "double3" 1e-006 0 -180 ;
	setAttr ".ssc" no;
	setAttr ".fbxID" 5;
createNode joint -n "Bip01_R_Finger2" -p "|Sit_Idle_2|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_R_Clavicle|Bip01_R_UpperArm|Bip01_R_Forearm|Bip01_R_Hand";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".t" -type "double3" 7.3709 -0.4816 0.6341 ;
	setAttr ".r" -type "double3" 0.043615000000000029 -0.013407000000000007 17.086613999999997 ;
	setAttr ".s" -type "double3" 0.99999999999999989 0.99999999999999989 0.99999999999999989 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" -0.43393399999999999 -0.045623999999999998 -0.89978899999999995 0
		 0.90003599999999995 0.022875199999999998 -0.43521399999999999 0 0.040439000000000003 -0.99869600000000003 0.031137000000000001 0
		 -57.766043000000003 2.0527600000000001 99.922762000000006 1;
	setAttr -k on ".liw";
	setAttr ".fbxID" 5;
createNode joint -n "Bip01_R_Finger21" -p "|Sit_Idle_2|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_R_Clavicle|Bip01_R_UpperArm|Bip01_R_Forearm|Bip01_R_Hand|Bip01_R_Finger2";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".t" -type "double3" 3.8208 0 0 ;
	setAttr ".r" -type "double3" 0 0 29.946389999999987 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" 0.0732873 -0.028113900000000001 -0.99691399999999997 0
		 0.99648999999999999 0.042598999999999998 0.072054900000000005 0 0.0404418 -0.99869600000000003 0.0311372 0
		 -59.424045999999997 1.8784400000000001 96.484806000000006 1;
	setAttr -k on ".liw";
	setAttr ".fbxID" 5;
createNode joint -n "_notused_Bip01_R_Finger2Nub" -p "|Sit_Idle_2|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_R_Clavicle|Bip01_R_UpperArm|Bip01_R_Forearm|Bip01_R_Hand|Bip01_R_Finger2|Bip01_R_Finger21";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".t" -type "double3" 4.7174000000000005 0 0 ;
	setAttr ".r" -type "double3" 0 0 180 ;
	setAttr ".ssc" no;
	setAttr ".fbxID" 5;
createNode joint -n "Bip01_R_Finger1" -p "|Sit_Idle_2|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_R_Clavicle|Bip01_R_UpperArm|Bip01_R_Forearm|Bip01_R_Hand";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".t" -type "double3" 7.3263 -0.211 2.2784 ;
	setAttr ".r" -type "double3" -0.73310399999999953 -5.3145149999999983 22.646732999999994 ;
	setAttr ".s" -type "double3" 1 1 0.99999999999999989 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" -0.33941900000000003 -0.13520499999999999 -0.93086800000000003 0
		 0.93680799999999997 0.040593299999999999 -0.34748099999999998 0 0.084767999999999996 -0.98998600000000003 0.112883 0
		 -57.470132999999997 0.41539700000000002 99.821579 1;
	setAttr -k on ".liw";
	setAttr ".fbxID" 5;
createNode joint -n "Bip01_R_Finger11" -p "|Sit_Idle_2|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_R_Clavicle|Bip01_R_UpperArm|Bip01_R_Forearm|Bip01_R_Hand|Bip01_R_Finger1";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".t" -type "double3" 3.5597999999999996 0 0 ;
	setAttr ".r" -type "double3" -2e-006 3.999999999999999e-006 22.627677999999996 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" 0.047135799999999999 -0.10918 -0.99290400000000001 0
		 0.995286 0.089482699999999998 0.037409400000000002 0 0.084763400000000003 -0.98998600000000003 0.112883 0
		 -58.678344000000003 -0.065903900000000001 96.507863999999998 1;
	setAttr -k on ".liw";
	setAttr ".fbxID" 5;
createNode joint -n "_notused_Bip01_R_Finger1Nub" -p "|Sit_Idle_2|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_R_Clavicle|Bip01_R_UpperArm|Bip01_R_Forearm|Bip01_R_Hand|Bip01_R_Finger1|Bip01_R_Finger11";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".t" -type "double3" 4.1404000000000005 0 0 ;
	setAttr ".r" -type "double3" 0 0 180 ;
	setAttr ".ssc" no;
	setAttr ".fbxID" 5;
createNode joint -n "Bip01_R_Finger3" -p "|Sit_Idle_2|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_R_Clavicle|Bip01_R_UpperArm|Bip01_R_Forearm|Bip01_R_Hand";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".t" -type "double3" 7.2345 -0.5997 -0.9332 ;
	setAttr ".r" -type "double3" 0.043363999999999805 3.826455 18.509659999999997 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" -0.41327900000000001 0.0219578 -0.91034000000000004 0
		 0.91051400000000005 0.0239971 -0.41277999999999998 0 0.0127817 -0.999471 -0.0299104 0
		 -57.824306 3.6237469999999998 100.05504999999999 1;
	setAttr -k on ".liw";
	setAttr ".fbxID" 5;
createNode joint -n "Bip01_R_Finger31" -p "|Sit_Idle_2|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_R_Clavicle|Bip01_R_UpperArm|Bip01_R_Forearm|Bip01_R_Hand|Bip01_R_Finger3";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".t" -type "double3" 3.7241 0 0 ;
	setAttr ".r" -type "double3" 3.0000000000000013e-006 3.0000000000000005e-006 27.984287000000009 ;
	setAttr ".s" -type "double3" 1 0.99999999999999989 0.99999999999999989 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" 0.0622821 0.0306512 -0.99758800000000003 0 0.99797599999999997 0.0108983 0.062641199999999994 0
		 0.012792100000000001 -0.999471 -0.0299104 0 -59.363360999999998 3.705527 96.664748000000003 1;
	setAttr -k on ".liw";
	setAttr ".fbxID" 5;
createNode joint -n "_notused_Bip01_R_Finger3Nub" -p "|Sit_Idle_2|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_R_Clavicle|Bip01_R_UpperArm|Bip01_R_Forearm|Bip01_R_Hand|Bip01_R_Finger3|Bip01_R_Finger31";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".t" -type "double3" 4.2397 0 0 ;
	setAttr ".r" -type "double3" 3e-006 -5e-006 180 ;
	setAttr ".ssc" no;
	setAttr ".fbxID" 5;
createNode joint -n "Bip01_R_Finger4" -p "|Sit_Idle_2|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_R_Clavicle|Bip01_R_UpperArm|Bip01_R_Forearm|Bip01_R_Hand";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".t" -type "double3" 7.2292 -0.4187 -2.3639 ;
	setAttr ".r" -type "double3" -0.17713400000000007 5.5619560000000012 16.615208 ;
	setAttr ".s" -type "double3" 0.99999999999999989 0.99999999999999989 1 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" -0.44323200000000001 0.051426199999999998 -0.89493100000000003 0
		 0.89640699999999995 0.026346700000000001 -0.44244899999999998 0 0.00082491599999999997 -0.99832900000000002 -0.057776500000000001 0
		 -57.746822000000002 5.0544450000000003 99.892002000000005 1;
	setAttr -k on ".liw";
	setAttr ".fbxID" 5;
createNode joint -n "Bip01_R_Finger41" -p "|Sit_Idle_2|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_R_Clavicle|Bip01_R_UpperArm|Bip01_R_Forearm|Bip01_R_Hand|Bip01_R_Finger4";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".t" -type "double3" 3.4791000000000003 0 0 ;
	setAttr ".r" -type "double3" -1.9999999999999995e-006 -3.0000000000000005e-006 29.444368 ;
	setAttr ".s" -type "double3" 1 0.99999999999999989 1 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" 0.054672699999999998 0.057735700000000001 -0.996834 0
		 0.99850399999999995 -0.0023359700000000001 0.054628999999999997 0 0.00082546799999999999 -0.99832900000000002 -0.057777000000000002 0
		 -59.288860999999997 5.2333790000000002 96.778379000000001 1;
	setAttr -k on ".liw";
	setAttr ".fbxID" 5;
createNode joint -n "_notused_Bip01_R_Finger4Nub" -p "|Sit_Idle_2|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_R_Clavicle|Bip01_R_UpperArm|Bip01_R_Forearm|Bip01_R_Hand|Bip01_R_Finger4|Bip01_R_Finger41";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".t" -type "double3" 3.3974 0 0 ;
	setAttr ".r" -type "double3" 0 0 -180 ;
	setAttr ".ssc" no;
	setAttr ".fbxID" 5;
createNode joint -n "Bip01_L_Clavicle" -p "|Sit_Idle_2|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".t" -type "double3" -0.8268 -0.6612 3.9165 ;
	setAttr ".s" -type "double3" 0.99999999999999989 0.99999999999999956 0.99999999999999989 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" 0.99454799999999999 9.9454800000000002e-007 -0.104286 0
		 -9.8906500000000008e-007 1 1.0428599999999999e-007 0 0.104286 -5.7179000000000015e-010 0.99454799999999999 0
		 3.9165070000000002 2.9252039999999999 138.82619700000001 1;
	setAttr -k on ".liw";
	setAttr ".fbxID" 5;
createNode joint -n "Bip01_L_UpperArm" -p "|Sit_Idle_2|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_L_Clavicle";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".t" -type "double3" 10.6745 0 0 ;
	setAttr ".s" -type "double3" 1 1 0.99999999999999989 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" 0.75332500000000002 0.100004 -0.65000000000000002 0
		 -0.071785600000000005 0.99496899999999999 0.069882299999999994 0 0.65371900000000005 -0.0059834299999999997 0.756714 0
		 14.532825000000001 2.925173 137.712974 1;
	setAttr -k on ".liw";
	setAttr ".fbxID" 5;
createNode joint -n "Bip01_L_Forearm" -p "|Sit_Idle_2|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_L_Clavicle|Bip01_L_UpperArm";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".t" -type "double3" 25.3374 0 0 ;
	setAttr ".s" -type "double3" 1 1 0.99999999999999989 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" 0.752861 -0.095943299999999995 -0.65114899999999998 0
		 0.076497700000000002 0.99536899999999995 -0.058215299999999998 0 0.65371900000000005 -0.0059834199999999997 0.756714 0
		 33.620189000000003 5.4590990000000001 121.243576 1;
	setAttr -k on ".liw";
	setAttr ".fbxID" 5;
createNode joint -n "Bip01_L_Hand" -p "|Sit_Idle_2|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_L_Clavicle|Bip01_L_UpperArm|Bip01_L_Forearm";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".t" -type "double3" 24.9881 0 0 ;
	setAttr ".r" -type "double3" -90.133325 6.300397000000002 2.565332999999999 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" 0.67922800000000005 -0.050329499999999999 -0.73219999999999996 0
		 -0.73278100000000002 0.0092525000000000003 -0.68040299999999998 0 0.041019 0.99868999999999997 -0.030595899999999999 0
		 52.432744999999997 3.0615610000000002 104.972662 1;
	setAttr -k on ".liw";
	setAttr ".fbxID" 5;
createNode joint -n "Bip01_L_Finger0" -p "|Sit_Idle_2|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_L_Clavicle|Bip01_L_UpperArm|Bip01_L_Forearm|Bip01_L_Hand";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".t" -type "double3" 2.3036000000000003 1.3941 -2.9222 ;
	setAttr ".r" -type "double3" 81.930180000000021 23.314181000000019 18.295909 ;
	setAttr ".s" -type "double3" 0.99999999999999989 1 1.0000000000000002 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" 0.36474600000000001 -0.43647000000000002 -0.82246900000000001 0
		 0.072258799999999998 0.89392799999999994 -0.44234699999999999 0 0.92830000000000001 0.101913 0.357595 0
		 52.856034999999999 0.0402022 102.42681 1;
	setAttr -k on ".liw";
	setAttr ".fbxID" 5;
createNode joint -n "Bip01_L_Finger01" -p "|Sit_Idle_2|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_L_Clavicle|Bip01_L_UpperArm|Bip01_L_Forearm|Bip01_L_Hand|Bip01_L_Finger0";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".t" -type "double3" 4.0806 0 0 ;
	setAttr ".r" -type "double3" 0 0 24.06455200000001 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" 0.36250900000000003 -0.034021900000000001 -0.93135900000000005 0
		 -0.082751900000000003 0.99421199999999998 -0.068527000000000005 0 0.92829899999999999 0.101913 0.357595 0
		 54.344436000000002 -1.7408459999999999 99.070606999999995 1;
	setAttr -k on ".liw";
	setAttr ".fbxID" 5;
createNode joint -n "_notused_Bip01_L_Finger0Nub" -p "|Sit_Idle_2|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_L_Clavicle|Bip01_L_UpperArm|Bip01_L_Forearm|Bip01_L_Hand|Bip01_L_Finger0|Bip01_L_Finger01";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".t" -type "double3" 3.3413 0 0 ;
	setAttr ".ssc" no;
	setAttr ".fbxID" 5;
createNode joint -n "Bip01_L_Finger2" -p "|Sit_Idle_2|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_L_Clavicle|Bip01_L_UpperArm|Bip01_L_Forearm|Bip01_L_Hand";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".t" -type "double3" 7.3709 -0.4816 -0.6341 ;
	setAttr ".r" -type "double3" -0.043610000000000031 0.01341 17.086619000000006 ;
	setAttr ".s" -type "double3" 1 0.99999999999999989 0.99999999999999989 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" 0.43393500000000002 -0.045622700000000002 -0.89978899999999995 0
		 -0.90003599999999995 0.022871599999999999 -0.43521399999999999 0 0.040435199999999998 0.99869699999999995 -0.031137399999999999 0
		 57.766060000000003 2.0529269999999999 99.922754999999995 1;
	setAttr -k on ".liw";
	setAttr ".fbxID" 5;
createNode joint -n "Bip01_L_Finger21" -p "|Sit_Idle_2|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_L_Clavicle|Bip01_L_UpperArm|Bip01_L_Forearm|Bip01_L_Hand|Bip01_L_Finger2";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".t" -type "double3" 3.8208 0 0 ;
	setAttr ".r" -type "double3" -4.0000000000000024e-006 -4.0000000000000015e-006 29.946390000000019 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" -0.0732873 -0.0281141 -0.99691399999999997 0 -0.99649100000000002 0.042592999999999999 0.072054999999999994 0
		 0.040435899999999997 0.99869699999999995 -0.031137000000000001 0 59.424058000000002 1.878614 96.484800000000007 1;
	setAttr -k on ".liw";
	setAttr ".fbxID" 5;
createNode joint -n "_notused_Bip01_L_Finger2Nub" -p "|Sit_Idle_2|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_L_Clavicle|Bip01_L_UpperArm|Bip01_L_Forearm|Bip01_L_Hand|Bip01_L_Finger2|Bip01_L_Finger21";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".t" -type "double3" 4.7174000000000005 0 0 ;
	setAttr ".ssc" no;
	setAttr ".fbxID" 5;
createNode joint -n "Bip01_L_Finger1" -p "|Sit_Idle_2|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_L_Clavicle|Bip01_L_UpperArm|Bip01_L_Forearm|Bip01_L_Hand";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".t" -type "double3" 7.3263 -0.211 -2.2784 ;
	setAttr ".r" -type "double3" 0.73310799999999965 5.3145129999999989 22.646742999999997 ;
	setAttr ".s" -type "double3" 1 0.99999999999999989 0.99999999999999989 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" 0.33942 -0.13520399999999999 -0.93086800000000003 0
		 -0.93680799999999997 0.040590800000000003 -0.34748099999999998 0 0.084765400000000005 0.98998600000000003 -0.112883 0
		 57.470143999999998 0.415574 99.821611000000004 1;
	setAttr -k on ".liw";
	setAttr ".fbxID" 5;
createNode joint -n "Bip01_L_Finger11" -p "|Sit_Idle_2|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_L_Clavicle|Bip01_L_UpperArm|Bip01_L_Forearm|Bip01_L_Hand|Bip01_L_Finger1";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".t" -type "double3" 3.5597999999999996 0 0 ;
	setAttr ".r" -type "double3" -2.0000000000000012e-006 3.0000000000000005e-006 22.627677000000009 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" -0.047135499999999997 -0.10918 -0.99290400000000001 0
		 -0.99528499999999998 0.089479600000000006 0.037409299999999999 0 0.0847604 0.98998699999999995 -0.112883 0
		 58.678319000000002 -0.065726099999999996 96.507845000000003 1;
	setAttr -k on ".liw";
	setAttr ".fbxID" 5;
createNode joint -n "_notused_Bip01_L_Finger1Nub" -p "|Sit_Idle_2|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_L_Clavicle|Bip01_L_UpperArm|Bip01_L_Forearm|Bip01_L_Hand|Bip01_L_Finger1|Bip01_L_Finger11";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".t" -type "double3" 4.1404000000000005 0 0 ;
	setAttr ".ssc" no;
	setAttr ".fbxID" 5;
createNode joint -n "Bip01_L_Finger3" -p "|Sit_Idle_2|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_L_Clavicle|Bip01_L_UpperArm|Bip01_L_Forearm|Bip01_L_Hand";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".t" -type "double3" 7.2345 -0.5997 0.9332 ;
	setAttr ".r" -type "double3" -0.043362000000000123 -3.826458 18.509666999999997 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" 0.41327900000000001 0.021958700000000001 -0.91034000000000004 0
		 -0.91051400000000005 0.0239966 -0.41277999999999998 0 0.012781000000000001 0.999471 0.029911 0
		 57.824314000000001 3.6238670000000002 100.055049 1;
	setAttr -k on ".liw";
	setAttr ".fbxID" 5;
createNode joint -n "Bip01_L_Finger31" -p "|Sit_Idle_2|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_L_Clavicle|Bip01_L_UpperArm|Bip01_L_Forearm|Bip01_L_Hand|Bip01_L_Finger3";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".t" -type "double3" 3.7241 0 0 ;
	setAttr ".r" -type "double3" 2e-006 -9.4787915988669346e-023 27.984288 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" -0.062282200000000003 0.030651100000000001 -0.99758800000000003 0
		 -0.99797599999999997 0.0108933 0.062641199999999994 0 0.012787099999999999 0.999471 0.029910599999999999 0
		 59.363363 3.7056490000000002 96.664751999999993 1;
	setAttr -k on ".liw";
	setAttr ".fbxID" 5;
createNode joint -n "_notused_Bip01_L_Finger3Nub" -p "|Sit_Idle_2|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_L_Clavicle|Bip01_L_UpperArm|Bip01_L_Forearm|Bip01_L_Hand|Bip01_L_Finger3|Bip01_L_Finger31";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".t" -type "double3" 4.2397 0 0 ;
	setAttr ".ssc" no;
	setAttr ".fbxID" 5;
createNode joint -n "Bip01_L_Finger4" -p "|Sit_Idle_2|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_L_Clavicle|Bip01_L_UpperArm|Bip01_L_Forearm|Bip01_L_Hand";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".t" -type "double3" 7.2292 -0.4187 2.3639 ;
	setAttr ".r" -type "double3" 0.17713199999999998 -5.5619539999999983 16.615218000000002 ;
	setAttr ".s" -type "double3" 1 0.99999999999999989 1 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" 0.44323200000000001 0.051427599999999997 -0.89493100000000003 0
		 -0.89640600000000004 0.026343800000000001 -0.44244899999999998 0 0.00082181400000000003 0.99832900000000002 0.057776399999999999 0
		 57.746775 5.054627 99.891990000000007 1;
	setAttr -k on ".liw";
	setAttr ".fbxID" 5;
createNode joint -n "Bip01_L_Finger41" -p "|Sit_Idle_2|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_L_Clavicle|Bip01_L_UpperArm|Bip01_L_Forearm|Bip01_L_Hand|Bip01_L_Finger4";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".t" -type "double3" 3.4791000000000003 0 0 ;
	setAttr ".r" -type "double3" 2e-006 -3.0000000000000009e-006 29.444363999999997 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" -0.054672800000000001 0.057735500000000002 -0.996834 0
		 -0.99850399999999995 -0.00234096 0.054628799999999998 0 0.00082047800000000003 0.99832900000000002 0.057777200000000001 0
		 59.288862000000002 5.2335589999999996 96.778389000000004 1;
	setAttr -k on ".liw";
	setAttr ".fbxID" 5;
createNode joint -n "_notused_Bip01_L_Finger4Nub" -p "|Sit_Idle_2|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_L_Clavicle|Bip01_L_UpperArm|Bip01_L_Forearm|Bip01_L_Hand|Bip01_L_Finger4|Bip01_L_Finger41";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".t" -type "double3" 3.3974 0 0 ;
	setAttr ".ssc" no;
	setAttr ".fbxID" 5;
createNode joint -n "Bip01_L_Thigh" -p "|Sit_Idle_2|Bip01_Spine";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".t" -type "double3" -11.0456 2.2584 8.5201999999999991 ;
	setAttr ".s" -type "double3" 1 0.99999999999999989 1.0000000000000002 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" 0.059694200000000003 0.0067868399999999997 -0.998193 0
		 0.0013515700000000001 -0.99997599999999998 -0.00671814 0 -0.99821599999999999 -0.00094809699999999996 -0.059701999999999998 0
		 8.5202019999999994 4.3064900000000002e-005 88.396292000000003 1;
	setAttr -k on ".liw";
	setAttr ".fbxID" 5;
createNode joint -n "Bip01_L_Calf" -p "|Sit_Idle_2|Bip01_Spine|Bip01_L_Thigh";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".t" -type "double3" 36.1527 0 0 ;
	setAttr ".s" -type "double3" 0.99999999999999989 0.99999999999999989 0.99999999999999989 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" 0.059157700000000001 0.119882 -0.99102400000000002 0
		 0.0080950799999999993 -0.99278699999999998 -0.119612 0 -0.99821599999999999 -0.00094644599999999998 -0.059701400000000002 0
		 10.678299000000001 0.245397 52.308965999999998 1;
	setAttr -k on ".liw";
	setAttr ".fbxID" 5;
createNode joint -n "Bip01_L_Foot" -p "|Sit_Idle_2|Bip01_Spine|Bip01_L_Thigh|Bip01_L_Calf";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".t" -type "double3" 43.2543 0 0 ;
	setAttr ".s" -type "double3" 1 1 0.99999999999999989 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" 0 0 -1 0 0.123471 -0.99234800000000001 0 0 -0.99234800000000001 -0.123471 0 0
		 13.237131 5.4308170000000002 9.4429110000000005 1;
	setAttr -k on ".liw";
	setAttr ".fbxID" 5;
createNode joint -n "Bip01_L_Toe0" -p "|Sit_Idle_2|Bip01_Spine|Bip01_L_Thigh|Bip01_L_Calf|Bip01_L_Foot";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".t" -type "double3" 9.4714999999999989 11.2377 0 ;
	setAttr ".r" -type "double3" -5.000000000000003e-006 1.9999999999999995e-006 89.999979999999979 ;
	setAttr ".s" -type "double3" 0.99999999999999989 1 1 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" 0.123471 -0.99234800000000001 0 0 0 0 1 0 -0.99234800000000001 -0.123471 0 0
		 14.624665999999999 -5.7209120000000002 -0.028559000000000001 1;
	setAttr -k on ".liw";
	setAttr ".fbxID" 5;
createNode joint -n "_notused_Bip01_L_Toe0Nub" -p "|Sit_Idle_2|Bip01_Spine|Bip01_L_Thigh|Bip01_L_Calf|Bip01_L_Foot|Bip01_L_Toe0";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".t" -type "double3" 7.2685 0 0 ;
	setAttr ".r" -type "double3" 0 0 180 ;
	setAttr ".ssc" no;
	setAttr ".fbxID" 5;
createNode joint -n "Bip01_R_Thigh" -p "|Sit_Idle_2|Bip01_Spine";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".t" -type "double3" -11.0456 2.2585 -8.5201999999999991 ;
	setAttr ".s" -type "double3" 1 0.99999999999999989 1 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" -0.059694200000000003 0.0067826700000000002 -0.998193 0
		 -0.00134545 -0.99997599999999998 -0.0067143300000000001 0 -0.99821599999999999 0.00094221699999999997 0.059701900000000002 0
		 -8.5201879999999992 1.40303e-005 88.396298000000002 1;
	setAttr -k on ".liw";
	setAttr ".fbxID" 5;
createNode joint -n "Bip01_R_Calf" -p "|Sit_Idle_2|Bip01_Spine|Bip01_R_Thigh";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".t" -type "double3" 36.1527 0 0 ;
	setAttr ".s" -type "double3" 0.99999999999999989 1 1 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" -0.059158000000000002 0.11988699999999999 -0.99102400000000002 0
		 -0.0080972100000000005 -0.99278699999999998 -0.119617 0 -0.99821599999999999 0.00094822899999999998 0.059701999999999998 0
		 -10.678288 0.24518599999999999 52.308996999999998 1;
	setAttr -k on ".liw";
	setAttr ".fbxID" 5;
createNode joint -n "Bip01_R_Foot" -p "|Sit_Idle_2|Bip01_Spine|Bip01_R_Thigh|Bip01_R_Calf";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".t" -type "double3" 43.2543 0 0 ;
	setAttr ".s" -type "double3" 0.99999999999999989 1 1 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" 0 0 -1 0 -0.123469 -0.99234800000000001 0 0 -0.99234800000000001 0.123469 0 0
		 -13.237124 5.4308209999999999 9.442933 1;
	setAttr -k on ".liw";
	setAttr ".fbxID" 5;
createNode joint -n "Bip01_R_Toe0" -p "|Sit_Idle_2|Bip01_Spine|Bip01_R_Thigh|Bip01_R_Calf|Bip01_R_Foot";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".t" -type "double3" 9.4714999999999989 11.2377 0 ;
	setAttr ".r" -type "double3" 1.9999999999999991e-006 -1.9999999999999995e-006 89.999986 ;
	setAttr ".s" -type "double3" 0.99999999999999989 0.99999999999999989 1 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" -0.12346799999999999 -0.99234800000000001 0 0 0 0 1 0
		 -0.99234800000000001 0.12346799999999999 0 0 -14.624625999999999 -5.7209019999999997 -0.028538999999999998 1;
	setAttr -k on ".liw";
	setAttr ".fbxID" 5;
createNode joint -n "_notused_Bip01_R_Toe0Nub" -p "|Sit_Idle_2|Bip01_Spine|Bip01_R_Thigh|Bip01_R_Calf|Bip01_R_Foot|Bip01_R_Toe0";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".t" -type "double3" 7.2685 0 0 ;
	setAttr ".ssc" no;
	setAttr ".fbxID" 5;
createNode animCurveTL -n "Bip01_Spine_translateX4";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 66 ".ktv[0:65]"  0.792 0.44004678726196289 1.824 0.38745748996734614
		 2.86 0.33490651845932007 3.896 0.28357011079788208 4.928 0.23451054096221924 5.964 0.18592466413974759
		 7 0.13831472396850586 8.032 0.10122109204530716 9.068 0.089113734662532806 10.1 0.1010206863284111
		 11.136 0.12805938720703125 12.172 0.18091100454330444 13.204 0.25841543078422546
		 14.24 0.32814520597457886 15.276 0.38552233576774603 16.308 0.46223381161689758 17.344 0.56030559539794922
		 18.376 0.63986575603485107 19.412 0.68156147003173828 20.448 0.70578682422637939
		 21.48 0.72421973943710316 22.516 0.73352682590484619 23.552 0.73136085271835327 24.584 0.71418118476867676
		 25.62 0.69252824783325195 26.652 0.67929589748382568 27.688 0.66790884733200073 28.724 0.65291726589202881
		 29.756 0.63968628644943237 30.792 0.62364083528518677 31.824 0.58888339996337891
		 32.86 0.53980165719985962 33.896 0.50217217206954956 34.928 0.47412258386611938 35.964 0.42509913444519037
		 37 0.35060358047485352 38.032 0.27695560455322266 39.068 0.21916478872299192 40.1 0.16482412815093994
		 41.136 0.097324632108211517 42.172 0.019522465765476227 43.204 -0.057809986174106598
		 44.24 -0.12948136031627655 45.276 -0.18918032944202423 46.308 -0.24016660451889041
		 47.344 -0.30167895555496216 48.376 -0.37671557068824768 49.412 -0.44594749808311462
		 50.448 -0.50528419017791748 51.48 -0.56446021795272827 52.516 -0.61999958753585815
		 53.552 -0.66550391912460327 54.584 -0.70547491312026978 55.62 -0.74565380811691284
		 56.652 -0.78503841161727905 57.688 -0.82261031866073597 58.724 -0.86514127254486084
		 59.756 -0.9117453098297118 60.792 -0.95013582706451427 61.824 -0.97978210449218772
		 62.86 -1.0073833465576172 63.896 -1.032794713973999 64.928 -1.058571457862854 65.964 -1.0915380716323853
		 67 -1.1323165893554687 68.032 -1.1759971380233765;
createNode animCurveTL -n "Bip01_Spine_translateY4";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 66 ".ktv[0:65]"  0.792 68.239799499511719 1.824 68.240371704101563
		 2.86 68.249130249023438 3.896 68.262405395507812 4.928 68.278076171875 5.964 68.296142578125
		 7 68.317543029785156 8.032 68.347557067871094 9.068 68.384521484375 10.1 68.418075561523438
		 11.136 68.446205139160156 12.172 68.478118896484375 13.204 68.516036987304688 14.24 68.551116943359375
		 15.276 68.57952880859375 16.308 68.606346130371094 17.344 68.635848999023437 18.376 68.665077209472656
		 19.412 68.686729431152344 20.448 68.700355529785156 21.48 68.718620300292969 22.516 68.750411987304688
		 23.552 68.785346984863281 24.584 68.818893432617188 25.62 68.863540649414063 26.652 68.912773132324219
		 27.688 68.948020935058594 28.724 68.976615905761719 29.756 69.011878967285156 30.792 69.045524597167969
		 31.824 69.071243286132813 32.86 69.098457336425781 33.896 69.130142211914062 34.928 69.160400390625
		 35.964 69.186630249023438 37 69.209030151367188 38.032 69.23291015625 39.068 69.262054443359375
		 40.1 69.289436340332031 41.136 69.312667846679688 42.172 69.339698791503906 43.204 69.369659423828125
		 44.24 69.395904541015625 45.276 69.420143127441406 46.308 69.445747375488281 47.344 69.471206665039063
		 48.376 69.495590209960937 49.412 69.520957946777344 50.448 69.544395446777344 51.48 69.560157775878906
		 52.516 69.571006774902344 53.552 69.584785461425781 54.584 69.601127624511719 55.62 69.613082885742187
		 56.652 69.620956420898438 57.688 69.624908447265625 58.724 69.621475219726562 59.756 69.61907958984375
		 60.792 69.621559143066406 61.824 69.620697021484375 62.86 69.617561340332031 63.896 69.614082336425781
		 64.928 69.605766296386719 65.964 69.596916198730469 67 69.595832824707031 68.032 69.596778869628906;
createNode animCurveTL -n "Bip01_Spine_translateZ4";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 66 ".ktv[0:65]"  0.792 -4.3157176971435547 1.824 -4.3055734634399414
		 2.86 -4.2986993789672852 3.896 -4.2975544929504395 4.928 -4.3014211654663086 5.964 -4.2963643074035645
		 7 -4.2758116722106934 8.032 -4.2562146186828613 9.068 -4.2477211952209473 10.1 -4.2418313026428223
		 11.136 -4.2308735847473145 12.172 -4.2175745964050293 13.204 -4.2069573402404785
		 14.24 -4.1968545913696289 15.276 -4.1787004470825195 16.308 -4.1483316421508789 17.344 -4.1139850616455078
		 18.376 -4.0861124992370605 19.412 -4.0640449523925781 20.448 -4.0379362106323242
		 21.48 -4.0068082809448242 22.516 -3.9841792583465576 23.552 -3.9709405899047856 24.584 -3.9561681747436519
		 25.62 -3.9400365352630615 26.652 -3.9184634685516362 27.688 -3.881356000900269 28.724 -3.8349053859710698
		 29.756 -3.793020486831665 30.792 -3.755095243453979 31.824 -3.7071561813354497 32.86 -3.6448216438293457
		 33.896 -3.5829939842224121 34.928 -3.535578727722168 35.964 -3.4977207183837891 37 -3.4533789157867432
		 38.032 -3.3887383937835693 39.068 -3.3015997409820557 40.1 -3.2007336616516113 41.136 -3.0966799259185791
		 42.172 -2.9991366863250732 43.204 -2.9057211875915527 44.24 -2.8093788623809814 45.276 -2.7171356678009033
		 46.308 -2.6224539279937744 47.344 -2.5051913261413574 48.376 -2.374354362487793 49.412 -2.2444484233856201
		 50.448 -2.1049177646636963 51.48 -1.9587699174880979 52.516 -1.8225260972976685 53.552 -1.6885882616043093
		 54.584 -1.544744610786438 55.62 -1.3969466686248779 56.652 -1.2526942491531372 57.688 -1.1098800897598269
		 58.724 -0.96493625640869141 59.756 -0.82055294513702404 60.792 -0.67909932136535645
		 61.824 -0.54199898242950439 62.86 -0.41813907027244568 63.896 -0.3095414936542511
		 64.928 -0.20173098146915436 65.964 -0.095500588417053223 67 -0.013520002365112305
		 68.032 0.043486319482326515;
createNode animCurveTA -n "Bip01_Spine_rotateX4";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 66 ".ktv[0:65]"  0.792 0.036053646355867386 1.824 -0.0076246340759098521
		 2.86 -0.048834014683961875 3.896 -0.10087917000055313 4.928 -0.16662101447582245
		 5.964 -0.21813639998435977 7 -0.24869994819164276 8.032 -0.29642751812934875 9.068 -0.39886343479156494
		 10.1 -0.55615794658660889 11.136 -0.75650495290756226 12.172 -1.006807804107666 13.204 -1.2670291662216189
		 14.24 -1.460498571395874 15.276 -1.6182456016540527 16.308 -1.8404737710952757 17.344 -2.0909910202026367
		 18.376 -2.2317287921905522 19.412 -2.2341952323913574 20.448 -2.1506989002227783
		 21.48 -2.0068566799163814 22.516 -1.8348226547241211 23.552 -1.6558375358581543 24.584 -1.4600167274475098
		 25.62 -1.2625763416290283 26.652 -1.093746542930603 27.688 -0.95200806856155396 28.724 -0.82019782066345215
		 29.756 -0.68743288516998291 30.792 -0.52865952253341675 31.824 -0.32229432463645935
		 32.86 -0.12679804861545563 33.896 -0.030527161434292793 34.928 -0.01451164111495018
		 35.964 -0.011538440361618996 37 -0.01797015406191349 38.032 -0.044440258294343955
		 39.068 -0.080163180828094482 40.1 -0.12266242504119873 41.136 -0.17923504114151001
		 42.172 -0.25032883882522583 43.204 -0.31454131007194525 44.24 -0.338492751121521
		 45.276 -0.31807941198348999 46.308 -0.28264066576957708 47.344 -0.24693506956100461
		 48.376 -0.19803465902805328 49.412 -0.13671921193599701 50.448 -0.081576839089393616
		 51.48 -0.034896988421678543 52.516 0.013199337758123876 53.552 0.068121962249279022
		 54.584 0.13372734189033508 55.62 0.20672757923603055 56.652 0.27841272950172424 57.688 0.3462195098400116
		 58.724 0.41132751107215881 59.756 0.48020964860916138 60.792 0.55873209238052368
		 61.824 0.6376611590385437 62.86 0.71028816699981689 63.896 0.78750205039978027 64.928 0.87660139799118053
		 65.964 0.97531199455261253 67 1.0837147235870359 68.032 1.1975688934326172;
createNode animCurveTA -n "Bip01_Spine_rotateY4";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 66 ".ktv[0:65]"  0.792 -9.1799411773681641 1.824 -9.2011346817016602
		 2.86 -9.2142906188964844 3.896 -9.2191677093505859 4.928 -9.2038192749023437 5.964 -9.1856660842895508
		 7 -9.1746330261230469 8.032 -9.1297922134399414 9.068 -8.9899435043334961 10.1 -8.7528047561645526
		 11.136 -8.4366903305053711 12.172 -7.9704365730285645 13.204 -7.3768820762634277
		 14.24 -6.8578453063964844 15.276 -6.4293003082275391 16.308 -5.8770589828491211 17.344 -5.196324348449707
		 18.376 -4.6445589065551758 19.412 -4.3161211013793945 20.448 -4.0893254280090332
		 21.48 -3.9158360958099365 22.516 -3.8037850856781006 23.552 -3.7282230854034424 24.584 -3.6825680732727042
		 25.62 -3.6528036594390874 26.652 -3.6094086170196533 27.688 -3.5564765930175786 28.724 -3.5100040435791016
		 29.756 -3.4672496318817139 30.792 -3.4270570278167725 31.824 -3.408586978912354 32.86 -3.4191315174102783
		 33.896 -3.4232704639434814 34.928 -3.4006171226501465 35.964 -3.3763034343719478
		 37 -3.3642899990081787 38.032 -3.3566224575042725 39.068 -3.3463931083679199 40.1 -3.3274619579315186
		 41.136 -3.2963752746582031 42.172 -3.2523562908172607 43.204 -3.1966722011566162
		 44.24 -3.1389954090118408 45.276 -3.0880160331726074 46.308 -3.0387446880340576 47.344 -2.9887864589691162
		 48.376 -2.9437243938446045 49.412 -2.9021861553192139 50.448 -2.8605878353118896
		 51.48 -2.8200068473815918 52.516 -2.7781386375427246 53.552 -2.7305152416229248 54.584 -2.6770539283752441
		 55.62 -2.6190125942230225 56.652 -2.5604500770568848 57.688 -2.5127606391906734 58.724 -2.4838695526123051
		 59.756 -2.4700162410736084 60.792 -2.4635701179504395 61.824 -2.4630815982818604
		 62.86 -2.4747447967529297 63.896 -2.5061135292053223 64.928 -2.5551471710205078 65.964 -2.6086277961730957
		 67 -2.6648204326629639 68.032 -2.7368617057800293;
createNode animCurveTA -n "Bip01_Spine_rotateZ4";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 66 ".ktv[0:65]"  0.792 -12.563371658325195 1.824 -12.584918022155762
		 2.86 -12.61461353302002 3.896 -12.658093452453612 4.928 -12.708576202392578 5.964 -12.761058807373049
		 7 -12.817403793334959 8.032 -12.876925468444824 9.068 -12.929821014404297 10.1 -12.966983795166016
		 11.136 -12.977288246154783 12.172 -12.937681198120115 13.204 -12.856592178344728
		 14.24 -12.784400939941408 15.276 -12.743849754333498 16.308 -12.708498954772947 17.344 -12.627246856689451
		 18.376 -12.477256774902344 19.412 -12.31402587890625 20.448 -12.189305305480957 21.48 -12.081822395324709
		 22.516 -11.964409828186035 23.552 -11.811012268066406 24.584 -11.572473526000977
		 25.62 -11.265450477600098 26.652 -10.975083351135254 27.688 -10.729724884033203 28.724 -10.502080917358398
		 29.756 -10.272193908691406 30.792 -10.030154228210447 31.824 -9.8032760620117188
		 32.86 -9.5987911224365234 33.896 -9.2941541671752912 34.928 -8.7911510467529297 35.964 -8.221705436706543
		 37 -7.7811594009399414 38.032 -7.4929585456848145 39.068 -7.2751636505126962 40.1 -7.0705337524414062
		 41.136 -6.7949357032775879 42.172 -6.3303160667419434 43.204 -5.7544412612915039
		 44.24 -5.3164653778076172 45.276 -5.0698866844177246 46.308 -4.8759207725524902 47.344 -4.6740097999572754
		 48.376 -4.4890022277832031 49.412 -4.3255205154418945 50.448 -4.1623921394348145
		 51.48 -3.9875025749206543 52.516 -3.8062713146209712 53.552 -3.627138853073121 54.584 -3.4454004764556885
		 55.62 -3.251462459564209 56.652 -3.0616621971130371 57.688 -2.9023492336273193 58.724 -2.7765960693359375
		 59.756 -2.6780343055725098 60.792 -2.5968987941741943 61.824 -2.5246925354003902
		 62.86 -2.4678528308868408 63.896 -2.4317009449005127 64.928 -2.4111931324005127 65.964 -2.4011161327362061
		 67 -2.401728630065918 68.032 -2.4115188121795654;
createNode animCurveTA -n "Bip01_Spine1_rotateX4";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 66 ".ktv[0:65]"  0.792 -3.2946579456329346 1.824 -3.1954319477081299
		 2.86 -3.0932352542877197 3.896 -2.9872751235961914 4.928 -2.8768844604492187 5.964 -2.7615787982940674
		 7 -2.6411259174346924 8.032 -2.515580415725708 9.068 -2.3852732181549072 10.1 -2.2509186267852783
		 11.136 -2.113541841506958 12.172 -1.9743961095809941 13.204 -1.8349285125732422 14.24 -1.6967086791992187
		 15.276 -1.5613101720809937 16.308 -1.4302724599838257 17.344 -1.3049654960632324
		 18.376 -1.1866068840026855 19.412 -1.0761888027191162 20.448 -0.97450751066207875
		 21.48 -0.88209021091461182 22.516 -0.79923051595687877 23.552 -0.72604191303253163
		 24.584 -0.6625049114227296 25.62 -0.60845804214477539 26.652 -0.56360393762588501
		 27.688 -0.52766060829162598 28.724 -0.50027883052825928 29.756 -0.48111018538475031
		 30.792 -0.46983587741851812 31.824 -0.4661396741867066 32.86 -0.46975249052047729
		 33.896 -0.48043644428253174 34.928 -0.49800634384155273 35.964 -0.5223311185836792
		 37 -0.5532955527305603 38.032 -0.59077960252761841 39.068 -0.63460344076156616 40.1 -0.68453693389892578
		 41.136 -0.74023085832595825 42.172 -0.8011896014213562 43.204 -0.8667600154876709
		 44.24 -0.93614256381988525 45.276 -1.0083558559417725 46.308 -1.0822633504867554
		 47.344 -1.1566276550292969 48.376 -1.2301607131958008 49.412 -1.3015749454498291
		 50.448 -1.3697015047073364 51.48 -1.4334440231323242 52.516 -1.4919421672821045 53.552 -1.5445728302001951
		 54.584 -1.5910236835479736 55.62 -1.6312507390975952 56.652 -1.6654782295227053 57.688 -1.6941728591918943
		 58.724 -1.7180233001708984 59.756 -1.7378485202789309 60.792 -1.7545434236526487
		 61.824 -1.7690583467483521 62.86 -1.7822424173355105 63.896 -1.7948447465896606 64.928 -1.8074235916137695
		 65.964 -1.8203742504119873 67 -1.833978533744812 68.032 -1.8482569456100464;
createNode animCurveTA -n "Bip01_Spine1_rotateY4";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 66 ".ktv[0:65]"  0.792 6.8332505226135254 1.824 6.7976546287536621
		 2.86 6.7448186874389648 3.896 6.6750526428222656 4.928 6.5889387130737305 5.964 6.4873652458190918
		 7 6.3715076446533203 8.032 6.2428684234619141 9.068 6.1032218933105478 10.1 5.9545722007751465
		 11.136 5.7991852760314941 12.172 5.6395092010498047 13.204 5.4782094955444336 14.24 5.3179445266723633
		 15.276 5.1614031791687012 16.308 5.0111026763916016 17.344 4.8693914413452148 18.376 4.7382907867431641
		 19.412 4.6194171905517578 20.448 4.5138726234436035 21.48 4.4222102165222168 22.516 4.3444013595581055
		 23.552 4.279850959777832 24.584 4.2273902893066406 25.62 4.1853561401367187 26.652 4.1517138481140137
		 27.688 4.1241054534912109 28.724 4.1000213623046875 29.756 4.0768899917602539 30.792 4.0522556304931641
		 31.824 4.0238661766052246 32.86 3.9898104667663574 33.896 3.9485812187194824 34.928 3.8991348743438721
		 35.964 3.8409512042999263 37 3.7740378379821777 38.032 3.698893785476685 39.068 3.6164155006408691
		 40.1 3.5278851985931396 41.136 3.4348471164703369 42.172 3.3390419483184814 43.204 3.2422986030578613
		 44.24 3.1464388370513916 45.276 3.0532088279724121 46.308 2.9642205238342285 47.344 2.8808932304382329
		 48.376 2.8044745922088623 49.412 2.7360038757324219 50.448 2.6763370037078857 51.48 2.6261646747589111
		 52.516 2.5860123634338379 53.552 2.5562789440155029 54.584 2.5372602939605713 55.62 2.5291564464569092
		 56.652 2.5321102142333984 57.688 2.5462024211883545 58.724 2.5714111328125 59.756 2.6076016426086426
		 60.792 2.6545212268829346 61.824 2.7118260860443115 62.86 2.7790079116821289 63.896 2.8554327487945557
		 64.928 2.9403271675109863 65.964 3.0327467918395996 67 3.1317160129547119 68.032 3.2361299991607666;
createNode animCurveTA -n "Bip01_Spine1_rotateZ4";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 66 ".ktv[0:65]"  0.792 14.893885612487798 1.824 14.940522193908691
		 2.86 14.98939037322998 3.896 15.038713455200197 4.928 15.086394309997562 5.964 15.130176544189451
		 7 15.167737007141112 8.032 15.196773529052734 9.068 15.215065002441408 10.1 15.220588684082031
		 11.136 15.211665153503418 12.172 15.186953544616699 13.204 15.145509719848633 14.24 15.086832046508789
		 15.276 15.010825157165526 16.308 14.917791366577148 17.344 14.808327674865723 18.376 14.683261871337894
		 19.412 14.543630599975588 20.448 14.390520095825195 21.48 14.225081443786619 22.516 14.048418998718263
		 23.552 13.861613273620604 24.584 13.66567325592041 25.62 13.46153450012207 26.652 13.250028610229492
		 27.688 13.03190803527832 28.724 12.807851791381836 29.756 12.578420639038086 30.792 12.344145774841309
		 31.824 12.105470657348633 32.86 11.862792015075685 33.896 11.616447448730469 34.928 11.366683006286619
		 35.964 11.113651275634766 37 10.857468605041504 38.032 10.598228454589844 39.068 10.335960388183594
		 40.1 10.070627212524414 41.136 9.8022699356079102 42.172 9.5308904647827148 43.204 9.2566499710083008
		 44.24 8.9798078536987305 45.276 8.7006988525390625 46.308 8.4199123382568359 47.344 8.1381864547729492
		 48.376 7.8564658164978036 49.412 7.5759043693542516 50.448 7.297886848449707 51.48 7.0240306854248047
		 52.516 6.7560796737670898 53.552 6.4959368705749512 54.584 6.2455711364746094 55.62 6.0070104598999032
		 56.652 5.7821774482727051 57.688 5.5729594230651855 58.724 5.3809528350830078 59.756 5.2075810432434082
		 60.792 5.0538754463195801 61.824 4.9205203056335449 62.86 4.807774543762207 63.896 4.7154293060302743
		 64.928 4.6428875923156738 65.964 4.5891232490539551 67 4.5527858734130859 68.032 4.5321989059448242;
createNode animCurveTA -n "Bip01_Spine2_rotateX4";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 66 ".ktv[0:65]"  0.792 -1.8672379255294802 1.824 -1.8172744512557977
		 2.86 -1.7652907371520996 3.896 -1.7108722925186155 4.928 -1.6536859273910522 5.964 -1.5934520959854126
		 7 -1.5300660133361816 8.032 -1.4635648727416992 9.068 -1.3941749334335327 10.1 -1.3222919702529909
		 11.136 -1.2484593391418457 12.172 -1.1733959913253784 13.204 -1.0979421138763428
		 14.24 -1.0229541063308716 15.276 -0.94934755563735951 16.308 -0.87795549631118763
		 17.344 -0.80959641933441173 18.376 -0.74493545293807995 19.412 -0.68456763029098511
		 20.448 -0.62889796495437622 21.48 -0.57825154066085815 22.516 -0.53274780511856079
		 23.552 -0.49245896935462968 24.584 -0.45730975270271301 25.62 -0.42717474699020386
		 26.652 -0.4018578827381134 27.688 -0.3811319768428803 28.724 -0.36474901437759399
		 29.756 -0.35247460007667542 30.792 -0.34408533573150635 31.824 -0.33937481045722961
		 32.86 -0.33816036581993103 33.896 -0.34029534459114075 34.928 -0.34569558501243597
		 35.964 -0.35428065061569214 37 -0.36602270603179937 38.032 -0.38087183237075806 39.068 -0.39879414439201355
		 40.1 -0.41971352696418768 41.136 -0.44349020719528198 42.172 -0.46992361545562744
		 43.204 -0.49875587224960327 44.24 -0.52959829568862915 45.276 -0.56200981140136719
		 46.308 -0.59545242786407471 47.344 -0.62932372093200684 48.376 -0.66300028562545776
		 49.412 -0.69586032629013062 50.448 -0.72730493545532227 51.48 -0.75679999589920044
		 52.516 -0.7839183211326598 53.552 -0.80834203958511364 54.584 -0.82991254329681396
		 55.62 -0.84858936071395874 56.652 -0.864488184452057 57.688 -0.87784725427627563
		 58.724 -0.88899612426757813 59.756 -0.898334801197052 60.792 -0.90632390975952148
		 61.824 -0.91341543197631847 62.86 -0.92002600431442272 63.896 -0.92653250694274902
		 64.928 -0.93320900201797485 65.964 -0.94025319814681996 67 -0.94777172803878795 68.032 -0.95577609539031982;
createNode animCurveTA -n "Bip01_Spine2_rotateY4";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 66 ".ktv[0:65]"  0.792 3.3096699714660645 1.824 3.2947549819946289
		 2.86 3.2713344097137451 3.896 3.2395915985107422 4.928 3.1998493671417236 5.964 3.1525819301605225
		 7 3.0984046459197998 8.032 3.0380768775939941 9.068 2.9724602699279785 10.1 2.9025609493255615
		 11.136 2.8294632434844971 12.172 2.7543466091156006 13.204 2.6784710884094238 14.24 2.6031262874603271
		 15.276 2.529557466506958 16.308 2.458991527557373 17.344 2.3925251960754395 18.376 2.3311192989349365
		 19.412 2.2755513191223145 20.448 2.2263317108154297 21.48 2.1837184429168701 22.516 2.1476953029632568
		 23.552 2.1179580688476558 24.584 2.0939347743988037 25.62 2.074805736541748 26.652 2.0595591068267822
		 27.688 2.0470478534698486 28.724 2.0360229015350342 29.756 2.0252389907836914 30.792 2.0134694576263428
		 31.824 1.9996205568313601 32.86 1.982747435569763 33.896 1.9621106386184699 34.928 1.9372140169143677
		 35.964 1.9078074693679807 37 1.8739124536514282 38.032 1.8357806205749514 39.068 1.7938940525054925
		 40.1 1.7489010095596311 41.136 1.7015895843505859 42.172 1.652860164642334 43.204 1.6036455631256104
		 44.24 1.5548733472824097 45.276 1.5074666738510134 46.308 1.4622290134429932 47.344 1.4199069738388062
		 48.376 1.3811513185501101 49.412 1.3464967012405396 50.448 1.3163672685623169 51.48 1.2911155223846436
		 52.516 1.2710131406784058 53.552 1.2562500238418579 54.584 1.2469499111175537 55.62 1.243198037147522
		 56.652 1.2450493574142456 57.688 1.252504825592041 58.724 1.2655363082885742 59.756 1.2840570211410522
		 60.792 1.3079274892807009 61.824 1.3369330167770386 62.86 1.370818018913269 63.896 1.4092595577239992
		 64.928 1.4518598318099976 65.964 1.4981623888015747 67 1.5476436614990234 68.032 1.5997966527938845;
createNode animCurveTA -n "Bip01_Spine2_rotateZ4";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 66 ".ktv[0:65]"  0.792 7.4830899238586461 1.824 7.5047612190246591
		 2.86 7.5274853706359854 3.896 7.5503401756286612 4.928 7.5722980499267596 5.964 7.5922603607177708
		 7 7.6090540885925284 8.032 7.6215548515319842 9.068 7.6286659240722656 10.1 7.6294088363647461
		 11.136 7.6229739189147949 12.172 7.608698844909668 13.204 7.5861430168151855 14.24 7.555069923400878
		 15.276 7.51546335220337 16.308 7.4674735069274902 17.344 7.4113893508911133 18.376 7.3476667404174805
		 19.412 7.2767877578735405 20.448 7.199310302734375 21.48 7.1157846450805664 22.516 7.026766300201416
		 23.552 6.9327783584594727 24.584 6.834312915802002 25.62 6.7318415641784668 26.652 6.6257786750793457
		 27.688 6.5164961814880371 28.724 6.4043135643005371 29.756 6.2895240783691406 30.792 6.1723771095275879
		 31.824 6.0530996322631845 32.86 5.9318971633911133 33.896 5.8089284896850586 34.928 5.6843018531799316
		 35.964 5.5580906867980957 37 5.4303464889526367 38.032 5.3011379241943359 39.068 5.1704139709472656
		 40.1 5.0382137298583984 41.136 4.9044890403747559 42.172 4.7692680358886719 43.204 4.6326265335083008
		 44.24 4.4946413040161133 45.276 4.3555355072021484 46.308 4.2155666351318359 47.344 4.0750842094421387
		 48.376 3.934590101242065 49.412 3.7946441173553462 50.448 3.6559627056121826 51.48 3.5193126201629639
		 52.516 3.3856098651885982 53.552 3.2557828426361084 54.584 3.1308403015136719 55.62 3.0117809772491455
		 56.652 2.8995969295501709 57.688 2.7951982021331787 58.724 2.6994411945343018 59.756 2.6130073070526123
		 60.792 2.5364246368408203 61.824 2.4700603485107422 62.86 2.4140009880065918 63.896 2.3681697845458984
		 64.928 2.3322818279266357 65.964 2.3058135509490967 67 2.2880806922912607 68.032 2.2782466411590576;
createNode animCurveTA -n "Bip01_Neck_rotateX3";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 66 ".ktv[0:65]"  0.792 0.11658068746328354 1.824 0.10591562837362288
		 2.86 0.095291569828987122 3.896 0.084866106510162367 4.928 0.074867069721221924 5.964 0.065995268523693085
		 7 0.059785142540931695 8.032 0.049422580748796463 9.068 0.041891366243362427 10.1 0.040293961763381958
		 11.136 0.036812707781791687 12.172 0.036385584622621536 13.204 0.039419520646333694
		 14.24 0.042842604219913483 15.276 0.048603944480419159 16.308 0.05642347410321235
		 17.344 0.066002316772937775 18.376 0.077156029641628265 19.412 0.089666225016117082
		 20.448 0.10329704731702805 21.48 0.1178387925028801 22.516 0.13304872810840607 23.552 0.14874836802482608
		 24.584 0.16474395990371704 25.62 0.18085852265357971 26.652 0.19695359468460083 27.688 0.21290427446365356
		 28.724 0.22858329117298123 29.756 0.24389074742794037 30.792 0.25870135426521307
		 31.824 0.27291151881217957 32.86 0.28638321161270142 33.896 0.29898977279663086 34.928 0.31061729788780212
		 35.964 0.32111978530883789 37 0.33038964867591858 38.032 0.33830323815345764 39.068 0.34455719590187073
		 40.1 0.34854385256767273 41.136 0.35362306237220764 42.172 0.35582107305526733 43.204 0.3555901050567627
		 44.24 0.35459265112876898 45.276 0.35259318351745605 46.308 0.3497123122215271 47.344 0.34626668691635132
		 48.376 0.34262436628341675 49.412 0.33918365836143494 50.448 0.33636626601219177
		 51.48 0.33460062742233276 52.516 0.33429870009422302 53.552 0.33580943942070007 54.584 0.33945539593696594
		 55.62 0.34542065858840948 56.652 0.3538244366645813 57.688 0.36465045809745794 58.724 0.37776327133178711
		 59.756 0.39292657375335693 60.792 0.40977996587753296 61.824 0.42789354920387263
		 62.86 0.4467539787292481 63.896 0.46583014726638794 64.928 0.48455104231834406 65.964 0.50236946344375621
		 67 0.51878130435943615 68.032 0.53335440158843994;
createNode animCurveTA -n "Bip01_Neck_rotateY3";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 66 ".ktv[0:65]"  0.792 -0.46484786272048945 1.824 -0.4219973087310791
		 2.86 -0.3793607354164123 3.896 -0.33759364485740662 4.928 -0.29759484529495239 5.964 -0.26214608550071716
		 7 -0.23737899959087369 8.032 -0.19609378278255463 9.068 -0.16613757610321045 10.1 -0.15979444980621338
		 11.136 -0.14603877067565918 12.172 -0.14447696506977081 13.204 -0.15687869489192963
		 14.24 -0.17076562345027924 15.276 -0.19421468675136569 16.308 -0.22610421478748324
		 17.344 -0.26545223593711853 18.376 -0.31163781881332397 19.412 -0.36397320032119751
		 20.448 -0.42173227667808538 21.48 -0.48420372605323808 22.516 -0.55072498321533214
		 23.552 -0.62066864967346191 24.584 -0.69342225790023804 25.62 -0.76840353012084961
		 26.652 -0.8450617790222168 27.688 -0.9228360652923584 28.724 -1.001143217086792 29.756 -1.0793635845184326
		 30.792 -1.15671706199646 31.824 -1.232396125793457 32.86 -1.3054323196411133 33.896 -1.3748209476470947
		 34.928 -1.4395242929458618 35.964 -1.4984568357467651 37 -1.5506129264831543 38.032 -1.5950256586074831
		 39.068 -1.6298304796218872 40.1 -1.6517850160598757 41.136 -1.678186297416687 42.172 -1.6870540380477905
		 43.204 -1.6833240985870359 44.24 -1.6740943193435669 45.276 -1.6584361791610718 46.308 -1.6373025178909302
		 47.344 -1.6124529838562014 48.376 -1.5859047174453735 49.412 -1.559766411781311 50.448 -1.5361886024475098
		 51.48 -1.5172837972640991 52.516 -1.5049728155136108 53.552 -1.5009068250656128 54.584 -1.5064154863357544
		 55.62 -1.5223332643508911 56.652 -1.5490660667419434 57.688 -1.5864781141281128 58.724 -1.6339279413223269
		 59.756 -1.690339207649231 60.792 -1.7541930675506592 61.824 -1.8236603736877439 62.86 -1.8966559171676636
		 63.896 -1.9709933996200562 64.928 -2.0444152355194096 65.964 -2.1147449016571045
		 67 -2.1800081729888916 68.032 -2.2385177612304687;
createNode animCurveTA -n "Bip01_Neck_rotateZ3";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 66 ".ktv[0:65]"  0.792 1.0353765487670898 1.824 1.0170853137969973
		 2.86 0.99904745817184459 3.896 0.98183250427246083 4.928 0.96623778343200672 5.964 0.95334720611572255
		 7 0.94491010904312134 8.032 0.9344182014465332 9.068 0.93182641267776478 10.1 0.93291425704956055
		 11.136 0.94340753555297852 12.172 0.96433299779891979 13.204 1.0062617063522341 14.24 1.0394785404205322
		 15.276 1.0931932926177981 16.308 1.1645981073379517 17.344 1.253743052482605 18.376 1.3620967864990234
		 19.412 1.4907946586608889 20.448 1.6404508352279665 21.48 1.8111511468887329 22.516 2.0022943019866943
		 23.552 2.212532758712769 24.584 2.4397928714752202 25.62 2.6813600063323975 26.652 2.9338769912719727
		 27.688 3.1935110092163086 28.724 3.4560244083404541 29.756 3.7169835567474374 30.792 3.9718332290649414
		 31.824 4.2161688804626465 32.86 4.4457321166992187 33.896 4.6566729545593262 34.928 4.8456287384033203
		 35.964 5.0097212791442871 37 5.1467313766479501 38.032 5.2550458908081055 39.068 5.3325953483581543
		 40.1 5.3773488998413086 41.136 5.4108738899230957 42.172 5.3901243209838867 43.204 5.3542847633361816
		 44.24 5.2918276786804199 45.276 5.2049589157104492 46.308 5.0967350006103516 47.344 4.9698119163513184
		 48.376 4.8270277976989746 49.412 4.6713628768920898 50.448 4.5058112144470215 51.48 4.3334164619445801
		 52.516 4.1570863723754883 53.552 3.9796915054321285 54.584 3.8038520812988281 55.62 3.63201904296875
		 56.652 3.4663119316101074 57.688 3.3086035251617432 58.724 3.1604137420654297 59.756 3.0230290889739995
		 60.792 2.8974430561065678 61.824 2.7844023704528809 62.86 2.6844854354858398 63.896 2.5980443954467773
		 64.928 2.5252766609191895 65.964 2.4662926197052002 67 2.4211821556091309 68.032 2.3899939060211186;
createNode animCurveTA -n "Bip01_Head_rotateX3";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 66 ".ktv[0:65]"  0.792 0.046574030071496957 1.824 0.054027106612920761
		 2.86 0.060734398663043976 3.896 0.066471830010414124 4.928 0.071059413254261017 5.964 0.074344560503959642
		 7 0.076238438487052917 8.032 0.077595256268978119 9.068 0.076682426035404205 10.1 0.075211368501186371
		 11.136 0.072693318128585802 12.172 0.069247730076313019 13.204 0.065027222037315369
		 14.24 0.060121469199657433 15.276 0.054647099226713181 16.308 0.048673532903194421
		 17.344 0.042329691350460052 18.376 0.035698346793651581 19.412 0.028926722705364227
		 20.448 0.02211771160364151 21.48 0.01544083561748266 22.516 0.0090103177353739738
		 23.552 0.0030052827205508947 24.584 -0.0024441268760710955 25.62 -0.0071899225004017362
		 26.652 -0.011118615977466106 27.688 -0.014115489087998874 28.724 -0.01611209474503994
		 29.756 -0.017068531364202499 30.792 -0.016978135332465172 31.824 -0.015893237665295601
		 32.86 -0.013872259296476841 33.896 -0.011023385450243952 34.928 -0.0075040613301098338
		 35.964 -0.0034661069512367249 37 0.00089335307711735368 38.032 0.0053830635733902454
		 39.068 0.0097893420606851578 40.1 0.013938938267529013 41.136 0.017659435048699379
		 42.172 0.020771201699972156 43.204 0.023157242685556412 44.24 0.024708922952413559
		 45.276 0.025327051058411602 46.308 0.024964712560176849 47.344 0.023590153083205223
		 48.376 0.021209618076682091 49.412 0.01784152165055275 50.448 0.013552813790738584
		 51.48 0.0084003573283553123 52.516 0.0024828482419252396 53.552 -0.0041039767675101757
		 54.584 -0.011277257464826109 55.62 -0.018938569352030751 56.652 -0.027010247111320496
		 57.688 -0.035421282052993774 58.724 -0.044131428003311157 59.756 -0.053114745765924454
		 60.792 -0.062342274934053428 61.824 -0.071805499494075789 62.86 -0.081457234919071198
		 63.896 -0.091242931783199324 64.928 -0.10105323791503906 65.964 -0.11076097935438156
		 67 -0.12016449868679049 68.032 -0.12907233834266665;
createNode animCurveTA -n "Bip01_Head_rotateY3";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 66 ".ktv[0:65]"  0.792 1.0002927780151367 1.824 1.1485408544540403
		 2.86 1.2815839052200315 3.896 1.3955190181732178 4.928 1.4871509075164795 5.964 1.5532362461090088
		 7 1.5913888216018677 8.032 1.617598295211792 9.068 1.5940520763397217 10.1 1.5589767694473269
		 11.136 1.498993992805481 12.172 1.4172677993774414 13.204 1.3174810409545898 14.24 1.2028363943099976
		 15.276 1.0767185688018801 16.308 0.94260299205780029 17.344 0.80395495891571045 18.376 0.66413986682891846
		 19.412 0.52631115913391113 20.448 0.39347398281097407 21.48 0.26830253005027771 22.516 0.15319180488586426
		 23.552 0.050111029297113419 24.584 -0.039305221289396286 25.62 -0.11378391832113266
		 26.652 -0.17243416607379913 27.688 -0.21480545401573181 28.724 -0.24083442986011505
		 29.756 -0.25090688467025757 30.792 -0.24584138393402108 31.824 -0.2268380671739578
		 32.86 -0.19546632468700409 33.896 -0.15357078611850741 34.928 -0.10322289913892746
		 35.964 -0.046771872788667679 37 0.013351050205528736 38.032 0.074652150273323059
		 39.068 0.13473303616046906 40.1 0.19130773842334747 41.136 0.24228051304817197 42.172 0.28574064373970032
		 43.204 0.31999680399894714 44.24 0.34359356760978704 45.276 0.35528558492660522 46.308 0.35400226712226868
		 47.344 0.3388887345790863 48.376 0.30929693579673767 49.412 0.26478511095046997 50.448 0.20512838661670688
		 51.48 0.13029719889163971 52.516 0.040462642908096313 53.552 -0.063998766243457794
		 54.584 -0.18239906430244449 55.62 -0.31372278928756714 56.652 -0.45657256245613098
		 57.688 -0.60922342538833618 58.724 -0.76956576108932495 59.756 -0.9351423978805542
		 60.792 -1.1031885147094729 61.824 -1.2706756591796875 62.86 -1.4344183206558228 63.896 -1.5911107063293457
		 64.928 -1.7375341653823853 65.964 -1.8706631660461426 67 -1.9878470897674561 68.032 -2.0869159698486328;
createNode animCurveTA -n "Bip01_Head_rotateZ3";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 66 ".ktv[0:65]"  0.792 1.7205305099487305 1.824 1.778496265411377
		 2.86 1.8213238716125493 3.896 1.8507936000823977 4.928 1.8695782423019409 5.964 1.8806301355361941
		 7 1.8862723112106325 8.032 1.8958750963211055 9.068 1.9136027097702024 10.1 1.9308019876480107
		 11.136 1.9610959291458132 12.172 2.0050334930419922 13.204 2.0635137557983398 14.24 2.1372909545898437
		 15.276 2.2263753414154053 16.308 2.3301036357879639 17.344 2.4472634792327881 18.376 2.5762636661529541
		 19.412 2.7151615619659424 20.448 2.8618695735931401 21.48 3.0141267776489258 22.516 3.1696901321411133
		 23.552 3.3263745307922363 24.584 3.4820868968963623 25.62 3.6348667144775395 26.652 3.7829504013061519
		 27.688 3.9247519969940186 28.724 4.0588665008544922 29.756 4.1840581893920898 30.792 4.2992677688598633
		 31.824 4.4036684036254883 32.86 4.4965677261352539 33.896 4.5774312019348145 34.928 4.6458206176757812
		 35.964 4.7015051841735849 37 4.7442121505737314 38.032 4.7737436294555664 39.068 4.7898116111755371
		 40.1 4.7920799255371094 41.136 4.7802205085754395 42.172 4.7538666725158691 43.204 4.7126617431640625
		 44.24 4.6563014984130859 45.276 4.5846781730651855 46.308 4.4978699684143066 47.344 4.3963208198547363
		 48.376 4.2808995246887207 49.412 4.1529850959777832 50.448 4.0145807266235352 51.48 3.868316650390625
		 52.516 3.7175295352935791 53.552 3.5661041736602783 54.584 3.4184920787811279 55.62 3.2793874740600586
		 56.652 3.153700590133667 57.688 3.0462522506713867 58.724 2.9615328311920166 59.756 2.9033164978027348
		 60.792 2.8745265007019043 61.824 2.877025842666626 62.86 2.9114046096801758 63.896 2.97702956199646
		 64.928 3.0720479488372807 65.964 3.1933977603912354 67 3.3369128704071045 68.032 3.497693777084351;
createNode animCurveTA -n "Bip01_R_Clavicle_rotateX4";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 66 ".ktv[0:65]"  0.792 0.1337568461894989 1.824 0.051727920770645149
		 2.86 -0.030780769884586338 3.896 -0.11258003860712053 4.928 -0.19175001978874209
		 5.964 -0.26246744394302368 7 -0.31196388602256775 8.032 -0.39559128880500793 9.068 -0.45597720146179194
		 10.1 -0.46859571337699896 11.136 -0.49496531486511225 12.172 -0.49565896391868586
		 13.204 -0.46673566102981562 14.24 -0.43390971422195435 15.276 -0.38002181053161627
		 16.308 -0.30654174089431763 17.344 -0.22298417985439301 18.376 -0.11812587082386018
		 19.412 0.00093916076002642501 20.448 0.12529531121253967 21.48 0.26077136397361755
		 22.516 0.40202188491821289 23.552 0.55853235721588135 24.584 0.72123223543167125
		 25.62 0.87930727005004883 26.652 1.043561577796936 27.688 1.2102870941162107 28.724 1.3773766756057739
		 29.756 1.5434107780456543 30.792 1.7066655158996582 31.824 1.8654308319091799 32.86 2.016876220703125
		 33.896 2.1575174331665044 34.928 2.2987399101257324 35.964 2.4166150093078613 37 2.5227007865905762
		 38.032 2.6137659549713135 39.068 2.685490608215332 40.1 2.731459379196167 41.136 2.790107250213623
		 42.172 2.8071820735931396 43.204 2.8092176914215088 44.24 2.7932021617889404 45.276 2.7694480419158936
		 46.308 2.7341969013214111 47.344 2.6877017021179199 48.376 2.6395201683044434 49.412 2.5897741317749023
		 50.448 2.5424726009368896 51.48 2.5012404918670654 52.516 2.4689586162567139 53.552 2.4514768123626709
		 54.584 2.4459016323089604 55.62 2.4566643238067627 56.652 2.4839894771575928 57.688 2.5267703533172607
		 58.724 2.5823853015899663 59.756 2.656686544418335 60.792 2.7414417266845703 61.824 2.8265550136566162
		 62.86 2.9257862567901611 63.896 3.0184402465820313 64.928 3.1182544231414795 65.964 3.2080698013305664
		 67 3.2928605079650879 68.032 3.3693675994873047;
createNode animCurveTA -n "Bip01_R_Clavicle_rotateY4";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 66 ".ktv[0:65]"  0.792 79.693885803222656 1.824 79.729316711425781
		 2.86 79.765502929687514 3.896 79.80181884765625 4.928 79.837333679199219 5.964 79.869094848632812
		 7 79.890953063964844 8.032 79.92877197265625 9.068 79.954971313476563 10.1 79.960166931152358
		 11.136 79.969413757324219 12.172 79.966285705566406 13.204 79.948493957519531 14.24 79.927665710449219
		 15.276 79.89495849609375 16.308 79.849594116210938 17.344 79.806655883789063 18.376 79.741828918457017
		 19.412 79.665985107421875 20.448 79.595428466796875 21.48 79.515350341796875 22.516 79.435035705566406
		 23.552 79.330307006835938 24.584 79.218582153320327 25.62 79.124000549316406 26.652 79.0184326171875
		 27.688 78.908218383789063 28.724 78.796005249023438 29.756 78.682685852050781 30.792 78.56927490234375
		 31.824 78.456886291503906 32.86 78.349304199218764 33.896 78.252967834472656 34.928 78.125701904296875
		 35.964 78.04052734375 37 77.955284118652358 38.032 77.876304626464844 39.068 77.810188293457017
		 40.1 77.76416015625 41.136 77.691139221191406 42.172 77.668647766113281 43.204 77.634925842285156
		 44.24 77.633445739746094 45.276 77.623428344726563 46.308 77.623786926269531 47.344 77.642417907714844
		 48.376 77.655982971191406 49.412 77.6746826171875 50.448 77.695083618164063 51.48 77.715080261230469
		 52.516 77.733917236328125 53.552 77.740089416503906 54.584 77.748695373535156 55.62 77.74893951416017
		 56.652 77.741500854492188 57.688 77.728599548339844 58.724 77.716110229492188 59.756 77.678436279296875
		 60.792 77.635726928710938 61.824 77.616256713867188 62.86 77.560104370117188 63.896 77.534255981445313
		 64.928 77.475265502929673 65.964 77.441093444824219 67 77.403152465820327 68.032 77.367591857910156;
createNode animCurveTA -n "Bip01_R_Clavicle_rotateZ4";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 66 ".ktv[0:65]"  0.792 174.13888549804687 1.824 174.07402038574219
		 2.86 174.00881958007815 3.896 173.94389343261719 4.928 173.88031005859375 5.964 173.82257080078128
		 7 173.78146362304687 8.032 173.70875549316406 9.068 173.65078735351562 10.1 173.63688659667969
		 11.136 173.59873962402344 12.172 173.57521057128909 13.204 173.55931091308594 14.24 173.55528259277344
		 15.276 173.5504150390625 16.308 173.54518127441406 17.344 173.5361328125 18.376 173.52241516113281
		 19.412 173.50010681152344 20.448 173.46635437011719 21.48 173.42034912109375 22.516 173.36097717285156
		 23.552 173.28854370117187 24.584 173.20323181152344 25.62 173.10697937011719 26.652 173.00177001953125
		 27.688 172.89019775390625 28.724 172.77532958984375 29.756 172.66033935546875 30.792 172.54818725585935
		 31.824 172.44180297851565 32.86 172.34388732910156 33.896 172.25721740722656 34.928 172.17805480957031
		 35.964 172.11640930175784 37 172.06755065917969 38.032 172.03227233886719 39.068 172.010498046875
		 40.1 171.99978637695315 41.136 172.00300598144531 42.172 172.03448486328125 43.204 172.05653381347656
		 44.24 172.09916687011719 45.276 172.15251159667969 46.308 172.21823120117187 47.344 172.29801940917969
		 48.376 172.38919067382812 49.412 172.494140625 50.448 172.61309814453125 51.48 172.74653625488281
		 52.516 172.89486694335938 53.552 173.0566101074219 54.584 173.23341369628906 55.62 173.42277526855469
		 56.652 173.62332153320312 57.688 173.83299255371094 58.724 174.0499267578125 59.756 174.26719665527344
		 60.792 174.48377990722656 61.824 174.70034790039062 62.86 174.90333557128906 63.896 175.09986877441406
		 64.928 175.275146484375 65.964 175.43620300292969 67 175.57472229003906 68.032 175.68942260742187;
createNode animCurveTA -n "Bip01_R_UpperArm_rotateX4";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 66 ".ktv[0:65]"  0.792 6.0990800857543945 1.824 5.8517556190490723
		 2.86 5.8102269172668457 3.896 5.9965534210205078 4.928 6.2024445533752441 5.964 6.1094217300415039
		 7 5.7692832946777344 8.032 5.5401511192321777 9.068 5.4796829223632812 10.1 5.4323768615722656
		 11.136 5.3378419876098633 12.172 5.2037477493286133 13.204 4.9920291900634766 14.24 4.79931640625
		 15.276 4.6391534805297852 16.308 4.4270410537719735 17.344 4.0818390846252441 18.376 3.6584658622741695
		 19.412 3.2381525039672852 20.448 2.7664289474487305 21.48 2.291377067565918 22.516 1.9674530029296875
		 23.552 1.6666338443756104 24.584 1.2491333484649658 25.62 0.8669852614402771 26.652 0.53570157289505005
		 27.688 0.10118187963962556 28.724 -0.4329947829246521 29.756 -0.98505580425262451
		 30.792 -1.502656102180481 31.824 -1.9680608510971067 32.86 -2.3642668724060059 33.896 -2.6369702816009521
		 34.928 -2.8339571952819824 35.964 -3.0835678577423096 37 -3.3772661685943604 38.032 -3.649064302444458
		 39.068 -3.8829972743988037 40.1 -4.0167145729064941 41.136 -4.0583419799804687 42.172 -4.1469130516052246
		 43.204 -4.3079743385314941 44.24 -4.4062943458557129 45.276 -4.3797621726989746 46.308 -4.4005231857299805
		 47.344 -4.6304574012756348 48.376 -4.8614044189453125 49.412 -4.8500661849975586
		 50.448 -4.7599263191223145 51.48 -4.7162632942199707 52.516 -4.5661945343017578 53.552 -4.4587292671203613
		 54.584 -4.7815117835998535 55.62 -5.4403371810913086 56.652 -6.0335516929626465 57.688 -6.5521492958068848
		 58.724 -7.1968154907226571 59.756 -7.9409976005554208 60.792 -8.6935262680053711
		 61.824 -9.3588066101074237 62.86 -9.8591651916503906 63.896 -10.338018417358398 64.928 -10.961429595947266
		 65.964 -11.558413505554199 67 -11.848116874694824 68.032 -12.142559051513672;
createNode animCurveTA -n "Bip01_R_UpperArm_rotateY4";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 66 ".ktv[0:65]"  0.792 -65.223190307617188 1.824 -65.270576477050781
		 2.86 -65.280281066894531 3.896 -65.265243530273438 4.928 -65.270759582519531 5.964 -65.343460083007813
		 7 -65.4398193359375 8.032 -65.475753784179687 9.068 -65.454597473144531 10.1 -65.425262451171875
		 11.136 -65.381233215332031 12.172 -65.273384094238281 13.204 -65.134910583496094
		 14.24 -65.017158508300781 15.276 -64.931266784667969 16.308 -64.834968566894531 17.344 -64.724266052246094
		 18.376 -64.646263122558594 19.412 -64.637245178222656 20.448 -64.679481506347656
		 21.48 -64.730064392089844 22.516 -64.761917114257813 23.552 -64.798080444335937 24.584 -64.877586364746094
		 25.62 -64.977607727050781 26.652 -65.066696166992188 27.688 -65.158599853515625 28.724 -65.263656616210937
		 29.756 -65.373512268066406 30.792 -65.476875305175781 31.824 -65.579681396484375
		 32.86 -65.701034545898437 33.896 -65.812637329101563 34.928 -65.882736206054687 35.964 -65.979545593261719
		 37 -66.160484313964844 38.032 -66.361221313476563 39.068 -66.517356872558594 40.1 -66.648124694824219
		 41.136 -66.793197631835938 42.172 -66.950973510742188 43.204 -67.087852478027344
		 44.24 -67.186264038085937 45.276 -67.257331848144531 46.308 -67.324333190917969 47.344 -67.4146728515625
		 48.376 -67.525184631347656 49.412 -67.6087646484375 50.448 -67.641555786132812 51.48 -67.649986267089844
		 52.516 -67.64404296875 53.552 -67.6065673828125 54.584 -67.544105529785156 55.62 -67.479927062988281
		 56.652 -67.417678833007813 57.688 -67.336585998535156 58.724 -67.236091613769531
		 59.756 -67.126708984375 60.792 -66.988601684570313 61.824 -66.809654235839844 62.86 -66.613578796386719
		 63.896 -66.429107666015625 64.928 -66.234245300292969 65.964 -65.948020935058608
		 67 -65.531494140625 68.032 -64.95782470703125;
createNode animCurveTA -n "Bip01_R_UpperArm_rotateZ4";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 66 ".ktv[0:65]"  0.792 -28.471498489379883 1.824 -28.122364044189453
		 2.86 -28.079921722412109 3.896 -28.383102416992188 4.928 -28.730424880981445 5.964 -28.646551132202148
		 7 -28.192829132080082 8.032 -27.884637832641602 9.068 -27.8046875 10.1 -27.743682861328125
		 11.136 -27.582563400268555 12.172 -27.300861358642575 13.204 -26.889053344726559
		 14.24 -26.481391906738281 15.276 -26.105310440063477 16.308 -25.617763519287113 17.344 -24.890529632568359
		 18.376 -24.024021148681641 19.412 -23.162664413452152 20.448 -22.225093841552734
		 21.48 -21.272071838378903 22.516 -20.525478363037109 23.552 -19.798789978027344 24.584 -18.907960891723633
		 25.62 -18.084016799926761 26.652 -17.336307525634766 27.688 -16.441659927368164 28.724 -15.41597366333008
		 29.756 -14.38100719451905 30.792 -13.412479400634766 31.824 -12.54287147521973 32.86 -11.807156562805176
		 33.896 -11.265951156616213 34.928 -10.82960033416748 35.964 -10.352744102478027 37 -9.8856668472290039
		 38.032 -9.485234260559082 39.068 -9.1318168640136719 40.1 -8.926788330078125 41.136 -8.8852920532226562
		 42.172 -8.8093814849853516 43.204 -8.6428689956665039 44.24 -8.5670242309570312 45.276 -8.6787900924682617
		 46.308 -8.7550344467163104 47.344 -8.5879335403442383 48.376 -8.4692783355712891
		 49.412 -8.7139778137207031 50.448 -9.0843572616577148 51.48 -9.4281949996948242 52.516 -9.9713354110717773
		 53.552 -10.497264862060549 54.584 -10.463679313659668 55.62 -10.016513824462894 56.652 -9.7247381210327148
		 57.688 -9.5858526229858398 58.724 -9.3094873428344727 59.756 -8.9308280944824219
		 60.792 -8.5566558837890625 61.824 -8.3008594512939453 62.86 -8.2714853286743182 63.896 -8.2647819519042969
		 64.928 -8.0107669830322266 65.964 -7.6829609870910636 67 -7.638432502746582 68.032 -7.7691869735717747;
createNode animCurveTA -n "Bip01_R_Forearm_rotateY4";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 66 ".ktv[0:65]"  0.792 1.8792273998260496 1.824 1.8859310150146489
		 2.86 1.8867843151092532 3.896 1.8821737766265867 4.928 1.8767600059509273 5.964 1.8765730857849121
		 7 1.8789674043655402 8.032 1.8763407468795776 9.068 1.870763897895813 10.1 1.8676549196243284
		 11.136 1.8683283329010008 12.172 1.8723077774047852 13.204 1.8794115781784051 14.24 1.8840162754058836
		 15.276 1.8855748176574705 16.308 1.8866611719131468 17.344 1.8948529958724969 18.376 1.906724691390991
		 19.412 1.916410088539124 20.448 1.9243764877319338 21.48 1.9298655986785889 22.516 1.9298015832901001
		 23.552 1.9294049739837651 24.584 1.9329150915145872 25.62 1.9352543354034424 26.652 1.9362219572067259
		 27.688 1.9403614997863767 28.724 1.9461220502853387 29.756 1.9509422779083252 30.792 1.9551914930343628
		 31.824 1.9623152017593382 32.86 1.9652324914932251 33.896 1.9677120447158813 34.928 1.9713685512542729
		 35.964 1.9760758876800537 37 1.9796265363693233 38.032 1.9814826250076296 39.068 1.9816819429397583
		 40.1 1.9779787063598633 41.136 1.9718964099884035 42.172 1.9705436229705813 43.204 1.9753050804138179
		 44.24 1.9792888164520266 45.276 1.9780489206314087 46.308 1.9743715524673466 47.344 1.9723286628723145
		 48.376 1.9696320295333862 49.412 1.9632827043533327 50.448 1.9570651054382324 51.48 1.9519051313400269
		 52.516 1.9427270889282224 53.552 1.9327449798583984 54.584 1.9309216737747192 55.62 1.9345941543579104
		 56.652 1.9348889589309692 57.688 1.9332211017608643 58.724 1.9349904060363767 59.756 1.9381461143493652
		 60.792 1.9408438205718994 61.824 1.9434167146682739 62.86 1.943522095680237 63.896 1.9414796829223635
		 64.928 1.9411923885345459 65.964 1.9419697523117065 67 1.9400839805603027 68.032 1.9353117942810056;
createNode animCurveTA -n "Bip01_R_Forearm_rotateX4";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 66 ".ktv[0:65]"  0.792 -1.0197174549102783 1.824 -1.0273818969726562
		 2.86 -1.0248838663101196 3.896 -1.0125992298126221 4.928 -1.0000224113464355 5.964 -0.99901020526885986
		 7 -1.0057182312011721 8.032 -1.006435751914978 9.068 -1.0016016960144043 10.1 -0.99836039543151855
		 11.136 -0.99665611982345548 12.172 -1.0005649328231812 13.204 -1.0075477361679075
		 14.24 -1.0113010406494141 15.276 -1.0111541748046875 16.308 -1.0175236463546753 17.344 -1.0347039699554443
		 18.376 -1.0542911291122437 19.412 -1.0683091878890991 20.448 -1.0799487829208374
		 21.48 -1.0881257057189939 22.516 -1.0881046056747437 23.552 -1.0903471708297727 24.584 -1.102530837059021
		 25.62 -1.1138269901275637 26.652 -1.1207396984100342 27.688 -1.1310418844223022 28.724 -1.1451895236968994
		 29.756 -1.1596825122833252 30.792 -1.1716556549072266 31.824 -1.1765069961547852
		 32.86 -1.1775165796279907 33.896 -1.1798336505889897 34.928 -1.1885337829589844 35.964 -1.2008823156356812
		 37 -1.2092298269271853 38.032 -1.2114524841308594 39.068 -1.2107095718383789 40.1 -1.2067838907241819
		 41.136 -1.2010238170623779 42.172 -1.2005434036254885 43.204 -1.2048970460891724
		 44.24 -1.2050875425338743 45.276 -1.1986638307571411 46.308 -1.1927953958511353 47.344 -1.191767692565918
		 48.376 -1.1868685483932495 49.412 -1.1709669828414917 50.448 -1.1529419422149658
		 51.48 -1.1380417346954346 52.516 -1.1193869113922119 53.552 -1.1011413335800173 54.584 -1.0964609384536743
		 55.62 -1.1017218828201294 56.652 -1.1027572154998779 57.688 -1.1008548736572266 58.724 -1.1043694019317627
		 59.756 -1.110271215438843 60.792 -1.1148818731307983 61.824 -1.1179642677307129 62.86 -1.116385817527771
		 63.896 -1.112371563911438 64.928 -1.1136486530303955 65.964 -1.1189416646957395 67 -1.1202765703201296
		 68.032 -1.1222374439239502;
createNode animCurveTA -n "Bip01_R_Forearm_rotateZ4";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 66 ".ktv[0:65]"  0.792 -50.155075073242187 1.824 -50.420726776123047
		 2.86 -50.377120971679688 3.896 -50.039627075195313 4.928 -49.685085296630859 5.964 -49.660022735595703
		 7 -49.842010498046875 8.032 -49.819137573242187 9.068 -49.631568908691406 10.1 -49.515178680419922
		 11.136 -49.486968994140625 12.172 -49.631095886230469 13.204 -49.888591766357422
		 14.24 -50.038463592529297 15.276 -50.059066772460938 16.308 -50.213756561279297 17.344 -50.710262298583984
		 18.376 -51.313838958740234 19.412 -51.762908935546875 20.448 -52.134590148925781
		 21.48 -52.394210815429687 22.516 -52.393039703369141 23.552 -52.437492370605469 24.584 -52.759529113769531
		 25.62 -53.046756744384766 26.652 -53.217502593994141 27.688 -53.507991790771499 28.724 -53.909477233886719
		 29.756 -54.308628082275391 30.792 -54.642093658447266 31.824 -54.840644836425781
		 32.86 -54.899879455566406 33.896 -54.984127044677734 34.928 -55.2320556640625 35.964 -55.578399658203139
		 37 -55.817050933837891 38.032 -55.891689300537109 39.068 -55.876659393310547 40.1 -55.740715026855469
		 41.136 -55.533493041992195 42.172 -55.506019592285149 43.204 -55.664924621582031
		 44.24 -55.716838836669922 45.276 -55.550792694091797 46.308 -55.368373870849616 47.344 -55.319328308105469
		 48.376 -55.172096252441413 49.412 -54.723239898681641 50.448 -54.228107452392585
		 51.48 -53.819557189941406 52.516 -53.272571563720703 53.552 -52.724845886230469 54.584 -52.594585418701165
		 55.62 -52.763656616210945 56.652 -52.790912628173821 57.688 -52.724788665771491 58.724 -52.828182220458984
		 59.756 -53.004516601562507 60.792 -53.145042419433594 61.824 -53.250228881835945
		 62.86 -53.216224670410163 63.896 -53.098201751708991 64.928 -53.122756958007813 65.964 -53.252895355224609
		 67 -53.257625579833984 68.032 -53.240512847900391;
createNode animCurveTA -n "Bip01_L_Clavicle_rotateX4";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 66 ".ktv[0:65]"  0.792 1.6171633005142212 1.824 1.5263689756393433
		 2.86 1.4369982481002808 3.896 1.3503788709640503 4.928 1.2685598134994509 5.964 1.1975570917129517
		 7 1.1505160331726074 8.032 1.0689347982406616 9.068 1.0141719579696655 10.1 1.0036174058914185
		 11.136 0.98543810844421387 12.172 0.99443811178207397 13.204 1.0250109434127808 14.24 1.0692697763442991
		 15.276 1.1359888315200806 16.308 1.2113746404647827 17.344 1.3172440528869629 18.376 1.441871762275696
		 19.412 1.5701272487640383 20.448 1.7331229448318479 21.48 1.8965229988098145 22.516 2.0728163719177246
		 23.552 2.2769556045532227 24.584 2.4924170970916748 25.62 2.7009506225585937 26.652 2.9363512992858887
		 27.688 3.1769540309906006 28.724 3.4248282909393311 29.756 3.6643249988555895 30.792 3.9160938262939458
		 31.824 4.1620440483093262 32.86 4.3993492126464844 33.896 4.6263842582702637 34.928 4.8342747688293457
		 35.964 5.0168733596801758 37 5.1841635704040527 38.032 5.323765754699707 39.068 5.4067821502685547
		 40.1 5.4711813926696777 41.136 5.5227136611938477 42.172 5.5098485946655273 43.204 5.4810428619384766
		 44.24 5.3979735374450684 45.276 5.3184971809387207 46.308 5.2177014350891113 47.344 5.1103954315185547
		 48.376 4.9847273826599121 49.412 4.8697128295898437 50.448 4.7671079635620117 51.48 4.6817483901977539
		 52.516 4.618621826171875 53.552 4.580833911895752 54.584 4.579953670501709 55.62 4.6105909347534189
		 56.652 4.6714057922363281 57.688 4.7679100036621094 58.724 4.8973169326782227 59.756 5.0564761161804208
		 60.792 5.2413463592529297 61.824 5.4468574523925781 62.86 5.6669135093688965 63.896 5.8941550254821777
		 64.928 6.1202301979064941 65.964 6.3430128097534189 67 6.5414085388183594 68.032 6.7263484001159677;
createNode animCurveTA -n "Bip01_L_Clavicle_rotateY4";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 66 ".ktv[0:65]"  0.792 -80.219253540039063 1.824 -80.187164306640625
		 2.86 -80.155540466308594 3.896 -80.125083923339844 4.928 -80.096755981445327 5.964 -80.073356628417969
		 7 -80.060813903808594 8.032 -80.034027099609375 9.068 -80.021499633789063 10.1 -80.020561218261719
		 11.136 -80.02655029296875 12.172 -80.045883178710938 13.204 -80.064163208007813 14.24 -80.101203918457017
		 15.276 -80.149406433105469 16.308 -80.187736511230469 17.344 -80.254295349121094
		 18.376 -80.329360961914063 19.412 -80.390121459960938 20.448 -80.485267639160156
		 21.48 -80.56268310546875 22.516 -80.642166137695327 23.552 -80.745101928710938 24.584 -80.847732543945313
		 25.62 -80.927085876464844 26.652 -81.028396606445313 27.688 -81.124343872070327 28.724 -81.218894958496094
		 29.756 -81.295997619628906 30.792 -81.381614685058594 31.824 -81.458091735839844
		 32.86 -81.526069641113281 33.896 -81.587501525878906 34.928 -81.63741302490233 35.964 -81.673927307128906
		 37 -81.710014343261719 38.032 -81.737129211425781 39.068 -81.734779357910156 40.1 -81.744071960449219
		 41.136 -81.734077453613281 42.172 -81.707046508789063 43.204 -81.692092895507813
		 44.24 -81.642379760742187 45.276 -81.609748840332031 46.308 -81.569900512695312 47.344 -81.531875610351563
		 48.376 -81.480361938476563 49.412 -81.437408447265639 50.448 -81.400581359863281
		 51.48 -81.370292663574219 52.516 -81.347763061523438 53.552 -81.333122253417969 54.584 -81.335113525390625
		 55.62 -81.346893310546875 56.652 -81.366264343261719 57.688 -81.398429870605469 58.724 -81.441352844238281
		 59.756 -81.493209838867187 60.792 -81.5523681640625 61.824 -81.616600036621094 62.86 -81.683624267578125
		 63.896 -81.750663757324219 64.928 -81.814514160156264 65.964 -81.877220153808594
		 67 -81.926033020019531 68.032 -81.972686767578125;
createNode animCurveTA -n "Bip01_L_Clavicle_rotateZ4";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 66 ".ktv[0:65]"  0.792 172.26048278808594 1.824 172.37202453613281
		 2.86 172.48202514648435 3.896 172.58859252929687 4.928 172.68902587890625 5.964 172.77630615234375
		 7 172.83580017089844 8.032 172.93202209472656 9.068 172.99534606933594 10.1 173.00679016113281
		 11.136 173.02220153808594 12.172 173.00111389160156 13.204 172.93170166015625 14.24 172.86476135253906
		 15.276 172.7559814453125 16.308 172.61349487304687 17.344 172.4320068359375 18.376 172.21362304687503
		 19.412 171.96293640136719 20.448 171.669189453125 21.48 171.34477233886719 22.516 170.98652648925781
		 23.552 170.59008789062503 24.584 170.16426086425781 25.62 169.72056579589844 26.652 169.24784851074219
		 27.688 168.76029968261719 28.724 168.26197814941406 29.756 167.76641845703125 30.792 167.26872253417969
		 31.824 166.78425598144531 32.86 166.32066345214844 33.896 165.88459777832031 34.928 165.48648071289062
		 35.964 165.13496398925781 37 164.82798767089844 38.032 164.5762939453125 39.068 164.40284729003906
		 40.1 164.29008483886719 41.136 164.19168090820312 42.172 164.21067810058594 43.204 164.27015686035156
		 44.24 164.39764404296875 45.276 164.55532836914062 46.308 164.75445556640625 47.344 164.98081970214844
		 48.376 165.23626708984375 49.412 165.49737548828125 50.448 165.75758361816406 51.48 166.00848388671875
		 52.516 166.24200439453125 53.552 166.45159912109375 54.584 166.6263427734375 55.62 166.76593017578125
		 56.652 166.86851501464844 57.688 166.92897033691406 58.724 166.94792175292972 59.756 166.92686462402344
		 60.792 166.86871337890625 61.824 166.77752685546875 62.86 166.65864562988281 63.896 166.5186767578125
		 64.928 166.36526489257812 65.964 166.20210266113284 67 166.04551696777344 68.032 165.89009094238281;
createNode animCurveTA -n "Bip01_L_UpperArm_rotateX4";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 66 ".ktv[0:65]"  0.792 -10.275147438049316 1.824 -9.9666528701782227
		 2.86 -9.6285314559936523 3.896 -9.3791809082031232 4.928 -9.3204851150512695 5.964 -9.3308677673339844
		 7 -9.2379293441772461 8.032 -9.1365337371826172 9.068 -9.1703796386718768 10.1 -9.2332887649536133
		 11.136 -9.1994647979736328 12.172 -9.15411376953125 13.204 -9.1936464309692383 14.24 -9.2473630905151367
		 15.276 -9.2053461074829102 16.308 -9.0466270446777344 17.344 -8.8237791061401367
		 18.376 -8.5699243545532227 19.412 -8.3381986618041992 20.448 -8.2202243804931641
		 21.48 -8.238896369934082 22.516 -8.5088872909545898 23.552 -9.150057792663576 24.584 -10.03571891784668
		 25.62 -10.941205978393556 26.652 -11.63589572906494 27.688 -12.058489799499512 28.724 -12.541045188903809
		 29.756 -13.423276901245115 30.792 -14.522975921630858 31.824 -15.494593620300297
		 32.86 -16.273130416870117 33.896 -16.800453186035156 34.928 -17.081935882568359 35.964 -17.284950256347656
		 37 -17.432228088378906 38.032 -17.495956420898438 39.068 -17.543386459350586 40.1 -17.495063781738281
		 41.136 -17.089765548706055 42.172 -16.252668380737305 43.204 -15.200475692749023
		 44.24 -14.347419738769528 45.276 -14.027223587036133 46.308 -13.850588798522947 47.344 -13.129047393798828
		 48.376 -12.003389358520508 49.412 -10.969727516174316 50.448 -9.8349847793579102
		 51.48 -8.3986215591430664 52.516 -6.9971990585327157 53.552 -5.786048412322998 54.584 -4.6337637901306152
		 55.62 -3.4388315677642822 56.652 -2.184398889541626 57.688 -0.90781831741333008 58.724 0.36603012681007385
		 59.756 1.5528486967086792 60.792 2.6634178161621098 61.824 3.7680730819702148 62.86 4.7039518356323242
		 63.896 5.4418973922729492 64.928 6.2309846878051758 65.964 6.9515509605407724 67 7.2173042297363281
		 68.032 7.1675214767456055;
createNode animCurveTA -n "Bip01_L_UpperArm_rotateY4";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 66 ".ktv[0:65]"  0.792 68.419960021972656 1.824 68.369010925292969
		 2.86 68.318855285644531 3.896 68.264862060546875 4.928 68.175758361816406 5.964 68.073738098144531
		 7 68.011253356933594 8.032 67.965538024902344 9.068 67.899147033691406 10.1 67.839179992675781
		 11.136 67.819808959960937 12.172 67.841934204101563 13.204 67.878868103027344 14.24 67.893722534179687
		 15.276 67.909355163574219 16.308 67.984649658203125 17.344 68.086631774902344 18.376 68.124961853027344
		 19.412 68.097770690917969 20.448 68.072418212890625 21.48 68.060646057128906 22.516 68.018264770507812
		 23.552 67.970306396484375 24.584 67.989067077636719 25.62 68.025337219238281 26.652 67.984397888183594
		 27.688 67.910316467285156 28.724 67.872825622558594 29.756 67.815750122070313 30.792 67.719558715820312
		 31.824 67.675827026367188 32.86 67.721199035644531 33.896 67.842506408691406 34.928 67.97442626953125
		 35.964 68.000717163085937 37 67.929100036621094 38.032 67.884033203125 39.068 67.909629821777358
		 40.1 67.984451293945313 41.136 68.118431091308594 42.172 68.284934997558594 43.204 68.42462158203125
		 44.24 68.488853454589844 45.276 68.442314147949219 46.308 68.341468811035156 47.344 68.30487060546875
		 48.376 68.319549560546875 49.412 68.298995971679688 50.448 68.265602111816406 51.48 68.239326477050781
		 52.516 68.165657043457031 53.552 68.052009582519531 54.584 67.93878173828125 55.62 67.81878662109375
		 56.652 67.68243408203125 57.688 67.546165466308594 58.724 67.421119689941406 59.756 67.291206359863281
		 60.792 67.149749755859375 61.824 67.012771606445313 62.86 66.87640380859375 63.896 66.737380981445312
		 64.928 66.619377136230469 65.964 66.539871215820312 67 66.481155395507813 68.032 66.426925659179688;
createNode animCurveTA -n "Bip01_L_UpperArm_rotateZ4";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 66 ".ktv[0:65]"  0.792 -33.503959655761719 1.824 -33.342002868652344
		 2.86 -33.104396820068359 3.896 -32.975818634033203 4.928 -33.110591888427734 5.964 -33.347652435302734
		 7 -33.460250854492188 8.032 -33.591449737548828 9.068 -33.945453643798828 10.1 -34.378505706787109
		 11.136 -34.721141815185547 12.172 -35.100543975830078 13.204 -35.654304504394531
		 14.24 -36.253261566162109 15.276 -36.679271697998047 16.308 -36.931476593017578 17.344 -37.235260009765625
		 18.376 -37.648597717285149 19.412 -38.080486297607422 20.448 -38.5960693359375 21.48 -39.250465393066406
		 22.516 -40.124034881591797 23.552 -41.205219268798835 24.584 -42.202751159667969
		 25.62 -43.000312805175795 26.652 -43.618240356445313 27.688 -44.010795593261719 28.724 -44.487865447998047
		 29.756 -45.472400665283203 30.792 -46.697452545166023 31.824 -47.631881713867188
		 32.86 -48.166454315185554 33.896 -48.279243469238281 34.928 -48.070884704589837 35.964 -47.816810607910163
		 37 -47.553592681884766 38.032 -47.234622955322266 39.068 -46.926506042480469 40.1 -46.511516571044922
		 41.136 -45.646163940429688 42.172 -44.260177612304687 43.204 -42.690067291259766
		 44.24 -41.544116973876953 45.276 -41.301460266113281 46.308 -41.396526336669922 47.344 -40.834831237792976
		 48.376 -39.818492889404297 49.412 -39.051460266113281 50.448 -38.237964630126953
		 51.48 -37.074764251708984 52.516 -36.027111053466797 53.552 -35.301067352294922 54.584 -34.692283630371094
		 55.62 -34.031223297119141 56.652 -33.271575927734375 57.688 -32.452110290527344 58.724 -31.594732284545898
		 59.756 -30.807641983032223 60.792 -30.062349319458004 61.824 -29.240171432495117
		 62.86 -28.554431915283203 63.896 -28.036109924316406 64.928 -27.327634811401367 65.964 -26.611822128295898
		 67 -26.449642181396484 68.032 -26.623754501342773;
createNode animCurveTA -n "Bip01_L_Forearm_rotateY4";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 66 ".ktv[0:65]"  0.792 -1.9175049066543577 1.824 -1.9200536012649536
		 2.86 -1.9237563610076909 3.896 -1.9267755746841435 4.928 -1.9254515171051028 5.964 -1.9217501878738403
		 7 -1.920449495315552 8.032 -1.919646143913269 9.068 -1.9151695966720583 10.1 -1.9084693193435669
		 11.136 -1.902218222618103 12.172 -1.8944711685180664 13.204 -1.882856130599976 14.24 -1.8697899580001831
		 15.276 -1.8599759340286257 16.308 -1.8538936376571653 17.344 -1.8488101959228516
		 18.376 -1.8450890779495237 19.412 -1.8411296606063847 20.448 -1.8314483165740969
		 21.48 -1.8132416009902954 22.516 -1.7831449508666992 23.552 -1.7451318502426147 24.584 -1.7122821807861328
		 25.62 -1.6772917509078979 26.652 -1.617618203163147 27.688 -1.5403192043304443 28.724 -1.474395751953125
		 29.756 -1.4228509664535522 30.792 -1.3743088245391846 31.824 -1.3313047885894775
		 32.86 -1.296563982963562 33.896 -1.2704011201858521 34.928 -1.2631762027740481 35.964 -1.2851767539978027
		 37 -1.3275109529495239 38.032 -1.3740085363388062 39.068 -1.4182837009429932 40.1 -1.4579088687896729
		 41.136 -1.4920427799224856 42.172 -1.5219509601593018 43.204 -1.5548411607742312
		 44.24 -1.589593768119812 45.276 -1.6062619686126709 46.308 -1.6049131155014038 47.344 -1.6110361814498899
		 48.376 -1.6273254156112671 49.412 -1.6391409635543823 50.448 -1.6499159336090088
		 51.48 -1.6689509153366089 52.516 -1.689706563949585 53.552 -1.6994456052780151 54.584 -1.7050356864929199
		 55.62 -1.7106021642684937 56.652 -1.7172033786773682 57.688 -1.7240393161773682 58.724 -1.7311022281646729
		 59.756 -1.7359150648117063 60.792 -1.7398651838302612 61.824 -1.7462947368621826
		 62.86 -1.7511298656463623 63.896 -1.7546176910400393 64.928 -1.7629745006561279 65.964 -1.7690181732177734
		 67 -1.7610468864440918 68.032 -1.7462128400802612;
createNode animCurveTA -n "Bip01_L_Forearm_rotateZ4";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 66 ".ktv[0:65]"  0.792 -52.375679016113281 1.824 -52.558086395263672
		 2.86 -52.754470825195313 3.896 -52.869544982910163 4.928 -52.744918823242195 5.964 -52.495166778564453
		 7 -52.3330078125 8.032 -52.145759582519531 9.068 -51.759384155273438 10.1 -51.283981323242195
		 11.136 -50.859794616699219 12.172 -50.418807983398438 13.204 -49.891605377197266
		 14.24 -49.413253784179688 15.276 -49.171459197998047 16.308 -49.093837738037109 17.344 -48.942527770996094
		 18.376 -48.708709716796875 19.412 -48.421005249023438 20.448 -47.945858001708984
		 21.48 -47.153907775878906 22.516 -45.817211151123047 23.552 -44.057773590087891 24.584 -42.409214019775391
		 25.62 -40.607151031494141 26.652 -37.93524169921875 27.688 -34.875232696533203 28.724 -32.483505249023438
		 29.756 -30.72691535949707 30.792 -29.119161605834961 31.824 -27.711973190307617 32.86 -26.572755813598633
		 33.896 -25.683893203735352 34.928 -25.382612228393555 35.964 -26.040929794311523
		 37 -27.432863235473633 38.032 -29.061595916748047 39.068 -30.682369232177734 40.1 -32.167072296142578
		 41.136 -33.458595275878906 42.172 -34.593223571777344 43.204 -35.827930450439453
		 44.24 -37.123386383056641 45.276 -37.756984710693359 46.308 -37.755115509033203 47.344 -38.075912475585938
		 48.376 -38.788036346435554 49.412 -39.298973083496094 50.448 -39.757289886474609
		 51.48 -40.479377746582031 52.516 -41.129440307617195 53.552 -41.479225158691406 54.584 -41.706432342529297
		 55.62 -41.971588134765625 56.652 -42.291347503662109 57.688 -42.634231567382813 58.724 -42.999275207519531
		 59.756 -43.279247283935547 60.792 -43.532638549804688 61.824 -43.911582946777344
		 62.86 -44.256034851074219 63.896 -44.539131164550781 64.928 -44.952861785888672 65.964 -45.154747009277344
		 67 -44.663421630859375 68.032 -43.848918914794922;
createNode animCurveTA -n "Bip01_L_Forearm_rotateX4";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 66 ".ktv[0:65]"  0.792 1.0946440696716309 1.824 1.1011116504669187
		 2.86 1.1075500249862671 3.896 1.1108649969100952 4.928 1.1062325239181521 5.964 1.0974682569503784
		 7 1.0911285877227783 8.032 1.0833489894866943 9.068 1.0688962936401367 10.1 1.0517426729202273
		 11.136 1.0365539789199829 12.172 1.0215716361999512 13.204 1.0052752494812012 14.24 0.99221777915954579
		 15.276 0.98765695095062256 16.308 0.98800539970397949 17.344 0.98441165685653675
		 18.376 0.97636121511459339 19.412 0.96607077121734619 20.448 0.95094829797744751
		 21.48 0.92707347869873047 22.516 0.88643217086792003 23.552 0.83196598291397084 24.584 0.77852970361709595
		 25.62 0.71827596426010132 26.652 0.63419592380523682 27.688 0.54557567834854126 28.724 0.48209276795387268
		 29.756 0.43938755989074701 30.792 0.4021583497524262 31.824 0.37016895413398743 32.86 0.34382641315460205
		 33.896 0.32119631767272949 34.928 0.31004267930984503 35.964 0.32166975736618042
		 37 0.35463517904281622 38.032 0.39785510301589966 39.068 0.44296002388000488 40.1 0.48461177945137024
		 41.136 0.5205039381980896 42.172 0.55183559656143188 43.204 0.58530992269515991 44.24 0.6200566291809082
		 45.276 0.63737016916275024 46.308 0.63862442970275879 47.344 0.64963746070861816
		 48.376 0.67138290405273438 49.412 0.68681126832962047 50.448 0.70043820142745972
		 51.48 0.72009289264678955 52.516 0.73456650972366333 53.552 0.74364560842514027 54.584 0.75021851062774658
		 55.62 0.75875270366668701 56.652 0.7691129446029662 57.688 0.7803913950920105 58.724 0.79252725839614868
		 59.756 0.80229079723358165 60.792 0.81137579679489136 61.824 0.82445466518402111
		 62.86 0.83696109056472789 63.896 0.84751194715499867 64.928 0.8606986403465271 65.964 0.8658100962638855
		 67 0.84877878427505493 68.032 0.82149809598922741;
createNode animCurveTA -n "Bip01_L_Thigh_rotateX4";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 66 ".ktv[0:65]"  0.792 0.3302122950553894 1.824 0.34418687224388123
		 2.86 0.33932378888130188 3.896 0.32928341627120972 4.928 0.29935351014137268 5.964 0.2451627999544144
		 7 0.16880129277706146 8.032 0.066799245774745941 9.068 -0.10559471696615221 10.1 -0.35656318068504333
		 11.136 -0.65282946825027466 12.172 -1.026659369468689 13.204 -1.5038436651229858
		 14.24 -1.9606466293334959 15.276 -2.3587920665740967 16.308 -2.8060874938964844 17.344 -3.3011395931243896
		 18.376 -3.7191469669342041 19.412 -3.985688447952271 20.448 -4.2015495300292969 21.48 -4.3737525939941406
		 22.516 -4.4951658248901367 23.552 -4.5711994171142578 24.584 -4.6048312187194824
		 25.62 -4.6164155006408691 26.652 -4.636742115020752 27.688 -4.6440639495849609 28.724 -4.650702953338623
		 29.756 -4.6663575172424316 30.792 -4.7072839736938477 31.824 -4.7346596717834473
		 32.86 -4.7393312454223633 33.896 -4.7378835678100586 34.928 -4.7459702491760254 35.964 -4.7552595138549805
		 37 -4.7539482116699219 38.032 -4.7417082786560059 39.068 -4.739959716796875 40.1 -4.7617073059082031
		 41.136 -4.7929072380065918 42.172 -4.7949838638305664 43.204 -4.7899022102355957
		 44.24 -4.8208293914794922 45.276 -4.9117527008056641 46.308 -5.0079765319824219 47.344 -5.0658416748046875
		 48.376 -5.1182427406311035 49.412 -5.1872825622558594 50.448 -5.2510218620300293
		 51.48 -5.301264762878418 52.516 -5.3519449234008789 53.552 -5.4123783111572266 54.584 -5.4717736244201669
		 55.62 -5.5372066497802734 56.652 -5.6305618286132812 57.688 -5.7263484001159668 58.724 -5.7973551750183114
		 59.756 -5.8384180068969727 60.792 -5.8709568977355957 61.824 -5.9109272956848145
		 62.86 -5.9364724159240723 63.896 -5.9509797096252441 64.928 -5.9330573081970215 65.964 -5.903933048248291
		 67 -5.8586902618408203 68.032 -5.792027473449707;
createNode animCurveTA -n "Bip01_L_Thigh_rotateY4";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 66 ".ktv[0:65]"  0.792 -174.02499389648435 1.824 -173.89495849609375
		 2.86 -173.76904296875 3.896 -173.6282958984375 4.928 -173.48759460449222 5.964 -173.34922790527344
		 7 -173.20855712890625 8.032 -173.07907104492187 9.068 -173.02210998535156 10.1 -173.06404113769531
		 11.136 -173.15751647949219 12.172 -173.36355590820312 13.204 -173.6982421875 14.24 -174.02555847167969
		 15.276 -174.29875183105469 16.308 -174.66615295410156 17.344 -175.13287353515625
		 18.376 -175.53834533691406 19.412 -175.80538940429687 20.448 -176.03059387207031
		 21.48 -176.24348449707031 22.516 -176.41896057128906 23.552 -176.57284545898435 24.584 -176.70742797851565
		 25.62 -176.84368896484375 26.652 -176.99308776855469 27.688 -177.13900756835937 28.724 -177.268798828125
		 29.756 -177.40272521972656 30.792 -177.54487609863281 31.824 -177.66313171386719
		 32.86 -177.72615051269531 33.896 -177.74069213867187 34.928 -177.75802612304687 35.964 -177.75028991699219
		 37 -177.69622802734375 38.032 -177.59428405761719 39.068 -177.48472595214844 40.1 -177.38998413085935
		 41.136 -177.29240417480469 42.172 -177.17958068847656 43.204 -177.0645751953125 44.24 -176.98641967773435
		 45.276 -176.96194458007812 46.308 -176.93707275390625 47.344 -176.89817810058594
		 48.376 -176.84030151367187 49.412 -176.80715942382812 50.448 -176.76902770996094
		 51.48 -176.7178955078125 52.516 -176.67231750488281 53.552 -176.635986328125 54.584 -176.59608459472656
		 55.62 -176.56668090820313 56.652 -176.55746459960935 57.688 -176.55654907226565 58.724 -176.51142883300781
		 59.756 -176.44929504394531 60.792 -176.38938903808594 61.824 -176.34016418457031
		 62.86 -176.30908203125 63.896 -176.23939514160156 64.928 -176.19892883300781 65.964 -176.1553955078125
		 67 -176.1280517578125 68.032 -176.10360717773435;
createNode animCurveTA -n "Bip01_L_Thigh_rotateZ4";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 66 ".ktv[0:65]"  0.792 -75.208168029785156 1.824 -75.204612731933594
		 2.86 -75.1649169921875 3.896 -75.103363037109375 4.928 -75.045143127441406 5.964 -74.987442016601562
		 7 -74.865074157714844 8.032 -74.663719177246094 9.068 -74.447120666503906 10.1 -74.263442993164063
		 11.136 -74.098945617675781 12.172 -73.915512084960937 13.204 -73.715225219726562
		 14.24 -73.544441223144531 15.276 -73.401527404785156 16.308 -73.259376525878906 17.344 -73.128013610839844
		 18.376 -73.109931945800781 19.412 -73.212356567382812 20.448 -73.337509155273438
		 21.48 -73.430564880371094 22.516 -73.484825134277344 23.552 -73.61517333984375 24.584 -73.8681640625
		 25.62 -74.167518615722656 26.652 -74.425041198730469 27.688 -74.684234619140625 28.724 -74.938728332519531
		 29.756 -75.13232421875 30.792 -75.356246948242188 31.824 -75.654884338378906 32.86 -75.954734802246094
		 33.896 -76.324050903320341 34.928 -76.861946105957031 35.964 -77.456169128417969
		 37 -77.922698974609375 38.032 -78.253829956054688 39.068 -78.49444580078125 40.1 -78.677467346191406
		 41.136 -78.910537719726563 42.172 -79.344993591308594 43.204 -79.923812866210938
		 44.24 -80.398399353027344 45.276 -80.653274536132813 46.308 -80.826347351074219 47.344 -80.99896240234375
		 48.376 -81.162261962890625 49.412 -81.292282104492188 50.448 -81.412956237792983
		 51.48 -81.554611206054702 52.516 -81.71323394775392 53.552 -81.876564025878906 54.584 -82.063980102539077
		 55.62 -82.269462585449219 56.652 -82.411666870117188 57.688 -82.500595092773438 58.724 -82.596382141113281
		 59.756 -82.697196960449219 60.792 -82.779403686523438 61.824 -82.8355712890625 62.86 -82.866172790527358
		 63.896 -82.913764953613281 64.928 -82.959060668945313 65.964 -82.966934204101563
		 67 -82.956306457519531 68.032 -82.965972900390625;
createNode animCurveTA -n "Bip01_L_Calf_rotateX4";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 66 ".ktv[0:65]"  0.792 0.93464356660842896 1.824 0.93461334705352772
		 2.86 0.93459576368331909 3.896 0.93460780382156383 4.928 0.93458455801010143 5.964 0.93457621335983265
		 7 0.93460118770599376 8.032 0.9346766471862793 9.068 0.93473696708679199 10.1 0.93472480773925781
		 11.136 0.93471914529800415 12.172 0.93474495410919189 13.204 0.93480706214904785
		 14.24 0.9348083734512328 15.276 0.934783935546875 16.308 0.93477666378021251 17.344 0.93477439880371083
		 18.376 0.93473064899444591 19.412 0.9346693754196167 20.448 0.93462765216827393 21.48 0.93461328744888306
		 22.516 0.93457323312759399 23.552 0.93452423810958862 24.584 0.93443906307220459
		 25.62 0.93433576822280895 26.652 0.93423479795455933 27.688 0.93418848514556885 28.724 0.93419617414474476
		 29.756 0.93415188789367676 30.792 0.93409878015518177 31.824 0.93410283327102628
		 32.86 0.93413352966308605 33.896 0.93415296077728271 34.928 0.93412065505981445 35.964 0.93410056829452515
		 37 0.93414157629013062 38.032 0.93418794870376587 39.068 0.93424391746520996 40.1 0.93429446220397949
		 41.136 0.93437492847442638 42.172 0.93445008993148815 43.204 0.93450772762298595
		 44.24 0.93456876277923584 45.276 0.93459749221801791 46.308 0.93465542793273926 47.344 0.93472713232040416
		 48.376 0.93481093645095825 49.412 0.93486952781677235 50.448 0.93494528532028209
		 51.48 0.93502050638198853 52.516 0.93510329723358177 53.552 0.93515700101852417 54.584 0.935200035572052
		 55.62 0.93524366617202759 56.652 0.93530076742172241 57.688 0.93536615371704079 58.724 0.93537473678588867
		 59.756 0.93537396192550692 60.792 0.9353596568107605 61.824 0.93533128499984708 62.86 0.93528807163238536
		 63.896 0.93522149324417136 64.928 0.93515008687973011 65.964 0.93506085872650124
		 67 0.93499690294265747 68.032 0.93493545055389393;
createNode animCurveTA -n "Bip01_L_Calf_rotateY4";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 66 ".ktv[0:65]"  0.792 0.0096561461687088013 1.824 0.010609671473503113
		 2.86 0.010968985967338083 3.896 0.010854168795049194 4.928 0.011458823457360268 5.964 0.011727442033588886
		 7 0.011559261940419674 8.032 0.010072388686239719 9.068 0.0076506342738866806 10.1 0.0054122712463140488
		 11.136 0.0032660092692822218 12.172 -7.4585979746188955e-005 13.204 -0.0048801214434206486
		 14.24 -0.0094395810738205927 15.276 -0.012682213447988032 16.308 -0.015690440312027931
		 17.344 -0.019719682633876801 18.376 -0.023976461961865425 19.412 -0.02682663127779961
		 20.448 -0.028214095160365105 21.48 -0.029801234602928162 22.516 -0.032534841448068619
		 23.552 -0.035172611474990845 24.584 -0.03721984475851059 25.62 -0.040101513266563416
		 26.652 -0.043021943420171738 27.688 -0.044054999947547913 28.724 -0.044323597103357315
		 29.756 -0.045921005308628082 30.792 -0.047394528985023499 31.824 -0.046940192580223077
		 32.86 -0.045393723994493491 33.896 -0.044473931193351746 34.928 -0.044793564826250083
		 35.964 -0.045573621988296509 37 -0.045235961675643921 38.032 -0.043623458594083793
		 39.068 -0.041943985968828201 40.1 -0.040606632828712463 41.136 -0.038913831114768982
		 42.172 -0.037964746356010437 43.204 -0.037212427705526352 44.24 -0.035618048161268234
		 45.276 -0.033813640475273132 46.308 -0.031912889331579215 47.344 -0.029803244397044189
		 48.376 -0.027204465121030807 49.412 -0.024750478565692905 50.448 -0.022307874634861943
		 51.48 -0.019442593678832058 52.516 -0.016480779275298119 53.552 -0.013477282598614693
		 54.584 -0.0099270334467291832 55.62 -0.0061722751706838608 56.652 -0.0029898909851908684
		 57.688 3.8362763007171459e-005 58.724 0.0038256281986832619 59.756 0.0081120841205120087
		 60.792 0.012261070311069489 61.824 0.016161460429430008 62.86 0.019765263423323631
		 63.896 0.023296646773815155 64.928 0.027139196172356609 65.964 0.030712544918060299
		 67 0.033207967877388 68.032 0.035419400781393051;
createNode animCurveTA -n "Bip01_L_Calf_rotateZ4";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 66 ".ktv[0:65]"  0.792 -88.111442565917969 1.824 -88.149085998535156
		 2.86 -88.157501220703125 3.896 -88.157661437988281 4.928 -88.178375244140625 5.964 -88.186866760253906
		 7 -88.196098327636719 8.032 -88.170761108398438 9.068 -88.0694580078125 10.1 -87.925827026367188
		 11.136 -87.796722412109375 12.172 -87.642059326171875 13.204 -87.434310913085938
		 14.24 -87.2232666015625 15.276 -87.051017761230469 16.308 -86.916419982910156 17.344 -86.753463745117188
		 18.376 -86.562507629394531 19.412 -86.410430908203125 20.448 -86.329330444335938
		 21.48 -86.270408630371094 22.516 -86.157318115234375 23.552 -86.044319152832031 24.584 -85.921340942382813
		 25.62 -85.766387939453125 26.652 -85.619400024414063 27.688 -85.566963195800781 28.724 -85.564468383789063
		 29.756 -85.495826721191406 30.792 -85.427024841308594 31.824 -85.436302185058608
		 32.86 -85.4947509765625 33.896 -85.524047851562514 34.928 -85.484710693359375 35.964 -85.46160888671875
		 37 -85.498832702636719 38.032 -85.566787719726563 39.068 -85.648712158203125 40.1 -85.72119140625
		 41.136 -85.825225830078125 42.172 -85.915184020996094 43.204 -85.980392456054688
		 44.24 -86.072799682617188 45.276 -86.142234802246094 46.308 -86.248435974121108 47.344 -86.37091064453125
		 48.376 -86.518356323242188 49.412 -86.652969360351563 50.448 -86.800666809082045
		 51.48 -86.977386474609375 52.516 -87.158073425292983 53.552 -87.336151123046875 54.584 -87.5306396484375
		 55.62 -87.746261596679673 56.652 -87.961273193359375 57.688 -88.169082641601563 58.724 -88.398513793945313
		 59.756 -88.645843505859375 60.792 -88.890045166015625 61.824 -89.119575500488281
		 62.86 -89.330986022949219 63.896 -89.52567291259767 64.928 -89.751083374023438 65.964 -89.956581115722656
		 67 -90.10791015625 68.032 -90.231719970703125;
createNode animCurveTA -n "Bip01_L_Foot_rotateX4";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 66 ".ktv[0:65]"  0.792 -10.356041908264162 1.824 -10.406513214111328
		 2.86 -10.444716453552246 3.896 -10.392083168029783 4.928 -10.333157539367676 5.964 -10.410717010498049
		 7 -10.535895347595217 8.032 -10.536661148071287 9.068 -10.570674896240234 10.1 -10.774553298950195
		 11.136 -10.979465484619141 12.172 -11.013189315795898 13.204 -11.094751358032228
		 14.24 -11.322257995605469 15.276 -11.60603141784668 16.308 -11.754755020141602 17.344 -11.74981212615967
		 18.376 -11.756341934204102 19.412 -11.796988487243652 20.448 -11.823188781738279
		 21.48 -11.82375431060791 22.516 -11.767815589904783 23.552 -11.674807548522947 24.584 -11.56903076171875
		 25.62 -11.496984481811523 26.652 -11.437042236328123 27.688 -11.33200263977051 28.724 -11.17343044281006
		 29.756 -11.080841064453123 30.792 -11.12063694000244 31.824 -11.170721054077148 32.86 -11.124490737915041
		 33.896 -11.038925170898436 34.928 -11.005878448486328 35.964 -10.985596656799318
		 37 -10.929266929626465 38.032 -10.804999351501465 39.068 -10.64414119720459 40.1 -10.602719306945801
		 41.136 -10.652618408203123 42.172 -10.567076683044434 43.204 -10.250189781188965
		 44.24 -10.107611656188965 45.276 -10.279282569885254 46.308 -10.370617866516112 47.344 -10.308480262756348
		 48.376 -10.285041809082031 49.412 -10.404745101928713 50.448 -10.43074893951416 51.48 -10.308767318725586
		 52.516 -10.193170547485352 53.552 -10.09639835357666 54.584 -9.9132785797119141 55.62 -9.7133493423461914
		 56.652 -9.6171455383300781 57.688 -9.7093763351440447 58.724 -9.7425851821899414
		 59.756 -9.6789655685424805 60.792 -9.5730905532836914 61.824 -9.4889774322509766
		 62.86 -9.4603414535522461 63.896 -9.4658508300781232 64.928 -9.4253997802734375 65.964 -9.3692951202392578
		 67 -9.2872619628906232 68.032 -9.2186870574951172;
createNode animCurveTA -n "Bip01_L_Foot_rotateY4";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 66 ".ktv[0:65]"  0.792 2.1367208957672119 1.824 2.1724135875701904
		 2.86 2.2041614055633545 3.896 2.1741726398468018 4.928 2.1721112728118901 5.964 2.1910369396209717
		 7 2.2759771347045894 8.032 2.3056976795196533 9.068 2.3303091526031494 10.1 2.4366552829742432
		 11.136 2.5359139442443848 12.172 2.5096509456634521 13.204 2.5008053779602051 14.24 2.6115953922271729
		 15.276 2.7528460025787354 16.308 2.7925822734832764 17.344 2.7178635597229004 18.376 2.6696333885192871
		 19.412 2.6081173419952393 20.448 2.6281089782714844 21.48 2.5699450969696045 22.516 2.5603141784667969
		 23.552 2.4635910987854004 24.584 2.3980934619903564 25.62 2.3076953887939453 26.652 2.2817513942718506
		 27.688 2.1732935905456543 28.724 2.0797553062438965 29.756 2.0261330604553223 30.792 2.0500795841217041
		 31.824 2.0530998706817627 32.86 2.0103585720062256 33.896 1.9334264993667603 34.928 1.9075291156768803
		 35.964 1.8892394304275513 37 1.8725831508636477 38.032 1.777922511100769 39.068 1.6580208539962769
		 40.1 1.6296777725219729 41.136 1.6922907829284668 42.172 1.6274603605270388 43.204 1.4555728435516355
		 44.24 1.3349987268447876 45.276 1.4250336885452273 46.308 1.495574951171875 47.344 1.4451315402984619
		 48.376 1.4214755296707151 49.412 1.4860424995422363 50.448 1.499436616897583 51.48 1.4318064451217651
		 52.516 1.3479037284851074 53.552 1.2784581184387207 54.584 1.1498863697052002 55.62 1.0066887140274048
		 56.652 0.95965808629989635 57.688 1.0101331472396853 58.724 1.0484534502029419 59.756 0.99942779541015647
		 60.792 0.90992146730422996 61.824 0.87437641620635986 62.86 0.86036282777786277 63.896 0.85688668489456177
		 64.928 0.84087264537811279 65.964 0.78979653120040894 67 0.73714995384216309 68.032 0.70570051670074463;
createNode animCurveTA -n "Bip01_L_Foot_rotateZ4";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 66 ".ktv[0:65]"  0.792 5.8128595352172852 1.824 5.7779808044433594
		 2.86 5.7893428802490243 3.896 5.8866167068481454 4.928 5.8488860130310059 5.964 5.7930159568786621
		 7 6.1727728843688965 8.032 6.8432154655456543 9.068 7.0836610794067383 10.1 6.8083362579345703
		 11.136 6.5297126770019531 12.172 6.4553704261779785 13.204 6.4778656959533691 14.24 6.4867959022521973
		 15.276 6.3661508560180664 16.308 6.189124584197998 17.344 6.0728769302368164 18.376 5.9085440635681152
		 19.412 5.5823864936828613 20.448 5.2603812217712402 21.48 5.2181820869445801 22.516 5.3632822036743164
		 23.552 5.3353548049926758 24.584 5.0955162048339844 25.62 4.9103856086730957 26.652 4.814237117767334
		 27.688 4.6515717506408691 28.724 4.4978866577148437 29.756 4.5290031433105469 30.792 4.5459442138671875
		 31.824 4.3173995018005371 32.86 3.9554400444030762 33.896 3.6029660701751713 34.928 3.3885765075683594
		 35.964 3.5025999546051025 37 3.6624941825866695 38.032 3.5062432289123535 39.068 3.2523171901702881
		 40.1 3.3045647144317627 41.136 3.6581175327301025 42.172 4.0211005210876465 43.204 4.1531634330749512
		 44.24 4.0354123115539551 45.276 3.886760711669921 46.308 3.8921997547149658 47.344 3.9793946743011479
		 48.376 4.0620889663696289 49.412 4.1949186325073242 50.448 4.3777003288269043 51.48 4.5299577713012695
		 52.516 4.661931037902832 53.552 4.7218594551086426 54.584 4.5962519645690918 55.62 4.5627307891845703
		 56.652 4.9541482925415039 57.688 5.4879970550537109 58.724 5.7848944664001465 59.756 5.9064464569091797
		 60.792 6.0266580581665048 61.824 6.255063533782959 62.86 6.4961357116699219 63.896 6.5935430526733398
		 64.928 6.6823697090148926 65.964 6.8986377716064453 67 7.1033334732055664 68.032 7.1710715293884277;
createNode animCurveTA -n "Bip01_R_Thigh_rotateX4";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 66 ".ktv[0:65]"  0.792 14.376577377319336 1.824 14.36133289337158
		 2.86 14.340350151062012 3.896 14.288030624389648 4.928 14.203691482543944 5.964 14.110692024230955
		 7 14.037568092346191 8.032 13.952054023742676 9.068 13.797336578369141 10.1 13.581693649291992
		 11.136 13.318158149719238 12.172 12.959026336669922 13.204 12.505638122558594 14.24 12.100922584533691
		 15.276 11.773537635803226 16.308 11.382632255554199 17.344 10.900749206542969 18.376 10.483015060424805
		 19.412 10.208099365234377 20.448 9.9983167648315447 21.48 9.8321657180786133 22.516 9.7419662475585937
		 23.552 9.7038402557373047 24.584 9.7075557708740234 25.62 9.7240581512451172 26.652 9.7326231002807617
		 27.688 9.7508735656738281 28.724 9.7830295562744141 29.756 9.8101587295532227 30.792 9.8101043701171875
		 31.824 9.7917995452880859 32.86 9.7867450714111328 33.896 9.7821693420410156 34.928 9.7632627487182617
		 35.964 9.7703666687011719 37 9.8309774398803711 38.032 9.8831605911254883 39.068 9.9019708633422852
		 40.1 9.917597770690918 41.136 9.9496860504150391 42.172 9.9619398117065447 43.204 9.9506425857543945
		 44.24 9.9607372283935547 45.276 9.9843149185180664 46.308 9.9914054870605469 47.344 10.018370628356934
		 48.376 10.057781219482422 49.412 10.109512329101562 50.448 10.153239250183104 51.48 10.190464019775392
		 52.516 10.228754043579102 53.552 10.26151180267334 54.584 10.289310455322266 55.62 10.325114250183104
		 56.652 10.367666244506836 57.688 10.423441886901855 58.724 10.517807960510254 59.756 10.633374214172363
		 60.792 10.726921081542969 61.824 10.837801933288574 62.86 10.979364395141602 63.896 11.174311637878418
		 64.928 11.398983955383301 65.964 11.603589057922363 67 11.804287910461426 68.032 12.023482322692873;
createNode animCurveTA -n "Bip01_R_Thigh_rotateY4";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 66 ".ktv[0:65]"  0.792 174.38320922851562 1.824 174.5316162109375
		 2.86 174.69003295898435 3.896 174.83782958984375 4.928 174.94497680664065 5.964 175.055419921875
		 7 175.20381164550781 8.032 175.33267211914062 9.068 175.35955810546875 10.1 175.29995727539062
		 11.136 175.18669128417969 12.172 174.92216491699219 13.204 174.51954650878906 14.24 174.15570068359375
		 15.276 173.862060546875 16.308 173.45841979980469 17.344 172.91561889648435 18.376 172.42720031738284
		 19.412 172.09989929199219 20.448 171.81655883789065 21.48 171.53221130371094 22.516 171.30436706542969
		 23.552 171.13496398925781 24.584 170.9970703125 25.62 170.85935974121094 26.652 170.70431518554688
		 27.688 170.55229187011719 28.724 170.41523742675781 29.756 170.281494140625 30.792 170.12269592285159
		 31.824 169.97526550292969 32.86 169.86358642578125 33.896 169.81304931640625 34.928 169.80563354492187
		 35.964 169.8656005859375 37 169.95539855957031 38.032 170.03463745117187 39.068 170.06715393066406
		 40.1 170.10830688476562 41.136 170.18885803222656 42.172 170.2991943359375 43.204 170.38755798339844
		 44.24 170.41310119628906 45.276 170.37890625 46.308 170.31588745117187 47.344 170.25511169433597
		 48.376 170.208984375 49.412 170.14283752441409 50.448 170.05403137207031 51.48 169.95265197753906
		 52.516 169.84107971191406 53.552 169.70635986328125 54.584 169.55622863769531 55.62 169.39837646484375
		 56.652 169.23268127441406 57.688 169.06813049316406 58.724 168.92887878417969 59.756 168.7794189453125
		 60.792 168.58338928222656 61.824 168.37545776367187 62.86 168.19464111328125 63.896 168.05049133300784
		 64.928 167.90180969238284 65.964 167.7293701171875 67 167.54496765136719 68.032 167.36691284179687;
createNode animCurveTA -n "Bip01_R_Thigh_rotateZ4";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 66 ".ktv[0:65]"  0.792 -69.188285827636719 1.824 -69.157928466796875
		 2.86 -69.169242858886719 3.896 -69.174102783203125 4.928 -69.140304565429688 5.964 -69.074577331542969
		 7 -69.002037048339844 8.032 -68.947013854980469 9.068 -68.862205505371094 10.1 -68.804946899414062
		 11.136 -68.826667785644531 12.172 -68.974494934082031 13.204 -69.187980651855469
		 14.24 -69.364990234375 15.276 -69.485176086425781 16.308 -69.647514343261719 17.344 -69.944465637207031
		 18.376 -70.312507629394545 19.412 -70.622184753417969 20.448 -70.846298217773438
		 21.48 -71.001396179199219 22.516 -71.121391296386719 23.552 -71.26239013671875 24.584 -71.477516174316406
		 25.62 -71.729034423828125 26.652 -71.941970825195313 27.688 -72.132560729980469 28.724 -72.333473205566406
		 29.756 -72.517532348632812 30.792 -72.740386962890625 31.824 -72.944313049316406
		 32.86 -73.138725280761719 33.896 -73.478050231933594 34.928 -74.05621337890625 35.964 -74.64483642578125
		 37 -75.032875061035156 38.032 -75.272857666015625 39.068 -75.460578918457017 40.1 -75.6429443359375
		 41.136 -75.908393859863281 42.172 -76.392410278320327 43.204 -77.003631591796875
		 44.24 -77.472427368164062 45.276 -77.702362060546875 46.308 -77.854888916015639 47.344 -78.00042724609375
		 48.376 -78.108482360839844 49.412 -78.202957153320327 50.448 -78.321640014648438
		 51.48 -78.479225158691406 52.516 -78.677680969238281 53.552 -78.865837097167969 54.584 -79.020530700683594
		 55.62 -79.177490234375 56.652 -79.347747802734375 57.688 -79.507904052734375 58.724 -79.648773193359375
		 59.756 -79.725814819335938 60.792 -79.725982666015625 61.824 -79.725486755371094
		 62.86 -79.742347717285156 63.896 -79.722457885742188 64.928 -79.677619934082031 65.964 -79.606918334960937
		 67 -79.543418884277344 68.032 -79.461624145507813;
createNode animCurveTA -n "Bip01_R_Calf_rotateX4";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 66 ".ktv[0:65]"  0.792 -0.93100500106811523 1.824 -0.93100404739379883
		 2.86 -0.93101537227630615 3.896 -0.93101418018341053 4.928 -0.93093544244766246 5.964 -0.93094235658645652
		 7 -0.930933177471161 8.032 -0.93094003200530995 9.068 -0.93092167377471924 10.1 -0.93088269233703613
		 11.136 -0.93096232414245605 12.172 -0.93113988637924161 13.204 -0.93134951591491666
		 14.24 -0.93149733543395996 15.276 -0.93164253234863281 16.308 -0.9318392276763916
		 17.344 -0.93207800388336193 18.376 -0.93230527639389038 19.412 -0.93252408504486084
		 20.448 -0.9327220320701598 21.48 -0.93286246061325073 22.516 -0.93294882774353016
		 23.552 -0.9329628944396976 24.584 -0.93298810720443726 25.62 -0.93296855688095093
		 26.652 -0.93288105726242032 27.688 -0.93284755945205711 28.724 -0.9328519105911256
		 29.756 -0.93281167745590199 30.792 -0.93287527561187744 31.824 -0.93297755718231201
		 32.86 -0.9330754280090332 33.896 -0.93315041065216064 34.928 -0.93314725160598766
		 35.964 -0.93308597803115845 37 -0.9330260753631594 38.032 -0.93305778503417947 39.068 -0.9331365227699282
		 40.1 -0.93321436643600464 41.136 -0.9333130717277528 42.172 -0.93340963125228882
		 43.204 -0.93346911668777455 44.24 -0.93355095386505116 45.276 -0.9336150884628297
		 46.308 -0.933718502521515 47.344 -0.93386334180831909 48.376 -0.93401455879211392
		 49.412 -0.93415135145187378 50.448 -0.93433463573455777 51.48 -0.93454706668853771
		 52.516 -0.93473738431930542 53.552 -0.93491846323013295 54.584 -0.93508648872375488
		 55.62 -0.93523162603378318 56.652 -0.93535232543945301 57.688 -0.93548077344894409
		 58.724 -0.93558144569396973 59.756 -0.93568325042724609 60.792 -0.93577033281326305
		 61.824 -0.93585443496704079 62.86 -0.93591439723968517 63.896 -0.93591302633285522
		 64.928 -0.93592828512191772 65.964 -0.93593466281890858 67 -0.93596339225769043 68.032 -0.93596625328063954;
createNode animCurveTA -n "Bip01_R_Calf_rotateY4";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 66 ".ktv[0:65]"  0.792 0.10671231150627136 1.824 0.10704530030488968
		 2.86 0.10680826753377914 3.896 0.10630078613758089 4.928 0.10710384696722032 5.964 0.10751035064458848
		 7 0.10744027048349382 8.032 0.10730161517858504 9.068 0.1076049506664276 10.1 0.10816819965839386
		 11.136 0.10711203515529633 12.172 0.10501953959465028 13.204 0.10246577858924866
		 14.24 0.10039286315441132 15.276 0.098657287657260895 16.308 0.095990896224975586
		 17.344 0.092277042567729964 18.376 0.088739685714244843 19.412 0.085777752101421356
		 20.448 0.083124615252017975 21.48 0.081229723989963531 22.516 0.080021195113658919
		 23.552 0.079525604844093323 24.584 0.079011045396327972 25.62 0.079368531703948975
		 26.652 0.080491051077842726 27.688 0.080850526690483093 28.724 0.080552570521831526
		 29.756 0.080573193728923798 30.792 0.079975023865699782 31.824 0.078805029392242432
		 32.86 0.077322207391262054 33.896 0.075797468423843398 34.928 0.07533467561006546
		 35.964 0.076063863933086395 37 0.0770096555352211 38.032 0.076945319771766663 39.068 0.076174058020114899
		 40.1 0.075063548982143402 41.136 0.073640570044517517 42.172 0.072430774569511414
		 43.204 0.071332328021526337 44.24 0.06992967426776886 45.276 0.068532302975654602
		 46.308 0.067033417522907257 47.344 0.065297119319438934 48.376 0.063260443508625031
		 49.412 0.061354760080575943 50.448 0.05840018019080162 51.48 0.054485514760017395
		 52.516 0.050357956439256675 53.552 0.046472087502479553 54.584 0.042851902544498451
		 55.62 0.039306089282035821 56.652 0.035672031342983253 57.688 0.03155391663312912
		 58.724 0.02697373740375042 59.756 0.023152707144618031 60.792 0.020285623148083687
		 61.824 0.017159666866064072 62.86 0.013813796453177927 63.896 0.01095094718039036
		 64.928 0.0083741731941699982 65.964 0.0059273876249790192 67 0.0039038248360157017
		 68.032 0.0022614726331084971;
createNode animCurveTA -n "Bip01_R_Calf_rotateZ4";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 66 ".ktv[0:65]"  0.792 -83.204017639160156 1.824 -83.217041015625
		 2.86 -83.2177734375 3.896 -83.196746826171875 4.928 -83.157478332519531 5.964 -83.1741943359375
		 7 -83.170135498046875 8.032 -83.164093017578125 9.068 -83.160972595214844 10.1 -83.158706665039063
		 11.136 -83.184646606445312 12.172 -83.268257141113281 13.204 -83.364364624023438
		 14.24 -83.440109252929702 15.276 -83.519020080566406 16.308 -83.6217041015625 17.344 -83.744537353515625
		 18.376 -83.872856140136719 19.412 -84.012321472167969 20.448 -84.144821166992188
		 21.48 -84.241867065429702 22.516 -84.296226501464844 23.552 -84.303924560546875 24.584 -84.322525024414063
		 25.62 -84.303329467773452 26.652 -84.246688842773438 27.688 -84.223228454589844 28.724 -84.216766357421875
		 29.756 -84.195686340332031 30.792 -84.237838745117188 31.824 -84.300865173339844
		 32.86 -84.380195617675781 33.896 -84.433021545410156 34.928 -84.427833557128906 35.964 -84.375740051269531
		 37 -84.337799072265625 38.032 -84.36260986328125 39.068 -84.420303344726562 40.1 -84.488044738769531
		 41.136 -84.558494567871094 42.172 -84.633636474609375 43.204 -84.683372497558594
		 44.24 -84.741828918457017 45.276 -84.799942016601563 46.308 -84.893913269042983 47.344 -85.014488220214844
		 48.376 -85.147293090820327 49.412 -85.271682739257813 50.448 -85.442985534667969
		 51.48 -85.655685424804688 52.516 -85.873916625976563 53.552 -86.085899353027344 54.584 -86.287101745605483
		 55.62 -86.48368072509767 56.652 -86.689781188964844 57.688 -86.907005310058594 58.724 -87.126350402832031
		 59.756 -87.343688964843764 60.792 -87.5291748046875 61.824 -87.719108581542983 62.86 -87.90570068359375
		 63.896 -88.029838562011719 64.928 -88.163543701171875 65.964 -88.2969970703125 67 -88.416580200195312
		 68.032 -88.508140563964844;
createNode animCurveTA -n "Bip01_R_Foot_rotateX4";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 66 ".ktv[0:65]"  0.792 6.6876740455627441 1.824 6.737424373626709
		 2.86 6.876988410949707 3.896 6.9156341552734375 4.928 6.6885890960693359 5.964 6.4657688140869141
		 7 6.5330061912536621 8.032 6.6697359085083008 9.068 6.6079049110412598 10.1 6.5197005271911621
		 11.136 6.4694557189941406 12.172 6.3529148101806641 13.204 6.1775579452514648 14.24 6.0258617401123047
		 15.276 5.9639167785644531 16.308 5.8166661262512207 17.344 5.5368995666503906 18.376 5.2985825538635254
		 19.412 5.0993561744689941 20.448 4.860990047454834 21.48 4.5520644187927246 22.516 4.4571380615234375
		 23.552 4.6132078170776367 24.584 4.7848029136657715 25.62 4.8577094078063965 26.652 4.8812484741210938
		 27.688 4.9705080986022949 28.724 5.1020107269287109 29.756 5.169684886932373 30.792 5.1032142639160156
		 31.824 4.9919195175170898 32.86 4.8035082817077637 33.896 4.6935038566589355 34.928 4.756810188293457
		 35.964 4.9928092956542969 37 5.3128528594970703 38.032 5.4531712532043457 39.068 5.3363704681396484
		 40.1 5.285893440246582 41.136 5.4662413597106934 42.172 5.6918888092041025 43.204 5.8810644149780273
		 44.24 6.047217845916748 45.276 6.1267428398132324 46.308 6.1287307739257813 47.344 6.1485085487365723
		 48.376 6.2349233627319336 49.412 6.308009147644043 50.448 6.3814349174499512 51.48 6.4237112998962402
		 52.516 6.465050220489502 53.552 6.448481559753418 54.584 6.5338788032531738 55.62 6.6531186103820801
		 56.652 6.7521243095397958 57.688 6.8990349769592294 58.724 7.1749997138977051 59.756 7.368791103363038
		 60.792 7.3061351776123038 61.824 7.2886824607849121 62.86 7.4580717086791983 63.896 7.8866381645202628
		 64.928 8.3100662231445313 65.964 8.5977306365966797 67 8.7063627243041992 68.032 8.7658586502075195;
createNode animCurveTA -n "Bip01_R_Foot_rotateY4";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 66 ".ktv[0:65]"  0.792 1.4297463893890381 1.824 1.3831478357315063
		 2.86 1.2873779535293579 3.896 1.2643790245056152 4.928 1.4041455984115605 5.964 1.5423526763916016
		 7 1.4930180311203003 8.032 1.4289944171905518 9.068 1.4509453773498535 10.1 1.4670411348342896
		 11.136 1.4692723751068115 12.172 1.5091344118118286 13.204 1.5722546577453611 14.24 1.6198658943176272
		 15.276 1.6575952768325806 16.308 1.7040145397186279 17.344 1.8259449005126953 18.376 1.9193572998046875
		 19.412 2.0231120586395264 20.448 2.1783711910247803 21.48 2.3445854187011719 22.516 2.3758661746978764
		 23.552 2.2989118099212646 24.584 2.1822726726531982 25.62 2.123746395111084 26.652 2.1289117336273193
		 27.688 2.0769457817077637 28.724 1.9913756847381592 29.756 1.9617630243301394 30.792 2.003474235534668
		 31.824 2.067068338394165 32.86 2.1955385208129883 33.896 2.2609753608703618 34.928 2.2285385131835937
		 35.964 2.0751886367797852 37 1.9041488170623779 38.032 1.8426544666290283 39.068 1.9218249320983891
		 40.1 1.9704955816268923 41.136 1.8757591247558592 42.172 1.7192168235778809 43.204 1.629892110824585
		 44.24 1.5250322818756104 45.276 1.4727927446365356 46.308 1.4877098798751831 47.344 1.488462567329407
		 48.376 1.4569245576858521 49.412 1.3997974395751951 50.448 1.3700121641159058 51.48 1.3470594882965088
		 52.516 1.3437263965606687 53.552 1.3280413150787354 54.584 1.3094974756240847 55.62 1.2362492084503174
		 56.652 1.1806962490081787 57.688 1.0959904193878174 58.724 0.94545328617095947 59.756 0.77705115079879761
		 60.792 0.84241420030593872 61.824 0.88245713710784912 62.86 0.76520800590515137 63.896 0.48433631658554083
		 64.928 0.16724984347820282 65.964 0.0034236132632941008 67 -0.040239870548248291
		 68.032 -0.085272647440433502;
createNode animCurveTA -n "Bip01_R_Foot_rotateZ4";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 66 ".ktv[0:65]"  0.792 5.4642257690429687 1.824 5.6878390312194824
		 2.86 5.4809970855712891 3.896 5.0493731498718262 4.928 4.8670763969421387 5.964 4.9894614219665527
		 7 5.0224704742431641 8.032 5.0025520324707031 9.068 5.234847068786622 10.1 5.5269203186035156
		 11.136 5.6013894081115723 12.172 5.5670256614685059 13.204 5.5308132171630859 14.24 5.583702564239502
		 15.276 5.7699708938598642 16.308 5.8144059181213379 17.344 5.4501705169677734 18.376 5.0576744079589844
		 19.412 4.9821181297302246 20.448 5.0233569145202637 21.48 5.0394940376281738 22.516 5.0497136116027832
		 23.552 5.0882167816162109 24.584 5.2246675491333008 25.62 5.3934063911437988 26.652 5.5538568496704102
		 27.688 5.6514892578125 28.724 5.6231417655944824 29.756 5.5818371772766113 30.792 5.6348147392272949
		 31.824 5.7141585350036621 32.86 5.5975627899169931 33.896 5.2225885391235352 34.928 4.8683319091796875
		 35.964 4.9438405036926278 37 5.3663592338562012 38.032 5.6414661407470703 39.068 5.6573500633239746
		 40.1 5.6721844673156738 41.136 5.795454978942872 42.172 5.9644308090209961 43.204 5.9975428581237793
		 44.24 5.9009337425231934 45.276 5.9063210487365723 46.308 6.0910720825195312 47.344 6.3987822532653809
		 48.376 6.7423186302185059 49.412 7.0598678588867188 50.448 7.3196878433227548 51.48 7.4614782333374015
		 52.516 7.4929656982421884 53.552 7.5530533790588388 54.584 7.7822480201721183 55.62 8.1160717010498047
		 56.652 8.4025869369506836 57.688 8.5649232864379883 58.724 8.6744651794433594 59.756 8.8993387222290039
		 60.792 9.2766866683959961 61.824 9.6526336669921875 62.86 9.8965921401977539 63.896 10.072977066040041
		 64.928 10.309902191162108 65.964 10.586875915527344 67 10.779726028442386 68.032 10.890285491943359;
select -ne :time1;
	setAttr ".o" 24.2;
	setAttr ".unw" 24.2;
select -ne :renderPartition;
	setAttr -s 126 ".st";
select -ne :initialShadingGroup;
	setAttr ".ro" yes;
select -ne :initialParticleSE;
	setAttr ".ro" yes;
select -ne :defaultShaderList1;
	setAttr -s 64 ".s";
select -ne :defaultTextureList1;
	setAttr -s 31 ".tx";
select -ne :postProcessList1;
	setAttr -s 2 ".p";
select -ne :defaultRenderUtilityList1;
	setAttr -s 31 ".u";
select -ne :defaultRenderingList1;
select -ne :renderGlobalsList1;
select -ne :defaultResolution;
	setAttr ".pa" 1;
select -ne :hardwareRenderGlobals;
	setAttr ".ctrs" 256;
	setAttr ".btrs" 512;
select -ne :hardwareRenderingGlobals;
	setAttr ".otfna" -type "stringArray" 18 "NURBS Curves" "NURBS Surfaces" "Polygons" "Subdiv Surfaces" "Particles" "Fluids" "Image Planes" "UI:" "Lights" "Cameras" "Locators" "Joints" "IK Handles" "Deformers" "Motion Trails" "Components" "Misc. UI" "Ornaments"  ;
	setAttr ".otfva" -type "Int32Array" 18 0 1 1 1 1 1
		 1 0 0 0 0 0 0 0 0 0 0 0 ;
select -ne :defaultHardwareRenderGlobals;
	setAttr ".fn" -type "string" "im";
	setAttr ".res" -type "string" "ntsc_4d 646 485 1.333";
select -ne :hyperGraphLayout;
	setAttr -s 4 ".hyp";
	setAttr ".hyp[1].isc" yes;
	setAttr ".hyp[2].isc" yes;
	setAttr ".hyp[3].isc" yes;
	setAttr ".hyp[4].isc" yes;
connectAttr "Bip01_Spine_translateX4.o" "|Sit_Idle_2|Bip01_Spine.tx";
connectAttr "Bip01_Spine_translateY4.o" "|Sit_Idle_2|Bip01_Spine.ty";
connectAttr "Bip01_Spine_translateZ4.o" "|Sit_Idle_2|Bip01_Spine.tz";
connectAttr "Bip01_Spine_rotateX4.o" "|Sit_Idle_2|Bip01_Spine.rx";
connectAttr "Bip01_Spine_rotateY4.o" "|Sit_Idle_2|Bip01_Spine.ry";
connectAttr "Bip01_Spine_rotateZ4.o" "|Sit_Idle_2|Bip01_Spine.rz";
connectAttr "Bip01_Spine1_rotateX4.o" "|Sit_Idle_2|Bip01_Spine|Bip01_Spine1.rx";
connectAttr "Bip01_Spine1_rotateY4.o" "|Sit_Idle_2|Bip01_Spine|Bip01_Spine1.ry";
connectAttr "Bip01_Spine1_rotateZ4.o" "|Sit_Idle_2|Bip01_Spine|Bip01_Spine1.rz";
connectAttr "|Sit_Idle_2|Bip01_Spine.s" "|Sit_Idle_2|Bip01_Spine|Bip01_Spine1.is"
		;
connectAttr "Bip01_Spine2_rotateX4.o" "|Sit_Idle_2|Bip01_Spine|Bip01_Spine1|Bip01_Spine2.rx"
		;
connectAttr "Bip01_Spine2_rotateY4.o" "|Sit_Idle_2|Bip01_Spine|Bip01_Spine1|Bip01_Spine2.ry"
		;
connectAttr "Bip01_Spine2_rotateZ4.o" "|Sit_Idle_2|Bip01_Spine|Bip01_Spine1|Bip01_Spine2.rz"
		;
connectAttr "|Sit_Idle_2|Bip01_Spine|Bip01_Spine1.s" "|Sit_Idle_2|Bip01_Spine|Bip01_Spine1|Bip01_Spine2.is"
		;
connectAttr "Bip01_Neck_rotateX3.o" "|Sit_Idle_2|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck.rx"
		;
connectAttr "Bip01_Neck_rotateY3.o" "|Sit_Idle_2|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck.ry"
		;
connectAttr "Bip01_Neck_rotateZ3.o" "|Sit_Idle_2|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck.rz"
		;
connectAttr "|Sit_Idle_2|Bip01_Spine|Bip01_Spine1|Bip01_Spine2.s" "|Sit_Idle_2|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck.is"
		;
connectAttr "Bip01_Head_rotateX3.o" "|Sit_Idle_2|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_Head.rx"
		;
connectAttr "Bip01_Head_rotateY3.o" "|Sit_Idle_2|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_Head.ry"
		;
connectAttr "Bip01_Head_rotateZ3.o" "|Sit_Idle_2|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_Head.rz"
		;
connectAttr "|Sit_Idle_2|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck.s" "|Sit_Idle_2|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_Head.is"
		;
connectAttr "|Sit_Idle_2|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_Head.s" "|Sit_Idle_2|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_Head|_notused_Bip01_HeadNub.is"
		;
connectAttr "Bip01_R_Clavicle_rotateX4.o" "|Sit_Idle_2|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_R_Clavicle.rx"
		;
connectAttr "Bip01_R_Clavicle_rotateY4.o" "|Sit_Idle_2|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_R_Clavicle.ry"
		;
connectAttr "Bip01_R_Clavicle_rotateZ4.o" "|Sit_Idle_2|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_R_Clavicle.rz"
		;
connectAttr "|Sit_Idle_2|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck.s" "|Sit_Idle_2|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_R_Clavicle.is"
		;
connectAttr "Bip01_R_UpperArm_rotateX4.o" "|Sit_Idle_2|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_R_Clavicle|Bip01_R_UpperArm.rx"
		;
connectAttr "Bip01_R_UpperArm_rotateY4.o" "|Sit_Idle_2|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_R_Clavicle|Bip01_R_UpperArm.ry"
		;
connectAttr "Bip01_R_UpperArm_rotateZ4.o" "|Sit_Idle_2|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_R_Clavicle|Bip01_R_UpperArm.rz"
		;
connectAttr "|Sit_Idle_2|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_R_Clavicle.s" "|Sit_Idle_2|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_R_Clavicle|Bip01_R_UpperArm.is"
		;
connectAttr "Bip01_R_Forearm_rotateY4.o" "|Sit_Idle_2|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_R_Clavicle|Bip01_R_UpperArm|Bip01_R_Forearm.ry"
		;
connectAttr "Bip01_R_Forearm_rotateX4.o" "|Sit_Idle_2|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_R_Clavicle|Bip01_R_UpperArm|Bip01_R_Forearm.rx"
		;
connectAttr "Bip01_R_Forearm_rotateZ4.o" "|Sit_Idle_2|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_R_Clavicle|Bip01_R_UpperArm|Bip01_R_Forearm.rz"
		;
connectAttr "|Sit_Idle_2|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_R_Clavicle|Bip01_R_UpperArm.s" "|Sit_Idle_2|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_R_Clavicle|Bip01_R_UpperArm|Bip01_R_Forearm.is"
		;
connectAttr "|Sit_Idle_2|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_R_Clavicle|Bip01_R_UpperArm|Bip01_R_Forearm.s" "|Sit_Idle_2|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_R_Clavicle|Bip01_R_UpperArm|Bip01_R_Forearm|Bip01_R_Hand.is"
		;
connectAttr "|Sit_Idle_2|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_R_Clavicle|Bip01_R_UpperArm|Bip01_R_Forearm|Bip01_R_Hand.s" "|Sit_Idle_2|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_R_Clavicle|Bip01_R_UpperArm|Bip01_R_Forearm|Bip01_R_Hand|Bip01_R_Finger0.is"
		;
connectAttr "|Sit_Idle_2|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_R_Clavicle|Bip01_R_UpperArm|Bip01_R_Forearm|Bip01_R_Hand|Bip01_R_Finger0.s" "|Sit_Idle_2|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_R_Clavicle|Bip01_R_UpperArm|Bip01_R_Forearm|Bip01_R_Hand|Bip01_R_Finger0|Bip01_R_Finger01.is"
		;
connectAttr "|Sit_Idle_2|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_R_Clavicle|Bip01_R_UpperArm|Bip01_R_Forearm|Bip01_R_Hand|Bip01_R_Finger0|Bip01_R_Finger01.s" "|Sit_Idle_2|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_R_Clavicle|Bip01_R_UpperArm|Bip01_R_Forearm|Bip01_R_Hand|Bip01_R_Finger0|Bip01_R_Finger01|_notused_Bip01_R_Finger0Nub.is"
		;
connectAttr "|Sit_Idle_2|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_R_Clavicle|Bip01_R_UpperArm|Bip01_R_Forearm|Bip01_R_Hand.s" "|Sit_Idle_2|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_R_Clavicle|Bip01_R_UpperArm|Bip01_R_Forearm|Bip01_R_Hand|Bip01_R_Finger2.is"
		;
connectAttr "|Sit_Idle_2|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_R_Clavicle|Bip01_R_UpperArm|Bip01_R_Forearm|Bip01_R_Hand|Bip01_R_Finger2.s" "|Sit_Idle_2|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_R_Clavicle|Bip01_R_UpperArm|Bip01_R_Forearm|Bip01_R_Hand|Bip01_R_Finger2|Bip01_R_Finger21.is"
		;
connectAttr "|Sit_Idle_2|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_R_Clavicle|Bip01_R_UpperArm|Bip01_R_Forearm|Bip01_R_Hand|Bip01_R_Finger2|Bip01_R_Finger21.s" "|Sit_Idle_2|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_R_Clavicle|Bip01_R_UpperArm|Bip01_R_Forearm|Bip01_R_Hand|Bip01_R_Finger2|Bip01_R_Finger21|_notused_Bip01_R_Finger2Nub.is"
		;
connectAttr "|Sit_Idle_2|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_R_Clavicle|Bip01_R_UpperArm|Bip01_R_Forearm|Bip01_R_Hand.s" "|Sit_Idle_2|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_R_Clavicle|Bip01_R_UpperArm|Bip01_R_Forearm|Bip01_R_Hand|Bip01_R_Finger1.is"
		;
connectAttr "|Sit_Idle_2|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_R_Clavicle|Bip01_R_UpperArm|Bip01_R_Forearm|Bip01_R_Hand|Bip01_R_Finger1.s" "|Sit_Idle_2|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_R_Clavicle|Bip01_R_UpperArm|Bip01_R_Forearm|Bip01_R_Hand|Bip01_R_Finger1|Bip01_R_Finger11.is"
		;
connectAttr "|Sit_Idle_2|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_R_Clavicle|Bip01_R_UpperArm|Bip01_R_Forearm|Bip01_R_Hand|Bip01_R_Finger1|Bip01_R_Finger11.s" "|Sit_Idle_2|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_R_Clavicle|Bip01_R_UpperArm|Bip01_R_Forearm|Bip01_R_Hand|Bip01_R_Finger1|Bip01_R_Finger11|_notused_Bip01_R_Finger1Nub.is"
		;
connectAttr "|Sit_Idle_2|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_R_Clavicle|Bip01_R_UpperArm|Bip01_R_Forearm|Bip01_R_Hand.s" "|Sit_Idle_2|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_R_Clavicle|Bip01_R_UpperArm|Bip01_R_Forearm|Bip01_R_Hand|Bip01_R_Finger3.is"
		;
connectAttr "|Sit_Idle_2|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_R_Clavicle|Bip01_R_UpperArm|Bip01_R_Forearm|Bip01_R_Hand|Bip01_R_Finger3.s" "|Sit_Idle_2|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_R_Clavicle|Bip01_R_UpperArm|Bip01_R_Forearm|Bip01_R_Hand|Bip01_R_Finger3|Bip01_R_Finger31.is"
		;
connectAttr "|Sit_Idle_2|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_R_Clavicle|Bip01_R_UpperArm|Bip01_R_Forearm|Bip01_R_Hand|Bip01_R_Finger3|Bip01_R_Finger31.s" "|Sit_Idle_2|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_R_Clavicle|Bip01_R_UpperArm|Bip01_R_Forearm|Bip01_R_Hand|Bip01_R_Finger3|Bip01_R_Finger31|_notused_Bip01_R_Finger3Nub.is"
		;
connectAttr "|Sit_Idle_2|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_R_Clavicle|Bip01_R_UpperArm|Bip01_R_Forearm|Bip01_R_Hand.s" "|Sit_Idle_2|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_R_Clavicle|Bip01_R_UpperArm|Bip01_R_Forearm|Bip01_R_Hand|Bip01_R_Finger4.is"
		;
connectAttr "|Sit_Idle_2|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_R_Clavicle|Bip01_R_UpperArm|Bip01_R_Forearm|Bip01_R_Hand|Bip01_R_Finger4.s" "|Sit_Idle_2|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_R_Clavicle|Bip01_R_UpperArm|Bip01_R_Forearm|Bip01_R_Hand|Bip01_R_Finger4|Bip01_R_Finger41.is"
		;
connectAttr "|Sit_Idle_2|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_R_Clavicle|Bip01_R_UpperArm|Bip01_R_Forearm|Bip01_R_Hand|Bip01_R_Finger4|Bip01_R_Finger41.s" "|Sit_Idle_2|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_R_Clavicle|Bip01_R_UpperArm|Bip01_R_Forearm|Bip01_R_Hand|Bip01_R_Finger4|Bip01_R_Finger41|_notused_Bip01_R_Finger4Nub.is"
		;
connectAttr "Bip01_L_Clavicle_rotateX4.o" "|Sit_Idle_2|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_L_Clavicle.rx"
		;
connectAttr "Bip01_L_Clavicle_rotateY4.o" "|Sit_Idle_2|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_L_Clavicle.ry"
		;
connectAttr "Bip01_L_Clavicle_rotateZ4.o" "|Sit_Idle_2|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_L_Clavicle.rz"
		;
connectAttr "|Sit_Idle_2|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck.s" "|Sit_Idle_2|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_L_Clavicle.is"
		;
connectAttr "Bip01_L_UpperArm_rotateX4.o" "|Sit_Idle_2|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_L_Clavicle|Bip01_L_UpperArm.rx"
		;
connectAttr "Bip01_L_UpperArm_rotateY4.o" "|Sit_Idle_2|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_L_Clavicle|Bip01_L_UpperArm.ry"
		;
connectAttr "Bip01_L_UpperArm_rotateZ4.o" "|Sit_Idle_2|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_L_Clavicle|Bip01_L_UpperArm.rz"
		;
connectAttr "|Sit_Idle_2|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_L_Clavicle.s" "|Sit_Idle_2|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_L_Clavicle|Bip01_L_UpperArm.is"
		;
connectAttr "Bip01_L_Forearm_rotateY4.o" "|Sit_Idle_2|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_L_Clavicle|Bip01_L_UpperArm|Bip01_L_Forearm.ry"
		;
connectAttr "Bip01_L_Forearm_rotateZ4.o" "|Sit_Idle_2|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_L_Clavicle|Bip01_L_UpperArm|Bip01_L_Forearm.rz"
		;
connectAttr "Bip01_L_Forearm_rotateX4.o" "|Sit_Idle_2|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_L_Clavicle|Bip01_L_UpperArm|Bip01_L_Forearm.rx"
		;
connectAttr "|Sit_Idle_2|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_L_Clavicle|Bip01_L_UpperArm.s" "|Sit_Idle_2|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_L_Clavicle|Bip01_L_UpperArm|Bip01_L_Forearm.is"
		;
connectAttr "|Sit_Idle_2|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_L_Clavicle|Bip01_L_UpperArm|Bip01_L_Forearm.s" "|Sit_Idle_2|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_L_Clavicle|Bip01_L_UpperArm|Bip01_L_Forearm|Bip01_L_Hand.is"
		;
connectAttr "|Sit_Idle_2|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_L_Clavicle|Bip01_L_UpperArm|Bip01_L_Forearm|Bip01_L_Hand.s" "|Sit_Idle_2|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_L_Clavicle|Bip01_L_UpperArm|Bip01_L_Forearm|Bip01_L_Hand|Bip01_L_Finger0.is"
		;
connectAttr "|Sit_Idle_2|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_L_Clavicle|Bip01_L_UpperArm|Bip01_L_Forearm|Bip01_L_Hand|Bip01_L_Finger0.s" "|Sit_Idle_2|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_L_Clavicle|Bip01_L_UpperArm|Bip01_L_Forearm|Bip01_L_Hand|Bip01_L_Finger0|Bip01_L_Finger01.is"
		;
connectAttr "|Sit_Idle_2|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_L_Clavicle|Bip01_L_UpperArm|Bip01_L_Forearm|Bip01_L_Hand|Bip01_L_Finger0|Bip01_L_Finger01.s" "|Sit_Idle_2|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_L_Clavicle|Bip01_L_UpperArm|Bip01_L_Forearm|Bip01_L_Hand|Bip01_L_Finger0|Bip01_L_Finger01|_notused_Bip01_L_Finger0Nub.is"
		;
connectAttr "|Sit_Idle_2|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_L_Clavicle|Bip01_L_UpperArm|Bip01_L_Forearm|Bip01_L_Hand.s" "|Sit_Idle_2|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_L_Clavicle|Bip01_L_UpperArm|Bip01_L_Forearm|Bip01_L_Hand|Bip01_L_Finger2.is"
		;
connectAttr "|Sit_Idle_2|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_L_Clavicle|Bip01_L_UpperArm|Bip01_L_Forearm|Bip01_L_Hand|Bip01_L_Finger2.s" "|Sit_Idle_2|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_L_Clavicle|Bip01_L_UpperArm|Bip01_L_Forearm|Bip01_L_Hand|Bip01_L_Finger2|Bip01_L_Finger21.is"
		;
connectAttr "|Sit_Idle_2|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_L_Clavicle|Bip01_L_UpperArm|Bip01_L_Forearm|Bip01_L_Hand|Bip01_L_Finger2|Bip01_L_Finger21.s" "|Sit_Idle_2|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_L_Clavicle|Bip01_L_UpperArm|Bip01_L_Forearm|Bip01_L_Hand|Bip01_L_Finger2|Bip01_L_Finger21|_notused_Bip01_L_Finger2Nub.is"
		;
connectAttr "|Sit_Idle_2|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_L_Clavicle|Bip01_L_UpperArm|Bip01_L_Forearm|Bip01_L_Hand.s" "|Sit_Idle_2|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_L_Clavicle|Bip01_L_UpperArm|Bip01_L_Forearm|Bip01_L_Hand|Bip01_L_Finger1.is"
		;
connectAttr "|Sit_Idle_2|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_L_Clavicle|Bip01_L_UpperArm|Bip01_L_Forearm|Bip01_L_Hand|Bip01_L_Finger1.s" "|Sit_Idle_2|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_L_Clavicle|Bip01_L_UpperArm|Bip01_L_Forearm|Bip01_L_Hand|Bip01_L_Finger1|Bip01_L_Finger11.is"
		;
connectAttr "|Sit_Idle_2|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_L_Clavicle|Bip01_L_UpperArm|Bip01_L_Forearm|Bip01_L_Hand|Bip01_L_Finger1|Bip01_L_Finger11.s" "|Sit_Idle_2|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_L_Clavicle|Bip01_L_UpperArm|Bip01_L_Forearm|Bip01_L_Hand|Bip01_L_Finger1|Bip01_L_Finger11|_notused_Bip01_L_Finger1Nub.is"
		;
connectAttr "|Sit_Idle_2|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_L_Clavicle|Bip01_L_UpperArm|Bip01_L_Forearm|Bip01_L_Hand.s" "|Sit_Idle_2|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_L_Clavicle|Bip01_L_UpperArm|Bip01_L_Forearm|Bip01_L_Hand|Bip01_L_Finger3.is"
		;
connectAttr "|Sit_Idle_2|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_L_Clavicle|Bip01_L_UpperArm|Bip01_L_Forearm|Bip01_L_Hand|Bip01_L_Finger3.s" "|Sit_Idle_2|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_L_Clavicle|Bip01_L_UpperArm|Bip01_L_Forearm|Bip01_L_Hand|Bip01_L_Finger3|Bip01_L_Finger31.is"
		;
connectAttr "|Sit_Idle_2|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_L_Clavicle|Bip01_L_UpperArm|Bip01_L_Forearm|Bip01_L_Hand|Bip01_L_Finger3|Bip01_L_Finger31.s" "|Sit_Idle_2|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_L_Clavicle|Bip01_L_UpperArm|Bip01_L_Forearm|Bip01_L_Hand|Bip01_L_Finger3|Bip01_L_Finger31|_notused_Bip01_L_Finger3Nub.is"
		;
connectAttr "|Sit_Idle_2|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_L_Clavicle|Bip01_L_UpperArm|Bip01_L_Forearm|Bip01_L_Hand.s" "|Sit_Idle_2|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_L_Clavicle|Bip01_L_UpperArm|Bip01_L_Forearm|Bip01_L_Hand|Bip01_L_Finger4.is"
		;
connectAttr "|Sit_Idle_2|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_L_Clavicle|Bip01_L_UpperArm|Bip01_L_Forearm|Bip01_L_Hand|Bip01_L_Finger4.s" "|Sit_Idle_2|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_L_Clavicle|Bip01_L_UpperArm|Bip01_L_Forearm|Bip01_L_Hand|Bip01_L_Finger4|Bip01_L_Finger41.is"
		;
connectAttr "|Sit_Idle_2|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_L_Clavicle|Bip01_L_UpperArm|Bip01_L_Forearm|Bip01_L_Hand|Bip01_L_Finger4|Bip01_L_Finger41.s" "|Sit_Idle_2|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_L_Clavicle|Bip01_L_UpperArm|Bip01_L_Forearm|Bip01_L_Hand|Bip01_L_Finger4|Bip01_L_Finger41|_notused_Bip01_L_Finger4Nub.is"
		;
connectAttr "Bip01_L_Thigh_rotateX4.o" "|Sit_Idle_2|Bip01_Spine|Bip01_L_Thigh.rx"
		;
connectAttr "Bip01_L_Thigh_rotateY4.o" "|Sit_Idle_2|Bip01_Spine|Bip01_L_Thigh.ry"
		;
connectAttr "Bip01_L_Thigh_rotateZ4.o" "|Sit_Idle_2|Bip01_Spine|Bip01_L_Thigh.rz"
		;
connectAttr "|Sit_Idle_2|Bip01_Spine.s" "|Sit_Idle_2|Bip01_Spine|Bip01_L_Thigh.is"
		;
connectAttr "Bip01_L_Calf_rotateX4.o" "|Sit_Idle_2|Bip01_Spine|Bip01_L_Thigh|Bip01_L_Calf.rx"
		;
connectAttr "Bip01_L_Calf_rotateY4.o" "|Sit_Idle_2|Bip01_Spine|Bip01_L_Thigh|Bip01_L_Calf.ry"
		;
connectAttr "Bip01_L_Calf_rotateZ4.o" "|Sit_Idle_2|Bip01_Spine|Bip01_L_Thigh|Bip01_L_Calf.rz"
		;
connectAttr "|Sit_Idle_2|Bip01_Spine|Bip01_L_Thigh.s" "|Sit_Idle_2|Bip01_Spine|Bip01_L_Thigh|Bip01_L_Calf.is"
		;
connectAttr "Bip01_L_Foot_rotateX4.o" "|Sit_Idle_2|Bip01_Spine|Bip01_L_Thigh|Bip01_L_Calf|Bip01_L_Foot.rx"
		;
connectAttr "Bip01_L_Foot_rotateY4.o" "|Sit_Idle_2|Bip01_Spine|Bip01_L_Thigh|Bip01_L_Calf|Bip01_L_Foot.ry"
		;
connectAttr "Bip01_L_Foot_rotateZ4.o" "|Sit_Idle_2|Bip01_Spine|Bip01_L_Thigh|Bip01_L_Calf|Bip01_L_Foot.rz"
		;
connectAttr "|Sit_Idle_2|Bip01_Spine|Bip01_L_Thigh|Bip01_L_Calf.s" "|Sit_Idle_2|Bip01_Spine|Bip01_L_Thigh|Bip01_L_Calf|Bip01_L_Foot.is"
		;
connectAttr "|Sit_Idle_2|Bip01_Spine|Bip01_L_Thigh|Bip01_L_Calf|Bip01_L_Foot.s" "|Sit_Idle_2|Bip01_Spine|Bip01_L_Thigh|Bip01_L_Calf|Bip01_L_Foot|Bip01_L_Toe0.is"
		;
connectAttr "|Sit_Idle_2|Bip01_Spine|Bip01_L_Thigh|Bip01_L_Calf|Bip01_L_Foot|Bip01_L_Toe0.s" "|Sit_Idle_2|Bip01_Spine|Bip01_L_Thigh|Bip01_L_Calf|Bip01_L_Foot|Bip01_L_Toe0|_notused_Bip01_L_Toe0Nub.is"
		;
connectAttr "Bip01_R_Thigh_rotateX4.o" "|Sit_Idle_2|Bip01_Spine|Bip01_R_Thigh.rx"
		;
connectAttr "Bip01_R_Thigh_rotateY4.o" "|Sit_Idle_2|Bip01_Spine|Bip01_R_Thigh.ry"
		;
connectAttr "Bip01_R_Thigh_rotateZ4.o" "|Sit_Idle_2|Bip01_Spine|Bip01_R_Thigh.rz"
		;
connectAttr "|Sit_Idle_2|Bip01_Spine.s" "|Sit_Idle_2|Bip01_Spine|Bip01_R_Thigh.is"
		;
connectAttr "Bip01_R_Calf_rotateX4.o" "|Sit_Idle_2|Bip01_Spine|Bip01_R_Thigh|Bip01_R_Calf.rx"
		;
connectAttr "Bip01_R_Calf_rotateY4.o" "|Sit_Idle_2|Bip01_Spine|Bip01_R_Thigh|Bip01_R_Calf.ry"
		;
connectAttr "Bip01_R_Calf_rotateZ4.o" "|Sit_Idle_2|Bip01_Spine|Bip01_R_Thigh|Bip01_R_Calf.rz"
		;
connectAttr "|Sit_Idle_2|Bip01_Spine|Bip01_R_Thigh.s" "|Sit_Idle_2|Bip01_Spine|Bip01_R_Thigh|Bip01_R_Calf.is"
		;
connectAttr "Bip01_R_Foot_rotateX4.o" "|Sit_Idle_2|Bip01_Spine|Bip01_R_Thigh|Bip01_R_Calf|Bip01_R_Foot.rx"
		;
connectAttr "Bip01_R_Foot_rotateY4.o" "|Sit_Idle_2|Bip01_Spine|Bip01_R_Thigh|Bip01_R_Calf|Bip01_R_Foot.ry"
		;
connectAttr "Bip01_R_Foot_rotateZ4.o" "|Sit_Idle_2|Bip01_Spine|Bip01_R_Thigh|Bip01_R_Calf|Bip01_R_Foot.rz"
		;
connectAttr "|Sit_Idle_2|Bip01_Spine|Bip01_R_Thigh|Bip01_R_Calf.s" "|Sit_Idle_2|Bip01_Spine|Bip01_R_Thigh|Bip01_R_Calf|Bip01_R_Foot.is"
		;
connectAttr "|Sit_Idle_2|Bip01_Spine|Bip01_R_Thigh|Bip01_R_Calf|Bip01_R_Foot.s" "|Sit_Idle_2|Bip01_Spine|Bip01_R_Thigh|Bip01_R_Calf|Bip01_R_Foot|Bip01_R_Toe0.is"
		;
connectAttr "|Sit_Idle_2|Bip01_Spine|Bip01_R_Thigh|Bip01_R_Calf|Bip01_R_Foot|Bip01_R_Toe0.s" "|Sit_Idle_2|Bip01_Spine|Bip01_R_Thigh|Bip01_R_Calf|Bip01_R_Foot|Bip01_R_Toe0|_notused_Bip01_R_Toe0Nub.is"
		;
// End of Animation v5 - Sit Idle 2.ma
