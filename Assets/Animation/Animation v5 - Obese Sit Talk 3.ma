//Maya ASCII 2014 scene
//Name: Animation v5 - Obese Sit Talk 3.ma
//Last modified: Tue, Apr 14, 2015 07:40:14 AM
//Codeset: 1252
requires maya "2014";
currentUnit -l centimeter -a degree -t film;
fileInfo "application" "maya";
fileInfo "product" "Maya 2014";
fileInfo "version" "2014 x64";
fileInfo "cutIdentifier" "201303010241-864206";
fileInfo "osv" "Microsoft Windows 7 Enterprise Edition, 64-bit Windows 7 Service Pack 1 (Build 7601)\n";
fileInfo "license" "education";
createNode transform -n "Obese_Sit_Talk_3";
	setAttr ".v" no;
createNode joint -n "Bip01_Spine" -p "Obese_Sit_Talk_3";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".jo" -type "double3" 90.000171 0 89.999999999999986 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" 0.0058944645619291025 0.99957100188420478 -0.028689152649306493 0
		 -0.00095053300202146973 0.028695238773347783 0.99958775490636764 0 0.99998217572841774 -0.0058647646114406982 0.0011192682932001929 0
		 -6.7139604917915256e-006 99.443700000000007 -2.2495999999899592 1;
	setAttr -k on ".liw";
	setAttr ".fbxID" 5;
createNode joint -n "Bip01_Spine1" -p "|Obese_Sit_Talk_3|Bip01_Spine";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".t" -type "double3" 13.3984 -0.0107 0 ;
	setAttr ".s" -type "double3" 1 0.99999999999999989 1 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" -0.013016029413255978 -0.008122775381498382 0.99988229482195301 0
		 0.029728798363151423 -0.9995280890415984 -0.0077329014434088985 0 0.99947325203138693 0.029624647457009743 0.013251367357806723 0
		 9.9999999999999995e-007 2.2603360000000001 112.84212599999999 1;
	setAttr -k on ".liw";
	setAttr ".fbxID" 5;
createNode joint -n "Bip01_Spine2" -p "|Obese_Sit_Talk_3|Bip01_Spine|Bip01_Spine1";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".t" -type "double3" 13.3983 -0.0107 0 ;
	setAttr ".s" -type "double3" 1 0.99999999999999956 1 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" 0 0 1 0 0 -1 0 0 1 0 0 0 -3.9999999999999998e-006 -9.6239790000000003 -19.074804 1;
	setAttr -k on ".liw";
	setAttr ".fbxID" 5;
createNode joint -n "Bip01_Neck" -p "|Obese_Sit_Talk_3|Bip01_Spine|Bip01_Spine1|Bip01_Spine2";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".t" -type "double3" 13.3985 -0.007899999999999999 0 ;
	setAttr ".r" -type "double3" 1.1848489498583726e-023 -5.9999999999999985e-006 1.1479959999999994 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" 0 -0.020830999999999999 0.99978299999999998 0 0 -0.99978299999999998 -0.020830999999999999 0
		 1 0 0 0 -1.0000000000000001e-005 -9.6266999999999996 -5.6762839999999999 1;
	setAttr -k on ".liw";
	setAttr ".fbxID" 5;
createNode joint -n "Bip01_Head" -p "|Obese_Sit_Talk_3|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".t" -type "double3" 9.9829 0 0 ;
	setAttr ".r" -type "double3" 9.4787915988671744e-023 -1.3000000000000004e-005 4.622390999999995 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" 0 -0.10133399999999999 0.99485299999999999 0 0 -0.99485199999999996 -0.10133399999999999 0
		 1 0 0 0 -4.4000000000000012e-005 -9.8346470000000004 4.3044190000000002 1;
	setAttr -k on ".liw";
	setAttr ".fbxID" 5;
createNode joint -n "_notused_Bip01_HeadNub" -p "|Obese_Sit_Talk_3|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_Head";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".t" -type "double3" 18.3537 0 0 ;
	setAttr ".ssc" no;
	setAttr ".fbxID" 5;
createNode joint -n "Bip01_R_Clavicle" -p "|Obese_Sit_Talk_3|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".t" -type "double3" -0.8268 -0.6612 -3.9165 ;
	setAttr ".s" -type "double3" 0.99999999999999989 0.99999999999999989 0.99999999999999989 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" -0.99454799999999999 -9.9454800000000002e-007 -0.104286 0
		 -9.8906500000000008e-007 1 -1.0428599999999999e-007 0 0.104286 -5.7179000000000015e-010 -0.99454799999999999 0
		 -3.9165049999999999 2.9251839999999998 138.82619800000001 1;
	setAttr -k on ".liw";
	setAttr ".fbxID" 5;
createNode joint -n "Bip01_R_UpperArm" -p "|Obese_Sit_Talk_3|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_R_Clavicle";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".t" -type "double3" 10.6745 0 0 ;
	setAttr ".s" -type "double3" 0.99999999999999989 1 1 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" -0.75332600000000005 0.10000199999999999 -0.65000000000000002 0
		 0.071782399999999996 0.99496899999999999 0.0698825 0 0.65371900000000005 0.0059857399999999998 -0.75671500000000003 0
		 -14.532831 2.9251239999999998 137.712976 1;
	setAttr -k on ".liw";
	setAttr ".fbxID" 5;
createNode joint -n "Bip01_R_Forearm" -p "|Obese_Sit_Talk_3|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_R_Clavicle|Bip01_R_UpperArm";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".t" -type "double3" 25.3375 0 0 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" -0.752861 -0.095945600000000006 -0.65114899999999998 0
		 -0.076500899999999997 0.99536800000000003 -0.058215000000000003 0 0.65371900000000005 0.0059856600000000003 -0.756714 0
		 -33.620182 5.4589869999999996 121.243574 1;
	setAttr -k on ".liw";
	setAttr ".fbxID" 5;
createNode joint -n "Bip01_R_Hand" -p "|Obese_Sit_Talk_3|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_R_Clavicle|Bip01_R_UpperArm|Bip01_R_Forearm";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".t" -type "double3" 24.9881 0 0 ;
	setAttr ".s" -type "double3" 1 0.99999999999999989 1 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" -0.67922700000000003 -0.050331500000000001 -0.73219999999999996 0
		 0.73278100000000002 0.0092545500000000003 -0.68040299999999998 0 0.0410219 -0.99868900000000005 0.030596000000000002 0
		 -52.432737000000003 3.0614050000000002 104.972679 1;
	setAttr -k on ".liw";
	setAttr ".fbxID" 5;
createNode joint -n "Bip01_R_Finger0" -p "|Obese_Sit_Talk_3|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_R_Clavicle|Bip01_R_UpperArm|Bip01_R_Forearm|Bip01_R_Hand";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".t" -type "double3" 2.3036000000000003 1.3941 2.9222 ;
	setAttr ".r" -type "double3" -81.93018 -23.314181 18.295908 ;
	setAttr ".s" -type "double3" 0.99999999999999989 1 1 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" -0.36474400000000001 -0.436471 -0.82246900000000001 0
		 -0.072261699999999998 0.89392799999999994 -0.44234699999999999 0 0.92829899999999999 -0.10191 -0.357595 0
		 -52.855969000000002 0.0400224 102.426821 1;
	setAttr -k on ".liw";
	setAttr ".fbxID" 5;
createNode joint -n "Bip01_R_Finger01" -p "|Obese_Sit_Talk_3|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_R_Clavicle|Bip01_R_UpperArm|Bip01_R_Forearm|Bip01_R_Hand|Bip01_R_Finger0";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".t" -type "double3" 4.0806 0 0 ;
	setAttr ".r" -type "double3" 9.4787915988669452e-023 3.0000000000000005e-006 24.064549 ;
	setAttr ".s" -type "double3" 1 1 0.99999999999999989 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" -0.36250900000000003 -0.034022900000000002 -0.93135900000000005 0
		 0.082749500000000004 0.99421099999999996 -0.068527199999999996 0 0.92830000000000001 -0.101911 -0.357595 0
		 -54.344414999999998 -1.7410159999999999 99.070615000000004 1;
	setAttr -k on ".liw";
	setAttr ".fbxID" 5;
createNode joint -n "_notused_Bip01_R_Finger0Nub" -p "|Obese_Sit_Talk_3|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_R_Clavicle|Bip01_R_UpperArm|Bip01_R_Forearm|Bip01_R_Hand|Bip01_R_Finger0|Bip01_R_Finger01";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".t" -type "double3" 3.3413 0 0 ;
	setAttr ".r" -type "double3" 1e-006 0 -180 ;
	setAttr ".ssc" no;
	setAttr ".fbxID" 5;
createNode joint -n "Bip01_R_Finger2" -p "|Obese_Sit_Talk_3|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_R_Clavicle|Bip01_R_UpperArm|Bip01_R_Forearm|Bip01_R_Hand";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".t" -type "double3" 7.3709 -0.4816 0.6341 ;
	setAttr ".r" -type "double3" 0.043615000000000029 -0.013407000000000007 17.086613999999997 ;
	setAttr ".s" -type "double3" 0.99999999999999989 0.99999999999999989 0.99999999999999989 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" -0.43393399999999999 -0.045623999999999998 -0.89978899999999995 0
		 0.90003599999999995 0.022875199999999998 -0.43521399999999999 0 0.040439000000000003 -0.99869600000000003 0.031137000000000001 0
		 -57.766043000000003 2.0527600000000001 99.922762000000006 1;
	setAttr -k on ".liw";
	setAttr ".fbxID" 5;
createNode joint -n "Bip01_R_Finger21" -p "|Obese_Sit_Talk_3|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_R_Clavicle|Bip01_R_UpperArm|Bip01_R_Forearm|Bip01_R_Hand|Bip01_R_Finger2";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".t" -type "double3" 3.8208 0 0 ;
	setAttr ".r" -type "double3" 0 0 29.946389999999987 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" 0.0732873 -0.028113900000000001 -0.99691399999999997 0
		 0.99648999999999999 0.042598999999999998 0.072054900000000005 0 0.0404418 -0.99869600000000003 0.0311372 0
		 -59.424045999999997 1.8784400000000001 96.484806000000006 1;
	setAttr -k on ".liw";
	setAttr ".fbxID" 5;
createNode joint -n "_notused_Bip01_R_Finger2Nub" -p "|Obese_Sit_Talk_3|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_R_Clavicle|Bip01_R_UpperArm|Bip01_R_Forearm|Bip01_R_Hand|Bip01_R_Finger2|Bip01_R_Finger21";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".t" -type "double3" 4.7174000000000005 0 0 ;
	setAttr ".r" -type "double3" 0 0 180 ;
	setAttr ".ssc" no;
	setAttr ".fbxID" 5;
createNode joint -n "Bip01_R_Finger1" -p "|Obese_Sit_Talk_3|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_R_Clavicle|Bip01_R_UpperArm|Bip01_R_Forearm|Bip01_R_Hand";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".t" -type "double3" 7.3263 -0.211 2.2784 ;
	setAttr ".r" -type "double3" -0.73310399999999953 -5.3145149999999983 22.646732999999994 ;
	setAttr ".s" -type "double3" 1 1 0.99999999999999989 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" -0.33941900000000003 -0.13520499999999999 -0.93086800000000003 0
		 0.93680799999999997 0.040593299999999999 -0.34748099999999998 0 0.084767999999999996 -0.98998600000000003 0.112883 0
		 -57.470132999999997 0.41539700000000002 99.821579 1;
	setAttr -k on ".liw";
	setAttr ".fbxID" 5;
createNode joint -n "Bip01_R_Finger11" -p "|Obese_Sit_Talk_3|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_R_Clavicle|Bip01_R_UpperArm|Bip01_R_Forearm|Bip01_R_Hand|Bip01_R_Finger1";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".t" -type "double3" 3.5597999999999996 0 0 ;
	setAttr ".r" -type "double3" -2e-006 3.999999999999999e-006 22.627677999999996 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" 0.047135799999999999 -0.10918 -0.99290400000000001 0
		 0.995286 0.089482699999999998 0.037409400000000002 0 0.084763400000000003 -0.98998600000000003 0.112883 0
		 -58.678344000000003 -0.065903900000000001 96.507863999999998 1;
	setAttr -k on ".liw";
	setAttr ".fbxID" 5;
createNode joint -n "_notused_Bip01_R_Finger1Nub" -p "|Obese_Sit_Talk_3|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_R_Clavicle|Bip01_R_UpperArm|Bip01_R_Forearm|Bip01_R_Hand|Bip01_R_Finger1|Bip01_R_Finger11";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".t" -type "double3" 4.1404000000000005 0 0 ;
	setAttr ".r" -type "double3" 0 0 180 ;
	setAttr ".ssc" no;
	setAttr ".fbxID" 5;
createNode joint -n "Bip01_R_Finger3" -p "|Obese_Sit_Talk_3|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_R_Clavicle|Bip01_R_UpperArm|Bip01_R_Forearm|Bip01_R_Hand";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".t" -type "double3" 7.2345 -0.5997 -0.9332 ;
	setAttr ".r" -type "double3" 0.043363999999999805 3.826455 18.509659999999997 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" -0.41327900000000001 0.0219578 -0.91034000000000004 0
		 0.91051400000000005 0.0239971 -0.41277999999999998 0 0.0127817 -0.999471 -0.0299104 0
		 -57.824306 3.6237469999999998 100.05504999999999 1;
	setAttr -k on ".liw";
	setAttr ".fbxID" 5;
createNode joint -n "Bip01_R_Finger31" -p "|Obese_Sit_Talk_3|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_R_Clavicle|Bip01_R_UpperArm|Bip01_R_Forearm|Bip01_R_Hand|Bip01_R_Finger3";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".t" -type "double3" 3.7241 0 0 ;
	setAttr ".r" -type "double3" 3.0000000000000013e-006 3.0000000000000005e-006 27.984287000000009 ;
	setAttr ".s" -type "double3" 1 0.99999999999999989 0.99999999999999989 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" 0.0622821 0.0306512 -0.99758800000000003 0 0.99797599999999997 0.0108983 0.062641199999999994 0
		 0.012792100000000001 -0.999471 -0.0299104 0 -59.363360999999998 3.705527 96.664748000000003 1;
	setAttr -k on ".liw";
	setAttr ".fbxID" 5;
createNode joint -n "_notused_Bip01_R_Finger3Nub" -p "|Obese_Sit_Talk_3|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_R_Clavicle|Bip01_R_UpperArm|Bip01_R_Forearm|Bip01_R_Hand|Bip01_R_Finger3|Bip01_R_Finger31";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".t" -type "double3" 4.2397 0 0 ;
	setAttr ".r" -type "double3" 3e-006 -5e-006 180 ;
	setAttr ".ssc" no;
	setAttr ".fbxID" 5;
createNode joint -n "Bip01_R_Finger4" -p "|Obese_Sit_Talk_3|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_R_Clavicle|Bip01_R_UpperArm|Bip01_R_Forearm|Bip01_R_Hand";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".t" -type "double3" 7.2292 -0.4187 -2.3639 ;
	setAttr ".r" -type "double3" -0.17713400000000007 5.5619560000000012 16.615208 ;
	setAttr ".s" -type "double3" 0.99999999999999989 0.99999999999999989 1 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" -0.44323200000000001 0.051426199999999998 -0.89493100000000003 0
		 0.89640699999999995 0.026346700000000001 -0.44244899999999998 0 0.00082491599999999997 -0.99832900000000002 -0.057776500000000001 0
		 -57.746822000000002 5.0544450000000003 99.892002000000005 1;
	setAttr -k on ".liw";
	setAttr ".fbxID" 5;
createNode joint -n "Bip01_R_Finger41" -p "|Obese_Sit_Talk_3|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_R_Clavicle|Bip01_R_UpperArm|Bip01_R_Forearm|Bip01_R_Hand|Bip01_R_Finger4";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".t" -type "double3" 3.4791000000000003 0 0 ;
	setAttr ".r" -type "double3" -1.9999999999999995e-006 -3.0000000000000005e-006 29.444368 ;
	setAttr ".s" -type "double3" 1 0.99999999999999989 1 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" 0.054672699999999998 0.057735700000000001 -0.996834 0
		 0.99850399999999995 -0.0023359700000000001 0.054628999999999997 0 0.00082546799999999999 -0.99832900000000002 -0.057777000000000002 0
		 -59.288860999999997 5.2333790000000002 96.778379000000001 1;
	setAttr -k on ".liw";
	setAttr ".fbxID" 5;
createNode joint -n "_notused_Bip01_R_Finger4Nub" -p "|Obese_Sit_Talk_3|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_R_Clavicle|Bip01_R_UpperArm|Bip01_R_Forearm|Bip01_R_Hand|Bip01_R_Finger4|Bip01_R_Finger41";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".t" -type "double3" 3.3974 0 0 ;
	setAttr ".r" -type "double3" 0 0 -180 ;
	setAttr ".ssc" no;
	setAttr ".fbxID" 5;
createNode joint -n "Bip01_L_Clavicle" -p "|Obese_Sit_Talk_3|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".t" -type "double3" -0.8268 -0.6612 3.9165 ;
	setAttr ".s" -type "double3" 1 0.99999999999999989 0.99999999999999989 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" 0.99454799999999999 9.9454800000000002e-007 -0.104286 0
		 -9.8906500000000008e-007 1 1.0428599999999999e-007 0 0.104286 -5.7179000000000015e-010 0.99454799999999999 0
		 3.9165070000000002 2.9252039999999999 138.82619700000001 1;
	setAttr -k on ".liw";
	setAttr ".fbxID" 5;
createNode joint -n "Bip01_L_UpperArm" -p "|Obese_Sit_Talk_3|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_L_Clavicle";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".t" -type "double3" 10.6745 0 0 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" 0.75332500000000002 0.100004 -0.65000000000000002 0
		 -0.071785600000000005 0.99496899999999999 0.069882299999999994 0 0.65371900000000005 -0.0059834299999999997 0.756714 0
		 14.532825000000001 2.925173 137.712974 1;
	setAttr -k on ".liw";
	setAttr ".fbxID" 5;
createNode joint -n "Bip01_L_Forearm" -p "|Obese_Sit_Talk_3|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_L_Clavicle|Bip01_L_UpperArm";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".t" -type "double3" 25.3374 0 0 ;
	setAttr ".s" -type "double3" 1 1 0.99999999999999989 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" 0.752861 -0.095943299999999995 -0.65114899999999998 0
		 0.076497700000000002 0.99536899999999995 -0.058215299999999998 0 0.65371900000000005 -0.0059834199999999997 0.756714 0
		 33.620189000000003 5.4590990000000001 121.243576 1;
	setAttr -k on ".liw";
	setAttr ".fbxID" 5;
createNode joint -n "Bip01_L_Hand" -p "|Obese_Sit_Talk_3|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_L_Clavicle|Bip01_L_UpperArm|Bip01_L_Forearm";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".t" -type "double3" 24.9881 0 0 ;
	setAttr ".s" -type "double3" 0.99999999999999989 1 0.99999999999999989 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" 0.67922800000000005 -0.050329499999999999 -0.73219999999999996 0
		 -0.73278100000000002 0.0092525000000000003 -0.68040299999999998 0 0.041019 0.99868999999999997 -0.030595899999999999 0
		 52.432744999999997 3.0615610000000002 104.972662 1;
	setAttr -k on ".liw";
	setAttr ".fbxID" 5;
createNode joint -n "Bip01_L_Finger0" -p "|Obese_Sit_Talk_3|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_L_Clavicle|Bip01_L_UpperArm|Bip01_L_Forearm|Bip01_L_Hand";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".t" -type "double3" 2.3036000000000003 1.3941 -2.9222 ;
	setAttr ".r" -type "double3" 81.930180000000021 23.314181000000019 18.295909 ;
	setAttr ".s" -type "double3" 0.99999999999999989 1 1.0000000000000002 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" 0.36474600000000001 -0.43647000000000002 -0.82246900000000001 0
		 0.072258799999999998 0.89392799999999994 -0.44234699999999999 0 0.92830000000000001 0.101913 0.357595 0
		 52.856034999999999 0.0402022 102.42681 1;
	setAttr -k on ".liw";
	setAttr ".fbxID" 5;
createNode joint -n "Bip01_L_Finger01" -p "|Obese_Sit_Talk_3|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_L_Clavicle|Bip01_L_UpperArm|Bip01_L_Forearm|Bip01_L_Hand|Bip01_L_Finger0";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".t" -type "double3" 4.0806 0 0 ;
	setAttr ".r" -type "double3" 0 0 24.06455200000001 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" 0.36250900000000003 -0.034021900000000001 -0.93135900000000005 0
		 -0.082751900000000003 0.99421199999999998 -0.068527000000000005 0 0.92829899999999999 0.101913 0.357595 0
		 54.344436000000002 -1.7408459999999999 99.070606999999995 1;
	setAttr -k on ".liw";
	setAttr ".fbxID" 5;
createNode joint -n "_notused_Bip01_L_Finger0Nub" -p "|Obese_Sit_Talk_3|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_L_Clavicle|Bip01_L_UpperArm|Bip01_L_Forearm|Bip01_L_Hand|Bip01_L_Finger0|Bip01_L_Finger01";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".t" -type "double3" 3.3413 0 0 ;
	setAttr ".ssc" no;
	setAttr ".fbxID" 5;
createNode joint -n "Bip01_L_Finger2" -p "|Obese_Sit_Talk_3|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_L_Clavicle|Bip01_L_UpperArm|Bip01_L_Forearm|Bip01_L_Hand";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".t" -type "double3" 7.3709 -0.4816 -0.6341 ;
	setAttr ".r" -type "double3" -0.043610000000000031 0.01341 17.086619000000006 ;
	setAttr ".s" -type "double3" 1 0.99999999999999989 0.99999999999999989 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" 0.43393500000000002 -0.045622700000000002 -0.89978899999999995 0
		 -0.90003599999999995 0.022871599999999999 -0.43521399999999999 0 0.040435199999999998 0.99869699999999995 -0.031137399999999999 0
		 57.766060000000003 2.0529269999999999 99.922754999999995 1;
	setAttr -k on ".liw";
	setAttr ".fbxID" 5;
createNode joint -n "Bip01_L_Finger21" -p "|Obese_Sit_Talk_3|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_L_Clavicle|Bip01_L_UpperArm|Bip01_L_Forearm|Bip01_L_Hand|Bip01_L_Finger2";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".t" -type "double3" 3.8208 0 0 ;
	setAttr ".r" -type "double3" -4.0000000000000024e-006 -4.0000000000000015e-006 29.946390000000019 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" -0.0732873 -0.0281141 -0.99691399999999997 0 -0.99649100000000002 0.042592999999999999 0.072054999999999994 0
		 0.040435899999999997 0.99869699999999995 -0.031137000000000001 0 59.424058000000002 1.878614 96.484800000000007 1;
	setAttr -k on ".liw";
	setAttr ".fbxID" 5;
createNode joint -n "_notused_Bip01_L_Finger2Nub" -p "|Obese_Sit_Talk_3|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_L_Clavicle|Bip01_L_UpperArm|Bip01_L_Forearm|Bip01_L_Hand|Bip01_L_Finger2|Bip01_L_Finger21";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".t" -type "double3" 4.7174000000000005 0 0 ;
	setAttr ".ssc" no;
	setAttr ".fbxID" 5;
createNode joint -n "Bip01_L_Finger1" -p "|Obese_Sit_Talk_3|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_L_Clavicle|Bip01_L_UpperArm|Bip01_L_Forearm|Bip01_L_Hand";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".t" -type "double3" 7.3263 -0.211 -2.2784 ;
	setAttr ".r" -type "double3" 0.73310799999999965 5.3145129999999989 22.646742999999997 ;
	setAttr ".s" -type "double3" 1 0.99999999999999989 0.99999999999999989 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" 0.33942 -0.13520399999999999 -0.93086800000000003 0
		 -0.93680799999999997 0.040590800000000003 -0.34748099999999998 0 0.084765400000000005 0.98998600000000003 -0.112883 0
		 57.470143999999998 0.415574 99.821611000000004 1;
	setAttr -k on ".liw";
	setAttr ".fbxID" 5;
createNode joint -n "Bip01_L_Finger11" -p "|Obese_Sit_Talk_3|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_L_Clavicle|Bip01_L_UpperArm|Bip01_L_Forearm|Bip01_L_Hand|Bip01_L_Finger1";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".t" -type "double3" 3.5597999999999996 0 0 ;
	setAttr ".r" -type "double3" -2.0000000000000012e-006 3.0000000000000005e-006 22.627677000000009 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" -0.047135499999999997 -0.10918 -0.99290400000000001 0
		 -0.99528499999999998 0.089479600000000006 0.037409299999999999 0 0.0847604 0.98998699999999995 -0.112883 0
		 58.678319000000002 -0.065726099999999996 96.507845000000003 1;
	setAttr -k on ".liw";
	setAttr ".fbxID" 5;
createNode joint -n "_notused_Bip01_L_Finger1Nub" -p "|Obese_Sit_Talk_3|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_L_Clavicle|Bip01_L_UpperArm|Bip01_L_Forearm|Bip01_L_Hand|Bip01_L_Finger1|Bip01_L_Finger11";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".t" -type "double3" 4.1404000000000005 0 0 ;
	setAttr ".ssc" no;
	setAttr ".fbxID" 5;
createNode joint -n "Bip01_L_Finger3" -p "|Obese_Sit_Talk_3|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_L_Clavicle|Bip01_L_UpperArm|Bip01_L_Forearm|Bip01_L_Hand";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".t" -type "double3" 7.2345 -0.5997 0.9332 ;
	setAttr ".r" -type "double3" -0.043362000000000123 -3.826458 18.509666999999997 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" 0.41327900000000001 0.021958700000000001 -0.91034000000000004 0
		 -0.91051400000000005 0.0239966 -0.41277999999999998 0 0.012781000000000001 0.999471 0.029911 0
		 57.824314000000001 3.6238670000000002 100.055049 1;
	setAttr -k on ".liw";
	setAttr ".fbxID" 5;
createNode joint -n "Bip01_L_Finger31" -p "|Obese_Sit_Talk_3|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_L_Clavicle|Bip01_L_UpperArm|Bip01_L_Forearm|Bip01_L_Hand|Bip01_L_Finger3";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".t" -type "double3" 3.7241 0 0 ;
	setAttr ".r" -type "double3" 2e-006 -9.4787915988669323e-023 27.984288 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" -0.062282200000000003 0.030651100000000001 -0.99758800000000003 0
		 -0.99797599999999997 0.0108933 0.062641199999999994 0 0.012787099999999999 0.999471 0.029910599999999999 0
		 59.363363 3.7056490000000002 96.664751999999993 1;
	setAttr -k on ".liw";
	setAttr ".fbxID" 5;
createNode joint -n "_notused_Bip01_L_Finger3Nub" -p "|Obese_Sit_Talk_3|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_L_Clavicle|Bip01_L_UpperArm|Bip01_L_Forearm|Bip01_L_Hand|Bip01_L_Finger3|Bip01_L_Finger31";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".t" -type "double3" 4.2397 0 0 ;
	setAttr ".ssc" no;
	setAttr ".fbxID" 5;
createNode joint -n "Bip01_L_Finger4" -p "|Obese_Sit_Talk_3|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_L_Clavicle|Bip01_L_UpperArm|Bip01_L_Forearm|Bip01_L_Hand";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".t" -type "double3" 7.2292 -0.4187 2.3639 ;
	setAttr ".r" -type "double3" 0.17713199999999998 -5.5619539999999983 16.615218000000002 ;
	setAttr ".s" -type "double3" 1 0.99999999999999989 1 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" 0.44323200000000001 0.051427599999999997 -0.89493100000000003 0
		 -0.89640600000000004 0.026343800000000001 -0.44244899999999998 0 0.00082181400000000003 0.99832900000000002 0.057776399999999999 0
		 57.746775 5.054627 99.891990000000007 1;
	setAttr -k on ".liw";
	setAttr ".fbxID" 5;
createNode joint -n "Bip01_L_Finger41" -p "|Obese_Sit_Talk_3|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_L_Clavicle|Bip01_L_UpperArm|Bip01_L_Forearm|Bip01_L_Hand|Bip01_L_Finger4";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".t" -type "double3" 3.4791000000000003 0 0 ;
	setAttr ".r" -type "double3" 2e-006 -3.0000000000000009e-006 29.444363999999997 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" -0.054672800000000001 0.057735500000000002 -0.996834 0
		 -0.99850399999999995 -0.00234096 0.054628799999999998 0 0.00082047800000000003 0.99832900000000002 0.057777200000000001 0
		 59.288862000000002 5.2335589999999996 96.778389000000004 1;
	setAttr -k on ".liw";
	setAttr ".fbxID" 5;
createNode joint -n "_notused_Bip01_L_Finger4Nub" -p "|Obese_Sit_Talk_3|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_L_Clavicle|Bip01_L_UpperArm|Bip01_L_Forearm|Bip01_L_Hand|Bip01_L_Finger4|Bip01_L_Finger41";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".t" -type "double3" 3.3974 0 0 ;
	setAttr ".ssc" no;
	setAttr ".fbxID" 5;
createNode joint -n "Bip01_L_Thigh" -p "|Obese_Sit_Talk_3|Bip01_Spine";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".t" -type "double3" -11.0456 2.2584 8.5201999999999991 ;
	setAttr ".s" -type "double3" 1 1 0.99999999999999989 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" 0.059694200000000003 0.0067868399999999997 -0.998193 0
		 0.0013515700000000001 -0.99997599999999998 -0.00671814 0 -0.99821599999999999 -0.00094809699999999996 -0.059701999999999998 0
		 8.5202019999999994 4.3064900000000002e-005 88.396292000000003 1;
	setAttr -k on ".liw";
	setAttr ".fbxID" 5;
createNode joint -n "Bip01_L_Calf" -p "|Obese_Sit_Talk_3|Bip01_Spine|Bip01_L_Thigh";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".t" -type "double3" 36.1527 0 0 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" 0.059157700000000001 0.119882 -0.99102400000000002 0
		 0.0080950799999999993 -0.99278699999999998 -0.119612 0 -0.99821599999999999 -0.00094644599999999998 -0.059701400000000002 0
		 10.678299000000001 0.245397 52.308965999999998 1;
	setAttr -k on ".liw";
	setAttr ".fbxID" 5;
createNode joint -n "Bip01_L_Foot" -p "|Obese_Sit_Talk_3|Bip01_Spine|Bip01_L_Thigh|Bip01_L_Calf";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".t" -type "double3" 43.2543 0 0 ;
	setAttr ".s" -type "double3" 1 0.99999999999999989 1 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" 0 0 -1 0 0.123471 -0.99234800000000001 0 0 -0.99234800000000001 -0.123471 0 0
		 13.237131 5.4308170000000002 9.4429110000000005 1;
	setAttr -k on ".liw";
	setAttr ".fbxID" 5;
createNode joint -n "Bip01_L_Toe0" -p "|Obese_Sit_Talk_3|Bip01_Spine|Bip01_L_Thigh|Bip01_L_Calf|Bip01_L_Foot";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".t" -type "double3" 9.4714999999999989 11.2377 0 ;
	setAttr ".s" -type "double3" 0.99999999999999989 0.99999999999999989 1 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" 0.123471 -0.99234800000000001 0 0 0 0 1 0 -0.99234800000000001 -0.123471 0 0
		 14.624665999999999 -5.7209120000000002 -0.028559000000000001 1;
	setAttr -k on ".liw";
	setAttr ".fbxID" 5;
createNode joint -n "_notused_Bip01_L_Toe0Nub" -p "|Obese_Sit_Talk_3|Bip01_Spine|Bip01_L_Thigh|Bip01_L_Calf|Bip01_L_Foot|Bip01_L_Toe0";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".t" -type "double3" 7.2685 0 0 ;
	setAttr ".r" -type "double3" 0 0 180 ;
	setAttr ".ssc" no;
	setAttr ".fbxID" 5;
createNode joint -n "Bip01_R_Thigh" -p "|Obese_Sit_Talk_3|Bip01_Spine";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".t" -type "double3" -11.0456 2.2585 -8.5201999999999991 ;
	setAttr ".s" -type "double3" 1.0000000000000002 1 1.0000000000000002 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" -0.059694200000000003 0.0067826700000000002 -0.998193 0
		 -0.00134545 -0.99997599999999998 -0.0067143300000000001 0 -0.99821599999999999 0.00094221699999999997 0.059701900000000002 0
		 -8.5201879999999992 1.40303e-005 88.396298000000002 1;
	setAttr -k on ".liw";
	setAttr ".fbxID" 5;
createNode joint -n "Bip01_R_Calf" -p "|Obese_Sit_Talk_3|Bip01_Spine|Bip01_R_Thigh";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".t" -type "double3" 36.1527 0 0 ;
	setAttr ".s" -type "double3" 0.99999999999999989 1 0.99999999999999989 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" -0.059158000000000002 0.11988699999999999 -0.99102400000000002 0
		 -0.0080972100000000005 -0.99278699999999998 -0.119617 0 -0.99821599999999999 0.00094822899999999998 0.059701999999999998 0
		 -10.678288 0.24518599999999999 52.308996999999998 1;
	setAttr -k on ".liw";
	setAttr ".fbxID" 5;
createNode joint -n "Bip01_R_Foot" -p "|Obese_Sit_Talk_3|Bip01_Spine|Bip01_R_Thigh|Bip01_R_Calf";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".t" -type "double3" 43.2543 0 0 ;
	setAttr ".s" -type "double3" 1 1 0.99999999999999989 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" 0 0 -1 0 -0.123469 -0.99234800000000001 0 0 -0.99234800000000001 0.123469 0 0
		 -13.237124 5.4308209999999999 9.442933 1;
	setAttr -k on ".liw";
	setAttr ".fbxID" 5;
createNode joint -n "Bip01_R_Toe0" -p "|Obese_Sit_Talk_3|Bip01_Spine|Bip01_R_Thigh|Bip01_R_Calf|Bip01_R_Foot";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".t" -type "double3" 9.4714999999999989 11.2377 0 ;
	setAttr ".s" -type "double3" 0.99999999999999989 0.99999999999999989 1 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" -0.12346799999999999 -0.99234800000000001 0 0 0 0 1 0
		 -0.99234800000000001 0.12346799999999999 0 0 -14.624625999999999 -5.7209019999999997 -0.028538999999999998 1;
	setAttr -k on ".liw";
	setAttr ".fbxID" 5;
createNode joint -n "_notused_Bip01_R_Toe0Nub" -p "|Obese_Sit_Talk_3|Bip01_Spine|Bip01_R_Thigh|Bip01_R_Calf|Bip01_R_Foot|Bip01_R_Toe0";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".t" -type "double3" 7.2685 0 0 ;
	setAttr ".ssc" no;
	setAttr ".fbxID" 5;
createNode animCurveTL -n "Bip01_Spine_translateX14";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 108 ".ktv[0:107]"  0.6 -8.6861553192138672 1.4 -8.6900568008422852
		 2.2 -8.6908140182495117 3 -8.6952171325683594 3.8 -8.6962814331054687 4.6 -8.6933755874633789
		 5.4 -8.6978282928466797 6.2 -8.7106094360351562 7 -8.7231712341308594 7.8 -8.7376728057861328
		 8.6 -8.7566671371459961 9.4 -8.7706670761108398 10.2 -8.7770061492919922 11 -8.7846746444702148
		 11.8 -8.7949953079223633 12.6 -8.8018341064453125 13.4 -8.8039140701293945 14.2 -8.8024845123291016
		 15 -8.8010940551757812 15.8 -8.8048677444458008 16.6 -8.8115005493164062 17.4 -8.8147850036621094
		 18.2 -8.8134574890136719 19 -8.8071041107177734 19.8 -8.8064327239990234 20.6 -8.7987766265869141
		 21.4 -8.7964305877685547 22.2 -8.7940559387207031 23 -8.7898120880126953 23.8 -8.786198616027832
		 24.6 -8.7881011962890625 25.4 -8.7926712036132812 26.2 -8.7922582626342773 27 -8.7869710922241211
		 27.8 -8.7870140075683594 28.6 -8.7951126098632812 29.4 -8.8035459518432617 30.2 -8.8084030151367188
		 31 -8.8076639175415039 31.8 -8.8040075302124023 32.6 -8.8058023452758789 33.4 -8.8125152587890625
		 34.2 -8.8199739456176758 35 -8.8304843902587891 35.8 -8.8456268310546875 36.6 -8.8588705062866211
		 37.4 -8.8646440505981445 38.2 -8.8707828521728516 39 -8.8842649459838867 39.8 -8.9012479782104492
		 40.6 -8.9199972152709961 41.4 -8.9409141540527344 42.2 -8.9586725234985352 43 -8.9674272537231445
		 43.8 -8.9712390899658203 44.6 -8.9795570373535156 45.4 -8.9922246932983398 46.2 -9.0049943923950195
		 47 -9.0192079544067383 47.8 -9.0341529846191406 48.6 -9.0478162765502912 49.4 -9.0593414306640625
		 50.2 -9.0667505264282227 51 -9.0657291412353516 51.8 -9.0679359436035156 52.6 -9.0756340026855469
		 53.4 -9.0809526443481445 54.2 -9.0850820541381836 55 -9.0959215164184553 55.8 -9.1040821075439453
		 56.6 -9.1104869842529297 57.4 -9.1162233352661133 58.2 -9.1245908737182617 59 -9.1373844146728516
		 59.8 -9.1443357467651367 60.6 -9.1342868804931641 61.4 -9.1188325881958008 62.2 -9.1182594299316406
		 63 -9.1330766677856445 63.8 -9.1493282318115234 64.6 -9.1620054244995117 65.4 -9.1769866943359375
		 66.2 -9.1948451995849609 67 -9.2045469284057617 67.8 -9.2167463302612305 68.6 -9.2301673889160156
		 69.4 -9.2498331069946289 70.2 -9.2732925415039062 71 -9.2983999252319336 71.8 -9.3232784271240234
		 72.6 -9.3504180908203125 73.4 -9.3647317886352539 74.2 -9.3850889205932617 75 -9.4097595214843768
		 75.8 -9.4301977157592773 76.6 -9.4455480575561523 77.4 -9.4535961151123047 78.2 -9.4763774871826172
		 79 -9.500732421875 79.8 -9.5264730453491211 80.6 -9.5547504425048828 81.4 -9.5810956954956055
		 82.2 -9.60272216796875 83 -9.6239433288574219 83.8 -9.6493844985961914 84.6 -9.6812992095947266
		 85.4 -9.7192831039428711 86.2 -9.7622995376586914;
createNode animCurveTL -n "Bip01_Spine_translateY14";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 108 ".ktv[0:107]"  0.6 64.839668273925781 1.4 64.804145812988281
		 2.2 64.770370483398438 3 64.738258361816406 3.8 64.705535888671875 4.6 64.668922424316406
		 5.4 64.631362915039063 6.2 64.599197387695313 7 64.567413330078125 7.8 64.531669616699219
		 8.6 64.501739501953125 9.4 64.483810424804688 10.2 64.472640991210938 11 64.458938598632813
		 11.8 64.437232971191406 12.6 64.41168212890625 13.4 64.389801025390625 14.2 64.372589111328125
		 15 64.352531433105469 15.8 64.327262878417969 16.6 64.304664611816406 17.4 64.288536071777344
		 18.2 64.274696350097656 19 64.259696960449219 19.8 64.239730834960937 20.6 64.223625183105469
		 21.4 64.210479736328125 22.2 64.198860168457031 23 64.189186096191406 23.8 64.1805419921875
		 24.6 64.170379638671875 25.4 64.162025451660156 26.2 64.152374267578125 27 64.137710571289063
		 27.8 64.126518249511719 28.6 64.118667602539063 29.4 64.107627868652344 30.2 64.099258422851562
		 31 64.095413208007812 31.8 64.090003967285156 32.6 64.081062316894531 33.4 64.067039489746094
		 34.2 64.0506591796875 35 64.039886474609375 35.8 64.0389404296875 36.6 64.0452880859375
		 37.4 64.053474426269531 38.2 64.060653686523438 39 64.063247680664063 39.8 64.06427001953125
		 40.6 64.073883056640625 41.4 64.088600158691406 42.2 64.100151062011719 43 64.112220764160156
		 43.8 64.127853393554687 44.6 64.142173767089844 45.4 64.151924133300781 46.2 64.159866333007813
		 47 64.170097351074219 47.8 64.186180114746094 48.6 64.206184387207031 49.4 64.219398498535156
		 50.2 64.220314025878906 51 64.220863342285156 51.8 64.223396301269531 52.6 64.226661682128906
		 53.4 64.228477478027344 54.2 64.231956481933594 55 64.238075256347656 55.8 64.235763549804688
		 56.6 64.224174499511719 57.4 64.207969665527344 58.2 64.191635131835937 59 64.174606323242188
		 59.8 64.155799865722656 60.6 64.132194519042969 61.4 64.10205078125 62.2 64.068763732910156
		 63 64.0321044921875 63.8 63.990428924560547 64.6 63.953170776367188 65.4 63.924739837646477
		 66.2 63.894191741943359 67 63.857917785644531 67.8 63.817623138427734 68.6 63.780616760253906
		 69.4 63.745830535888672 70.2 63.714321136474609 71 63.690326690673821 71.8 63.677928924560547
		 72.6 63.659801483154297 73.4 63.637065887451165 74.2 63.620563507080078 75 63.60479736328125
		 75.8 63.586578369140625 76.6 63.582065582275391 77.4 63.599388122558594 78.2 63.628154754638672
		 79 63.654941558837891 79.8 63.669704437255859 80.6 63.675930023193359 81.4 63.685066223144531
		 82.2 63.706298828125 83 63.736797332763672 83.8 63.766124725341797 84.6 63.793010711669922
		 85.4 63.820850372314453 86.2 63.846466064453125;
createNode animCurveTL -n "Bip01_Spine_translateZ14";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 108 ".ktv[0:107]"  0.6 -48.297134399414063 1.4 -48.298938751220703
		 2.2 -48.300212860107422 3 -48.300788879394531 3.8 -48.301113128662109 4.6 -48.305202484130859
		 5.4 -48.310142517089837 6.2 -48.306728363037109 7 -48.296981811523438 7.8 -48.290958404541016
		 8.6 -48.293979644775391 9.4 -48.303600311279297 10.2 -48.314750671386719 11 -48.322399139404297
		 11.8 -48.323013305664062 12.6 -48.321147918701165 13.4 -48.324470520019531 14.2 -48.333076477050781
		 15 -48.3409423828125 15.8 -48.345932006835938 16.6 -48.351314544677734 17.4 -48.35760498046875
		 18.2 -48.368629455566406 19 -48.375076293945313 19.8 -48.378761291503906 20.6 -48.382942199707031
		 21.4 -48.39825439453125 22.2 -48.414192199707031 23 -48.427295684814453 23.8 -48.434154510498047
		 24.6 -48.438777923583984 25.4 -48.451656341552734 26.2 -48.468772888183594 27 -48.476726531982422
		 27.8 -48.478275299072266 28.6 -48.485073089599609 29.4 -48.500411987304688 30.2 -48.518051147460938
		 31 -48.531875610351562 31.8 -48.540702819824219 32.6 -48.544139862060547 33.4 -48.546829223632813
		 34.2 -48.556198120117188 35 -48.570816040039063 35.8 -48.582897186279297 36.6 -48.589813232421875
		 37.4 -48.599266052246094 38.2 -48.617042541503906 39 -48.635608673095703 39.8 -48.650497436523438
		 40.6 -48.665229797363281 41.4 -48.678916931152344 42.2 -48.694332122802734 43 -48.715190887451165
		 43.8 -48.737751007080078 44.6 -48.761093139648438 45.4 -48.785030364990234 46.2 -48.80694580078125
		 47 -48.826351165771491 47.8 -48.843463897705078 48.6 -48.859592437744141 49.4 -48.874610900878906
		 50.2 -48.889163970947266 51 -48.904472351074219 51.8 -48.933338165283203 52.6 -48.949855804443359
		 53.4 -48.964260101318359 54.2 -48.989353179931641 55 -49.004848480224609 55.8 -49.023593902587891
		 56.6 -49.038051605224609 57.4 -49.054630279541016 58.2 -49.068286895751953 59 -49.076885223388672
		 59.8 -49.083534240722656 60.6 -49.089481353759766 61.4 -49.092021942138672 62.2 -49.091781616210937
		 63 -49.089706420898438 63.8 -49.080829620361328 64.6 -49.067405700683594 65.4 -49.057903289794922
		 66.2 -49.051036834716797 67 -49.038631439208984 67.8 -49.023368835449219 68.6 -48.999057769775391
		 69.4 -48.981735229492195 70.2 -48.9715576171875 71 -48.965747833251953 71.8 -48.946102142333984
		 72.6 -48.924068450927734 73.4 -48.905601501464851 74.2 -48.88995361328125 75 -48.871376037597656
		 75.8 -48.854961395263672 76.6 -48.851192474365234 77.4 -48.858024597167969 78.2 -48.864738464355469
		 79 -48.873569488525391 79.8 -48.879230499267578 80.6 -48.882190704345703 81.4 -48.886184692382813
		 82.2 -48.894779205322266 83 -48.911708831787109 83.8 -48.932285308837891 84.6 -48.948429107666016
		 85.4 -48.962078094482422 86.2 -48.975399017333984;
createNode animCurveTA -n "Bip01_Spine_rotateX14";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 108 ".ktv[0:107]"  0.6 8.6723842620849609 1.4 8.7163381576538086
		 2.2 8.7625875473022461 3 8.8105144500732422 3.8 8.8601541519165039 4.6 8.9081192016601562
		 5.4 8.9490652084350586 6.2 8.9855670928955078 7 9.0231657028198242 7.8 9.0568342208862305
		 8.6 9.0834341049194336 9.4 9.1105690002441406 10.2 9.1387023925781232 11 9.1613759994506836
		 11.8 9.1817436218261719 12.6 9.2051124572753906 13.4 9.2278175354003906 14.2 9.2485427856445313
		 15 9.2704973220825195 15.8 9.2884426116943359 16.6 9.2932996749877912 17.4 9.2910757064819336
		 18.2 9.3125591278076172 19 9.3314094543457031 19.8 9.3613252639770508 20.6 9.3731040954589844
		 21.4 9.3519582748413086 22.2 9.3493585586547852 23 9.3484439849853516 23.8 9.3500299453735352
		 24.6 9.3461160659790039 25.4 9.3368215560913086 26.2 9.3233804702758789 27 9.3028507232666016
		 27.8 9.2790021896362305 28.6 9.2581119537353516 29.4 9.2359304428100586 30.2 9.2032461166381836
		 31 9.1614685058593768 31.8 9.1140356063842773 32.6 9.0595130920410156 33.4 9.0027389526367187
		 34.2 8.9476079940795916 35 8.8904685974121094 35.8 8.8269729614257812 36.6 8.7595758438110352
		 37.4 8.6969079971313477 38.2 8.6351556777954102 39 8.5628490447998047 39.8 8.4873228073120117
		 40.6 8.4198331832885742 41.4 8.3511028289794922 42.2 8.2743453979492187 43 8.199793815612793
		 43.8 8.1309099197387695 44.6 8.0641670227050781 45.4 8.0046710968017578 46.2 7.9549508094787598
		 47 7.9080710411071795 47.8 7.8614945411682129 48.6 7.8150601387023926 49.4 7.7676610946655273
		 50.2 7.7300491333007795 51 7.7068052291870117 51.8 7.6879706382751474 52.6 7.6810765266418448
		 53.4 7.679323673248291 54.2 7.6785511970520011 55 7.6801991462707528 55.8 7.6959109306335467
		 56.6 7.705718994140625 57.4 7.7233519554138175 58.2 7.7383136749267596 59 7.7482500076293945
		 59.8 7.7649016380310067 60.6 7.7940669059753427 61.4 7.8268747329711932 62.2 7.8515529632568368
		 63 7.8655385971069336 63.8 7.8734803199768093 64.6 7.8798699378967267 65.4 7.8842210769653329
		 66.2 7.881704807281495 67 7.8651118278503436 67.8 7.8535466194152832 68.6 7.845557689666748
		 69.4 7.826106071472168 70.2 7.8013648986816415 71 7.7728257179260254 71.8 7.7301278114318848
		 72.6 7.7132978439331055 73.4 7.6544661521911621 74.2 7.6272616386413556 75 7.5995278358459455
		 75.8 7.5374951362609863 76.6 7.5213713645935059 77.4 7.4991445541381827 78.2 7.471566677093505
		 79 7.4265136718750018 79.8 7.3705573081970206 80.6 7.3117818832397461 81.4 7.2498912811279297
		 82.2 7.1849102973937979 83 7.1199355125427246 83.8 7.0555157661437988 84.6 6.9912843704223633
		 85.4 6.923640251159668 86.2 6.8285965919494629;
createNode animCurveTA -n "Bip01_Spine_rotateY14";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 108 ".ktv[0:107]"  0.6 8.60833740234375 1.4 8.5212202072143555
		 2.2 8.4335031509399414 3 8.3487129211425781 3.8 8.2663640975952148 4.6 8.1789340972900391
		 5.4 8.0799131393432617 6.2 7.9723629951477069 7 7.8680496215820321 7.8 7.7696599960327157
		 8.6 7.6682991981506348 9.4 7.568396568298339 10.2 7.4821362495422372 11 7.4029293060302717
		 11.8 7.320271968841551 12.6 7.2392787933349609 13.4 7.1703014373779306 14.2 7.1190652847290048
		 15 7.0774054527282715 15.8 7.0338373184204102 16.6 6.9969844818115234 17.4 6.9782123565673828
		 18.2 6.9697127342224121 19 6.9660201072692871 19.8 6.9982504844665527 20.6 7.0089254379272461
		 21.4 7.041252613067627 22.2 7.0790324211120605 23 7.1322231292724609 23.8 7.1910576820373535
		 24.6 7.2545456886291504 25.4 7.3268833160400382 26.2 7.406819820404051 27 7.4918546676635742
		 27.8 7.5835108757019052 28.6 7.6850314140319824 29.4 7.788477897644043 30.2 7.8820590972900382
		 31 7.9737577438354492 31.8 8.0740337371826172 32.6 8.1814546585083008 33.4 8.2939491271972656
		 34.2 8.4047908782958984 35 8.5067577362060547 35.8 8.6019401550292969 36.6 8.6967830657958984
		 37.4 8.7931327819824237 38.2 8.8861780166625977 39 8.971435546875 39.8 9.0466365814208984
		 40.6 9.1082124710083008 41.4 9.1541481018066406 42.2 9.1910543441772461 43 9.2279129028320312
		 43.8 9.2615928649902344 44.6 9.2846555709838867 45.4 9.2995910644531232 46.2 9.3085050582885742
		 47 9.3094072341918945 47.8 9.3058967590332031 48.6 9.2942085266113281 49.4 9.2678194046020508
		 50.2 9.2412939071655273 51 9.2022180557250977 51.8 9.1723184585571289 52.6 9.1550531387329102
		 53.4 9.1262035369873065 54.2 9.0763807296752912 55 9.0364446640014648 55.8 8.9910087585449219
		 56.6 8.9506673812866211 57.4 8.9070558547973633 58.2 8.8620395660400391 59 8.8123579025268555
		 59.8 8.7607574462890625 60.6 8.7171697616577148 61.4 8.6810388565063477 62.2 8.6453037261962908
		 63 8.6066818237304687 63.8 8.5649662017822266 64.6 8.5210437774658203 65.4 8.4757099151611328
		 66.2 8.4338283538818359 67 8.3782691955566406 67.8 8.3489599227905273 68.6 8.3204307556152344
		 69.4 8.2955217361450195 70.2 8.2716817855834961 71 8.2506618499755859 71.8 8.2233343124389648
		 72.6 8.2086019515991211 73.4 8.1816778182983398 74.2 8.1771221160888672 75 8.1736879348754883
		 75.8 8.1729154586791992 76.6 8.1704292297363281 77.4 8.1367912292480469 78.2 8.1365137100219727
		 79 8.1304121017456055 79.8 8.1206321716308594 80.6 8.1084957122802734 81.4 8.0930385589599609
		 82.2 8.0775241851806641 83 8.0635652542114258 83.8 8.0476217269897461 84.6 8.0273942947387695
		 85.4 7.9981517791748047 86.2 7.938528537750245;
createNode animCurveTA -n "Bip01_Spine_rotateZ14";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 108 ".ktv[0:107]"  0.6 -0.38985109329223633 1.4 -0.42615211009979248
		 2.2 -0.46174836158752441 3 -0.49572196602821361 3.8 -0.52054041624069214 4.6 -0.53318870067596436
		 5.4 -0.5436515212059021 6.2 -0.56086176633834839 7 -0.58213609457015991 7.8 -0.60268419981002808
		 8.6 -0.6193806529045105 9.4 -0.63216328620910645 10.2 -0.64560312032699585 11 -0.65911531448364258
		 11.8 -0.66932660341262828 12.6 -0.67685431241989136 13.4 -0.6829833984375 14.2 -0.68931972980499268
		 15 -0.693323314189911 15.8 -0.68962597846984863 16.6 -0.68222218751907349 17.4 -0.67779439687728882
		 18.2 -0.69109749794006359 19 -0.68765681982040405 19.8 -0.69071429967880249 20.6 -0.69447237253189087
		 21.4 -0.71170330047607422 22.2 -0.71591091156005859 23 -0.72170507907867421 23.8 -0.73659026622772228
		 24.6 -0.75794869661331188 25.4 -0.77981644868850708 26.2 -0.80260276794433616 27 -0.82685661315917969
		 27.8 -0.85373938083648693 28.6 -0.88298386335372925 29.4 -0.91519802808761597 30.2 -0.95516562461853038
		 31 -1.0001462697982788 31.8 -1.0405416488647461 32.6 -1.0737184286117554 33.4 -1.1077630519866943
		 34.2 -1.1515321731567385 35 -1.2033976316452026 35.8 -1.2596524953842163 36.6 -1.3228260278701782
		 37.4 -1.3890378475189209 38.2 -1.4503198862075806 39 -1.509311318397522 39.8 -1.5722208023071289
		 40.6 -1.6407904624938965 41.4 -1.7152944803237915 42.2 -1.7917875051498413 43 -1.8604415655136108
		 43.8 -1.9181306362152095 44.6 -1.9713459014892576 45.4 -2.0226821899414062 46.2 -2.0736112594604492
		 47 -2.1281366348266602 47.8 -2.1853854656219478 48.6 -2.2428934574127197 49.4 -2.2936637401580811
		 50.2 -2.3247992992401123 51 -2.322443962097168 51.8 -2.3581740856170654 52.6 -2.3633446693420415
		 53.4 -2.366276741027832 54.2 -2.4002587795257568 55 -2.3974502086639404 55.8 -2.4105463027954106
		 56.6 -2.3903713226318359 57.4 -2.3651986122131348 58.2 -2.3451368808746338 59 -2.3269674777984619
		 59.8 -2.2960538864135742 60.6 -2.250497579574585 61.4 -2.2080206871032715 62.2 -2.1782233715057373
		 63 -2.1482620239257812 63.8 -2.1076931953430176 64.6 -2.0641040802001953 65.4 -2.026653528213501
		 66.2 -1.9968107938766473 67 -1.9652117490768437 67.8 -1.947846531867981 68.6 -1.9118309020996092
		 69.4 -1.8932948112487793 70.2 -1.8680822849273679 71 -1.849506735801697 71.8 -1.818265438079834
		 72.6 -1.8229976892471309 73.4 -1.8323817253112791 74.2 -1.8326698541641229 75 -1.8355214595794682
		 75.8 -1.8369009494781492 76.6 -1.8420319557189944 77.4 -1.8681215047836306 78.2 -1.8982752561569214
		 79 -1.9453709125518797 79.8 -1.995836615562439 80.6 -2.0429308414459229 81.4 -2.0991990566253662
		 82.2 -2.1753239631652832 83 -2.2609126567840576 83.8 -2.3451497554779053 84.6 -2.428849458694458
		 85.4 -2.5163092613220215 86.2 -2.638718843460083;
createNode animCurveTA -n "Bip01_Spine1_rotateX14";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 108 ".ktv[0:107]"  0.6 0.11039955914020538 1.4 0.098311655223369598
		 2.2 0.094726420938968658 3 0.083058610558509827 3.8 0.073445290327072144 4.6 0.064204655587673187
		 5.4 0.053659025579690933 6.2 0.050580102950334549 7 0.04064473882317543 7.8 0.032201420515775681
		 8.6 0.02291982993483543 9.4 0.018709076568484303 10.2 0.01417172607034445 11 0.0040806727483868599
		 11.8 0.00016830867389217019 12.6 -0.0032770195975899696 13.4 -0.0097438255324959755
		 14.2 -0.014475682750344275 15 -0.018564552068710327 15.8 -0.022449258714914318 16.6 -0.026693537831306458
		 17.4 -0.028468156233429909 18.2 -0.030317516997456551 19 -0.033855970948934555 19.8 -0.034688673913478851
		 20.6 -0.036261316388845437 21.4 -0.036725796759128571 22.2 -0.036606807261705399
		 23 -0.035503279417753227 23.8 -0.03348986804485321 24.6 -0.029843686148524284 25.4 -0.028408955782651891
		 26.2 -0.022455839440226555 27 -0.016119681298732758 27.8 -0.0083694839850068092 28.6 0.0029380482155829668
		 29.4 0.0092032877728342993 30.2 0.016449268907308578 31 0.03604806587100029 31.8 0.045253824442625046
		 32.6 0.053930085152387619 33.4 0.07474314421415329 34.2 0.096330881118774414 35 0.10283775627613069
		 35.8 0.12656097114086151 36.6 0.15261872112751007 37.4 0.16497418284416199 38.2 0.17873542010784149
		 39 0.21234767138957977 39.8 0.22811421751976013 40.6 0.24797813594341278 41.4 0.26925942301750183
		 42.2 0.28925731778144836 43 0.30393147468566895 43.8 0.33292770385742188 44.6 0.3600488007068634
		 45.4 0.36763620376586914 46.2 0.39223924279212952 47 0.41336509585380554 47.8 0.43555909395217896
		 48.6 0.44282734394073486 49.4 0.46656170487403875 50.2 0.47308832406997664 51 0.49067851901054382
		 51.8 0.50613677501678467 52.6 0.51008057594299328 53.4 0.52193194627761841 54.2 0.53056824207305908
		 55 0.53769975900650024 55.8 0.54480397701263428 56.6 0.54762017726898193 57.4 0.55084848403930664
		 58.2 0.55323469638824463 59 0.55693608522415161 59.8 0.55789226293563843 60.6 0.56029343605041504
		 61.4 0.56134533882141113 62.2 0.56269252300262451 63 0.56432157754898071 63.8 0.56619751453399658
		 64.6 0.56837362051010132 65.4 0.57082325220108032 66.2 0.57360571622848511 67 0.57680654525756836
		 67.8 0.58048146963119507 68.6 0.58466589450836182 69.4 0.58932477235794078 70.2 0.59442132711410522
		 71 0.59987664222717285 71.8 0.605640709400177 72.6 0.61163789033889771 73.4 0.61782729625701904
		 74.2 0.62421518564224243 75 0.63085335493087769 75.8 0.6377982497215271 76.6 0.64503622055053722
		 77.4 0.65255063772201538 78.2 0.66031354665756237 79 0.66836869716644287 79.8 0.67684602737426758
		 80.6 0.68519860506057739 81.4 0.69223165512084961 82.2 0.70884746313095093 83 0.71732413768768311
		 83.8 0.72865784168243408 84.6 0.74169754981994629 85.4 0.75588071346282959 86.2 0.77122455835342396;
createNode animCurveTA -n "Bip01_Spine1_rotateY14";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 108 ".ktv[0:107]"  0.6 -0.90043222904205344 1.4 -0.92126727104187012
		 2.2 -0.92715233564376831 3 -0.94537186622619629 3.8 -0.95911878347396851 4.6 -0.97082221508026134
		 5.4 -0.98249918222427346 6.2 -0.98555153608322144 7 -0.99422544240951516 7.8 -0.99989801645278931
		 8.6 -1.0039194822311399 9.4 -1.0048203468322754 10.2 -1.0054024457931521 11 -1.0040701627731323
		 11.8 -1.0024240016937256 12.6 -1.0005735158920288 13.4 -0.99498945474624634 14.2 -0.98908281326293968
		 15 -0.982338547706604 15.8 -0.97392046451568604 16.6 -0.96186357736587536 17.4 -0.95538514852523804
		 18.2 -0.94796156883239724 19 -0.92926031351089478 19.8 -0.92364311218261708 20.6 -0.90509361028671276
		 21.4 -0.88484597206115723 22.2 -0.87870508432388306 23 -0.85723060369491588 23.8 -0.83657932281494141
		 24.6 -0.812269628047943 25.4 -0.80473273992538452 26.2 -0.77892273664474476 27 -0.75625795125961315
		 27.8 -0.73307085037231445 28.6 -0.70418024063110352 29.4 -0.69038033485412598 30.2 -0.67524987459182739
		 31 -0.63979768753051758 31.8 -0.62514936923980713 32.6 -0.61194252967834473 33.4 -0.58405154943466187
		 34.2 -0.55885583162307739 35 -0.5519823431968689 35.8 -0.5289757251739502 36.6 -0.50624781847000122
		 37.4 -0.4965062141418457 38.2 -0.4860278964042663 39 -0.46293124556541448 39.8 -0.4530470073223114
		 40.6 -0.44126525521278381 41.4 -0.429401844739914 42.2 -0.4189770519733429 43 -0.41168513894081121
		 43.8 -0.39882287383079529 44.6 -0.38812738656997681 45.4 -0.38540947437286377 46.2 -0.37758505344390875
		 47 -0.37248298525810242 47.8 -0.36917722225189209 48.6 -0.36890602111816406 49.4 -0.37010270357131958
		 50.2 -0.37110990285873424 51 -0.3774401843547821 51.8 -0.38732624053955078 52.6 -0.39097458124160767
		 53.4 -0.40576061606407166 54.2 -0.42147940397262573 55 -0.44077152013778687 55.8 -0.46877521276473993
		 56.6 -0.48578825592994695 57.4 -0.50850248336791992 58.2 -0.529288649559021 59 -0.57771831750869751
		 59.8 -0.59455603361129761 60.6 -0.65114951133728027 61.4 -0.68027216196060181 62.2 -0.71810609102249146
		 63 -0.75990623235702515 63.8 -0.80323958396911621 64.6 -0.84790116548538208 65.4 -0.89370113611221313
		 66.2 -0.94036960601806641 67 -0.98762995004653931 67.8 -1.0352178812026978 68.6 -1.0828766822814939
		 69.4 -1.1303108930587769 70.2 -1.1771835088729858 71 -1.22312331199646 71.8 -1.2677443027496338
		 72.6 -1.3106914758682251 73.4 -1.3516470193862915 74.2 -1.390289306640625 75 -1.4263350963592527
		 75.8 -1.4594423770904541 76.6 -1.4892823696136477 77.4 -1.5154945850372314 78.2 -1.537874698638916
		 79 -1.5562773942947388 79.8 -1.5705342292785645 80.6 -1.5804146528244021 81.4 -1.5856560468673708
		 82.2 -1.5870178937911987 83 -1.58294677734375 83.8 -1.5743826627731323 84.6 -1.5613430738449097
		 85.4 -1.5439205169677734 86.2 -1.5222333669662476;
createNode animCurveTA -n "Bip01_Spine1_rotateZ14";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 108 ".ktv[0:107]"  0.6 0.75359612703323364 1.4 0.7504081130027771
		 2.2 0.74939262866973877 3 0.74576050043106079 3.8 0.74228256940841675 4.6 0.73822319507598888
		 5.4 0.7328467369079591 6.2 0.73111885786056507 7 0.72504442930221547 7.8 0.71920377016067516
		 8.6 0.71217948198318481 9.4 0.70879942178726196 10.2 0.70508724451065063 11 0.69622981548309326
		 11.8 0.69255846738815308 12.6 0.68924915790557861 13.4 0.682758629322052 14.2 0.67801564931869507
		 15 0.67412781715393066 15.8 0.67077654600143433 16.6 0.66781121492385864 17.4 0.66695708036422729
		 18.2 0.66627830266952515 19 0.66630321741104126 19.8 0.66665256023406982 20.6 0.66932296752929688
		 21.4 0.67369335889816295 22.2 0.67533421516418457 23 0.68196243047714233 23.8 0.689430832862854
		 24.6 0.69948655366897583 25.4 0.70286417007446289 26.2 0.71531271934509277 27 0.7271614670753479
		 27.8 0.74037748575210571 28.6 0.75821900367736816 29.4 0.76741534471511841 30.2 0.77782660722732544
		 31 0.80422461032867421 31.8 0.81604814529418945 32.6 0.8269825577735902 33.4 0.85219228267669678
		 34.2 0.87728005647659291 35 0.88465362787246704 35.8 0.91110628843307484 36.6 0.939605712890625
		 37.4 0.95277184247970581 38.2 0.96727180480957009 39 1.001655101776123 39.8 1.017447829246521
		 40.6 1.0370688438415527 41.4 1.0577740669250488 42.2 1.0769196748733521 43 1.0907971858978271
		 43.8 1.1173920631408691 44.6 1.1414005756378174 45.4 1.1479146480560305 46.2 1.168460488319397
		 47 1.1852046251296997 47.8 1.2017942667007446 48.6 1.206928014755249 49.4 1.2229053974151611
		 50.2 1.2270594835281372 51 1.2369266748428345 51.8 1.2441377639770508 52.6 1.2456451654434204
		 53.4 1.2491956949234009 54.2 1.2506715059280396 55 1.250485897064209 55.8 1.248631477355957
		 56.6 1.2469707727432251 57.4 1.2446334362030027 58.2 1.2424424886703491 59 1.2375253438949585
		 59.8 1.2361119985580444 60.6 1.2324457168579102 61.4 1.2314736843109133 62.2 1.2310758829116819
		 63 1.2317600250244141 63.8 1.2339210510253906 64.6 1.2376987934112549 65.4 1.2432756423950195
		 66.2 1.2507623434066772 67 1.2602920532226565 67.8 1.2719615697860718 68.6 1.2857780456542969
		 69.4 1.3017024993896484 70.2 1.3196611404418945 71 1.3396116495132446 71.8 1.3615084886550903
		 72.6 1.3852894306182859 73.4 1.4108610153198242 74.2 1.4380971193313601 75 1.4669111967086792
		 75.8 1.4971456527709961 76.6 1.5286661386489868 77.4 1.5612874031066897 78.2 1.5948864221572876
		 79 1.629315972328186 79.8 1.6644464731216433 80.6 1.6978632211685181 81.4 1.7248706817626951
		 82.2 1.7838878631591797 83 1.8110288381576538 83.8 1.8447788953781128 84.6 1.8804820775985722
		 85.4 1.9157148599624636 86.2 1.9503012895584109;
createNode animCurveTA -n "Bip01_Spine2_rotateX14";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 108 ".ktv[0:107]"  0.6 0.32430967688560486 1.4 0.29139804840087891
		 2.2 0.2590949535369873 3 0.22762349247932431 3.8 0.19717016816139221 4.6 0.16768549382686615
		 5.4 0.13921287655830383 6.2 0.1117735430598259 7 0.087014973163604736 7.8 0.067984901368618011
		 8.6 0.02942143194377422 9.4 0.012743641622364519 10.2 -0.0060642445459961891 11 -0.020492691546678543
		 11.8 -0.046868741512298584 12.6 -0.070025749504566193 13.4 -0.079331070184707642
		 14.2 -0.088481120765209198 15 -0.10668254643678664 15.8 -0.12259657680988312 16.6 -0.12939691543579102
		 17.4 -0.1380598396062851 18.2 -0.14625473320484161 19 -0.15307579934597015 19.8 -0.15823768079280853
		 20.6 -0.16137853264808658 21.4 -0.16226264834403992 22.2 -0.16076920926570895 23 -0.15681511163711548
		 23.8 -0.15018317103385925 24.6 -0.14045259356498718 25.4 -0.1271706223487854 26.2 -0.11013536900281906
		 27 -0.089394353330135345 27.8 -0.065010137856006622 28.6 -0.036815658211708069 29.4 -0.0046043065376579762
		 30.2 0.031816553324460983 31 0.07237591594457625 31.8 0.11681745201349258 32.6 0.1648392379283905
		 33.4 0.21630956232547763 34.2 0.27109917998313904 35 0.32900190353393555 35.8 0.38964226841926575
		 36.6 0.45268437266349781 37.4 0.51796460151672363 38.2 0.5853232741355896 39 0.65434128046035767
		 39.8 0.72443068027496338 40.6 0.79517704248428356 41.4 0.86628961563110352 42.2 0.93751639127731323
		 43 1.0084422826766968 43.8 1.0786226987838743 44.6 1.1475622653961182 45.4 1.2147789001464844
		 46.2 1.2798087596893313 47 1.342262864112854 47.8 1.4017380475997927 48.6 1.4578335285186768
		 49.4 1.5100858211517334 50.2 1.5581353902816772 51 1.6017025709152222 51.8 1.6406208276748655
		 52.6 1.6749017238616946 53.4 1.7046533823013306 54.2 1.7301071882247925 55 1.751455545425415
		 55.8 1.7688999176025393 56.6 1.7826302051544187 57.4 1.7930157184600832 58.2 1.8006232976913452
		 59 1.8060503005981448 59.8 1.8098015785217287 60.6 1.8122708797454832 61.4 1.8139935731887815
		 62.2 1.8155812025070193 63 1.8175444602966302 63.8 1.8201785087585449 64.6 1.8235509395599367
		 65.4 1.8276724815368652 66.2 1.832649827003479 67 1.8387711048126221 67.8 1.8462531566619873
		 68.6 1.8552013635635378 69.4 1.8655314445495605 70.2 1.877068042755127 71 1.8896142244338991
		 71.8 1.9029990434646609 72.6 1.917039275169373 73.4 1.9315959215164185 74.2 1.9467312097549443
		 75 1.9626632928848269 75.8 1.9796206951141357 76.6 1.9976334571838377 77.4 2.0166406631469727
		 78.2 2.0366661548614502 79 2.0579426288604741 79.8 2.080864429473877 80.6 2.1058652400970459
		 81.4 2.1332564353942871 82.2 2.1633427143096924 83 2.1965920925140381 83.8 2.2335429191589355
		 84.6 2.274733304977417 85.4 2.3204107284545894 86.2 2.3706057071685791;
createNode animCurveTA -n "Bip01_Spine2_rotateY14";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 108 ".ktv[0:107]"  0.6 -3.0013082027435303 1.4 -3.0569908618927002
		 2.2 -3.108532190322876 3 -3.1554741859436035 3.8 -3.1974802017211914 4.6 -3.234367847442627
		 5.4 -3.2661399841308594 6.2 -3.2928545475006104 7 -3.313776969909668 7.8 -3.32745361328125
		 8.6 -3.3458681106567383 9.4 -3.3495016098022461 10.2 -3.3509342670440674 11 -3.3505823612213135
		 11.8 -3.3413550853729248 12.6 -3.3243894577026367 13.4 -3.3137700557708745 14.2 -3.3027093410491943
		 15 -3.2724366188049316 15.8 -3.2355625629425049 16.6 -3.2138030529022217 17.4 -3.1821842193603516
		 18.2 -3.14542555809021 19 -3.1051235198974609 19.8 -3.0612277984619141 20.6 -3.0137205123901367
		 21.4 -2.962660551071167 22.2 -2.9081430435180664 23 -2.8502974510192871 23.8 -2.7892534732818604
		 24.6 -2.725257396697998 25.4 -2.658632755279541 26.2 -2.5897278785705566 27 -2.5189859867095947
		 27.8 -2.4469699859619141 28.6 -2.3742728233337398 29.4 -2.3013749122619629 30.2 -2.2287015914916992
		 31 -2.1568074226379395 31.8 -2.086296558380127 32.6 -2.0177562236785889 33.4 -1.951717734336853
		 34.2 -1.8885754346847532 35 -1.8285437822341921 35.8 -1.7715252637863159 36.6 -1.7173212766647341
		 37.4 -1.6658370494842529 38.2 -1.6171935796737671 39 -1.5714131593704224 39.8 -1.5283790826797483
		 40.6 -1.4879789352416992 41.4 -1.4503133296966553 42.2 -1.4155527353286743 43 -1.3838305473327637
		 43.8 -1.3552176952362061 44.6 -1.3298392295837402 45.4 -1.3080130815505979 46.2 -1.2902473211288452
		 47 -1.2771434783935549 47.8 -1.2692968845367432 48.6 -1.2672871351242063 49.4 -1.2716741561889648
		 50.2 -1.2828205823898315 51 -1.3010300397872925 51.8 -1.3267267942428591 52.6 -1.3604104518890381
		 53.4 -1.4026056528091433 54.2 -1.4536086320877075 55 -1.513575553894043 55.8 -1.5824660062789917
		 56.6 -1.6602554321289065 57.4 -1.7468702793121338 58.2 -1.8421939611434937 59 -1.9460704326629643
		 59.8 -2.0581789016723633 60.6 -2.178053617477417 61.4 -2.3051092624664307 62.2 -2.4386477470397954
		 63 -2.5779993534088135 63.8 -2.7224862575531006 64.6 -2.871504545211792 65.4 -3.0243813991546631
		 66.2 -3.1802713871002197 67 -3.3382618427276611 67.8 -3.4974610805511475 68.6 -3.6570162773132324
		 69.4 -3.8159675598144527 70.2 -3.9731981754302974 71 -4.12744140625 71.8 -4.2774443626403809
		 72.6 -4.4219894409179687 73.4 -4.5599732398986816 74.2 -4.6903901100158691 75 -4.8122520446777344
		 75.8 -4.924497127532959 76.6 -5.0259122848510742 77.4 -5.1154050827026367 78.2 -5.1922111511230469
		 79 -5.2558426856994629 79.8 -5.3058013916015625 80.6 -5.3414921760559082 81.4 -5.3623428344726562
		 82.2 -5.3679428100585938 83 -5.3580718040466309 83.8 -5.3328628540039062 84.6 -5.2927322387695312
		 85.4 -5.2381668090820313 86.2 -5.1696071624755859;
createNode animCurveTA -n "Bip01_Spine2_rotateZ14";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 108 ".ktv[0:107]"  0.6 2.5072140693664551 1.4 2.499351978302002
		 2.2 2.4907777309417725 3 2.4813117980957031 3.8 2.4706718921661377 4.6 2.4586505889892578
		 5.4 2.4452142715454106 6.2 2.43050217628479 7 2.415785551071167 7.8 2.4034707546234131
		 8.6 2.3754496574401855 9.4 2.3623080253601074 10.2 2.3468506336212163 11 2.3346459865570068
		 11.8 2.310391902923584 12.6 2.2873804569244385 13.4 2.2778022289276123 14.2 2.2684111595153809
		 15 2.2505753040313725 15.8 2.2366728782653809 16.6 2.2320694923400879 17.4 2.2272918224334717
		 18.2 2.2246794700622559 19 2.2251331806182861 19.8 2.2288558483123779 20.6 2.2360060214996338
		 21.4 2.2467615604400635 22.2 2.2612497806549072 23 2.2794909477233887 23.8 2.3014900684356689
		 24.6 2.3273193836212158 25.4 2.357029914855957 26.2 2.3905332088470459 27 2.4276590347290039
		 27.8 2.4682173728942871 28.6 2.5121219158172607 29.4 2.5593671798706055 30.2 2.6098687648773193
		 31 2.6634001731872559 31.8 2.7196254730224609 32.6 2.7782828807830811 33.4 2.8391680717468266
		 34.2 2.9021098613739018 35 2.9670090675354004 35.8 3.0336921215057373 36.6 3.1018624305725098
		 37.4 3.1710805892944336 38.2 3.2408728599548344 39 3.3108224868774414 39.8 3.3806226253509521
		 40.6 3.4499602317810059 41.4 3.5185413360595703 42.2 3.585917472839355 43 3.6516251564025879
		 43.8 3.7151041030883798 44.6 3.775790691375732 45.4 3.8331100940704346 46.2 3.886547327041626
		 47 3.935749053955079 47.8 3.9804487228393555 48.6 4.0204095840454102 49.4 4.0553712844848633
		 50.2 4.0849666595458984 51 4.1089687347412109 51.8 4.1273822784423828 52.6 4.1404166221618652
		 53.4 4.1484737396240234 54.2 4.1519436836242676 55 4.1512408256530762 55.8 4.1468334197998047
		 56.6 4.1393828392028809 57.4 4.1296696662902832 58.2 4.1186189651489258 59 4.1072649955749512
		 59.8 4.0965819358825692 60.6 4.0874919891357422 61.4 4.0808873176574707 62.2 4.0775809288024902
		 63 4.0783977508544922 63.8 4.084014892578125 64.6 4.0950121879577637 65.4 4.1118812561035156
		 66.2 4.1351146697998047 67 4.1651244163513184 67.8 4.202202320098877 68.6 4.2464103698730469
		 69.4 4.2975878715515137 70.2 4.3555579185485849 71 4.4201631546020508 71.8 4.4913058280944824
		 72.6 4.5687704086303711 73.4 4.6522026062011719 74.2 4.7412896156311035 75 4.8356685638427734
		 75.8 4.9349021911621094 76.6 5.0384511947631836 77.4 5.1458339691162109 78.2 5.2565498352050781
		 79 5.3701157569885263 79.8 5.4861207008361816 80.6 5.6041278839111328 81.4 5.7235217094421387
		 82.2 5.8436150550842285 83 5.9636468887329102 83.8 6.0830745697021484 84.6 6.2014274597167969
		 85.4 6.3182663917541504 86.2 6.433016300201416;
createNode animCurveTA -n "Bip01_R_Clavicle_rotateX14";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 108 ".ktv[0:107]"  0.6 -0.87503057718276978 1.4 -0.87606334686279297
		 2.2 -0.87645810842514027 3 -0.87791490554809581 3.8 -0.87972462177276611 4.6 -0.88030874729156494
		 5.4 -0.88230979442596436 6.2 -0.88420617580413829 7 -0.88663500547409058 7.8 -0.88743335008621216
		 8.6 -0.89007788896560669 9.4 -0.89252668619155884 10.2 -0.89544779062271118 11 -0.89636337757110585
		 11.8 -0.89943480491638172 12.6 -0.901985764503479 13.4 -0.90445178747177135 14.2 -0.90729838609695435
		 15 -0.90815621614456177 15.8 -0.91083234548568714 16.6 -0.91292589902877808 17.4 -0.91477030515670787
		 18.2 -0.91641414165496815 19 -0.91810429096221924 19.8 -0.91850018501281749 20.6 -0.91962867975234974
		 21.4 -0.92023622989654541 22.2 -0.92034220695495617 23 -0.91979229450225841 23.8 -0.91950041055679343
		 24.6 -0.91813337802886974 25.4 -0.91641092300415039 26.2 -0.91431492567062389 27 -0.91160929203033425
		 27.8 -0.90846741199493408 28.6 -0.90478026866912864 29.4 -0.90049958229064964 30.2 -0.89565747976303112
		 31 -0.89028525352478027 31.8 -0.8844258189201355 32.6 -0.87783604860305797 33.4 -0.87060606479644764
		 34.2 -0.86286842823028564 35 -0.85451328754425071 35.8 -0.84501177072525024 36.6 -0.83311611413955677
		 37.4 -0.82870733737945557 38.2 -0.81310409307479858 39 -0.80845052003860485 39.8 -0.79449921846389771
		 40.6 -0.78221786022186268 41.4 -0.77046054601669323 42.2 -0.75761872529983521 43 -0.74230921268463146
		 43.8 -0.73771888017654419 44.6 -0.72168081998825073 45.4 -0.7060127854347229 46.2 -0.70098310708999634
		 47 -0.68411803245544434 47.8 -0.67875665426254272 48.6 -0.66372072696685791 49.4 -0.65957313776016235
		 50.2 -0.64887493848800659 51 -0.64083749055862427 51.8 -0.63431686162948608 52.6 -0.6282343864440918
		 53.4 -0.62670594453811646 54.2 -0.62296164035797119 55 -0.62105423212051392 55.8 -0.62076276540756226
		 56.6 -0.62103337049484253 57.4 -0.62302923202514648 58.2 -0.6264989972114563 59 -0.62770134210586548
		 59.8 -0.63238674402236938 60.6 -0.63688308000564575 61.4 -0.64163225889205933 62.2 -0.64709776639938354
		 63 -0.64868557453155518 63.8 -0.65344136953353882 64.6 -0.65675580501556396 65.4 -0.65903830528259277
		 66.2 -0.66029161214828491 67 -0.6603894829750061 67.8 -0.65920674800872803 68.6 -0.65661108493804932
		 69.4 -0.65248566865921021 70.2 -0.64642125368118286 71 -0.63724935054779053 71.8 -0.63414758443832397
		 72.6 -0.62170183658599854 73.4 -0.60653012990951538 74.2 -0.59884846210479736 75 -0.59102475643157959
		 75.8 -0.57262653112411499 76.6 -0.55307173728942871 77.4 -0.54393672943115234 78.2 -0.53477543592453003
		 79 -0.51463615894317627 79.8 -0.4955702126026153 80.6 -0.48934310674667358 81.4 -0.46851718425750727
		 82.2 -0.46192738413810719 83 -0.44278413057327271 83.8 -0.43673914670944214 84.6 -0.41970682144165039
		 85.4 -0.41491860151290894 86.2 -0.40238380432128906;
createNode animCurveTA -n "Bip01_R_Clavicle_rotateY14";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 108 ".ktv[0:107]"  0.6 81.516418457031264 1.4 81.518348693847642
		 2.2 81.519088745117188 3 81.522003173828125 3.8 81.525520324707017 4.6 81.526626586914063
		 5.4 81.530517578125 6.2 81.5343017578125 7 81.53887939453125 7.8 81.54032135009767
		 8.6 81.545402526855469 9.4 81.55029296875 10.2 81.555999755859375 11 81.557693481445313
		 11.8 81.563552856445313 12.6 81.568458557128906 13.4 81.573226928710938 14.2 81.578605651855469
		 15 81.580276489257813 15.8 81.585433959960937 16.6 81.589546203613281 17.4 81.592994689941406
		 18.2 81.596221923828125 19 81.599403381347642 19.8 81.600242614746094 20.6 81.602348327636719
		 21.4 81.603553771972656 22.2 81.603820800781264 23 81.602760314941406 23.8 81.602188110351563
		 24.6 81.599449157714844 25.4 81.59625244140625 26.2 81.59207916259767 27 81.587028503417969
		 27.8 81.580947875976577 28.6 81.573860168457017 29.4 81.565620422363281 30.2 81.556350708007812
		 31 81.545997619628906 31.8 81.534469604492188 32.6 81.52178955078125 33.4 81.507881164550781
		 34.2 81.492927551269531 35 81.476776123046875 35.8 81.458320617675781 36.6 81.435218811035156
		 37.4 81.426689147949219 38.2 81.39646148681642 39 81.387344360351562 39.8 81.36029052734375
		 40.6 81.336441040039063 41.4 81.31329345703125 42.2 81.288238525390625 43 81.258354187011719
		 43.8 81.249366760253906 44.6 81.21795654296875 45.4 81.187202453613281 46.2 81.177291870117188
		 47 81.144218444824219 47.8 81.133712768554688 48.6 81.104080200195313 49.4 81.095932006835938
		 50.2 81.07476806640625 51 81.058998107910156 51.8 81.046028137207017 52.6 81.033988952636733
		 53.4 81.03106689453125 54.2 81.023513793945313 55 81.019859313964844 55.8 81.019279479980469
		 56.6 81.019752502441406 57.4 81.023666381835938 58.2 81.030479431152358 59 81.032974243164062
		 59.8 81.042266845703125 60.6 81.051124572753906 61.4 81.060440063476562 62.2 81.071281433105469
		 63 81.074409484863281 63.8 81.08380126953125 64.6 81.090400695800781 65.4 81.0948486328125
		 66.2 81.097267150878906 67 81.097511291503906 67.8 81.09521484375 68.6 81.090087890625
		 69.4 81.082023620605469 70.2 81.069923400878906 71 81.051895141601563 71.8 81.045677185058594
		 72.6 81.020980834960938 73.4 80.991065979003906 74.2 80.975852966308594 75 80.960456848144531
		 75.8 80.923820495605469 76.6 80.885009765625 77.4 80.86688232421875 78.2 80.84869384765625
		 79 80.80841064453125 79.8 80.770355224609375 80.6 80.757949829101563 81.4 80.71636962890625
		 82.2 80.703041076660156 83 80.66475677490233 83.8 80.652633666992188 84.6 80.618331909179673
		 85.4 80.608787536621094 86.2 80.583442687988281;
createNode animCurveTA -n "Bip01_R_Clavicle_rotateZ14";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 108 ".ktv[0:107]"  0.6 174.00935363769531 1.4 174.00907897949222
		 2.2 174.00895690917969 3 174.00860595703128 3.8 174.00813293457031 4.6 174.00796508789068
		 5.4 174.0074462890625 6.2 174.00700378417969 7 174.00630187988281 7.8 174.00607299804687
		 8.6 174.00534057617187 9.4 174.0047607421875 10.2 174.00399780273435 11 174.00373840332031
		 11.8 174.00289916992187 12.6 174.002197265625 13.4 174.00155639648435 14.2 174.00076293945312
		 15 174.00053405761719 15.8 173.99983215332031 16.6 173.99929809570312 17.4 173.9987487792969
		 18.2 173.99835205078125 19 173.99786376953125 19.8 173.99777221679687 20.6 173.99746704101562
		 21.4 173.99729919433594 22.2 173.99729919433594 23 173.99745178222656 23.8 173.99754333496094
		 24.6 173.99786376953125 25.4 173.99835205078125 26.2 173.99887084960935 27 173.99964904785156
		 27.8 174.00048828125 28.6 174.00146484375 29.4 174.00262451171875 30.2 174.00393676757812
		 31 174.00537109375 31.8 174.00685119628906 32.6 174.00859069824219 33.4 174.01055908203125
		 34.2 174.01258850097656 35 174.01480102539065 35.8 174.01728820800781 36.6 174.0203857421875
		 37.4 174.02154541015625 38.2 174.02565002441406 39 174.02682495117187 39.8 174.03047180175781
		 40.6 174.03366088867187 41.4 174.03660583496094 42.2 174.03990173339844 43 174.0438232421875
		 43.8 174.04496765136722 44.6 174.04905700683597 45.4 174.05302429199219 46.2 174.05426025390625
		 47 174.05850219726562 47.8 174.05987548828125 48.6 174.0635986328125 49.4 174.06463623046875
		 50.2 174.06726074218753 51 174.06929016113281 51.8 174.07087707519531 52.6 174.07234191894531
		 53.4 174.07275390625 54.2 174.07363891601562 55 174.07414245605469 55.8 174.07421875
		 56.6 174.07412719726562 57.4 174.07363891601562 58.2 174.07273864746094 59 174.07247924804687
		 59.8 174.07135009765625 60.6 174.07023620605469 61.4 174.06906127929687 62.2 174.06771850585935
		 63 174.06732177734375 63.8 174.066162109375 64.6 174.06535339355469 65.4 174.06477355957031
		 66.2 174.06443786621094 67 174.06442260742187 67.8 174.06472778320312 68.6 174.06538391113281
		 69.4 174.06642150878906 70.2 174.06788635253906 71 174.07017517089844 71.8 174.07090759277344
		 72.6 174.07391357421875 73.4 174.07768249511719 74.2 174.07955932617187 75 174.08148193359375
		 75.8 174.08590698242187 76.6 174.09065246582031 77.4 174.09284973144531 78.2 174.09507751464844
		 79 174.09982299804687 79.8 174.10435485839847 80.6 174.10585021972656 81.4 174.11077880859375
		 82.2 174.11225891113281 83 174.11677551269531 83.8 174.11819458007812 84.6 174.12210083007812
		 85.4 174.12324523925781 86.2 174.12606811523435;
createNode animCurveTA -n "Bip01_R_UpperArm_rotateX14";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 108 ".ktv[0:107]"  0.6 -23.900274276733402 1.4 -24.138803482055664
		 2.2 -24.388198852539063 3 -24.615297317504883 3.8 -24.866209030151367 4.6 -25.165395736694336
		 5.4 -25.467216491699219 6.2 -25.739837646484375 7 -25.995941162109375 7.8 -26.225408554077148
		 8.6 -26.407289505004883 9.4 -26.581781387329105 10.2 -26.760820388793945 11 -26.891778945922855
		 11.8 -27.008798599243168 12.6 -27.17689323425293 13.4 -27.342473983764648 14.2 -27.472673416137695
		 15 -27.616519927978516 15.8 -27.72148323059082 16.6 -27.699464797973633 17.4 -27.610626220703129
		 18.2 -27.534412384033203 19 -27.462213516235352 19.8 -27.364444732666016 20.6 -27.205183029174808
		 21.4 -26.974382400512699 22.2 -26.702030181884769 23 -26.41473388671875 23.8 -26.125566482543945
		 24.6 -25.804374694824219 25.4 -25.388259887695313 26.2 -24.898508071899421 27 -24.409082412719727
		 27.8 -23.918607711791992 28.6 -23.400436401367188 29.4 -22.838933944702148 30.2 -22.206655502319336
		 31 -21.52947998046875 31.8 -20.884183883666992 32.6 -20.294275283813477 33.4 -19.697355270385746
		 34.2 -19.032571792602539 35 -18.323574066162109 35.8 -17.623979568481445 36.6 -16.965192794799805
		 37.4 -16.327957153320312 38.2 -15.642601013183596 39 -14.911434173583984 39.8 -14.175601005554199
		 40.6 -13.435742378234863 41.4 -12.689006805419922 42.2 -11.926973342895508 43 -11.18631649017334
		 43.8 -10.493677139282228 44.6 -9.8000783920288086 45.4 -9.132929801940918 46.2 -8.5385704040527344
		 47 -7.966580390930174 47.8 -7.3957386016845694 48.6 -6.8397088050842294 49.4 -6.2896227836608887
		 50.2 -5.7640156745910645 51 -5.2903623580932617 51.8 -4.8575520515441895 52.6 -4.4554500579833993
		 53.4 -4.0831856727600098 54.2 -3.7409183979034424 55 -3.4426274299621582 55.8 -3.1729662418365479
		 56.6 -2.9119272232055664 57.4 -2.6558847427368164 58.2 -2.3579649925231938 59 -2.0262458324432373
		 59.8 -1.772743821144104 60.6 -1.6260168552398682 61.4 -1.5186582803726196 62.2 -1.3811168670654297
		 63 -1.1724218130111694 63.8 -0.94245642423629772 64.6 -0.72700917720794678 65.4 -0.46328014135360723
		 66.2 -0.13339373469352722 67 0.19047795236110687 67.8 0.47709557414054887 68.6 0.77089095115661621
		 69.4 1.1234481334686279 70.2 1.5464843511581421 71 1.9888088703155515 71.8 2.3927583694458008
		 72.6 2.7604029178619385 73.4 3.1238672733306885 74.2 3.4780271053314209 75 3.7698659896850586
		 75.8 4.0041742324829102 76.6 4.2375173568725586 77.4 4.4744291305541992 78.2 4.6991782188415527
		 79 4.8898177146911621 79.8 5.0198116302490234 80.6 5.097376823425293 81.4 5.1506175994873047
		 82.2 5.1904191970825195 83 5.2325468063354492 83.8 5.2873740196228027 84.6 5.306185245513916
		 85.4 5.2526850700378418 86.2 5.1639132499694824;
createNode animCurveTA -n "Bip01_R_UpperArm_rotateY14";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 108 ".ktv[0:107]"  0.6 -53.86705398559571 1.4 -53.850593566894538
		 2.2 -53.831317901611328 3 -53.830554962158203 3.8 -53.814376831054688 4.6 -53.7576904296875
		 5.4 -53.695442199707031 6.2 -53.667449951171875 7 -53.664875030517578 7.8 -53.671871185302734
		 8.6 -53.687126159667969 9.4 -53.692569732666016 10.2 -53.681694030761719 11 -53.682674407958984
		 11.8 -53.690921783447266 12.6 -53.671394348144531 13.4 -53.633037567138672 14.2 -53.595802307128906
		 15 -53.553661346435547 15.8 -53.521141052246094 16.6 -53.517730712890625 17.4 -53.526714324951165
		 18.2 -53.523639678955078 19 -53.497188568115234 19.8 -53.463397979736328 20.6 -53.453914642333984
		 21.4 -53.466091156005859 22.2 -53.471664428710937 23 -53.455146789550781 23.8 -53.427330017089837
		 24.6 -53.418655395507805 25.4 -53.44061279296875 26.2 -53.474567413330078 27 -53.508155822753906
		 27.8 -53.550247192382812 28.6 -53.599918365478509 29.4 -53.647991180419922 30.2 -53.690887451171875
		 31 -53.718284606933594 31.8 -53.743434906005859 32.6 -53.798030853271491 33.4 -53.887054443359382
		 34.2 -53.984371185302734 35 -54.078105926513672 35.8 -54.192092895507805 36.6 -54.314075469970703
		 37.4 -54.387790679931641 38.2 -54.435054779052734 39 -54.529197692871094 39.8 -54.680324554443359
		 40.6 -54.867828369140625 41.4 -55.064346313476563 42.2 -55.221485137939453 43 -55.284862518310547
		 43.8 -55.244899749755859 44.6 -55.184597015380859 45.4 -55.191493988037116 46.2 -55.260322570800781
		 47 -55.35992431640625 47.8 -55.474472045898438 48.6 -55.559391021728509 49.4 -55.554767608642578
		 50.2 -55.483455657958984 51 -55.466136932373047 51.8 -55.555038452148438 52.6 -55.681209564208984
		 53.4 -55.784343719482422 54.2 -55.862438201904297 55 -55.926918029785149 55.8 -55.982753753662109
		 56.6 -56.031185150146477 57.4 -56.078643798828125 58.2 -56.131900787353523 59 -56.176200866699219
		 59.8 -56.1788330078125 60.6 -56.144657135009766 61.4 -56.136959075927734 62.2 -56.173374176025398
		 63 -56.216487884521491 63.8 -56.274494171142578 64.6 -56.341564178466797 65.4 -56.344394683837891
		 66.2 -56.226959228515625 67 -55.987518310546875 67.8 -55.721961975097656 68.6 -55.581058502197266
		 69.4 -55.565093994140625 70.2 -55.57606506347657 71 -55.576904296875007 71.8 -55.515254974365234
		 72.6 -55.384998321533203 73.4 -55.285797119140625 74.2 -55.245639801025391 75 -55.237628936767578
		 75.8 -55.290359497070313 76.6 -55.422386169433594 77.4 -55.659530639648438 78.2 -56.047832489013672
		 79 -56.495895385742195 79.8 -56.841106414794922 80.6 -57.068954467773438 81.4 -57.245491027832031
		 82.2 -57.391910552978516 83 -57.517471313476563 83.8 -57.656982421874993 84.6 -57.824092864990227
		 85.4 -57.995979309082024 86.2 -58.198226928710938;
createNode animCurveTA -n "Bip01_R_UpperArm_rotateZ14";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 108 ".ktv[0:107]"  0.6 -16.215690612792969 1.4 -16.24586296081543
		 2.2 -16.264045715332031 3 -16.340456008911133 3.8 -16.368795394897461 4.6 -16.28767204284668
		 5.4 -16.197288513183594 6.2 -16.174911499023438 7 -16.190891265869141 7.8 -16.25452995300293
		 8.6 -16.399374008178711 9.4 -16.535528182983398 10.2 -16.634950637817383 11 -16.80723762512207
		 11.8 -16.98036003112793 12.6 -17.011144638061523 13.4 -16.998727798461914 14.2 -17.006464004516602
		 15 -16.938714981079102 15.8 -16.901638031005859 16.6 -17.061609268188477 17.4 -17.289999008178711
		 18.2 -17.42341423034668 19 -17.467437744140625 19.8 -17.479900360107422 20.6 -17.540868759155273
		 21.4 -17.662906646728516 22.2 -17.771493911743168 23 -17.809728622436523 23.8 -17.759368896484375
		 24.6 -17.68980598449707 25.4 -17.724000930786133 26.2 -17.817642211914066 27 -17.832111358642578
		 27.8 -17.774824142456055 28.6 -17.694677352905273 29.4 -17.621437072753906 30.2 -17.612628936767578
		 31 -17.616933822631836 31.8 -17.498481750488281 32.6 -17.241512298583984 33.4 -16.975818634033203
		 34.2 -16.803619384765625 35 -16.677923202514648 35.8 -16.519697189331055 36.6 -16.272241592407227
		 37.4 -15.940192222595217 38.2 -15.664085388183596 39 -15.491888046264648 39.8 -15.364684104919434
		 40.6 -15.272122383117676 41.4 -15.203248977661133 42.2 -15.145817756652832 43 -15.00773811340332
		 43.8 -14.745827674865724 44.6 -14.502627372741701 45.4 -14.285677909851074 46.2 -14.017436027526855
		 47 -13.773674964904783 47.8 -13.585089683532717 48.6 -13.406064033508301 49.4 -13.22343158721924
		 50.2 -13.022107124328612 51 -12.831353187561035 51.8 -12.708160400390623 52.6 -12.626606941223145
		 53.4 -12.546451568603516 54.2 -12.464632987976076 55 -12.362952232360842 55.8 -12.265484809875488
		 56.6 -12.200450897216797 57.4 -12.172721862792969 58.2 -12.26215648651123 59 -12.431838035583496
		 59.8 -12.452220916748049 60.6 -12.27153205871582 61.4 -12.043188095092772 62.2 -11.888259887695314
		 63 -11.838908195495604 63.8 -11.806872367858888 64.6 -11.72346305847168 65.4 -11.646266937255859
		 66.2 -11.57180118560791 67 -11.369158744812012 67.8 -11.037859916687012 68.6 -10.762149810791016
		 69.4 -10.643107414245604 70.2 -10.640695571899414 71 -10.648951530456545 71.8 -10.542812347412108
		 72.6 -10.327181816101074 73.4 -10.135616302490234 74.2 -9.9921178817749023 75 -9.7940149307250977
		 75.8 -9.5749540328979492 76.6 -9.4529542922973633 77.4 -9.4658136367797852 78.2 -9.6274251937866211
		 79 -9.8391609191894531 79.8 -9.9436492919921875 80.6 -9.9478769302368164 81.4 -9.9467697143554687
		 82.2 -9.9714088439941406 83 -10.04954719543457 83.8 -10.215861320495604 84.6 -10.391769409179687
		 85.4 -10.495006561279297 86.2 -10.596993446350098;
createNode animCurveTA -n "Bip01_R_Forearm_rotateX14";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 108 ".ktv[0:107]"  0.6 -0.99948310852050759 1.4 -0.9985002875328064
		 2.2 -0.99814969301223755 3 -0.99658590555191029 3.8 -0.99714171886444103 4.6 -1.0024131536483765
		 5.4 -1.0082048177719116 6.2 -1.0091601610183716 7 -1.006584644317627 7.8 -1.00294029712677
		 8.6 -0.99826145172119141 9.4 -0.99504625797271717 10.2 -0.99309098720550559 11 -0.98827081918716431
		 11.8 -0.98322832584381115 12.6 -0.98257613182067904 13.4 -0.98312616348266613 14.2 -0.98360544443130482
		 15 -0.98817622661590587 15.8 -0.99258929491043091 16.6 -0.98929882049560558 17.4 -0.98261314630508423
		 18.2 -0.97649234533309937 19 -0.97637891769409191 19.8 -0.97773194313049316 20.6 -0.97707813978195179
		 21.4 -0.97400605678558339 22.2 -0.97216117382049583 23 -0.97358185052871704 23.8 -0.97825229167938221
		 24.6 -0.98381143808364868 25.4 -0.98586702346801758 26.2 -0.98525911569595337 27 -0.98658585548400879
		 27.8 -0.99018144607543934 28.6 -0.99598824977874767 29.4 -1.0039516687393188 30.2 -1.0100156068801882
		 31 -1.0141136646270752 31.8 -1.0227762460708618 32.6 -1.0384261608123779 33.4 -1.0544857978820801
		 34.2 -1.0650986433029177 35 -1.0735491514205933 35.8 -1.0846621990203855 36.6 -1.1006066799163818
		 37.4 -1.1223099231719973 38.2 -1.1435600519180298 39 -1.1577639579772947 39.8 -1.1651506423950195
		 40.6 -1.1677730083465576 41.4 -1.1691055297851565 42.2 -1.176050066947937 43 -1.1997699737548828
		 43.8 -1.2430386543273926 44.6 -1.2895435094833374 45.4 -1.3257071971893313 46.2 -1.3541675806045532
		 47 -1.3791666030883789 47.8 -1.4020094871520996 48.6 -1.4271669387817385 49.4 -1.4627269506454468
		 50.2 -1.5062503814697266 51 -1.5396382808685305 51.8 -1.555189847946167 52.6 -1.5641170740127563
		 53.4 -1.5696771144866943 54.2 -1.5735988616943362 55 -1.578410267829895 55.8 -1.5821796655654907
		 56.6 -1.5860899686813354 57.4 -1.587799668312073 58.2 -1.5824415683746338 59 -1.5733742713928225
		 59.8 -1.5715214014053345 60.6 -1.5754817724227903 61.4 -1.5791023969650269 62.2 -1.5830316543579104
		 63 -1.5854132175445557 63.8 -1.5873208045959473 64.6 -1.5905715227127075 65.4 -1.5962300300598145
		 66.2 -1.6053075790405271 67 -1.6218410730361941 67.8 -1.6432995796203611 68.6 -1.6598571538925171
		 69.4 -1.6674543619155884 70.2 -1.6696643829345705 71 -1.6719361543655396 71.8 -1.6803444623947144
		 72.6 -1.6953921318054199 73.4 -1.7120382785797119 74.2 -1.730196475982666 75 -1.7517120838165283
		 75.8 -1.7698279619216919 76.6 -1.7739804983139038 77.4 -1.7625535726547239 78.2 -1.7385441064834597
		 79 -1.7158620357513428 79.8 -1.7053900957107544 80.6 -1.7041862010955813 81.4 -1.7048680782318115
		 82.2 -1.7017498016357422 83 -1.6859009265899658 83.8 -1.6573081016540527 84.6 -1.6346863508224487
		 85.4 -1.632710337638855 86.2 -1.6467434167861938;
createNode animCurveTA -n "Bip01_R_Forearm_rotateY14";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 108 ".ktv[0:107]"  0.6 1.8774468898773191 1.4 1.8771032094955444
		 2.2 1.8755036592483521 3 1.8716766834259031 3.8 1.8694202899932864 4.6 1.8719329833984377
		 5.4 1.8762069940567017 6.2 1.8777382373809819 7 1.8769606351852417 7.8 1.8749045133590694
		 8.6 1.8707262277603147 9.4 1.8668619394302368 10.2 1.8650300502777104 11 1.8629674911499023
		 11.8 1.8613160848617556 12.6 1.8621876239776611 13.4 1.8631440401077275 14.2 1.8630763292312622
		 15 1.8644709587097168 15.8 1.8651597499847412 16.6 1.8616093397140501 17.4 1.8572632074356079
		 18.2 1.8572896718978884 19 1.8586280345916748 19.8 1.8612390756607056 20.6 1.8626174926757817
		 21.4 1.862175464630127 22.2 1.8618159294128416 23 1.8627184629440308 23.8 1.8657869100570681
		 24.6 1.8705477714538574 25.4 1.8734992742538452 26.2 1.8736308813095093 27 1.873730421066284
		 27.8 1.8751420974731445 28.6 1.8781578540802002 29.4 1.8813199996948242 30.2 1.8811413049697869
		 31 1.8792055845260616 31.8 1.8818747997283936 32.6 1.8907953500747681 33.4 1.9005889892578123
		 34.2 1.9071756601333618 35 1.9128626585006712 35.8 1.9200021028518679 36.6 1.9289035797119141
		 37.4 1.9403488636016843 38.2 1.9517682790756223 39 1.9595856666564941 39.8 1.9633392095565789
		 40.6 1.964395999908447 41.4 1.9654846191406248 42.2 1.9698166847229008 43 1.981655955314636
		 43.8 2.001490592956543 44.6 2.02130126953125 45.4 2.0351471900939941 46.2 2.0444328784942627
		 47 2.0511710643768311 47.8 2.0557878017425537 48.6 2.0599181652069092 49.4 2.0679471492767334
		 50.2 2.0807056427001953 51 2.0918130874633789 51.8 2.0974717140197754 52.6 2.0980944633483887
		 53.4 2.1002020835876465 54.2 2.1025564670562744 55 2.1054656505584717 55.8 2.1079745292663574
		 56.6 2.1092572212219238 57.4 2.109748363494873 58.2 2.1081645488739018 59 2.1055557727813721
		 59.8 2.1056094169616699 60.6 2.1080996990203857 61.4 2.1108736991882324 62.2 2.1135516166687012
		 63 2.1171870231628418 63.8 2.1192371845245361 64.6 2.1200351715087891 65.4 2.1201984882354736
		 66.2 2.1219241619110107 67 2.1250796318054199 67.8 2.1292669773101807 68.6 2.1324090957641602
		 69.4 2.1337113380432129 70.2 2.133762121200562 71 2.1334824562072754 71.8 2.1342973709106445
		 72.6 2.1358950138092041 73.4 2.1364045143127441 74.2 2.1360435485839844 75 2.1366894245147705
		 75.8 2.1382229328155522 76.6 2.1386101245880127 77.4 2.1371920108795166 78.2 2.1338355541229248
		 79 2.1312682628631592 79.8 2.1319444179534912 80.6 2.1351461410522461 81.4 2.139176607131958
		 82.2 2.1423754692077637 83 2.1418070793151855 83.8 2.1371409893035889 84.6 2.1338584423065186
		 85.4 2.1358814239501953 86.2 2.1409451961517334;
createNode animCurveTA -n "Bip01_R_Forearm_rotateZ14";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 108 ".ktv[0:107]"  0.6 -49.683746337890625 1.4 -49.657539367675781
		 2.2 -49.625946044921875 3 -49.534984588623047 3.8 -49.513946533203125 4.6 -49.666595458984375
		 5.4 -49.856037139892578 6.2 -49.899127960205078 7 -49.831214904785156 7.8 -49.721057891845703
		 8.6 -49.557743072509766 9.4 -49.430809020996094 10.2 -49.361137390136719 11 -49.224834442138672
		 11.8 -49.089683532714851 12.6 -49.088119506835945 13.4 -49.114151000976563 14.2 -49.123703002929688
		 15 -49.244850158691406 15.8 -49.352176666259766 16.6 -49.228111267089837 17.4 -49.017520904541016
		 18.2 -48.882972717285149 19 -48.900199890136719 19.8 -48.968463897705085 20.6 -48.975528717041016
		 21.4 -48.902873992919922 22.2 -48.857891082763672 23 -48.902053833007813 23.8 -49.049274444580078
		 24.6 -49.241859436035149 25.4 -49.331840515136719 26.2 -49.320903778076179 27 -49.350425720214851
		 27.8 -49.448947906494141 28.6 -49.619621276855469 29.4 -49.839439392089837 30.2 -49.969188690185547
		 31 -50.031017303466797 31.8 -50.261268615722656 32.6 -50.736148834228509 33.4 -51.232463836669922
		 34.2 -51.562263488769531 35 -51.830982208251953 35.8 -52.179039001464851 36.6 -52.660648345947266
		 37.4 -53.307064056396477 38.2 -53.943092346191406 39 -54.370849609375 39.8 -54.589244842529297
		 40.6 -54.663043975830078 41.4 -54.707942962646477 42.2 -54.923580169677734 43 -55.621635437011719
		 43.8 -56.874244689941413 44.6 -58.2037353515625 45.4 -59.221111297607422 46.2 -60.005271911621087
		 47 -60.680850982666016 47.8 -61.286109924316406 48.6 -61.94749832153321 49.4 -62.906204223632812
		 50.2 -64.105445861816406 51 -65.034896850585937 51.8 -65.471054077148438 52.6 -65.702896118164063
		 53.4 -65.859375 54.2 -65.975761413574219 55 -66.118385314941406 55.8 -66.232246398925781
		 56.6 -66.340522766113281 57.4 -66.387451171875 58.2 -66.240226745605469 59 -65.991172790527344
		 59.8 -65.944541931152344 60.6 -66.063735961914063 61.4 -66.176277160644531 62.2 -66.296424865722656
		 63 -66.385536193847656 63.8 -66.450836181640625 64.6 -66.53857421875 65.4 -66.681449890136719
		 66.2 -66.923110961914063 67 -67.363899230957031 67.8 -67.937454223632813 68.6 -68.379608154296875
		 69.4 -68.581733703613281 70.2 -68.638320922851577 71 -68.694244384765625 71.8 -68.914070129394531
		 72.6 -69.309501647949219 73.4 -69.740989685058594 74.2 -70.208938598632812 75 -70.771400451660156
		 75.8 -71.250694274902344 76.6 -71.360893249511719 77.4 -71.056121826171875 78.2 -70.414756774902344
		 79 -69.81201171875 79.8 -69.544174194335938 80.6 -69.53118896484375 81.4 -69.57373046875
		 82.2 -69.515525817871094 83 -69.108985900878906 83.8 -68.352188110351563 84.6 -67.758415222167969
		 85.4 -67.726730346679688 86.2 -68.120452880859389;
createNode animCurveTA -n "Bip01_R_Hand_rotateX4";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 108 ".ktv[0:107]"  0.6 90.133316040039063 1.4 90.133316040039063
		 2.2 90.133308410644531 3 90.133316040039063 3.8 90.133316040039063 4.6 90.133308410644531
		 5.4 90.133316040039063 6.2 90.133308410644531 7 90.133316040039063 7.8 90.133316040039063
		 8.6 90.133316040039063 9.4 90.133316040039063 10.2 90.133316040039063 11 90.133316040039063
		 11.8 90.133316040039063 12.6 90.133308410644531 13.4 90.133316040039063 14.2 90.133316040039063
		 15 90.133316040039063 15.8 90.133316040039063 16.6 90.133316040039063 17.4 90.133316040039063
		 18.2 90.133308410644531 19 90.133308410644531 19.8 90.133331298828125 20.6 90.133316040039063
		 21.4 90.133316040039063 22.2 90.133316040039063 23 90.133293151855469 23.8 90.133308410644531
		 24.6 90.133308410644531 25.4 90.133316040039063 26.2 90.133316040039063 27 90.133316040039063
		 27.8 90.133316040039063 28.6 90.133316040039063 29.4 90.133316040039063 30.2 90.133316040039063
		 31 90.133316040039063 31.8 90.133308410644531 32.6 90.133316040039063 33.4 90.133316040039063
		 34.2 90.133316040039063 35 90.133316040039063 35.8 90.133316040039063 36.6 90.133316040039063
		 37.4 90.133308410644531 38.2 90.133316040039063 39 90.133316040039063 39.8 90.133316040039063
		 40.6 90.133316040039063 41.4 90.133308410644531 42.2 90.133308410644531 43 90.133316040039063
		 43.8 90.133316040039063 44.6 90.133308410644531 45.4 90.133308410644531 46.2 90.133316040039063
		 47 90.133316040039063 47.8 90.133308410644531 48.6 90.133316040039063 49.4 90.133316040039063
		 50.2 90.133316040039063 51 90.133316040039063 51.8 90.133308410644531 52.6 90.133316040039063
		 53.4 90.133316040039063 54.2 90.133308410644531 55 90.133308410644531 55.8 90.133316040039063
		 56.6 90.133316040039063 57.4 90.133316040039063 58.2 90.133316040039063 59 90.133308410644531
		 59.8 90.133316040039063 60.6 90.133316040039063 61.4 90.133316040039063 62.2 90.133308410644531
		 63 90.133316040039063 63.8 90.133308410644531 64.6 90.133316040039063 65.4 90.133308410644531
		 66.2 90.133316040039063 67 90.133308410644531 67.8 90.133308410644531 68.6 90.133316040039063
		 69.4 90.133316040039063 70.2 90.133316040039063 71 90.133316040039063 71.8 90.133308410644531
		 72.6 90.133316040039063 73.4 90.133316040039063 74.2 90.133308410644531 75 90.133316040039063
		 75.8 90.133316040039063 76.6 90.133316040039063 77.4 90.133316040039063 78.2 90.133316040039063
		 79 90.133316040039063 79.8 90.133316040039063 80.6 90.133316040039063 81.4 90.133316040039063
		 82.2 90.133308410644531 83 90.133316040039063 83.8 90.133316040039063 84.6 90.133316040039063
		 85.4 90.133316040039063 86.2 90.133316040039063;
createNode animCurveTA -n "Bip01_R_Hand_rotateY4";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 108 ".ktv[0:107]"  0.6 -6.3004064559936523 1.4 -6.3004045486450195
		 2.2 -6.3004016876220703 3 -6.3004050254821777 3.8 -6.3004002571105957 4.6 -6.3004012107849121
		 5.4 -6.3003973960876465 6.2 -6.3004045486450195 7 -6.3003988265991211 7.8 -6.3004064559936523
		 8.6 -6.3003973960876465 9.4 -6.3004059791564941 10.2 -6.3003993034362793 11 -6.3004055023193359
		 11.8 -6.3004031181335449 12.6 -6.3004035949707031 13.4 -6.3004026412963876 14.2 -6.3003983497619629
		 15 -6.3003997802734375 15.8 -6.3004026412963876 16.6 -6.3004083633422852 17.4 -6.3004074096679687
		 18.2 -6.3003993034362793 19 -6.3004045486450195 19.8 -6.3004021644592285 20.6 -6.3004069328308105
		 21.4 -6.3004016876220703 22.2 -6.3004045486450195 23 -6.3004012107849121 23.8 -6.3004045486450195
		 24.6 -6.3004045486450195 25.4 -6.3004007339477539 26.2 -6.3004045486450195 27 -6.3003988265991211
		 27.8 -6.3004059791564941 28.6 -6.3004007339477539 29.4 -6.3004031181335449 30.2 -6.3003993034362793
		 31 -6.3004021644592285 31.8 -6.3004031181335449 32.6 -6.3004007339477539 33.4 -6.3004045486450195
		 34.2 -6.3003983497619629 35 -6.3004002571105957 35.8 -6.3003964424133301 36.6 -6.3003983497619629
		 37.4 -6.3003988265991211 38.2 -6.3004069328308105 39 -6.3004050254821777 39.8 -6.3004021644592285
		 40.6 -6.3004045486450195 41.4 -6.3004045486450195 42.2 -6.3004088401794434 43 -6.3004121780395508
		 43.8 -6.3003950119018555 44.6 -6.3004026412963876 45.4 -6.3004059791564941 46.2 -6.3003988265991211
		 47 -6.3003988265991211 47.8 -6.3004007339477539 48.6 -6.3004035949707031 49.4 -6.3004012107849121
		 50.2 -6.3004035949707031 51 -6.3003959655761728 51.8 -6.3004074096679687 52.6 -6.3004097938537598
		 53.4 -6.3004007339477539 54.2 -6.3004002571105957 55 -6.3004031181335449 55.8 -6.3004055023193359
		 56.6 -6.3004088401794434 57.4 -6.3004040718078622 58.2 -6.3003964424133301 59 -6.3004031181335449
		 59.8 -6.3004021644592285 60.6 -6.3004169464111328 61.4 -6.300407886505127 62.2 -6.3004088401794434
		 63 -6.3004016876220703 63.8 -6.3004145622253418 64.6 -6.3004064559936523 65.4 -6.3004069328308105
		 66.2 -6.3004097938537598 67 -6.3004026412963876 67.8 -6.3003964424133301 68.6 -6.3004035949707031
		 69.4 -6.3004074096679687 70.2 -6.3004059791564941 71 -6.3003954887390137 71.8 -6.3004050254821777
		 72.6 -6.3004050254821777 73.4 -6.3004040718078622 74.2 -6.3004026412963876 75 -6.3004083633422852
		 75.8 -6.3004007339477539 76.6 -6.3004007339477539 77.4 -6.3004088401794434 78.2 -6.300412654876709
		 79 -6.3003988265991211 79.8 -6.3004045486450195 80.6 -6.3004059791564941 81.4 -6.3004045486450195
		 82.2 -6.3003964424133301 83 -6.3003988265991211 83.8 -6.3003964424133301 84.6 -6.3003983497619629
		 85.4 -6.3003945350646973 86.2 -6.3004012107849121;
createNode animCurveTA -n "Bip01_R_Hand_rotateZ4";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 108 ".ktv[0:107]"  0.6 2.565330982208252 1.4 2.5653247833251953
		 2.2 2.5653324127197266 3 2.5653371810913086 3.8 2.5653297901153564 4.6 2.5653340816497803
		 5.4 2.5653352737426758 6.2 2.5653274059295654 7 2.5653386116027832 7.8 2.5653247833251953
		 8.6 2.5653436183929443 9.4 2.5653271675109863 10.2 2.5653319358825684 11 2.5653281211853027
		 11.8 2.5653386116027832 12.6 2.5653297901153564 13.4 2.5653314590454106 14.2 2.5653324127197266
		 15 2.5653326511383057 15.8 2.5653326511383057 16.6 2.5653281211853027 17.4 2.5653266906738281
		 18.2 2.5653352737426758 19 2.5653278827667236 19.8 2.5653326511383057 20.6 2.5653297901153564
		 21.4 2.5653278827667236 22.2 2.5653266906738281 23 2.5653314590454106 23.8 2.5653257369995117
		 24.6 2.5653314590454106 25.4 2.5653350353240967 26.2 2.5653331279754639 27 2.5653307437896729
		 27.8 2.5653326511383057 28.6 2.565333366394043 29.4 2.565330982208252 30.2 2.5653359889984131
		 31 2.5653300285339355 31.8 2.5653314590454106 32.6 2.5653278827667236 33.4 2.565333366394043
		 34.2 2.5653343200683594 35 2.5653331279754639 35.8 2.5653324127197266 36.6 2.5653312206268311
		 37.4 2.5653328895568848 38.2 2.5653338432312016 39 2.5653336048126221 39.8 2.5653328895568848
		 40.6 2.565333366394043 41.4 2.5653316974639893 42.2 2.5653281211853027 43 2.5653345584869385
		 43.8 2.5653378963470463 44.6 2.5653364658355713 45.4 2.5653269290924072 46.2 2.5653338432312016
		 47 2.5653345584869385 47.8 2.5653336048126221 48.6 2.565330982208252 49.4 2.5653355121612549
		 50.2 2.5653247833251953 51 2.5653314590454106 51.8 2.5653324127197266 52.6 2.5653264522552495
		 53.4 2.5653328895568848 54.2 2.5653312206268311 55 2.5653319358825684 55.8 2.5653324127197266
		 56.6 2.5653321743011475 57.4 2.5653235912322998 58.2 2.5653352737426758 59 2.5653307437896729
		 59.8 2.5653312206268311 60.6 2.5653247833251953 61.4 2.5653285980224609 62.2 2.5653302669525142
		 63 2.5653278827667236 63.8 2.5653295516967773 64.6 2.5653297901153564 65.4 2.5653345584869385
		 66.2 2.5653359889984131 67 2.5653316974639893 67.8 2.5653400421142578 68.6 2.5653297901153564
		 69.4 2.5653352737426758 70.2 2.5653278827667236 71 2.5653355121612549 71.8 2.5653345584869385
		 72.6 2.565335750579834 73.4 2.5653324127197266 74.2 2.5653312206268311 75 2.5653276443481445
		 75.8 2.5653359889984131 76.6 2.5653374195098877 77.4 2.5653269290924072 78.2 2.565330982208252
		 79 2.5653300285339355 79.8 2.5653278827667236 80.6 2.5653266906738281 81.4 2.5653328895568848
		 82.2 2.5653359889984131 83 2.5653362274169922 83.8 2.5653378963470463 84.6 2.5653345584869385
		 85.4 2.5653283596038814 86.2 2.5653324127197266;
createNode animCurveTA -n "Bip01_L_Clavicle_rotateX14";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 108 ".ktv[0:107]"  0.6 -0.050971671938896179 1.4 -0.061575878411531448
		 2.2 -0.066673159599304199 3 -0.071752697229385376 3.8 -0.082543350756168365 4.6 -0.091837875545024858
		 5.4 -0.10198834538459778 6.2 -0.10492255538702012 7 -0.11461684852838516 7.8 -0.12250693142414093
		 8.6 -0.12956903874874115 9.4 -0.13620169460773468 10.2 -0.14246244728565216 11 -0.1483137309551239
		 11.8 -0.153755784034729 12.6 -0.15862785279750824 13.4 -0.16293057799339294 14.2 -0.16669222712516785
		 15 -0.17011784017086029 15.8 -0.17358934879302981 16.6 -0.17456845939159393 17.4 -0.1776484549045563
		 18.2 -0.1784040778875351 19 -0.18017016351222992 19.8 -0.18135884404182431 20.6 -0.18223364651203156
		 21.4 -0.18287728726863861 22.2 -0.18320819735527041 23 -0.18334735929965973 23.8 -0.18335656821727755
		 24.6 -0.18333196640014648 25.4 -0.18319313228130343 26.2 -0.18312443792819977 27 -0.18297252058982849
		 27.8 -0.18293698132038116 28.6 -0.18298400938510895 29.4 -0.18309658765792847 30.2 -0.18323323130607605
		 31 -0.18343962728977203 31.8 -0.1837896853685379 32.6 -0.18414159119129181 33.4 -0.18468841910362244
		 34.2 -0.18488419055938721 35 -0.18560679256916049 35.8 -0.1862589567899704 36.6 -0.18690317869186401
		 37.4 -0.18756891787052157 38.2 -0.18817934393882751 39 -0.18881835043430328 39.8 -0.18892136216163635
		 40.6 -0.18901327252388 41.4 -0.18904198706150058 42.2 -0.18846303224563599 43 -0.18739834427833557
		 43.8 -0.18697638809680939 44.6 -0.18506981432437897 45.4 -0.18250387907028201 46.2 -0.1816719472408295
		 47 -0.17840985953807831 47.8 -0.17503194510936737 48.6 -0.17086201906204226 49.4 -0.16929219663143158
		 50.2 -0.1636369526386261 51 -0.16192097961902618 51.8 -0.15693803131580353 52.6 -0.15261223912239075
		 53.4 -0.14839270710945127 54.2 -0.14394241571426392 55 -0.1386883556842804 55.8 -0.13687404990196228
		 56.6 -0.13077563047409058 57.4 -0.12899166345596311 58.2 -0.12377816438674927 59 -0.1182253807783127
		 59.8 -0.11567583680152892 60.6 -0.11312609910964964 61.4 -0.1077556535601616 62.2 -0.1033167690038681
		 63 -0.099123053252696991 63.8 -0.094783194363117218 64.6 -0.090335398912429796 65.4 -0.085569307208061218
		 66.2 -0.079783596098423004 67 -0.077717006206512451 67.8 -0.070526622235774994 68.6 -0.068408057093620314
		 69.4 -0.062188122421503067 70.2 -0.057003255933523171 71 -0.052099011838436127 71.8 -0.047140587121248245
		 72.6 -0.041436359286308289 73.4 -0.039720941334962845 74.2 -0.033840760588645935
		 75 -0.02800968661904335 75.8 -0.026413531973958015 76.6 -0.021037617698311809 77.4 -0.016274493187665939
		 78.2 -0.010916653089225292 79 -0.00935348030179739 79.8 -0.0038231189828366037 80.6 0.00116068369243294
		 81.4 0.006126313004642725 82.2 0.011672023683786392 83 0.01874123141169548 83.8 0.020996784791350365
		 84.6 0.028821706771850582 85.4 0.036052089184522629 86.2 0.043747536838054657;
createNode animCurveTA -n "Bip01_L_Clavicle_rotateY14";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 108 ".ktv[0:107]"  0.6 -79.66021728515625 1.4 -79.638267517089844
		 2.2 -79.627700805664063 3 -79.617057800292983 3.8 -79.594596862792997 4.6 -79.575279235839844
		 5.4 -79.554145812988281 6.2 -79.547943115234375 7 -79.52787017822267 7.8 -79.511512756347642
		 8.6 -79.496810913085938 9.4 -79.482879638671875 10.2 -79.469810485839844 11 -79.457550048828125
		 11.8 -79.446296691894531 12.6 -79.436141967773438 13.4 -79.427169799804687 14.2 -79.419349670410156
		 15 -79.412200927734375 15.8 -79.404960632324219 16.6 -79.402877807617188 17.4 -79.396507263183594
		 18.2 -79.394844055175781 19 -79.391204833984375 19.8 -79.388694763183594 20.6 -79.3868408203125
		 21.4 -79.385566711425781 22.2 -79.384796142578125 23 -79.384498596191406 23.8 -79.38448333740233
		 24.6 -79.384620666503906 25.4 -79.384864807128906 26.2 -79.385063171386733 27 -79.385276794433594
		 27.8 -79.385353088378906 28.6 -79.385238647460938 29.4 -79.385009765625 30.2 -79.3846435546875
		 31 -79.384239196777344 31.8 -79.383651733398438 32.6 -79.382919311523438 33.4 -79.381668090820327
		 34.2 -79.381301879882813 35 -79.3797607421875 35.8 -79.378479003906264 36.6 -79.377105712890625
		 37.4 -79.375717163085938 38.2 -79.374382019042983 39 -79.37310791015625 39.8 -79.372871398925781
		 40.6 -79.37255859375 41.4 -79.372665405273438 42.2 -79.37384033203125 43 -79.376106262207017
		 43.8 -79.376968383789063 44.6 -79.380966186523438 45.4 -79.386299133300781 46.2 -79.388076782226562
		 47 -79.39487457275392 47.8 -79.40185546875 48.6 -79.410598754882813 49.4 -79.41387939453125
		 50.2 -79.425704956054688 51 -79.429252624511719 51.8 -79.439697265625 52.6 -79.448753356933594
		 53.4 -79.457496643066406 54.2 -79.466804504394531 55 -79.477729797363281 55.8 -79.481529235839844
		 56.6 -79.494308471679673 57.4 -79.498001098632813 58.2 -79.508949279785156 59 -79.520446777343764
		 59.8 -79.525665283203125 60.6 -79.531028747558594 61.4 -79.542243957519545 62.2 -79.551399230957017
		 63 -79.560195922851562 63.8 -79.569129943847642 64.6 -79.578330993652358 65.4 -79.588333129882827
		 66.2 -79.600372314453125 67 -79.604667663574219 67.8 -79.619575500488295 68.6 -79.624000549316406
		 69.4 -79.636787414550781 70.2 -79.647697448730483 71 -79.657791137695313 71.8 -79.668205261230469
		 72.6 -79.680084228515625 73.4 -79.683563232421875 74.2 -79.695793151855469 75 -79.707794189453139
		 75.8 -79.711174011230469 76.6 -79.722267150878906 77.4 -79.73211669921875 78.2 -79.743133544921875
		 79 -79.74652099609375 79.8 -79.757957458496094 80.6 -79.768173217773438 81.4 -79.77838134765625
		 82.2 -79.789970397949219 83 -79.80450439453125 83.8 -79.809112548828125 84.6 -79.825355529785156
		 85.4 -79.840263366699219 86.2 -79.856193542480469;
createNode animCurveTA -n "Bip01_L_Clavicle_rotateZ14";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 108 ".ktv[0:107]"  0.6 174.22525024414062 1.4 174.22740173339844
		 2.2 174.22842407226562 3 174.22943115234375 3.8 174.2315673828125 4.6 174.23344421386719
		 5.4 174.2354736328125 6.2 174.23600769042969 7 174.23797607421878 7.8 174.23956298828125
		 8.6 174.240966796875 9.4 174.24224853515625 10.2 174.24346923828125 11 174.24459838867187
		 11.8 174.2457275390625 12.6 174.24668884277344 13.4 174.24751281738281 14.2 174.2482604980469
		 15 174.24893188476562 15.8 174.2496337890625 16.6 174.24980163574219 17.4 174.25042724609375
		 18.2 174.25054931640625 19 174.25088500976565 19.8 174.25111389160156 20.6 174.25129699707031
		 21.4 174.25143432617187 22.2 174.25149536132812 23 174.25149536132812 23.8 174.25151062011719
		 24.6 174.25151062011719 25.4 174.25148010253906 26.2 174.25151062011719 27 174.25141906738281
		 27.8 174.25141906738281 28.6 174.25141906738281 29.4 174.25144958496094 30.2 174.25144958496094
		 31 174.25149536132812 31.8 174.25161743164065 32.6 174.25169372558594 33.4 174.25175476074219
		 34.2 174.25180053710935 35 174.25193786621094 35.8 174.25209045410159 36.6 174.25221252441406
		 37.4 174.25233459472656 38.2 174.25242614746094 39 174.25256347656253 39.8 174.25259399414062
		 40.6 174.25256347656253 41.4 174.25263977050781 42.2 174.25251770019531 43 174.25230407714847
		 43.8 174.25222778320312 44.6 174.25186157226562 45.4 174.25135803222659 46.2 174.25120544433594
		 47 174.25056457519531 47.8 174.2498779296875 48.6 174.24905395507812 49.4 174.24876403808594
		 50.2 174.24766540527344 51 174.24731445312503 51.8 174.246337890625 52.6 174.24551391601562
		 53.4 174.24464416503909 54.2 174.24380493164062 55 174.24275207519534 55.8 174.24237060546875
		 56.6 174.2412109375 57.4 174.2408447265625 58.2 174.23983764648435 59 174.23872375488281
		 59.8 174.23818969726565 60.6 174.23768615722656 61.4 174.23663330078125 62.2 174.23573303222656
		 63 174.23492431640625 63.8 174.23403930664062 64.6 174.23312377929687 65.4 174.23220825195315
		 66.2 174.23103332519531 67 174.23063659667969 67.8 174.22917175292969 68.6 174.228759765625
		 69.4 174.22746276855469 70.2 174.22647094726562 71 174.22543334960935 71.8 174.2244873046875
		 72.6 174.22334289550784 73.4 174.22297668457031 74.2 174.22178649902344 75 174.2205810546875
		 75.8 174.22027587890625 76.6 174.21917724609375 77.4 174.21820068359375 78.2 174.21708679199219
		 79 174.216796875 79.8 174.21566772460935 80.6 174.21461486816406 81.4 174.21357727050781
		 82.2 174.21249389648437 83 174.21101379394531 83.8 174.21052551269531 84.6 174.20893859863281
		 85.4 174.20742797851562 86.2 174.20587158203128;
createNode animCurveTA -n "Bip01_L_UpperArm_rotateX14";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 108 ".ktv[0:107]"  0.6 11.178706169128418 1.4 10.150518417358398
		 2.2 9.0312051773071289 3 7.9060912132263184 3.8 6.7805624008178711 4.6 5.6235904693603516
		 5.4 4.4899559020996094 6.2 3.4385857582092285 7 2.4261517524719238 7.8 1.4450908899307253
		 8.6 0.49643918871879578 9.4 -0.46790084242820734 10.2 -1.4142798185348513 11 -2.2494552135467529
		 11.8 -2.9796454906463623 12.6 -3.6887493133544926 13.4 -4.4133758544921875 14.2 -5.1516609191894531
		 15 -5.8392448425292969 15.8 -6.3467440605163574 16.6 -6.6732416152954102 17.4 -6.9839539527893066
		 18.2 -7.3757090568542463 19 -7.7375545501708975 19.8 -7.9598345756530779 20.6 -8.1094932556152344
		 21.4 -8.2584810256958026 22.2 -8.4009132385253906 23 -8.5333919525146484 23.8 -8.6258754730224609
		 24.6 -8.6439723968505859 25.4 -8.6582059860229492 26.2 -8.6625289916992187 27 -8.6194629669189453
		 27.8 -8.5754814147949237 28.6 -8.5832433700561523 29.4 -8.6496400833129883 30.2 -8.7554616928100586
		 31 -8.8170690536499023 31.8 -8.7620706558227539 32.6 -8.6463851928710937 33.4 -8.5584402084350586
		 34.2 -8.4915370941162109 35 -8.4320278167724609 35.8 -8.3920125961303711 36.6 -8.3719263076782227
		 37.4 -8.4539022445678711 38.2 -8.6639804840087891 39 -8.8094673156738281 39.8 -8.8102216720581055
		 40.6 -8.8035650253295898 41.4 -8.8563413619995135 42.2 -8.9585428237915039 43 -9.0439796447753906
		 43.8 -9.0383157730102539 44.6 -9.0150499343872088 45.4 -9.0710945129394531 46.2 -9.2013950347900391
		 47 -9.3084850311279297 47.8 -9.2928562164306641 48.6 -9.2168216705322266 49.4 -9.1842708587646484
		 50.2 -9.1942710876464844 51 -9.257075309753418 51.8 -9.3861236572265625 52.6 -9.5275287628173828
		 53.4 -9.6877670288085938 54.2 -9.9180231094360352 55 -10.169567108154297 55.8 -10.404106140136721
		 56.6 -10.680034637451172 57.4 -10.980161666870115 58.2 -11.197728157043455 59 -11.350923538208008
		 59.8 -11.551598548889162 60.6 -11.808933258056641 61.4 -12.073853492736816 62.2 -12.348801612854004
		 63 -12.629067420959473 63.8 -12.879339218139648 64.6 -13.123130798339844 65.4 -13.388522148132324
		 66.2 -13.643845558166504 67 -13.916499137878418 67.8 -14.258621215820313 68.6 -14.612888336181642
		 69.4 -14.916313171386719 70.2 -15.186895370483397 71 -15.450334548950195 71.8 -15.682191848754885
		 72.6 -15.864949226379393 73.4 -16.088106155395508 74.2 -16.384626388549805 75 -16.623504638671875
		 75.8 -16.779634475708008 76.6 -16.954572677612308 77.4 -17.178916931152344 78.2 -17.433954238891602
		 79 -17.651416778564453 79.8 -17.733039855957031 80.6 -17.674318313598633 81.4 -17.573024749755859
		 82.2 -17.524503707885742 83 -17.523340225219727 83.8 -17.478862762451172 84.6 -17.34809684753418
		 85.4 -17.145965576171875 86.2 -16.831300735473633;
createNode animCurveTA -n "Bip01_L_UpperArm_rotateY14";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 108 ".ktv[0:107]"  0.6 63.679573059082031 1.4 63.875282287597656
		 2.2 64.064094543457031 3 64.199211120605469 3.8 64.304664611816406 4.6 64.420890808105469
		 5.4 64.501358032226563 6.2 64.467124938964844 7 64.312873840332031 7.8 64.129302978515625
		 8.6 64.038917541503906 9.4 64.072525024414062 10.2 64.1527099609375 11 64.219551086425781
		 11.8 64.263389587402344 12.6 64.276374816894531 13.4 64.272270202636719 14.2 64.264129638671875
		 15 64.224639892578125 15.8 64.17669677734375 16.6 64.183074951171875 17.4 64.223472595214844
		 18.2 64.252799987792969 19 64.287666320800781 19.8 64.34869384765625 20.6 64.418350219726563
		 21.4 64.471351623535156 22.2 64.505844116210938 23 64.538314819335937 23.8 64.572822570800781
		 24.6 64.595375061035156 25.4 64.622451782226563 26.2 64.648933410644531 27 64.669380187988281
		 27.8 64.68121337890625 28.6 64.682815551757813 29.4 64.67230224609375 30.2 64.65948486328125
		 31 64.668167114257813 31.8 64.700859069824219 32.6 64.732925415039063 33.4 64.762786865234375
		 34.2 64.803604125976562 35 64.849678039550781 35.8 64.893264770507812 36.6 64.93243408203125
		 37.4 64.954238891601563 38.2 64.949333190917969 39 64.942985534667969 39.8 64.947242736816406
		 40.6 64.9337158203125 41.4 64.9150390625 42.2 64.948715209960938 43 65.039772033691406
		 43.8 65.12750244140625 44.6 65.167129516601577 45.4 65.168624877929688 46.2 65.157989501953125
		 47 65.149673461914063 47.8 65.155487060546875 48.6 65.173599243164063 49.4 65.183853149414063
		 50.2 65.177032470703125 51 65.158966064453125 51.8 65.152511596679688 52.6 65.192024230957031
		 53.4 65.253082275390625 54.2 65.268501281738281 55 65.231330871582031 55.8 65.168388366699219
		 56.6 65.096092224121094 57.4 65.037391662597656 58.2 64.987289428710937 59 64.920333862304687
		 59.8 64.853446960449219 60.6 64.814567565917969 61.4 64.765350341796875 62.2 64.654808044433594
		 63 64.518486022949219 63.8 64.426223754882813 64.6 64.383628845214844 65.4 64.3524169921875
		 66.2 64.299751281738281 67 64.196952819824219 67.8 64.056953430175781 68.6 63.92732238769532
		 69.4 63.822971343994141 70.2 63.740810394287109 71 63.666294097900384 71.8 63.575042724609375
		 72.6 63.485633850097656 73.4 63.433856964111328 74.2 63.403278350830078 75 63.358688354492202
		 75.8 63.309040069580078 76.6 63.271350860595696 77.4 63.247028350830078 78.2 63.26732254028321
		 79 63.336708068847649 79.8 63.388866424560547 80.6 63.397895812988281 81.4 63.397804260253906
		 82.2 63.401691436767578 83 63.394733428955078 83.8 63.35440826416017 84.6 63.270469665527344
		 85.4 63.161746978759759 86.2 63.053077697753906;
createNode animCurveTA -n "Bip01_L_UpperArm_rotateZ14";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 108 ".ktv[0:107]"  0.6 -13.768073081970217 1.4 -13.896890640258787
		 2.2 -14.125899314880373 3 -14.310508728027344 3.8 -14.46511936187744 4.6 -14.665904045104982
		 5.4 -14.812341690063477 6.2 -14.78575325012207 7 -14.660593986511232 7.8 -14.504727363586426
		 8.6 -14.39096164703369 9.4 -14.41457939147949 10.2 -14.489736557006838 11 -14.452590942382813
		 11.8 -14.31512451171875 12.6 -14.206459045410156 13.4 -14.195648193359377 14.2 -14.292274475097656
		 15 -14.384524345397949 15.8 -14.288494110107422 16.6 -14.03816604614258 17.4 -13.872439384460447
		 18.2 -13.912106513977053 19 -13.995994567871094 19.8 -13.966953277587892 20.6 -13.913854598999023
		 21.4 -13.927088737487791 22.2 -13.990012168884276 23 -14.098036766052248 23.8 -14.200514793395996
		 24.6 -14.238198280334473 25.4 -14.30302619934082 26.2 -14.382545471191404 27 -14.410529136657715
		 27.8 -14.445762634277342 28.6 -14.55662822723389 29.4 -14.747586250305178 30.2 -14.99412727355957
		 31 -15.182012557983398 31.8 -15.200682640075684 32.6 -15.117376327514648 33.4 -15.060474395751953
		 34.2 -15.027036666870117 35 -14.998715400695804 35.8 -14.991822242736816 36.6 -15.005519866943359
		 37.4 -15.159206390380859 38.2 -15.492825508117674 39 -15.737864494323732 39.8 -15.786928176879886
		 40.6 -15.834660530090332 41.4 -15.9875545501709 42.2 -16.259260177612305 43 -16.5533447265625
		 43.8 -16.725910186767578 44.6 -16.861835479736328 45.4 -17.11046028137207 46.2 -17.4755859375
		 47 -17.811870574951172 47.8 -17.967721939086914 48.6 -18.028898239135746 49.4 -18.13526725769043
		 50.2 -18.274967193603516 51 -18.459846496582031 51.8 -18.713525772094727 52.6 -18.965631484985352
		 53.4 -19.22087287902832 54.2 -19.544771194458008 55 -19.84174919128418 55.8 -19.995389938354492
		 56.6 -20.091081619262695 57.4 -20.183784484863281 58.2 -20.142333984375 59 -19.962837219238281
		 59.8 -19.811077117919918 60.6 -19.718448638916016 61.4 -19.599386215209964 62.2 -19.437192916870117
		 63 -19.243911743164062 63.8 -19.007560729980469 64.6 -18.776905059814453 65.4 -18.581764221191406
		 66.2 -18.361873626708984 67 -18.149871826171875 67.8 -18.036008834838867 68.6 -17.96253776550293
		 69.4 -17.845453262329102 70.2 -17.713228225708008 71 -17.600894927978516 71.8 -17.461408615112305
		 72.6 -17.264909744262695 73.4 -17.141958236694336 74.2 -17.167779922485352 75 -17.203788757324219
		 75.8 -17.23193359375 76.6 -17.371082305908203 77.4 -17.629865646362305 78.2 -17.978355407714844
		 79 -18.322084426879883 79.8 -18.496162414550781 80.6 -18.488498687744141 81.4 -18.465518951416016
		 82.2 -18.580419540405273 83 -18.816312789916992 83.8 -19.018970489501953 84.6 -19.112449645996097
		 85.4 -19.125698089599609 86.2 -19.005205154418945;
createNode animCurveTA -n "Bip01_L_Forearm_rotateX14";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 108 ".ktv[0:107]"  0.6 1.4627469778060913 1.4 1.4672842025756836
		 2.2 1.4685835838317871 3 1.4709575176239014 3.8 1.4747939109802246 4.6 1.4788339138031006
		 5.4 1.4844515323638916 6.2 1.4921033382415771 7 1.4983439445495603 7.8 1.501854419708252
		 8.6 1.5035560131072998 9.4 1.5023964643478394 10.2 1.4992829561233521 11 1.498521089553833
		 11.8 1.5000571012496948 12.6 1.4992331266403198 13.4 1.4945564270019531 14.2 1.4864729642868042
		 15 1.4772341251373291 15.8 1.47260582447052 16.6 1.4731111526489258 17.4 1.4722855091094973
		 18.2 1.4658116102218628 19 1.4584872722625732 19.8 1.4558923244476318 20.6 1.4549388885498049
		 21.4 1.4514446258544922 22.2 1.4460318088531494 23 1.4398016929626465 23.8 1.4339079856872561
		 24.6 1.4297018051147461 25.4 1.4258646965026855 26.2 1.4222841262817385 27 1.4201834201812744
		 27.8 1.4172801971435549 28.6 1.4118660688400269 29.4 1.4035128355026243 30.2 1.3926975727081301
		 31 1.3855074644088743 31.8 1.3888713121414185 32.6 1.3998064994812012 33.4 1.4104098081588743
		 34.2 1.4193505048751831 35 1.4270135164260864 35.8 1.4318935871124268 36.6 1.4347046613693235
		 37.4 1.4340002536773682 38.2 1.4293259382247925 39 1.4307112693786621 39.8 1.4415057897567749
		 40.6 1.4538600444793699 41.4 1.4546524286270142 42.2 1.4401148557662964 43 1.4179743528366089
		 43.8 1.4028176069259644 44.6 1.3941339254379272 45.4 1.3843591213226321 46.2 1.3713330030441286
		 47 1.3584779500961304 47.8 1.349173903465271 48.6 1.3417376279830933 49.4 1.3351407051086426
		 50.2 1.331424355506897 51 1.3293164968490601 51.8 1.3238152265548706 52.6 1.3146547079086304
		 53.4 1.3033556938171389 54.2 1.2933578491210935 55 1.2897340059280396 55.8 1.2965306043624878
		 56.6 1.3111015558242798 57.4 1.3264076709747314 58.2 1.3434402942657473 59 1.3665800094604492
		 59.8 1.3932393789291382 60.6 1.4217520952224731 61.4 1.4549113512039185 62.2 1.4901530742645264
		 63 1.5222626924514773 63.8 1.5525702238082886 64.6 1.5830202102661133 65.4 1.612866997718811
		 66.2 1.6447142362594604 67 1.6799811124801636 67.8 1.715303897857666 68.6 1.7500568628311155
		 69.4 1.7864189147949221 70.2 1.8218874931335447 71 1.8519501686096191 71.8 1.8788856267929077
		 72.6 1.9085534811019897 73.4 1.9373525381088257 74.2 1.9565351009368896 75 1.9662585258483884
		 75.8 1.9692851305007937 76.6 1.9644148349761961 77.4 1.9446648359298706 78.2 1.8989348411560056
		 79 1.8382947444915778 79.8 1.7859127521514893 80.6 1.7338415384292605 81.4 1.6711041927337646
		 82.2 1.6144922971725464 83 1.5749672651290894 83.8 1.5489002466201782 84.6 1.5321611166000366
		 85.4 1.5201489925384519 86.2 1.5124545097351074;
createNode animCurveTA -n "Bip01_L_Forearm_rotateY14";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 108 ".ktv[0:107]"  0.6 -2.0766482353210449 1.4 -2.0781283378601074
		 2.2 -2.0791032314300537 3 -2.0816404819488525 3.8 -2.0828068256378174 4.6 -2.083092212677002
		 5.4 -2.0824952125549316 6.2 -2.0813391208648682 7 -2.0798633098602295 7.8 -2.0793430805206299
		 8.6 -2.0803909301757812 9.4 -2.0816495418548588 10.2 -2.0826179981231689 11 -2.0846142768859863
		 11.8 -2.0872585773468018 12.6 -2.088463306427002 13.4 -2.0875813961029053 14.2 -2.0869488716125488
		 15 -2.0827887058258057 15.8 -2.0794575214385982 16.6 -2.0778312683105469 17.4 -2.0762519836425781
		 18.2 -2.0736687183380127 19 -2.0721497535705566 19.8 -2.0732786655426025 20.6 -2.0745699405670166
		 21.4 -2.0738556385040283 22.2 -2.0719516277313232 23 -2.0700774192810059 23.8 -2.0686700344085693
		 24.6 -2.0676698684692383 25.4 -2.0663156509399414 26.2 -2.0653841495513916 27 -2.0663058757781982
		 27.8 -2.0681667327880859 28.6 -2.0692844390869141 29.4 -2.0683133602142334 30.2 -2.0650207996368408
		 31 -2.0625860691070557 31.8 -2.0644991397857666 32.6 -2.0686354637145996 33.4 -2.0704705715179443
		 34.2 -2.0696024894714355 35 -2.0676450729370117 35.8 -2.0654356479644775 36.6 -2.0636560916900635
		 37.4 -2.0612506866455078 38.2 -2.0576727390289307 39 -2.0542283058166504 39.8 -2.0582301616668701
		 40.6 -2.0630848407745361 41.4 -2.0633735656738281 42.2 -2.0568497180938721 43 -2.0484554767608643
		 43.8 -2.0439357757568359 44.6 -2.0435333251953125 45.4 -2.0439341068267822 46.2 -2.0436642169952393
		 47 -2.0436522960662842 47.8 -2.045008659362793 48.6 -2.0464849472045894 49.4 -2.0470521450042725
		 50.2 -2.0472249984741211 51 -2.0456235408782963 51.8 -2.0429973602294922 52.6 -2.0396256446838379
		 53.4 -2.0366125106811523 54.2 -2.0344982147216797 55 -2.0342681407928467 55.8 -2.0374298095703125
		 56.6 -2.043179988861084 57.4 -2.0484309196472168 58.2 -2.0529210567474365 59 -2.0578508377075195
		 59.8 -2.0629534721374512 60.6 -2.0694394111633301 61.4 -2.0790877342224121 62.2 -2.089989185333252
		 63 -2.0992774963378902 63.8 -2.1071784496307373 64.6 -2.1141655445098877 65.4 -2.1200540065765381
		 66.2 -2.1259324550628662 67 -2.1323771476745605 67.8 -2.1383936405181885 68.6 -2.1436142921447754
		 69.4 -2.1482255458831787 70.2 -2.1515753269195557 71 -2.1532261371612549 71.8 -2.1537477970123291
		 72.6 -2.1538424491882324 73.4 -2.1535086631774902 74.2 -2.153005838394165 75 -2.1530439853668213
		 75.8 -2.1535394191741943 76.6 -2.1541299819946289 77.4 -2.1541650295257568 78.2 -2.15175461769104
		 79 -2.1472516059875488 79.8 -2.1435863971710205 80.6 -2.1378414630889893 81.4 -2.1263022422790527
		 82.2 -2.1139349937438965 83 -2.1059234142303467 83.8 -2.1021943092346191 84.6 -2.1007952690124512
		 85.4 -2.0990638732910156 86.2 -2.0962283611297607;
createNode animCurveTA -n "Bip01_L_Forearm_rotateZ14";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 108 ".ktv[0:107]"  0.6 -62.978908538818359 1.4 -63.104824066162109
		 2.2 -63.145709991455085 3 -63.227584838867188 3.8 -63.333061218261719 4.6 -63.436012268066399
		 5.4 -63.571075439453125 6.2 -63.753643035888672 7 -63.899005889892578 7.8 -63.983863830566406
		 8.6 -64.034774780273437 9.4 -64.014717102050781 10.2 -63.943782806396484 11 -63.940643310546882
		 11.8 -64.000770568847656 12.6 -63.990398406982429 13.4 -63.866077423095696 14.2 -63.659969329833977
		 15 -63.393569946289063 15.8 -63.248813629150391 16.6 -63.247459411621087 17.4 -63.213035583496094
		 18.2 -63.029331207275384 19 -62.833591461181634 19.8 -62.778709411621101 20.6 -62.766662597656243
		 21.4 -62.673278808593743 22.2 -62.522197723388665 23 -62.350784301757805 23.8 -62.192333221435547
		 24.6 -62.079437255859375 25.4 -61.971950531005859 26.2 -61.87554931640625 27 -61.833301544189453
		 27.8 -61.78193283081054 28.6 -61.662574768066406 29.4 -61.451072692871101 30.2 -61.155651092529297
		 31 -60.956684112548828 31.8 -61.058601379394531 32.6 -61.366378784179687 33.4 -61.640850067138672
		 34.2 -61.847618103027344 35 -62.014694213867188 35.8 -62.113475799560547 36.6 -62.167018890380838
		 37.4 -62.128326416015625 38.2 -61.981346130371094 39 -61.988586425781257 39.8 -62.29270935058593
		 40.6 -62.64338302612304 41.4 -62.665622711181641 42.2 -62.247104644775384 43 -61.620609283447266
		 43.8 -61.201618194580085 44.6 -60.980266571044922 45.4 -60.739768981933601 46.2 -60.416004180908203
		 47 -60.102703094482429 47.8 -59.893672943115227 48.6 -59.733894348144538 49.4 -59.585494995117195
		 50.2 -59.500621795654304 51 -59.431549072265625 51.8 -59.270549774169922 52.6 -59.015865325927741
		 53.4 -58.716094970703125 54.2 -58.459140777587898 55 -58.373001098632805 55.8 -58.570453643798835
		 56.6 -58.979907989501953 57.4 -59.4002685546875 58.2 -59.852317810058594 59 -60.455482482910156
		 59.8 -61.147533416748047 60.6 -61.903537750244141 61.4 -62.808525085449219 62.2 -63.779033660888672
		 63 -64.658058166503906 63.8 -65.481338500976563 64.6 -66.301597595214844 65.4 -67.099655151367188
		 66.2 -67.949943542480469 67 -68.893020629882813 67.8 -69.835868835449219 68.6 -70.761360168457031
		 69.4 -71.727073669433594 70.2 -72.665428161621094 71 -73.458076477050781 71.8 -74.167465209960937
		 72.6 -74.950897216796875 73.4 -75.712493896484375 74.2 -76.221160888671875 75 -76.479347229003906
		 75.8 -76.560134887695312 76.6 -76.430999755859375 77.4 -75.906883239746094 78.2 -74.692604064941406
		 79 -73.081855773925781 79.8 -71.694869995117187 80.6 -70.312332153320312 81.4 -68.627578735351563
		 82.2 -67.097755432128906 83 -66.035804748535156 83.8 -65.349075317382813 84.6 -64.918426513671875
		 85.4 -64.604103088378906 86.2 -64.387840270996094;
createNode animCurveTA -n "Bip01_L_Hand_rotateX4";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 108 ".ktv[0:107]"  0.6 -90.133316040039063 1.4 -90.133316040039063
		 2.2 -90.133316040039063 3 -90.133316040039063 3.8 -90.133316040039063 4.6 -90.133308410644531
		 5.4 -90.133316040039063 6.2 -90.133308410644531 7 -90.133316040039063 7.8 -90.133331298828125
		 8.6 -90.133308410644531 9.4 -90.133316040039063 10.2 -90.133316040039063 11 -90.133316040039063
		 11.8 -90.133331298828125 12.6 -90.133316040039063 13.4 -90.133316040039063 14.2 -90.133316040039063
		 15 -90.133308410644531 15.8 -90.133316040039063 16.6 -90.133308410644531 17.4 -90.133316040039063
		 18.2 -90.133308410644531 19 -90.133316040039063 19.8 -90.133316040039063 20.6 -90.133316040039063
		 21.4 -90.133316040039063 22.2 -90.133316040039063 23 -90.133316040039063 23.8 -90.133316040039063
		 24.6 -90.133308410644531 25.4 -90.133316040039063 26.2 -90.133316040039063 27 -90.133316040039063
		 27.8 -90.133316040039063 28.6 -90.133316040039063 29.4 -90.133316040039063 30.2 -90.133308410644531
		 31 -90.133316040039063 31.8 -90.133316040039063 32.6 -90.133316040039063 33.4 -90.133316040039063
		 34.2 -90.133331298828125 35 -90.133308410644531 35.8 -90.133316040039063 36.6 -90.133316040039063
		 37.4 -90.133316040039063 38.2 -90.133308410644531 39 -90.133308410644531 39.8 -90.133316040039063
		 40.6 -90.133316040039063 41.4 -90.133316040039063 42.2 -90.133308410644531 43 -90.133316040039063
		 43.8 -90.133308410644531 44.6 -90.133316040039063 45.4 -90.133316040039063 46.2 -90.133316040039063
		 47 -90.133316040039063 47.8 -90.133308410644531 48.6 -90.133316040039063 49.4 -90.133316040039063
		 50.2 -90.133316040039063 51 -90.133316040039063 51.8 -90.133316040039063 52.6 -90.133316040039063
		 53.4 -90.133316040039063 54.2 -90.133316040039063 55 -90.133308410644531 55.8 -90.133308410644531
		 56.6 -90.133316040039063 57.4 -90.133316040039063 58.2 -90.133308410644531 59 -90.133316040039063
		 59.8 -90.133316040039063 60.6 -90.133316040039063 61.4 -90.133316040039063 62.2 -90.133316040039063
		 63 -90.133316040039063 63.8 -90.133308410644531 64.6 -90.133316040039063 65.4 -90.133308410644531
		 66.2 -90.133308410644531 67 -90.133316040039063 67.8 -90.133308410644531 68.6 -90.133316040039063
		 69.4 -90.133316040039063 70.2 -90.133316040039063 71 -90.133308410644531 71.8 -90.133316040039063
		 72.6 -90.133316040039063 73.4 -90.133316040039063 74.2 -90.133316040039063 75 -90.133331298828125
		 75.8 -90.133316040039063 76.6 -90.133308410644531 77.4 -90.133316040039063 78.2 -90.133316040039063
		 79 -90.133316040039063 79.8 -90.133316040039063 80.6 -90.133316040039063 81.4 -90.133316040039063
		 82.2 -90.133316040039063 83 -90.133316040039063 83.8 -90.133316040039063 84.6 -90.133316040039063
		 85.4 -90.133316040039063 86.2 -90.133308410644531;
createNode animCurveTA -n "Bip01_L_Hand_rotateY4";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 108 ".ktv[0:107]"  0.6 6.3004074096679687 1.4 6.3004045486450195
		 2.2 6.3003964424133301 3 6.3003988265991211 3.8 6.3003978729248047 4.6 6.3003950119018555
		 5.4 6.3003997802734375 6.2 6.3003993034362793 7 6.3004016876220703 7.8 6.3003997802734375
		 8.6 6.3003940582275391 9.4 6.3003983497619629 10.2 6.3003978729248047 11 6.3003988265991211
		 11.8 6.3003969192504883 12.6 6.3003988265991211 13.4 6.3003969192504883 14.2 6.3004007339477539
		 15 6.3003954887390137 15.8 6.3004021644592285 16.6 6.3003997802734375 17.4 6.3004002571105957
		 18.2 6.3003964424133301 19 6.3003954887390137 19.8 6.3004012107849121 20.6 6.3003969192504883
		 21.4 6.3004007339477539 22.2 6.3003945350646973 23 6.3003993034362793 23.8 6.3003978729248047
		 24.6 6.3003988265991211 25.4 6.3003993034362793 26.2 6.3003945350646973 27 6.3003931045532227
		 27.8 6.3003907203674316 28.6 6.3003907203674316 29.4 6.3004007339477539 30.2 6.3003945350646973
		 31 6.300391674041748 31.8 6.3003940582275391 32.6 6.3003964424133301 33.4 6.3003978729248047
		 34.2 6.3003945350646973 35 6.3003964424133301 35.8 6.3003931045532227 36.6 6.300386905670166
		 37.4 6.3003888130187988 38.2 6.3003950119018555 39 6.3003983497619629 39.8 6.3003921508789063
		 40.6 6.3003959655761728 41.4 6.3003959655761728 42.2 6.3004012107849121 43 6.3003921508789063
		 43.8 6.3003969192504883 44.6 6.3003964424133301 45.4 6.3003964424133301 46.2 6.3003964424133301
		 47 6.3003926277160645 47.8 6.3003983497619629 48.6 6.3003954887390137 49.4 6.3003978729248047
		 50.2 6.3003978729248047 51 6.3003978729248047 51.8 6.3003983497619629 52.6 6.3003973960876465
		 53.4 6.3004007339477539 54.2 6.3003940582275391 55 6.3003945350646973 55.8 6.3003964424133301
		 56.6 6.3003973960876465 57.4 6.3003945350646973 58.2 6.3003926277160645 59 6.3003997802734375
		 59.8 6.3004002571105957 60.6 6.3003993034362793 61.4 6.3003935813903809 62.2 6.3003959655761728
		 63 6.3003983497619629 63.8 6.3004088401794434 64.6 6.3003931045532227 65.4 6.300391674041748
		 66.2 6.3004007339477539 67 6.3003907203674316 67.8 6.3003964424133301 68.6 6.300384521484375
		 69.4 6.3003997802734375 70.2 6.3003964424133301 71 6.3004021644592285 71.8 6.3003964424133301
		 72.6 6.3004007339477539 73.4 6.300391674041748 74.2 6.3003973960876465 75 6.3003911972045898
		 75.8 6.3003935813903809 76.6 6.3004040718078622 77.4 6.3003964424133301 78.2 6.3003973960876465
		 79 6.3003926277160645 79.8 6.300412654876709 80.6 6.3004035949707031 81.4 6.3003959655761728
		 82.2 6.3003997802734375 83 6.3004002571105957 83.8 6.3003926277160645 84.6 6.3003911972045898
		 85.4 6.3003950119018555 86.2 6.3004055023193359;
createNode animCurveTA -n "Bip01_L_Hand_rotateZ4";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 108 ".ktv[0:107]"  0.6 2.5653307437896729 1.4 2.5653321743011475
		 2.2 2.5653281211853027 3 2.5653314590454106 3.8 2.5653307437896729 4.6 2.5653324127197266
		 5.4 2.5653264522552495 6.2 2.5653371810913086 7 2.5653316974639893 7.8 2.5653312206268311
		 8.6 2.5653402805328369 9.4 2.5653314590454106 10.2 2.5653338432312016 11 2.5653314590454106
		 11.8 2.5653285980224609 12.6 2.5653293132781982 13.4 2.5653369426727295 14.2 2.5653319358825684
		 15 2.565338134765625 15.8 2.5653355121612549 16.6 2.5653336048126221 17.4 2.5653302669525142
		 18.2 2.565333366394043 19 2.5653355121612549 19.8 2.5653297901153564 20.6 2.56532883644104
		 21.4 2.5653345584869385 22.2 2.565330982208252 23 2.5653297901153564 23.8 2.5653319358825684
		 24.6 2.5653312206268311 25.4 2.5653352737426758 26.2 2.5653352737426758 27 2.5653378963470463
		 27.8 2.5653305053710938 28.6 2.5653347969055176 29.4 2.5653338432312016 30.2 2.5653338432312016
		 31 2.5653297901153564 31.8 2.5653290748596191 32.6 2.5653345584869385 33.4 2.5653319358825684
		 34.2 2.5653340816497803 35 2.5653388500213623 35.8 2.5653407573699951 36.6 2.5653319358825684
		 37.4 2.5653328895568848 38.2 2.5653393268585205 39 2.5653293132781982 39.8 2.5653297901153564
		 40.6 2.5653343200683594 41.4 2.5653343200683594 42.2 2.5653316974639893 43 2.5653312206268311
		 43.8 2.5653359889984131 44.6 2.5653324127197266 45.4 2.5653347969055176 46.2 2.5653319358825684
		 47 2.5653302669525142 47.8 2.5653345584869385 48.6 2.5653336048126221 49.4 2.5653352737426758
		 50.2 2.5653295516967773 51 2.5653293132781982 51.8 2.5653343200683594 52.6 2.565338134765625
		 53.4 2.5653319358825684 54.2 2.5653386116027832 55 2.5653367042541504 55.8 2.5653347969055176
		 56.6 2.5653378963470463 57.4 2.5653328895568848 58.2 2.5653395652770996 59 2.5653297901153564
		 59.8 2.5653350353240967 60.6 2.5653364658355713 61.4 2.5653307437896729 62.2 2.5653345584869385
		 63 2.5653326511383057 63.8 2.5653324127197266 64.6 2.5653297901153564 65.4 2.5653336048126221
		 66.2 2.5653336048126221 67 2.5653398036956787 67.8 2.5653336048126221 68.6 2.5653345584869385
		 69.4 2.5653305053710938 70.2 2.5653345584869385 71 2.5653352737426758 71.8 2.565338134765625
		 72.6 2.5653324127197266 73.4 2.5653326511383057 74.2 2.5653316974639893 75 2.5653297901153564
		 75.8 2.5653343200683594 76.6 2.5653340816497803 77.4 2.5653350353240967 78.2 2.5653390884399414
		 79 2.5653336048126221 79.8 2.5653278827667236 80.6 2.5653266906738281 81.4 2.5653350353240967
		 82.2 2.5653374195098877 83 2.5653343200683594 83.8 2.5653300285339355 84.6 2.5653302669525142
		 85.4 2.5653338432312016 86.2 2.5653340816497803;
createNode animCurveTA -n "Bip01_L_Thigh_rotateX14";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 108 ".ktv[0:107]"  0.6 -8.1720752716064453 1.4 -8.0869979858398437
		 2.2 -8.0091066360473633 3 -7.9310240745544451 3.8 -7.8611650466918945 4.6 -7.7706513404846165
		 5.4 -7.6636905670166016 6.2 -7.5377144813537607 7 -7.4294147491455078 7.8 -7.3157057762146005
		 8.6 -7.1921615600585938 9.4 -7.1021122932434082 10.2 -7.0399303436279297 11 -6.9665193557739258
		 11.8 -6.8611431121826172 12.6 -6.7619690895080566 13.4 -6.6904430389404297 14.2 -6.6523852348327637
		 15 -6.6195182800292969 15.8 -6.5683193206787109 16.6 -6.4988698959350586 17.4 -6.4491734504699707
		 18.2 -6.4537363052368164 19 -6.4861111640930176 19.8 -6.4918756484985352 20.6 -6.4971761703491211
		 21.4 -6.5176668167114267 22.2 -6.5504517555236816 23 -6.5742206573486328 23.8 -6.6176605224609375
		 24.6 -6.6420855522155771 25.4 -6.6683135032653809 26.2 -6.7220807075500488 27 -6.7963109016418457
		 27.8 -6.8980264663696289 28.6 -7.0003695487976074 29.4 -7.0723872184753418 30.2 -7.111565113067627
		 31 -7.1711244583129892 31.8 -7.251693248748782 32.6 -7.3012261390686035 33.4 -7.356550693511962
		 34.2 -7.4628829956054661 35 -7.5798702239990243 35.8 -7.6629528999328604 36.6 -7.7467727661132813
		 37.4 -7.8368492126464853 38.2 -7.9264888763427725 39 -7.9939985275268564 39.8 -8.0432033538818359
		 40.6 -8.0929269790649414 41.4 -8.1248378753662109 42.2 -8.1484975814819336 43 -8.1981658935546875
		 43.8 -8.2632989883422852 44.6 -8.2903194427490234 45.4 -8.2993898391723633 46.2 -8.3119297027587891
		 47 -8.3098812103271484 47.8 -8.3012475967407227 48.6 -8.2747735977172852 49.4 -8.2417697906494141
		 50.2 -8.2248859405517578 51 -8.2236566543579102 51.8 -8.2200231552124023 52.6 -8.1778669357299805
		 53.4 -8.1536884307861346 54.2 -8.12188720703125 55 -8.081233024597168 55.8 -8.0277938842773437
		 56.6 -7.984299659729003 57.4 -7.9361600875854501 58.2 -7.8744153976440412 59 -7.8148560523986816
		 59.8 -7.7616353034973153 60.6 -7.7510347366332981 61.4 -7.7499418258666992 62.2 -7.7116107940673828
		 63 -7.662351608276369 63.8 -7.6196765899658194 64.6 -7.5856351852416992 65.4 -7.5434789657592765
		 66.2 -7.506486892700198 67 -7.473947525024415 67.8 -7.4477472305297852 68.6 -7.4295244216918945
		 69.4 -7.4042525291442853 70.2 -7.3841719627380371 71 -7.3642277717590332 71.8 -7.3413872718811044
		 72.6 -7.3308963775634766 73.4 -7.3245034217834482 74.2 -7.3232464790344238 75 -7.3207602500915518
		 75.8 -7.3323168754577646 76.6 -7.3762211799621582 77.4 -7.4327478408813468 78.2 -7.4786467552185059
		 79 -7.508781909942627 79.8 -7.5245456695556641 80.6 -7.527414321899415 81.4 -7.5351462364196768
		 82.2 -7.5483694076538095 83 -7.5649256706237802 83.8 -7.5843782424926749 84.6 -7.5987763404846191
		 85.4 -7.5873837471008292 86.2 -7.5250101089477548;
createNode animCurveTA -n "Bip01_L_Thigh_rotateY14";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 108 ".ktv[0:107]"  0.6 -179.45547485351562 1.4 -179.45712280273435
		 2.2 -179.47093200683594 3 -179.48599243164062 3.8 -179.50050354003906 4.6 -179.50825500488281
		 5.4 -179.4854736328125 6.2 -179.46168518066406 7 -179.43305969238281 7.8 -179.40396118164062
		 8.6 -179.37030029296875 9.4 -179.34957885742187 10.2 -179.3614501953125 11 -179.36647033691406
		 11.8 -179.33059692382812 12.6 -179.2960205078125 13.4 -179.28239440917972 14.2 -179.31001281738281
		 15 -179.33015441894531 15.8 -179.32864379882812 16.6 -179.29696655273435 17.4 -179.30307006835935
		 18.2 -179.33309936523435 19 -179.37904357910156 19.8 -179.43287658691406 20.6 -179.4642333984375
		 21.4 -179.48869323730469 22.2 -179.50411987304687 23 -179.54710388183594 23.8 -179.57598876953125
		 24.6 -179.59652709960935 25.4 -179.60894775390625 26.2 -179.63723754882812 27 -179.67277526855469
		 27.8 -179.7281494140625 28.6 -179.77922058105472 29.4 -179.79380798339847 30.2 -179.78137207031253
		 31 -179.77133178710935 31.8 -179.7677001953125 32.6 -179.73283386230469 33.4 -179.69525146484375
		 34.2 -179.69622802734375 35 -179.70816040039062 35.8 -179.68330383300781 36.6 -179.65074157714844
		 37.4 -179.62840270996094 38.2 -179.59146118164062 39 -179.54193115234378 39.8 -179.46678161621094
		 40.6 -179.39840698242187 41.4 -179.30644226074219 42.2 -179.21624755859375 43 -179.1571044921875
		 43.8 -179.11363220214844 44.6 -179.04417419433594 45.4 -178.96977233886719 46.2 -178.89762878417969
		 47 -178.82435607910156 47.8 -178.75291442871094 48.6 -178.68092346191406 49.4 -178.60069274902344
		 50.2 -178.55158996582031 51 -178.538818359375 51.8 -178.5330810546875 52.6 -178.5048828125
		 53.4 -178.48898315429687 54.2 -178.47898864746094 55 -178.46658325195312 55.8 -178.45477294921875
		 56.6 -178.44429016113281 57.4 -178.4373779296875 58.2 -178.43292236328125 59 -178.4100341796875
		 59.8 -178.40155029296875 60.6 -178.43576049804687 61.4 -178.46539306640625 62.2 -178.45579528808594
		 63 -178.42588806152344 63.8 -178.38990783691406 64.6 -178.3548583984375 65.4 -178.30850219726562
		 66.2 -178.24781799316406 67 -178.18257141113281 67.8 -178.12083435058594 68.6 -178.05043029785156
		 69.4 -177.966552734375 70.2 -177.86909484863281 71 -177.77139282226565 71.8 -177.67198181152344
		 72.6 -177.57008361816406 73.4 -177.4683837890625 74.2 -177.35853576660156 75 -177.25450134277344
		 75.8 -177.16412353515625 76.6 -177.10682678222656 77.4 -177.0546875 78.2 -176.98377990722656
		 79 -176.89242553710935 79.8 -176.78993225097656 80.6 -176.6890869140625 81.4 -176.57179260253906
		 82.2 -176.46656799316406 83 -176.36174011230469 83.8 -176.26124572753906 84.6 -176.15495300292969
		 85.4 -176.01115417480469 86.2 -175.81103515625;
createNode animCurveTA -n "Bip01_L_Thigh_rotateZ14";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 108 ".ktv[0:107]"  0.6 -85.884841918945313 1.4 -85.919868469238281
		 2.2 -85.95538330078125 3 -85.98947906494142 3.8 -86.048248291015625 4.6 -86.15618896484375
		 5.4 -86.252166748046875 6.2 -86.330032348632813 7 -86.394729614257813 7.8 -86.459205627441406
		 8.6 -86.522697448730469 9.4 -86.55914306640625 10.2 -86.563026428222656 11 -86.588790893554688
		 11.8 -86.664329528808594 12.6 -86.737411499023438 13.4 -86.788009643554688 14.2 -86.802330017089844
		 15 -86.826210021972656 15.8 -86.9052734375 16.6 -86.998817443847642 17.4 -87.052970886230469
		 18.2 -87.0560302734375 19 -87.036087036132813 19.8 -87.036659240722656 20.6 -87.043403625488281
		 21.4 -87.03253173828125 22.2 -87.002586364746094 23 -86.989151000976563 23.8 -86.987838745117188
		 24.6 -86.966583251953139 25.4 -86.92486572265625 26.2 -86.8731689453125 27 -86.831039428710938
		 27.8 -86.778343200683594 28.6 -86.723617553710937 29.4 -86.665153503417983 30.2 -86.614852905273438
		 31 -86.533905029296875 31.8 -86.461692810058594 32.6 -86.3956298828125 33.4 -86.351593017578125
		 34.2 -86.295280456542997 35 -86.21728515625 35.8 -86.115264892578125 36.6 -85.994232177734375
		 37.4 -85.88150787353517 38.2 -85.781570434570327 39 -85.704551696777344 39.8 -85.616363525390625
		 40.6 -85.504264831542983 41.4 -85.373588562011719 42.2 -85.2574462890625 43 -85.167236328125
		 43.8 -85.075218200683594 44.6 -84.971267700195313 45.4 -84.897468566894531 46.2 -84.820014953613295
		 47 -84.739692687988295 47.8 -84.646034240722656 48.6 -84.53836822509767 49.4 -84.451705932617188
		 50.2 -84.426132202148438 51 -84.447883605957017 51.8 -84.40570068359375 52.6 -84.4228515625
		 53.4 -84.418510437011719 54.2 -84.39996337890625 55 -84.360671997070327 55.8 -84.37799072265625
		 56.6 -84.41241455078125 57.4 -84.477386474609375 58.2 -84.533638000488281 59 -84.594917297363281
		 59.8 -84.6759033203125 60.6 -84.76436614990233 61.4 -84.863540649414063 62.2 -84.94451904296875
		 63 -85.040847778320327 63.8 -85.1693115234375 64.6 -85.284111022949219 65.4 -85.373237609863281
		 66.2 -85.466094970703125 67 -85.568603515625 67.8 -85.682830810546875 68.6 -85.797088623046875
		 69.4 -85.892135620117187 70.2 -85.981941223144531 71 -86.066474914550781 71.8 -86.105049133300781
		 72.6 -86.136138916015639 73.4 -86.170944213867188 74.2 -86.20135498046875 75 -86.22088623046875
		 75.8 -86.261428833007813 76.6 -86.292137145996094 77.4 -86.271430969238281 78.2 -86.16668701171875
		 79 -86.053085327148438 79.8 -85.995491027832031 80.6 -85.95145416259767 81.4 -85.88238525390625
		 82.2 -85.752937316894531 83 -85.601448059082031 83.8 -85.473228454589844 84.6 -85.351295471191406
		 85.4 -85.220672607421875 86.2 -85.089698791503906;
createNode animCurveTA -n "Bip01_L_Calf_rotateX14";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 108 ".ktv[0:107]"  0.6 0.91036480665206909 1.4 0.91064536571502686
		 2.2 0.91089117527008057 3 0.91112273931503307 3.8 0.91141784191131581 4.6 0.91180044412612915
		 5.4 0.91215336322784435 6.2 0.91244930028915405 7 0.91277307271957397 7.8 0.91308087110519387
		 8.6 0.9132891893386843 9.4 0.91333305835723888 10.2 0.91326540708541859 11 0.91332060098648071
		 11.8 0.9135589599609375 12.6 0.9137427806854248 13.4 0.91393846273422252 14.2 0.91389816999435403
		 15 0.91390210390090976 15.8 0.91407489776611328 16.6 0.91425526142120372 17.4 0.91433197259902976
		 18.2 0.91430550813674949 19 0.91422802209854126 19.8 0.91421502828598 20.6 0.91416823863983165
		 21.4 0.91406762599945068 22.2 0.91395217180252086 23 0.91391074657440174 23.8 0.91388106346130382
		 24.6 0.91386348009109486 25.4 0.91375821828842174 26.2 0.91359323263168324 27 0.91360390186309814
		 27.8 0.91348206996917714 28.6 0.91340553760528564 29.4 0.91333037614822388 30.2 0.91331750154495228
		 31 0.91326367855072021 31.8 0.91323447227478027 32.6 0.91316360235214245 33.4 0.91332155466079701
		 34.2 0.913338303565979 35 0.91325676441192638 35.8 0.91322547197341919 36.6 0.91309541463851951
		 37.4 0.9131019115447998 38.2 0.91297876834869374 39 0.91290223598480225 39.8 0.91287565231323253
		 40.6 0.91281014680862438 41.4 0.91271489858627308 42.2 0.91267317533493042 43 0.9125906229019165
		 43.8 0.91241300106048573 44.6 0.91223180294036865 45.4 0.91208440065383911 46.2 0.91194152832031261
		 47 0.91182541847229004 47.8 0.91167515516281117 48.6 0.91146230697631836 49.4 0.91125851869583141
		 50.2 0.91115111112594604 51 0.91108483076095559 51.8 0.91095316410064686 52.6 0.91085457801818859
		 53.4 0.91074568033218384 54.2 0.91056960821151733 55 0.91034173965454102 55.8 0.91022509336471569
		 56.6 0.91016000509262085 57.4 0.9101436734199525 58.2 0.91008937358856212 59 0.91020441055297863
		 59.8 0.91024655103683483 60.6 0.91026169061660789 61.4 0.91041475534439076 62.2 0.91061657667160034
		 63 0.91070920228958141 63.8 0.91111540794372559 64.6 0.91144931316375744 65.4 0.91167300939559937
		 66.2 0.91192156076431286 67 0.91230255365371693 67.8 0.91278570890426636 68.6 0.91328138113021851
		 69.4 0.91369396448135387 70.2 0.91397839784622203 71 0.91422623395919811 71.8 0.91445654630661
		 72.6 0.91478019952774048 73.4 0.91515320539474476 74.2 0.91544324159622203 75 0.9156506061553954
		 75.8 0.91587877273559581 76.6 0.91609680652618408 77.4 0.91615432500839245 78.2 0.91598218679428112
		 79 0.91579556465148926 79.8 0.9158085584640504 80.6 0.91592967510223378 81.4 0.91596341133117676
		 82.2 0.9158780574798584 83 0.91570025682449341 83.8 0.91557669639587413 84.6 0.91547763347625732
		 85.4 0.91538703441619851 86.2 0.91540175676345825;
createNode animCurveTA -n "Bip01_L_Calf_rotateY14";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 108 ".ktv[0:107]"  0.6 -0.22146047651767731 1.4 -0.22016535699367523
		 2.2 -0.21900686621665957 3 -0.2178808003664017 3.8 -0.2164011150598526 4.6 -0.21453791856765747
		 5.4 -0.21279053390026093 6.2 -0.21133348345756531 7 -0.2097385972738266 7.8 -0.20826201140880585
		 8.6 -0.2073415070772171 9.4 -0.20704959332942963 10.2 -0.20734569430351257 11 -0.20710116624832153
		 11.8 -0.20586445927619937 12.6 -0.20484258234500885 13.4 -0.20383629202842712 14.2 -0.20400798320770264
		 15 -0.20388023555278775 15.8 -0.20300258696079257 16.6 -0.20193745195865631 17.4 -0.201457679271698
		 18.2 -0.20162793993949893 19 -0.20208448171615601 19.8 -0.20221628248691559 20.6 -0.20225374400615692
		 21.4 -0.20270340144634247 22.2 -0.20336167514324188 23 -0.20371036231517792 23.8 -0.20371794700622561
		 24.6 -0.20374634861946103 25.4 -0.20427230000495911 26.2 -0.2051740437746048 27 -0.20508354902267456
		 27.8 -0.20547406375408173 28.6 -0.20590992271900177 29.4 -0.20624755322933197 30.2 -0.20645835995674133
		 31 -0.20674346387386319 31.8 -0.20702637732028961 32.6 -0.20747958123683927 33.4 -0.20677852630615232
		 34.2 -0.20660121738910675 35 -0.20692779123783112 35.8 -0.20701295137405396 36.6 -0.20775042474269867
		 37.4 -0.20786149799823761 38.2 -0.20857903361320496 39 -0.20881681144237521 39.8 -0.20877620577812195
		 40.6 -0.20930328965187073 41.4 -0.2099501043558121 42.2 -0.21017315983772281 43 -0.21059484779834747
		 43.8 -0.21144095063209539 44.6 -0.21227967739105225 45.4 -0.21306431293487549 46.2 -0.21381418406963348
		 47 -0.21441900730133057 47.8 -0.21515737473964691 48.6 -0.21632273495197296 49.4 -0.21737866103649139
		 50.2 -0.21779255568981171 51 -0.21797609329223633 51.8 -0.21841736137866977 52.6 -0.21887816488742828
		 53.4 -0.21934978663921356 54.2 -0.22024886310100555 55 -0.22123682498931885 55.8 -0.22176417708396912
		 56.6 -0.22193852066993713 57.4 -0.22207897901535031 58.2 -0.2224040180444718 59 -0.2218609154224396
		 59.8 -0.221588134765625 60.6 -0.22132273018360135 61.4 -0.22065144777297976 62.2 -0.21973550319671631
		 63 -0.21926632523536682 63.8 -0.21735294163227081 64.6 -0.21569548547267914 65.4 -0.21465624868869779
		 66.2 -0.21350876986980441 67 -0.21166352927684784 67.8 -0.2092384397983551 68.6 -0.20681324601173401
		 69.4 -0.20489446818828583 70.2 -0.20333534479141235 71 -0.20196276903152463 71.8 -0.20083978772163391
		 72.6 -0.19929760694503784 73.4 -0.19752335548400879 74.2 -0.1960412859916687 75 -0.19488085806369779
		 75.8 -0.19362153112888336 76.6 -0.19234846532344815 77.4 -0.19200389087200165 78.2 -0.19297103583812711
		 79 -0.19398555159568787 79.8 -0.19377817213535309 80.6 -0.19308498501777649 81.4 -0.19291216135025024
		 82.2 -0.19345949590206143 83 -0.19442254304885864 83.8 -0.19518233835697177 84.6 -0.19564847648143768
		 85.4 -0.19616970419883728 86.2 -0.19612798094749451;
createNode animCurveTA -n "Bip01_L_Calf_rotateZ14";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 108 ".ktv[0:107]"  0.6 -74.761283874511719 1.4 -74.836174011230469
		 2.2 -74.907249450683594 3 -74.957077026367188 3.8 -75.025497436523438 4.6 -75.119804382324219
		 5.4 -75.213653564453125 6.2 -75.287818908691406 7 -75.387680053710937 7.8 -75.471038818359375
		 8.6 -75.538246154785156 9.4 -75.542320251464844 10.2 -75.517738342285156 11 -75.540573120117187
		 11.8 -75.6038818359375 12.6 -75.639595031738281 13.4 -75.701416015625 14.2 -75.68194580078125
		 15 -75.667236328125 15.8 -75.711502075195313 16.6 -75.751083374023438 17.4 -75.76629638671875
		 18.2 -75.759056091308594 19 -75.740043640136719 19.8 -75.738861083984375 20.6 -75.715118408203125
		 21.4 -75.684555053710937 22.2 -75.65728759765625 23 -75.64508056640625 23.8 -75.6298828125
		 24.6 -75.61785888671875 25.4 -75.588005065917969 26.2 -75.553169250488281 27 -75.544242858886719
		 27.8 -75.496429443359375 28.6 -75.464897155761719 29.4 -75.44925689697267 30.2 -75.461654663085938
		 31 -75.443145751953125 31.8 -75.457649230957017 32.6 -75.45062255859375 33.4 -75.500411987304688
		 34.2 -75.498641967773438 35 -75.467453002929702 35.8 -75.446945190429702 36.6 -75.424835205078139
		 37.4 -75.440635681152358 38.2 -75.420425415039063 39 -75.378135681152358 39.8 -75.356246948242188
		 40.6 -75.359886169433594 41.4 -75.355682373046875 42.2 -75.351051330566406 43 -75.321693420410156
		 43.8 -75.270851135253906 44.6 -75.216072082519531 45.4 -75.185279846191406 46.2 -75.154441833496094
		 47 -75.125160217285156 47.8 -75.093856811523438 48.6 -75.051002502441406 49.4 -75.009391784667969
		 50.2 -74.971023559570327 51 -74.931167602539062 51.8 -74.877609252929702 52.6 -74.849105834960938
		 53.4 -74.816604614257813 54.2 -74.782127380371094 55 -74.707511901855469 55.8 -74.680831909179673
		 56.6 -74.644493103027344 57.4 -74.651870727539063 58.2 -74.640007019042983 59 -74.669746398925781
		 59.8 -74.673347473144531 60.6 -74.66461181640625 61.4 -74.696998596191406 62.2 -74.753021240234375
		 63 -74.780998229980469 63.8 -74.884834289550781 64.6 -74.970245361328125 65.4 -75.028343200683608
		 66.2 -75.10518646240233 67 -75.19842529296875 67.8 -75.332901000976563 68.6 -75.473602294921875
		 69.4 -75.597587585449219 70.2 -75.670806884765625 71 -75.732460021972656 71.8 -75.795524597167969
		 72.6 -75.914543151855469 73.4 -76.037078857421875 74.2 -76.126808166503906 75 -76.178749084472656
		 75.8 -76.236160278320327 76.6 -76.290641784667969 77.4 -76.311668395996094 78.2 -76.265235900878906
		 79 -76.208656311035156 79.8 -76.2020263671875 80.6 -76.231056213378906 81.4 -76.251029968261719
		 82.2 -76.225196838378906 83 -76.173301696777344 83.8 -76.14154052734375 84.6 -76.114471435546875
		 85.4 -76.091094970703125 86.2 -76.102180480957017;
createNode animCurveTA -n "Bip01_L_Foot_rotateX14";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 108 ".ktv[0:107]"  0.6 -12.913430213928224 1.4 -12.90187931060791
		 2.2 -12.889501571655272 3 -12.909873962402346 3.8 -12.897876739501951 4.6 -12.809207916259766
		 5.4 -12.598300933837892 6.2 -12.496328353881836 7 -12.383022308349608 7.8 -12.168318748474119
		 8.6 -11.952130317687988 9.4 -11.992186546325685 10.2 -12.223160743713381 11 -12.316608428955078
		 11.8 -12.103046417236328 12.6 -11.852590560913086 13.4 -11.778584480285645 14.2 -11.956611633300779
		 15 -12.13913631439209 15.8 -12.133092880249023 16.6 -11.931772232055664 17.4 -11.839238166809082
		 18.2 -12.003583908081056 19 -12.17704963684082 19.8 -12.252114295959473 20.6 -12.33922290802002
		 21.4 -12.471877098083496 22.2 -12.547624588012695 23 -12.584033966064451 23.8 -12.574709892272947
		 24.6 -12.456763267517092 25.4 -12.313030242919922 26.2 -12.247714042663574 27 -12.422492027282717
		 27.8 -12.851352691650392 28.6 -13.19176197052002 29.4 -13.118192672729492 30.2 -12.869036674499512
		 31 -12.811641693115234 31.8 -12.739896774291992 32.6 -12.378165245056152 33.4 -12.045112609863279
		 34.2 -12.22981643676758 35 -12.640139579772947 35.8 -12.819924354553224 36.6 -12.816860198974608
		 37.4 -12.818567276000977 38.2 -12.782732963562012 39 -12.72479248046875 39.8 -12.650923728942873
		 40.6 -12.560285568237305 41.4 -12.31832790374756 42.2 -12.169964790344238 43 -12.35478687286377
		 43.8 -12.540096282958984 44.6 -12.506932258605957 45.4 -12.4249267578125 46.2 -12.335614204406738
		 47 -12.239711761474608 47.8 -12.088540077209473 48.6 -11.860616683959959 49.4 -11.712018013000488
		 50.2 -11.881795883178713 51 -12.19625186920166 51.8 -12.326743125915527 52.6 -12.334478378295898
		 53.4 -12.333107948303224 54.2 -12.380824089050291 55 -12.401861190795898 55.8 -12.42255973815918
		 56.6 -12.433330535888672 57.4 -12.457779884338381 58.2 -12.395895957946776 59 -12.290780067443848
		 59.8 -12.429094314575195 60.6 -12.740395545959473 61.4 -12.944423675537108 62.2 -12.797122955322266
		 63 -12.612396240234377 63.8 -12.621148109436035 64.6 -12.719941139221191 65.4 -12.68150806427002
		 66.2 -12.642824172973633 67 -12.626089096069336 67.8 -12.589863777160645 68.6 -12.483399391174316
		 69.4 -12.32598876953125 70.2 -12.182723045349119 71 -12.078281402587894 71.8 -11.940313339233398
		 72.6 -11.751441955566406 73.4 -11.51804828643799 74.2 -11.295848846435549 75 -11.190789222717283
		 75.8 -11.225308418273928 76.6 -11.417960166931152 77.4 -11.60897159576416 78.2 -11.595918655395508
		 79 -11.554558753967283 79.8 -11.598023414611816 80.6 -11.605423927307127 81.4 -11.596714973449709
		 82.2 -11.637607574462892 83 -11.605656623840332 83.8 -11.618374824523926 84.6 -11.645819664001465
		 85.4 -11.522782325744627 86.2 -11.296809196472168;
createNode animCurveTA -n "Bip01_L_Foot_rotateY14";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 108 ".ktv[0:107]"  0.6 1.9339481592178345 1.4 1.9285598993301387
		 2.2 1.9431562423706059 3 1.959779620170593 3.8 1.9598292112350466 4.6 1.9037721157073972
		 5.4 1.8257583379745483 6.2 1.7555344104766846 7 1.7260184288024902 7.8 1.6507172584533691
		 8.6 1.5565263032913208 9.4 1.5928698778152466 10.2 1.6966269016265867 11 1.7647049427032473
		 11.8 1.6439656019210815 12.6 1.5498331785202026 13.4 1.5159419775009157 14.2 1.5986495018005371
		 15 1.6749544143676758 15.8 1.6726604700088501 16.6 1.5814796686172483 17.4 1.5172045230865481
		 18.2 1.6173360347747805 19 1.6863824129104614 19.8 1.7348034381866457 20.6 1.7678357362747192
		 21.4 1.8225592374801636 22.2 1.8645246028900144 23 1.8697432279586792 23.8 1.8376848697662356
		 24.6 1.7970967292785642 25.4 1.7151166200637815 26.2 1.7022343873977659 27 1.7751337289810181
		 27.8 1.9388389587402344 28.6 2.1027910709381104 29.4 2.071256160736084 30.2 1.940558910369873
		 31 1.8749357461929319 31.8 1.8740404844284053 32.6 1.7138357162475586 33.4 1.5487112998962402
		 34.2 1.6348559856414795 35 1.8041354417800903 35.8 1.8776159286499023 36.6 1.884784460067749
		 37.4 1.8508439064025877 38.2 1.8436102867126465 39 1.8142498731613161 39.8 1.7569332122802737
		 40.6 1.7333117723464966 41.4 1.6360317468643188 42.2 1.5767179727554319 43 1.6349296569824221
		 43.8 1.7343490123748779 44.6 1.7173560857772827 45.4 1.6665740013122561 46.2 1.6383588314056396
		 47 1.5805313587188721 47.8 1.540412425994873 48.6 1.4355992078781128 49.4 1.3633772134780884
		 50.2 1.4687134027481079 51 1.5850545167922974 51.8 1.6527880430221558 52.6 1.6458550691604614
		 53.4 1.6463980674743652 54.2 1.6947309970855713 55 1.7082618474960327 55.8 1.725169897079468
		 56.6 1.7281321287155151 57.4 1.7446736097335815 58.2 1.7138464450836182 59 1.6964632272720337
		 59.8 1.7159324884414673 60.6 1.8760820627212529 61.4 1.9610818624496467 62.2 1.9046909809112549
		 63 1.8123557567596436 63.8 1.8255109786987307 64.6 1.8680131435394287 65.4 1.8575991392135613
		 66.2 1.857232332229614 67 1.8449001312255859 67.8 1.8343839645385744 68.6 1.7887948751449585
		 69.4 1.6922566890716553 70.2 1.6504240036010742 71 1.5938774347305298 71.8 1.5352708101272583
		 72.6 1.4566282033920288 73.4 1.3558754920959473 74.2 1.2333073616027832 75 1.1916564702987671
		 75.8 1.1946853399276731 76.6 1.2796534299850464 77.4 1.3695733547210691 78.2 1.3720625638961794
		 79 1.3512455224990845 79.8 1.3504706621170044 80.6 1.3656706809997561 81.4 1.3871556520462038
		 82.2 1.4062442779541016 83 1.3904869556427002 83.8 1.3944085836410522 84.6 1.3987365961074829
		 85.4 1.3701299428939819 86.2 1.270978569984436;
createNode animCurveTA -n "Bip01_L_Foot_rotateZ14";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 108 ".ktv[0:107]"  0.6 -9.7052202224731445 1.4 -9.6438817977905273
		 2.2 -9.5853815078735352 3 -9.5842142105102539 3.8 -9.6582908630371094 4.6 -9.7547454833984375
		 5.4 -9.8063774108886719 6.2 -9.8319072723388672 7 -9.8158426284790039 7.8 -9.7089786529541016
		 8.6 -9.6498308181762695 9.4 -9.6888322830200195 10.2 -9.6082496643066406 11 -9.5158271789550781
		 11.8 -9.6594696044921875 12.6 -9.8335790634155273 13.4 -9.8447246551513672 14.2 -9.8048448562622088
		 15 -9.8285045623779297 15.8 -9.9472112655639648 16.6 -10.142934799194336 17.4 -10.316098213195801
		 18.2 -10.277679443359377 19 -10.142765998840332 19.8 -10.05653190612793 20.6 -10.160594940185549
		 21.4 -10.1983642578125 22.2 -10.095249176025392 23 -10.065388679504396 23.8 -10.250511169433594
		 24.6 -10.425962448120115 25.4 -10.449639320373535 26.2 -10.414447784423828 27 -10.439785957336426
		 27.8 -10.504349708557127 28.6 -10.55521297454834 29.4 -10.555915832519531 30.2 -10.541573524475098
		 31 -10.488303184509276 31.8 -10.3935546875 32.6 -10.350272178649902 33.4 -10.395688056945801
		 34.2 -10.443480491638184 35 -10.453380584716797 35.8 -10.420270919799805 36.6 -10.312609672546388
		 37.4 -10.21682071685791 38.2 -10.24141788482666 39 -10.405899047851562 39.8 -10.534102439880373
		 40.6 -10.429972648620604 41.4 -10.218855857849119 42.2 -10.160508155822754 43 -10.221755027770998
		 43.8 -10.234101295471191 44.6 -10.175607681274414 45.4 -10.122429847717283 46.2 -10.086161613464355
		 47 -10.061138153076172 47.8 -10.014070510864258 48.6 -9.8860378265380859 49.4 -9.7884340286254883
		 50.2 -9.8221912384033203 51 -9.9317483901977539 51.8 -10.06051540374756 52.6 -10.179074287414553
		 53.4 -10.208887100219728 54.2 -10.153706550598145 55 -10.133868217468262 55.8 -10.179224014282228
		 56.6 -10.21516227722168 57.4 -10.211421966552734 58.2 -10.155792236328123 59 -10.133090972900392
		 59.8 -10.196866035461426 60.6 -10.263235092163086 61.4 -10.237293243408203 62.2 -10.195448875427246
		 63 -10.223670005798342 63.8 -10.259193420410156 64.6 -10.179582595825195 65.4 -10.077466011047363
		 66.2 -10.049796104431152 67 -10.04898166656494 67.8 -10.032912254333496 68.6 -9.9492788314819336
		 69.4 -9.8678512573242187 70.2 -9.9567537307739258 71 -10.099328994750977 71.8 -10.05482292175293
		 72.6 -9.8451251983642578 73.4 -9.6761960983276367 74.2 -9.6506166458129883 75 -9.7048091888427734
		 75.8 -9.7949113845825195 76.6 -9.8568334579467773 77.4 -9.8306217193603516 78.2 -9.7345905303955078
		 79 -9.7438020706176758 79.8 -9.8245677947998047 80.6 -9.8337106704711914 81.4 -9.7582015991210937
		 82.2 -9.6735458374023437 83 -9.6111154556274414 83.8 -9.5652856826782227 84.6 -9.5005054473876953
		 85.4 -9.4452114105224609 86.2 -9.465693473815918;
createNode animCurveTA -n "Bip01_L_Toe0_rotateX4";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 108 ".ktv[0:107]"  0.6 -6.009271146467654e-006 1.4 -7.1872559601615649e-006
		 2.2 -1.0420026228530332e-005 3 -4.6359805310203228e-006 3.8 -5.9146382227481809e-006
		 4.6 -6.7356681938690599e-006 5.4 -6.3070478972804267e-006 6.2 -6.3257402871386148e-006
		 7 -8.1988182500936091e-006 7.8 -3.9081337490642917e-006 8.6 -2.894529416153091e-006
		 9.4 -7.4623376349336468e-006 10.2 -8.8319284259341657e-006 11 -6.5622716647339985e-006
		 11.8 -7.5295133683539461e-006 12.6 -3.6033823107572975e-006 13.4 -8.1501757449586876e-006
		 14.2 -1.4462232229561778e-006 15 -1.5227906260406598e-006 15.8 -3.8321450119838119e-006
		 16.6 -6.1692503550148103e-006 17.4 -5.9476419664861169e-006 18.2 -6.4155633481277619e-006
		 19 -6.4600449150020722e-006 19.8 -5.6419139582430944e-006 20.6 -8.934239303926006e-006
		 21.4 -3.9745123103784863e-006 22.2 -9.4821125458111055e-006 23 -3.2636125979479398e-006
		 23.8 -4.1381244955118746e-006 24.6 -4.421202447701944e-006 25.4 -3.2674299745849567e-006
		 26.2 -5.3607768677466083e-006 27 -9.6020648925332353e-006 27.8 -6.0992197177256458e-006
		 28.6 -5.0975390877283644e-006 29.4 -5.6951175793074071e-006 30.2 -3.3537116905790754e-006
		 31 -6.7968139774166048e-006 31.8 -8.4723114923690446e-006 32.6 -4.6571817620133524e-006
		 33.4 -7.0735213739681049e-006 34.2 -7.9296096373582259e-006 35 -3.774297738345922e-006
		 35.8 -6.2354474721360029e-006 36.6 -6.4631972236384172e-006 37.4 -5.2260415941418614e-006
		 38.2 -3.9883634599391363e-006 39 -5.5577261264261324e-006 39.8 -7.0871401476324536e-006
		 40.6 -8.5255451267585158e-006 41.4 -7.040704076644034e-006 42.2 -5.9632934608089272e-006
		 43 -6.362904969137162e-006 43.8 -7.3232376962550924e-006 44.6 -1.9368380890227858e-006
		 45.4 -4.5482061068469193e-006 46.2 -5.9507951846171636e-006 47 -3.8884613786649425e-006
		 47.8 -4.7424073272850364e-006 48.6 -4.1204502849723212e-006 49.4 -9.8366217571310699e-006
		 50.2 -2.6129080197279109e-006 51 -6.4805581132532097e-006 51.8 -4.2181773096672259e-006
		 52.6 -4.601650289259851e-006 53.4 -3.8821526686660945e-006 54.2 -1.6253079593298023e-006
		 55 -5.7933007155952509e-006 55.8 -7.1937138272915035e-006 56.6 -3.2615478176012402e-006
		 57.4 -7.5702505455410557e-006 58.2 -7.2078591983881771e-006 59 -6.888698862894671e-006
		 59.8 -7.2383295446343263e-006 60.6 -7.2787511271599215e-006 61.4 -5.4225138228503064e-006
		 62.2 -9.7346419352106767e-006 63 -2.6541235342847358e-007 63.8 -9.3998169177211838e-006
		 64.6 -3.9784108594176359e-006 65.4 -6.8614217525464483e-006 66.2 -8.6099562395247631e-006
		 67 -2.5253298190364148e-006 67.8 1.2013892956019845e-006 68.6 -3.9409501368936617e-006
		 69.4 -2.8355075301078614e-006 70.2 -5.6576454880996607e-006 71 -3.9658984860579957e-006
		 71.8 -1.5435903151228558e-006 72.6 -6.2357767092180438e-006 73.4 -4.7119024202402224e-006
		 74.2 -8.5779329310753383e-006 75 -2.6376403639005734e-006 75.8 -4.2154706534347497e-006
		 76.6 -3.9085821299522649e-006 77.4 -8.7424223238485865e-006 78.2 -2.7234007120569004e-006
		 79 -2.2262977381615201e-006 79.8 -4.7818984967307188e-006 80.6 -5.660760507453233e-006
		 81.4 -3.0512226203427417e-006 82.2 -8.1409916674601845e-006 83 -3.3325977710774168e-006
		 83.8 -4.2966948967659846e-006 84.6 -5.8523719417280518e-006 85.4 -3.0231810796976788e-006
		 86.2 -5.310347205522703e-006;
createNode animCurveTA -n "Bip01_L_Toe0_rotateY4";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 108 ".ktv[0:107]"  0.6 1.2213695299578832e-005 1.4 1.5297169738914818e-005
		 2.2 7.513368018408074e-006 3 2.2760773390473336e-006 3.8 6.6012116803904064e-006
		 4.6 -4.2476140151848085e-006 5.4 8.7122225522762164e-006 6.2 1.5730879567854572e-006
		 7 7.3054875429079411e-006 7.8 -2.9888556127843913e-006 8.6 8.1219168350799026e-006
		 9.4 7.3283613346575302e-006 10.2 -4.9090294851339422e-006 11 5.6021772252279334e-006
		 11.8 6.1524855254901922e-007 12.6 8.5548217612085864e-006 13.4 1.0450003173900768e-005
		 14.2 1.4060879038879648e-006 15 2.9039333639957476e-006 15.8 9.6786170615814626e-006
		 16.6 4.056318175571505e-006 17.4 -6.793326292608981e-007 18.2 -2.8606821160792606e-006
		 19 4.7650196393078659e-006 19.8 4.6972236305009574e-006 20.6 5.153366601007292e-006
		 21.4 -5.5230370890058111e-006 22.2 -4.3938916860497546e-007 23 8.4951841472502565e-007
		 23.8 6.873989377709222e-007 24.6 3.8429366213676985e-006 25.4 1.0277369256073143e-005
		 26.2 8.0684912973083556e-006 27 8.5256024249247275e-006 27.8 6.5525669015187304e-006
		 28.6 8.083333341346588e-006 29.4 -6.4710511651355773e-006 30.2 5.2141213018330745e-006
		 31 5.7077586461673491e-006 31.8 5.9648732531059068e-006 32.6 4.6988925532787107e-006
		 33.4 -3.4036800116155064e-006 34.2 -4.3612235458567739e-006 35 8.4707033920494734e-007
		 35.8 7.0354485615098383e-006 36.6 1.7583765838935506e-006 37.4 9.3005410235491581e-006
		 38.2 2.7300893634674139e-006 39 7.4965837484342055e-006 39.8 4.994135451852344e-006
		 40.6 6.3313927967101336e-006 41.4 3.962134371704451e-007 42.2 8.7458556663477793e-006
		 43 1.801916710064688e-006 43.8 5.4008946790418122e-006 44.6 2.0982444766559638e-005
		 45.4 1.0560103874013294e-005 46.2 8.0244162745657377e-006 47 -3.1628455872123595e-006
		 47.8 -3.8165098885656334e-006 48.6 2.585310767244664e-006 49.4 7.0361688813136425e-006
		 50.2 7.9896317402017286e-006 51 4.6811787797196303e-006 51.8 1.3051167115918361e-006
		 52.6 -7.3738519859034568e-006 53.4 8.0607787822373211e-006 54.2 7.4324557317595463e-006
		 55 5.22643586009508e-006 55.8 4.4824842007074039e-006 56.6 6.4338105403294321e-006
		 57.4 5.1758534027612768e-006 58.2 3.3701126085361466e-006 59 -1.8362832179263939e-006
		 59.8 9.4543847808381543e-006 60.6 -2.5529914182698121e-006 61.4 8.8340057118330168e-006
		 62.2 3.8086725453467807e-006 63 6.6508305280876812e-006 63.8 7.5827870205102971e-006
		 64.6 3.5076450330961974e-006 65.4 2.6372933916718466e-006 66.2 1.5762510656713855e-006
		 67 7.8134480645530874e-006 67.8 1.0198376003245356e-005 68.6 5.4003262448532041e-006
		 69.4 4.7311568778241053e-006 70.2 6.3451861933572218e-006 71 2.4168323875528591e-007
		 71.8 7.0714199864596594e-006 72.6 1.1445446943980642e-005 73.4 7.5303732955944733e-006
		 74.2 -2.1964729057799559e-006 75 -7.1945862600841792e-007 75.8 -9.5078812591964379e-006
		 76.6 8.0708523455541581e-006 77.4 2.526091975596501e-006 78.2 3.4825893635570537e-006
		 79 -2.8010797450406244e-006 79.8 2.2701069610775448e-006 80.6 6.3976881392591167e-006
		 81.4 2.3792449610482436e-006 82.2 2.5561407710483763e-006 83 4.4755170165444724e-006
		 83.8 1.9317483292979891e-006 84.6 3.4607865018188022e-006 85.4 4.6792401917628013e-006
		 86.2 7.7101179840610722e-007;
createNode animCurveTA -n "Bip01_L_Toe0_rotateZ4";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 108 ".ktv[0:107]"  0.6 89.999977111816406 1.4 89.999977111816406
		 2.2 89.999961853027344 3 89.999977111816406 3.8 89.999961853027344 4.6 89.999977111816406
		 5.4 89.999961853027344 6.2 89.999977111816406 7 89.999961853027344 7.8 89.999977111816406
		 8.6 89.999977111816406 9.4 89.999977111816406 10.2 89.999961853027344 11 89.999961853027344
		 11.8 89.999977111816406 12.6 89.999977111816406 13.4 89.999977111816406 14.2 89.999977111816406
		 15 89.999977111816406 15.8 89.999977111816406 16.6 89.999961853027344 17.4 89.999977111816406
		 18.2 89.999977111816406 19 89.999961853027344 19.8 89.999977111816406 20.6 89.999977111816406
		 21.4 89.999977111816406 22.2 89.999977111816406 23 89.999977111816406 23.8 89.999977111816406
		 24.6 89.999977111816406 25.4 89.999977111816406 26.2 89.999977111816406 27 89.999961853027344
		 27.8 89.999977111816406 28.6 89.999977111816406 29.4 89.999977111816406 30.2 89.999961853027344
		 31 89.999977111816406 31.8 89.999977111816406 32.6 89.999961853027344 33.4 89.999977111816406
		 34.2 89.999977111816406 35 89.999977111816406 35.8 89.999977111816406 36.6 89.999977111816406
		 37.4 89.999977111816406 38.2 89.999984741210937 39 89.999977111816406 39.8 89.999961853027344
		 40.6 89.999977111816406 41.4 89.999977111816406 42.2 89.999977111816406 43 89.999961853027344
		 43.8 89.999977111816406 44.6 89.999977111816406 45.4 89.999977111816406 46.2 89.999977111816406
		 47 89.999977111816406 47.8 89.999977111816406 48.6 89.999977111816406 49.4 89.999977111816406
		 50.2 89.999977111816406 51 89.999977111816406 51.8 89.999984741210937 52.6 89.999977111816406
		 53.4 89.999977111816406 54.2 89.999977111816406 55 89.999977111816406 55.8 89.999977111816406
		 56.6 89.999977111816406 57.4 89.999977111816406 58.2 89.999977111816406 59 89.999977111816406
		 59.8 89.999977111816406 60.6 89.999977111816406 61.4 89.999977111816406 62.2 89.999961853027344
		 63 89.999977111816406 63.8 89.999977111816406 64.6 89.999977111816406 65.4 89.999977111816406
		 66.2 89.999977111816406 67 89.999977111816406 67.8 89.999977111816406 68.6 89.999977111816406
		 69.4 89.999977111816406 70.2 89.999977111816406 71 89.999977111816406 71.8 89.999977111816406
		 72.6 89.999977111816406 73.4 89.999977111816406 74.2 89.999961853027344 75 89.999977111816406
		 75.8 89.999977111816406 76.6 89.999977111816406 77.4 89.999977111816406 78.2 89.999977111816406
		 79 89.999984741210937 79.8 89.999961853027344 80.6 89.999977111816406 81.4 89.999977111816406
		 82.2 89.999977111816406 83 89.999977111816406 83.8 89.999977111816406 84.6 89.999977111816406
		 85.4 89.999977111816406 86.2 89.999977111816406;
createNode animCurveTA -n "Bip01_R_Thigh_rotateX14";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 108 ".ktv[0:107]"  0.6 -3.7104318141937256 1.4 -3.6360373497009282
		 2.2 -3.5633640289306645 3 -3.4875216484069824 3.8 -3.438617467880249 4.6 -3.3821456432342529
		 5.4 -3.3044452667236328 6.2 -3.1971566677093506 7 -3.1098942756652832 7.8 -3.0311284065246582
		 8.6 -2.9231295585632324 9.4 -2.8086555004119873 10.2 -2.7036619186401372 11 -2.6152791976928711
		 11.8 -2.538344144821167 12.6 -2.4680750370025635 13.4 -2.4092807769775391 14.2 -2.3771514892578125
		 15 -2.3201057910919189 15.8 -2.2913370132446289 16.6 -2.2549731731414795 17.4 -2.2325701713562012
		 18.2 -2.2175438404083252 19 -2.2461211681365967 19.8 -2.269890308380127 20.6 -2.306363582611084
		 21.4 -2.3419623374938965 22.2 -2.3602128028869629 23 -2.4090621471405029 23.8 -2.4535403251647949
		 24.6 -2.5203397274017334 25.4 -2.5692164897918701 26.2 -2.6295952796936035 27 -2.7052192687988281
		 27.8 -2.7861979007720947 28.6 -2.8779411315917969 29.4 -2.9582641124725342 30.2 -3.021484375
		 31 -3.106889009475708 31.8 -3.224040269851685 32.6 -3.3343615531921387 33.4 -3.4404754638671875
		 34.2 -3.5425729751586914 35 -3.6199727058410653 35.8 -3.6728930473327641 36.6 -3.7310042381286626
		 37.4 -3.7993335723876953 38.2 -3.8788642883300786 39 -3.9515373706817623 39.8 -4.0213146209716797
		 40.6 -4.0518045425415039 41.4 -4.0677051544189453 42.2 -4.0817480087280273 43 -4.1008725166320801
		 43.8 -4.1209535598754883 44.6 -4.1287670135498047 45.4 -4.1393404006958008 46.2 -4.1622352600097656
		 47 -4.1905393600463867 47.8 -4.1705408096313477 48.6 -4.1277561187744141 49.4 -4.0662298202514648
		 50.2 -4.016232967376709 51 -4.0032830238342285 51.8 -4.0077633857727051 52.6 -4.0180087089538574
		 53.4 -4.0201396942138672 54.2 -3.9929556846618661 55 -3.9650337696075431 55.8 -3.945581436157227
		 56.6 -3.9283967018127441 57.4 -3.9131245613098153 58.2 -3.9106655120849609 59 -3.8898472785949711
		 59.8 -3.884902000427247 60.6 -3.9048736095428471 61.4 -3.9480950832366943 62.2 -3.9694325923919673
		 63 -3.9671649932861328 63.8 -3.9650337696075431 64.6 -3.9642961025238046 65.4 -3.9471662044525151
		 66.2 -3.9285879135131845 67 -3.9494063854217529 67.8 -3.9768364429473886 68.6 -3.9788582324981694
		 69.4 -3.9865627288818359 70.2 -4.0103044509887695 71 -4.0282540321350098 71.8 -4.0241560935974121
		 72.6 -4.0237188339233398 73.4 -4.0442914962768555 74.2 -4.0692081451416016 75 -4.085026741027832
		 75.8 -4.1039872169494629 76.6 -4.1464438438415527 77.4 -4.175786018371582 78.2 -4.1824798583984375
		 79 -4.1681909561157227 79.8 -4.1603226661682129 80.6 -4.1561427116394043 81.4 -4.1422910690307617
		 82.2 -4.1345043182373047 83 -4.1016378402709961 83.8 -4.075136661529541 84.6 -4.0653009414672852
		 85.4 -4.0545368194580078 86.2 -3.9869997501373287;
createNode animCurveTA -n "Bip01_R_Thigh_rotateY14";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 108 ".ktv[0:107]"  0.6 166.17356872558594 1.4 166.16360473632812
		 2.2 166.14170837402344 3 166.12797546386719 3.8 166.09695434570312 4.6 166.07444763183594
		 5.4 166.08036804199219 6.2 166.10000610351562 7 166.09780883789062 7.8 166.09231567382812
		 8.6 166.11514282226562 9.4 166.13345336914062 10.2 166.14340209960935 11 166.1593017578125
		 11.8 166.17399597167969 12.6 166.18537902832031 13.4 166.19471740722656 14.2 166.19677734375
		 15 166.19071960449219 15.8 166.19966125488281 16.6 166.22270202636719 17.4 166.23355102539062
		 18.2 166.22689819335935 19 166.19587707519531 19.8 166.18959045410156 20.6 166.16893005371094
		 21.4 166.15414428710935 22.2 166.16131591796875 23 166.13552856445312 23.8 166.10635375976565
		 24.6 166.09837341308594 25.4 166.08584594726562 26.2 166.07827758789062 27 166.0697021484375
		 27.8 166.04444885253906 28.6 166.03038024902344 29.4 166.02975463867187 30.2 166.04737854003906
		 31 166.05506896972656 31.8 166.03935241699219 32.6 166.02763366699219 33.4 166.0390625
		 34.2 166.05996704101562 35 166.09713745117187 35.8 166.15045166015625 36.6 166.20643615722656
		 37.4 166.24229431152344 38.2 166.27848815917972 39 166.32376098632815 39.8 166.37237548828125
		 40.6 166.43133544921875 41.4 166.50981140136719 42.2 166.58537292480469 43 166.65727233886722
		 43.8 166.71945190429687 44.6 166.79502868652344 45.4 166.86222839355469 46.2 166.91874694824219
		 47 166.96372985839844 47.8 167.02716064453125 48.6 167.10844421386719 49.4 167.19671630859375
		 50.2 167.28605651855469 51 167.34474182128906 51.8 167.371337890625 52.6 167.39103698730469
		 53.4 167.41886901855469 54.2 167.44940185546875 55 167.48312377929687 55.8 167.50904846191406
		 56.6 167.54048156738281 57.4 167.5706787109375 58.2 167.59933471679687 59 167.64967346191409
		 59.8 167.67884826660156 60.6 167.69044494628906 61.4 167.687744140625 62.2 167.70732116699219
		 63 167.76631164550781 63.8 167.83200073242187 64.6 167.89248657226562 65.4 167.972412109375
		 66.2 168.04704284667969 67 168.10682678222656 67.8 168.1756591796875 68.6 168.25570678710935
		 69.4 168.34112548828125 70.2 168.42123413085935 71 168.50192260742187 71.8 168.59457397460935
		 72.6 168.69219970703125 73.4 168.76602172851565 74.2 168.85466003417969 75 168.95036315917969
		 75.8 169.03985595703125 76.6 169.10020446777344 77.4 169.14915466308594 78.2 169.2176513671875
		 79 169.306640625 79.8 169.40345764160156 80.6 169.50495910644531 81.4 169.61065673828125
		 82.2 169.71646118164062 83 169.82734680175781 83.8 169.93977355957031 84.6 170.04119873046875
		 85.4 170.14967346191406 86.2 170.30746459960935;
createNode animCurveTA -n "Bip01_R_Thigh_rotateZ14";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 108 ".ktv[0:107]"  0.6 -91.139335632324219 1.4 -91.120513916015625
		 2.2 -91.086875915527344 3 -91.064529418945313 3.8 -91.079963684082031 4.6 -91.133735656738281
		 5.4 -91.180099487304688 6.2 -91.200256347656236 7 -91.186653137207017 7.8 -91.186874389648438
		 8.6 -91.175750732421875 9.4 -91.145919799804687 10.2 -91.089553833007812 11 -91.057563781738281
		 11.8 -91.073631286621094 12.6 -91.101768493652358 13.4 -91.099662780761719 14.2 -91.105972290039063
		 15 -91.108840942382813 15.8 -91.150566101074219 16.6 -91.179084777832031 17.4 -91.193130493164062
		 18.2 -91.1851806640625 19 -91.210311889648438 19.8 -91.246429443359375 20.6 -91.283096313476563
		 21.4 -91.292716979980469 22.2 -91.318260192871094 23 -91.308097839355469 23.8 -91.291702270507813
		 24.6 -91.29052734375 25.4 -91.295280456542983 26.2 -91.300827026367188 27 -91.328315734863295
		 27.8 -91.332817077636719 28.6 -91.331809997558594 29.4 -91.333854675292983 30.2 -91.329734802246094
		 31 -91.306755065917969 31.8 -91.31905364990233 32.6 -91.338943481445312 33.4 -91.361831665039063
		 34.2 -91.382873535156236 35 -91.374458312988281 35.8 -91.334434509277344 36.6 -91.260307312011719
		 37.4 -91.190093994140625 38.2 -91.136360168457017 39 -91.102096557617188 39.8 -91.043876647949219
		 40.6 -90.944206237792983 41.4 -90.828697204589844 42.2 -90.736846923828125 43 -90.629859924316406
		 43.8 -90.530220031738281 44.6 -90.443855285644531 45.4 -90.371047973632812 46.2 -90.322883605957017
		 47 -90.247970581054688 47.8 -90.128875732421875 48.6 -89.981208801269531 49.4 -89.852470397949219
		 50.2 -89.801597595214844 51 -89.78094482421875 51.8 -89.75753021240233 52.6 -89.739089965820327
		 53.4 -89.691909790039063 54.2 -89.633827209472656 55 -89.567459106445327 55.8 -89.517494201660156
		 56.6 -89.527549743652358 57.4 -89.563911437988281 58.2 -89.588554382324219 59 -89.644012451171875
		 59.8 -89.667892456054688 60.6 -89.731658935546875 61.4 -89.805313110351563 62.2 -89.852775573730469
		 63 -89.928726196289062 63.8 -90.03131103515625 64.6 -90.132949829101563 65.4 -90.178764343261719
		 66.2 -90.222343444824219 67 -90.303153991699219 67.8 -90.397224426269531 68.6 -90.476043701171875
		 69.4 -90.566856384277344 70.2 -90.665809631347642 71 -90.72564697265625 71.8 -90.742202758789063
		 72.6 -90.734222412109375 73.4 -90.769874572753906 74.2 -90.831481933593764 75 -90.917953491210938
		 75.8 -90.982650756835938 76.6 -90.960792541503906 77.4 -90.865386962890625 78.2 -90.740043640136733
		 79 -90.622238159179673 79.8 -90.531120300292983 80.6 -90.469512939453125 81.4 -90.408615112304687
		 82.2 -90.289962768554688 83 -90.129287719726562 83.8 -89.958038330078125 84.6 -89.810096740722656
		 85.4 -89.662132263183594 86.2 -89.46563720703125;
createNode animCurveTA -n "Bip01_R_Calf_rotateX14";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 108 ".ktv[0:107]"  0.6 -0.91363894939422607 1.4 -0.91378468275070213
		 2.2 -0.91388958692550659 3 -0.91404926776885964 3.8 -0.91431093215942383 4.6 -0.91462373733520519
		 5.4 -0.91486120223999012 6.2 -0.91508060693740845 7 -0.91528576612472534 7.8 -0.91552406549453735
		 8.6 -0.91552299261093151 9.4 -0.91545200347900391 10.2 -0.91530472040176403 11 -0.9151892662048341
		 11.8 -0.91529202461242676 12.6 -0.91548198461532571 13.4 -0.91555488109588623 14.2 -0.91555500030517589
		 15 -0.91554814577102672 15.8 -0.91564488410949696 16.6 -0.91564786434173595 17.4 -0.91564208269119263
		 18.2 -0.91559481620788563 19 -0.91567528247833252 19.8 -0.91578811407089233 20.6 -0.91590815782546997
		 21.4 -0.91592425107955933 22.2 -0.91593986749649037 23 -0.91584241390228271 23.8 -0.91580313444137573
		 24.6 -0.9157753586769104 25.4 -0.91583800315856911 26.2 -0.91579854488372792 27 -0.91592115163803101
		 27.8 -0.91604262590408325 28.6 -0.91607838869094838 29.4 -0.91612893342971824 30.2 -0.91621452569961559
		 31 -0.91628265380859364 31.8 -0.91640460491180431 32.6 -0.91644012928009055 33.4 -0.91654241085052479
		 34.2 -0.91669839620590188 35 -0.91669660806655895 35.8 -0.91670930385589589 36.6 -0.91664665937423717
		 37.4 -0.91660058498382557 38.2 -0.91654473543167103 39 -0.91651070117950439 39.8 -0.91647142171859708
		 40.6 -0.91634368896484364 41.4 -0.91614621877670299 42.2 -0.91599500179290771 43 -0.91580688953399647
		 43.8 -0.91552639007568348 44.6 -0.91536074876785278 45.4 -0.91509985923767057 46.2 -0.91502344608306885
		 47 -0.91486024856567383 47.8 -0.91460627317428589 48.6 -0.91423892974853516 49.4 -0.91389048099517844
		 50.2 -0.91375768184661876 51 -0.91359108686447155 51.8 -0.91343390941619884 52.6 -0.91333776712417603
		 53.4 -0.91312015056610107 54.2 -0.91291695833206177 55 -0.91264194250106812 55.8 -0.91239798069000244
		 56.6 -0.91230201721191395 57.4 -0.91227716207504272 58.2 -0.91225850582122803 59 -0.91225218772888195
		 59.8 -0.91218727827072144 60.6 -0.91225111484527588 61.4 -0.91224950551986694 62.2 -0.91227096319198597
		 63 -0.91238868236541759 63.8 -0.91266924142837536 64.6 -0.91295868158340443 65.4 -0.91305774450302124
		 66.2 -0.91311490535736095 67 -0.91333466768264748 67.8 -0.91361266374588046 68.6 -0.91385459899902333
		 69.4 -0.91411525011062644 70.2 -0.91441541910171509 71 -0.91450178623199452 71.8 -0.91459912061691273
		 72.6 -0.91474574804306041 73.4 -0.91499036550521862 74.2 -0.91525542736053467 75 -0.91560667753219616
		 75.8 -0.91590631008148193 76.6 -0.91588604450225852 77.4 -0.91561496257781971 78.2 -0.91530871391296387
		 79 -0.91510516405105591 79.8 -0.91500931978225697 80.6 -0.91497325897216797 81.4 -0.91493380069732655
		 82.2 -0.9147396683692931 83 -0.91439151763916005 83.8 -0.91399300098419189 84.6 -0.91383254528045654
		 85.4 -0.91362696886062622 86.2 -0.91335344314575195;
createNode animCurveTA -n "Bip01_R_Calf_rotateY14";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 108 ".ktv[0:107]"  0.6 0.19879825413227081 1.4 0.1981203705072403
		 2.2 0.19769468903541565 3 0.19691629707813263 3.8 0.19548565149307251 4.6 0.19389866292476657
		 5.4 0.19248507916927335 6.2 0.19135412573814392 7 0.19032129645347595 7.8 0.18930716812610623
		 8.6 0.18914668262004852 9.4 0.18952895700931549 10.2 0.19021299481391907 11 0.19078688323497772
		 11.8 0.19008499383926392 12.6 0.18902298808097839 13.4 0.18874111771583557 14.2 0.18881228566169739
		 15 0.18881793320178983 15.8 0.1882721483707428 16.6 0.18803076446056369 17.4 0.1881522536277771
		 18.2 0.18846389651298523 19 0.18809306621551511 19.8 0.1873747706413269 20.6 0.1865956038236618
		 21.4 0.18658246099948883 22.2 0.1865243464708328 23 0.1870439350605011 23.8 0.18733970820903778
		 24.6 0.18744246661663058 25.4 0.18706345558166504 26.2 0.1872183233499527 27 0.18653357028961184
		 27.8 0.1858461797237396 28.6 0.18569132685661316 29.4 0.18545904755592343 30.2 0.18513645231723783
		 31 0.18483196198940277 31.8 0.18408896028995511 32.6 0.1837787330150604 33.4 0.1831275075674057
		 34.2 0.18224720656871796 35 0.18220195174217224 35.8 0.18202134966850281 36.6 0.18223673105239868
		 37.4 0.18249453604221344 38.2 0.18281528353691104 39 0.18293952941894531 39.8 0.18316277861595151
		 40.6 0.18401919305324552 41.4 0.18500465154647827 42.2 0.18580798804759979 43 0.1868153661489487
		 43.8 0.18831558525562292 44.6 0.18905448913574219 45.4 0.19027695059776303 46.2 0.19067791104316711
		 47 0.19159777462482452 47.8 0.19308033585548401 48.6 0.19506189227104187 49.4 0.19674259424209595
		 50.2 0.19740408658981323 51 0.19813156127929688 51.8 0.19887553155422211 52.6 0.1994938850402832
		 53.4 0.20074979960918429 54.2 0.20179133117198944 55 0.20327962934970856 55.8 0.20452402532100675
		 56.6 0.20509417355060577 57.4 0.20524564385414124 58.2 0.20521041750907895 59 0.2053709477186203
		 59.8 0.20573870837688449 60.6 0.20544585585594177 61.4 0.20553898811340332 62.2 0.20546895265579224
		 63 0.20498386025428775 63.8 0.20358812808990481 64.6 0.20218606293201449 65.4 0.20179943740367887
		 66.2 0.20168547332286837 67 0.20074398815631864 67.8 0.19938619434833532 68.6 0.19797979295253751
		 69.4 0.19667501747608185 70.2 0.19513300061225891 71 0.19469529390335083 71.8 0.19443723559379575
		 72.6 0.19394123554229736 73.4 0.19292245805263519 74.2 0.19151651859283447 75 0.18949949741363523
		 75.8 0.18789577484130859 76.6 0.18830165266990664 77.4 0.19003100693225863 78.2 0.19172781705856326
		 79 0.19304953515529633 79.8 0.19381284713745117 80.6 0.19403164088726044 81.4 0.19430045783519745
		 82.2 0.19535638391971588 83 0.19730934500694275 83.8 0.19936470687389377 84.6 0.2003280967473984
		 85.4 0.2016255110502243 86.2 0.20306460559368136;
createNode animCurveTA -n "Bip01_R_Calf_rotateZ14";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 108 ".ktv[0:107]"  0.6 -74.820671081542983 1.4 -74.876754760742188
		 2.2 -74.922027587890625 3 -74.972328186035156 3.8 -75.043380737304688 4.6 -75.138679504394531
		 5.4 -75.198959350585937 6.2 -75.268898010253906 7 -75.348892211914063 7.8 -75.439643859863281
		 8.6 -75.42549896240233 9.4 -75.392547607421875 10.2 -75.343704223632813 11 -75.302635192871094
		 11.8 -75.321640014648452 12.6 -75.378936767578125 13.4 -75.404495239257813 14.2 -75.420783996582031
		 15 -75.414588928222656 15.8 -75.437026977539063 16.6 -75.423538208007812 17.4 -75.421836853027344
		 18.2 -75.42313385009767 19 -75.457099914550781 19.8 -75.475570678710938 20.6 -75.504913330078125
		 21.4 -75.511550903320341 22.2 -75.517265319824219 23 -75.489006042480469 23.8 -75.478713989257827
		 24.6 -75.4697265625 25.4 -75.486198425292983 26.2 -75.468048095703125 27 -75.507133483886719
		 27.8 -75.541671752929702 28.6 -75.558509826660156 29.4 -75.577568054199219 30.2 -75.616264343261719
		 31 -75.644805908203125 31.8 -75.674919128417969 32.6 -75.677001953125 33.4 -75.708633422851563
		 34.2 -75.753570556640639 35 -75.743606567382812 35.8 -75.737846374511719 36.6 -75.709274291992188
		 37.4 -75.694244384765625 38.2 -75.668350219726562 39 -75.659584045410156 39.8 -75.646469116210938
		 40.6 -75.618705749511719 41.4 -75.548454284667969 42.2 -75.497825622558594 43 -75.428131103515625
		 43.8 -75.340736389160156 44.6 -75.278289794921875 45.4 -75.176399230957031 46.2 -75.152351379394531
		 47 -75.106307983398438 47.8 -75.042510986328125 48.6 -74.931877136230469 49.4 -74.819282531738281
		 50.2 -74.778633117675781 51 -74.710563659667969 51.8 -74.656623840332031 52.6 -74.643852233886719
		 53.4 -74.590744018554688 54.2 -74.534217834472656 55 -74.46944427490233 55.8 -74.399009704589844
		 56.6 -74.383140563964844 57.4 -74.37286376953125 58.2 -74.358016967773438 59 -74.369758605957017
		 59.8 -74.349418640136719 60.6 -74.375564575195327 61.4 -74.390205383300781 62.2 -74.395637512207017
		 63 -74.443229675292983 63.8 -74.525337219238281 64.6 -74.616050720214844 65.4 -74.659568786621094
		 66.2 -74.692703247070327 67 -74.776260375976563 67.8 -74.86175537109375 68.6 -74.923820495605469
		 69.4 -74.999412536621094 70.2 -75.087821960449219 71 -75.124427795410156 71.8 -75.17620849609375
		 72.6 -75.255134582519531 73.4 -75.354217529296875 74.2 -75.434898376464844 75 -75.530075073242187
		 75.8 -75.625396728515625 76.6 -75.645698547363281 77.4 -75.584068298339844 78.2 -75.500152587890625
		 79 -75.4664306640625 79.8 -75.461982727050781 80.6 -75.454925537109375 81.4 -75.446151733398437
		 82.2 -75.395515441894531 83 -75.302177429199219 83.8 -75.189460754394531 84.6 -75.161972045898437
		 85.4 -75.122909545898438 86.2 -75.050048828125;
createNode animCurveTA -n "Bip01_R_Foot_rotateX14";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 108 ".ktv[0:107]"  0.6 14.817420005798342 1.4 14.793118476867676
		 2.2 14.764103889465332 3 14.726728439331056 3.8 14.625541687011715 4.6 14.542414665222168
		 5.4 14.63031005859375 6.2 14.778993606567385 7 14.731759071350098 7.8 14.605639457702638
		 8.6 14.725082397460939 9.4 14.934720039367674 10.2 15.097464561462402 11 15.222250938415526
		 11.8 15.288301467895508 12.6 15.319003105163574 13.4 15.337894439697267 14.2 15.340462684631349
		 15 15.333030700683594 15.8 15.429211616516113 16.6 15.519522666931151 17.4 15.574324607849125
		 18.2 15.550473213195804 19 15.544693946838382 19.8 15.604093551635742 20.6 15.591826438903809
		 21.4 15.532435417175289 22.2 15.504504203796383 23 15.46365547180176 23.8 15.440158843994142
		 24.6 15.470823287963867 25.4 15.544961929321293 26.2 15.591681480407718 27 15.548564910888672
		 27.8 15.372303009033203 28.6 15.236622810363771 29.4 15.214879035949707 30.2 15.262036323547363
		 31 15.181859970092775 31.8 14.92866325378418 32.6 14.674860000610352 33.4 14.560676574707031
		 34.2 14.491596221923828 35 14.509458541870117 35.8 14.621496200561522 36.6 14.71164608001709
		 37.4 14.766842842102054 38.2 14.792710304260254 39 14.747404098510742 39.8 14.677218437194824
		 40.6 14.698525428771973 41.4 14.875662803649902 42.2 14.998985290527344 43 15.060403823852541
		 43.8 15.132704734802246 44.6 15.271583557128906 45.4 15.333615303039553 46.2 15.218538284301758
		 47 14.991096496582033 47.8 15.056942939758301 48.6 15.386212348937988 49.4 15.741551399230957
		 50.2 16.038019180297852 51 16.192245483398438 51.8 16.009195327758789 52.6 15.759576797485353
		 53.4 15.669528961181642 54.2 15.658964157104494 55 15.644443511962894 55.8 15.603062629699709
		 56.6 15.586449623107912 57.4 15.588032722473143 58.2 15.556451797485353 59 15.402706146240234
		 59.8 15.301952362060549 60.6 15.179179191589354 61.4 14.96077346801758 62.2 14.857735633850098
		 63 14.811911582946779 63.8 14.723122596740721 64.6 14.660730361938477 65.4 14.678931236267092
		 66.2 14.568182945251465 67 14.248551368713381 67.8 14.06248664855957 68.6 14.125467300415041
		 69.4 14.097090721130373 70.2 13.803924560546877 71 13.562299728393556 71.8 13.46696949005127
		 72.6 13.304303169250488 73.4 13.024511337280272 74.2 12.823390007019045 75 12.738539695739746
		 75.8 12.604462623596191 76.6 12.280707359313965 77.4 11.992799758911135 78.2 11.836524963378906
		 79 11.747122764587402 79.8 11.599947929382324 80.6 11.477285385131836 81.4 11.389772415161133
		 82.2 11.335238456726074 83 11.425662994384767 83.8 11.470048904418944 84.6 11.301011085510254
		 85.4 11.083035469055176 86.2 11.028775215148926;
createNode animCurveTA -n "Bip01_R_Foot_rotateY14";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 108 ".ktv[0:107]"  0.6 -3.6771321296691895 1.4 -3.6455094814300537
		 2.2 -3.6468741893768306 3 -3.6320164203643803 3.8 -3.5910725593566895 4.6 -3.5592312812805176
		 5.4 -3.574084997177124 6.2 -3.6311378479003902 7 -3.6287627220153817 7.8 -3.5762150287628174
		 8.6 -3.5844237804412846 9.4 -3.6808340549468994 10.2 -3.7517292499542245 11 -3.8012323379516602
		 11.8 -3.8061943054199219 12.6 -3.825713872909545 13.4 -3.8448207378387451 14.2 -3.8396718502044682
		 15 -3.825436830520629 15.8 -3.8310520648956294 16.6 -3.8820559978485103 17.4 -3.8964536190032963
		 18.2 -3.9148178100585942 19 -3.9137225151062021 19.8 -3.9433314800262451 20.6 -3.9196996688842769
		 21.4 -3.9001269340515146 22.2 -3.8836901187896737 23 -3.868701696395874 23.8 -3.8820219039917001
		 24.6 -3.8980758190155025 25.4 -3.9391415119171143 26.2 -3.9601955413818373 27 -3.9434990882873535
		 27.8 -3.8621964454650879 28.6 -3.8245103359222412 29.4 -3.8169655799865727 30.2 -3.874296903610229
		 31 -3.8462884426116948 31.8 -3.7133166790008554 32.6 -3.5841715335845947 33.4 -3.5439233779907227
		 34.2 -3.5248138904571533 35 -3.5261151790618896 35.8 -3.5857658386230469 36.6 -3.6199407577514657
		 37.4 -3.6594862937927251 38.2 -3.6713442802429199 39 -3.6544926166534415 39.8 -3.6039581298828121
		 40.6 -3.657278299331665 41.4 -3.7373278141021729 42.2 -3.7629201412200914 43 -3.8068702220916757
		 43.8 -3.8184728622436519 44.6 -3.8955931663513184 45.4 -3.9106309413909899 46.2 -3.8272364139556889
		 47 -3.7313649654388437 47.8 -3.760417222976685 48.6 -3.9345636367797856 49.4 -4.0774874687194824
		 50.2 -4.1980786323547363 51 -4.2538089752197266 51.8 -4.1635599136352539 52.6 -4.0567564964294434
		 53.4 -3.9790246486663823 54.2 -4.0173788070678711 55 -4.0254158973693848 55.8 -3.9861147403717054
		 56.6 -4.0003676414489746 57.4 -3.9987802505493173 58.2 -3.9386334419250488 59 -3.8990974426269531
		 59.8 -3.8414874076843271 60.6 -3.802898883819581 61.4 -3.7154405117034908 62.2 -3.656832218170166
		 63 -3.6107127666473398 63.8 -3.5784714221954346 64.6 -3.5856389999389644 65.4 -3.6181073188781738
		 66.2 -3.5611531734466557 67 -3.4318447113037114 67.8 -3.3294370174407959 68.6 -3.3586652278900142
		 69.4 -3.3414363861083984 70.2 -3.200750589370728 71 -3.1173326969146729 71.8 -3.0797479152679443
		 72.6 -3.025914192199707 73.4 -2.8804843425750732 74.2 -2.825183629989624 75 -2.7619173526763916
		 75.8 -2.7160217761993408 76.6 -2.5752377510070801 77.4 -2.4554901123046879 78.2 -2.3781559467315674
		 79 -2.3451027870178223 79.8 -2.2749574184417725 80.6 -2.218048095703125 81.4 -2.2072219848632813
		 82.2 -2.165241956710815 83 -2.1943297386169434 83.8 -2.2491912841796875 84.6 -2.171794176101685
		 85.4 -2.0673985481262207 86.2 -2.0461671352386475;
createNode animCurveTA -n "Bip01_R_Foot_rotateZ14";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 108 ".ktv[0:107]"  0.6 -11.615686416625977 1.4 -11.507885932922363
		 2.2 -11.411525726318359 3 -11.342251777648926 3.8 -11.33238410949707 4.6 -11.396939277648926
		 5.4 -11.508280754089355 6.2 -11.493185997009276 7 -11.270003318786619 7.8 -11.093301773071287
		 8.6 -11.189475059509276 9.4 -11.347331047058104 10.2 -11.39330005645752 11 -11.458413124084473
		 11.8 -11.583188056945803 12.6 -11.607903480529783 13.4 -11.508937835693359 14.2 -11.432788848876951
		 15 -11.426540374755859 15.8 -11.507973670959473 16.6 -11.651994705200195 17.4 -11.704246520996094
		 18.2 -11.551197052001951 19 -11.430926322937012 19.8 -11.515674591064451 20.6 -11.621268272399902
		 21.4 -11.61476993560791 22.2 -11.603346824645996 23 -11.585223197937012 23.8 -11.511422157287598
		 24.6 -11.466957092285156 25.4 -11.487995147705078 26.2 -11.514989852905272 27 -11.561190605163574
		 27.8 -11.560454368591309 28.6 -11.532214164733888 29.4 -11.469246864318848 30.2 -11.23790454864502
		 31 -11.077808380126951 31.8 -11.263761520385742 32.6 -11.492339134216309 33.4 -11.546086311340332
		 34.2 -11.602762222290041 35 -11.712360382080078 35.8 -11.790225982666016 36.6 -11.773521423339844
		 37.4 -11.764797210693359 38.2 -11.829606056213381 39 -11.913643836975098 39.8 -11.899264335632324
		 40.6 -11.795341491699221 41.4 -11.846519470214844 42.2 -12.008213043212892 43 -12.033742904663086
		 43.8 -12.022244453430176 44.6 -12.140250205993652 45.4 -12.336006164550779 46.2 -12.528334617614746
		 47 -12.577365875244141 47.8 -12.478816032409668 48.6 -12.392510414123535 49.4 -12.377904891967772
		 50.2 -12.452775001525881 51 -12.670836448669434 51.8 -12.832880020141602 52.6 -12.786786079406738
		 53.4 -12.6986083984375 54.2 -12.64830207824707 55 -12.54334545135498 55.8 -12.445316314697267
		 56.6 -12.443662643432615 57.4 -12.502939224243164 58.2 -12.556392669677734 59 -12.574918746948242
		 59.8 -12.56704044342041 60.6 -12.538939476013184 61.4 -12.492703437805176 62.2 -12.459936141967772
		 63 -12.447492599487305 63.8 -12.399150848388672 64.6 -12.266592025756836 65.4 -12.077692985534668
		 66.2 -11.910013198852541 67 -11.842708587646484 67.8 -11.871691703796388 68.6 -11.922160148620604
		 69.4 -11.967799186706545 70.2 -12.046407699584959 71 -12.055859565734863 71.8 -11.814703941345217
		 72.6 -11.427506446838381 73.4 -11.223480224609377 74.2 -11.299371719360352 75 -11.447481155395508
		 75.8 -11.424515724182127 76.6 -11.228137016296388 77.4 -11.082656860351564 78.2 -10.988358497619627
		 79 -10.787921905517578 79.8 -10.557451248168944 80.6 -10.525911331176758 81.4 -10.62367057800293
		 82.2 -10.618880271911619 83 -10.558765411376951 83.8 -10.506375312805176 84.6 -10.38458251953125
		 85.4 -10.262084007263184 86.2 -10.266341209411619;
createNode animCurveTA -n "Bip01_R_Toe0_rotateX4";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 108 ".ktv[0:107]"  0.6 4.22165749114356e-006 1.4 6.9417285430972697e-007
		 2.2 2.4596188268333212e-006 3 4.7661825419709198e-007 3.8 2.802044264171855e-006
		 4.6 3.0188807613740209e-006 5.4 1.3819001196679892e-006 6.2 2.7074843274021987e-006
		 7 6.2036019699007738e-006 7.8 8.4970388343208469e-006 8.6 -1.7100027207561654e-006
		 9.4 4.5346169486037985e-007 10.2 -8.8951691168404078e-007 11 7.7178310675662942e-006
		 11.8 4.9063078222388867e-006 12.6 7.5935457743980795e-007 13.4 6.2774807929599774e-007
		 14.2 3.0460407742793905e-006 15 -1.7708637756186365e-007 15.8 2.1299488253134768e-006
		 16.6 8.7335126863763435e-007 17.4 3.6952840218873466e-006 18.2 8.6997538062405511e-008
		 19 6.7014025262324139e-006 19.8 4.2437300180608872e-006 20.6 1.9411204448260833e-006
		 21.4 2.5533224743412575e-006 22.2 4.5940660129417665e-006 23 5.2603522817662451e-006
		 23.8 4.142372290516505e-006 24.6 3.6774899854208347e-006 25.4 1.6855316289365874e-006
		 26.2 3.8733933251933186e-006 27 -4.9092051312982221e-007 27.8 -5.720792159991106e-007
		 28.6 8.7773167933846697e-007 29.4 1.800966174414498e-006 30.2 4.9998266149486881e-006
		 31 3.7947181681374791e-006 31.8 2.9935642942291452e-006 32.6 4.0106365304382052e-006
		 33.4 6.2464205257128933e-006 34.2 2.6583950329950312e-007 35 5.9316130318620708e-007
		 35.8 1.3464583616951131e-006 36.6 8.3873635503550759e-007 37.4 3.4381121167825772e-006
		 38.2 2.7728217446565399e-006 39 3.1662368655815953e-006 39.8 6.4348142814196763e-007
		 40.6 2.9073819405311951e-006 41.4 8.6879987293286852e-007 42.2 4.8949545998766553e-006
		 43 4.2009330059045169e-007 43.8 1.0846498526007051e-006 44.6 5.0149128583143465e-006
		 45.4 3.471615627859137e-006 46.2 2.7668329494190398e-006 47 1.2759838909914831e-006
		 47.8 2.153939931304194e-006 48.6 3.6327660382085014e-006 49.4 1.5721794852652238e-006
		 50.2 2.0956244952685665e-006 51 1.7941694068213108e-006 51.8 1.576186434704141e-007
		 52.6 -4.357626039563911e-006 53.4 3.4235188195452793e-006 54.2 2.9002046630921541e-006
		 55 6.9897787398076608e-006 55.8 -5.5640963836367519e-008 56.6 3.424126589379739e-006
		 57.4 4.3684754018613603e-006 58.2 2.3995196443138411e-006 59 -1.6080992963907192e-006
		 59.8 -1.2344629567451193e-006 60.6 4.1343164411955513e-006 61.4 4.7069238462427165e-006
		 62.2 5.4404094953497406e-006 63 6.9204379542497918e-006 63.8 3.260663788751117e-006
		 64.6 1.0129024303751066e-006 65.4 3.8045452583901351e-006 66.2 1.11649353584653e-006
		 67 3.057463800359983e-006 67.8 1.836743649619166e-006 68.6 4.4450948735175197e-006
		 69.4 4.5440715439326596e-006 70.2 1.2073597872586106e-006 71 9.7910913154919399e-007
		 71.8 5.6187504924309906e-006 72.6 4.063207143190084e-006 73.4 7.7608780202353955e-007
		 74.2 2.2988307080140657e-007 75 -3.0931010996937403e-007 75.8 1.7653634358794081e-006
		 76.6 5.005726052331739e-006 77.4 1.4139343420538353e-006 78.2 2.6441396130394423e-006
		 79 -1.2737594943246222e-006 79.8 4.9468653742223987e-006 80.6 3.4194611089333193e-006
		 81.4 -7.215031132545846e-007 82.2 3.410043973417487e-006 83 3.6551937228068705e-006
		 83.8 -2.0034376575495116e-006 84.6 3.2377727166021941e-006 85.4 1.2123932435770254e-007
		 86.2 1.55401130541577e-006;
createNode animCurveTA -n "Bip01_R_Toe0_rotateY4";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 108 ".ktv[0:107]"  0.6 -1.5418105249409564e-005 1.4 -1.2329454875725789e-005
		 2.2 -6.376987585099414e-006 3 -2.6221850930596702e-006 3.8 -1.8039576161754671e-006
		 4.6 -3.8121029319881927e-006 5.4 -1.6488955225213434e-005 6.2 -4.6094100980553776e-006
		 7 -5.8918571994581725e-006 7.8 -4.6494019443343859e-006 8.6 -7.2338671088800766e-006
		 9.4 -6.3475222304987255e-006 10.2 1.7694556930791805e-008 11 -1.0543216376390774e-005
		 11.8 -3.7183233416726576e-006 12.6 -1.6365480405511338e-005 13.4 -4.3757504499808419e-006
		 14.2 -2.4066657715593465e-006 15 -5.1895663091272581e-006 15.8 -6.3521965785184884e-006
		 16.6 -1.4179340723785572e-006 17.4 -5.4456058933283202e-006 18.2 -7.1958061198529322e-006
		 19 -8.1747166404966265e-006 19.8 -5.0846310841734521e-006 20.6 -8.9174154709326103e-006
		 21.4 2.7828518796013668e-006 22.2 -1.3970254258310886e-005 23 -5.3782669056090526e-006
		 23.8 -8.1650732681737281e-006 24.6 -3.8888510971446522e-006 25.4 3.8449916246463545e-006
		 26.2 -2.4397561446676264e-006 27 -8.6324371295631863e-006 27.8 -7.0809551289130468e-006
		 28.6 -1.3352526366361417e-005 29.4 -5.4231663852988277e-006 30.2 -4.3350109990569763e-006
		 31 -6.1454047681763768e-006 31.8 -1.219122077600332e-005 32.6 -5.5633931879128795e-006
		 33.4 5.1783393928417354e-007 34.2 -3.7771740153402797e-006 35 -2.6347870516474359e-006
		 35.8 -8.5585170381818898e-006 36.6 2.1522942006413359e-006 37.4 -1.2290710401430262e-005
		 38.2 -1.2767790394718762e-005 39 -4.6512827793776523e-006 39.8 -1.7594078599358909e-005
		 40.6 -4.5485080590879079e-006 41.4 -6.7232281253382098e-006 42.2 -1.0981728337355888e-005
		 43 -1.597190203028731e-005 43.8 -1.792268790268281e-006 44.6 -4.4725611587637104e-006
		 45.4 -7.802404070389457e-006 46.2 -7.453700618498261e-006 47 -7.0399732976511586e-006
		 47.8 -3.258142442064127e-006 48.6 7.8562907219748013e-006 49.4 -1.6117614904942457e-006
		 50.2 -5.6360727285209578e-007 51 -1.137962408392923e-005 51.8 -9.7514375738683157e-006
		 52.6 -2.0417528503458016e-006 53.4 -1.2211129387651454e-006 54.2 -1.3631506590172648e-005
		 55 -5.1234865168225951e-006 55.8 -9.5410878202528693e-006 56.6 -8.5299734564614482e-006
		 57.4 -1.4172187547956126e-005 58.2 1.0317335181753151e-006 59 -4.7902249207254499e-006
		 59.8 2.0500300479397993e-007 60.6 -6.5084223024314269e-006 61.4 -4.9642681005934719e-006
		 62.2 -8.8077567852451466e-006 63 -1.0130727787327489e-005 63.8 -7.340654519794044e-006
		 64.6 -8.7318912846967578e-006 65.4 2.6818695459951418e-006 66.2 -4.5490678530768491e-006
		 67 -6.6268517002754379e-006 67.8 -1.6882166164577942e-005 68.6 1.0989052725562942e-006
		 69.4 -1.391027672070777e-005 70.2 -1.2339318345766516e-005 71 -8.4285793491289951e-006
		 71.8 -5.5126647566794418e-006 72.6 -6.3513193708786284e-006 73.4 -2.3215861801872961e-006
		 74.2 -3.9656120520703553e-007 75 -1.131378013496942e-008 75.8 -1.0209989341092298e-005
		 76.6 8.0041559158416931e-007 77.4 -2.2470230760518461e-006 78.2 -1.2933786592839167e-006
		 79 -1.6562309610890225e-005 79.8 -1.3758297427557409e-005 80.6 -5.7306428971060086e-006
		 81.4 -1.2635965504159684e-005 82.2 -4.1400649024581071e-006 83 5.2238272019167198e-007
		 83.8 -4.0470094972988591e-006 84.6 4.9463114493164539e-008 85.4 -4.4952698772249278e-006
		 86.2 -1.2608540600922424e-005;
createNode animCurveTA -n "Bip01_R_Toe0_rotateZ4";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 108 ".ktv[0:107]"  0.6 89.999977111816406 1.4 89.999977111816406
		 2.2 89.999977111816406 3 89.999977111816406 3.8 89.999984741210937 4.6 89.999977111816406
		 5.4 89.999977111816406 6.2 89.999977111816406 7 89.999977111816406 7.8 89.999977111816406
		 8.6 89.999977111816406 9.4 89.999977111816406 10.2 89.999977111816406 11 89.999977111816406
		 11.8 89.999977111816406 12.6 89.999977111816406 13.4 89.999977111816406 14.2 89.999977111816406
		 15 89.999977111816406 15.8 89.999961853027344 16.6 89.999977111816406 17.4 89.999977111816406
		 18.2 89.999977111816406 19 89.999977111816406 19.8 89.999984741210937 20.6 89.999977111816406
		 21.4 89.999977111816406 22.2 89.999977111816406 23 89.999977111816406 23.8 89.999984741210937
		 24.6 89.999977111816406 25.4 89.999977111816406 26.2 89.999977111816406 27 89.999984741210937
		 27.8 89.999977111816406 28.6 89.999977111816406 29.4 89.999984741210937 30.2 89.999977111816406
		 31 89.999984741210937 31.8 89.999977111816406 32.6 89.999984741210937 33.4 89.999984741210937
		 34.2 89.999984741210937 35 89.999977111816406 35.8 89.999977111816406 36.6 89.999977111816406
		 37.4 89.999977111816406 38.2 89.999984741210937 39 89.999977111816406 39.8 89.999977111816406
		 40.6 89.999977111816406 41.4 89.999977111816406 42.2 89.999984741210937 43 89.999977111816406
		 43.8 89.999977111816406 44.6 89.999977111816406 45.4 89.999977111816406 46.2 89.999977111816406
		 47 89.999977111816406 47.8 89.999977111816406 48.6 89.999977111816406 49.4 89.999977111816406
		 50.2 89.999961853027344 51 89.999984741210937 51.8 89.999984741210937 52.6 89.999984741210937
		 53.4 89.999984741210937 54.2 89.999977111816406 55 89.999961853027344 55.8 89.999977111816406
		 56.6 89.999977111816406 57.4 89.999977111816406 58.2 89.999977111816406 59 89.999977111816406
		 59.8 89.999977111816406 60.6 89.999984741210937 61.4 89.999977111816406 62.2 89.999977111816406
		 63 89.999977111816406 63.8 89.999984741210937 64.6 89.999977111816406 65.4 89.999977111816406
		 66.2 89.999977111816406 67 89.999977111816406 67.8 89.999984741210937 68.6 89.999977111816406
		 69.4 89.999984741210937 70.2 89.999977111816406 71 89.999977111816406 71.8 89.999984741210937
		 72.6 89.999977111816406 73.4 89.999977111816406 74.2 89.999977111816406 75 89.999977111816406
		 75.8 89.999984741210937 76.6 89.999977111816406 77.4 89.999984741210937 78.2 89.999977111816406
		 79 89.999984741210937 79.8 89.999977111816406 80.6 89.999977111816406 81.4 89.999984741210937
		 82.2 89.999984741210937 83 89.999977111816406 83.8 89.999984741210937 84.6 89.999984741210937
		 85.4 89.999977111816406 86.2 89.999984741210937;
select -ne :time1;
	setAttr ".o" 24.2;
	setAttr ".unw" 24.2;
select -ne :renderPartition;
	setAttr -s 126 ".st";
select -ne :initialShadingGroup;
	setAttr ".ro" yes;
select -ne :initialParticleSE;
	setAttr ".ro" yes;
select -ne :defaultShaderList1;
	setAttr -s 64 ".s";
select -ne :defaultTextureList1;
	setAttr -s 31 ".tx";
select -ne :postProcessList1;
	setAttr -s 2 ".p";
select -ne :defaultRenderUtilityList1;
	setAttr -s 31 ".u";
select -ne :defaultRenderingList1;
select -ne :renderGlobalsList1;
select -ne :defaultResolution;
	setAttr ".pa" 1;
select -ne :hardwareRenderGlobals;
	setAttr ".ctrs" 256;
	setAttr ".btrs" 512;
select -ne :hardwareRenderingGlobals;
	setAttr ".otfna" -type "stringArray" 18 "NURBS Curves" "NURBS Surfaces" "Polygons" "Subdiv Surfaces" "Particles" "Fluids" "Image Planes" "UI:" "Lights" "Cameras" "Locators" "Joints" "IK Handles" "Deformers" "Motion Trails" "Components" "Misc. UI" "Ornaments"  ;
	setAttr ".otfva" -type "Int32Array" 18 0 1 1 1 1 1
		 1 0 0 0 0 0 0 0 0 0 0 0 ;
select -ne :defaultHardwareRenderGlobals;
	setAttr ".fn" -type "string" "im";
	setAttr ".res" -type "string" "ntsc_4d 646 485 1.333";
select -ne :hyperGraphLayout;
	setAttr -s 4 ".hyp";
	setAttr ".hyp[1].isc" yes;
	setAttr ".hyp[2].isc" yes;
	setAttr ".hyp[3].isc" yes;
	setAttr ".hyp[4].isc" yes;
connectAttr "Bip01_Spine_translateX14.o" "|Obese_Sit_Talk_3|Bip01_Spine.tx";
connectAttr "Bip01_Spine_translateY14.o" "|Obese_Sit_Talk_3|Bip01_Spine.ty";
connectAttr "Bip01_Spine_translateZ14.o" "|Obese_Sit_Talk_3|Bip01_Spine.tz";
connectAttr "Bip01_Spine_rotateX14.o" "|Obese_Sit_Talk_3|Bip01_Spine.rx";
connectAttr "Bip01_Spine_rotateY14.o" "|Obese_Sit_Talk_3|Bip01_Spine.ry";
connectAttr "Bip01_Spine_rotateZ14.o" "|Obese_Sit_Talk_3|Bip01_Spine.rz";
connectAttr "|Obese_Sit_Talk_3|Bip01_Spine.s" "|Obese_Sit_Talk_3|Bip01_Spine|Bip01_Spine1.is"
		;
connectAttr "Bip01_Spine1_rotateX14.o" "|Obese_Sit_Talk_3|Bip01_Spine|Bip01_Spine1.rx"
		;
connectAttr "Bip01_Spine1_rotateY14.o" "|Obese_Sit_Talk_3|Bip01_Spine|Bip01_Spine1.ry"
		;
connectAttr "Bip01_Spine1_rotateZ14.o" "|Obese_Sit_Talk_3|Bip01_Spine|Bip01_Spine1.rz"
		;
connectAttr "|Obese_Sit_Talk_3|Bip01_Spine|Bip01_Spine1.s" "|Obese_Sit_Talk_3|Bip01_Spine|Bip01_Spine1|Bip01_Spine2.is"
		;
connectAttr "Bip01_Spine2_rotateX14.o" "|Obese_Sit_Talk_3|Bip01_Spine|Bip01_Spine1|Bip01_Spine2.rx"
		;
connectAttr "Bip01_Spine2_rotateY14.o" "|Obese_Sit_Talk_3|Bip01_Spine|Bip01_Spine1|Bip01_Spine2.ry"
		;
connectAttr "Bip01_Spine2_rotateZ14.o" "|Obese_Sit_Talk_3|Bip01_Spine|Bip01_Spine1|Bip01_Spine2.rz"
		;
connectAttr "|Obese_Sit_Talk_3|Bip01_Spine|Bip01_Spine1|Bip01_Spine2.s" "|Obese_Sit_Talk_3|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck.is"
		;
connectAttr "|Obese_Sit_Talk_3|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck.s" "|Obese_Sit_Talk_3|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_Head.is"
		;
connectAttr "|Obese_Sit_Talk_3|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_Head.s" "|Obese_Sit_Talk_3|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_Head|_notused_Bip01_HeadNub.is"
		;
connectAttr "|Obese_Sit_Talk_3|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck.s" "|Obese_Sit_Talk_3|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_R_Clavicle.is"
		;
connectAttr "Bip01_R_Clavicle_rotateX14.o" "|Obese_Sit_Talk_3|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_R_Clavicle.rx"
		;
connectAttr "Bip01_R_Clavicle_rotateY14.o" "|Obese_Sit_Talk_3|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_R_Clavicle.ry"
		;
connectAttr "Bip01_R_Clavicle_rotateZ14.o" "|Obese_Sit_Talk_3|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_R_Clavicle.rz"
		;
connectAttr "|Obese_Sit_Talk_3|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_R_Clavicle.s" "|Obese_Sit_Talk_3|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_R_Clavicle|Bip01_R_UpperArm.is"
		;
connectAttr "Bip01_R_UpperArm_rotateX14.o" "|Obese_Sit_Talk_3|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_R_Clavicle|Bip01_R_UpperArm.rx"
		;
connectAttr "Bip01_R_UpperArm_rotateY14.o" "|Obese_Sit_Talk_3|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_R_Clavicle|Bip01_R_UpperArm.ry"
		;
connectAttr "Bip01_R_UpperArm_rotateZ14.o" "|Obese_Sit_Talk_3|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_R_Clavicle|Bip01_R_UpperArm.rz"
		;
connectAttr "|Obese_Sit_Talk_3|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_R_Clavicle|Bip01_R_UpperArm.s" "|Obese_Sit_Talk_3|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_R_Clavicle|Bip01_R_UpperArm|Bip01_R_Forearm.is"
		;
connectAttr "Bip01_R_Forearm_rotateX14.o" "|Obese_Sit_Talk_3|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_R_Clavicle|Bip01_R_UpperArm|Bip01_R_Forearm.rx"
		;
connectAttr "Bip01_R_Forearm_rotateY14.o" "|Obese_Sit_Talk_3|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_R_Clavicle|Bip01_R_UpperArm|Bip01_R_Forearm.ry"
		;
connectAttr "Bip01_R_Forearm_rotateZ14.o" "|Obese_Sit_Talk_3|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_R_Clavicle|Bip01_R_UpperArm|Bip01_R_Forearm.rz"
		;
connectAttr "|Obese_Sit_Talk_3|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_R_Clavicle|Bip01_R_UpperArm|Bip01_R_Forearm.s" "|Obese_Sit_Talk_3|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_R_Clavicle|Bip01_R_UpperArm|Bip01_R_Forearm|Bip01_R_Hand.is"
		;
connectAttr "Bip01_R_Hand_rotateX4.o" "|Obese_Sit_Talk_3|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_R_Clavicle|Bip01_R_UpperArm|Bip01_R_Forearm|Bip01_R_Hand.rx"
		;
connectAttr "Bip01_R_Hand_rotateY4.o" "|Obese_Sit_Talk_3|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_R_Clavicle|Bip01_R_UpperArm|Bip01_R_Forearm|Bip01_R_Hand.ry"
		;
connectAttr "Bip01_R_Hand_rotateZ4.o" "|Obese_Sit_Talk_3|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_R_Clavicle|Bip01_R_UpperArm|Bip01_R_Forearm|Bip01_R_Hand.rz"
		;
connectAttr "|Obese_Sit_Talk_3|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_R_Clavicle|Bip01_R_UpperArm|Bip01_R_Forearm|Bip01_R_Hand.s" "|Obese_Sit_Talk_3|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_R_Clavicle|Bip01_R_UpperArm|Bip01_R_Forearm|Bip01_R_Hand|Bip01_R_Finger0.is"
		;
connectAttr "|Obese_Sit_Talk_3|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_R_Clavicle|Bip01_R_UpperArm|Bip01_R_Forearm|Bip01_R_Hand|Bip01_R_Finger0.s" "|Obese_Sit_Talk_3|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_R_Clavicle|Bip01_R_UpperArm|Bip01_R_Forearm|Bip01_R_Hand|Bip01_R_Finger0|Bip01_R_Finger01.is"
		;
connectAttr "|Obese_Sit_Talk_3|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_R_Clavicle|Bip01_R_UpperArm|Bip01_R_Forearm|Bip01_R_Hand|Bip01_R_Finger0|Bip01_R_Finger01.s" "|Obese_Sit_Talk_3|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_R_Clavicle|Bip01_R_UpperArm|Bip01_R_Forearm|Bip01_R_Hand|Bip01_R_Finger0|Bip01_R_Finger01|_notused_Bip01_R_Finger0Nub.is"
		;
connectAttr "|Obese_Sit_Talk_3|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_R_Clavicle|Bip01_R_UpperArm|Bip01_R_Forearm|Bip01_R_Hand.s" "|Obese_Sit_Talk_3|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_R_Clavicle|Bip01_R_UpperArm|Bip01_R_Forearm|Bip01_R_Hand|Bip01_R_Finger2.is"
		;
connectAttr "|Obese_Sit_Talk_3|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_R_Clavicle|Bip01_R_UpperArm|Bip01_R_Forearm|Bip01_R_Hand|Bip01_R_Finger2.s" "|Obese_Sit_Talk_3|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_R_Clavicle|Bip01_R_UpperArm|Bip01_R_Forearm|Bip01_R_Hand|Bip01_R_Finger2|Bip01_R_Finger21.is"
		;
connectAttr "|Obese_Sit_Talk_3|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_R_Clavicle|Bip01_R_UpperArm|Bip01_R_Forearm|Bip01_R_Hand|Bip01_R_Finger2|Bip01_R_Finger21.s" "|Obese_Sit_Talk_3|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_R_Clavicle|Bip01_R_UpperArm|Bip01_R_Forearm|Bip01_R_Hand|Bip01_R_Finger2|Bip01_R_Finger21|_notused_Bip01_R_Finger2Nub.is"
		;
connectAttr "|Obese_Sit_Talk_3|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_R_Clavicle|Bip01_R_UpperArm|Bip01_R_Forearm|Bip01_R_Hand.s" "|Obese_Sit_Talk_3|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_R_Clavicle|Bip01_R_UpperArm|Bip01_R_Forearm|Bip01_R_Hand|Bip01_R_Finger1.is"
		;
connectAttr "|Obese_Sit_Talk_3|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_R_Clavicle|Bip01_R_UpperArm|Bip01_R_Forearm|Bip01_R_Hand|Bip01_R_Finger1.s" "|Obese_Sit_Talk_3|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_R_Clavicle|Bip01_R_UpperArm|Bip01_R_Forearm|Bip01_R_Hand|Bip01_R_Finger1|Bip01_R_Finger11.is"
		;
connectAttr "|Obese_Sit_Talk_3|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_R_Clavicle|Bip01_R_UpperArm|Bip01_R_Forearm|Bip01_R_Hand|Bip01_R_Finger1|Bip01_R_Finger11.s" "|Obese_Sit_Talk_3|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_R_Clavicle|Bip01_R_UpperArm|Bip01_R_Forearm|Bip01_R_Hand|Bip01_R_Finger1|Bip01_R_Finger11|_notused_Bip01_R_Finger1Nub.is"
		;
connectAttr "|Obese_Sit_Talk_3|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_R_Clavicle|Bip01_R_UpperArm|Bip01_R_Forearm|Bip01_R_Hand.s" "|Obese_Sit_Talk_3|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_R_Clavicle|Bip01_R_UpperArm|Bip01_R_Forearm|Bip01_R_Hand|Bip01_R_Finger3.is"
		;
connectAttr "|Obese_Sit_Talk_3|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_R_Clavicle|Bip01_R_UpperArm|Bip01_R_Forearm|Bip01_R_Hand|Bip01_R_Finger3.s" "|Obese_Sit_Talk_3|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_R_Clavicle|Bip01_R_UpperArm|Bip01_R_Forearm|Bip01_R_Hand|Bip01_R_Finger3|Bip01_R_Finger31.is"
		;
connectAttr "|Obese_Sit_Talk_3|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_R_Clavicle|Bip01_R_UpperArm|Bip01_R_Forearm|Bip01_R_Hand|Bip01_R_Finger3|Bip01_R_Finger31.s" "|Obese_Sit_Talk_3|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_R_Clavicle|Bip01_R_UpperArm|Bip01_R_Forearm|Bip01_R_Hand|Bip01_R_Finger3|Bip01_R_Finger31|_notused_Bip01_R_Finger3Nub.is"
		;
connectAttr "|Obese_Sit_Talk_3|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_R_Clavicle|Bip01_R_UpperArm|Bip01_R_Forearm|Bip01_R_Hand.s" "|Obese_Sit_Talk_3|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_R_Clavicle|Bip01_R_UpperArm|Bip01_R_Forearm|Bip01_R_Hand|Bip01_R_Finger4.is"
		;
connectAttr "|Obese_Sit_Talk_3|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_R_Clavicle|Bip01_R_UpperArm|Bip01_R_Forearm|Bip01_R_Hand|Bip01_R_Finger4.s" "|Obese_Sit_Talk_3|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_R_Clavicle|Bip01_R_UpperArm|Bip01_R_Forearm|Bip01_R_Hand|Bip01_R_Finger4|Bip01_R_Finger41.is"
		;
connectAttr "|Obese_Sit_Talk_3|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_R_Clavicle|Bip01_R_UpperArm|Bip01_R_Forearm|Bip01_R_Hand|Bip01_R_Finger4|Bip01_R_Finger41.s" "|Obese_Sit_Talk_3|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_R_Clavicle|Bip01_R_UpperArm|Bip01_R_Forearm|Bip01_R_Hand|Bip01_R_Finger4|Bip01_R_Finger41|_notused_Bip01_R_Finger4Nub.is"
		;
connectAttr "|Obese_Sit_Talk_3|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck.s" "|Obese_Sit_Talk_3|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_L_Clavicle.is"
		;
connectAttr "Bip01_L_Clavicle_rotateX14.o" "|Obese_Sit_Talk_3|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_L_Clavicle.rx"
		;
connectAttr "Bip01_L_Clavicle_rotateY14.o" "|Obese_Sit_Talk_3|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_L_Clavicle.ry"
		;
connectAttr "Bip01_L_Clavicle_rotateZ14.o" "|Obese_Sit_Talk_3|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_L_Clavicle.rz"
		;
connectAttr "|Obese_Sit_Talk_3|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_L_Clavicle.s" "|Obese_Sit_Talk_3|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_L_Clavicle|Bip01_L_UpperArm.is"
		;
connectAttr "Bip01_L_UpperArm_rotateX14.o" "|Obese_Sit_Talk_3|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_L_Clavicle|Bip01_L_UpperArm.rx"
		;
connectAttr "Bip01_L_UpperArm_rotateY14.o" "|Obese_Sit_Talk_3|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_L_Clavicle|Bip01_L_UpperArm.ry"
		;
connectAttr "Bip01_L_UpperArm_rotateZ14.o" "|Obese_Sit_Talk_3|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_L_Clavicle|Bip01_L_UpperArm.rz"
		;
connectAttr "|Obese_Sit_Talk_3|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_L_Clavicle|Bip01_L_UpperArm.s" "|Obese_Sit_Talk_3|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_L_Clavicle|Bip01_L_UpperArm|Bip01_L_Forearm.is"
		;
connectAttr "Bip01_L_Forearm_rotateX14.o" "|Obese_Sit_Talk_3|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_L_Clavicle|Bip01_L_UpperArm|Bip01_L_Forearm.rx"
		;
connectAttr "Bip01_L_Forearm_rotateY14.o" "|Obese_Sit_Talk_3|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_L_Clavicle|Bip01_L_UpperArm|Bip01_L_Forearm.ry"
		;
connectAttr "Bip01_L_Forearm_rotateZ14.o" "|Obese_Sit_Talk_3|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_L_Clavicle|Bip01_L_UpperArm|Bip01_L_Forearm.rz"
		;
connectAttr "|Obese_Sit_Talk_3|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_L_Clavicle|Bip01_L_UpperArm|Bip01_L_Forearm.s" "|Obese_Sit_Talk_3|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_L_Clavicle|Bip01_L_UpperArm|Bip01_L_Forearm|Bip01_L_Hand.is"
		;
connectAttr "Bip01_L_Hand_rotateX4.o" "|Obese_Sit_Talk_3|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_L_Clavicle|Bip01_L_UpperArm|Bip01_L_Forearm|Bip01_L_Hand.rx"
		;
connectAttr "Bip01_L_Hand_rotateY4.o" "|Obese_Sit_Talk_3|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_L_Clavicle|Bip01_L_UpperArm|Bip01_L_Forearm|Bip01_L_Hand.ry"
		;
connectAttr "Bip01_L_Hand_rotateZ4.o" "|Obese_Sit_Talk_3|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_L_Clavicle|Bip01_L_UpperArm|Bip01_L_Forearm|Bip01_L_Hand.rz"
		;
connectAttr "|Obese_Sit_Talk_3|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_L_Clavicle|Bip01_L_UpperArm|Bip01_L_Forearm|Bip01_L_Hand.s" "|Obese_Sit_Talk_3|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_L_Clavicle|Bip01_L_UpperArm|Bip01_L_Forearm|Bip01_L_Hand|Bip01_L_Finger0.is"
		;
connectAttr "|Obese_Sit_Talk_3|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_L_Clavicle|Bip01_L_UpperArm|Bip01_L_Forearm|Bip01_L_Hand|Bip01_L_Finger0.s" "|Obese_Sit_Talk_3|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_L_Clavicle|Bip01_L_UpperArm|Bip01_L_Forearm|Bip01_L_Hand|Bip01_L_Finger0|Bip01_L_Finger01.is"
		;
connectAttr "|Obese_Sit_Talk_3|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_L_Clavicle|Bip01_L_UpperArm|Bip01_L_Forearm|Bip01_L_Hand|Bip01_L_Finger0|Bip01_L_Finger01.s" "|Obese_Sit_Talk_3|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_L_Clavicle|Bip01_L_UpperArm|Bip01_L_Forearm|Bip01_L_Hand|Bip01_L_Finger0|Bip01_L_Finger01|_notused_Bip01_L_Finger0Nub.is"
		;
connectAttr "|Obese_Sit_Talk_3|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_L_Clavicle|Bip01_L_UpperArm|Bip01_L_Forearm|Bip01_L_Hand.s" "|Obese_Sit_Talk_3|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_L_Clavicle|Bip01_L_UpperArm|Bip01_L_Forearm|Bip01_L_Hand|Bip01_L_Finger2.is"
		;
connectAttr "|Obese_Sit_Talk_3|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_L_Clavicle|Bip01_L_UpperArm|Bip01_L_Forearm|Bip01_L_Hand|Bip01_L_Finger2.s" "|Obese_Sit_Talk_3|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_L_Clavicle|Bip01_L_UpperArm|Bip01_L_Forearm|Bip01_L_Hand|Bip01_L_Finger2|Bip01_L_Finger21.is"
		;
connectAttr "|Obese_Sit_Talk_3|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_L_Clavicle|Bip01_L_UpperArm|Bip01_L_Forearm|Bip01_L_Hand|Bip01_L_Finger2|Bip01_L_Finger21.s" "|Obese_Sit_Talk_3|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_L_Clavicle|Bip01_L_UpperArm|Bip01_L_Forearm|Bip01_L_Hand|Bip01_L_Finger2|Bip01_L_Finger21|_notused_Bip01_L_Finger2Nub.is"
		;
connectAttr "|Obese_Sit_Talk_3|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_L_Clavicle|Bip01_L_UpperArm|Bip01_L_Forearm|Bip01_L_Hand.s" "|Obese_Sit_Talk_3|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_L_Clavicle|Bip01_L_UpperArm|Bip01_L_Forearm|Bip01_L_Hand|Bip01_L_Finger1.is"
		;
connectAttr "|Obese_Sit_Talk_3|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_L_Clavicle|Bip01_L_UpperArm|Bip01_L_Forearm|Bip01_L_Hand|Bip01_L_Finger1.s" "|Obese_Sit_Talk_3|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_L_Clavicle|Bip01_L_UpperArm|Bip01_L_Forearm|Bip01_L_Hand|Bip01_L_Finger1|Bip01_L_Finger11.is"
		;
connectAttr "|Obese_Sit_Talk_3|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_L_Clavicle|Bip01_L_UpperArm|Bip01_L_Forearm|Bip01_L_Hand|Bip01_L_Finger1|Bip01_L_Finger11.s" "|Obese_Sit_Talk_3|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_L_Clavicle|Bip01_L_UpperArm|Bip01_L_Forearm|Bip01_L_Hand|Bip01_L_Finger1|Bip01_L_Finger11|_notused_Bip01_L_Finger1Nub.is"
		;
connectAttr "|Obese_Sit_Talk_3|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_L_Clavicle|Bip01_L_UpperArm|Bip01_L_Forearm|Bip01_L_Hand.s" "|Obese_Sit_Talk_3|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_L_Clavicle|Bip01_L_UpperArm|Bip01_L_Forearm|Bip01_L_Hand|Bip01_L_Finger3.is"
		;
connectAttr "|Obese_Sit_Talk_3|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_L_Clavicle|Bip01_L_UpperArm|Bip01_L_Forearm|Bip01_L_Hand|Bip01_L_Finger3.s" "|Obese_Sit_Talk_3|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_L_Clavicle|Bip01_L_UpperArm|Bip01_L_Forearm|Bip01_L_Hand|Bip01_L_Finger3|Bip01_L_Finger31.is"
		;
connectAttr "|Obese_Sit_Talk_3|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_L_Clavicle|Bip01_L_UpperArm|Bip01_L_Forearm|Bip01_L_Hand|Bip01_L_Finger3|Bip01_L_Finger31.s" "|Obese_Sit_Talk_3|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_L_Clavicle|Bip01_L_UpperArm|Bip01_L_Forearm|Bip01_L_Hand|Bip01_L_Finger3|Bip01_L_Finger31|_notused_Bip01_L_Finger3Nub.is"
		;
connectAttr "|Obese_Sit_Talk_3|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_L_Clavicle|Bip01_L_UpperArm|Bip01_L_Forearm|Bip01_L_Hand.s" "|Obese_Sit_Talk_3|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_L_Clavicle|Bip01_L_UpperArm|Bip01_L_Forearm|Bip01_L_Hand|Bip01_L_Finger4.is"
		;
connectAttr "|Obese_Sit_Talk_3|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_L_Clavicle|Bip01_L_UpperArm|Bip01_L_Forearm|Bip01_L_Hand|Bip01_L_Finger4.s" "|Obese_Sit_Talk_3|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_L_Clavicle|Bip01_L_UpperArm|Bip01_L_Forearm|Bip01_L_Hand|Bip01_L_Finger4|Bip01_L_Finger41.is"
		;
connectAttr "|Obese_Sit_Talk_3|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_L_Clavicle|Bip01_L_UpperArm|Bip01_L_Forearm|Bip01_L_Hand|Bip01_L_Finger4|Bip01_L_Finger41.s" "|Obese_Sit_Talk_3|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_L_Clavicle|Bip01_L_UpperArm|Bip01_L_Forearm|Bip01_L_Hand|Bip01_L_Finger4|Bip01_L_Finger41|_notused_Bip01_L_Finger4Nub.is"
		;
connectAttr "|Obese_Sit_Talk_3|Bip01_Spine.s" "|Obese_Sit_Talk_3|Bip01_Spine|Bip01_L_Thigh.is"
		;
connectAttr "Bip01_L_Thigh_rotateX14.o" "|Obese_Sit_Talk_3|Bip01_Spine|Bip01_L_Thigh.rx"
		;
connectAttr "Bip01_L_Thigh_rotateY14.o" "|Obese_Sit_Talk_3|Bip01_Spine|Bip01_L_Thigh.ry"
		;
connectAttr "Bip01_L_Thigh_rotateZ14.o" "|Obese_Sit_Talk_3|Bip01_Spine|Bip01_L_Thigh.rz"
		;
connectAttr "|Obese_Sit_Talk_3|Bip01_Spine|Bip01_L_Thigh.s" "|Obese_Sit_Talk_3|Bip01_Spine|Bip01_L_Thigh|Bip01_L_Calf.is"
		;
connectAttr "Bip01_L_Calf_rotateX14.o" "|Obese_Sit_Talk_3|Bip01_Spine|Bip01_L_Thigh|Bip01_L_Calf.rx"
		;
connectAttr "Bip01_L_Calf_rotateY14.o" "|Obese_Sit_Talk_3|Bip01_Spine|Bip01_L_Thigh|Bip01_L_Calf.ry"
		;
connectAttr "Bip01_L_Calf_rotateZ14.o" "|Obese_Sit_Talk_3|Bip01_Spine|Bip01_L_Thigh|Bip01_L_Calf.rz"
		;
connectAttr "|Obese_Sit_Talk_3|Bip01_Spine|Bip01_L_Thigh|Bip01_L_Calf.s" "|Obese_Sit_Talk_3|Bip01_Spine|Bip01_L_Thigh|Bip01_L_Calf|Bip01_L_Foot.is"
		;
connectAttr "Bip01_L_Foot_rotateX14.o" "|Obese_Sit_Talk_3|Bip01_Spine|Bip01_L_Thigh|Bip01_L_Calf|Bip01_L_Foot.rx"
		;
connectAttr "Bip01_L_Foot_rotateY14.o" "|Obese_Sit_Talk_3|Bip01_Spine|Bip01_L_Thigh|Bip01_L_Calf|Bip01_L_Foot.ry"
		;
connectAttr "Bip01_L_Foot_rotateZ14.o" "|Obese_Sit_Talk_3|Bip01_Spine|Bip01_L_Thigh|Bip01_L_Calf|Bip01_L_Foot.rz"
		;
connectAttr "|Obese_Sit_Talk_3|Bip01_Spine|Bip01_L_Thigh|Bip01_L_Calf|Bip01_L_Foot.s" "|Obese_Sit_Talk_3|Bip01_Spine|Bip01_L_Thigh|Bip01_L_Calf|Bip01_L_Foot|Bip01_L_Toe0.is"
		;
connectAttr "Bip01_L_Toe0_rotateX4.o" "|Obese_Sit_Talk_3|Bip01_Spine|Bip01_L_Thigh|Bip01_L_Calf|Bip01_L_Foot|Bip01_L_Toe0.rx"
		;
connectAttr "Bip01_L_Toe0_rotateY4.o" "|Obese_Sit_Talk_3|Bip01_Spine|Bip01_L_Thigh|Bip01_L_Calf|Bip01_L_Foot|Bip01_L_Toe0.ry"
		;
connectAttr "Bip01_L_Toe0_rotateZ4.o" "|Obese_Sit_Talk_3|Bip01_Spine|Bip01_L_Thigh|Bip01_L_Calf|Bip01_L_Foot|Bip01_L_Toe0.rz"
		;
connectAttr "|Obese_Sit_Talk_3|Bip01_Spine|Bip01_L_Thigh|Bip01_L_Calf|Bip01_L_Foot|Bip01_L_Toe0.s" "|Obese_Sit_Talk_3|Bip01_Spine|Bip01_L_Thigh|Bip01_L_Calf|Bip01_L_Foot|Bip01_L_Toe0|_notused_Bip01_L_Toe0Nub.is"
		;
connectAttr "|Obese_Sit_Talk_3|Bip01_Spine.s" "|Obese_Sit_Talk_3|Bip01_Spine|Bip01_R_Thigh.is"
		;
connectAttr "Bip01_R_Thigh_rotateX14.o" "|Obese_Sit_Talk_3|Bip01_Spine|Bip01_R_Thigh.rx"
		;
connectAttr "Bip01_R_Thigh_rotateY14.o" "|Obese_Sit_Talk_3|Bip01_Spine|Bip01_R_Thigh.ry"
		;
connectAttr "Bip01_R_Thigh_rotateZ14.o" "|Obese_Sit_Talk_3|Bip01_Spine|Bip01_R_Thigh.rz"
		;
connectAttr "|Obese_Sit_Talk_3|Bip01_Spine|Bip01_R_Thigh.s" "|Obese_Sit_Talk_3|Bip01_Spine|Bip01_R_Thigh|Bip01_R_Calf.is"
		;
connectAttr "Bip01_R_Calf_rotateX14.o" "|Obese_Sit_Talk_3|Bip01_Spine|Bip01_R_Thigh|Bip01_R_Calf.rx"
		;
connectAttr "Bip01_R_Calf_rotateY14.o" "|Obese_Sit_Talk_3|Bip01_Spine|Bip01_R_Thigh|Bip01_R_Calf.ry"
		;
connectAttr "Bip01_R_Calf_rotateZ14.o" "|Obese_Sit_Talk_3|Bip01_Spine|Bip01_R_Thigh|Bip01_R_Calf.rz"
		;
connectAttr "|Obese_Sit_Talk_3|Bip01_Spine|Bip01_R_Thigh|Bip01_R_Calf.s" "|Obese_Sit_Talk_3|Bip01_Spine|Bip01_R_Thigh|Bip01_R_Calf|Bip01_R_Foot.is"
		;
connectAttr "Bip01_R_Foot_rotateX14.o" "|Obese_Sit_Talk_3|Bip01_Spine|Bip01_R_Thigh|Bip01_R_Calf|Bip01_R_Foot.rx"
		;
connectAttr "Bip01_R_Foot_rotateY14.o" "|Obese_Sit_Talk_3|Bip01_Spine|Bip01_R_Thigh|Bip01_R_Calf|Bip01_R_Foot.ry"
		;
connectAttr "Bip01_R_Foot_rotateZ14.o" "|Obese_Sit_Talk_3|Bip01_Spine|Bip01_R_Thigh|Bip01_R_Calf|Bip01_R_Foot.rz"
		;
connectAttr "|Obese_Sit_Talk_3|Bip01_Spine|Bip01_R_Thigh|Bip01_R_Calf|Bip01_R_Foot.s" "|Obese_Sit_Talk_3|Bip01_Spine|Bip01_R_Thigh|Bip01_R_Calf|Bip01_R_Foot|Bip01_R_Toe0.is"
		;
connectAttr "Bip01_R_Toe0_rotateX4.o" "|Obese_Sit_Talk_3|Bip01_Spine|Bip01_R_Thigh|Bip01_R_Calf|Bip01_R_Foot|Bip01_R_Toe0.rx"
		;
connectAttr "Bip01_R_Toe0_rotateY4.o" "|Obese_Sit_Talk_3|Bip01_Spine|Bip01_R_Thigh|Bip01_R_Calf|Bip01_R_Foot|Bip01_R_Toe0.ry"
		;
connectAttr "Bip01_R_Toe0_rotateZ4.o" "|Obese_Sit_Talk_3|Bip01_Spine|Bip01_R_Thigh|Bip01_R_Calf|Bip01_R_Foot|Bip01_R_Toe0.rz"
		;
connectAttr "|Obese_Sit_Talk_3|Bip01_Spine|Bip01_R_Thigh|Bip01_R_Calf|Bip01_R_Foot|Bip01_R_Toe0.s" "|Obese_Sit_Talk_3|Bip01_Spine|Bip01_R_Thigh|Bip01_R_Calf|Bip01_R_Foot|Bip01_R_Toe0|_notused_Bip01_R_Toe0Nub.is"
		;
// End of Animation v5 - Obese Sit Talk 3.ma
