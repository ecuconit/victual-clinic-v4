//Maya ASCII 2014 scene
//Name: Animation v5 - Stand Idle 2.ma
//Last modified: Tue, Apr 14, 2015 07:16:29 AM
//Codeset: 1252
requires maya "2014";
currentUnit -l centimeter -a degree -t film;
fileInfo "application" "maya";
fileInfo "product" "Maya 2014";
fileInfo "version" "2014 x64";
fileInfo "cutIdentifier" "201303010241-864206";
fileInfo "osv" "Microsoft Windows 7 Enterprise Edition, 64-bit Windows 7 Service Pack 1 (Build 7601)\n";
fileInfo "license" "education";
createNode transform -n "Stand_Idle_2";
	setAttr ".v" no;
createNode joint -n "Bip01_Spine" -p "Stand_Idle_2";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".s" -type "double3" 0.99999999999999956 0.99999999999999989 0.99999999999999989 ;
	setAttr ".jo" -type "double3" 90.000171000000023 0 89.999999999999986 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" -0.0081722831436215043 0.99872757760529951 0.04976383746125676 0
		 -0.031468606931617168 -0.049997711134416305 0.99825345261566845 0 0.99947133053730142 0.006592011223395697 0.031837161023350762 0
		 -6.7139604917915256e-006 99.443700000000007 -2.2495999999899592 1;
	setAttr -k on ".liw";
	setAttr ".fbxID" 5;
createNode joint -n "Bip01_Spine1" -p "|Stand_Idle_2|Bip01_Spine";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".t" -type "double3" 13.3984 -0.0107 0 ;
	setAttr ".s" -type "double3" 1 1 0.99999999999999989 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" -0.0087295896663812361 0.015254963731834248 0.99984552824213668 0
		 -0.0050586545493994011 -0.99987150655113555 0.015211193290218554 0 0.99994910084384092 -0.0049250856543791108 0.0088056375629807955 0
		 9.9999999999999995e-007 2.2603360000000001 112.84212599999999 1;
	setAttr -k on ".liw";
	setAttr ".fbxID" 5;
createNode joint -n "Bip01_Spine2" -p "|Stand_Idle_2|Bip01_Spine|Bip01_Spine1";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".t" -type "double3" 13.3983 -0.0107 0 ;
	setAttr ".s" -type "double3" 1 0.99999999999999989 0.99999999999999989 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" 0 0 1 0 0 -1 0 0 1 0 0 0 -3.9999999999999998e-006 -9.6239790000000003 -19.074804 1;
	setAttr -k on ".liw";
	setAttr ".fbxID" 5;
createNode joint -n "Bip01_Neck" -p "|Stand_Idle_2|Bip01_Spine|Bip01_Spine1|Bip01_Spine2";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".t" -type "double3" 13.3985 -0.007899999999999999 0 ;
	setAttr ".s" -type "double3" 0.99999999999999956 0.99999999999999956 0.99999999999999989 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" 0 -0.020830999999999999 0.99978299999999998 0 0 -0.99978299999999998 -0.020830999999999999 0
		 1 0 0 0 -1.0000000000000001e-005 -9.6266999999999996 -5.6762839999999999 1;
	setAttr -k on ".liw";
	setAttr ".fbxID" 5;
createNode joint -n "Bip01_Head" -p "|Stand_Idle_2|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".t" -type "double3" 9.9829 0 0 ;
	setAttr ".s" -type "double3" 0.99999999999999989 1 1 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" 0 -0.10133399999999999 0.99485299999999999 0 0 -0.99485199999999996 -0.10133399999999999 0
		 1 0 0 0 -4.4000000000000012e-005 -9.8346470000000004 4.3044190000000002 1;
	setAttr -k on ".liw";
	setAttr ".fbxID" 5;
createNode joint -n "_notused_Bip01_HeadNub" -p "|Stand_Idle_2|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_Head";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".t" -type "double3" 18.3537 0 0 ;
	setAttr ".ssc" no;
	setAttr ".fbxID" 5;
createNode joint -n "Bip01_R_Clavicle" -p "|Stand_Idle_2|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".t" -type "double3" -0.8268 -0.6612 -3.9165 ;
	setAttr ".s" -type "double3" 1 1 0.99999999999999989 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" -0.99454799999999999 -9.9454800000000002e-007 -0.104286 0
		 -9.8906500000000008e-007 1 -1.0428599999999999e-007 0 0.104286 -5.7179000000000015e-010 -0.99454799999999999 0
		 -3.9165049999999999 2.9251839999999998 138.82619800000001 1;
	setAttr -k on ".liw";
	setAttr ".fbxID" 5;
createNode joint -n "Bip01_R_UpperArm" -p "|Stand_Idle_2|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_R_Clavicle";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".t" -type "double3" 10.6745 0 0 ;
	setAttr ".s" -type "double3" 0.99999999999999989 0.99999999999999989 1 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" -0.75332600000000005 0.10000199999999999 -0.65000000000000002 0
		 0.071782399999999996 0.99496899999999999 0.0698825 0 0.65371900000000005 0.0059857399999999998 -0.75671500000000003 0
		 -14.532831 2.9251239999999998 137.712976 1;
	setAttr -k on ".liw";
	setAttr ".fbxID" 5;
createNode joint -n "Bip01_R_Forearm" -p "|Stand_Idle_2|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_R_Clavicle|Bip01_R_UpperArm";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".t" -type "double3" 25.3375 0 0 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" -0.752861 -0.095945600000000006 -0.65114899999999998 0
		 -0.076500899999999997 0.99536800000000003 -0.058215000000000003 0 0.65371900000000005 0.0059856600000000003 -0.756714 0
		 -33.620182 5.4589869999999996 121.243574 1;
	setAttr -k on ".liw";
	setAttr ".fbxID" 5;
createNode joint -n "Bip01_R_Hand" -p "|Stand_Idle_2|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_R_Clavicle|Bip01_R_UpperArm|Bip01_R_Forearm";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".t" -type "double3" 24.9881 0 0 ;
	setAttr ".r" -type "double3" 90.133325000000013 -6.3004020000000009 2.5653330000000008 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" -0.67922700000000003 -0.050331500000000001 -0.73219999999999996 0
		 0.73278100000000002 0.0092545500000000003 -0.68040299999999998 0 0.0410219 -0.99868900000000005 0.030596000000000002 0
		 -52.432737000000003 3.0614050000000002 104.972679 1;
	setAttr -k on ".liw";
	setAttr ".fbxID" 5;
createNode joint -n "Bip01_R_Finger0" -p "|Stand_Idle_2|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_R_Clavicle|Bip01_R_UpperArm|Bip01_R_Forearm|Bip01_R_Hand";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".t" -type "double3" 2.3036000000000003 1.3941 2.9222 ;
	setAttr ".r" -type "double3" -81.93018 -23.314181 18.295908 ;
	setAttr ".s" -type "double3" 0.99999999999999989 1 1 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" -0.36474400000000001 -0.436471 -0.82246900000000001 0
		 -0.072261699999999998 0.89392799999999994 -0.44234699999999999 0 0.92829899999999999 -0.10191 -0.357595 0
		 -52.855969000000002 0.0400224 102.426821 1;
	setAttr -k on ".liw";
	setAttr ".fbxID" 5;
createNode joint -n "Bip01_R_Finger01" -p "|Stand_Idle_2|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_R_Clavicle|Bip01_R_UpperArm|Bip01_R_Forearm|Bip01_R_Hand|Bip01_R_Finger0";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".t" -type "double3" 4.0806 0 0 ;
	setAttr ".r" -type "double3" 9.4787915988669464e-023 3.0000000000000005e-006 24.064549 ;
	setAttr ".s" -type "double3" 1 1 0.99999999999999989 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" -0.36250900000000003 -0.034022900000000002 -0.93135900000000005 0
		 0.082749500000000004 0.99421099999999996 -0.068527199999999996 0 0.92830000000000001 -0.101911 -0.357595 0
		 -54.344414999999998 -1.7410159999999999 99.070615000000004 1;
	setAttr -k on ".liw";
	setAttr ".fbxID" 5;
createNode joint -n "_notused_Bip01_R_Finger0Nub" -p "|Stand_Idle_2|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_R_Clavicle|Bip01_R_UpperArm|Bip01_R_Forearm|Bip01_R_Hand|Bip01_R_Finger0|Bip01_R_Finger01";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".t" -type "double3" 3.3413 0 0 ;
	setAttr ".r" -type "double3" 1e-006 0 -180 ;
	setAttr ".ssc" no;
	setAttr ".fbxID" 5;
createNode joint -n "Bip01_R_Finger2" -p "|Stand_Idle_2|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_R_Clavicle|Bip01_R_UpperArm|Bip01_R_Forearm|Bip01_R_Hand";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".t" -type "double3" 7.3709 -0.4816 0.6341 ;
	setAttr ".r" -type "double3" 0.043615000000000029 -0.013407000000000007 17.086613999999997 ;
	setAttr ".s" -type "double3" 0.99999999999999989 0.99999999999999989 0.99999999999999989 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" -0.43393399999999999 -0.045623999999999998 -0.89978899999999995 0
		 0.90003599999999995 0.022875199999999998 -0.43521399999999999 0 0.040439000000000003 -0.99869600000000003 0.031137000000000001 0
		 -57.766043000000003 2.0527600000000001 99.922762000000006 1;
	setAttr -k on ".liw";
	setAttr ".fbxID" 5;
createNode joint -n "Bip01_R_Finger21" -p "|Stand_Idle_2|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_R_Clavicle|Bip01_R_UpperArm|Bip01_R_Forearm|Bip01_R_Hand|Bip01_R_Finger2";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".t" -type "double3" 3.8208 0 0 ;
	setAttr ".r" -type "double3" 0 0 29.946389999999987 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" 0.0732873 -0.028113900000000001 -0.99691399999999997 0
		 0.99648999999999999 0.042598999999999998 0.072054900000000005 0 0.0404418 -0.99869600000000003 0.0311372 0
		 -59.424045999999997 1.8784400000000001 96.484806000000006 1;
	setAttr -k on ".liw";
	setAttr ".fbxID" 5;
createNode joint -n "_notused_Bip01_R_Finger2Nub" -p "|Stand_Idle_2|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_R_Clavicle|Bip01_R_UpperArm|Bip01_R_Forearm|Bip01_R_Hand|Bip01_R_Finger2|Bip01_R_Finger21";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".t" -type "double3" 4.7174000000000005 0 0 ;
	setAttr ".r" -type "double3" 0 0 180 ;
	setAttr ".ssc" no;
	setAttr ".fbxID" 5;
createNode joint -n "Bip01_R_Finger1" -p "|Stand_Idle_2|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_R_Clavicle|Bip01_R_UpperArm|Bip01_R_Forearm|Bip01_R_Hand";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".t" -type "double3" 7.3263 -0.211 2.2784 ;
	setAttr ".r" -type "double3" -0.73310399999999953 -5.3145149999999983 22.646732999999994 ;
	setAttr ".s" -type "double3" 1 1 0.99999999999999989 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" -0.33941900000000003 -0.13520499999999999 -0.93086800000000003 0
		 0.93680799999999997 0.040593299999999999 -0.34748099999999998 0 0.084767999999999996 -0.98998600000000003 0.112883 0
		 -57.470132999999997 0.41539700000000002 99.821579 1;
	setAttr -k on ".liw";
	setAttr ".fbxID" 5;
createNode joint -n "Bip01_R_Finger11" -p "|Stand_Idle_2|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_R_Clavicle|Bip01_R_UpperArm|Bip01_R_Forearm|Bip01_R_Hand|Bip01_R_Finger1";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".t" -type "double3" 3.5597999999999996 0 0 ;
	setAttr ".r" -type "double3" -2e-006 3.999999999999999e-006 22.627677999999996 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" 0.047135799999999999 -0.10918 -0.99290400000000001 0
		 0.995286 0.089482699999999998 0.037409400000000002 0 0.084763400000000003 -0.98998600000000003 0.112883 0
		 -58.678344000000003 -0.065903900000000001 96.507863999999998 1;
	setAttr -k on ".liw";
	setAttr ".fbxID" 5;
createNode joint -n "_notused_Bip01_R_Finger1Nub" -p "|Stand_Idle_2|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_R_Clavicle|Bip01_R_UpperArm|Bip01_R_Forearm|Bip01_R_Hand|Bip01_R_Finger1|Bip01_R_Finger11";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".t" -type "double3" 4.1404000000000005 0 0 ;
	setAttr ".r" -type "double3" 0 0 180 ;
	setAttr ".ssc" no;
	setAttr ".fbxID" 5;
createNode joint -n "Bip01_R_Finger3" -p "|Stand_Idle_2|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_R_Clavicle|Bip01_R_UpperArm|Bip01_R_Forearm|Bip01_R_Hand";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".t" -type "double3" 7.2345 -0.5997 -0.9332 ;
	setAttr ".r" -type "double3" 0.043363999999999805 3.826455 18.509659999999997 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" -0.41327900000000001 0.0219578 -0.91034000000000004 0
		 0.91051400000000005 0.0239971 -0.41277999999999998 0 0.0127817 -0.999471 -0.0299104 0
		 -57.824306 3.6237469999999998 100.05504999999999 1;
	setAttr -k on ".liw";
	setAttr ".fbxID" 5;
createNode joint -n "Bip01_R_Finger31" -p "|Stand_Idle_2|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_R_Clavicle|Bip01_R_UpperArm|Bip01_R_Forearm|Bip01_R_Hand|Bip01_R_Finger3";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".t" -type "double3" 3.7241 0 0 ;
	setAttr ".r" -type "double3" 3.0000000000000013e-006 3.0000000000000005e-006 27.984287000000009 ;
	setAttr ".s" -type "double3" 1 0.99999999999999989 0.99999999999999989 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" 0.0622821 0.0306512 -0.99758800000000003 0 0.99797599999999997 0.0108983 0.062641199999999994 0
		 0.012792100000000001 -0.999471 -0.0299104 0 -59.363360999999998 3.705527 96.664748000000003 1;
	setAttr -k on ".liw";
	setAttr ".fbxID" 5;
createNode joint -n "_notused_Bip01_R_Finger3Nub" -p "|Stand_Idle_2|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_R_Clavicle|Bip01_R_UpperArm|Bip01_R_Forearm|Bip01_R_Hand|Bip01_R_Finger3|Bip01_R_Finger31";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".t" -type "double3" 4.2397 0 0 ;
	setAttr ".r" -type "double3" 3e-006 -5e-006 180 ;
	setAttr ".ssc" no;
	setAttr ".fbxID" 5;
createNode joint -n "Bip01_R_Finger4" -p "|Stand_Idle_2|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_R_Clavicle|Bip01_R_UpperArm|Bip01_R_Forearm|Bip01_R_Hand";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".t" -type "double3" 7.2292 -0.4187 -2.3639 ;
	setAttr ".r" -type "double3" -0.17713400000000007 5.5619560000000012 16.615208 ;
	setAttr ".s" -type "double3" 0.99999999999999989 0.99999999999999989 1 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" -0.44323200000000001 0.051426199999999998 -0.89493100000000003 0
		 0.89640699999999995 0.026346700000000001 -0.44244899999999998 0 0.00082491599999999997 -0.99832900000000002 -0.057776500000000001 0
		 -57.746822000000002 5.0544450000000003 99.892002000000005 1;
	setAttr -k on ".liw";
	setAttr ".fbxID" 5;
createNode joint -n "Bip01_R_Finger41" -p "|Stand_Idle_2|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_R_Clavicle|Bip01_R_UpperArm|Bip01_R_Forearm|Bip01_R_Hand|Bip01_R_Finger4";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".t" -type "double3" 3.4791000000000003 0 0 ;
	setAttr ".r" -type "double3" -1.9999999999999995e-006 -3.0000000000000005e-006 29.444368 ;
	setAttr ".s" -type "double3" 1 0.99999999999999989 1 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" 0.054672699999999998 0.057735700000000001 -0.996834 0
		 0.99850399999999995 -0.0023359700000000001 0.054628999999999997 0 0.00082546799999999999 -0.99832900000000002 -0.057777000000000002 0
		 -59.288860999999997 5.2333790000000002 96.778379000000001 1;
	setAttr -k on ".liw";
	setAttr ".fbxID" 5;
createNode joint -n "_notused_Bip01_R_Finger4Nub" -p "|Stand_Idle_2|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_R_Clavicle|Bip01_R_UpperArm|Bip01_R_Forearm|Bip01_R_Hand|Bip01_R_Finger4|Bip01_R_Finger41";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".t" -type "double3" 3.3974 0 0 ;
	setAttr ".r" -type "double3" 0 0 -180 ;
	setAttr ".ssc" no;
	setAttr ".fbxID" 5;
createNode joint -n "Bip01_L_Clavicle" -p "|Stand_Idle_2|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".t" -type "double3" -0.8268 -0.6612 3.9165 ;
	setAttr ".s" -type "double3" 0.99999999999999989 0.99999999999999956 0.99999999999999989 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" 0.99454799999999999 9.9454800000000002e-007 -0.104286 0
		 -9.8906500000000008e-007 1 1.0428599999999999e-007 0 0.104286 -5.7179000000000015e-010 0.99454799999999999 0
		 3.9165070000000002 2.9252039999999999 138.82619700000001 1;
	setAttr -k on ".liw";
	setAttr ".fbxID" 5;
createNode joint -n "Bip01_L_UpperArm" -p "|Stand_Idle_2|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_L_Clavicle";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".t" -type "double3" 10.6745 0 0 ;
	setAttr ".s" -type "double3" 1 1 0.99999999999999989 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" 0.75332500000000002 0.100004 -0.65000000000000002 0
		 -0.071785600000000005 0.99496899999999999 0.069882299999999994 0 0.65371900000000005 -0.0059834299999999997 0.756714 0
		 14.532825000000001 2.925173 137.712974 1;
	setAttr -k on ".liw";
	setAttr ".fbxID" 5;
createNode joint -n "Bip01_L_Forearm" -p "|Stand_Idle_2|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_L_Clavicle|Bip01_L_UpperArm";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".t" -type "double3" 25.3374 0 0 ;
	setAttr ".s" -type "double3" 1 1 0.99999999999999989 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" 0.752861 -0.095943299999999995 -0.65114899999999998 0
		 0.076497700000000002 0.99536899999999995 -0.058215299999999998 0 0.65371900000000005 -0.0059834199999999997 0.756714 0
		 33.620189000000003 5.4590990000000001 121.243576 1;
	setAttr -k on ".liw";
	setAttr ".fbxID" 5;
createNode joint -n "Bip01_L_Hand" -p "|Stand_Idle_2|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_L_Clavicle|Bip01_L_UpperArm|Bip01_L_Forearm";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".t" -type "double3" 24.9881 0 0 ;
	setAttr ".r" -type "double3" -90.133325 6.300397000000002 2.565332999999999 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" 0.67922800000000005 -0.050329499999999999 -0.73219999999999996 0
		 -0.73278100000000002 0.0092525000000000003 -0.68040299999999998 0 0.041019 0.99868999999999997 -0.030595899999999999 0
		 52.432744999999997 3.0615610000000002 104.972662 1;
	setAttr -k on ".liw";
	setAttr ".fbxID" 5;
createNode joint -n "Bip01_L_Finger0" -p "|Stand_Idle_2|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_L_Clavicle|Bip01_L_UpperArm|Bip01_L_Forearm|Bip01_L_Hand";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".t" -type "double3" 2.3036000000000003 1.3941 -2.9222 ;
	setAttr ".r" -type "double3" 81.930180000000021 23.314181000000019 18.295909 ;
	setAttr ".s" -type "double3" 0.99999999999999989 1 1.0000000000000002 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" 0.36474600000000001 -0.43647000000000002 -0.82246900000000001 0
		 0.072258799999999998 0.89392799999999994 -0.44234699999999999 0 0.92830000000000001 0.101913 0.357595 0
		 52.856034999999999 0.0402022 102.42681 1;
	setAttr -k on ".liw";
	setAttr ".fbxID" 5;
createNode joint -n "Bip01_L_Finger01" -p "|Stand_Idle_2|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_L_Clavicle|Bip01_L_UpperArm|Bip01_L_Forearm|Bip01_L_Hand|Bip01_L_Finger0";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".t" -type "double3" 4.0806 0 0 ;
	setAttr ".r" -type "double3" 0 0 24.06455200000001 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" 0.36250900000000003 -0.034021900000000001 -0.93135900000000005 0
		 -0.082751900000000003 0.99421199999999998 -0.068527000000000005 0 0.92829899999999999 0.101913 0.357595 0
		 54.344436000000002 -1.7408459999999999 99.070606999999995 1;
	setAttr -k on ".liw";
	setAttr ".fbxID" 5;
createNode joint -n "_notused_Bip01_L_Finger0Nub" -p "|Stand_Idle_2|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_L_Clavicle|Bip01_L_UpperArm|Bip01_L_Forearm|Bip01_L_Hand|Bip01_L_Finger0|Bip01_L_Finger01";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".t" -type "double3" 3.3413 0 0 ;
	setAttr ".ssc" no;
	setAttr ".fbxID" 5;
createNode joint -n "Bip01_L_Finger2" -p "|Stand_Idle_2|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_L_Clavicle|Bip01_L_UpperArm|Bip01_L_Forearm|Bip01_L_Hand";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".t" -type "double3" 7.3709 -0.4816 -0.6341 ;
	setAttr ".r" -type "double3" -0.043610000000000031 0.01341 17.086619000000006 ;
	setAttr ".s" -type "double3" 1 0.99999999999999989 0.99999999999999989 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" 0.43393500000000002 -0.045622700000000002 -0.89978899999999995 0
		 -0.90003599999999995 0.022871599999999999 -0.43521399999999999 0 0.040435199999999998 0.99869699999999995 -0.031137399999999999 0
		 57.766060000000003 2.0529269999999999 99.922754999999995 1;
	setAttr -k on ".liw";
	setAttr ".fbxID" 5;
createNode joint -n "Bip01_L_Finger21" -p "|Stand_Idle_2|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_L_Clavicle|Bip01_L_UpperArm|Bip01_L_Forearm|Bip01_L_Hand|Bip01_L_Finger2";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".t" -type "double3" 3.8208 0 0 ;
	setAttr ".r" -type "double3" -4.0000000000000024e-006 -4.0000000000000015e-006 29.946390000000019 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" -0.0732873 -0.0281141 -0.99691399999999997 0 -0.99649100000000002 0.042592999999999999 0.072054999999999994 0
		 0.040435899999999997 0.99869699999999995 -0.031137000000000001 0 59.424058000000002 1.878614 96.484800000000007 1;
	setAttr -k on ".liw";
	setAttr ".fbxID" 5;
createNode joint -n "_notused_Bip01_L_Finger2Nub" -p "|Stand_Idle_2|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_L_Clavicle|Bip01_L_UpperArm|Bip01_L_Forearm|Bip01_L_Hand|Bip01_L_Finger2|Bip01_L_Finger21";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".t" -type "double3" 4.7174000000000005 0 0 ;
	setAttr ".ssc" no;
	setAttr ".fbxID" 5;
createNode joint -n "Bip01_L_Finger1" -p "|Stand_Idle_2|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_L_Clavicle|Bip01_L_UpperArm|Bip01_L_Forearm|Bip01_L_Hand";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".t" -type "double3" 7.3263 -0.211 -2.2784 ;
	setAttr ".r" -type "double3" 0.73310799999999965 5.3145129999999989 22.646742999999997 ;
	setAttr ".s" -type "double3" 1 0.99999999999999989 0.99999999999999989 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" 0.33942 -0.13520399999999999 -0.93086800000000003 0
		 -0.93680799999999997 0.040590800000000003 -0.34748099999999998 0 0.084765400000000005 0.98998600000000003 -0.112883 0
		 57.470143999999998 0.415574 99.821611000000004 1;
	setAttr -k on ".liw";
	setAttr ".fbxID" 5;
createNode joint -n "Bip01_L_Finger11" -p "|Stand_Idle_2|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_L_Clavicle|Bip01_L_UpperArm|Bip01_L_Forearm|Bip01_L_Hand|Bip01_L_Finger1";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".t" -type "double3" 3.5597999999999996 0 0 ;
	setAttr ".r" -type "double3" -2.0000000000000012e-006 3.0000000000000005e-006 22.627677000000009 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" -0.047135499999999997 -0.10918 -0.99290400000000001 0
		 -0.99528499999999998 0.089479600000000006 0.037409299999999999 0 0.0847604 0.98998699999999995 -0.112883 0
		 58.678319000000002 -0.065726099999999996 96.507845000000003 1;
	setAttr -k on ".liw";
	setAttr ".fbxID" 5;
createNode joint -n "_notused_Bip01_L_Finger1Nub" -p "|Stand_Idle_2|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_L_Clavicle|Bip01_L_UpperArm|Bip01_L_Forearm|Bip01_L_Hand|Bip01_L_Finger1|Bip01_L_Finger11";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".t" -type "double3" 4.1404000000000005 0 0 ;
	setAttr ".ssc" no;
	setAttr ".fbxID" 5;
createNode joint -n "Bip01_L_Finger3" -p "|Stand_Idle_2|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_L_Clavicle|Bip01_L_UpperArm|Bip01_L_Forearm|Bip01_L_Hand";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".t" -type "double3" 7.2345 -0.5997 0.9332 ;
	setAttr ".r" -type "double3" -0.043362000000000123 -3.826458 18.509666999999997 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" 0.41327900000000001 0.021958700000000001 -0.91034000000000004 0
		 -0.91051400000000005 0.0239966 -0.41277999999999998 0 0.012781000000000001 0.999471 0.029911 0
		 57.824314000000001 3.6238670000000002 100.055049 1;
	setAttr -k on ".liw";
	setAttr ".fbxID" 5;
createNode joint -n "Bip01_L_Finger31" -p "|Stand_Idle_2|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_L_Clavicle|Bip01_L_UpperArm|Bip01_L_Forearm|Bip01_L_Hand|Bip01_L_Finger3";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".t" -type "double3" 3.7241 0 0 ;
	setAttr ".r" -type "double3" 2e-006 -9.4787915988669346e-023 27.984288 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" -0.062282200000000003 0.030651100000000001 -0.99758800000000003 0
		 -0.99797599999999997 0.0108933 0.062641199999999994 0 0.012787099999999999 0.999471 0.029910599999999999 0
		 59.363363 3.7056490000000002 96.664751999999993 1;
	setAttr -k on ".liw";
	setAttr ".fbxID" 5;
createNode joint -n "_notused_Bip01_L_Finger3Nub" -p "|Stand_Idle_2|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_L_Clavicle|Bip01_L_UpperArm|Bip01_L_Forearm|Bip01_L_Hand|Bip01_L_Finger3|Bip01_L_Finger31";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".t" -type "double3" 4.2397 0 0 ;
	setAttr ".ssc" no;
	setAttr ".fbxID" 5;
createNode joint -n "Bip01_L_Finger4" -p "|Stand_Idle_2|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_L_Clavicle|Bip01_L_UpperArm|Bip01_L_Forearm|Bip01_L_Hand";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".t" -type "double3" 7.2292 -0.4187 2.3639 ;
	setAttr ".r" -type "double3" 0.17713199999999998 -5.5619539999999983 16.615218000000002 ;
	setAttr ".s" -type "double3" 1 0.99999999999999989 1 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" 0.44323200000000001 0.051427599999999997 -0.89493100000000003 0
		 -0.89640600000000004 0.026343800000000001 -0.44244899999999998 0 0.00082181400000000003 0.99832900000000002 0.057776399999999999 0
		 57.746775 5.054627 99.891990000000007 1;
	setAttr -k on ".liw";
	setAttr ".fbxID" 5;
createNode joint -n "Bip01_L_Finger41" -p "|Stand_Idle_2|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_L_Clavicle|Bip01_L_UpperArm|Bip01_L_Forearm|Bip01_L_Hand|Bip01_L_Finger4";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".t" -type "double3" 3.4791000000000003 0 0 ;
	setAttr ".r" -type "double3" 2e-006 -3.0000000000000009e-006 29.444363999999997 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" -0.054672800000000001 0.057735500000000002 -0.996834 0
		 -0.99850399999999995 -0.00234096 0.054628799999999998 0 0.00082047800000000003 0.99832900000000002 0.057777200000000001 0
		 59.288862000000002 5.2335589999999996 96.778389000000004 1;
	setAttr -k on ".liw";
	setAttr ".fbxID" 5;
createNode joint -n "_notused_Bip01_L_Finger4Nub" -p "|Stand_Idle_2|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_L_Clavicle|Bip01_L_UpperArm|Bip01_L_Forearm|Bip01_L_Hand|Bip01_L_Finger4|Bip01_L_Finger41";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".t" -type "double3" 3.3974 0 0 ;
	setAttr ".ssc" no;
	setAttr ".fbxID" 5;
createNode joint -n "Bip01_L_Thigh" -p "|Stand_Idle_2|Bip01_Spine";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".t" -type "double3" -11.0456 2.2584 8.5201999999999991 ;
	setAttr ".s" -type "double3" 1 0.99999999999999989 1.0000000000000002 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" 0.059694200000000003 0.0067868399999999997 -0.998193 0
		 0.0013515700000000001 -0.99997599999999998 -0.00671814 0 -0.99821599999999999 -0.00094809699999999996 -0.059701999999999998 0
		 8.5202019999999994 4.3064900000000002e-005 88.396292000000003 1;
	setAttr -k on ".liw";
	setAttr ".fbxID" 5;
createNode joint -n "Bip01_L_Calf" -p "|Stand_Idle_2|Bip01_Spine|Bip01_L_Thigh";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".t" -type "double3" 36.1527 0 0 ;
	setAttr ".s" -type "double3" 0.99999999999999989 0.99999999999999989 0.99999999999999989 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" 0.059157700000000001 0.119882 -0.99102400000000002 0
		 0.0080950799999999993 -0.99278699999999998 -0.119612 0 -0.99821599999999999 -0.00094644599999999998 -0.059701400000000002 0
		 10.678299000000001 0.245397 52.308965999999998 1;
	setAttr -k on ".liw";
	setAttr ".fbxID" 5;
createNode joint -n "Bip01_L_Foot" -p "|Stand_Idle_2|Bip01_Spine|Bip01_L_Thigh|Bip01_L_Calf";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".t" -type "double3" 43.2543 0 0 ;
	setAttr ".s" -type "double3" 1 1 0.99999999999999989 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" 0 0 -1 0 0.123471 -0.99234800000000001 0 0 -0.99234800000000001 -0.123471 0 0
		 13.237131 5.4308170000000002 9.4429110000000005 1;
	setAttr -k on ".liw";
	setAttr ".fbxID" 5;
createNode joint -n "Bip01_L_Toe0" -p "|Stand_Idle_2|Bip01_Spine|Bip01_L_Thigh|Bip01_L_Calf|Bip01_L_Foot";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".t" -type "double3" 9.4714999999999989 11.2377 0 ;
	setAttr ".r" -type "double3" -5.000000000000003e-006 1.9999999999999995e-006 89.999979999999979 ;
	setAttr ".s" -type "double3" 0.99999999999999989 1 1 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" 0.123471 -0.99234800000000001 0 0 0 0 1 0 -0.99234800000000001 -0.123471 0 0
		 14.624665999999999 -5.7209120000000002 -0.028559000000000001 1;
	setAttr -k on ".liw";
	setAttr ".fbxID" 5;
createNode joint -n "_notused_Bip01_L_Toe0Nub" -p "|Stand_Idle_2|Bip01_Spine|Bip01_L_Thigh|Bip01_L_Calf|Bip01_L_Foot|Bip01_L_Toe0";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".t" -type "double3" 7.2685 0 0 ;
	setAttr ".r" -type "double3" 0 0 180 ;
	setAttr ".ssc" no;
	setAttr ".fbxID" 5;
createNode joint -n "Bip01_R_Thigh" -p "|Stand_Idle_2|Bip01_Spine";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".t" -type "double3" -11.0456 2.2585 -8.5201999999999991 ;
	setAttr ".s" -type "double3" 1 0.99999999999999989 1 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" -0.059694200000000003 0.0067826700000000002 -0.998193 0
		 -0.00134545 -0.99997599999999998 -0.0067143300000000001 0 -0.99821599999999999 0.00094221699999999997 0.059701900000000002 0
		 -8.5201879999999992 1.40303e-005 88.396298000000002 1;
	setAttr -k on ".liw";
	setAttr ".fbxID" 5;
createNode joint -n "Bip01_R_Calf" -p "|Stand_Idle_2|Bip01_Spine|Bip01_R_Thigh";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".t" -type "double3" 36.1527 0 0 ;
	setAttr ".s" -type "double3" 0.99999999999999989 1 1 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" -0.059158000000000002 0.11988699999999999 -0.99102400000000002 0
		 -0.0080972100000000005 -0.99278699999999998 -0.119617 0 -0.99821599999999999 0.00094822899999999998 0.059701999999999998 0
		 -10.678288 0.24518599999999999 52.308996999999998 1;
	setAttr -k on ".liw";
	setAttr ".fbxID" 5;
createNode joint -n "Bip01_R_Foot" -p "|Stand_Idle_2|Bip01_Spine|Bip01_R_Thigh|Bip01_R_Calf";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".t" -type "double3" 43.2543 0 0 ;
	setAttr ".s" -type "double3" 0.99999999999999989 1 1 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" 0 0 -1 0 -0.123469 -0.99234800000000001 0 0 -0.99234800000000001 0.123469 0 0
		 -13.237124 5.4308209999999999 9.442933 1;
	setAttr -k on ".liw";
	setAttr ".fbxID" 5;
createNode joint -n "Bip01_R_Toe0" -p "|Stand_Idle_2|Bip01_Spine|Bip01_R_Thigh|Bip01_R_Calf|Bip01_R_Foot";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".t" -type "double3" 9.4714999999999989 11.2377 0 ;
	setAttr ".r" -type "double3" 1.9999999999999991e-006 -1.9999999999999995e-006 89.999986 ;
	setAttr ".s" -type "double3" 0.99999999999999989 0.99999999999999989 1 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" -0.12346799999999999 -0.99234800000000001 0 0 0 0 1 0
		 -0.99234800000000001 0.12346799999999999 0 0 -14.624625999999999 -5.7209019999999997 -0.028538999999999998 1;
	setAttr -k on ".liw";
	setAttr ".fbxID" 5;
createNode joint -n "_notused_Bip01_R_Toe0Nub" -p "|Stand_Idle_2|Bip01_Spine|Bip01_R_Thigh|Bip01_R_Calf|Bip01_R_Foot|Bip01_R_Toe0";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".t" -type "double3" 7.2685 0 0 ;
	setAttr ".ssc" no;
	setAttr ".fbxID" 5;
createNode animCurveTL -n "Bip01_Spine_translateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 72 ".ktv[0:71]"  0.724 -4.749396800994873 1.76 -4.7841434478759766
		 2.792 -4.8176116943359375 3.828 -4.8480134010314941 4.86 -4.8748264312744141 5.896 -4.9058852195739746
		 6.932 -4.9428510665893555 7.964 -4.9722447395324707 9 -4.9959511756896973 10.036 -5.0352349281311035
		 11.068 -5.0907602310180664 12.104 -5.1518826484680176 13.136 -5.2147989273071289
		 14.172 -5.2764577865600586 15.208 -5.3385038375854492 16.24 -5.4068880081176758 17.276 -5.4911441802978516
		 18.312 -5.5823893547058105 19.344 -5.6659445762634277 20.38 -5.771397590637207 21.412 -5.9255490303039551
		 22.448 -6.0797910690307617 23.484 -6.2303481101989746 24.516 -6.412238597869873 25.552 -6.613105297088623
		 26.588 -6.8789052963256836 27.62 -7.3012747764587411 28.656 -7.788853645324707 29.688 -8.1887025833129883
		 30.724 -8.5157299041748047 31.76 -8.7773857116699219 32.792 -8.8876981735229492 33.828 -8.8243417739868164
		 34.86 -8.6217145919799805 35.896 -8.2880687713623047 36.932 -7.77120018005371 37.964 -6.9648427963256836
		 39 -5.8116626739501953 40.036 -4.4064254760742187 41.068 -3.0129902362823486 42.104 -1.9042303562164309
		 43.136 -1.1649999618530271 44.172 -0.75575935840606689 45.208 -0.584716796875 46.24 -0.50559079647064209
		 47.276 -0.44132116436958313 48.312 -0.41655436158180242 49.344 -0.4861955046653747
		 50.38 -0.7081679105758667 51.412 -1.1001167297363279 52.448 -1.6913249492645264 53.484 -2.6399791240692139
		 54.516 -3.9463162422180176 55.552 -5.1767778396606445 56.588 -6.0171136856079102
		 57.62 -6.6019730567932129 58.656 -7.0259609222412109 59.688 -7.2364568710327148 60.724 -7.2796416282653809
		 61.76 -7.2546091079711923 62.792 -7.2063450813293457 63.828 -7.1261835098266602 64.86 -7.0053877830505371
		 65.896 -6.8450632095336914 66.932 -6.631746768951416 67.964 -6.3906474113464355 69 -6.1677718162536621
		 70.036 -5.9384908676147461 71.068 -5.651458740234375 72.104 -5.3016424179077148 73.136 -4.9715900421142578
		 74.172 -4.7601184844970703;
createNode animCurveTL -n "Bip01_Spine_translateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 72 ".ktv[0:71]"  0.724 105.64816284179687 1.76 105.66179656982422
		 2.792 105.66867828369141 3.828 105.67433929443359 4.86 105.68039703369141 5.896 105.68325805664062
		 6.932 105.68441772460936 7.964 105.68899536132812 9 105.69621276855467 10.036 105.70310974121094
		 11.068 105.71110534667967 12.104 105.72293853759766 13.136 105.73713684082033 14.172 105.75100708007812
		 15.208 105.76543426513672 16.24 105.78139495849608 17.276 105.80200958251952 18.312 105.83153533935548
		 19.344 105.86305236816406 20.38 105.88732147216795 21.412 105.91280364990234 22.448 105.9390869140625
		 23.484 105.96400451660156 24.516 105.99046325683594 25.552 106.01394653320312 26.588 106.02581787109376
		 27.62 106.03060913085936 28.656 106.03148651123048 29.688 106.01634979248048 30.724 105.98618316650392
		 31.76 105.96685791015624 32.792 105.96648406982422 33.828 105.9626922607422 34.86 105.95503997802734
		 35.896 105.96225738525392 36.932 105.95782470703124 37.964 105.90711212158205 39 105.83004760742187
		 40.036 105.76068878173828 41.068 105.71177673339844 42.104 105.67466735839844 43.136 105.65393829345705
		 44.172 105.67190551757812 45.208 105.70573425292967 46.24 105.72348785400392 47.276 105.73661041259766
		 48.312 105.75652313232422 49.344 105.78289794921876 50.38 105.81584167480467 51.412 105.84638214111328
		 52.448 105.86865234375 53.484 105.88272094726562 54.516 105.88076782226562 55.552 105.83817291259766
		 56.588 105.72740936279295 57.62 105.57559204101562 58.656 105.4507293701172 59.688 105.37614440917967
		 60.724 105.32583618164062 61.76 105.28358459472656 62.792 105.25914001464844 63.828 105.24864196777344
		 64.86 105.22707366943359 65.896 105.19407653808594 66.932 105.16677093505859 67.964 105.14454650878906
		 69 105.12734985351562 70.036 105.11463165283205 71.068 105.09132385253906 72.104 105.05924987792967
		 73.136 105.04062652587892 74.172 105.03848266601562;
createNode animCurveTL -n "Bip01_Spine_translateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 72 ".ktv[0:71]"  0.724 61.705692291259766 1.76 61.733722686767578
		 2.792 61.748855590820313 3.828 61.746902465820312 4.86 61.739997863769531 5.896 61.731582641601563
		 6.932 61.716419219970703 7.964 61.699176788330078 9 61.685436248779297 10.036 61.670555114746094
		 11.068 61.657188415527344 12.104 61.654354095458984 13.136 61.646320343017578 14.172 61.606586456298835
		 15.208 61.543903350830078 16.24 61.476955413818359 17.276 61.402339935302734 18.312 61.330181121826179
		 19.344 61.279361724853523 20.38 61.226657867431641 21.412 61.171298980712891 22.448 61.105281829833984
		 23.484 61.029232025146477 24.516 60.954540252685547 25.552 60.891742706298835 26.588 60.805091857910163
		 27.62 60.667423248291016 28.656 60.535972595214851 29.688 60.464946746826179 30.724 60.414627075195312
		 31.76 60.349033355712891 32.792 60.297309875488281 33.828 60.265995025634766 34.86 60.227878570556641
		 35.896 60.196208953857422 36.932 60.195281982421875 37.964 60.201175689697266 39 60.18603515625
		 40.036 60.130607604980469 41.068 60.013141632080078 42.104 59.867938995361328 43.136 59.746814727783203
		 44.172 59.65814208984375 45.208 59.622734069824219 46.24 59.651607513427734 47.276 59.685722351074219
		 48.312 59.674110412597656 49.344 59.642971038818359 50.38 59.642253875732422 51.412 59.694377899169922
		 52.448 59.804801940917969 53.484 59.972007751464851 54.516 60.182605743408203 55.552 60.409191131591797
		 56.588 60.617053985595703 57.62 60.790714263916016 58.656 60.941078186035149 59.688 61.069339752197266
		 60.724 61.158153533935547 61.76 61.200069427490234 62.792 61.203956604003906 63.828 61.184432983398437
		 64.86 61.150367736816406 65.896 61.085769653320313 66.932 60.954593658447266 67.964 60.762588500976563
		 69 60.541763305664063 70.036 60.267166137695312 71.068 59.872688293457031 72.104 59.302726745605469
		 73.136 58.661052703857422 74.172 58.172290802001953;
createNode animCurveTA -n "Bip01_Spine_rotateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 72 ".ktv[0:71]"  0.724 -2.7922854423522949 1.76 -2.7248811721801758
		 2.792 -2.6412835121154785 3.828 -2.5486383438110352 4.86 -2.453332901000977 5.896 -2.3596906661987305
		 6.932 -2.2709951400756836 7.964 -2.1844136714935303 9 -2.0959343910217285 10.036 -2.0043208599090576
		 11.068 -1.9151953458786013 12.104 -1.838018536567688 13.136 -1.7722880840301514 14.172 -1.7163959741592407
		 15.208 -1.6722520589828491 16.24 -1.6351685523986816 17.276 -1.6032627820968628 18.312 -1.5806291103363037
		 19.344 -1.5665102005004885 20.38 -1.5429694652557373 21.412 -1.5227369070053101 22.448 -1.4950670003890991
		 23.484 -1.4655439853668213 24.516 -1.4387041330337524 25.552 -1.4250227212905884
		 26.588 -1.4513074159622192 27.62 -1.4989597797393801 28.656 -1.4707000255584719 29.688 -1.3394448757171633
		 30.724 -1.2058658599853516 31.76 -1.139371395111084 32.792 -1.0936014652252195 33.828 -0.97317981719970703
		 34.86 -0.78096556663513184 35.896 -0.66526550054550171 36.932 -0.73043924570083618
		 37.964 -0.85582804679870605 39 -0.84303075075149536 40.036 -0.72414904832839966 41.068 -0.67621314525604248
		 42.104 -0.68639802932739258 43.136 -0.66069537401199341 44.172 -0.7025870680809021
		 45.208 -0.88264960050582886 46.24 -0.96654576063156128 47.276 -0.72519654035568237
		 48.312 -0.23675578832626346 49.344 0.34554329514503479 50.38 0.91487926244735718
		 51.412 1.2386664152145386 52.448 1.1558949947357178 53.484 0.94612866640090965 54.516 1.0740984678268433
		 55.552 1.584968090057373 56.588 2.1976072788238525 57.62 2.7403964996337891 58.656 3.1625947952270508
		 59.688 3.4950230121612553 60.724 3.7605981826782231 61.76 3.8920519351959229 62.792 3.9318144321441646
		 63.828 4.0211567878723145 64.86 4.1894850730895996 65.896 4.3998622894287109 66.932 4.6406669616699219
		 67.964 4.8818740844726562 69 5.0874886512756357 70.036 5.2768130302429199 71.068 5.4954142570495605
		 72.104 5.7426314353942871 73.136 5.9800143241882324 74.172 6.1925930976867676;
createNode animCurveTA -n "Bip01_Spine_rotateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 72 ".ktv[0:71]"  0.724 0.50208330154418945 1.76 0.48280641436576832
		 2.792 0.46352136135101335 3.828 0.44884952902793895 4.86 0.4416787624359132 5.896 0.43149670958518982
		 6.932 0.40900775790214539 7.964 0.37935861945152283 9 0.34781095385551453 10.036 0.30937501788139343
		 11.068 0.26397109031677246 12.104 0.21456779539585111 13.136 0.15956152975559237
		 14.172 0.10512841492891312 15.208 0.060363281518220901 16.24 0.02157190814614296
		 17.276 -0.018850034102797508 18.312 -0.055928103625774384 19.344 -0.080975234508514404
		 20.38 -0.11586113274097444 21.412 -0.13450489938259125 22.448 -0.14764635264873505
		 23.484 -0.15208287537097931 24.516 -0.14716887474060061 25.552 -0.1371116042137146
		 26.588 -0.1372465044260025 27.62 -0.1497008204460144 28.656 -0.14774252474308014
		 29.688 -0.10976786911487579 30.724 -0.060167793184518807 31.76 -0.045285440981388099
		 32.792 -0.054039038717746735 33.828 -0.024289559572935104 34.86 0.037841293960809715
		 35.896 0.032308463007211685 36.932 -0.12488729506731033 37.964 -0.42075243592262274
		 39 -0.75564092397689819 40.036 -1.0547361373901367 41.068 -1.3526790142059326 42.104 -1.6879554986953735
		 43.136 -2.0226814746856689 44.172 -2.3102109432220459 45.208 -2.5193560123443604
		 46.24 -2.6543512344360356 47.276 -2.7488050460815434 48.312 -2.8032243251800537 49.344 -2.8367190361022954
		 50.38 -2.8908460140228271 51.412 -2.9121997356414795 52.448 -2.793142557144165 53.484 -2.5309152603149414
		 54.516 -2.2146883010864258 55.552 -1.9178390502929688 56.588 -1.6408039331436155
		 57.62 -1.306273341178894 58.656 -0.93717658519744862 59.688 -0.69905656576156627
		 60.724 -0.64630502462387085 61.76 -0.69503957033157349 62.792 -0.78501409292221069
		 63.828 -0.90669387578964233 64.86 -1.0382319688796997 65.896 -1.1308584213256836
		 66.932 -1.1833558082580566 67.964 -1.2490338087081907 69 -1.3431837558746338 70.036 -1.4303567409515381
		 71.068 -1.5004504919052126 72.104 -1.5706316232681274 73.136 -1.6405620574951172
		 74.172 -1.7210005521774292;
createNode animCurveTA -n "Bip01_Spine_rotateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 72 ".ktv[0:71]"  0.724 1.3846538066864014 1.76 1.3624259233474731
		 2.792 1.3395543098449707 3.828 1.3068678379058838 4.86 1.2644367218017578 5.896 1.232913613319397
		 6.932 1.2175874710083008 7.964 1.2004015445709229 9 1.1714788675308228 10.036 1.1385880708694458
		 11.068 1.1049051284790039 12.104 1.0702637434005735 13.136 1.0408432483673096 14.172 1.0149704217910769
		 15.208 0.98643946647644032 16.24 0.95528930425643921 17.276 0.91896927356719971 18.312 0.8739970326423645
		 19.344 0.82667970657348644 20.38 0.74424892663955677 21.412 0.68209636211395264 22.448 0.59841150045394897
		 23.484 0.50424981117248535 24.516 0.40417900681495672 25.552 0.31731587648391724
		 26.588 0.30458834767341614 27.62 0.34013041853904724 28.656 0.25744044780731201 29.688 0.03380916640162468
		 30.724 -0.15338757634162903 31.76 -0.26656374335289001 32.792 -0.4374979436397553
		 33.828 -0.74503028392791748 34.86 -1.1200926303863525 35.896 -1.3590487241744995
		 36.932 -1.3268245458602903 37.964 -1.1402105093002319 39 -1.0059592723846436 40.036 -0.98684799671173085
		 41.068 -1.0369075536727903 42.104 -1.0905802249908447 43.136 -1.0862895250320437
		 44.172 -1.0256147384643557 45.208 -0.96135443449020386 46.24 -0.90009850263595581
		 47.276 -0.79968065023422252 48.312 -0.67683064937591553 49.344 -0.56951075792312622
		 50.38 -0.48710530996322632 51.412 -0.50839632749557495 52.448 -0.66107076406478882
		 53.484 -0.84835523366928112 54.516 -1.0538688898086548 55.552 -1.2861781120300293
		 56.588 -1.4963890314102173 57.62 -1.7120847702026367 58.656 -1.9520803689956667 59.688 -2.1249620914459229
		 60.724 -2.1513416767120361 61.76 -2.0341496467590336 62.792 -1.8742921352386479 63.828 -1.8070570230484009
		 64.86 -1.8357806205749514 65.896 -1.9029353857040405 66.932 -2.0294570922851558 67.964 -2.206437349319458
		 69 -2.3303132057189941 70.036 -2.2954668998718266 71.068 -2.1314032077789307 72.104 -1.9771405458450317
		 73.136 -1.8802907466888428 74.172 -1.8199449777603145;
createNode animCurveTA -n "Bip01_Spine1_rotateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 72 ".ktv[0:71]"  0.724 0.75406628847122192 1.76 0.7879769802093507
		 2.792 0.86440032720565796 3.828 0.89643645286560059 4.86 0.9253840446472168 5.896 0.98244827985763539
		 6.932 1.0253303050994873 7.964 1.064674973487854 9 1.0751168727874756 10.036 1.102739691734314
		 11.068 1.1078430414199829 12.104 1.1106078624725342 13.136 1.1074066162109375 14.172 1.083716869354248
		 15.208 1.065477728843689 16.24 1.0377074480056765 17.276 1.0030893087387085 18.312 0.96349745988845825
		 19.344 0.91987168788909934 20.38 0.87314003705978405 21.412 0.82418900728225708 22.448 0.77376252412796021
		 23.484 0.7224859595298766 24.516 0.67090177536010742 25.552 0.61951613426208496 26.588 0.5688747763633728
		 27.62 0.51966065168380737 28.656 0.47274053096771235 29.688 0.42920520901679993 30.724 0.39041885733604431
		 31.76 0.35801798105239868 32.792 0.33389964699745178 33.828 0.32009649276733398 34.86 0.31867489218711853
		 35.896 0.33155187964439392 36.932 0.3603108823299408 37.964 0.40609318017959595 39 0.46939736604690546
		 40.036 0.54995441436767578 41.068 0.64663404226303101 42.104 0.75746101140975952
		 43.136 0.87955057621002197 44.172 1.0093361139297483 45.208 1.1426346302032473 46.24 1.2749553918838501
		 47.276 1.4017552137374878 48.312 1.5186681747436523 49.344 1.6218067407608032 50.38 1.7080205678939819
		 51.412 1.773490309715271 52.448 1.8134371042251585 53.484 1.8547382354736328 54.516 1.8611605167388907
		 55.552 1.8545749187469485 56.588 1.8136061429977417 57.62 1.787814736366272 58.656 1.7587922811508181
		 59.688 1.6822859048843384 60.724 1.6475235223770144 61.76 1.6061954498291016 62.792 1.5681858062744141
		 63.828 1.5412648916244509 64.86 1.4983129501342771 65.896 1.488005518913269 66.932 1.4659923315048218
		 67.964 1.460911750793457 69 1.4506046772003174 70.036 1.4426606893539429 71.068 1.4328806400299072
		 72.104 1.4158318042755127 73.136 1.4030221700668335 74.172 1.3828471899032593;
createNode animCurveTA -n "Bip01_Spine1_rotateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 72 ".ktv[0:71]"  0.724 -0.50291389226913452 1.76 -0.49723160266876226
		 2.792 -0.48241996765136719 3.828 -0.47466957569122314 4.86 -0.46711981296539307 5.896 -0.44732081890106201
		 6.932 -0.42553058266639709 7.964 -0.39574235677719122 9 -0.38350912928581238 10.036 -0.33748668432235718
		 11.068 -0.32050430774688721 12.104 -0.26489770412445068 13.136 -0.24539154767990115
		 14.172 -0.18184071779251101 15.208 -0.15127627551555634 16.24 -0.11357323080301283
		 17.276 -0.0747828409075737 18.312 -0.038300186395645142 19.344 -0.0048459642566740513
		 20.38 0.025067757815122604 21.412 0.05109696090221405 22.448 0.073124028742313399
		 23.484 0.091199956834316254 24.516 0.10561200976371764 25.552 0.1168173849582672
		 26.588 0.1254323273897171 27.62 0.13219679892063141 28.656 0.13795934617519379 29.688 0.14366644620895386
		 30.724 0.15028546750545502 31.76 0.15878188610076904 32.792 0.17005068063735965 33.828 0.18490353226661679
		 34.86 0.20397908985614777 35.896 0.22776606678962708 36.932 0.25658869743347168 37.964 0.29052811861038214
		 39 0.32946348190307617 40.036 0.37306255102157598 41.068 0.42082783579826349 42.104 0.47215884923934937
		 43.136 0.52634620666503906 44.172 0.58258479833602905 45.208 0.64011573791503917
		 46.24 0.69812262058258057 47.276 0.75598937273025524 48.312 0.81315076351165771 49.344 0.86918580532073964
		 50.38 0.92373466491699219 51.412 0.97353965044021606 52.448 1.0093412399291992 53.484 1.0775388479232788
		 54.516 1.1394741535186768 55.552 1.1590654850006106 56.588 1.2262814044952393 57.62 1.2539643049240112
		 58.656 1.2804627418518066 59.688 1.3374797105789185 60.724 1.3606866598129272 61.76 1.3873171806335447
		 62.792 1.4117854833602903 63.828 1.4296027421951294 64.86 1.461416482925415 65.896 1.4706569910049441
		 66.932 1.4963283538818359 67.964 1.503736138343811 69 1.5253282785415647 70.036 1.5453275442123413
		 71.068 1.5682834386825562 72.104 1.6011421680450442 73.136 1.621113657951355 74.172 1.6500290632247925;
createNode animCurveTA -n "Bip01_Spine1_rotateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 72 ".ktv[0:71]"  0.724 -0.43048432469367987 1.76 -0.43383941054344177
		 2.792 -0.44054567813873285 3.828 -0.44266152381896978 4.86 -0.44432097673416138 5.896 -0.44535964727401739
		 6.932 -0.44279024004936218 7.964 -0.43536105751991272 9 -0.43083345890045166 10.036 -0.41041669249534607
		 11.068 -0.40069383382797241 12.104 -0.36223897337913513 13.136 -0.34532037377357483
		 14.172 -0.27762678265571594 15.208 -0.23476780951023099 16.24 -0.17206668853759766
		 17.276 -0.093403525650501251 18.312 -0.00014940746768843385 19.344 0.10878458619117735
		 20.38 0.23409584164619449 21.412 0.37607723474502558 22.448 0.53444671630859375 23.484 0.70829874277114879
		 24.516 0.89604276418685924 25.552 1.095372200012207 26.588 1.3033038377761841 27.62 1.5162566900253296
		 28.656 1.7301927804946899 29.688 1.9408119916915898 30.724 2.1437549591064453 31.76 2.3348309993743896
		 32.792 2.5102148056030273 33.828 2.6666743755340581 34.86 2.8017807006835938 35.896 2.9139788150787358
		 36.932 3.0026941299438477 37.964 3.0683069229125977 39 3.1121177673339848 40.036 3.1362533569335938
		 41.068 3.1435127258300781 42.104 3.1372129917144775 43.136 3.1209180355072021 44.172 3.0982706546783447
		 45.208 3.0727250576019292 46.24 3.0473954677581787 47.276 3.0248606204986572 48.312 3.0070302486419682
		 49.344 2.9950642585754395 50.38 2.9892904758453369 51.412 2.9887135028839111 52.448 2.9905219078063965
		 53.484 3.0004425048828125 54.516 3.0109524726867676 55.552 3.0129916667938232 56.588 3.0138170719146729
		 57.62 3.0084528923034668 58.656 2.9993808269500732 59.688 2.9591996669769287 60.724 2.9308731555938721
		 61.76 2.8890094757080078 62.792 2.842069149017334 63.828 2.8014929294586186 64.86 2.7116458415985107
		 65.896 2.6846380233764648 66.932 2.6112813949584961 67.964 2.593111515045166 69 2.5583164691925049
		 70.036 2.5480074882507324 71.068 2.5629587173461914 72.104 2.6134536266326904 73.136 2.6580226421356201
		 74.172 2.7296326160430908;
createNode animCurveTA -n "Bip01_Spine2_rotateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 72 ".ktv[0:71]"  0.724 0.3765556812286377 1.76 0.39351746439933777
		 2.792 0.43172886967659002 3.828 0.44775095582008362 4.86 0.46222618222236633 5.896 0.49077963829040527
		 6.932 0.51224136352539063 7.964 0.53196591138839722 9 0.53719604015350342 10.036 0.5510631799697876
		 11.068 0.55363273620605469 12.104 0.55509752035140991 13.136 0.55352061986923218
		 14.172 0.54174768924713135 15.208 0.53266102075576782 16.24 0.51880669593811035 17.276 0.50153332948684703
		 18.312 0.48174771666526794 19.344 0.45993030071258545 20.38 0.43655860424041742 21.412 0.41205257177352905
		 22.448 0.38679224252700806 23.484 0.36110040545463562 24.516 0.33524161577224731
		 25.552 0.30947136878967285 26.588 0.28408074378967285 27.62 0.25939410924911499 28.656 0.23584260046482092
		 29.688 0.21399348974227905 30.724 0.19450725615024567 31.76 0.17820899188518524 32.792 0.1660173237323761
		 33.828 0.15897324681282043 34.86 0.15809120237827301 35.896 0.16432109475135803 36.932 0.17847611010074615
		 37.964 0.20110230147838593 39 0.23245832324028015 40.036 0.27241730690002441 41.068 0.32043370604515076
		 42.104 0.37549331784248352 43.136 0.43619298934936518 44.172 0.50073027610778809
		 45.208 0.5670197606086731 46.24 0.63282698392868042 47.276 0.69586813449859619 48.312 0.75398349761962891
		 49.344 0.80520439147949219 50.38 0.84795576333999634 51.412 0.88036298751831055 52.448 0.90009903907775879
		 53.484 0.92027926445007324 54.516 0.92304772138595581 55.552 0.91961771249771118
		 56.588 0.89868569374084495 57.62 0.88561993837356567 58.656 0.87095177173614491 59.688 0.83245831727981567
		 60.724 0.81500506401062012 61.76 0.79429692029953003 62.792 0.77528029680252075 63.828 0.76184093952178955
		 64.86 0.74045884609222412 65.896 0.73532193899154652 66.932 0.7243952751159668 67.964 0.72188252210617077
		 69 0.71672999858856212 70.036 0.71266019344329834 71.068 0.70759874582290649 72.104 0.69870704412460327
		 73.136 0.69204878807067871 74.172 0.6815369725227356;
createNode animCurveTA -n "Bip01_Spine2_rotateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 72 ".ktv[0:71]"  0.724 -0.25216466188430786 1.76 -0.24935975670814511
		 2.792 -0.24204574525356293 3.828 -0.23820646107196811 4.86 -0.23445516824722293 5.896 -0.22460950911045074
		 6.932 -0.21376223862171173 7.964 -0.19888900220394137 9 -0.19275887310504913 10.036 -0.16973547637462616
		 11.068 -0.16121970117092133 12.104 -0.13332153856754303 13.136 -0.12352907657623294
		 14.172 -0.091578118503093706 15.208 -0.07618299126625061 16.24 -0.057177029550075531
		 17.276 -0.037599194794893265 18.312 -0.019155483692884445 19.344 -0.0022058531176298857
		 20.38 0.012975750491023064 21.412 0.026226937770843502 22.448 0.037463091313838959
		 23.484 0.04671783372759819 24.516 0.054120860993862152 25.552 0.059889782220125198
		 26.588 0.064331904053688049 27.62 0.067810647189617157 28.656 0.070765867829322815
		 29.688 0.073651082813739777 30.724 0.07696746289730072 31.76 0.081206142902374268
		 32.792 0.086857564747333527 33.828 0.094313777983188629 34.86 0.10393912345170976
		 35.896 0.1159985139966011 36.932 0.13065581023693085 37.964 0.14798691868782043 39 0.16791939735412598
		 40.036 0.19029411673545835 41.068 0.21485525369644165 42.104 0.24126189947128299
		 43.136 0.26915177702903748 44.172 0.29811853170394897 45.208 0.32770991325378418
		 46.24 0.35753890872001648 47.276 0.38724336028099066 48.312 0.41653531789779663 49.344 0.4451856911182403
		 50.38 0.47301879525184648 51.412 0.49834021925926208 52.448 0.51651102304458618 53.484 0.55090200901031494
		 54.516 0.58196645975112915 55.552 0.59172755479812622 56.588 0.62506520748138428
		 57.62 0.63871484994888306 58.656 0.6517411470413208 59.688 0.67960023880004883 60.724 0.69087702035903931
		 61.76 0.70378142595291138 62.792 0.71561735868453979 63.828 0.72421985864639282 64.86 0.7395719289779662
		 65.896 0.74404269456863403 66.932 0.75651854276657104 67.964 0.76013004779815685
		 69 0.77075940370559692 70.036 0.78068053722381603 71.068 0.79214882850646973 72.104 0.80864369869232178
		 73.136 0.81869059801101685 74.172 0.83325099945068348;
createNode animCurveTA -n "Bip01_Spine2_rotateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 72 ".ktv[0:71]"  0.724 -0.21441252529621124 1.76 -0.21606361865997312
		 2.792 -0.2193630039691925 3.828 -0.22040477395057681 4.86 -0.22121740877628329 5.896 -0.22171673178672793
		 6.932 -0.2204399257898331 7.964 -0.21676093339920044 9 -0.21451419591903689 10.036 -0.20439691841602323
		 11.068 -0.19957754015922544 12.104 -0.1804751455783844 13.136 -0.17206723988056183
		 14.172 -0.13837745785713196 15.208 -0.11704223603010176 16.24 -0.08577921986579895
		 17.276 -0.046546913683414459 18.312 9.3014623416820541e-006 19.344 0.054394524544477463
		 20.38 0.11701167374849321 21.412 0.18793676793575287 22.448 0.267091304063797 23.484 0.35400697588920593
		 24.516 0.44787544012069702 25.552 0.54753255844116211 26.588 0.65149551630020142
		 27.62 0.75798994302749634 28.656 0.8649556040763855 29.688 0.97027462720870972 30.724 1.0717483758926392
		 31.76 1.167299747467041 32.792 1.2549827098846436 33.828 1.3332014083862305 34.86 1.4007490873336792
		 35.896 1.4568248987197876 36.932 1.5011467933654783 37.964 1.5338912010192871 39 1.55570387840271
		 40.036 1.5676640272140503 41.068 1.5711506605148315 42.104 1.5678091049194336 43.136 1.5594266653060913
		 44.172 1.5478353500366211 45.208 1.5347483158111572 46.24 1.5217385292053225 47.276 1.5100799798965456
		 48.312 1.5007877349853516 49.344 1.4944171905517578 50.38 1.4911515712738037 51.412 1.4905325174331665
		 52.448 1.4912052154541016 53.484 1.495794415473938 54.516 1.5007777214050293 55.552 1.5017249584197998
		 56.588 1.5019683837890625 57.62 1.4992396831512451 58.656 1.4946821928024292 59.688 1.4745874404907229
		 60.724 1.4604423046112061 61.76 1.4395345449447632 62.792 1.4160898923873899 63.828 1.3958321809768677
		 64.86 1.3509315252304075 65.896 1.3374348878860474 66.932 1.3007442951202393 67.964 1.2916537523269651
		 69 1.2742151021957395 70.036 1.2690231800079346 71.068 1.2764507532119751 72.104 1.301652193069458
		 73.136 1.3239108324050903 74.172 1.3596975803375244;
createNode animCurveTA -n "Bip01_Neck_rotateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 72 ".ktv[0:71]"  0.724 0.020994812250137333 1.76 0.021517418324947357
		 2.792 0.019992135465145111 3.828 0.016081411391496658 4.86 0.0098845409229397774
		 5.896 0.0016371823148801923 6.932 -0.0083875097334384918 7.964 -0.019795607775449753
		 9 -0.032139476388692856 10.036 -0.044948998838663101 11.068 -0.057745512574911118
		 12.104 -0.070045553147792816 13.136 -0.081412538886070251 14.172 -0.091437995433807373
		 15.208 -0.099800832569599152 16.24 -0.10625847429037094 17.276 -0.11065815389156342
		 18.312 -0.11293630301952363 19.344 -0.11312749981880188 20.38 -0.11134372651576996
		 21.412 -0.10777975618839264 22.448 -0.10269546508789062 23.484 -0.096409931778907776
		 24.516 -0.089258484542369843 25.552 -0.08159580826759337 26.588 -0.073773391544818892
		 27.62 -0.06611325591802597 28.656 -0.058916475623846054 29.688 -0.052436616271734238
		 30.724 -0.046856220811605453 31.76 -0.042318984866142273 32.792 -0.03888474777340889
		 33.828 -0.036585859954357147 34.86 -0.035410024225711823 35.896 -0.035118259489536285
		 36.932 -0.035660162568092346 37.964 -0.036359779536724091 39 -0.038564741611480713
		 40.036 -0.040780551731586456 41.068 -0.043245788663625731 42.104 -0.044167134910821915
		 43.136 -0.045060031116008759 44.172 -0.045360535383224487 45.208 -0.044628694653511047
		 46.24 -0.042490493506193161 47.276 -0.039258930832147598 48.312 -0.035737831145524979
		 49.344 -0.031825102865695953 50.38 -0.02692863903939724 51.412 -0.021134637296199799
		 52.448 -0.014990705065429214 53.484 -0.0089278090745210648 54.516 -0.0032813388388603926
		 55.552 0.0015922482125461102 56.588 0.0053741652518510818 57.62 0.007748906034976244
		 58.656 0.0085476664826273918 59.688 0.0075755766592919827 60.724 0.0048375325277447701
		 61.76 0.00036721507785841823 62.792 -0.0057405470870435238 63.828 -0.013275898993015288
		 64.86 -0.022004803642630581 65.896 -0.031638044863939285 66.932 -0.041774537414312363
		 67.964 -0.052275430411100381 69 -0.063204921782016754 70.036 -0.07448413223028183
		 71.068 -0.085794143378734589 72.104 -0.097015082836151137 73.136 -0.10812652111053468
		 74.172 -0.1191142648458481;
createNode animCurveTA -n "Bip01_Neck_rotateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 72 ".ktv[0:71]"  0.724 -0.093747287988662734 1.76 -0.095933876931667328
		 2.792 -0.088889189064502716 3.828 -0.071251578629016876 4.86 -0.043653123080730438
		 5.896 -0.0072080153040587893 6.932 0.036574795842170715 7.964 0.085851669311523438
		 9 0.13861878216266632 10.036 0.19276587665081024 11.068 0.24617254734039309 12.104 0.29685947299003601
		 13.136 0.34300187230110168 14.172 0.38302391767501831 15.208 0.4156748354434967 16.24 0.44007903337478638
		 17.276 0.45575070381164551 18.312 0.46257144212722784 19.344 0.46081420779228205
		 20.38 0.45108360052108759 21.412 0.43428400158882147 22.448 0.41158381104469294 23.484 0.38432234525680542
		 24.516 0.35392996668815613 25.552 0.32185479998588562 26.588 0.28949806094169617
		 27.62 0.25815284252166754 28.656 0.22893621027469643 29.688 0.20278896391391754 30.724 0.18040080368518829
		 31.76 0.16221635043621063 32.792 0.1484629213809967 33.828 0.13921795785427094 34.86 0.13440927863121033
		 35.896 0.13278111815452576 36.932 0.13469751179218292 37.964 0.13723768293857574
		 39 0.1454998105764389 40.036 0.15394875407218933 41.068 0.16348151862621307 42.104 0.16715750098228457
		 43.136 0.17082767188549042 44.172 0.17232441902160645 45.208 0.16989770531654358
		 46.24 0.16203901171684265 47.276 0.15000960230827332 48.312 0.13695977628231049 49.344 0.12245510518550871
		 50.38 0.10398750752210616 51.412 0.081895798444747925 52.448 0.058322202414274216
		 53.484 0.034918807446956635 54.516 0.012990450486540794 55.552 -0.0060971076600253582
		 56.588 -0.021005453541874889 57.62 -0.030566029250621796 58.656 -0.033847350627183921
		 59.688 -0.030222661793231964 60.724 -0.019391529262065887 61.76 -0.0013481073547154665
		 62.792 0.023601138964295387 63.828 0.054878372699022293 64.86 0.091689996421337114
		 65.896 0.13284869492053986 66.932 0.17689353227615356 67.964 0.22315634787082675
		 69 0.27182522416114807 70.036 0.32226929068565369 71.068 0.37312409281730657 72.104 0.42362517118453974
		 73.136 0.47350776195526117 74.172 0.52256608009338379;
createNode animCurveTA -n "Bip01_Neck_rotateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 72 ".ktv[0:71]"  0.724 4.0400810241699219 1.76 3.9809703826904288
		 2.792 3.9007170200347896 3.828 3.8024723529815669 4.86 3.6886343955993639 5.896 3.5617673397064209
		 6.932 3.4244425296783447 7.964 3.2791914939880371 9 3.128293514251709 10.036 2.9737789630889897
		 11.068 2.8172845840454106 12.104 2.6600606441497803 13.136 2.5030066967010498 14.172 2.3466718196868896
		 15.208 2.1912457942962646 16.24 2.0366888046264648 17.276 1.8828576803207393 18.312 1.7294553518295288
		 19.344 1.5762207508087158 20.38 1.4229898452758789 21.412 1.269731879234314 22.448 1.1166096925735474
		 23.484 0.96408796310424805 24.516 0.81290525197982788 25.552 0.66410952806472778
		 26.588 0.51903331279754639 27.62 0.37918186187744146 28.656 0.24621805548667908 29.688 0.12184225767850877
		 30.724 0.0077062104828655711 31.76 -0.094699889421463013 32.792 -0.18404568731784821
		 33.828 -0.25652283430099487 34.86 -0.30570736527442932 35.896 -0.37958219647407537
		 36.932 -0.39854806661605829 37.964 -0.41186091303825378 39 -0.4188843965530395 40.036 -0.40586012601852417
		 41.068 -0.37082114815711975 42.104 -0.34392082691192627 43.136 -0.30270099639892578
		 44.172 -0.25221827626228333 45.208 -0.19886015355587008 46.24 -0.15111935138702393
		 47.276 -0.10243649780750276 48.312 -0.031449805945158005 49.344 0.061491776257753365
		 50.38 0.1543726921081543 51.412 0.24046944081783292 52.448 0.32807749509811401 53.484 0.42215755581855774
		 54.516 0.52431219816207886 55.552 0.63630551099777222 56.588 0.75984776020050049
		 57.62 0.89651167392730702 58.656 1.0474214553833008 59.688 1.2130706310272217 60.724 1.3931437730789185
		 61.76 1.5864244699478147 62.792 1.7907373905181885 63.828 2.0029160976409912 64.86 2.2188723087310791
		 65.896 2.4361386299133301 66.932 2.6544034481048584 67.964 2.8652102947235107 69 3.0513014793395996
		 70.036 3.2054593563079834 71.068 3.3312623500823975 72.104 3.4290885925292969 73.136 3.4984192848205571
		 74.172 3.5411555767059326;
createNode animCurveTA -n "Bip01_Head_rotateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 72 ".ktv[0:71]"  0.724 -0.11689879000186919 1.76 -0.11591656506061555
		 2.792 -0.11502569168806077 3.828 -0.11511208117008208 4.86 -0.11539366841316226 5.896 -0.11695311963558196
		 6.932 -0.11802209913730619 7.964 -0.11953561753034592 9 -0.12128662317991257 10.036 -0.12307562679052352
		 11.068 -0.1247633621096611 12.104 -0.12618263065814972 13.136 -0.12710848450660706
		 14.172 -0.12809446454048157 15.208 -0.12791939079761505 16.24 -0.12715664505958557
		 17.276 -0.12570001184940338 18.312 -0.12349794059991835 19.344 -0.12058109790086743
		 20.38 -0.11703599244356155 21.412 -0.1129772961139679 22.448 -0.10855139046907424
		 23.484 -0.10429364442825316 24.516 -0.10082567483186725 25.552 -0.097436003386974335
		 26.588 -0.09281427413225174 27.62 -0.087300308048725128 28.656 -0.082521080970764174
		 29.688 -0.076413169503211961 30.724 -0.067003943026065826 31.76 -0.061963167041540153
		 32.792 -0.066039860248565674 33.828 -0.068807557225227356 34.86 -0.06592550128698349
		 35.896 -0.064035244286060333 36.932 -0.061741650104522705 37.964 -0.055501684546470642
		 39 -0.047962751239538193 40.036 -0.041734460741281509 41.068 -0.035006243735551834
		 42.104 -0.02672375924885273 43.136 -0.017756957560777664 44.172 -0.0088943261653184891
		 45.208 2.0207226043567065e-005 46.24 0.0091725261881947517 47.276 0.018192272633314133
		 48.312 0.026876915246248245 49.344 0.035066213458776474 50.38 0.042629346251487725
		 51.412 0.049476683139801025 52.448 0.055533990263938904 53.484 0.060801565647125251
		 54.516 0.065103478729724884 55.552 0.067784182727336884 56.588 0.068710722029209137
		 57.62 0.071219094097614288 58.656 0.073105834424495697 59.688 0.075635902583599091
		 60.724 0.077109850943088531 61.76 0.077283456921577454 62.792 0.076721914112567902
		 63.828 0.075159013271331787 64.86 0.071398206055164337 65.896 0.066417887806892395
		 66.932 0.063099309802055359 67.964 0.063676230609416962 69 0.06273660808801651 70.036 0.059301327913999557
		 71.068 0.054642271250486374 72.104 0.049331400543451309 73.136 0.043358664959669113
		 74.172 0.036717914044857025;
createNode animCurveTA -n "Bip01_Head_rotateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 72 ".ktv[0:71]"  0.724 -2.326542854309082 1.76 -2.3174464702606201
		 2.792 -2.3089110851287842 3.828 -2.3090922832489018 4.86 -2.3113970756530762 5.896 -2.324237585067749
		 6.932 -2.3328642845153809 7.964 -2.3449022769927979 9 -2.3580048084259033 10.036 -2.370201587677002
		 11.068 -2.3799428939819336 12.104 -2.3860652446746826 13.136 -2.3878192901611328
		 14.172 -2.3773622512817387 15.208 -2.3640613555908203 16.24 -2.3406555652618408 17.276 -2.3074338436126709
		 18.312 -2.2647786140441895 19.344 -2.21317458152771 20.38 -2.1536457538604736 21.412 -2.0876038074493408
		 22.448 -2.0167841911315918 23.484 -1.9384090900421147 24.516 -1.8430790901184082
		 25.552 -1.739930272102356 26.588 -1.6644110679626465 27.62 -1.6234660148620603 28.656 -1.5834718942642212
		 29.688 -1.5145330429077148 30.724 -1.4323734045028689 31.76 -1.4047849178314209 32.792 -1.4139956235885622
		 33.828 -1.355939507484436 34.86 -1.2526421546936035 35.896 -1.1834381818771362 36.932 -1.1158691644668579
		 37.964 -1.003597617149353 39 -0.87158066034317028 40.036 -0.74224174022674561 41.068 -0.60250824689865123
		 42.104 -0.45275807380676264 43.136 -0.30634403228759766 44.172 -0.15796051919460297
		 45.208 -0.00013473349099513143 46.24 0.16148088872432709 47.276 0.32045602798461914
		 48.312 0.47357630729675299 49.344 0.61782073974609375 50.38 0.75050997734069824 51.412 0.86935675144195557
		 52.448 0.97261899709701549 53.484 1.0591806173324585 54.516 1.1312609910964966 55.552 1.1930360794067385
		 56.588 1.2409137487411499 57.62 1.2979875802993774 58.656 1.2909482717514038 59.688 1.2628377676010134
		 60.724 1.2322468757629397 61.76 1.2057218551635742 62.792 1.1768401861190796 63.828 1.1269768476486206
		 64.86 1.0290061235427856 65.896 0.91822582483291648 66.932 0.86638057231903076 67.964 0.89175575971603394
		 69 0.89174282550811779 70.036 0.8424147367477417 71.068 0.77267336845397949 72.104 0.69519150257110596
		 73.136 0.60930377244949341 74.172 0.51465082168579102;
createNode animCurveTA -n "Bip01_Head_rotateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 72 ".ktv[0:71]"  0.724 2.1621043682098389 1.76 2.1370344161987305
		 2.792 2.1162567138671875 3.828 2.1216766834259033 4.86 2.1305186748504639 5.896 2.1771476268768311
		 6.932 2.2086424827575684 7.964 2.253865003585815 9 2.306774377822876 10.036 2.3629651069641113
		 11.068 2.4198763370513916 12.104 2.4720911979675293 13.136 2.5115430355072021 14.172 2.5840637683868408
		 15.208 2.6086208820343018 16.24 2.631746768951416 17.276 2.6467545032501221 18.312 2.6503009796142578
		 19.344 2.6424052715301514 20.38 2.6236481666564941 21.412 2.5952267646789551 22.448 2.5588893890380859
		 23.484 2.5479021072387695 24.516 2.6287667751312256 25.552 2.7440104484558105 26.588 2.7050516605377197
		 27.62 2.5095460414886475 28.656 2.3517262935638428 29.688 2.1770236492156982 30.724 1.7862145900726318
		 31.76 1.5073295831680298 32.792 1.751476526260376 33.828 2.1833510398864746 34.86 2.4171755313873291
		 35.896 2.5634784698486328 36.932 2.6761016845703125 37.964 2.6843957901000977 39 2.6850185394287109
		 40.036 2.8063201904296875 41.068 2.9920597076416016 42.104 3.0606098175048828 43.136 2.9746360778808594
		 44.172 2.8866577148437504 45.208 2.8721878528594971 46.24 2.8816251754760742 47.276 2.8855741024017334
		 48.312 2.8867580890655522 49.344 2.8882932662963867 50.38 2.8935799598693848 51.412 2.9059295654296875
		 52.448 2.9284226894378667 53.484 2.9636764526367187 54.516 2.9752509593963623 55.552 2.8814609050750732
		 56.588 2.7018811702728271 57.62 2.627230167388916 58.656 2.8393678665161133 59.688 3.2346208095550537
		 60.724 3.5601224899291992 61.76 3.7408661842346191 62.792 3.8686585426330571 63.828 4.042172908782959
		 64.86 4.3547353744506836 65.896 4.6976528167724609 66.932 4.7555866241455078 67.964 4.5982604026794442
		 69 4.4810113906860352 70.036 4.4886593818664551 71.068 4.5304274559020996 72.104 4.56549072265625
		 73.136 4.5971965789794922 74.172 4.628934383392334;
createNode animCurveTA -n "Bip01_R_Clavicle_rotateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 72 ".ktv[0:71]"  0.724 -0.13330036401748657 1.76 -0.079525865614414201
		 2.792 -0.053791139274835587 3.828 -0.058599799871444695 4.86 -0.093869514763355241
		 5.896 -0.137999027967453 6.932 -0.21924425661563873 7.964 -0.30670884251594543 9 -0.41051530838012706
		 10.036 -0.5187453031539917 11.068 -0.62926566600799561 12.104 -0.73703348636627197
		 13.136 -0.83756721019744895 14.172 -0.92765969038009644 15.208 -1.0055428743362429
		 16.24 -1.0585430860519407 17.276 -1.1056970357894895 18.312 -1.1247459650039673 19.344 -1.1251918077468872
		 20.38 -1.1169196367263794 21.412 -1.0910722017288208 22.448 -1.0529775619506836 23.484 -1.0081744194030762
		 24.516 -0.94977551698684703 25.552 -0.88856285810470581 26.588 -0.83340030908584606
		 27.62 -0.77765738964080811 28.656 -0.72558891773223877 29.688 -0.68001431226730347
		 30.724 -0.63695085048675537 31.76 -0.60550516843795776 32.792 -0.5808490514755249
		 33.828 -0.56358575820922852 34.86 -0.55397528409957886 35.896 -0.54950320720672607
		 36.932 -0.55119204521179199 37.964 -0.55353742837905884 39 -0.56670850515365612 40.036 -0.58029371500015259
		 41.068 -0.59638673067092896 42.104 -0.60163295269012451 43.136 -0.60776585340499878
		 44.172 -0.61099714040756226 45.208 -0.60692262649536133 46.24 -0.5944676399230957
		 47.276 -0.57604557275772095 48.312 -0.55390405654907227 49.344 -0.52911132574081421
		 50.38 -0.5017092227935791 51.412 -0.46810764074325567 52.448 -0.42671123147010798
		 53.484 -0.38562899827957153 54.516 -0.35177209973335266 55.552 -0.32104817032814026
		 56.588 -0.29726913571357727 57.62 -0.28345268964767456 58.656 -0.27794548869132996
		 59.688 -0.28742536902427673 60.724 -0.30941903591156006 61.76 -0.34457126259803772
		 62.792 -0.3930002748966217 63.828 -0.4540427327156068 64.86 -0.52665239572525024
		 65.896 -0.60901021957397461 66.932 -0.69868171215057373 67.964 -0.79464453458786011
		 69 -0.89764720201492298 70.036 -1.0064334869384766 71.068 -1.1183593273162842 72.104 -1.2317048311233525
		 73.136 -1.345798134803772 74.172 -1.4600622653961182;
createNode animCurveTA -n "Bip01_R_Clavicle_rotateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 72 ".ktv[0:71]"  0.724 79.156623840332031 1.76 79.059585571289063
		 2.792 78.988937377929673 3.828 78.946556091308594 4.86 78.934234619140625 5.896 78.914657592773437
		 6.932 78.944656372070327 7.964 78.971138000488281 9 79.01788330078125 10.036 79.068428039550781
		 11.068 79.124320983886733 12.104 79.181861877441406 13.136 79.237991333007813 14.172 79.291511535644531
		 15.208 79.342887878417969 16.24 79.373916625976577 17.276 79.4166259765625 18.312 79.435821533203139
		 19.344 79.44598388671875 20.38 79.461112976074219 21.412 79.464836120605469 22.448 79.462577819824219
		 23.484 79.459640502929702 24.516 79.441169738769531 25.552 79.420883178710938 26.588 79.410179138183594
		 27.62 79.394920349121094 28.656 79.379615783691406 29.688 79.366897583007812 30.724 79.348472595214844
		 31.76 79.339202880859375 32.792 79.330436706542983 33.828 79.32330322265625 34.86 79.318435668945313
		 35.896 79.314849853515625 36.932 79.313255310058594 37.964 79.311264038085938 39 79.314224243164063
		 40.036 79.317352294921875 41.068 79.322006225585938 42.104 79.321990966796875 43.136 79.323333740234375
		 44.172 79.324775695800781 45.208 79.322952270507813 46.24 79.319839477539063 47.276 79.316497802734375
		 48.312 79.309120178222656 49.344 79.300498962402344 50.38 79.29715728759767 51.412 79.292121887207017
		 52.448 79.277099609375 53.484 79.262214660644531 54.516 79.256881713867188 55.552 79.250167846679673
		 56.588 79.245651245117188 57.62 79.245536804199219 58.656 79.244468688964844 59.688 79.252777099609389
		 60.724 79.264739990234389 61.76 79.281097412109375 62.792 79.302177429199219 63.828 79.327781677246094
		 64.86 79.357505798339844 65.896 79.390701293945313 66.932 79.426536560058594 67.964 79.464874267578125
		 69 79.506401062011719 70.036 79.550971984863281 71.068 79.597572326660156 72.104 79.645698547363281
		 73.136 79.695213317871094 74.172 79.745841979980469;
createNode animCurveTA -n "Bip01_R_Clavicle_rotateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 72 ".ktv[0:71]"  0.724 170.49673461914062 1.76 170.56898498535156
		 2.792 170.64218139648435 3.828 170.71099853515625 4.86 170.77412414550781 5.896 170.83502197265625
		 6.932 170.88774108886719 7.964 170.93861389160156 9 170.98609924316406 10.036 171.03385925292969
		 11.068 171.08381652832031 12.104 171.13890075683594 13.136 171.20211791992187 14.172 171.27618408203125
		 15.208 171.36329650878906 16.24 171.46879577636719 17.276 171.58766174316406 18.312 171.72743225097656
		 19.344 171.88497924804687 20.38 172.05636596679687 21.412 172.24314880371094 22.448 172.44171142578125
		 23.484 172.6478271484375 24.516 172.86080932617187 25.552 173.0740966796875 26.588 173.2818603515625
		 27.62 173.48269653320312 28.656 173.67214965820313 29.688 173.84649658203128 30.724 174.00428771972656
		 31.76 174.14103698730469 32.792 174.25650024414062 33.828 174.34669494628906 34.86 174.40522766113281
		 35.896 174.48263549804687 36.932 174.49848937988281 37.964 174.50761413574219 39 174.49958801269531
		 40.036 174.47111511230469 41.068 174.4183349609375 42.104 174.38479614257812 43.136 174.33665466308594
		 44.172 174.28298950195315 45.208 174.23390197753906 46.24 174.20027160644531 47.276 174.1732177734375
		 48.312 174.12619018554687 49.344 174.05992126464844 50.38 174.00004577636719 51.412 173.95381164550781
		 52.448 173.91014099121094 53.484 173.85966491699222 54.516 173.79740905761719 55.552 173.72050476074219
		 56.588 173.62443542480469 57.62 173.50514221191406 58.656 173.36051940917969 59.688 173.18754577636719
		 60.724 172.98649597167969 61.76 172.75825500488281 62.792 172.50517272949219 63.828 172.23117065429687
		 64.86 171.94160461425784 65.896 171.640869140625 66.932 171.33192443847656 67.964 171.02445983886719
		 69 170.73518371582031 70.036 170.47273254394534 71.068 170.23622131347656 72.104 170.02705383300781
		 73.136 169.84646606445312 74.172 169.69305419921875;
createNode animCurveTA -n "Bip01_R_UpperArm_rotateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 72 ".ktv[0:71]"  0.724 -27.72947883605957 1.76 -29.056941986083984
		 2.792 -30.535718917846683 3.828 -31.712474822998043 4.86 -32.546581268310547 5.896 -33.384635925292969
		 6.932 -34.267765045166016 7.964 -35.115867614746094 9 -35.976249694824219 10.036 -36.739513397216797
		 11.068 -37.304710388183594 12.104 -37.808135986328125 13.136 -38.325897216796875
		 14.172 -38.651298522949219 15.208 -38.608627319335938 16.24 -38.27880859375 17.276 -37.736953735351562
		 18.312 -37.065547943115234 19.344 -36.402725219726563 20.38 -35.726181030273437 21.412 -34.946544647216797
		 22.448 -34.041629791259766 23.484 -33.10577392578125 24.516 -32.410022735595703 25.552 -31.990346908569339
		 26.588 -31.612155914306641 27.62 -31.249124526977536 28.656 -31.026668548583981 29.688 -30.765089035034183
		 30.724 -29.86768913269043 31.76 -28.14219856262207 32.792 -26.235134124755859 33.828 -24.541412353515625
		 34.86 -22.541706085205082 35.896 -20.161867141723633 36.932 -18.209659576416016 37.964 -16.611425399780273
		 39 -14.757645606994632 40.036 -13.371111869812012 41.068 -13.540225982666016 42.104 -15.349650382995607
		 43.136 -18.160591125488281 44.172 -20.494707107543945 45.208 -21.727724075317383
		 46.24 -23.027294158935547 47.276 -24.908674240112308 48.312 -26.605703353881836 49.344 -27.81328010559082
		 50.38 -28.785301208496101 51.412 -29.733928680419918 52.448 -30.670635223388675 53.484 -31.482509613037109
		 54.516 -32.55987548828125 55.552 -34.455322265625 56.588 -36.626354217529297 57.62 -38.158252716064453
		 58.656 -39.147480010986328 59.688 -40.108871459960938 60.724 -41.070114135742195
		 61.76 -41.807445526123047 62.792 -42.280536651611328 63.828 -42.577213287353523 64.86 -42.864349365234375
		 65.896 -43.165061950683594 66.932 -43.210678100585938 67.964 -42.904979705810547
		 69 -42.468517303466797 70.036 -42.124202728271491 71.068 -41.851051330566406 72.104 -41.281391143798835
		 73.136 -40.443328857421875 74.172 -39.907115936279297;
createNode animCurveTA -n "Bip01_R_UpperArm_rotateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 72 ".ktv[0:71]"  0.724 -69.275459289550781 1.76 -69.066001892089844
		 2.792 -68.873611450195312 3.828 -68.752059936523438 4.86 -68.676383972167969 5.896 -68.584136962890625
		 6.932 -68.484817504882813 7.964 -68.384750366210937 9 -68.288307189941406 10.036 -68.225852966308594
		 11.068 -68.171463012695313 12.104 -68.083740234375 13.136 -67.959022521972656 14.172 -67.860565185546875
		 15.208 -67.852691650390625 16.24 -67.905143737792969 17.276 -67.969032287597656 18.312 -68.023307800292969
		 19.344 -68.076454162597656 20.38 -68.184577941894531 21.412 -68.350021362304688 22.448 -68.508743286132812
		 23.484 -68.614830017089844 24.516 -68.599067687988281 25.552 -68.40911865234375 26.588 -68.14208984375
		 27.62 -67.928367614746094 28.656 -67.73834228515625 29.688 -67.500907897949219 30.724 -67.32379150390625
		 31.76 -67.294303894042969 32.792 -67.342330932617188 33.828 -67.539443969726563 34.86 -67.948074340820312
		 35.896 -68.369430541992188 36.932 -68.719940185546875 37.964 -69.24761962890625 39 -70.025291442871094
		 40.036 -70.656501770019531 41.068 -70.74072265625 42.104 -70.2681884765625 43.136 -69.501426696777344
		 44.172 -68.769912719726563 45.208 -68.291519165039063 46.24 -67.953506469726562 47.276 -67.576934814453125
		 48.312 -67.184371948242187 49.344 -66.864723205566406 50.38 -66.662498474121094 51.412 -66.551361083984375
		 52.448 -66.490272521972656 53.484 -66.639015197753906 54.516 -67.088912963867187
		 55.552 -67.420600891113281 56.588 -67.253257751464844 57.62 -66.896812438964844 58.656 -66.684394836425781
		 59.688 -66.468254089355469 60.724 -66.149185180664063 61.76 -65.901451110839844 62.792 -65.835350036621094
		 63.828 -65.931632995605469 64.86 -66.133529663085938 65.896 -66.330940246582031 66.932 -66.473251342773438
		 67.964 -66.614143371582031 69 -66.710029602050781 70.036 -66.568916320800781 71.068 -66.168235778808594
		 72.104 -65.702194213867188 73.136 -65.290428161621094 74.172 -65.001029968261719;
createNode animCurveTA -n "Bip01_R_UpperArm_rotateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 72 ".ktv[0:71]"  0.724 -1.8098015785217287 1.76 -1.2287852764129641
		 2.792 -0.54379874467849731 3.828 -0.3649310171604157 4.86 -0.69356215000152588 5.896 -0.98688000440597556
		 6.932 -1.165081262588501 7.964 -1.3105987310409546 9 -1.3336349725723269 10.036 -1.3797198534011841
		 11.068 -1.5519975423812866 12.104 -1.627316951751709 13.136 -1.4932284355163574 14.172 -1.4725213050842283
		 15.208 -1.8516401052474976 16.24 -2.5034215450286865 17.276 -3.2945826053619385 18.312 -4.1031880378723145
		 19.344 -4.7623519897460938 20.38 -5.3546028137207031 21.412 -6.002166748046875 22.448 -6.6924405097961435
		 23.484 -7.2771897315979004 24.516 -7.3500037193298349 25.552 -6.8346915245056152
		 26.588 -6.1167292594909668 27.62 -5.3234667778015137 28.656 -4.2755274772644043 29.688 -3.179682731628418
		 30.724 -2.9562690258026123 31.76 -3.9610385894775395 32.792 -5.2813167572021484 33.828 -6.4498052597045907
		 34.86 -8.2487363815307617 35.896 -10.672627449035645 36.932 -12.585812568664553 37.964 -14.268417358398438
		 39 -16.612461090087891 40.036 -18.441455841064453 41.068 -18.153667449951172 42.104 -15.642551422119141
		 43.136 -11.836353302001951 44.172 -8.899052619934082 45.208 -7.8272647857666016 46.24 -6.944453239440918
		 47.276 -5.4003911018371582 48.312 -4.2287325859069824 49.344 -3.8713910579681396
		 50.38 -3.9722402095794682 51.412 -4.1813149452209473 52.448 -4.4136881828308105 53.484 -4.90460205078125
		 54.516 -5.1423320770263672 55.552 -4.0610866546630859 56.588 -2.1383895874023442
		 57.62 -0.82539784908294667 58.656 -0.23103545606136325 59.688 0.46792027354240417
		 60.724 1.3912903070449829 61.76 2.0823054313659668 62.792 2.3778893947601318 63.828 2.4058377742767334
		 64.86 2.4217121601104736 65.896 2.5218257904052734 66.932 2.3694067001342773 67.964 1.782378077507019
		 69 1.0841294527053833 70.036 0.71327561140060425 71.068 0.65118014812469482 72.104 0.22603154182434085
		 73.136 -0.62313789129257202 74.172 -1.1488912105560305;
createNode animCurveTA -n "Bip01_R_Forearm_rotateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 72 ".ktv[0:71]"  0.724 -0.094061538577079773 1.76 -0.10491982102394104
		 2.792 -0.11829789727926256 3.828 -0.12626852095127106 4.86 -0.12715809047222135 5.896 -0.12779711186885834
		 6.932 -0.12974759936332703 7.964 -0.13118970394134519 9 -0.13298729062080383 10.036 -0.13477960228919983
		 11.068 -0.13543306291103363 12.104 -0.13701383769512177 13.136 -0.14180567860603333
		 14.172 -0.14541251957416534 15.208 -0.14256606996059418 16.24 -0.13654804229736328
		 17.276 -0.13159599900245669 18.312 -0.12805241346359253 19.344 -0.12462850660085678
		 20.38 -0.11761719733476642 21.412 -0.10578300058841704 22.448 -0.092874042689800262
		 23.484 -0.083556994795799241 24.516 -0.083026006817817702 25.552 -0.094403043389320374
		 26.588 -0.11568968743085864 27.62 -0.14226861298084259 28.656 -0.16946311295032501
		 29.688 -0.19266378879547119 30.724 -0.20192749798297879 31.76 -0.1884206235408783
		 32.792 -0.15918076038360596 33.828 -0.12327994406223297 34.86 -0.079204805195331573
		 35.896 -0.034707006067037582 36.932 -0.0080777192488312721 37.964 0.00013532557932194322
		 39 0.0021349166054278612 40.036 0.0022633098997175698 41.068 -0.0033237857278436422
		 42.104 -0.019576944410800937 43.136 -0.050341647118330002 44.172 -0.084957219660282121
		 45.208 -0.10737523436546326 46.24 -0.12448842078447343 47.276 -0.14369513094425199
		 48.312 -0.15569113194942474 49.344 -0.15479066967964172 50.38 -0.14727720618247986
		 51.412 -0.14407846331596377 52.448 -0.14707420766353607 53.484 -0.14328937232494354
		 54.516 -0.13532374799251556 55.552 -0.13880141079425812 56.588 -0.15411621332168579
		 57.62 -0.15989227592945099 58.656 -0.15088801085948944 59.688 -0.14633689820766449
		 60.724 -0.15692850947380066 61.76 -0.17458286881446838 62.792 -0.18810941278934479
		 63.828 -0.19263844192028048 64.86 -0.19080907106399536 65.896 -0.18610265851020813
		 66.932 -0.17521519958972931 67.964 -0.15786002576351166 69 -0.14418257772922516 70.036 -0.14478215575218203
		 71.068 -0.15756484866142273 72.104 -0.16952814161777496 73.136 -0.17717766761779785
		 74.172 -0.18546882271766665;
createNode animCurveTA -n "Bip01_R_Forearm_rotateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 72 ".ktv[0:71]"  0.724 0.82892513275146495 1.76 0.85042554140090942
		 2.792 0.88064801692962635 3.828 0.89559805393218994 4.86 0.89175361394882213 5.896 0.88825184106826782
		 6.932 0.88975739479064941 7.964 0.89227831363677979 9 0.89804315567016624 10.036 0.90444588661193848
		 11.068 0.90740293264389038 12.104 0.91217225790023804 13.136 0.92502313852310181
		 14.172 0.93569624423980702 15.208 0.93138772249221813 16.24 0.91778826713562012 17.276 0.90237390995025646
		 18.312 0.8878207802772522 19.344 0.87735414505004883 20.38 0.86706554889678955 21.412 0.85229313373565685
		 22.448 0.83425086736679077 23.484 0.81765168905258179 24.516 0.81590652465820324
		 25.552 0.83773273229598988 26.588 0.87907254695892334 27.62 0.932475745677948 28.656 0.98667222261428844
		 29.688 1.0295780897140503 30.724 1.0441069602966309 31.76 1.0184245109558103 32.792 0.96773296594619762
		 33.828 0.90520620346069347 34.86 0.81097245216369629 35.896 0.68585711717605591 36.932 0.59200114011764526
		 37.964 0.55905866622924805 39 0.54736125469207764 40.036 0.54663914442062378 41.068 0.57477372884750366
		 42.104 0.63473522663116455 43.136 0.72389674186706543 44.172 0.8038657307624818 45.208 0.84266382455825806
		 46.24 0.87167769670486461 47.276 0.91023683547973633 48.312 0.93543940782546997 49.344 0.93669849634170532
		 50.38 0.92314952611923218 51.412 0.91347885131835938 52.448 0.91613578796386719 53.484 0.91054493188858032
		 54.516 0.90016692876815807 55.552 0.9180152416229248 56.588 0.958523750305176 57.62 0.97677534818649292
		 58.656 0.96880382299423229 59.688 0.97131013870239269 60.724 0.9953935146331786 61.76 1.022940993309021
		 62.792 1.040665864944458 63.828 1.0426995754241943 64.86 1.0329735279083252 65.896 1.0205909013748169
		 66.932 1.0008336305618286 67.964 0.96944212913513172 69 0.94232720136642456 70.036 0.94183844327926647
		 71.068 0.96506470441818237 72.104 0.98513966798782349 73.136 0.99744552373886097
		 74.172 1.0128637552261353;
createNode animCurveTA -n "Bip01_R_Forearm_rotateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 72 ".ktv[0:71]"  0.724 -12.598911285400392 1.76 -13.227798461914062
		 2.792 -14.097756385803224 3.828 -14.541797637939451 4.86 -14.45673656463623 5.896 -14.378090858459473
		 6.932 -14.434986114501957 7.964 -14.511234283447266 9 -14.669657707214357 10.036 -14.843499183654783
		 11.068 -14.92192554473877 12.104 -15.054024696350099 13.136 -15.415567398071289 14.172 -15.711891174316404
		 15.208 -15.578232765197754 16.24 -15.185866355895996 17.276 -14.760753631591795 18.312 -14.370529174804688
		 19.344 -14.08098030090332 20.38 -13.760134696960447 21.412 -13.285623550415041 22.448 -12.723237991333008
		 23.484 -12.231125831604004 24.516 -12.183409690856934 25.552 -12.822929382324221
		 26.588 -14.036676406860352 27.62 -15.602500915527344 28.656 -17.197963714599609 29.688 -18.482900619506836
		 30.724 -18.934526443481445 31.76 -18.169775009155273 32.792 -16.635995864868164 33.828 -14.755410194396973
		 34.86 -12.030635833740234 35.896 -8.5422468185424805 36.932 -5.9766459465026855 37.964 -5.0844330787658691
		 39 -4.7721419334411621 40.036 -4.7524094581604004 41.068 -5.5080957412719727 42.104 -7.1418819427490234
		 43.136 -9.6185302734375 44.172 -11.899802207946776 45.208 -13.062332153320313 46.24 -13.94315242767334
		 47.276 -15.081377983093263 48.312 -15.820394515991213 49.344 -15.839839935302734
		 50.38 -15.430535316467285 51.412 -15.163406372070313 52.448 -15.26107978820801 53.484 -15.084584236145021
		 54.516 -14.747795104980467 55.552 -15.214861869812012 56.588 -16.355134963989258
		 57.62 -16.859970092773438 58.656 -16.579496383666992 59.688 -16.599279403686523 60.724 -17.291074752807617
		 61.76 -18.134342193603516 62.792 -18.700311660766602 63.828 -18.796421051025391 64.86 -18.542936325073239
		 65.896 -18.194215774536133 66.932 -17.601245880126953 67.964 -16.659578323364258
		 69 -15.860668182373047 70.036 -15.855094909667969 71.068 -16.550666809082031 72.104 -17.162160873413086
		 73.136 -17.541370391845703 74.172 -18.002592086791992;
createNode animCurveTA -n "Bip01_L_Clavicle_rotateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 72 ".ktv[0:71]"  0.724 0.68889570236206055 1.76 0.63429749011993408
		 2.792 0.56870400905609131 3.828 0.48918768763542175 4.86 0.39690196514129639 5.896 0.29398098587989807
		 6.932 0.18404169380664823 7.964 0.072431273758411407 9 -0.048946361988782883 10.036 -0.16339409351348877
		 11.068 -0.27108520269393921 12.104 -0.37016063928604126 13.136 -0.45548486709594727
		 14.172 -0.52888041734695435 15.208 -0.58612573146820068 16.24 -0.62755751609802246
		 17.276 -0.65113961696624767 18.312 -0.65778690576553345 19.344 -0.65080106258392345
		 20.38 -0.62714177370071411 21.412 -0.59119385480880737 22.448 -0.5446208119392395
		 23.484 -0.48920303583145142 24.516 -0.43036052584648127 25.552 -0.36864277720451349
		 26.588 -0.3045513927936554 27.62 -0.24325063824653623 28.656 -0.18612773716449735
		 29.688 -0.13437835872173309 30.724 -0.091663591563701644 31.76 -0.054200109094381325
		 32.792 -0.027557330206036568 33.828 -0.0070832674391567707 34.86 0.0048737875185906887
		 35.896 0.011179987341165546 36.932 0.0112490588799119 37.964 0.01088416576385498
		 39 0.001328679732978344 40.036 -0.012101226486265659 41.068 -0.02199261449277401
		 42.104 -0.026521945372223858 43.136 -0.026153609156608588 44.172 -0.021878931671380997
		 45.208 -0.015693677589297295 46.24 0.0049608550034463406 47.276 0.031845100224018097
		 48.312 0.059577036648988717 49.344 0.089122205972671509 50.38 0.12515181303024292
		 51.412 0.16690592467784882 52.448 0.21102827787399292 53.484 0.25257235765457153
		 54.516 0.29340225458145142 55.552 0.32488283514976501 56.588 0.34809121489524841
		 57.62 0.35977345705032349 58.656 0.35724270343780518 59.688 0.34696248173713684 60.724 0.31415295600891113
		 61.76 0.27542933821678162 62.792 0.21668572723865509 63.828 0.14629246294498444 64.86 0.071446150541305542
		 65.896 -0.016514852643013 66.932 -0.10866734385490416 67.964 -0.20097784698009491
		 69 -0.29977455735206604 70.036 -0.39871674776077271 71.068 -0.49787801504135137 72.104 -0.59438163042068481
		 73.136 -0.68870079517364502 74.172 -0.7787742018699646;
createNode animCurveTA -n "Bip01_L_Clavicle_rotateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 72 ".ktv[0:71]"  0.724 -79.629066467285156 1.76 -79.526817321777344
		 2.792 -79.431251525878906 3.828 -79.339881896972656 4.86 -79.2518310546875 5.896 -79.166763305664063
		 6.932 -79.086143493652358 7.964 -79.014396667480469 9 -78.929832458496094 10.036 -78.858314514160156
		 11.068 -78.794113159179673 12.104 -78.735771179199219 13.136 -78.689384460449219
		 14.172 -78.647468566894531 15.208 -78.616127014160156 16.24 -78.592964172363281 17.276 -78.581733703613281
		 18.312 -78.580711364746094 19.344 -78.584030151367188 20.38 -78.600334167480469 21.412 -78.622802734375
		 22.448 -78.651336669921889 23.484 -78.685905456542983 24.516 -78.719085693359375
		 25.552 -78.753776550292983 26.588 -78.792770385742187 27.62 -78.829010009765625 28.656 -78.86279296875
		 29.688 -78.894264221191406 30.724 -78.917991638183594 31.76 -78.942741394042983 32.792 -78.958145141601562
		 33.828 -78.973381042480469 34.86 -78.983810424804687 35.896 -78.991462707519531 36.932 -78.996444702148438
		 37.964 -79.002243041992188 39 -79.005088806152358 40.036 -79.001052856445312 41.068 -79.0069580078125
		 42.104 -79.00799560546875 43.136 -79.01883697509767 44.172 -79.031753540039063 45.208 -79.038108825683594
		 46.24 -79.058341979980469 47.276 -79.079452514648437 48.312 -79.099388122558594 49.344 -79.11885833740233
		 50.38 -79.140045166015625 51.412 -79.16192626953125 52.448 -79.183723449707017 53.484 -79.200538635253906
		 54.516 -79.219268798828125 55.552 -79.227577209472656 56.588 -79.231086730957017
		 57.62 -79.227371215820327 58.656 -79.213798522949219 59.688 -79.204109191894531 60.724 -79.17230224609375
		 61.76 -79.148750305175781 62.792 -79.10687255859375 63.828 -79.060478210449219 64.86 -79.020973205566406
		 65.896 -78.968597412109375 66.932 -78.9161376953125 67.964 -78.869552612304688 69 -78.816459655761719
		 70.036 -78.767013549804688 71.068 -78.717063903808594 72.104 -78.669807434082031
		 73.136 -78.623222351074219 74.172 -78.581192016601591;
createNode animCurveTA -n "Bip01_L_Clavicle_rotateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 72 ".ktv[0:71]"  0.724 170.07356262207031 1.76 170.14289855957031
		 2.792 170.25056457519531 3.828 170.39582824707031 4.86 170.57450866699222 5.896 170.78143310546875
		 6.932 171.01042175292969 7.964 171.25491333007813 9 171.51065063476562 10.036 171.76919555664062
		 11.068 172.02589416503906 12.104 172.2762451171875 13.136 172.51614379882812 14.172 172.7430419921875
		 15.208 172.9547119140625 16.24 173.15011596679687 17.276 173.32881164550781 18.312 173.49122619628906
		 19.344 173.63842773437503 20.38 173.77151489257812 21.412 173.89234924316406 22.448 174.00271606445312
		 23.484 174.10452270507812 24.516 174.19972229003906 25.552 174.28964233398435 26.588 174.37518310546875
		 27.62 174.45747375488281 28.656 174.53675842285159 29.688 174.61297607421875 30.724 174.68609619140625
		 31.76 174.75474548339844 32.792 174.81884765625 33.828 174.87384033203125 34.86 174.91372680664062
		 35.896 174.98397827148435 36.932 175.00572204589844 37.964 175.02285766601562 39 175.0438232421875
		 40.036 175.04573059082031 41.068 175.02671813964844 42.104 175.00619506835935 43.136 174.97061157226562
		 44.172 174.92192077636719 45.208 174.86416625976562 46.24 174.8013916015625 47.276 174.73023986816406
		 48.312 174.63502502441406 49.344 174.51531982421875 50.38 174.388427734375 51.412 174.26148986816406
		 52.448 174.13009643554687 53.484 173.99278259277344 54.516 173.84947204589844 55.552 173.70233154296875
		 56.588 173.55145263671875 57.62 173.39791870117187 58.656 173.24250793457031 59.688 173.08438110351562
		 60.724 172.92762756347656 61.76 172.769775390625 62.792 172.61569213867187 63.828 172.46574401855469
		 64.86 172.32148742675781 65.896 172.18507385253906 66.932 172.05287170410156 67.964 171.93154907226562
		 69 171.83952331542969 70.036 171.78176879882813 71.068 171.75250244140625 72.104 171.74958801269534
		 73.136 171.7730712890625 74.172 171.820556640625;
createNode animCurveTA -n "Bip01_L_UpperArm_rotateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 72 ".ktv[0:71]"  0.724 31.22977447509766 1.76 30.905353546142589
		 2.792 30.397068023681641 3.828 29.672634124755859 4.86 28.844327926635742 5.896 28.016031265258793
		 6.932 27.24000358581543 7.964 26.563108444213871 9 25.99205207824707 10.036 25.513734817504886
		 11.068 25.173633575439457 12.104 25.017744064331055 13.136 24.923208236694336 14.172 24.654623031616211
		 15.208 24.1944580078125 16.24 23.640295028686523 17.276 23.019342422485352 18.312 22.572870254516605
		 19.344 22.457996368408203 20.38 22.3040771484375 21.412 21.802413940429688 22.448 21.05609130859375
		 23.484 20.241981506347656 24.516 19.537364959716797 25.552 19.026458740234375 26.588 18.595792770385746
		 27.62 18.014617919921875 28.656 17.205509185791016 29.688 16.485025405883789 30.724 16.056493759155273
		 31.76 15.712200164794922 32.792 15.257984161376953 33.828 14.555403709411626 34.86 13.653055191040041
		 35.896 13.170524597167969 36.932 13.481832504272459 37.964 13.893957138061525 39 13.734598159790044
		 40.036 13.133478164672852 41.068 12.387804985046388 42.104 11.658843994140623 43.136 11.198967933654783
		 44.172 11.181221008300779 45.208 11.483303070068359 46.24 11.784031867980957 47.276 11.677468299865724
		 48.312 11.079843521118164 49.344 10.502020835876465 50.38 10.696065902709959 51.412 11.937553405761721
		 52.448 13.406571388244627 53.484 14.030871391296388 54.516 13.705081939697267 55.552 12.88295841217041
		 56.588 11.968053817749023 57.62 11.401591300964355 58.656 11.391281127929688 59.688 11.50749397277832
		 60.724 11.550791740417482 61.76 11.858873367309572 62.792 12.236131668090822 63.828 12.328387260437012
		 64.86 12.024684906005859 65.896 11.040568351745604 66.932 9.1631660461425781 67.964 6.7820243835449219
		 69 5.2917695045471191 70.036 5.5756239891052246 71.068 6.6216835975646973 72.104 7.3701291084289551
		 73.136 8.1324634552001953 74.172 9.0898580551147461;
createNode animCurveTA -n "Bip01_L_UpperArm_rotateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 72 ".ktv[0:71]"  0.724 67.200164794921875 1.76 67.226982116699219
		 2.792 67.263870239257813 3.828 67.373397827148438 4.86 67.547698974609375 5.896 67.713973999023438
		 6.932 67.851493835449219 7.964 67.981391906738281 9 68.093788146972656 10.036 68.155632019042969
		 11.068 68.175895690917969 12.104 68.18597412109375 13.136 68.185951232910156 14.172 68.18096923828125
		 15.208 68.205612182617188 16.24 68.270477294921875 17.276 68.323326110839844 18.312 68.316139221191406
		 19.344 68.286026000976563 20.38 68.270431518554688 21.412 68.247993469238281 22.448 68.24365234375
		 23.484 68.296844482421875 24.516 68.334869384765625 25.552 68.28070068359375 26.588 68.134094238281264
		 27.62 67.927635192871094 28.656 67.746269226074219 29.688 67.626991271972656 30.724 67.493194580078125
		 31.76 67.342178344726563 32.792 67.299583435058594 33.828 67.442672729492188 34.86 67.702033996582031
		 35.896 67.947052001953125 36.932 68.16082763671875 37.964 68.480659484863281 39 68.93109130859375
		 40.036 69.20458984375 41.068 69.077354431152344 42.104 68.749160766601562 43.136 68.475944519042969
		 44.172 68.276168823242188 45.208 68.098442077636719 46.24 67.951118469238281 47.276 67.841453552246094
		 48.312 67.724052429199219 49.344 67.602706909179688 50.38 67.560676574707031 51.412 67.58209228515625
		 52.448 67.551322937011719 53.484 67.495574951171875 54.516 67.617332458496094 55.552 67.971977233886719
		 56.588 68.336021423339844 57.62 68.564338684082031 58.656 68.698173522949219 59.688 68.798896789550781
		 60.724 68.8824462890625 61.76 68.907188415527344 62.792 68.897163391113281 63.828 68.918701171875
		 64.86 69.013221740722656 65.896 69.239761352539077 66.932 69.582870483398437 67.964 69.899482727050781
		 69 70.063041687011719 70.036 70.086845397949219 71.068 70.050178527832031 72.104 70.048171997070312
		 73.136 70.081329345703125 74.172 70.067787170410156;
createNode animCurveTA -n "Bip01_L_UpperArm_rotateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 72 ".ktv[0:71]"  0.724 0.28692469000816345 1.76 -0.050355281680822365
		 2.792 -0.64229696989059448 3.828 -1.5780795812606812 4.86 -2.6966328620910645 5.896 -3.8026721477508545
		 6.932 -4.8083534240722656 7.964 -5.6600823402404785 9 -6.337897777557373 10.036 -6.8376250267028809
		 11.068 -7.1028356552124023 12.104 -7.0818591117858887 13.136 -6.9348373413085937
		 14.172 -6.9898271560668945 15.208 -7.2938518524169913 16.24 -7.7141995429992676 17.276 -8.174921989440918
		 18.312 -8.3091793060302734 19.344 -7.9229650497436532 20.38 -7.5549330711364755 21.412 -7.6139631271362305
		 22.448 -7.975835323333742 23.484 -8.4334993362426758 24.516 -8.7066640853881836 25.552 -8.6380043029785156
		 26.588 -8.3896579742431641 27.62 -8.313694953918457 28.656 -8.5852975845336914 29.688 -8.7971973419189453
		 30.724 -8.6279869079589844 31.76 -8.3703317642211914 32.792 -8.3799047470092773 33.828 -8.9013614654541016
		 34.86 -9.8138113021850586 35.896 -10.174466133117676 36.932 -9.4645433425903338 37.964 -8.7301483154296875
		 39 -8.9063825607299805 40.036 -9.6129961013793945 41.068 -10.311811447143556 42.104 -10.917097091674805
		 43.136 -11.227153778076172 44.172 -11.006536483764648 45.208 -10.399324417114258
		 46.24 -9.8611679077148437 47.276 -9.9715871810913086 48.312 -10.830811500549316 49.344 -11.726968765258787
		 50.38 -11.630060195922852 51.412 -10.140144348144531 52.448 -8.35992431640625 53.484 -7.787311553955079
		 54.516 -8.6773738861083984 55.552 -10.404236793518066 56.588 -12.243422508239746
		 57.62 -13.4866943359375 58.656 -13.855653762817385 59.688 -13.977461814880376 60.724 -14.13320255279541
		 61.76 -13.824252128601074 62.792 -13.334963798522947 63.828 -13.205301284790044 64.86 -13.617617607116699
		 65.896 -14.998147010803221 66.932 -17.645977020263672 67.964 -20.968162536621101
		 69 -22.989498138427734 70.036 -22.502933502197266 71.068 -20.961532592773441 72.104 -19.883159637451172
		 73.136 -18.83629035949707 74.172 -17.529239654541016;
createNode animCurveTA -n "Bip01_L_Forearm_rotateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 72 ".ktv[0:71]"  0.724 0.086429491639137268 1.76 0.083557821810245514
		 2.792 0.077334553003311157 3.828 0.066361077129840851 4.86 0.051312372088432312 5.896 0.035293962806463242
		 6.932 0.021718716248869896 7.964 0.012561311013996599 9 0.0091732684522867203 10.036 0.0086821466684341431
		 11.068 0.012458253651857376 12.104 0.019439833238720897 13.136 0.026211919263005257
		 14.172 0.02941061370074749 15.208 0.028905492275953296 16.24 0.025895468890666965
		 17.276 0.021245943382382393 18.312 0.017736246809363365 19.344 0.018210180103778839
		 20.38 0.022713914513587952 21.412 0.029769457876682285 22.448 0.035987954586744308
		 23.484 0.036901060491800301 24.516 0.033888150006532669 25.552 0.033559594303369522
		 26.588 0.038770552724599838 27.62 0.044221088290214546 28.656 0.044432044029235847
		 29.688 0.043792061507701874 30.724 0.047938387840986259 31.76 0.053841769695281982
		 32.792 0.056441478431224823 33.828 0.054259117692708969 34.86 0.049514349550008781
		 35.896 0.048896010965108871 36.932 0.056547120213508613 37.964 0.066990122199058533
		 39 0.072342135012149811 40.036 0.07146589457988739 41.068 0.065934218466281891 42.104 0.057983361184597015
		 43.136 0.050579670816659927 44.172 0.045007016509771347 45.208 0.042090795934200287
		 46.24 0.042442690581083298 47.276 0.042945820838212967 48.312 0.040020767599344254
		 49.344 0.036472596228122711 50.38 0.039810691028833389 51.412 0.051561668515205383
		 52.448 0.060285106301307678 53.484 0.057206626981496811 54.516 0.049629542976617813
		 55.552 0.043790176510810845 56.588 0.038618393242359161 57.62 0.035644467920064926
		 58.656 0.037537824362516403 59.688 0.041788153350353241 60.724 0.046686988323926926
		 61.76 0.05383509024977684 62.792 0.059957623481750488 63.828 0.059609010815620429
		 64.86 0.049778200685977936 65.896 0.029106361791491508 66.932 0.0019675192888826132
		 67.964 -0.020914757624268532 69 -0.030478097498416897 70.036 -0.025982802733778954
		 71.068 -0.01073998026549816 72.104 0.0097547192126512527 73.136 0.031631831079721451
		 74.172 0.0508267842233181;
createNode animCurveTA -n "Bip01_L_Forearm_rotateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 72 ".ktv[0:71]"  0.724 -0.77553039789199829 1.76 -0.76863104104995728
		 2.792 -0.75608599185943604 3.828 -0.73375421762466431 4.86 -0.70557767152786255 5.896 -0.67879688739776611
		 6.932 -0.65526086091995239 7.964 -0.63446938991546631 9 -0.62096762657165527 10.036 -0.61406779289245605
		 11.068 -0.61422955989837646 12.104 -0.61984884738922119 13.136 -0.62805134057998657
		 14.172 -0.63223463296890259 15.208 -0.63126927614212036 16.24 -0.6273840069770813
		 17.276 -0.6223301887512207 18.312 -0.62703657150268555 19.344 -0.64727556705474854
		 20.38 -0.66668355464935314 21.412 -0.67155110836029053 22.448 -0.66596901416778564
		 23.484 -0.65775126218795776 24.516 -0.65494257211685181 25.552 -0.66286683082580578
		 26.588 -0.67865365743637085 27.62 -0.68982714414596558 28.656 -0.68634271621704102
		 29.688 -0.68145358562469482 30.724 -0.69140398502349854 31.76 -0.70814484357833862
		 32.792 -0.71534937620162964 33.828 -0.70398581027984619 34.86 -0.68061137199401867
		 35.896 -0.67619365453720093 36.932 -0.70862656831741333 37.964 -0.74844211339950562
		 39 -0.76268768310546875 40.036 -0.75037193298339844 41.068 -0.72324639558792114 42.104 -0.69589608907699585
		 43.136 -0.68352216482162476 44.172 -0.68701183795928955 45.208 -0.69551604986190796
		 46.24 -0.70269405841827393 47.276 -0.70460283756256104 48.312 -0.69894081354141235
		 49.344 -0.69524610042572021 50.38 -0.71528083086013794 51.412 -0.76160281896591187
		 52.448 -0.79391837120056163 53.484 -0.78319346904754639 54.516 -0.74837583303451538
		 55.552 -0.71191728115081798 56.588 -0.67775434255599976 57.62 -0.65354883670806896
		 58.656 -0.64484351873397827 59.688 -0.64027541875839233 60.724 -0.63980334997177124
		 61.76 -0.6533048152923584 62.792 -0.66655373573303223 63.828 -0.66708552837371826
		 64.86 -0.65573668479919434 65.896 -0.62135618925094604 66.932 -0.55198991298675537
		 67.964 -0.46413853764534002 69 -0.41324028372764582 70.036 -0.4361288845539093 71.068 -0.50473678112030029
		 72.104 -0.58236420154571533 73.136 -0.66413062810897827 74.172 -0.73474800586700428;
createNode animCurveTA -n "Bip01_L_Forearm_rotateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 72 ".ktv[0:71]"  0.724 -11.205378532409668 1.76 -11.007665634155272
		 2.792 -10.639509201049805 3.828 -9.9869537353515625 4.86 -9.1603250503540039 5.896 -8.3692378997802734
		 6.932 -7.6848263740539551 7.964 -7.1002602577209473 9 -6.7336912155151367 10.036 -6.5530166625976563
		 11.068 -6.5757942199707031 12.104 -6.7588467597961426 13.136 -7.0097627639770508
		 14.172 -7.1368589401245117 15.208 -7.1086039543151855 16.24 -6.9897770881652841 17.276 -6.8320050239562988
		 18.312 -6.9342503547668457 19.344 -7.460408687591551 20.38 -7.985999107360839 21.412 -8.1521492004394531
		 22.448 -8.0461044311523437 23.484 -7.8397855758666992 24.516 -7.747333526611329 25.552 -7.9465446472167987
		 26.588 -8.3837289810180664 27.62 -8.7051858901977539 28.656 -8.617833137512207 29.688 -8.4892196655273437
		 30.724 -8.7720613479614258 31.76 -9.2398271560668945 32.792 -9.4418792724609375 33.828 -9.13861083984375
		 34.86 -8.5128955841064453 35.896 -8.3966093063354492 36.932 -9.2740068435668945 37.964 -10.358283996582031
		 39 -10.759761810302734 40.036 -10.445158004760742 41.068 -9.7197513580322266 42.104 -8.9684038162231445
		 43.136 -8.597264289855957 44.172 -8.6428709030151367 45.208 -8.8370294570922852 46.24 -9.0212182998657227
		 47.276 -9.0730514526367205 48.312 -8.9099512100219727 49.344 -8.7945308685302734
		 50.38 -9.3292760848999023 51.412 -10.589694976806641 52.448 -11.473539352416992 53.484 -11.179362297058104
		 54.516 -10.239626884460447 55.552 -9.2680225372314453 56.588 -8.359837532043457 57.62 -7.7232565879821777
		 58.656 -7.5180325508117667 59.688 -7.437739372253418 60.724 -7.4679737091064444 61.76 -7.8715157508850098
		 62.792 -8.2599430084228516 63.828 -8.2708921432495117 64.86 -7.9014320373535165 65.896 -6.8693819046020517
		 66.932 -4.9157781600952148 67.964 -2.5269308090209961 69 -1.1626895666122437 70.036 -1.7755714654922483
		 71.068 -3.6251699924468999 72.104 -5.7369446754455566 73.136 -7.9656224250793457
		 74.172 -9.8952865600585938;
createNode animCurveTA -n "Bip01_L_Thigh_rotateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 72 ".ktv[0:71]"  0.724 -0.30804151296615601 1.76 -0.3129865825176239
		 2.792 -0.31795895099639893 3.828 -0.32000800967216492 4.86 -0.32213902473449707 5.896 -0.321483314037323
		 6.932 -0.32159259915351868 7.964 -0.31746718287467957 9 -0.31361493468284607 10.036 -0.32104620337486267
		 11.068 -0.32623714208602905 12.104 -0.33194717764854431 13.136 -0.33771184086799622
		 14.172 -0.33913254737854004 15.208 -0.33771184086799622 16.24 -0.34077176451683044
		 17.276 -0.34336724877357483 18.312 -0.33902326226234436 19.344 -0.34312134981155396
		 20.38 -0.35306611657142639 21.412 -0.36000558733940125 22.448 -0.36587956547737122
		 23.484 -0.37044212222099304 24.516 -0.36995035409927368 25.552 -0.37672588229179382
		 26.588 -0.38317358493804926 27.62 -0.36738219857215881 28.656 -0.33066311478614807
		 29.688 -0.26389116048812866 30.724 -0.18971532583236697 31.76 -0.14556498825550079
		 32.792 -0.14799652993679047 33.828 -0.17684724926948547 34.86 -0.20337569713592529
		 35.896 -0.23580543696880341 36.932 -0.24924725294113159 37.964 -0.28058415651321411
		 39 -0.31973481178283691 40.036 -0.35003352165222168 41.068 -0.36333870887756353 42.104 -0.36388513445854181
		 43.136 -0.36604347825050354 44.172 -0.37112513184547424 45.208 -0.37956726551055903
		 46.24 -0.39205282926559448 47.276 -0.40623229742050165 48.312 -0.42314386367797857
		 49.344 -0.45322400331497198 50.38 -0.48158293962478643 51.412 -0.50589841604232788
		 52.448 -0.5351862907409668 53.484 -0.57182341814041138 54.516 -0.59556514024734497
		 55.552 -0.58933603763580333 56.588 -0.56512981653213501 57.62 -0.54728937149047852
		 58.656 -0.53734463453292847 59.688 -0.54152470827102661 60.724 -0.58387184143066406
		 61.76 -0.59081131219863892 62.792 -0.59802401065826416 63.828 -0.60968995094299316
		 64.86 -0.61993527412414551 65.896 -0.62231218814849865 66.932 -0.61619234085083008
		 67.964 -0.60482686758041382 69 -0.5824238657951355 70.036 -0.55420154333114624 71.068 -0.52136194705963135
		 72.104 -0.48658263683319086 73.136 -0.4568303525447846 74.172 -0.43562942743301392;
createNode animCurveTA -n "Bip01_L_Thigh_rotateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 72 ".ktv[0:71]"  0.724 -178.89456176757813 1.76 -178.86921691894531
		 2.792 -178.8306884765625 3.828 -178.78982543945313 4.86 -178.76646423339847 5.896 -178.74528503417969
		 6.932 -178.73052978515628 7.964 -178.72795104980469 9 -178.73069763183594 10.036 -178.68391418457031
		 11.068 -178.57881164550781 12.104 -178.46099853515625 13.136 -178.3450927734375 14.172 -178.23158264160156
		 15.208 -178.13595581054687 16.24 -178.06607055664062 17.276 -178.00587463378906 18.312 -177.94544982910156
		 19.344 -177.87966918945312 20.38 -177.77549743652344 21.412 -177.64556884765625 22.448 -177.55805969238281
		 23.484 -177.49972534179687 24.516 -177.41166687011719 25.552 -177.32572937011719
		 26.588 -177.22282409667969 27.62 -177.00727844238281 28.656 -176.73141479492187 29.688 -176.54701232910156
		 30.724 -176.46153259277344 31.76 -176.41476440429687 32.792 -176.44720458984375 33.828 -176.63571166992187
		 34.86 -176.95643615722656 35.896 -177.29254150390625 36.932 -177.56800842285156 37.964 -177.84869384765625
		 39 -178.33255004882812 40.036 -179.08731079101562 41.068 -179.86322021484375 42.104 -180.38259887695312
		 43.136 -180.60952758789065 44.172 -180.63818359375 45.208 -180.59945678710935 46.24 -180.59066772460935
		 47.276 -180.59709167480469 48.312 -180.60273742675781 49.344 -180.55416870117187
		 50.38 -180.37994384765625 51.412 -180.14448547363281 52.448 -179.95220947265625 53.484 -179.68983459472656
		 54.516 -179.24739074707031 55.552 -178.81535339355469 56.588 -178.60179138183594
		 57.62 -178.64883422851562 58.656 -178.89144897460935 59.688 -179.13320922851562 60.724 -179.26255798339847
		 61.76 -179.3450927734375 62.792 -179.4056396484375 63.828 -179.42623901367187 64.86 -179.41880798339844
		 65.896 -179.42037963867187 66.932 -179.46096801757812 67.964 -179.52430725097656
		 69 -179.57542419433594 70.036 -179.66883850097659 71.068 -179.84979248046875 72.104 -180.09971618652344
		 73.136 -180.32731628417969 74.172 -180.416015625;
createNode animCurveTA -n "Bip01_L_Thigh_rotateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 72 ".ktv[0:71]"  0.724 2.0027751922607422 1.76 2.0542201995849614
		 2.792 2.0946276187896729 3.828 2.1332592964172363 4.86 2.2296741008758545 5.896 2.317086935043335
		 6.932 2.2452197074890137 7.964 2.1120991706848145 9 2.0743284225463872 10.036 2.1791033744812012
		 11.068 2.3471806049346924 12.104 2.4648511409759521 13.136 2.4894261360168457 14.172 2.4394838809967041
		 15.208 2.309382438659668 16.24 2.1789941787719727 17.276 2.1096267700195313 18.312 2.0292491912841801
		 19.344 2.0043461322784424 20.38 2.0699434280395508 21.412 2.1554436683654785 22.448 2.1867942810058594
		 23.484 2.0911579132080078 24.516 1.9893881082534788 25.552 2.0001387596130371 26.588 1.8896263837814329
		 27.62 1.2850043773651123 28.656 0.00056007551029324532 29.688 -1.8824546337127686
		 30.724 -3.8883171081542982 31.76 -5.0514163970947266 32.792 -5.1231060028076172 33.828 -4.7314357757568359
		 34.86 -4.3601465225219727 35.896 -4.0414772033691406 36.932 -3.7191743850707999 37.964 -3.3725833892822266
		 39 -3.0191347599029541 40.036 -2.7370753288269043 41.068 -2.4533219337463379 42.104 -2.1997306346893311
		 43.136 -2.0700662136077881 44.172 -2.0458600521087646 45.208 -2.0018191337585449
		 46.24 -1.9141194820404053 47.276 -1.8860883712768557 48.312 -1.9088737964630127 49.344 -1.915512800216675
		 50.38 -1.6535613536834719 51.412 -0.60288709402084351 52.448 0.97898459434509266
		 53.484 1.8566365242004397 54.516 1.2836793661117554 55.552 -0.40552195906639094 56.588 -2.3791732788085937
		 57.62 -3.7640078067779545 58.656 -4.1915774345397949 59.688 -4.0068073272705078 60.724 -3.7963829040527344
		 61.76 -3.9311287403106689 62.792 -3.6084434986114506 63.828 -2.5268146991729736 64.86 -0.8605218529701234
		 65.896 0.85337746143341053 66.932 2.4095540046691895 67.964 3.4393551349639893 69 3.763447761535645
		 70.036 3.5728306770324707 71.068 2.994067907333374 72.104 2.1133286952972412 73.136 1.1263390779495239
		 74.172 0.3706606924533844;
createNode animCurveTA -n "Bip01_L_Calf_rotateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 72 ".ktv[0:71]"  0.724 -0.059049181640148163 1.76 -0.059135306626558304
		 2.792 -0.05907424911856652 3.828 -0.059086240828037255 4.86 -0.060408357530832291
		 5.896 -0.062217041850090027 6.932 -0.059851918369531631 7.964 -0.055861786007881165
		 9 -0.054484371095895767 10.036 -0.057072095572948463 11.068 -0.061158005148172385
		 12.104 -0.063520483672618866 13.136 -0.063845813274383545 14.172 -0.062634304165840149
		 15.208 -0.05957476794719696 16.24 -0.056568905711174011 17.276 -0.054949793964624405
		 18.312 -0.053435929119586952 19.344 -0.05266546085476876 20.38 -0.054042894393205643
		 21.412 -0.056232165545225143 22.448 -0.056775864213705063 23.484 -0.054028008133172989
		 24.516 -0.050590291619300842 25.552 -0.050187792629003539 26.588 -0.048805560916662216
		 27.62 -0.03588486835360527 28.656 -0.00092421472072601308 29.688 0.056292876601219177
		 30.724 0.11559392511844638 31.76 0.14979110658168793 32.792 0.15514150261878967 33.828 0.15127022564411163
		 34.86 0.15009883046150208 35.896 0.14692573249340057 36.932 0.13672319054603577 37.964 0.12190917134284973
		 39 0.10851328074932098 40.036 0.098805055022239685 41.068 0.090005122125148773 42.104 0.081515461206436157
		 43.136 0.07543608546257019 44.172 0.07169051468372345 45.208 0.068725153803825378
		 46.24 0.06566716730594635 47.276 0.061567176133394241 48.312 0.056337866932153695
		 49.344 0.050567209720611565 50.38 0.038688875734806061 51.412 0.010608828626573086
		 52.448 -0.025662438943982128 53.484 -0.04054718092083931 54.516 -0.015785122290253639
		 55.552 0.039722759276628494 56.588 0.10265519469976424 57.62 0.14972493052482605
		 58.656 0.17041483521461487 59.688 0.17181238532066345 60.724 0.16886301338672638
		 61.76 0.16675892472267151 62.792 0.1543831080198288 63.828 0.12130631506443024 64.86 0.07476874440908432
		 65.896 0.027324840426445007 66.932 -0.014226232655346395 67.964 -0.040813345462083817
		 69 -0.050041578710079193 70.036 -0.050809901207685478 71.068 -0.047406531870365143
		 72.104 -0.038510501384735107 73.136 -0.02667788602411747 74.172 -0.017437854781746864;
createNode animCurveTA -n "Bip01_L_Calf_rotateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 72 ".ktv[0:71]"  0.724 -1.0539724826812744 1.76 -1.0539880990982056
		 2.792 -1.0539906024932859 3.828 -1.0540024042129517 4.86 -1.0540589094161987 5.896 -1.0541230440139773
		 6.932 -1.05401611328125 7.964 -1.0538061857223513 9 -1.0537205934524536 10.036 -1.0538352727890017
		 11.068 -1.054018497467041 12.104 -1.054113507270813 13.136 -1.0541651248931885 14.172 -1.0541228055953979
		 15.208 -1.0539877414703367 16.24 -1.0538449287414553 17.276 -1.0537214279174805 18.312 -1.0536394119262695
		 19.344 -1.0535780191421509 20.38 -1.0536453723907473 21.412 -1.053770899772644 22.448 -1.0537799596786501
		 23.484 -1.0536251068115234 24.516 -1.05344557762146 25.552 -1.0534175634384155 26.588 -1.0533071756362915
		 27.62 -1.0524380207061768 28.656 -1.049396276473999 29.688 -1.0417684316635132 30.724 -1.0304189920425415
		 31.76 -1.0221010446548462 32.792 -1.0206584930419922 33.828 -1.0216838121414189 34.86 -1.0220141410827637
		 35.896 -1.0228496789932251 36.932 -1.0254666805267334 37.964 -1.0291154384613037
		 39 -1.0321998596191408 40.036 -1.0343297719955444 41.068 -1.0360795259475708 42.104 -1.0376981496810913
		 43.136 -1.0387964248657229 44.172 -1.0394337177276611 45.208 -1.0399177074432373
		 46.24 -1.0403664112091064 47.276 -1.041068434715271 48.312 -1.0419237613677981 49.344 -1.0428328514099121
		 50.38 -1.0446193218231199 51.412 -1.0482134819030762 52.448 -1.0516757965087893 53.484 -1.0527175664901731
		 54.516 -1.0508352518081665 55.552 -1.0444436073303225 56.588 -1.0334559679031372
		 57.62 -1.0225311517715454 58.656 -1.017006516456604 59.688 -1.0166194438934326 60.724 -1.0173599720001221
		 61.76 -1.0182136297225952 62.792 -1.0214422941207886 63.828 -1.0295765399932859 64.86 -1.0390633344650269
		 65.896 -1.0465083122253418 66.932 -1.0511019229888916 67.964 -1.0531511306762695
		 69 -1.0537093877792358 70.036 -1.0537288188934326 71.068 -1.0535275936126709 72.104 -1.052952766418457
		 73.136 -1.0520921945571899 74.172 -1.0513032674789429;
createNode animCurveTA -n "Bip01_L_Calf_rotateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 72 ".ktv[0:71]"  0.724 -0.03177545964717865 1.76 -0.037120316177606583
		 2.792 -0.047167010605335236 3.828 -0.058614186942577362 4.86 0.034916713833808899
		 5.896 0.16668194532394409 6.932 0.010963811539113522 7.964 -0.2320951521396637 9 -0.30825784802436829
		 10.036 -0.12501426041126251 11.068 0.14615769684314728 12.104 0.30280992388725281
		 13.136 0.29669329524040228 14.172 0.21178577840328217 15.208 0.0086199687793850899
		 16.24 -0.18593066930770871 17.276 -0.26102530956268311 18.312 -0.35483258962631226
		 19.344 -0.38972592353820801 20.38 -0.29409438371658325 21.412 -0.16276304423809052
		 22.448 -0.11187229305505753 23.484 -0.28155091404914856 24.516 -0.50612306594848633
		 25.552 -0.525920569896698 26.588 -0.58889073133468628 27.62 -1.3894367218017578 28.656 -3.6484730243682857
		 29.688 -7.2820405960083008 30.724 -11.020696640014648 31.76 -13.082000732421877 32.792 -13.378330230712892
		 33.828 -13.151604652404783 34.86 -13.095165252685549 35.896 -12.91226291656494 36.932 -12.33132266998291
		 37.964 -11.520792007446287 39 -10.774679183959959 40.036 -10.239540100097656 41.068 -9.6793546676635742
		 42.104 -9.1382570266723633 43.136 -8.7385520935058594 44.172 -8.4790019989013672
		 45.208 -8.2522029876708984 46.24 -8.0043611526489276 47.276 -7.7402954101562491 48.312 -7.416633129119873
		 49.344 -7.0445823669433594 50.38 -6.3000664710998544 51.412 -4.4669280052185067 52.448 -2.0582811832427979
		 53.484 -1.056561350822449 54.516 -2.6915521621704106 55.552 -6.3408069610595703 56.588 -10.420173645019531
		 57.62 -13.418538093566896 58.656 -14.737845420837404 59.688 -14.833294868469237 60.724 -14.580868721008306
		 61.76 -14.669037818908691 62.792 -13.79849910736084 63.828 -11.743417739868164 64.86 -8.815765380859375
		 65.896 -5.8385438919067392 66.932 -3.1348340511322021 67.964 -1.3969405889511108
		 69 -0.79287946224212635 70.036 -0.72041022777557384 71.068 -0.93979781866073608 72.104 -1.5054951906204226
		 73.136 -2.275273323059082 74.172 -2.8596727848052979;
createNode animCurveTA -n "Bip01_L_Foot_rotateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 72 ".ktv[0:71]"  0.724 -7.845897674560546 1.76 -7.9286742210388175
		 2.792 -7.8803038597106942 3.828 -7.6837325096130371 4.86 -7.5365543365478525 5.896 -7.6067438125610352
		 6.932 -7.8374290466308594 7.964 -8.2284297943115234 9 -8.6220941543579102 10.036 -8.7095146179199219
		 11.068 -8.4349966049194336 12.104 -8.1041736602783203 13.136 -7.9022817611694354
		 14.172 -7.7632217407226563 15.208 -7.7455391883850098 16.24 -7.9193110466003427 17.276 -8.2140827178955078
		 18.312 -8.4337377548217773 19.344 -8.4502105712890625 20.38 -8.2158632278442383 21.412 -7.9465270042419442
		 22.448 -7.9342665672302246 23.484 -8.0524330139160156 24.516 -7.9024848937988308
		 25.552 -7.72308301925659 26.588 -7.7723989486694336 27.62 -7.8229823112487793 28.656 -7.4967970848083505
		 29.688 -6.9902205467224121 30.724 -6.875636100769043 31.76 -6.9951791763305673 32.792 -7.019406795501709
		 33.828 -6.940758228302002 34.86 -6.904484748840332 35.896 -6.9796023368835449 36.932 -7.0920863151550293
		 37.964 -7.1004080772399902 39 -7.2289905548095676 40.036 -7.7662191390991211 41.068 -8.5376405715942383
		 42.104 -9.1223087310791016 43.136 -9.4182596206665039 44.172 -9.6019992828369141
		 45.208 -9.9006795883178711 46.24 -10.132382392883301 47.276 -9.9493741989135742 48.312 -9.4842262268066406
		 49.344 -8.9304752349853516 50.38 -8.3552579879760742 51.412 -7.9786467552185085 52.448 -7.9577369689941388
		 53.484 -7.986750602722168 54.516 -7.8619313240051261 55.552 -7.508284091949462 56.588 -7.0343823432922363
		 57.62 -6.7977800369262695 58.656 -7.0108819007873535 59.688 -7.2652864456176758 60.724 -7.3952093124389666
		 61.76 -7.6289172172546378 62.792 -7.9877743721008301 63.828 -8.2043437957763672 64.86 -8.1000528335571289
		 65.896 -7.5828771591186532 66.932 -6.9270143508911133 67.964 -6.5704531669616708
		 69 -6.5323543548583984 70.036 -6.5854291915893555 71.068 -6.7209181785583496 72.104 -6.9819736480712891
		 73.136 -7.2001628875732422 74.172 -7.2542634010314941;
createNode animCurveTA -n "Bip01_L_Foot_rotateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 72 ".ktv[0:71]"  0.724 0.44552779197692871 1.76 0.47877243161201483
		 2.792 0.46062740683555609 3.828 0.36038017272949219 4.86 0.28484460711479187 5.896 0.31324389576911926
		 6.932 0.43925005197525024 7.964 0.6409146785736084 9 0.82334953546524048 10.036 0.85940825939178467
		 11.068 0.71902263164520264 12.104 0.56238371133804321 13.136 0.42645326256752014
		 14.172 0.39296695590019226 15.208 0.37811589241027832 16.24 0.47689267992973344 17.276 0.61751443147659313
		 18.312 0.71020233631134033 19.344 0.73795008659362804 20.38 0.62255889177322388 21.412 0.47307777404785156
		 22.448 0.4891501665115357 23.484 0.51830798387527466 24.516 0.43962350487709045 25.552 0.34978678822517395
		 26.588 0.40586990118026733 27.62 0.40896448493003851 28.656 0.24886529147624967 29.688 0.0089659038931131363
		 30.724 -0.039009906351566315 31.76 0.022853102535009384 32.792 0.036917421966791153
		 33.828 -0.0072818100452423087 34.86 -0.033950436860322952 35.896 0.016781507059931755
		 36.932 0.066515415906906128 37.964 0.058257121592760086 39 0.13815595209598541 40.036 0.4125216007232666
		 41.068 0.812544345855713 42.104 1.0916862487792969 43.136 1.2668405771255491 44.172 1.3699436187744141
		 45.208 1.5261260271072388 46.24 1.6425564289093018 47.276 1.541996955871582 48.312 1.2888975143432615
		 49.344 0.96734076738357544 50.38 0.70057862997055054 51.412 0.52896732091903687 52.448 0.48218074440956105
		 53.484 0.50144040584564209 54.516 0.42760050296783447 55.552 0.26503884792327881
		 56.588 0.036385711282491677 57.62 -0.099461100995540619 58.656 0.002997242147102952
		 59.688 0.15121927857398987 60.724 0.20495301485061648 61.76 0.34210881590843201 62.792 0.52184170484542847
		 63.828 0.63839066028594971 64.86 0.58789849281311035 65.896 0.33561882376670837 66.932 -0.022164780646562576
		 67.964 -0.2024536728858948 69 -0.22833587229251867 70.036 -0.19659622013568881 71.068 -0.12694992125034332
		 72.104 0.0077298101969063273 73.136 0.12388356029987338 74.172 0.15591783821582794;
createNode animCurveTA -n "Bip01_L_Foot_rotateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 72 ".ktv[0:71]"  0.724 0.12912029027938843 1.76 0.16281270980834961
		 2.792 0.22704641520977023 3.828 0.22919207811355591 4.86 0.080049619078636169 5.896 -0.020786460489034653
		 6.932 0.079000517725944505 7.964 0.20017942786216736 9 0.16061648726463318 10.036 -0.013404745608568192
		 11.068 -0.24507738649845123 12.104 -0.39235061407089233 13.136 -0.37677234411239624
		 14.172 -0.30062201619148254 15.208 -0.22510650753974912 16.24 -0.1550573855638504
		 17.276 -0.13488167524337769 18.312 -0.22449032962322235 19.344 -0.4015619158744812
		 20.38 -0.56175881624221802 21.412 -0.72895801067352284 22.448 -0.93513083457946788
		 23.484 -1.0517719984054563 24.516 -1.0852758884429932 25.552 -1.1932241916656494
		 26.588 -1.2983882427215576 27.62 -1.1704741716384888 28.656 -0.62856197357177734
		 29.688 0.21737679839134216 30.724 0.91495883464813232 31.76 1.073413610458374 32.792 0.84013885259628285
		 33.828 0.54146957397460938 34.86 0.36551779508590698 35.896 0.30255219340324402 36.932 0.56884944438934326
		 37.964 1.274601936340332 39 2.1382861137390137 40.036 2.7207217216491699 41.068 2.8174917697906494
		 42.104 2.6718602180480957 43.136 2.558638334274292 44.172 2.4436864852905273 45.208 2.2667737007141113
		 46.24 2.0202722549438477 47.276 1.705070972442627 48.312 1.3052495718002322 49.344 0.8954930305480957
		 50.38 0.53446507453918457 51.412 -0.086136914789676666 52.448 -1.0365761518478394
		 53.484 -1.5488377809524536 54.516 -1.0634634494781494 55.552 0.15575744211673737
		 56.588 1.5682604312896729 57.62 2.5759155750274658 58.656 3.0306682586669926 59.688 3.2050340175628662
		 60.724 3.3672688007354736 61.76 3.8419005870819087 62.792 3.9254293441772461 63.828 3.8092048168182382
		 64.86 3.47314453125 65.896 2.9263772964477539 66.932 2.0874078273773193 67.964 1.5291413068771362
		 69 1.3589224815368652 70.036 1.2680644989013672 71.068 1.2901211977005005 72.104 1.4858702421188354
		 73.136 1.5860261917114258 74.172 1.4666955471038818;
createNode animCurveTA -n "Bip01_R_Thigh_rotateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 72 ".ktv[0:71]"  0.724 0.20886717736721039 1.76 0.18755698204040527
		 2.792 0.20312981307506561 3.828 0.20407238602638245 4.86 0.20360793173313141 5.896 0.20573894679546356
		 6.932 0.20852565765380859 7.964 0.21793766319751745 9 0.22509570419788363 10.036 0.23904295265674588
		 11.068 0.25121435523033142 12.104 0.26483374834060669 13.136 0.28408122062683105
		 14.172 0.3192976713180542 15.208 0.36205464601516718 16.24 0.40460672974586487 17.276 0.45176234841346746
		 18.312 0.5129198431968689 19.344 0.57955515384674072 20.38 0.65065741539001465 21.412 0.74402612447738636
		 22.448 0.83458077907562256 23.484 0.92808604240417469 24.516 1.0266729593276978 25.552 1.1288115978240969
		 26.588 1.2328217029571531 27.62 1.340984582901001 28.656 1.4461148977279663 29.688 1.5401802062988279
		 30.724 1.6242325305938721 31.76 1.6961954832077026 32.792 1.7528450489044187 33.828 1.7922005653381348
		 34.86 1.8143303394317625 35.896 1.8186880350112915 36.932 1.8088935613632204 37.964 1.7844277620315554
		 39 1.7377092838287354 40.036 1.6598588228225708 41.068 1.5466142892837524 42.104 1.4289711713790894
		 43.136 1.3323239088058472 44.172 1.2576563358306885 45.208 1.1882889270782473 46.24 1.1193723678588867
		 47.276 1.056261420249939 48.312 0.99795889854431152 49.344 0.95503795146942128 50.38 0.91565513610839844
		 51.412 0.8785671591758728 52.448 0.83198529481887828 53.484 0.7907719612121582 54.516 0.75353372097015381
		 55.552 0.70512139797210693 56.588 0.65827995538711548 57.62 0.60717648267745972 58.656 0.55607300996780396
		 59.688 0.5114172101020813 60.724 0.48314023017883306 61.76 0.47087320685386652 62.792 0.45248633623123169
		 63.828 0.42818453907966614 64.86 0.4157126247882843 65.896 0.41194233298301697 66.932 0.43509668111801142
		 67.964 0.47043609619140625 69 0.51286524534225464 70.036 0.57119500637054443 71.068 0.64819860458374023
		 72.104 0.74526923894882213 73.136 0.85601389408111583 74.172 0.98186695575714111;
createNode animCurveTA -n "Bip01_R_Thigh_rotateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 72 ".ktv[0:71]"  0.724 178.21955871582031 1.76 178.25054931640625
		 2.792 178.29176330566406 3.828 178.31323242187503 4.86 178.32131958007812 5.896 178.34986877441406
		 6.932 178.40744018554687 7.964 178.46380615234378 9 178.51608276367187 10.036 178.56939697265625
		 11.068 178.63493347167969 12.104 178.72364807128906 13.136 178.84071350097656 14.172 178.95916748046878
		 15.208 179.05195617675781 16.24 179.13316345214844 17.276 179.24089050292969 18.312 179.36698913574219
		 19.344 179.47196960449219 20.38 179.56315612792969 21.412 179.65927124023435 22.448 179.74417114257812
		 23.484 179.82113647460935 24.516 179.91976928710935 25.552 180.04522705078125 26.588 180.2481689453125
		 27.62 180.59588623046875 28.656 180.97605895996097 29.688 181.24555969238281 30.724 181.45570373535156
		 31.76 181.66368103027344 32.792 181.77754211425781 33.828 181.714599609375 34.86 181.5218811035156
		 35.896 181.32456970214844 36.932 181.1890869140625 37.964 181.03291320800781 39 180.68881225585935
		 40.036 180.09535217285156 41.068 179.47677612304687 42.104 179.11882019042969 43.136 179.08914184570312
		 44.172 179.2823486328125 45.208 179.53903198242187 46.24 179.73783874511719 47.276 179.89895629882812
		 48.312 180.09083557128906 49.344 180.36068725585937 50.38 180.71708679199219 51.412 181.06735229492187
		 52.448 181.34059143066409 53.484 181.6979675292969 54.516 182.301025390625 55.552 182.92044067382812
		 56.588 183.29954528808591 57.62 183.44639587402344 58.656 183.42732238769537 59.688 183.35945129394531
		 60.724 183.35589599609375 61.76 183.40220642089844 62.792 183.468017578125 63.828 183.54951477050781
		 64.86 183.62673950195312 65.896 183.64071655273435 66.932 183.55746459960935 67.964 183.48097229003903
		 69 183.45219421386719 70.036 183.39015197753903 71.068 183.24774169921875 72.104 183.04298400878906
		 73.136 182.83642578125003 74.172 182.72978210449219;
createNode animCurveTA -n "Bip01_R_Thigh_rotateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 72 ".ktv[0:71]"  0.724 -1.0449095964431765 1.76 -0.94849467277526844
		 2.792 -0.89434492588043213 3.828 -0.8605218529701234 4.86 -0.79410505294799805 5.896 -0.82301044464111328
		 6.932 -0.9136607050895692 7.964 -0.91876965761184703 9 -0.75916182994842529 10.036 -0.62091881036758423
		 11.068 -0.53346508741378784 12.104 -0.41246142983436584 13.136 -0.3232865035533905
		 14.172 -0.40869116783142095 15.208 -0.62151986360549927 16.24 -0.86890929937362671
		 17.276 -1.163071870803833 18.312 -1.3297830820083618 19.344 -1.2961785793304443 20.38 -1.3054130077362061
		 21.412 -1.3973746299743652 22.448 -1.4155702590942385 23.484 -1.4649661779403689
		 24.516 -1.6441357135772705 25.552 -1.8213381767272947 26.588 -2.0672249794006348
		 27.62 -2.4641408920288086 28.656 -2.7600793838500977 29.688 -2.7073228359222412 30.724 -2.5146842002868652
		 31.76 -2.4494423866271973 32.792 -2.397205114364624 33.828 -2.1907148361206055 34.86 -1.9026993513107295
		 35.896 -1.7847282886505127 36.932 -2.0368716716766357 37.964 -2.6502499580383301
		 39 -3.6454355716705322 40.036 -5.136110782623291 41.068 -6.9002118110656738 42.104 -8.5049238204956055
		 43.136 -9.6951255798339844 44.172 -10.586710929870604 45.208 -11.22598934173584 46.24 -11.57411003112793
		 47.276 -11.477640151977541 48.312 -10.947727203369141 49.344 -10.010570526123049
		 50.38 -8.7820110321044922 51.412 -7.4474468231201181 52.448 -6.1807198524475098 53.484 -4.986447811126709
		 54.516 -3.7529702186584482 55.552 -2.5796256065368652 56.588 -1.7408511638641355
		 57.62 -1.1346036195755005 58.656 -0.46196666359901434 59.688 0.19795253872871399
		 60.724 0.55779421329498291 61.76 0.67644822597503662 62.792 0.85269445180892944 63.828 1.1905019283294678
		 64.86 1.5368744134902954 65.896 1.8950905799865725 66.932 2.1461410522460942 67.964 2.2037467956542969
		 69 2.1562223434448242 70.036 1.986519455909729 71.068 1.5374071598052981 72.104 0.73248308897018433
		 73.136 -0.14389841258525848 74.172 -0.88429087400436401;
createNode animCurveTA -n "Bip01_R_Calf_rotateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 72 ".ktv[0:71]"  0.724 -0.014268065802752972 1.76 -0.012970953248441221
		 2.792 -0.013185832649469376 3.828 -0.013736927881836893 4.86 -0.013277976773679256
		 5.896 -0.015079619362950328 6.932 -0.018169116228818893 7.964 -0.018501244485378265
		 9 -0.01513240020722151 10.036 -0.011987493373453615 11.068 -0.010884297080338 12.104 -0.0088276127353310585
		 13.136 -0.0072258752770721921 14.172 -0.0099717304110527039 15.208 -0.016096489503979683
		 16.24 -0.02349899522960186 17.276 -0.03197663277387619 18.312 -0.037346344441175461
		 19.344 -0.037736851722002029 20.38 -0.039381347596645355 21.412 -0.043463926762342453
		 22.448 -0.045956816524267197 23.484 -0.049617774784564986 24.516 -0.05661676824092865
		 25.552 -0.063422933220863342 26.588 -0.069187887012958527 27.62 -0.076564259827136993
		 28.656 -0.08511941134929657 29.688 -0.089873619377613068 30.724 -0.089837871491909027
		 31.76 -0.090159960091114044 32.792 -0.092799611389636993 33.828 -0.096253283321857452
		 34.86 -0.099585518240928636 35.896 -0.10295633226633072 36.932 -0.10795992612838744
		 37.964 -0.11793632060289382 39 -0.14048479497432709 40.036 -0.18043522536754608 41.068 -0.22880686819553372
		 42.104 -0.27045521140098577 43.136 -0.29897817969322205 44.172 -0.31744125485420227
		 45.208 -0.32967546582221985 46.24 -0.33492276072502136 47.276 -0.32915148138999939
		 48.312 -0.31096175312995911 49.344 -0.28221473097801208 50.38 -0.24603797495365143
		 51.412 -0.20974819362163544 52.448 -0.17950604856014252 53.484 -0.15333922207355499
		 54.516 -0.12831105291843414 55.552 -0.107529379427433 56.588 -0.095951288938522339
		 57.62 -0.091116681694984436 58.656 -0.084732852876186371 59.688 -0.075400404632091522
		 60.724 -0.068386442959308624 61.76 -0.062575511634349823 62.792 -0.052638750523328781
		 63.828 -0.040713142603635802 64.86 -0.031355220824480057 65.896 -0.02364657074213028
		 66.932 -0.019415942952036858 67.964 -0.021482206881046295 69 -0.024073069915175441
		 70.036 -0.023438775911927223 71.068 -0.025165719911456108 72.104 -0.033304113894701004
		 73.136 -0.044154468923807144 74.172 -0.054635412991046906;
createNode animCurveTA -n "Bip01_R_Calf_rotateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 72 ".ktv[0:71]"  0.724 1.0476862192153933 1.76 1.0478463172912598
		 2.792 1.0477974414825439 3.828 1.0477626323699951 4.86 1.047817587852478 5.896 1.047624945640564
		 6.932 1.0472376346588137 7.964 1.0471948385238647 9 1.0475802421569824 10.036 1.0479799509048462
		 11.068 1.0481112003326416 12.104 1.0483251810073853 13.136 1.0484867095947266 14.172 1.0481619834899902
		 15.208 1.0474758148193359 16.24 1.0465668439865112 17.276 1.045473575592041 18.312 1.044711709022522
		 19.344 1.0446335077285769 20.38 1.0444148778915403 21.412 1.0438506603240969 22.448 1.0435140132904053
		 23.484 1.0429778099060061 24.516 1.041922926902771 25.552 1.0408116579055788 26.588 1.0398451089859009
		 27.62 1.0385456085205078 28.656 1.0369552373886108 29.688 1.0360488891601565 30.724 1.0360621213912964
		 31.76 1.0360076427459717 32.792 1.0354883670806885 33.828 1.0348262786865234 34.86 1.0341851711273191
		 35.896 1.0335140228271484 36.932 1.0324616432189939 37.964 1.030309796333313 39 1.0250015258789062
		 40.036 1.0141912698745728 41.068 0.99866199493408214 42.104 0.98314559459686268 43.136 0.97125166654586803
		 44.172 0.96298128366470337 45.208 0.95723241567611717 46.24 0.95477777719497692 47.276 0.95746707916259766
		 48.312 0.96581482887268078 49.344 0.97816061973571777 50.38 0.99226462841033936 51.412 1.0048941373825071
		 52.448 1.0143258571624756 53.484 1.0216468572616575 54.516 1.027901291847229 55.552 1.0325496196746826
		 56.588 1.0349774360656738 57.62 1.0359706878662107 58.656 1.0372284650802612 59.688 1.0389659404754641
		 60.724 1.0402125120162964 61.76 1.0411785840988159 62.792 1.0427374839782717 63.828 1.044468879699707
		 64.86 1.0457682609558103 65.896 1.0467510223388672 66.932 1.0472908020019531 67.964 1.0470317602157593
		 69 1.046695351600647 70.036 1.0468015670776367 71.068 1.0465618371963501 72.104 1.0455495119094849
		 73.136 1.0440288782119751 74.172 1.0424624681472778;
createNode animCurveTA -n "Bip01_R_Calf_rotateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 72 ".ktv[0:71]"  0.724 -4.6648855209350586 1.76 -4.5857620239257804
		 2.792 -4.5798730850219727 3.828 -4.643101692199707 4.86 -4.6142234802246094 5.896 -4.7503647804260254
		 6.932 -4.9347267150878906 7.964 -4.9532032012939453 9 -4.7220191955566406 10.036 -4.5413661003112793
		 11.068 -4.4682183265686035 12.104 -4.3213343620300293 13.136 -4.2027792930603027
		 14.172 -4.3725838661193848 15.208 -4.7863936424255371 16.24 -5.275634765625 17.276 -5.8407101631164551
		 18.312 -6.1721186637878418 19.344 -6.1761298179626465 20.38 -6.2965874671936044 21.412 -6.5877971649169922
		 22.448 -6.7729759216308594 23.484 -7.0232038497924805 24.516 -7.5118203163146973
		 25.552 -7.9569916725158691 26.588 -8.3337764739990234 27.62 -8.810671806335451 28.656 -9.3583879470825195
		 29.688 -9.6617698669433612 30.724 -9.6638603210449219 31.76 -9.6875209808349609 32.792 -9.8612556457519549
		 33.828 -10.099216461181641 34.86 -10.341559410095217 35.896 -10.572636604309082 36.932 -10.893047332763672
		 37.964 -11.546534538269045 39 -12.973881721496582 40.036 -15.432125091552734 41.068 -18.321226119995117
		 42.104 -20.816717147827148 43.136 -22.475893020629883 44.172 -23.526130676269531
		 45.208 -24.20347785949707 46.24 -24.541067123413089 47.276 -24.16758918762207 48.312 -23.068763732910156
		 49.344 -21.342992782592773 50.38 -19.178609848022461 51.412 -17.026098251342773 52.448 -15.261094093322752
		 53.484 -13.71530055999756 54.516 -12.177474975585938 55.552 -10.861177444458008 56.588 -10.153669357299805
		 57.62 -9.8722066879272461 58.656 -9.4885149002075195 59.688 -8.9095516204833984 60.724 -8.4701395034790039
		 61.76 -8.0864391326904297 62.792 -7.4119057655334464 63.828 -6.5892653465270996 64.86 -5.9804368019104004
		 65.896 -5.4565577507019043 66.932 -5.1915202140808105 67.964 -5.3222246170043945
		 69 -5.482292652130127 70.036 -5.4571075439453125 71.068 -5.5585083961486816 72.104 -6.1420636177062988
		 73.136 -6.8630542755126953 74.172 -7.565385341644288;
createNode animCurveTA -n "Bip01_R_Foot_rotateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 72 ".ktv[0:71]"  0.724 8.1963729858398438 1.76 8.1098489761352539
		 2.792 8.0897026062011719 3.828 8.0774002075195313 4.86 7.9757003784179696 5.896 7.9379773139953622
		 6.932 8.0455303192138672 7.964 8.0635547637939453 9 8.0884885787963867 10.036 8.0058755874633789
		 11.068 7.8809466361999521 12.104 7.8393774032592782 13.136 7.9584360122680655 14.172 8.0752344131469727
		 15.208 8.0488653182983398 16.24 7.9784793853759757 17.276 8.0438604354858398 18.312 8.3068714141845703
		 19.344 8.4793624877929687 20.38 8.3923816680908203 21.412 8.0722522735595703 22.448 7.7397394180297852
		 23.484 7.4622406959533691 24.516 7.3353104591369638 25.552 7.3511409759521484 26.588 7.4917702674865732
		 27.62 7.7896585464477539 28.656 8.1766300201416016 29.688 8.5283613204956055 30.724 8.8419761657714862
		 31.76 9.0749254226684553 32.792 9.0966081619262695 33.828 8.9650449752807617 34.86 8.7542123794555664
		 35.896 8.5342922210693359 36.932 8.279444694519043 37.964 8.0260238647460937 39 7.7828116416931152
		 40.036 7.3809266090393058 41.068 6.7179679870605469 42.104 6.1088557243347168 43.136 5.993779182434082
		 44.172 6.2355923652648926 45.208 6.4080357551574707 46.24 6.2511544227600098 47.276 6.1468596458435059
		 48.312 6.442145824432373 49.344 7.1515069007873535 50.38 7.6651315689086905 51.412 7.5642590522766104
		 52.448 6.9672298431396484 53.484 6.4750556945800781 54.516 6.6868395805358887 55.552 7.3415727615356445
		 56.588 8.0040092468261719 57.62 8.6767282485961914 58.656 9.2366771697998047 59.688 9.4076004028320312
		 60.724 9.3227500915527344 61.76 9.1234474182128906 62.792 8.9203262329101562 63.828 8.7115621566772461
		 64.86 8.5876073837280273 65.896 8.5302276611328125 66.932 8.3628120422363281 67.964 8.2205495834350586
		 69 8.1968460083007812 70.036 8.1628141403198242 71.068 8.1143465042114258 72.104 8.0682878494262713
		 73.136 7.9411993026733398 74.172 7.8402976989746076;
createNode animCurveTA -n "Bip01_R_Foot_rotateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 72 ".ktv[0:71]"  0.724 -0.62299525737762451 1.76 -0.57023686170578003
		 2.792 -0.58489120006561279 3.828 -0.55616968870162964 4.86 -0.50706607103347778 5.896 -0.50115662813186646
		 6.932 -0.53286153078079224 7.964 -0.5634744167327882 9 -0.56119155883789063 10.036 -0.51352131366729736
		 11.068 -0.44750213623046869 12.104 -0.43202021718025208 13.136 -0.48997896909713745
		 14.172 -0.54201078414916992 15.208 -0.53249353170394897 16.24 -0.50542742013931274
		 17.276 -0.54084885120391846 18.312 -0.66679811477661133 19.344 -0.749292552471161
		 20.38 -0.71765011548995972 21.412 -0.55819189548492432 22.448 -0.38368967175483704
		 23.484 -0.25122594833374023 24.516 -0.17986612021923065 25.552 -0.18904459476470947
		 26.588 -0.25443831086158752 27.62 -0.40885120630264282 28.656 -0.60381621122360229
		 29.688 -0.7731858491897583 30.724 -0.93197757005691517 31.76 -1.0481363534927368
		 32.792 -1.0728496313095093 33.828 -0.99743330478668202 34.86 -0.90023189783096302
		 35.896 -0.79274392127990723 36.932 -0.66138148307800293 37.964 -0.49743503332138056
		 39 -0.38544958829879761 40.036 -0.20012874901294708 41.068 0.14843578636646271 42.104 0.45318496227264393
		 43.136 0.51628732681274414 44.172 0.39154893159866339 45.208 0.303009033203125 46.24 0.37423276901245123
		 47.276 0.4449029266834259 48.312 0.29674169421195984 49.344 -0.051141813397407539
		 50.38 -0.3151034414768219 51.412 -0.2510758638381958 52.448 0.043343588709831238
		 53.484 0.27034658193588257 54.516 0.16282278299331665 55.552 -0.18673360347747803
		 56.588 -0.51828855276107788 57.62 -0.86369216442108165 58.656 -1.1760005950927734
		 59.688 -1.2877836227416992 60.724 -1.2338058948516846 61.76 -1.1253458261489868 62.792 -1.0238281488418579
		 63.828 -0.88838648796081532 64.86 -0.84033405780792236 65.896 -0.80204230546951316
		 66.932 -0.70568031072616577 67.964 -0.63306194543838501 69 -0.62284606695175171 70.036 -0.59386962652206421
		 71.068 -0.57473921775817871 72.104 -0.55656647682189941 73.136 -0.50767093896865845
		 74.172 -0.43755689263343805;
createNode animCurveTA -n "Bip01_R_Foot_rotateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 72 ".ktv[0:71]"  0.724 0.27279940247535706 1.76 0.18041408061981201
		 2.792 0.20336392521858215 3.828 0.30269905924797058 4.86 0.37202611565589905 5.896 0.44768232107162487
		 6.932 0.4587644636631012 7.964 0.31984394788742065 9 0.17194963991641998 10.036 0.16109421849250791
		 11.068 0.12248189002275466 12.104 -0.099630989134311676 13.136 -0.33363589644432068
		 14.172 -0.31619656085968018 15.208 -0.076014131307601943 16.24 0.15916746854782104
		 17.276 0.32295528054237366 18.312 0.34300997853279114 19.344 0.273964524269104 20.38 0.25630423426628113
		 21.412 0.28715783357620239 22.448 0.30519822239875793 23.484 0.35474848747253418
		 24.516 0.41847175359725952 25.552 0.45093125104904175 26.588 0.51190286874771118
		 27.62 0.59652471542358398 28.656 0.68420159816741943 29.688 0.81837207078933716 30.724 0.94159543514251709
		 31.76 0.95328712463378928 32.792 0.92762190103530873 33.828 1.0758446455001831 34.86 1.3760995864868164
		 35.896 1.5257816314697266 36.932 1.5333349704742432 37.964 1.706206202507019 39 2.0902318954467773
		 40.036 2.3238143920898437 41.068 1.9947046041488647 42.104 1.3148670196533203 43.136 0.48663991689682023
		 44.172 -0.39369761943817139 45.208 -1.1139363050460815 46.24 -1.5184588432312012
		 47.276 -1.6542580127716064 48.312 -1.406982421875 49.344 -0.86430394649505637 50.38 -0.41447010636329645
		 51.412 -0.23288105428218842 52.448 -0.13163958489894867 53.484 -0.062424398958683014
		 54.516 -0.060177490115165704 55.552 0.10567414015531541 56.588 0.73672014474868763
		 57.62 1.6698557138442991 58.656 2.4525091648101807 59.688 2.8305695056915283 60.724 2.8777008056640625
		 61.76 2.6514813899993896 62.792 2.0340933799743652 63.828 1.3193538188934326 64.86 1.0110113620758057
		 65.896 0.96243900060653687 66.932 0.92462313175201405 67.964 0.99431091547012351
		 69 1.0097230672836304 70.036 0.81986343860626221 71.068 0.80187821388244629 72.104 1.12738037109375
		 73.136 1.3582743406295776 74.172 1.3198254108428955;
select -ne :time1;
	setAttr ".o" 24.2;
	setAttr ".unw" 24.2;
select -ne :renderPartition;
	setAttr -s 126 ".st";
select -ne :initialShadingGroup;
	setAttr ".ro" yes;
select -ne :initialParticleSE;
	setAttr ".ro" yes;
select -ne :defaultShaderList1;
	setAttr -s 64 ".s";
select -ne :defaultTextureList1;
	setAttr -s 31 ".tx";
select -ne :postProcessList1;
	setAttr -s 2 ".p";
select -ne :defaultRenderUtilityList1;
	setAttr -s 31 ".u";
select -ne :defaultRenderingList1;
select -ne :renderGlobalsList1;
select -ne :defaultResolution;
	setAttr ".pa" 1;
select -ne :hardwareRenderGlobals;
	setAttr ".ctrs" 256;
	setAttr ".btrs" 512;
select -ne :hardwareRenderingGlobals;
	setAttr ".otfna" -type "stringArray" 18 "NURBS Curves" "NURBS Surfaces" "Polygons" "Subdiv Surfaces" "Particles" "Fluids" "Image Planes" "UI:" "Lights" "Cameras" "Locators" "Joints" "IK Handles" "Deformers" "Motion Trails" "Components" "Misc. UI" "Ornaments"  ;
	setAttr ".otfva" -type "Int32Array" 18 0 1 1 1 1 1
		 1 0 0 0 0 0 0 0 0 0 0 0 ;
select -ne :defaultHardwareRenderGlobals;
	setAttr ".fn" -type "string" "im";
	setAttr ".res" -type "string" "ntsc_4d 646 485 1.333";
select -ne :hyperGraphLayout;
	setAttr -s 4 ".hyp";
	setAttr ".hyp[1].isc" yes;
	setAttr ".hyp[2].isc" yes;
	setAttr ".hyp[3].isc" yes;
	setAttr ".hyp[4].isc" yes;
connectAttr "Bip01_Spine_translateX.o" "|Stand_Idle_2|Bip01_Spine.tx";
connectAttr "Bip01_Spine_translateY.o" "|Stand_Idle_2|Bip01_Spine.ty";
connectAttr "Bip01_Spine_translateZ.o" "|Stand_Idle_2|Bip01_Spine.tz";
connectAttr "Bip01_Spine_rotateX.o" "|Stand_Idle_2|Bip01_Spine.rx";
connectAttr "Bip01_Spine_rotateY.o" "|Stand_Idle_2|Bip01_Spine.ry";
connectAttr "Bip01_Spine_rotateZ.o" "|Stand_Idle_2|Bip01_Spine.rz";
connectAttr "|Stand_Idle_2|Bip01_Spine.s" "|Stand_Idle_2|Bip01_Spine|Bip01_Spine1.is"
		;
connectAttr "Bip01_Spine1_rotateX.o" "|Stand_Idle_2|Bip01_Spine|Bip01_Spine1.rx"
		;
connectAttr "Bip01_Spine1_rotateY.o" "|Stand_Idle_2|Bip01_Spine|Bip01_Spine1.ry"
		;
connectAttr "Bip01_Spine1_rotateZ.o" "|Stand_Idle_2|Bip01_Spine|Bip01_Spine1.rz"
		;
connectAttr "|Stand_Idle_2|Bip01_Spine|Bip01_Spine1.s" "|Stand_Idle_2|Bip01_Spine|Bip01_Spine1|Bip01_Spine2.is"
		;
connectAttr "Bip01_Spine2_rotateX.o" "|Stand_Idle_2|Bip01_Spine|Bip01_Spine1|Bip01_Spine2.rx"
		;
connectAttr "Bip01_Spine2_rotateY.o" "|Stand_Idle_2|Bip01_Spine|Bip01_Spine1|Bip01_Spine2.ry"
		;
connectAttr "Bip01_Spine2_rotateZ.o" "|Stand_Idle_2|Bip01_Spine|Bip01_Spine1|Bip01_Spine2.rz"
		;
connectAttr "|Stand_Idle_2|Bip01_Spine|Bip01_Spine1|Bip01_Spine2.s" "|Stand_Idle_2|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck.is"
		;
connectAttr "Bip01_Neck_rotateX.o" "|Stand_Idle_2|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck.rx"
		;
connectAttr "Bip01_Neck_rotateY.o" "|Stand_Idle_2|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck.ry"
		;
connectAttr "Bip01_Neck_rotateZ.o" "|Stand_Idle_2|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck.rz"
		;
connectAttr "|Stand_Idle_2|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck.s" "|Stand_Idle_2|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_Head.is"
		;
connectAttr "Bip01_Head_rotateX.o" "|Stand_Idle_2|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_Head.rx"
		;
connectAttr "Bip01_Head_rotateY.o" "|Stand_Idle_2|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_Head.ry"
		;
connectAttr "Bip01_Head_rotateZ.o" "|Stand_Idle_2|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_Head.rz"
		;
connectAttr "|Stand_Idle_2|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_Head.s" "|Stand_Idle_2|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_Head|_notused_Bip01_HeadNub.is"
		;
connectAttr "|Stand_Idle_2|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck.s" "|Stand_Idle_2|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_R_Clavicle.is"
		;
connectAttr "Bip01_R_Clavicle_rotateX.o" "|Stand_Idle_2|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_R_Clavicle.rx"
		;
connectAttr "Bip01_R_Clavicle_rotateY.o" "|Stand_Idle_2|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_R_Clavicle.ry"
		;
connectAttr "Bip01_R_Clavicle_rotateZ.o" "|Stand_Idle_2|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_R_Clavicle.rz"
		;
connectAttr "|Stand_Idle_2|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_R_Clavicle.s" "|Stand_Idle_2|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_R_Clavicle|Bip01_R_UpperArm.is"
		;
connectAttr "Bip01_R_UpperArm_rotateX.o" "|Stand_Idle_2|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_R_Clavicle|Bip01_R_UpperArm.rx"
		;
connectAttr "Bip01_R_UpperArm_rotateY.o" "|Stand_Idle_2|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_R_Clavicle|Bip01_R_UpperArm.ry"
		;
connectAttr "Bip01_R_UpperArm_rotateZ.o" "|Stand_Idle_2|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_R_Clavicle|Bip01_R_UpperArm.rz"
		;
connectAttr "|Stand_Idle_2|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_R_Clavicle|Bip01_R_UpperArm.s" "|Stand_Idle_2|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_R_Clavicle|Bip01_R_UpperArm|Bip01_R_Forearm.is"
		;
connectAttr "Bip01_R_Forearm_rotateX.o" "|Stand_Idle_2|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_R_Clavicle|Bip01_R_UpperArm|Bip01_R_Forearm.rx"
		;
connectAttr "Bip01_R_Forearm_rotateY.o" "|Stand_Idle_2|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_R_Clavicle|Bip01_R_UpperArm|Bip01_R_Forearm.ry"
		;
connectAttr "Bip01_R_Forearm_rotateZ.o" "|Stand_Idle_2|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_R_Clavicle|Bip01_R_UpperArm|Bip01_R_Forearm.rz"
		;
connectAttr "|Stand_Idle_2|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_R_Clavicle|Bip01_R_UpperArm|Bip01_R_Forearm.s" "|Stand_Idle_2|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_R_Clavicle|Bip01_R_UpperArm|Bip01_R_Forearm|Bip01_R_Hand.is"
		;
connectAttr "|Stand_Idle_2|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_R_Clavicle|Bip01_R_UpperArm|Bip01_R_Forearm|Bip01_R_Hand.s" "|Stand_Idle_2|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_R_Clavicle|Bip01_R_UpperArm|Bip01_R_Forearm|Bip01_R_Hand|Bip01_R_Finger0.is"
		;
connectAttr "|Stand_Idle_2|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_R_Clavicle|Bip01_R_UpperArm|Bip01_R_Forearm|Bip01_R_Hand|Bip01_R_Finger0.s" "|Stand_Idle_2|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_R_Clavicle|Bip01_R_UpperArm|Bip01_R_Forearm|Bip01_R_Hand|Bip01_R_Finger0|Bip01_R_Finger01.is"
		;
connectAttr "|Stand_Idle_2|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_R_Clavicle|Bip01_R_UpperArm|Bip01_R_Forearm|Bip01_R_Hand|Bip01_R_Finger0|Bip01_R_Finger01.s" "|Stand_Idle_2|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_R_Clavicle|Bip01_R_UpperArm|Bip01_R_Forearm|Bip01_R_Hand|Bip01_R_Finger0|Bip01_R_Finger01|_notused_Bip01_R_Finger0Nub.is"
		;
connectAttr "|Stand_Idle_2|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_R_Clavicle|Bip01_R_UpperArm|Bip01_R_Forearm|Bip01_R_Hand.s" "|Stand_Idle_2|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_R_Clavicle|Bip01_R_UpperArm|Bip01_R_Forearm|Bip01_R_Hand|Bip01_R_Finger2.is"
		;
connectAttr "|Stand_Idle_2|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_R_Clavicle|Bip01_R_UpperArm|Bip01_R_Forearm|Bip01_R_Hand|Bip01_R_Finger2.s" "|Stand_Idle_2|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_R_Clavicle|Bip01_R_UpperArm|Bip01_R_Forearm|Bip01_R_Hand|Bip01_R_Finger2|Bip01_R_Finger21.is"
		;
connectAttr "|Stand_Idle_2|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_R_Clavicle|Bip01_R_UpperArm|Bip01_R_Forearm|Bip01_R_Hand|Bip01_R_Finger2|Bip01_R_Finger21.s" "|Stand_Idle_2|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_R_Clavicle|Bip01_R_UpperArm|Bip01_R_Forearm|Bip01_R_Hand|Bip01_R_Finger2|Bip01_R_Finger21|_notused_Bip01_R_Finger2Nub.is"
		;
connectAttr "|Stand_Idle_2|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_R_Clavicle|Bip01_R_UpperArm|Bip01_R_Forearm|Bip01_R_Hand.s" "|Stand_Idle_2|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_R_Clavicle|Bip01_R_UpperArm|Bip01_R_Forearm|Bip01_R_Hand|Bip01_R_Finger1.is"
		;
connectAttr "|Stand_Idle_2|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_R_Clavicle|Bip01_R_UpperArm|Bip01_R_Forearm|Bip01_R_Hand|Bip01_R_Finger1.s" "|Stand_Idle_2|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_R_Clavicle|Bip01_R_UpperArm|Bip01_R_Forearm|Bip01_R_Hand|Bip01_R_Finger1|Bip01_R_Finger11.is"
		;
connectAttr "|Stand_Idle_2|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_R_Clavicle|Bip01_R_UpperArm|Bip01_R_Forearm|Bip01_R_Hand|Bip01_R_Finger1|Bip01_R_Finger11.s" "|Stand_Idle_2|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_R_Clavicle|Bip01_R_UpperArm|Bip01_R_Forearm|Bip01_R_Hand|Bip01_R_Finger1|Bip01_R_Finger11|_notused_Bip01_R_Finger1Nub.is"
		;
connectAttr "|Stand_Idle_2|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_R_Clavicle|Bip01_R_UpperArm|Bip01_R_Forearm|Bip01_R_Hand.s" "|Stand_Idle_2|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_R_Clavicle|Bip01_R_UpperArm|Bip01_R_Forearm|Bip01_R_Hand|Bip01_R_Finger3.is"
		;
connectAttr "|Stand_Idle_2|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_R_Clavicle|Bip01_R_UpperArm|Bip01_R_Forearm|Bip01_R_Hand|Bip01_R_Finger3.s" "|Stand_Idle_2|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_R_Clavicle|Bip01_R_UpperArm|Bip01_R_Forearm|Bip01_R_Hand|Bip01_R_Finger3|Bip01_R_Finger31.is"
		;
connectAttr "|Stand_Idle_2|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_R_Clavicle|Bip01_R_UpperArm|Bip01_R_Forearm|Bip01_R_Hand|Bip01_R_Finger3|Bip01_R_Finger31.s" "|Stand_Idle_2|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_R_Clavicle|Bip01_R_UpperArm|Bip01_R_Forearm|Bip01_R_Hand|Bip01_R_Finger3|Bip01_R_Finger31|_notused_Bip01_R_Finger3Nub.is"
		;
connectAttr "|Stand_Idle_2|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_R_Clavicle|Bip01_R_UpperArm|Bip01_R_Forearm|Bip01_R_Hand.s" "|Stand_Idle_2|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_R_Clavicle|Bip01_R_UpperArm|Bip01_R_Forearm|Bip01_R_Hand|Bip01_R_Finger4.is"
		;
connectAttr "|Stand_Idle_2|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_R_Clavicle|Bip01_R_UpperArm|Bip01_R_Forearm|Bip01_R_Hand|Bip01_R_Finger4.s" "|Stand_Idle_2|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_R_Clavicle|Bip01_R_UpperArm|Bip01_R_Forearm|Bip01_R_Hand|Bip01_R_Finger4|Bip01_R_Finger41.is"
		;
connectAttr "|Stand_Idle_2|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_R_Clavicle|Bip01_R_UpperArm|Bip01_R_Forearm|Bip01_R_Hand|Bip01_R_Finger4|Bip01_R_Finger41.s" "|Stand_Idle_2|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_R_Clavicle|Bip01_R_UpperArm|Bip01_R_Forearm|Bip01_R_Hand|Bip01_R_Finger4|Bip01_R_Finger41|_notused_Bip01_R_Finger4Nub.is"
		;
connectAttr "|Stand_Idle_2|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck.s" "|Stand_Idle_2|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_L_Clavicle.is"
		;
connectAttr "Bip01_L_Clavicle_rotateX.o" "|Stand_Idle_2|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_L_Clavicle.rx"
		;
connectAttr "Bip01_L_Clavicle_rotateY.o" "|Stand_Idle_2|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_L_Clavicle.ry"
		;
connectAttr "Bip01_L_Clavicle_rotateZ.o" "|Stand_Idle_2|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_L_Clavicle.rz"
		;
connectAttr "|Stand_Idle_2|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_L_Clavicle.s" "|Stand_Idle_2|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_L_Clavicle|Bip01_L_UpperArm.is"
		;
connectAttr "Bip01_L_UpperArm_rotateX.o" "|Stand_Idle_2|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_L_Clavicle|Bip01_L_UpperArm.rx"
		;
connectAttr "Bip01_L_UpperArm_rotateY.o" "|Stand_Idle_2|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_L_Clavicle|Bip01_L_UpperArm.ry"
		;
connectAttr "Bip01_L_UpperArm_rotateZ.o" "|Stand_Idle_2|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_L_Clavicle|Bip01_L_UpperArm.rz"
		;
connectAttr "|Stand_Idle_2|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_L_Clavicle|Bip01_L_UpperArm.s" "|Stand_Idle_2|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_L_Clavicle|Bip01_L_UpperArm|Bip01_L_Forearm.is"
		;
connectAttr "Bip01_L_Forearm_rotateX.o" "|Stand_Idle_2|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_L_Clavicle|Bip01_L_UpperArm|Bip01_L_Forearm.rx"
		;
connectAttr "Bip01_L_Forearm_rotateY.o" "|Stand_Idle_2|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_L_Clavicle|Bip01_L_UpperArm|Bip01_L_Forearm.ry"
		;
connectAttr "Bip01_L_Forearm_rotateZ.o" "|Stand_Idle_2|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_L_Clavicle|Bip01_L_UpperArm|Bip01_L_Forearm.rz"
		;
connectAttr "|Stand_Idle_2|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_L_Clavicle|Bip01_L_UpperArm|Bip01_L_Forearm.s" "|Stand_Idle_2|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_L_Clavicle|Bip01_L_UpperArm|Bip01_L_Forearm|Bip01_L_Hand.is"
		;
connectAttr "|Stand_Idle_2|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_L_Clavicle|Bip01_L_UpperArm|Bip01_L_Forearm|Bip01_L_Hand.s" "|Stand_Idle_2|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_L_Clavicle|Bip01_L_UpperArm|Bip01_L_Forearm|Bip01_L_Hand|Bip01_L_Finger0.is"
		;
connectAttr "|Stand_Idle_2|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_L_Clavicle|Bip01_L_UpperArm|Bip01_L_Forearm|Bip01_L_Hand|Bip01_L_Finger0.s" "|Stand_Idle_2|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_L_Clavicle|Bip01_L_UpperArm|Bip01_L_Forearm|Bip01_L_Hand|Bip01_L_Finger0|Bip01_L_Finger01.is"
		;
connectAttr "|Stand_Idle_2|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_L_Clavicle|Bip01_L_UpperArm|Bip01_L_Forearm|Bip01_L_Hand|Bip01_L_Finger0|Bip01_L_Finger01.s" "|Stand_Idle_2|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_L_Clavicle|Bip01_L_UpperArm|Bip01_L_Forearm|Bip01_L_Hand|Bip01_L_Finger0|Bip01_L_Finger01|_notused_Bip01_L_Finger0Nub.is"
		;
connectAttr "|Stand_Idle_2|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_L_Clavicle|Bip01_L_UpperArm|Bip01_L_Forearm|Bip01_L_Hand.s" "|Stand_Idle_2|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_L_Clavicle|Bip01_L_UpperArm|Bip01_L_Forearm|Bip01_L_Hand|Bip01_L_Finger2.is"
		;
connectAttr "|Stand_Idle_2|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_L_Clavicle|Bip01_L_UpperArm|Bip01_L_Forearm|Bip01_L_Hand|Bip01_L_Finger2.s" "|Stand_Idle_2|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_L_Clavicle|Bip01_L_UpperArm|Bip01_L_Forearm|Bip01_L_Hand|Bip01_L_Finger2|Bip01_L_Finger21.is"
		;
connectAttr "|Stand_Idle_2|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_L_Clavicle|Bip01_L_UpperArm|Bip01_L_Forearm|Bip01_L_Hand|Bip01_L_Finger2|Bip01_L_Finger21.s" "|Stand_Idle_2|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_L_Clavicle|Bip01_L_UpperArm|Bip01_L_Forearm|Bip01_L_Hand|Bip01_L_Finger2|Bip01_L_Finger21|_notused_Bip01_L_Finger2Nub.is"
		;
connectAttr "|Stand_Idle_2|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_L_Clavicle|Bip01_L_UpperArm|Bip01_L_Forearm|Bip01_L_Hand.s" "|Stand_Idle_2|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_L_Clavicle|Bip01_L_UpperArm|Bip01_L_Forearm|Bip01_L_Hand|Bip01_L_Finger1.is"
		;
connectAttr "|Stand_Idle_2|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_L_Clavicle|Bip01_L_UpperArm|Bip01_L_Forearm|Bip01_L_Hand|Bip01_L_Finger1.s" "|Stand_Idle_2|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_L_Clavicle|Bip01_L_UpperArm|Bip01_L_Forearm|Bip01_L_Hand|Bip01_L_Finger1|Bip01_L_Finger11.is"
		;
connectAttr "|Stand_Idle_2|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_L_Clavicle|Bip01_L_UpperArm|Bip01_L_Forearm|Bip01_L_Hand|Bip01_L_Finger1|Bip01_L_Finger11.s" "|Stand_Idle_2|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_L_Clavicle|Bip01_L_UpperArm|Bip01_L_Forearm|Bip01_L_Hand|Bip01_L_Finger1|Bip01_L_Finger11|_notused_Bip01_L_Finger1Nub.is"
		;
connectAttr "|Stand_Idle_2|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_L_Clavicle|Bip01_L_UpperArm|Bip01_L_Forearm|Bip01_L_Hand.s" "|Stand_Idle_2|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_L_Clavicle|Bip01_L_UpperArm|Bip01_L_Forearm|Bip01_L_Hand|Bip01_L_Finger3.is"
		;
connectAttr "|Stand_Idle_2|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_L_Clavicle|Bip01_L_UpperArm|Bip01_L_Forearm|Bip01_L_Hand|Bip01_L_Finger3.s" "|Stand_Idle_2|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_L_Clavicle|Bip01_L_UpperArm|Bip01_L_Forearm|Bip01_L_Hand|Bip01_L_Finger3|Bip01_L_Finger31.is"
		;
connectAttr "|Stand_Idle_2|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_L_Clavicle|Bip01_L_UpperArm|Bip01_L_Forearm|Bip01_L_Hand|Bip01_L_Finger3|Bip01_L_Finger31.s" "|Stand_Idle_2|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_L_Clavicle|Bip01_L_UpperArm|Bip01_L_Forearm|Bip01_L_Hand|Bip01_L_Finger3|Bip01_L_Finger31|_notused_Bip01_L_Finger3Nub.is"
		;
connectAttr "|Stand_Idle_2|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_L_Clavicle|Bip01_L_UpperArm|Bip01_L_Forearm|Bip01_L_Hand.s" "|Stand_Idle_2|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_L_Clavicle|Bip01_L_UpperArm|Bip01_L_Forearm|Bip01_L_Hand|Bip01_L_Finger4.is"
		;
connectAttr "|Stand_Idle_2|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_L_Clavicle|Bip01_L_UpperArm|Bip01_L_Forearm|Bip01_L_Hand|Bip01_L_Finger4.s" "|Stand_Idle_2|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_L_Clavicle|Bip01_L_UpperArm|Bip01_L_Forearm|Bip01_L_Hand|Bip01_L_Finger4|Bip01_L_Finger41.is"
		;
connectAttr "|Stand_Idle_2|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_L_Clavicle|Bip01_L_UpperArm|Bip01_L_Forearm|Bip01_L_Hand|Bip01_L_Finger4|Bip01_L_Finger41.s" "|Stand_Idle_2|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_L_Clavicle|Bip01_L_UpperArm|Bip01_L_Forearm|Bip01_L_Hand|Bip01_L_Finger4|Bip01_L_Finger41|_notused_Bip01_L_Finger4Nub.is"
		;
connectAttr "|Stand_Idle_2|Bip01_Spine.s" "|Stand_Idle_2|Bip01_Spine|Bip01_L_Thigh.is"
		;
connectAttr "Bip01_L_Thigh_rotateX.o" "|Stand_Idle_2|Bip01_Spine|Bip01_L_Thigh.rx"
		;
connectAttr "Bip01_L_Thigh_rotateY.o" "|Stand_Idle_2|Bip01_Spine|Bip01_L_Thigh.ry"
		;
connectAttr "Bip01_L_Thigh_rotateZ.o" "|Stand_Idle_2|Bip01_Spine|Bip01_L_Thigh.rz"
		;
connectAttr "|Stand_Idle_2|Bip01_Spine|Bip01_L_Thigh.s" "|Stand_Idle_2|Bip01_Spine|Bip01_L_Thigh|Bip01_L_Calf.is"
		;
connectAttr "Bip01_L_Calf_rotateX.o" "|Stand_Idle_2|Bip01_Spine|Bip01_L_Thigh|Bip01_L_Calf.rx"
		;
connectAttr "Bip01_L_Calf_rotateY.o" "|Stand_Idle_2|Bip01_Spine|Bip01_L_Thigh|Bip01_L_Calf.ry"
		;
connectAttr "Bip01_L_Calf_rotateZ.o" "|Stand_Idle_2|Bip01_Spine|Bip01_L_Thigh|Bip01_L_Calf.rz"
		;
connectAttr "|Stand_Idle_2|Bip01_Spine|Bip01_L_Thigh|Bip01_L_Calf.s" "|Stand_Idle_2|Bip01_Spine|Bip01_L_Thigh|Bip01_L_Calf|Bip01_L_Foot.is"
		;
connectAttr "Bip01_L_Foot_rotateX.o" "|Stand_Idle_2|Bip01_Spine|Bip01_L_Thigh|Bip01_L_Calf|Bip01_L_Foot.rx"
		;
connectAttr "Bip01_L_Foot_rotateY.o" "|Stand_Idle_2|Bip01_Spine|Bip01_L_Thigh|Bip01_L_Calf|Bip01_L_Foot.ry"
		;
connectAttr "Bip01_L_Foot_rotateZ.o" "|Stand_Idle_2|Bip01_Spine|Bip01_L_Thigh|Bip01_L_Calf|Bip01_L_Foot.rz"
		;
connectAttr "|Stand_Idle_2|Bip01_Spine|Bip01_L_Thigh|Bip01_L_Calf|Bip01_L_Foot.s" "|Stand_Idle_2|Bip01_Spine|Bip01_L_Thigh|Bip01_L_Calf|Bip01_L_Foot|Bip01_L_Toe0.is"
		;
connectAttr "|Stand_Idle_2|Bip01_Spine|Bip01_L_Thigh|Bip01_L_Calf|Bip01_L_Foot|Bip01_L_Toe0.s" "|Stand_Idle_2|Bip01_Spine|Bip01_L_Thigh|Bip01_L_Calf|Bip01_L_Foot|Bip01_L_Toe0|_notused_Bip01_L_Toe0Nub.is"
		;
connectAttr "|Stand_Idle_2|Bip01_Spine.s" "|Stand_Idle_2|Bip01_Spine|Bip01_R_Thigh.is"
		;
connectAttr "Bip01_R_Thigh_rotateX.o" "|Stand_Idle_2|Bip01_Spine|Bip01_R_Thigh.rx"
		;
connectAttr "Bip01_R_Thigh_rotateY.o" "|Stand_Idle_2|Bip01_Spine|Bip01_R_Thigh.ry"
		;
connectAttr "Bip01_R_Thigh_rotateZ.o" "|Stand_Idle_2|Bip01_Spine|Bip01_R_Thigh.rz"
		;
connectAttr "|Stand_Idle_2|Bip01_Spine|Bip01_R_Thigh.s" "|Stand_Idle_2|Bip01_Spine|Bip01_R_Thigh|Bip01_R_Calf.is"
		;
connectAttr "Bip01_R_Calf_rotateX.o" "|Stand_Idle_2|Bip01_Spine|Bip01_R_Thigh|Bip01_R_Calf.rx"
		;
connectAttr "Bip01_R_Calf_rotateY.o" "|Stand_Idle_2|Bip01_Spine|Bip01_R_Thigh|Bip01_R_Calf.ry"
		;
connectAttr "Bip01_R_Calf_rotateZ.o" "|Stand_Idle_2|Bip01_Spine|Bip01_R_Thigh|Bip01_R_Calf.rz"
		;
connectAttr "|Stand_Idle_2|Bip01_Spine|Bip01_R_Thigh|Bip01_R_Calf.s" "|Stand_Idle_2|Bip01_Spine|Bip01_R_Thigh|Bip01_R_Calf|Bip01_R_Foot.is"
		;
connectAttr "Bip01_R_Foot_rotateX.o" "|Stand_Idle_2|Bip01_Spine|Bip01_R_Thigh|Bip01_R_Calf|Bip01_R_Foot.rx"
		;
connectAttr "Bip01_R_Foot_rotateY.o" "|Stand_Idle_2|Bip01_Spine|Bip01_R_Thigh|Bip01_R_Calf|Bip01_R_Foot.ry"
		;
connectAttr "Bip01_R_Foot_rotateZ.o" "|Stand_Idle_2|Bip01_Spine|Bip01_R_Thigh|Bip01_R_Calf|Bip01_R_Foot.rz"
		;
connectAttr "|Stand_Idle_2|Bip01_Spine|Bip01_R_Thigh|Bip01_R_Calf|Bip01_R_Foot.s" "|Stand_Idle_2|Bip01_Spine|Bip01_R_Thigh|Bip01_R_Calf|Bip01_R_Foot|Bip01_R_Toe0.is"
		;
connectAttr "|Stand_Idle_2|Bip01_Spine|Bip01_R_Thigh|Bip01_R_Calf|Bip01_R_Foot|Bip01_R_Toe0.s" "|Stand_Idle_2|Bip01_Spine|Bip01_R_Thigh|Bip01_R_Calf|Bip01_R_Foot|Bip01_R_Toe0|_notused_Bip01_R_Toe0Nub.is"
		;
connectAttr "|Stand_Idle_2|Bip01_Spine.msg" ":hyperGraphLayout.hyp[4].dn";
// End of Animation v5 - Stand Idle 2.ma
