//Maya ASCII 2014 scene
//Name: Animation v5 - Stand Talk 5.ma
//Last modified: Tue, Apr 14, 2015 07:38:45 AM
//Codeset: 1252
requires maya "2014";
currentUnit -l centimeter -a degree -t film;
fileInfo "application" "maya";
fileInfo "product" "Maya 2014";
fileInfo "version" "2014 x64";
fileInfo "cutIdentifier" "201303010241-864206";
fileInfo "osv" "Microsoft Windows 7 Enterprise Edition, 64-bit Windows 7 Service Pack 1 (Build 7601)\n";
fileInfo "license" "education";
createNode transform -n "Stand_Talk_5";
createNode joint -n "Bip01_Spine" -p "Stand_Talk_5";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".s" -type "double3" 1 0.99999999999999956 1 ;
	setAttr ".jo" -type "double3" 90.000171 0 89.999999999999986 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" 0.00041359021872633406 0.99974830939735237 0.022430934003442312 0
		 0.70710082718146761 -0.016153575523873306 0.70692820158561975 0 0.70711261418702975 0.015568553398769388 -0.70692953750897569 0
		 -6.7139604917915256e-006 99.443700000000007 -2.2495999999899592 1;
	setAttr -k on ".liw";
	setAttr ".fbxID" 5;
createNode joint -n "Bip01_Spine1" -p "|Stand_Talk_5|Bip01_Spine";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".t" -type "double3" 13.3984 -0.0107 0 ;
	setAttr ".s" -type "double3" 0.99999999999999989 0.99999999999999989 1 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" 0.0038255966723317676 0.015307468004230922 0.99987551536848818 0
		 0.011000641830761762 -0.999822973655216 0.015264574365256002 0 0.99993217304445803 0.010940876315020786 -0.0039933114137866372 0
		 9.9999999999999995e-007 2.2603360000000001 112.84212599999999 1;
	setAttr -k on ".liw";
	setAttr ".fbxID" 5;
createNode joint -n "Bip01_Spine2" -p "|Stand_Talk_5|Bip01_Spine|Bip01_Spine1";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".t" -type "double3" 13.3983 -0.0107 0 ;
	setAttr ".s" -type "double3" 0.99999999999999989 1 1 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" 0 0 1 0 0 -1 0 0 1 0 0 0 -3.9999999999999998e-006 -9.6239790000000003 -19.074804 1;
	setAttr -k on ".liw";
	setAttr ".fbxID" 5;
createNode joint -n "Bip01_Neck" -p "|Stand_Talk_5|Bip01_Spine|Bip01_Spine1|Bip01_Spine2";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".t" -type "double3" 13.3985 -0.007899999999999999 0 ;
	setAttr ".r" -type "double3" 1.1848489498583726e-023 -5.9999999999999985e-006 1.1479959999999994 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" 0 -0.020830999999999999 0.99978299999999998 0 0 -0.99978299999999998 -0.020830999999999999 0
		 1 0 0 0 -1.0000000000000001e-005 -9.6266999999999996 -5.6762839999999999 1;
	setAttr -k on ".liw";
	setAttr ".fbxID" 5;
createNode joint -n "Bip01_Head" -p "|Stand_Talk_5|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".t" -type "double3" 9.9829 0 0 ;
	setAttr ".r" -type "double3" 9.4787915988671744e-023 -1.3000000000000004e-005 4.622390999999995 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" 0 -0.10133399999999999 0.99485299999999999 0 0 -0.99485199999999996 -0.10133399999999999 0
		 1 0 0 0 -4.4000000000000012e-005 -9.8346470000000004 4.3044190000000002 1;
	setAttr -k on ".liw";
	setAttr ".fbxID" 5;
createNode joint -n "_notused_Bip01_HeadNub" -p "|Stand_Talk_5|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_Head";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".t" -type "double3" 18.3537 0 0 ;
	setAttr ".ssc" no;
	setAttr ".fbxID" 5;
createNode joint -n "Bip01_R_Clavicle" -p "|Stand_Talk_5|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".t" -type "double3" -0.8268 -0.6612 -3.9165 ;
	setAttr ".s" -type "double3" 1 1.0000000000000002 1 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" -0.99454799999999999 -9.9454800000000002e-007 -0.104286 0
		 -9.8906500000000008e-007 1 -1.0428599999999999e-007 0 0.104286 -5.7179000000000015e-010 -0.99454799999999999 0
		 -3.9165049999999999 2.9251839999999998 138.82619800000001 1;
	setAttr -k on ".liw";
	setAttr ".fbxID" 5;
createNode joint -n "Bip01_R_UpperArm" -p "|Stand_Talk_5|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_R_Clavicle";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".t" -type "double3" 10.6745 0 0 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" -0.75332600000000005 0.10000199999999999 -0.65000000000000002 0
		 0.071782399999999996 0.99496899999999999 0.0698825 0 0.65371900000000005 0.0059857399999999998 -0.75671500000000003 0
		 -14.532831 2.9251239999999998 137.712976 1;
	setAttr -k on ".liw";
	setAttr ".fbxID" 5;
createNode joint -n "Bip01_R_Forearm" -p "|Stand_Talk_5|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_R_Clavicle|Bip01_R_UpperArm";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".t" -type "double3" 25.3375 0 0 ;
	setAttr ".s" -type "double3" 0.99999999999999989 1 1 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" -0.752861 -0.095945600000000006 -0.65114899999999998 0
		 -0.076500899999999997 0.99536800000000003 -0.058215000000000003 0 0.65371900000000005 0.0059856600000000003 -0.756714 0
		 -33.620182 5.4589869999999996 121.243574 1;
	setAttr -k on ".liw";
	setAttr ".fbxID" 5;
createNode joint -n "Bip01_R_Hand" -p "|Stand_Talk_5|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_R_Clavicle|Bip01_R_UpperArm|Bip01_R_Forearm";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".t" -type "double3" 24.9881 0 0 ;
	setAttr ".r" -type "double3" 90.133325000000013 -6.3004020000000009 2.5653330000000008 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" -0.67922700000000003 -0.050331500000000001 -0.73219999999999996 0
		 0.73278100000000002 0.0092545500000000003 -0.68040299999999998 0 0.0410219 -0.99868900000000005 0.030596000000000002 0
		 -52.432737000000003 3.0614050000000002 104.972679 1;
	setAttr -k on ".liw";
	setAttr ".fbxID" 5;
createNode joint -n "Bip01_R_Finger0" -p "|Stand_Talk_5|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_R_Clavicle|Bip01_R_UpperArm|Bip01_R_Forearm|Bip01_R_Hand";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".t" -type "double3" 2.3036000000000003 1.3941 2.9222 ;
	setAttr ".r" -type "double3" -81.93018 -23.314181 18.295908 ;
	setAttr ".s" -type "double3" 0.99999999999999989 1 1 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" -0.36474400000000001 -0.436471 -0.82246900000000001 0
		 -0.072261699999999998 0.89392799999999994 -0.44234699999999999 0 0.92829899999999999 -0.10191 -0.357595 0
		 -52.855969000000002 0.0400224 102.426821 1;
	setAttr -k on ".liw";
	setAttr ".fbxID" 5;
createNode joint -n "Bip01_R_Finger01" -p "|Stand_Talk_5|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_R_Clavicle|Bip01_R_UpperArm|Bip01_R_Forearm|Bip01_R_Hand|Bip01_R_Finger0";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".t" -type "double3" 4.0806 0 0 ;
	setAttr ".r" -type "double3" 9.4787915988669464e-023 3.0000000000000005e-006 24.064549 ;
	setAttr ".s" -type "double3" 1 1 0.99999999999999989 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" -0.36250900000000003 -0.034022900000000002 -0.93135900000000005 0
		 0.082749500000000004 0.99421099999999996 -0.068527199999999996 0 0.92830000000000001 -0.101911 -0.357595 0
		 -54.344414999999998 -1.7410159999999999 99.070615000000004 1;
	setAttr -k on ".liw";
	setAttr ".fbxID" 5;
createNode joint -n "_notused_Bip01_R_Finger0Nub" -p "|Stand_Talk_5|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_R_Clavicle|Bip01_R_UpperArm|Bip01_R_Forearm|Bip01_R_Hand|Bip01_R_Finger0|Bip01_R_Finger01";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".t" -type "double3" 3.3413 0 0 ;
	setAttr ".r" -type "double3" 1e-006 0 -180 ;
	setAttr ".ssc" no;
	setAttr ".fbxID" 5;
createNode joint -n "Bip01_R_Finger2" -p "|Stand_Talk_5|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_R_Clavicle|Bip01_R_UpperArm|Bip01_R_Forearm|Bip01_R_Hand";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".t" -type "double3" 7.3709 -0.4816 0.6341 ;
	setAttr ".r" -type "double3" 0.043615000000000029 -0.013407000000000007 17.086613999999997 ;
	setAttr ".s" -type "double3" 0.99999999999999989 0.99999999999999989 0.99999999999999989 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" -0.43393399999999999 -0.045623999999999998 -0.89978899999999995 0
		 0.90003599999999995 0.022875199999999998 -0.43521399999999999 0 0.040439000000000003 -0.99869600000000003 0.031137000000000001 0
		 -57.766043000000003 2.0527600000000001 99.922762000000006 1;
	setAttr -k on ".liw";
	setAttr ".fbxID" 5;
createNode joint -n "Bip01_R_Finger21" -p "|Stand_Talk_5|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_R_Clavicle|Bip01_R_UpperArm|Bip01_R_Forearm|Bip01_R_Hand|Bip01_R_Finger2";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".t" -type "double3" 3.8208 0 0 ;
	setAttr ".r" -type "double3" 0 0 29.946389999999987 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" 0.0732873 -0.028113900000000001 -0.99691399999999997 0
		 0.99648999999999999 0.042598999999999998 0.072054900000000005 0 0.0404418 -0.99869600000000003 0.0311372 0
		 -59.424045999999997 1.8784400000000001 96.484806000000006 1;
	setAttr -k on ".liw";
	setAttr ".fbxID" 5;
createNode joint -n "_notused_Bip01_R_Finger2Nub" -p "|Stand_Talk_5|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_R_Clavicle|Bip01_R_UpperArm|Bip01_R_Forearm|Bip01_R_Hand|Bip01_R_Finger2|Bip01_R_Finger21";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".t" -type "double3" 4.7174000000000005 0 0 ;
	setAttr ".r" -type "double3" 0 0 180 ;
	setAttr ".ssc" no;
	setAttr ".fbxID" 5;
createNode joint -n "Bip01_R_Finger1" -p "|Stand_Talk_5|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_R_Clavicle|Bip01_R_UpperArm|Bip01_R_Forearm|Bip01_R_Hand";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".t" -type "double3" 7.3263 -0.211 2.2784 ;
	setAttr ".r" -type "double3" -0.73310399999999953 -5.3145149999999983 22.646732999999994 ;
	setAttr ".s" -type "double3" 1 1 0.99999999999999989 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" -0.33941900000000003 -0.13520499999999999 -0.93086800000000003 0
		 0.93680799999999997 0.040593299999999999 -0.34748099999999998 0 0.084767999999999996 -0.98998600000000003 0.112883 0
		 -57.470132999999997 0.41539700000000002 99.821579 1;
	setAttr -k on ".liw";
	setAttr ".fbxID" 5;
createNode joint -n "Bip01_R_Finger11" -p "|Stand_Talk_5|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_R_Clavicle|Bip01_R_UpperArm|Bip01_R_Forearm|Bip01_R_Hand|Bip01_R_Finger1";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".t" -type "double3" 3.5597999999999996 0 0 ;
	setAttr ".r" -type "double3" -2e-006 3.999999999999999e-006 22.627677999999996 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" 0.047135799999999999 -0.10918 -0.99290400000000001 0
		 0.995286 0.089482699999999998 0.037409400000000002 0 0.084763400000000003 -0.98998600000000003 0.112883 0
		 -58.678344000000003 -0.065903900000000001 96.507863999999998 1;
	setAttr -k on ".liw";
	setAttr ".fbxID" 5;
createNode joint -n "_notused_Bip01_R_Finger1Nub" -p "|Stand_Talk_5|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_R_Clavicle|Bip01_R_UpperArm|Bip01_R_Forearm|Bip01_R_Hand|Bip01_R_Finger1|Bip01_R_Finger11";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".t" -type "double3" 4.1404000000000005 0 0 ;
	setAttr ".r" -type "double3" 0 0 180 ;
	setAttr ".ssc" no;
	setAttr ".fbxID" 5;
createNode joint -n "Bip01_R_Finger3" -p "|Stand_Talk_5|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_R_Clavicle|Bip01_R_UpperArm|Bip01_R_Forearm|Bip01_R_Hand";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".t" -type "double3" 7.2345 -0.5997 -0.9332 ;
	setAttr ".r" -type "double3" 0.043363999999999805 3.826455 18.509659999999997 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" -0.41327900000000001 0.0219578 -0.91034000000000004 0
		 0.91051400000000005 0.0239971 -0.41277999999999998 0 0.0127817 -0.999471 -0.0299104 0
		 -57.824306 3.6237469999999998 100.05504999999999 1;
	setAttr -k on ".liw";
	setAttr ".fbxID" 5;
createNode joint -n "Bip01_R_Finger31" -p "|Stand_Talk_5|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_R_Clavicle|Bip01_R_UpperArm|Bip01_R_Forearm|Bip01_R_Hand|Bip01_R_Finger3";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".t" -type "double3" 3.7241 0 0 ;
	setAttr ".r" -type "double3" 3.0000000000000013e-006 3.0000000000000005e-006 27.984287000000009 ;
	setAttr ".s" -type "double3" 1 0.99999999999999989 0.99999999999999989 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" 0.0622821 0.0306512 -0.99758800000000003 0 0.99797599999999997 0.0108983 0.062641199999999994 0
		 0.012792100000000001 -0.999471 -0.0299104 0 -59.363360999999998 3.705527 96.664748000000003 1;
	setAttr -k on ".liw";
	setAttr ".fbxID" 5;
createNode joint -n "_notused_Bip01_R_Finger3Nub" -p "|Stand_Talk_5|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_R_Clavicle|Bip01_R_UpperArm|Bip01_R_Forearm|Bip01_R_Hand|Bip01_R_Finger3|Bip01_R_Finger31";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".t" -type "double3" 4.2397 0 0 ;
	setAttr ".r" -type "double3" 3e-006 -5e-006 180 ;
	setAttr ".ssc" no;
	setAttr ".fbxID" 5;
createNode joint -n "Bip01_R_Finger4" -p "|Stand_Talk_5|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_R_Clavicle|Bip01_R_UpperArm|Bip01_R_Forearm|Bip01_R_Hand";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".t" -type "double3" 7.2292 -0.4187 -2.3639 ;
	setAttr ".r" -type "double3" -0.17713400000000007 5.5619560000000012 16.615208 ;
	setAttr ".s" -type "double3" 0.99999999999999989 0.99999999999999989 1 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" -0.44323200000000001 0.051426199999999998 -0.89493100000000003 0
		 0.89640699999999995 0.026346700000000001 -0.44244899999999998 0 0.00082491599999999997 -0.99832900000000002 -0.057776500000000001 0
		 -57.746822000000002 5.0544450000000003 99.892002000000005 1;
	setAttr -k on ".liw";
	setAttr ".fbxID" 5;
createNode joint -n "Bip01_R_Finger41" -p "|Stand_Talk_5|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_R_Clavicle|Bip01_R_UpperArm|Bip01_R_Forearm|Bip01_R_Hand|Bip01_R_Finger4";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".t" -type "double3" 3.4791000000000003 0 0 ;
	setAttr ".r" -type "double3" -1.9999999999999995e-006 -3.0000000000000005e-006 29.444368 ;
	setAttr ".s" -type "double3" 1 0.99999999999999989 1 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" 0.054672699999999998 0.057735700000000001 -0.996834 0
		 0.99850399999999995 -0.0023359700000000001 0.054628999999999997 0 0.00082546799999999999 -0.99832900000000002 -0.057777000000000002 0
		 -59.288860999999997 5.2333790000000002 96.778379000000001 1;
	setAttr -k on ".liw";
	setAttr ".fbxID" 5;
createNode joint -n "_notused_Bip01_R_Finger4Nub" -p "|Stand_Talk_5|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_R_Clavicle|Bip01_R_UpperArm|Bip01_R_Forearm|Bip01_R_Hand|Bip01_R_Finger4|Bip01_R_Finger41";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".t" -type "double3" 3.3974 0 0 ;
	setAttr ".r" -type "double3" 0 0 -180 ;
	setAttr ".ssc" no;
	setAttr ".fbxID" 5;
createNode joint -n "Bip01_L_Clavicle" -p "|Stand_Talk_5|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".t" -type "double3" -0.8268 -0.6612 3.9165 ;
	setAttr ".s" -type "double3" 1 1.0000000000000002 1 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" 0.99454799999999999 9.9454800000000002e-007 -0.104286 0
		 -9.8906500000000008e-007 1 1.0428599999999999e-007 0 0.104286 -5.7179000000000015e-010 0.99454799999999999 0
		 3.9165070000000002 2.9252039999999999 138.82619700000001 1;
	setAttr -k on ".liw";
	setAttr ".fbxID" 5;
createNode joint -n "Bip01_L_UpperArm" -p "|Stand_Talk_5|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_L_Clavicle";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".t" -type "double3" 10.6745 0 0 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" 0.75332500000000002 0.100004 -0.65000000000000002 0
		 -0.071785600000000005 0.99496899999999999 0.069882299999999994 0 0.65371900000000005 -0.0059834299999999997 0.756714 0
		 14.532825000000001 2.925173 137.712974 1;
	setAttr -k on ".liw";
	setAttr ".fbxID" 5;
createNode joint -n "Bip01_L_Forearm" -p "|Stand_Talk_5|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_L_Clavicle|Bip01_L_UpperArm";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".t" -type "double3" 25.3374 0 0 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" 0.752861 -0.095943299999999995 -0.65114899999999998 0
		 0.076497700000000002 0.99536899999999995 -0.058215299999999998 0 0.65371900000000005 -0.0059834199999999997 0.756714 0
		 33.620189000000003 5.4590990000000001 121.243576 1;
	setAttr -k on ".liw";
	setAttr ".fbxID" 5;
createNode joint -n "Bip01_L_Hand" -p "|Stand_Talk_5|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_L_Clavicle|Bip01_L_UpperArm|Bip01_L_Forearm";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".t" -type "double3" 24.9881 0 0 ;
	setAttr ".r" -type "double3" -90.133325 6.300397000000002 2.565332999999999 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" 0.67922800000000005 -0.050329499999999999 -0.73219999999999996 0
		 -0.73278100000000002 0.0092525000000000003 -0.68040299999999998 0 0.041019 0.99868999999999997 -0.030595899999999999 0
		 52.432744999999997 3.0615610000000002 104.972662 1;
	setAttr -k on ".liw";
	setAttr ".fbxID" 5;
createNode joint -n "Bip01_L_Finger0" -p "|Stand_Talk_5|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_L_Clavicle|Bip01_L_UpperArm|Bip01_L_Forearm|Bip01_L_Hand";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".t" -type "double3" 2.3036000000000003 1.3941 -2.9222 ;
	setAttr ".r" -type "double3" 81.930180000000021 23.314181000000019 18.295909 ;
	setAttr ".s" -type "double3" 0.99999999999999989 1 1.0000000000000002 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" 0.36474600000000001 -0.43647000000000002 -0.82246900000000001 0
		 0.072258799999999998 0.89392799999999994 -0.44234699999999999 0 0.92830000000000001 0.101913 0.357595 0
		 52.856034999999999 0.0402022 102.42681 1;
	setAttr -k on ".liw";
	setAttr ".fbxID" 5;
createNode joint -n "Bip01_L_Finger01" -p "|Stand_Talk_5|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_L_Clavicle|Bip01_L_UpperArm|Bip01_L_Forearm|Bip01_L_Hand|Bip01_L_Finger0";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".t" -type "double3" 4.0806 0 0 ;
	setAttr ".r" -type "double3" 0 0 24.06455200000001 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" 0.36250900000000003 -0.034021900000000001 -0.93135900000000005 0
		 -0.082751900000000003 0.99421199999999998 -0.068527000000000005 0 0.92829899999999999 0.101913 0.357595 0
		 54.344436000000002 -1.7408459999999999 99.070606999999995 1;
	setAttr -k on ".liw";
	setAttr ".fbxID" 5;
createNode joint -n "_notused_Bip01_L_Finger0Nub" -p "|Stand_Talk_5|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_L_Clavicle|Bip01_L_UpperArm|Bip01_L_Forearm|Bip01_L_Hand|Bip01_L_Finger0|Bip01_L_Finger01";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".t" -type "double3" 3.3413 0 0 ;
	setAttr ".ssc" no;
	setAttr ".fbxID" 5;
createNode joint -n "Bip01_L_Finger2" -p "|Stand_Talk_5|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_L_Clavicle|Bip01_L_UpperArm|Bip01_L_Forearm|Bip01_L_Hand";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".t" -type "double3" 7.3709 -0.4816 -0.6341 ;
	setAttr ".r" -type "double3" -0.043610000000000031 0.01341 17.086619000000006 ;
	setAttr ".s" -type "double3" 1 0.99999999999999989 0.99999999999999989 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" 0.43393500000000002 -0.045622700000000002 -0.89978899999999995 0
		 -0.90003599999999995 0.022871599999999999 -0.43521399999999999 0 0.040435199999999998 0.99869699999999995 -0.031137399999999999 0
		 57.766060000000003 2.0529269999999999 99.922754999999995 1;
	setAttr -k on ".liw";
	setAttr ".fbxID" 5;
createNode joint -n "Bip01_L_Finger21" -p "|Stand_Talk_5|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_L_Clavicle|Bip01_L_UpperArm|Bip01_L_Forearm|Bip01_L_Hand|Bip01_L_Finger2";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".t" -type "double3" 3.8208 0 0 ;
	setAttr ".r" -type "double3" -4.0000000000000024e-006 -4.0000000000000015e-006 29.946390000000019 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" -0.0732873 -0.0281141 -0.99691399999999997 0 -0.99649100000000002 0.042592999999999999 0.072054999999999994 0
		 0.040435899999999997 0.99869699999999995 -0.031137000000000001 0 59.424058000000002 1.878614 96.484800000000007 1;
	setAttr -k on ".liw";
	setAttr ".fbxID" 5;
createNode joint -n "_notused_Bip01_L_Finger2Nub" -p "|Stand_Talk_5|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_L_Clavicle|Bip01_L_UpperArm|Bip01_L_Forearm|Bip01_L_Hand|Bip01_L_Finger2|Bip01_L_Finger21";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".t" -type "double3" 4.7174000000000005 0 0 ;
	setAttr ".ssc" no;
	setAttr ".fbxID" 5;
createNode joint -n "Bip01_L_Finger1" -p "|Stand_Talk_5|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_L_Clavicle|Bip01_L_UpperArm|Bip01_L_Forearm|Bip01_L_Hand";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".t" -type "double3" 7.3263 -0.211 -2.2784 ;
	setAttr ".r" -type "double3" 0.73310799999999965 5.3145129999999989 22.646742999999997 ;
	setAttr ".s" -type "double3" 1 0.99999999999999989 0.99999999999999989 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" 0.33942 -0.13520399999999999 -0.93086800000000003 0
		 -0.93680799999999997 0.040590800000000003 -0.34748099999999998 0 0.084765400000000005 0.98998600000000003 -0.112883 0
		 57.470143999999998 0.415574 99.821611000000004 1;
	setAttr -k on ".liw";
	setAttr ".fbxID" 5;
createNode joint -n "Bip01_L_Finger11" -p "|Stand_Talk_5|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_L_Clavicle|Bip01_L_UpperArm|Bip01_L_Forearm|Bip01_L_Hand|Bip01_L_Finger1";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".t" -type "double3" 3.5597999999999996 0 0 ;
	setAttr ".r" -type "double3" -2.0000000000000012e-006 3.0000000000000005e-006 22.627677000000009 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" -0.047135499999999997 -0.10918 -0.99290400000000001 0
		 -0.99528499999999998 0.089479600000000006 0.037409299999999999 0 0.0847604 0.98998699999999995 -0.112883 0
		 58.678319000000002 -0.065726099999999996 96.507845000000003 1;
	setAttr -k on ".liw";
	setAttr ".fbxID" 5;
createNode joint -n "_notused_Bip01_L_Finger1Nub" -p "|Stand_Talk_5|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_L_Clavicle|Bip01_L_UpperArm|Bip01_L_Forearm|Bip01_L_Hand|Bip01_L_Finger1|Bip01_L_Finger11";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".t" -type "double3" 4.1404000000000005 0 0 ;
	setAttr ".ssc" no;
	setAttr ".fbxID" 5;
createNode joint -n "Bip01_L_Finger3" -p "|Stand_Talk_5|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_L_Clavicle|Bip01_L_UpperArm|Bip01_L_Forearm|Bip01_L_Hand";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".t" -type "double3" 7.2345 -0.5997 0.9332 ;
	setAttr ".r" -type "double3" -0.043362000000000123 -3.826458 18.509666999999997 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" 0.41327900000000001 0.021958700000000001 -0.91034000000000004 0
		 -0.91051400000000005 0.0239966 -0.41277999999999998 0 0.012781000000000001 0.999471 0.029911 0
		 57.824314000000001 3.6238670000000002 100.055049 1;
	setAttr -k on ".liw";
	setAttr ".fbxID" 5;
createNode joint -n "Bip01_L_Finger31" -p "|Stand_Talk_5|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_L_Clavicle|Bip01_L_UpperArm|Bip01_L_Forearm|Bip01_L_Hand|Bip01_L_Finger3";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".t" -type "double3" 3.7241 0 0 ;
	setAttr ".r" -type "double3" 2e-006 -9.4787915988669346e-023 27.984288 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" -0.062282200000000003 0.030651100000000001 -0.99758800000000003 0
		 -0.99797599999999997 0.0108933 0.062641199999999994 0 0.012787099999999999 0.999471 0.029910599999999999 0
		 59.363363 3.7056490000000002 96.664751999999993 1;
	setAttr -k on ".liw";
	setAttr ".fbxID" 5;
createNode joint -n "_notused_Bip01_L_Finger3Nub" -p "|Stand_Talk_5|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_L_Clavicle|Bip01_L_UpperArm|Bip01_L_Forearm|Bip01_L_Hand|Bip01_L_Finger3|Bip01_L_Finger31";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".t" -type "double3" 4.2397 0 0 ;
	setAttr ".ssc" no;
	setAttr ".fbxID" 5;
createNode joint -n "Bip01_L_Finger4" -p "|Stand_Talk_5|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_L_Clavicle|Bip01_L_UpperArm|Bip01_L_Forearm|Bip01_L_Hand";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".t" -type "double3" 7.2292 -0.4187 2.3639 ;
	setAttr ".r" -type "double3" 0.17713199999999998 -5.5619539999999983 16.615218000000002 ;
	setAttr ".s" -type "double3" 1 0.99999999999999989 1 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" 0.44323200000000001 0.051427599999999997 -0.89493100000000003 0
		 -0.89640600000000004 0.026343800000000001 -0.44244899999999998 0 0.00082181400000000003 0.99832900000000002 0.057776399999999999 0
		 57.746775 5.054627 99.891990000000007 1;
	setAttr -k on ".liw";
	setAttr ".fbxID" 5;
createNode joint -n "Bip01_L_Finger41" -p "|Stand_Talk_5|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_L_Clavicle|Bip01_L_UpperArm|Bip01_L_Forearm|Bip01_L_Hand|Bip01_L_Finger4";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".t" -type "double3" 3.4791000000000003 0 0 ;
	setAttr ".r" -type "double3" 2e-006 -3.0000000000000009e-006 29.444363999999997 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" -0.054672800000000001 0.057735500000000002 -0.996834 0
		 -0.99850399999999995 -0.00234096 0.054628799999999998 0 0.00082047800000000003 0.99832900000000002 0.057777200000000001 0
		 59.288862000000002 5.2335589999999996 96.778389000000004 1;
	setAttr -k on ".liw";
	setAttr ".fbxID" 5;
createNode joint -n "_notused_Bip01_L_Finger4Nub" -p "|Stand_Talk_5|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_L_Clavicle|Bip01_L_UpperArm|Bip01_L_Forearm|Bip01_L_Hand|Bip01_L_Finger4|Bip01_L_Finger41";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".t" -type "double3" 3.3974 0 0 ;
	setAttr ".ssc" no;
	setAttr ".fbxID" 5;
createNode joint -n "Bip01_L_Thigh" -p "|Stand_Talk_5|Bip01_Spine";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".t" -type "double3" -11.0456 2.2584 8.5201999999999991 ;
	setAttr ".s" -type "double3" 0.99999999999999989 0.99999999999999989 0.99999999999999989 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" 0.059694200000000003 0.0067868399999999997 -0.998193 0
		 0.0013515700000000001 -0.99997599999999998 -0.00671814 0 -0.99821599999999999 -0.00094809699999999996 -0.059701999999999998 0
		 8.5202019999999994 4.3064900000000002e-005 88.396292000000003 1;
	setAttr -k on ".liw";
	setAttr ".fbxID" 5;
createNode joint -n "Bip01_L_Calf" -p "|Stand_Talk_5|Bip01_Spine|Bip01_L_Thigh";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".t" -type "double3" 36.1527 0 0 ;
	setAttr ".s" -type "double3" 1 0.99999999999999989 0.99999999999999989 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" 0.059157700000000001 0.119882 -0.99102400000000002 0
		 0.0080950799999999993 -0.99278699999999998 -0.119612 0 -0.99821599999999999 -0.00094644599999999998 -0.059701400000000002 0
		 10.678299000000001 0.245397 52.308965999999998 1;
	setAttr -k on ".liw";
	setAttr ".fbxID" 5;
createNode joint -n "Bip01_L_Foot" -p "|Stand_Talk_5|Bip01_Spine|Bip01_L_Thigh|Bip01_L_Calf";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".t" -type "double3" 43.2543 0 0 ;
	setAttr ".s" -type "double3" 0.99999999999999989 1 1 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" 0 0 -1 0 0.123471 -0.99234800000000001 0 0 -0.99234800000000001 -0.123471 0 0
		 13.237131 5.4308170000000002 9.4429110000000005 1;
	setAttr -k on ".liw";
	setAttr ".fbxID" 5;
createNode joint -n "Bip01_L_Toe0" -p "|Stand_Talk_5|Bip01_Spine|Bip01_L_Thigh|Bip01_L_Calf|Bip01_L_Foot";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".t" -type "double3" 9.4714999999999989 11.2377 0 ;
	setAttr ".r" -type "double3" -5.000000000000003e-006 1.9999999999999995e-006 89.999979999999979 ;
	setAttr ".s" -type "double3" 0.99999999999999989 1 1 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" 0.123471 -0.99234800000000001 0 0 0 0 1 0 -0.99234800000000001 -0.123471 0 0
		 14.624665999999999 -5.7209120000000002 -0.028559000000000001 1;
	setAttr -k on ".liw";
	setAttr ".fbxID" 5;
createNode joint -n "_notused_Bip01_L_Toe0Nub" -p "|Stand_Talk_5|Bip01_Spine|Bip01_L_Thigh|Bip01_L_Calf|Bip01_L_Foot|Bip01_L_Toe0";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".t" -type "double3" 7.2685 0 0 ;
	setAttr ".r" -type "double3" 0 0 180 ;
	setAttr ".ssc" no;
	setAttr ".fbxID" 5;
createNode joint -n "Bip01_R_Thigh" -p "|Stand_Talk_5|Bip01_Spine";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".t" -type "double3" -11.0456 2.2585 -8.5201999999999991 ;
	setAttr ".s" -type "double3" 0.99999999999999956 1 0.99999999999999989 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" -0.059694200000000003 0.0067826700000000002 -0.998193 0
		 -0.00134545 -0.99997599999999998 -0.0067143300000000001 0 -0.99821599999999999 0.00094221699999999997 0.059701900000000002 0
		 -8.5201879999999992 1.40303e-005 88.396298000000002 1;
	setAttr -k on ".liw";
	setAttr ".fbxID" 5;
createNode joint -n "Bip01_R_Calf" -p "|Stand_Talk_5|Bip01_Spine|Bip01_R_Thigh";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".t" -type "double3" 36.1527 0 0 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" -0.059158000000000002 0.11988699999999999 -0.99102400000000002 0
		 -0.0080972100000000005 -0.99278699999999998 -0.119617 0 -0.99821599999999999 0.00094822899999999998 0.059701999999999998 0
		 -10.678288 0.24518599999999999 52.308996999999998 1;
	setAttr -k on ".liw";
	setAttr ".fbxID" 5;
createNode joint -n "Bip01_R_Foot" -p "|Stand_Talk_5|Bip01_Spine|Bip01_R_Thigh|Bip01_R_Calf";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".t" -type "double3" 43.2543 0 0 ;
	setAttr ".s" -type "double3" 1 1 0.99999999999999989 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" 0 0 -1 0 -0.123469 -0.99234800000000001 0 0 -0.99234800000000001 0.123469 0 0
		 -13.237124 5.4308209999999999 9.442933 1;
	setAttr -k on ".liw";
	setAttr ".fbxID" 5;
createNode joint -n "Bip01_R_Toe0" -p "|Stand_Talk_5|Bip01_Spine|Bip01_R_Thigh|Bip01_R_Calf|Bip01_R_Foot";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".t" -type "double3" 9.4714999999999989 11.2377 0 ;
	setAttr ".r" -type "double3" 1.9999999999999991e-006 -1.9999999999999995e-006 89.999986 ;
	setAttr ".s" -type "double3" 0.99999999999999989 0.99999999999999989 1 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" -0.12346799999999999 -0.99234800000000001 0 0 0 0 1 0
		 -0.99234800000000001 0.12346799999999999 0 0 -14.624625999999999 -5.7209019999999997 -0.028538999999999998 1;
	setAttr -k on ".liw";
	setAttr ".fbxID" 5;
createNode joint -n "_notused_Bip01_R_Toe0Nub" -p "|Stand_Talk_5|Bip01_Spine|Bip01_R_Thigh|Bip01_R_Calf|Bip01_R_Foot|Bip01_R_Toe0";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".t" -type "double3" 7.2685 0 0 ;
	setAttr ".ssc" no;
	setAttr ".fbxID" 5;
createNode animCurveTL -n "Bip01_Spine_translateX9";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 87 ".ktv[0:86]"  0.6 3.9557266235351558 1.4 3.9315593242645264
		 2.2 3.896862268447876 3 3.8641812801361088 3.8 3.849236011505127 4.6 3.8493578433990479
		 5.4 3.846969366073608 6.2 3.834928035736084 7 3.8250916004180913 7.8 3.824738740921021
		 8.6 3.8158438205718994 9.4 3.7861120700836186 10.2 3.7555625438690186 11 3.7534072399139409
		 11.8 3.7538738250732422 12.6 3.7428779602050786 13.4 3.7175121307373047 14.2 3.6833028793334961
		 15 3.6497452259063721 15.8 3.619096994400024 16.6 3.5879271030426025 17.4 3.5543513298034668
		 18.2 3.5249180793762207 19 3.5045089721679687 19.8 3.4921655654907227 20.6 3.4820756912231445
		 21.4 3.4642715454101558 22.2 3.4383518695831299 23 3.4219043254852295 23.8 3.4249029159545894
		 24.6 3.4335150718688965 25.4 3.4324209690093994 26.2 3.4191160202026367 27 3.4005374908447266
		 27.8 3.3941874504089355 28.6 3.4196839332580566 29.4 3.4722607135772705 30.2 3.5262889862060547
		 31 3.5719895362854004 31.8 3.6262631416320801 32.6 3.7101039886474609 33.4 3.8170759677886958
		 34.2 3.9259850978851318 35 4.043179988861084 35.8 4.1807880401611328 36.6 4.3226308822631836
		 37.4 4.4502582550048828 38.2 4.5726652145385742 39 4.7128214836120605 39.8 4.8561949729919434
		 40.6 4.9622230529785156 41.4 5.0444731712341309 42.2 5.1396651268005371 43 5.2358212471008301
		 43.8 5.3129458427429199 44.6 5.3786063194274902 45.4 5.4436516761779785 46.2 5.510655403137207
		 47 5.5765295028686523 47.8 5.6295657157897949 48.6 5.6577591896057129 49.4 5.6689434051513672
		 50.2 5.6811342239379883 51 5.6960420608520508 51.8 5.710601806640625 52.6 5.7194299697875977
		 53.4 5.7265110015869141 54.2 5.7402310371398926 55 5.7432894706726074 55.8 5.7257909774780273
		 56.6 5.6921353340148926 57.4 5.6476969718933105 58.2 5.5945005416870117 59 5.5477890968322754
		 59.8 5.5185284614562988 60.6 5.5019822120666504 61.4 5.4913735389709473 62.2 5.4648032188415527
		 63 5.395291805267334 63.8 5.2996320724487305 64.6 5.2173385620117187 65.4 5.147613525390625
		 66.2 5.064417839050293 67 4.9650783538818359 67.8 4.8696656227111816 68.6 4.7932868003845215
		 69.4 4.7296719551086426;
createNode animCurveTL -n "Bip01_Spine_translateY9";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 87 ".ktv[0:86]"  0.6 101.82371520996094 1.4 101.8096160888672
		 2.2 101.78742980957033 3 101.757080078125 3.8 101.72552490234376 4.6 101.7110595703125
		 5.4 101.72132110595705 6.2 101.73860168457033 7 101.74680328369141 7.8 101.7489013671875
		 8.6 101.74896240234376 9.4 101.74391937255859 10.2 101.73355102539062 11 101.72575378417967
		 11.8 101.71868133544922 12.6 101.71401214599608 13.4 101.71014404296876 14.2 101.70445251464844
		 15 101.69759368896484 15.8 101.68624114990234 16.6 101.67331695556641 17.4 101.66197204589844
		 18.2 101.648193359375 19 101.63346862792967 19.8 101.62519073486328 20.6 101.61962890625
		 21.4 101.60958099365234 22.2 101.59752655029295 23 101.58533477783205 23.8 101.57122039794922
		 24.6 101.55963897705078 25.4 101.55398559570312 26.2 101.55004119873048 27 101.54721069335936
		 27.8 101.5490264892578 28.6 101.55389404296876 29.4 101.55728912353516 30.2 101.55736541748048
		 31 101.55354309082033 31.8 101.54799652099608 32.6 101.54624176025392 33.4 101.54624176025392
		 34.2 101.54036712646484 35 101.53117370605467 35.8 101.52477264404295 36.6 101.52073669433594
		 37.4 101.51694488525392 38.2 101.51398468017578 39 101.51521301269533 39.8 101.51708221435548
		 40.6 101.51331329345705 41.4 101.50718688964844 42.2 101.50035858154295 43 101.49188995361328
		 43.8 101.48661041259766 44.6 101.48455047607422 45.4 101.47779083251952 46.2 101.46527099609376
		 47 101.45571136474608 47.8 101.45393371582033 48.6 101.45404052734376 49.4 101.45020294189452
		 50.2 101.44588470458984 51 101.44454956054687 51.8 101.44529724121094 52.6 101.44992828369141
		 53.4 101.45607757568359 54.2 101.46360778808594 55 101.47266387939452 55.8 101.48473358154295
		 56.6 101.49877166748048 57.4 101.51478576660156 58.2 101.53643798828124 59 101.56191253662108
		 59.8 101.58298492431641 60.6 101.59725952148436 61.4 101.60800933837892 62.2 101.61861419677734
		 63 101.63327026367187 63.8 101.65220642089844 64.6 101.66823577880859 65.4 101.67499542236328
		 66.2 101.67655181884766 67 101.68270111083984 67.8 101.69187164306641 68.6 101.69684600830078
		 69.4 101.70573425292967;
createNode animCurveTL -n "Bip01_Spine_translateZ9";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 87 ".ktv[0:86]"  0.6 32.404727935791016 1.4 32.404525756835937
		 2.2 32.42138671875 3 32.432491302490234 3.8 32.440715789794922 4.6 32.461643218994141
		 5.4 32.477897644042969 6.2 32.491310119628906 7 32.536979675292969 7.8 32.59967041015625
		 8.6 32.629661560058594 9.4 32.621982574462891 10.2 32.608428955078125 11 32.622901916503906
		 11.8 32.657321929931641 12.6 32.695316314697266 13.4 32.724342346191406 14.2 32.751564025878906
		 15 32.797908782958984 15.8 32.847320556640625 16.6 32.887454986572266 17.4 32.912040710449219
		 18.2 32.92266845703125 19 32.942493438720703 19.8 32.991161346435547 20.6 33.050823211669922
		 21.4 33.092132568359375 22.2 33.112392425537109 23 33.130950927734375 23.8 33.156658172607422
		 24.6 33.184986114501953 25.4 33.212562561035156 26.2 33.231174468994141 27 33.237430572509766
		 27.8 33.241664886474609 28.6 33.249668121337891 29.4 33.255077362060547 30.2 33.25244140625
		 31 33.247390747070313 31.8 33.2423095703125 32.6 33.220111846923828 33.4 33.173492431640625
		 34.2 33.124317169189453 35 33.080028533935547 35.8 33.019245147705078 36.6 32.938808441162109
		 37.4 32.862758636474609 38.2 32.798114776611328 39 32.7301025390625 39.8 32.656852722167969
		 40.6 32.586063385009766 41.4 32.51861572265625 42.2 32.453224182128906 43 32.385684967041016
		 43.8 32.322925567626953 44.6 32.286472320556641 45.4 32.274696350097656 46.2 32.263134002685547
		 47 32.244125366210937 47.8 32.22283935546875 48.6 32.194263458251953 49.4 32.161300659179688
		 50.2 32.144474029541016 51 32.148590087890625 51.8 32.150276184082031 52.6 32.152072906494141
		 53.4 32.152919769287109 54.2 32.161716461181641 55 32.177127838134766 55.8 32.183788299560547
		 56.6 32.179767608642578 57.4 32.167686462402344 58.2 32.148860931396484 59 32.142269134521484
		 59.8 32.159008026123047 60.6 32.181400299072266 61.4 32.194984436035156 62.2 32.207817077636719
		 63 32.234104156494141 63.8 32.269527435302734 64.6 32.303001403808594 65.4 32.348056793212891
		 66.2 32.414886474609375 67 32.478958129882812 67.8 32.521724700927734 68.6 32.552913665771484
		 69.4 32.585517883300781;
createNode animCurveTA -n "Bip01_Spine_rotateX9";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 87 ".ktv[0:86]"  0.6 47.462226867675781 1.4 47.519496917724609
		 2.2 47.580528259277344 3 47.636913299560547 3.8 47.686416625976563 4.6 47.727287292480469
		 5.4 47.759952545166016 6.2 47.794055938720703 7 47.827896118164063 7.8 47.851688385009766
		 8.6 47.871063232421875 9.4 47.888954162597656 10.2 47.900600433349609 11 47.902027130126953
		 11.8 47.878707885742187 12.6 47.859691619873047 13.4 47.831577301025391 14.2 47.770145416259773
		 15 47.734752655029297 15.8 47.685199737548835 16.6 47.629295349121094 17.4 47.574501037597656
		 18.2 47.518360137939453 19 47.457794189453125 19.8 47.395114898681641 20.6 47.334033966064453
		 21.4 47.277061462402344 22.2 47.223358154296882 23 47.165260314941406 23.8 47.101589202880859
		 24.6 47.042293548583991 25.4 46.988651275634766 26.2 46.934276580810547 27 46.881359100341797
		 27.8 46.832386016845703 28.6 46.780128479003906 29.4 46.720111846923821 30.2 46.656169891357422
		 31 46.589672088623047 31.8 46.518917083740234 32.6 46.444976806640625 33.4 46.367515563964851
		 34.2 46.282318115234375 35 46.188369750976562 35.8 46.089973449707031 36.6 45.991264343261719
		 37.4 45.897350311279297 38.2 45.811119079589837 39 45.723880767822266 39.8 45.628627777099609
		 40.6 45.533771514892578 41.4 45.445922851562507 42.2 45.357212066650391 43 45.26031494140625
		 43.8 45.160484313964851 44.6 45.065738677978509 45.4 44.976921081542969 46.2 44.894569396972656
		 47 44.821327209472656 47.8 44.754367828369141 48.6 44.690570831298835 49.4 44.630474090576179
		 50.2 44.574329376220703 51 44.528385162353523 51.8 44.486408233642578 52.6 44.46612548828125
		 53.4 44.4342041015625 54.2 44.388439178466797 55 44.349822998046875 55.8 44.332462310791016
		 56.6 44.340042114257805 57.4 44.374305725097656 58.2 44.415512084960938 59 44.436649322509766
		 59.8 44.446762084960937 60.6 44.464767456054688 61.4 44.486404418945313 62.2 44.506385803222656
		 63 44.524234771728516 63.8 44.535709381103523 64.6 44.542278289794922 65.4 44.551387786865234
		 66.2 44.57037353515625 67 44.60530090332032 67.8 44.643783569335945 68.6 44.661518096923821
		 69.4 44.669490814208984;
createNode animCurveTA -n "Bip01_Spine_rotateY9";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 87 ".ktv[0:86]"  0.6 2.670628786087037 1.4 2.822169303894043
		 2.2 2.9646036624908447 3 3.0937194824218754 3.8 3.2180373668670659 4.6 3.3360292911529545
		 5.4 3.4351413249969478 6.2 3.5159170627593994 7 3.5942831039428715 7.8 3.6762821674346928
		 8.6 3.7468998432159424 9.4 3.7932152748107906 10.2 3.8433723449707049 11 3.8706841468811031
		 11.8 3.9016847610473628 12.6 3.9041123390197754 13.4 3.8959157466888432 14.2 3.8921258449554439
		 15 3.8743221759796147 15.8 3.8543229103088383 16.6 3.830180406570435 17.4 3.8021299839019775
		 18.2 3.7707581520080571 19 3.734982967376709 19.8 3.6978244781494145 20.6 3.661287784576416
		 21.4 3.6246297359466535 22.2 3.5921590328216557 23 3.567126989364624 23.8 3.5443980693817139
		 24.6 3.5195560455322266 25.4 3.4959731101989751 26.2 3.4775416851043701 27 3.462565660476685
		 27.8 3.4513754844665527 28.6 3.4474780559539799 29.4 3.4489855766296391 30.2 3.4516639709472656
		 31 3.4538581371307373 31.8 3.4560956954956055 32.6 3.4612700939178467 33.4 3.4708967208862305
		 34.2 3.4834799766540527 35 3.4964184761047363 35.8 3.507920503616333 36.6 3.5207126140594478
		 37.4 3.5366086959838867 38.2 3.5523121356964111 39 3.5713472366333008 39.8 3.597916841506958
		 40.6 3.6253199577331547 41.4 3.6496007442474374 42.2 3.6733014583587646 43 3.6972033977508558
		 43.8 3.7208259105682373 44.6 3.7438735961914062 45.4 3.7656099796295166 46.2 3.7875809669494638
		 47 3.8117468357086182 47.8 3.8330264091491695 48.6 3.8438675403594966 49.4 3.8460092544555673
		 50.2 3.8473351001739498 51 3.8473160266876216 51.8 3.8233890533447266 52.6 3.809858083724976
		 53.4 3.8395678997039795 54.2 3.867756605148315 55 3.8834664821624743 55.8 3.8681190013885494
		 56.6 3.8139979839324951 57.4 3.6964750289917001 58.2 3.5208382606506348 59 3.3467919826507568
		 59.8 3.2000930309295654 60.6 3.0460796356201172 61.4 2.8915238380432129 62.2 2.7842123508453369
		 63 2.7094061374664307 63.8 2.6177237033843994 64.6 2.5065228939056396 65.4 2.3979072570800781
		 66.2 2.2797896862030029 67 2.1066153049468994 67.8 1.8954983949661257 68.6 1.7432271242141724
		 69.4 1.6720956563949585;
createNode animCurveTA -n "Bip01_Spine_rotateZ9";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 87 ".ktv[0:86]"  0.6 0.23969939351081851 1.4 0.22026991844177249
		 2.2 0.1897050738334656 3 0.14884020388126373 3.8 0.10743769258260728 4.6 0.068824023008346571
		 5.4 0.040590763092041016 6.2 0.019865667447447777 7 -0.0074158492498099804 7.8 -0.034080732613801956
		 8.6 -0.044896524399518967 9.4 -0.047400493174791343 10.2 -0.068451792001724243 11 -0.086683094501495361
		 11.8 -0.11782640218734745 12.6 -0.13621132075786593 13.4 -0.14345845580101013 14.2 -0.16815005242824554
		 15 -0.18981254100799561 15.8 -0.22134457528591156 16.6 -0.24786502122879028 17.4 -0.26810809969902039
		 18.2 -0.29264459013938909 19 -0.32675883173942566 19.8 -0.36699336767196655 20.6 -0.40740284323692327
		 21.4 -0.44397357106208801 22.2 -0.47881561517715454 23 -0.51871562004089355 23.8 -0.56481200456619263
		 24.6 -0.61168456077575684 25.4 -0.65877723693847656 26.2 -0.70863890647888184 27 -0.75887352228164684
		 27.8 -0.80859684944152832 28.6 -0.86086857318878174 29.4 -0.9155920147895813 30.2 -0.97019833326339733
		 31 -1.0229804515838623 31.8 -1.0727545022964478 32.6 -1.1198166608810425 33.4 -1.167026162147522
		 34.2 -1.2158046960830688 35 -1.264116644859314 35.8 -1.3131998777389526 36.6 -1.370152473449707
		 37.4 -1.4358589649200439 38.2 -1.4988377094268801 39 -1.5525586605072021 39.8 -1.6030904054641724
		 40.6 -1.6529783010482788 41.4 -1.6977447271347046 42.2 -1.7376996278762815 43 -1.7780719995498655
		 43.8 -1.8200151920318604 44.6 -1.8597283363342283 45.4 -1.894250154495239 46.2 -1.9271317720413206
		 47 -1.9647955894470219 47.8 -2.0049293041229248 48.6 -2.0366866588592529 49.4 -2.0556888580322266
		 50.2 -2.0697445869445801 51 -2.081437349319458 51.8 -2.1058902740478516 52.6 -2.1014726161956787
		 53.4 -2.1152024269104004 54.2 -2.1669406890869141 55 -2.2198405265808105 55.8 -2.2364425659179687
		 56.6 -2.2037203311920166 57.4 -2.1090476512908936 58.2 -1.9589293003082269 59 -1.8050850629806521
		 59.8 -1.6946040391921997 60.6 -1.6320270299911499 61.4 -1.5717014074325562 62.2 -1.4571661949157717
		 63 -1.3037469387054443 63.8 -1.1668031215667725 64.6 -1.0535734891891479 65.4 -0.93793594837188732
		 66.2 -0.79775351285934448 67 -0.61592650413513184 67.8 -0.4197658002376557 68.6 -0.28399613499641418
		 69.4 -0.22047697007656095;
createNode animCurveTA -n "Bip01_Spine1_rotateX9";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 87 ".ktv[0:86]"  0.6 0.25918594002723694 1.4 0.23829863965511316
		 2.2 0.21706280112266541 3 0.195704996585846 3.8 0.17451295256614685 4.6 0.15376274287700653
		 5.4 0.13482522964477539 6.2 0.12013009190559389 7 0.091057345271110535 7.8 0.082342348992824554
		 8.6 0.057265505194664001 9.4 0.047342747449874878 10.2 0.038696706295013421 11 0.022464519366621971
		 11.8 0.0097284866496920586 12.6 0.005289924331009388 13.4 0.0010771610541269183 14.2 -0.0055165598168969163
		 15 -0.0091720307245850563 15.8 -0.0097480025142431259 16.6 -0.010035133920609953
		 17.4 -0.0086224554106593132 18.2 -0.00759552838280797 19 -0.0028643074911087751 19.8 -0.00035423878580331802
		 20.6 0.0020742947235703468 21.4 0.0078108045272529125 22.2 0.013936130329966543 23 0.017018260434269905
		 23.8 0.021062655374407768 24.6 0.024900021031498909 25.4 0.027713745832443241 26.2 0.032741259783506393
		 27 0.034410607069730759 27.8 0.036004077643156052 28.6 0.037169083952903741 29.4 0.037755012512207031
		 30.2 0.037809934467077255 31 0.036422796547412872 31.8 0.035520564764738083 32.6 0.031274925917387016
		 33.4 0.028848135843873021 34.2 0.026118526235222816 35 0.018468314781785011 35.8 0.014392968267202377
		 36.6 0.0094024799764156342 37.4 0.0049597714096307755 38.2 -0.0051726461388170719
		 39 -0.0096007930114865303 39.8 -0.014007596299052238 40.6 -0.02406490221619606 41.4 -0.027938321232795715
		 42.2 -0.031560156494379044 43 -0.038781248033046722 43.8 -0.041129570454359055 44.6 -0.043057728558778763
		 45.4 -0.045582316815853119 46.2 -0.045877836644649513 47 -0.044680915772914886 47.8 -0.040620557963848114
		 48.6 -0.037500936537981033 49.4 -0.034166336059570313 50.2 -0.024972040206193924
		 51 -0.015963047742843628 51.8 -0.0053640785627067089 52.6 0.0088578546419739723 53.4 0.014382692985236646
		 54.2 0.035431403666734695 55 0.045490540564060211 55.8 0.055954508483409882 56.6 0.082294672727584839
		 57.4 0.095133870840072646 58.2 0.1116894632577896 59 0.12987722456455231 59.8 0.14853084087371826
		 60.6 0.16753405332565308 61.4 0.18680624663829809 62.2 0.20629166066646576 63 0.22593602538108826
		 63.8 0.24565821886062622 64.6 0.26544278860092163 65.4 0.28527736663818359 66.2 0.30499216914176941
		 67 0.32442444562911987 67.8 0.34344178438186646 68.6 0.36199110746383661 69.4 0.38003614544868469;
createNode animCurveTA -n "Bip01_Spine1_rotateY9";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 87 ".ktv[0:86]"  0.6 0.94677108526229858 1.4 0.99581098556518555
		 2.2 1.041717052459717 3 1.0839793682098389 3.8 1.1221393346786499 4.6 1.155780553817749
		 5.4 1.1834183931350708 6.2 1.2024837732315063 7 1.2318485975265503 7.8 1.2383459806442261
		 8.6 1.249697208404541 9.4 1.2507790327072144 10.2 1.2504297494888306 11 1.2415618896484375
		 11.8 1.2242244482040403 12.6 1.2132080793380735 13.4 1.2017202377319336 14.2 1.1708033084869385
		 15 1.1346161365509031 15.8 1.1164227724075315 16.6 1.095909595489502 17.4 1.0463553667068479
		 18.2 1.0298619270324707 19 0.97667485475540161 19.8 0.95283555984497093 20.6 0.93101620674133301
		 21.4 0.88257992267608643 22.2 0.83399057388305664 23 0.80977320671081532 23.8 0.77684742212295532
		 24.6 0.74404150247573853 25.4 0.7179073691368103 26.2 0.66163831949234009 27 0.6356775164604187
		 27.8 0.60318058729171753 28.6 0.56847512722015381 29.4 0.53589856624603271 30.2 0.50979834794998169
		 31 0.45519781112670898 31.8 0.43756330013275146 32.6 0.38187026977539063 33.4 0.35702100396156311
		 34.2 0.33193862438201904 35 0.27145618200302124 35.8 0.24299068748950961 36.6 0.20952342450618744
		 37.4 0.18067307770252228 38.2 0.117590107023716 39 0.090832173824310303 39.8 0.064131155610084534
		 40.6 0.0016050278209149837 41.4 -0.024343816563487053 42.2 -0.049920003861188889
		 43 -0.1083235889673233 43.8 -0.13188087940216064 44.6 -0.15471677482128143 45.4 -0.20345990359783175
		 46.2 -0.2163921594619751 47 -0.25153845548629761 47.8 -0.28351983428001404 48.6 -0.29578500986099243
		 49.4 -0.30766311287879944 50.2 -0.32798007130622864 51 -0.34027558565139771 51.8 -0.34790760278701782
		 52.6 -0.35191032290458679 53.4 -0.35149279236793524 54.2 -0.34515112638473511 55 -0.33934849500656128
		 55.8 -0.33182886242866516 56.6 -0.30649462342262268 57.4 -0.29095852375030518 58.2 -0.26868695020675659
		 59 -0.24170359969139096 59.8 -0.21108211576938629 60.6 -0.1769460141658783 61.4 -0.13952921330928802
		 62.2 -0.099155895411968217 63 -0.056222006678581238 63.8 -0.01110728085041046 64.6 0.035799048840999603
		 65.4 0.084065847098827362 66.2 0.13315902650356293 67 0.18246641755104065 67.8 0.23139084875583649
		 68.6 0.2793668806552887 69.4 0.3258503675460816;
createNode animCurveTA -n "Bip01_Spine1_rotateZ9";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 87 ".ktv[0:86]"  0.6 0.44705837965011602 1.4 0.4831159412860872
		 2.2 0.51888954639434814 3 0.55391573905944824 3.8 0.58777815103530884 4.6 0.62015712261199951
		 5.4 0.64906406402587891 6.2 0.67096799612045288 7 0.71222013235092163 7.8 0.72402805089950562
		 8.6 0.75641244649887085 9.4 0.76860237121582031 10.2 0.77896952629089355 11 0.79729783535003662
		 11.8 0.81090021133422852 12.6 0.8154202103614806 13.4 0.81969195604324341 14.2 0.82612705230712891
		 15 0.82978421449661244 15.8 0.83053022623062123 16.6 0.83111470937728882 17.4 0.8310554027557373
		 18.2 0.83084118366241455 19 0.82980233430862427 19.8 0.82925522327423107 20.6 0.8287220001220702
		 21.4 0.82754212617874146 22.2 0.82688248157501221 23 0.82682275772094715 23.8 0.82691919803619396
		 24.6 0.82727682590484619 25.4 0.82776188850402832 26.2 0.82946592569351196 27 0.83017528057098389
		 27.8 0.83105289936065685 28.6 0.83204489946365356 29.4 0.83311611413955677 30.2 0.83411294221878052
		 31 0.8365442156791687 31.8 0.83744585514068604 32.6 0.8406413197517395 33.4 0.84220778942108165
		 34.2 0.84388786554336548 35 0.84853285551071167 35.8 0.85104656219482411 36.6 0.8541838526725769
		 37.4 0.85709488391876221 38.2 0.86450469493865967 39 0.86815643310546875 39.8 0.87206923961639404
		 40.6 0.88249611854553212 41.4 0.88738274574279796 42.2 0.89246451854705822 43 0.90512818098068237
		 43.8 0.91058689355850209 44.6 0.91599553823471069 45.4 0.92777985334396373 46.2 0.93089592456817638
		 47 0.93913173675537109 47.8 0.94553774595260609 48.6 0.94704645872116089 49.4 0.94827854633331332
		 50.2 0.94739574193954468 51 0.94345802068710327 51.8 0.93579423427581787 52.6 0.92269062995910656
		 53.4 0.91654932498931885 54.2 0.89038574695587147 55 0.8762776255607605 55.8 0.86077260971069347
		 56.6 0.81807398796081532 57.4 0.79550933837890625 58.2 0.7653632164001466 59 0.73123079538345348
		 59.8 0.69511395692825317 60.6 0.65740150213241577 61.4 0.61853444576263428 62.2 0.57901489734649658
		 63 0.53937435150146484 63.8 0.50010877847671509 64.6 0.46167579293251038 65.4 0.42449840903282166
		 66.2 0.38895967602729797 67 0.35542389750480652 67.8 0.32428377866744995 68.6 0.29584115743637085
		 69.4 0.27021059393882751;
createNode animCurveTA -n "Bip01_Spine2_rotateX9";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 87 ".ktv[0:86]"  0.6 0.89411187171936035 1.4 0.82838648557662964
		 2.2 0.74755358695983887 3 0.62273460626602173 3.8 0.46772044897079479 4.6 0.34768149256706238
		 5.4 0.29436641931533813 6.2 0.28587284684181213 7 0.29328683018684387 7.8 0.29189956188201904
		 8.6 0.26637685298919678 9.4 0.2244730144739151 10.2 0.18178890645504003 11 0.14431636035442352
		 11.8 0.11239518225193024 12.6 0.086048595607280731 13.4 0.064996466040611267 14.2 0.049017030745744705
		 15 0.037922743707895279 15.8 0.031382948160171509 16.6 0.029020188376307487 17.4 0.030476752668619152
		 18.2 0.03536711260676384 19 0.043097559362649918 19.8 0.052882201969623566 20.6 0.064034409821033478
		 21.4 0.076072894036769867 22.2 0.088693931698799133 23 0.10151272267103197 23.8 0.11396327614784242
		 24.6 0.12545216083526611 25.4 0.13552609086036682 26.2 0.14383609592914581 27 0.15007266402244571
		 27.8 0.15412716567516327 28.6 0.15608055889606476 29.4 0.15590488910675049 30.2 0.15338286757469177
		 31 0.14820519089698794 31.8 0.14021299779415131 32.6 0.12946006655693054 33.4 0.11615553498268127
		 34.2 0.10053613781929016 35 0.082778863608837114 35.8 0.063161186873912811 36.6 0.042049221694469459
		 37.4 0.019860327243804932 38.2 -0.0031271018087863922 39 -0.026611818000674248 39.8 -0.050102204084396362
		 40.6 -0.07286158949136734 41.4 -0.094195529818534865 42.2 -0.11356505751609799 43 -0.13051792979240415
		 43.8 -0.14460450410842896 44.6 -0.15537343919277191 45.4 -0.16237726807594299 46.2 -0.16518789529800415
		 47 -0.16322878003120422 47.8 -0.15591931343078613 48.6 -0.1429518461227417 49.4 -0.1244736835360527
		 50.2 -0.10095964372158052 51 -0.073849722743034363 51.8 -0.045707810670137405 52.6 -0.016649441793560982
		 53.4 0.016263909637928009 54.2 0.054012574255466461 55 0.095017038285732269 55.8 0.14245165884494779
		 56.6 0.20518599450588224 57.4 0.28109240531921387 58.2 0.35487931966781616 59 0.42096078395843506
		 59.8 0.48519963026046758 60.6 0.55060309171676636 61.4 0.61686009168624878 62.2 0.68377560377120972
		 63 0.75106489658355713 63.8 0.81851577758789051 64.6 0.88599604368209839 65.4 0.95338004827499412
		 66.2 1.0202103853225708 67 1.0858604907989502 67.8 1.1499607563018801 68.6 1.2123390436172483
		 69.4 1.2728867530822754;
createNode animCurveTA -n "Bip01_Spine2_rotateY9";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 87 ".ktv[0:86]"  0.6 3.1461122035980225 1.4 3.3096363544464111
		 2.2 3.5116667747497559 3 3.8486948013305664 3.8 4.2648916244506836 4.6 4.5383572578430176
		 5.4 4.581207275390625 6.2 4.4765181541442871 7 4.321505069732666 7.8 4.2049694061279297
		 8.6 4.1646194458007804 9.4 4.1619992256164551 10.2 4.1544628143310547 11 4.130950927734375
		 11.8 4.0923595428466797 12.6 4.0398111343383789 13.4 3.9746956825256352 14.2 3.8985478878021231
		 15 3.8129305839538574 15.8 3.7193071842193604 16.6 3.6189777851104736 17.4 3.5132074356079106
		 18.2 3.4032092094421387 19 3.2900865077972412 19.8 3.1747674942016602 20.6 3.058002233505249
		 21.4 2.940427303314209 22.2 2.8225724697113037 23 2.7048466205596924 23.8 2.5875375270843506
		 24.6 2.4708163738250732 25.4 2.3547182083129883 26.2 2.2390916347503662 27 2.1236257553100586
		 27.8 2.008070707321167 28.6 1.8923475742340088 29.4 1.7765045166015627 30.2 1.6604498624801636
		 31 1.5438868999481199 31.8 1.4264752864837646 32.6 1.3079007863998413 33.4 1.1878687143325806
		 34.2 1.0660696029663086 35 0.94232690334320068 35.8 0.81675970554351807 36.6 0.68970030546188354
		 37.4 0.56153303384780884 38.2 0.43261498212814326 39 0.30342346429824829 39.8 0.17459762096405029
		 40.6 0.046907573938369751 41.4 -0.078828826546669006 42.2 -0.20177808403968811 43 -0.32107910513877869
		 43.8 -0.43583428859710693 44.6 -0.54515963792800903 45.4 -0.64826011657714844 46.2 -0.7443433403968811
		 47 -0.83247292041778564 47.8 -0.91170406341552734 48.6 -0.98128247261047374 49.4 -1.0406850576400757
		 50.2 -1.0894246101379397 51 -1.128643274307251 51.8 -1.1618229150772097 52.6 -1.1890153884887695
		 53.4 -1.2052100896835327 54.2 -1.2081736326217651 55 -1.199880838394165 55.8 -1.1731536388397217
		 56.6 -1.1092565059661863 57.4 -1.010999321937561 58.2 -0.90814357995986938 59 -0.811981201171875
		 59.8 -0.71048110723495483 60.6 -0.59719502925872803 61.4 -0.47286361455917358 62.2 -0.33857494592666626
		 63 -0.19564497470855713 63.8 -0.045354261994361877 64.6 0.11100473254919052 65.4 0.27198335528373718
		 66.2 0.43577146530151367 67 0.6003456711769104 67.8 0.76367771625518799 68.6 0.92386734485626187
		 69.4 1.0791018009185791;
createNode animCurveTA -n "Bip01_Spine2_rotateZ9";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 87 ".ktv[0:86]"  0.6 1.5065387487411499 1.4 1.6264694929122925
		 2.2 1.7752636671066284 3 2.0129303932189941 3.8 2.3077278137207031 4.6 2.5224437713623047
		 5.4 2.5959181785583496 6.2 2.5742254257202148 7 2.5196757316589355 7.8 2.4915423393249516
		 8.6 2.5155549049377441 9.4 2.5671863555908203 10.2 2.6179263591766357 11 2.6599783897399902
		 11.8 2.693854808807373 12.6 2.720372200012207 13.4 2.7404012680053711 14.2 2.7547976970672607
		 15 2.764366626739502 15.8 2.7698874473571777 16.6 2.7721748352050781 17.4 2.77215576171875
		 18.2 2.7707769870758057 19 2.768718957901001 19.8 2.7662723064422607 20.6 2.7636401653289795
		 21.4 2.7612271308898926 22.2 2.7594869136810303 23 2.758709192276001 23.8 2.7589869499206543
		 24.6 2.7604362964630127 25.4 2.7630329132080078 26.2 2.7662749290466309 27 2.7695386409759521
		 27.8 2.7725808620452881 28.6 2.7757678031921387 29.4 2.7795217037200928 30.2 2.7839395999908447
		 31 2.7890439033508301 31.8 2.7948217391967773 32.6 2.8013288974761963 33.4 2.808598518371582
		 34.2 2.8168001174926758 35 2.8261315822601318 35.8 2.8366832733154297 36.6 2.8485410213470459
		 37.4 2.8618869781494141 38.2 2.8770470619201665 39 2.8942611217498779 39.8 2.9135913848876953
		 40.6 2.9349856376647949 41.4 2.9583055973052983 42.2 2.9832110404968266 43 3.0091903209686279
		 43.8 3.0356173515319824 44.6 3.0617275238037109 45.4 3.0867011547088623 46.2 3.1097843647003174
		 47 3.1300289630889893 47.8 3.1460802555084229 48.6 3.1563432216644287 49.4 3.1594753265380859
		 50.2 3.1544740200042725 51 3.1419370174407959 51.8 3.1242632865905762 52.6 3.1008296012878418
		 53.4 3.0670709609985352 54.2 3.0210003852844238 55 2.9642241001129155 55.8 2.892723560333252
		 56.6 2.7956962585449219 57.4 2.676127672195435 58.2 2.5534262657165532 59 2.4352061748504639
		 59.8 2.3147757053375244 60.6 2.1891536712646484 61.4 2.0598351955413814 62.2 1.9284796714782717
		 63 1.7968698740005493 63.8 1.6666606664657593 64.6 1.539395809173584 65.4 1.4164654016494751
		 66.2 1.299142599105835 67 1.1886745691299438 67.8 1.0862809419631958 68.6 0.99295544624328602
		 69.4 0.90907222032547008;
createNode animCurveTA -n "Bip01_R_Clavicle_rotateX9";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 87 ".ktv[0:86]"  0.6 -0.6040230393409729 1.4 -0.60557591915130615
		 2.2 -0.60718756914138794 3 -0.60646730661392212 3.8 -0.60379123687744141 4.6 -0.59900319576263428
		 5.4 -0.59694826602935791 6.2 -0.5891801118850708 7 -0.58681076765060425 7.8 -0.5798572301864624
		 8.6 -0.57436031103134155 9.4 -0.56983077526092529 10.2 -0.56635886430740356 11 -0.56426370143890381
		 11.8 -0.56395953893661499 12.6 -0.56618857383728027 13.4 -0.56751823425292969 14.2 -0.57503646612167358
		 15 -0.58672130107879639 15.8 -0.59103685617446899 16.6 -0.609807848930359 17.4 -0.63532555103302002
		 18.2 -0.65131402015686035 19 -0.67519068717956543 19.8 -0.70382040739059448 20.6 -0.73607379198074341
		 21.4 -0.77209824323654175 22.2 -0.81168007850646973 23 -0.85486239194869995 23.8 -0.90143388509750366
		 24.6 -0.95120912790298451 25.4 -1.0039864778518677 26.2 -1.0594347715377808 27 -1.1170574426651003
		 27.8 -1.1764881610870359 28.6 -1.2371829748153689 29.4 -1.2984874248504641 30.2 -1.3598597049713137
		 31 -1.4204411506652832 31.8 -1.4795602560043335 32.6 -1.5366685390472412 33.4 -1.5908794403076172
		 34.2 -1.6415722370147705 35 -1.6881275177001953 35.8 -1.7302484512329102 36.6 -1.7672693729400637
		 37.4 -1.798963785171509 38.2 -1.8251094818115237 39 -1.8449978828430178 39.8 -1.8572683334350579
		 40.6 -1.8710365295410156 41.4 -1.8749654293060303 42.2 -1.8743199110031126 43 -1.867199182510376
		 43.8 -1.8545303344726558 44.6 -1.8328822851181028 45.4 -1.8184050321578979 46.2 -1.796947717666626
		 47 -1.7714732885360718 47.8 -1.743404746055603 48.6 -1.7129015922546389 49.4 -1.6800963878631594
		 50.2 -1.6451669931411743 51 -1.6081569194793699 51.8 -1.5691101551055908 52.6 -1.5280673503875732
		 53.4 -1.4848474264144895 54.2 -1.4396190643310549 55 -1.3922191858291628 55.8 -1.3426792621612549
		 56.6 -1.2912797927856443 57.4 -1.237911581993103 58.2 -1.182707667350769 59 -1.1258996725082395
		 59.8 -1.0677874088287354 60.6 -1.008533239364624 61.4 -0.9485762715339664 62.2 -0.88821244239807151
		 63 -0.82780259847640991 63.8 -0.76779574155807484 64.6 -0.70864397287368774 65.4 -0.65064507722854614
		 66.2 -0.5943530797958374 67 -0.53998959064483643 67.8 -0.48792234063148499 68.6 -0.43842831254005432
		 69.4 -0.39181321859359747;
createNode animCurveTA -n "Bip01_R_Clavicle_rotateY9";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 87 ".ktv[0:86]"  0.6 79.75167083740233 1.4 79.75445556640625
		 2.2 79.757347106933594 3 79.756050109863281 3.8 79.751182556152358 4.6 79.742546081542983
		 5.4 79.738777160644531 6.2 79.724655151367188 7 79.720359802246094 7.8 79.707717895507813
		 8.6 79.697662353515625 9.4 79.689491271972656 10.2 79.683174133300781 11 79.679290771484375
		 11.8 79.67877197265625 12.6 79.682868957519531 13.4 79.685272216796875 14.2 79.698944091796875
		 15 79.720169067382813 15.8 79.728034973144531 16.6 79.762199401855469 17.4 79.808441162109375
		 18.2 79.837486267089844 19 79.880653381347642 19.8 79.932357788085938 20.6 79.990707397460938
		 21.4 80.055496215820327 22.2 80.126686096191406 23 80.203994750976563 23.8 80.287162780761719
		 24.6 80.375839233398438 25.4 80.469474792480469 26.2 80.567436218261719 27 80.668937683105469
		 27.8 80.773109436035156 28.6 80.879013061523438 29.4 80.985504150390625 30.2 81.091506958007812
		 31 81.195671081542983 31.8 81.296829223632812 32.6 81.393898010253906 33.4 81.485679626464844
		 34.2 81.571052551269531 35 81.649154663085938 35.8 81.719345092773438 36.6 81.780975341796875
		 37.4 81.833549499511719 38.2 81.876754760742188 39 81.909492492675781 39.8 81.929641723632813
		 40.6 81.952369689941406 41.4 81.958831787109375 42.2 81.95770263671875 43 81.945854187011719
		 43.8 81.92510986328125 44.6 81.889404296875 45.4 81.865592956542983 46.2 81.830162048339844
		 47 81.788078308105469 47.8 81.741287231445313 48.6 81.6904296875 49.4 81.635581970214844
		 50.2 81.576957702636719 51 81.514701843261719 51.8 81.448722839355469 52.6 81.379066467285156
		 53.4 81.305587768554688 54.2 81.22823333740233 55 81.147003173828125 55.8 81.061843872070298
		 56.6 80.972892761230469 57.4 80.880203247070327 58.2 80.784011840820327 59 80.684539794921875
		 59.8 80.582244873046875 60.6 80.477638244628906 61.4 80.371269226074219 62.2 80.263748168945327
		 63 80.155776977539062 63.8 80.048072814941406 64.6 79.941482543945313 65.4 79.836700439453125
		 66.2 79.734458923339844 67 79.635505676269531 67.8 79.540420532226563 68.6 79.449851989746094
		 69.4 79.364326477050781;
createNode animCurveTA -n "Bip01_R_Clavicle_rotateZ9";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 87 ".ktv[0:86]"  0.6 173.1373291015625 1.4 173.136962890625
		 2.2 173.13658142089844 3 173.13674926757812 3.8 173.13735961914062 4.6 173.13847351074219
		 5.4 173.138916015625 6.2 173.14068603515625 7 173.14122009277344 7.8 173.14279174804687
		 8.6 173.14399719238281 9.4 173.14506530761719 10.2 173.14582824707031 11 173.14628601074219
		 11.8 173.14634704589844 12.6 173.14588928222656 13.4 173.14556884765625 14.2 173.14387512207031
		 15 173.1412353515625 15.8 173.1402587890625 16.6 173.13601684570312 17.4 173.13018798828125
		 18.2 173.12655639648435 19 173.12103271484378 19.8 173.11439514160156 20.6 173.10693359375
		 21.4 173.09843444824219 22.2 173.08908081054687 23 173.07870483398435 23.8 173.06742858886719
		 24.6 173.05525207519531 25.4 173.04214477539062 26.2 173.02818298339844 27 173.01351928710935
		 27.8 172.99813842773435 28.6 172.98220825195312 29.4 172.96589660644534 30.2 172.94923400878906
		 31 172.9325866699219 31.8 172.91612243652344 32.6 172.89988708496094 33.4 172.88432312011719
		 34.2 172.86955261230469 35 172.85585021972656 35.8 172.84323120117187 36.6 172.83210754394531
		 37.4 172.82249450683594 38.2 172.81448364257812 39 172.80833435058594 39.8 172.80453491210935
		 40.6 172.80029296875 41.4 172.799072265625 42.2 172.79925537109375 43 172.80140686035156
		 43.8 172.80535888671875 44.6 172.81202697753906 45.4 172.81651306152344 46.2 172.82307434082031
		 47 172.83087158203125 47.8 172.83930969238281 48.6 172.84843444824219 49.4 172.85816955566406
		 50.2 172.86843872070312 51 172.87925720214844 51.8 172.89054870605469 52.6 172.90225219726562
		 53.4 172.91452026367187 54.2 172.92715454101562 55 172.94029235839844 55.8 172.95391845703125
		 56.6 172.9677734375 57.4 172.98197937011719 58.2 172.99652099609375 59 173.01127624511719
		 59.8 173.02610778808594 60.6 173.04106140136719 61.4 173.05593872070312 62.2 173.07070922851565
		 63 173.08529663085935 63.8 173.09957885742187 64.6 173.11343383789062 65.4 173.12687683105469
		 66.2 173.13967895507812 67 173.15188598632812 67.8 173.1634521484375 68.6 173.17433166503906
		 69.4 173.18446350097659;
createNode animCurveTA -n "Bip01_R_UpperArm_rotateX9";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 87 ".ktv[0:86]"  0.6 -50.145214080810547 1.4 -45.782993316650391
		 2.2 -39.262321472167969 3 -31.178066253662109 3.8 -22.427923202514648 4.6 -13.61935329437256
		 5.4 -6.2589778900146484 6.2 -2.1520774364471436 7 -0.82470440864562988 7.8 -0.15163013339042664
		 8.6 1.0657364130020142 9.4 2.6664650440216064 10.2 4.2105312347412109 11 5.834658145904541
		 11.8 8.2721776962280273 12.6 11.206608772277832 13.4 13.239444732666016 14.2 14.026040077209473
		 15 14.050295829772947 15.8 13.568985939025881 16.6 13.058489799499512 17.4 13.065567970275882
		 18.2 13.435688018798828 19 13.435674667358398 19.8 12.712835311889648 20.6 11.776307106018066
		 21.4 11.187792778015137 22.2 10.85450553894043 23 10.410425186157228 23.8 9.8170385360717773
		 24.6 9.0873003005981445 25.4 7.9245786666870126 26.2 6.4496574401855469 27 4.8974590301513672
		 27.8 2.2663669586181641 28.6 -2.4778540134429932 29.4 -8.4905128479003906 30.2 -13.811578750610352
		 31 -17.000242233276371 31.8 -17.617086410522461 32.6 -15.775620460510256 33.4 -11.761540412902832
		 34.2 -6.2309980392456055 35 -0.32547873258590698 35.8 4.9891633987426758 36.6 9.4650688171386719
		 37.4 13.872556686401367 38.2 19.090391159057617 39 24.380294799804691 39.8 29.153837203979492
		 40.6 34.909698486328125 41.4 41.648365020751953 42.2 47.106056213378906 43 50.624752044677734
		 43.8 52.619991302490234 44.6 52.878524780273445 45.4 51.82366943359375 46.2 50.452430725097663
		 47 49.357452392578125 47.8 48.806919097900391 48.6 48.810699462890625 49.4 49.044448852539063
		 50.2 49.168399810791016 51 49.1219482421875 51.8 49.019466400146477 52.6 48.952613830566406
		 53.4 48.871986389160163 54.2 48.711612701416016 55 48.541473388671875 55.8 48.210720062255859
		 56.6 47.317642211914063 57.4 45.630447387695313 58.2 42.742897033691406 59 37.990554809570313
		 59.8 30.544075012207028 60.6 18.695005416870117 61.4 2.9546151161193848 62.2 -11.893647193908693
		 63 -21.897926330566406 63.8 -27.446100234985352 64.6 -30.303634643554684 65.4 -31.941572189331051
		 66.2 -34.07403564453125 67 -36.795810699462891 67.8 -38.748886108398437 68.6 -39.406925201416016
		 69.4 -39.438480377197266;
createNode animCurveTA -n "Bip01_R_UpperArm_rotateY9";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 87 ".ktv[0:86]"  0.6 -56.705776214599609 1.4 -57.281711578369141
		 2.2 -58.898834228515625 3 -60.935871124267578 3.8 -62.781929016113281 4.6 -64.264930725097656
		 5.4 -65.166778564453125 6.2 -65.313743591308594 7 -65.010833740234375 7.8 -64.722755432128906
		 8.6 -64.616226196289062 9.4 -64.552642822265625 10.2 -64.353042602539063 11 -64.000457763671875
		 11.8 -63.485183715820313 12.6 -62.812015533447266 13.4 -62.208404541015632 14.2 -61.894855499267578
		 15 -61.793087005615234 15.8 -61.680374145507813 16.6 -61.523841857910163 17.4 -61.471904754638672
		 18.2 -61.527439117431641 19 -61.512386322021491 19.8 -61.375701904296854 20.6 -61.281764984130866
		 21.4 -61.341972351074219 22.2 -61.474617004394524 23 -61.585380554199226 23.8 -61.686592102050781
		 24.6 -61.819156646728509 25.4 -61.954647064208977 26.2 -62.035278320312507 27 -61.931991577148438
		 27.8 -61.285789489746094 28.6 -59.842731475830078 29.4 -57.793422698974609 30.2 -55.440704345703125
		 31 -53.086162567138679 31.8 -51.029396057128906 32.6 -49.477394104003906 33.4 -48.50445556640625
		 34.2 -47.915962219238281 35 -47.436626434326179 35.8 -46.795272827148438 36.6 -45.923492431640625
		 37.4 -45.337409973144531 38.2 -45.564601898193359 39 -46.199939727783203 39.8 -46.608295440673821
		 40.6 -47.121791839599609 41.4 -47.941322326660163 42.2 -48.572257995605469 43 -49.021190643310547
		 43.8 -49.540966033935547 44.6 -50.075408935546875 45.4 -50.704048156738281 46.2 -51.445991516113281
		 47 -52.006973266601563 47.8 -52.271308898925781 48.6 -52.37750244140625 49.4 -52.3939208984375
		 50.2 -52.360660552978509 51 -52.329395294189453 51.8 -52.269588470458984 52.6 -52.151966094970703
		 53.4 -52.013980865478509 54.2 -51.864402770996094 55 -51.723762512207031 55.8 -51.801666259765625
		 56.6 -52.255729675292969 57.4 -52.936817169189453 58.2 -53.642719268798835 59 -54.307182312011726
		 59.8 -54.870429992675795 60.6 -54.874435424804687 61.4 -54.184688568115234 62.2 -53.911785125732422
		 63 -55.214756011962891 63.8 -57.88134765625 64.6 -61.005184173583984 65.4 -63.890892028808601
		 66.2 -66.133476257324219 67 -67.80731201171875 67.8 -69.016464233398437 68.6 -69.578712463378906
		 69.4 -69.525787353515625;
createNode animCurveTA -n "Bip01_R_UpperArm_rotateZ9";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 87 ".ktv[0:86]"  0.6 19.71619987487793 1.4 18.753482818603516
		 2.2 17.144857406616214 3 14.454304695129395 3.8 11.098624229431152 4.6 8.0695381164550781
		 5.4 6.1935129165649414 6.2 5.9947400093078613 7 6.8966884613037109 7.8 7.6471648216247532
		 8.6 7.5728087425231907 9.4 6.9830150604248047 10.2 6.5088515281677246 11 6.4878325462341309
		 11.8 6.7632393836975098 12.6 6.9512519836425781 13.4 6.9988002777099609 14.2 7.1358151435852051
		 15 7.390709877014161 15.8 7.6371302604675302 16.6 7.7786226272583008 17.4 7.7001466751098642
		 18.2 7.4624037742614755 19 7.3991332054138184 19.8 7.7344579696655282 20.6 8.2199611663818359
		 21.4 8.5030050277709961 22.2 8.5999927520751953 23 8.7116270065307617 23.8 8.8791399002075195
		 24.6 9.2388429641723633 25.4 10.141354560852053 26.2 11.372127532958984 27 12.165164947509766
		 27.8 12.365751266479492 28.6 12.392794609069824 29.4 12.228890419006348 30.2 11.586054801940918
		 31 10.632468223571776 31.8 9.8627004623413086 32.6 9.6128950119018555 33.4 9.8374900817871094
		 34.2 10.297993659973145 35 11.172904014587404 35.8 12.782957077026367 36.6 14.954169273376465
		 37.4 17.078659057617188 38.2 18.638980865478516 39 19.644542694091797 39.8 20.111629486083984
		 40.6 19.524177551269531 41.4 18.108942031860352 42.2 16.975103378295898 43 16.635766983032227
		 43.8 17.139785766601563 44.6 18.553018569946289 45.4 20.439682006835938 46.2 22.12712287902832
		 47 23.334753036499023 47.8 24.016559600830082 48.6 24.217887878417969 49.4 24.158119201660156
		 50.2 24.092100143432617 51 24.125280380249023 51.8 24.197370529174805 52.6 24.20220947265625
		 53.4 24.170585632324222 54.2 24.189678192138672 55 24.141553878784183 55.8 23.855335235595703
		 56.6 23.364887237548828 57.4 22.890562057495117 58.2 22.75126838684082 59 23.177383422851563
		 59.8 24.266542434692383 60.6 26.116453170776367 61.4 27.436077117919918 62.2 26.491973876953125
		 63 23.445175170898441 63.8 19.560144424438477 64.6 15.750552177429199 65.4 13.132380485534668
		 66.2 12.932287216186523 67 14.306457519531252 67.8 15.368237495422362 68.6 15.564152717590334
		 69.4 15.297438621520996;
createNode animCurveTA -n "Bip01_R_Forearm_rotateX9";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 87 ".ktv[0:86]"  0.6 -2.4703586101531982 1.4 -2.8134205341339111
		 2.2 -3.0617170333862305 3 -3.224215030670166 3.8 -3.3105635643005371 4.6 -3.3207602500915527
		 5.4 -3.2613611221313477 6.2 -3.1469008922576904 7 -3.0135419368743896 7.8 -2.9274754524230957
		 8.6 -2.9116489887237549 9.4 -2.9198801517486572 10.2 -2.9175605773925781 11 -2.8871970176696777
		 11.8 -2.8097271919250488 12.6 -2.7121779918670654 13.4 -2.6428883075714111 14.2 -2.6073763370513916
		 15 -2.5906579494476318 15.8 -2.5843403339385982 16.6 -2.5780131816864018 17.4 -2.564349889755249
		 18.2 -2.5467617511749268 19 -2.532907247543335 19.8 -2.525383472442627 20.6 -2.5204756259918213
		 21.4 -2.5140690803527832 22.2 -2.5057854652404785 23 -2.4977140426635742 23.8 -2.4911625385284424
		 24.6 -2.4887149333953857 25.4 -2.4957578182220459 26.2 -2.5050654411315918 27 -2.5090844631195068
		 27.8 -2.5184004306793213 28.6 -2.5557596683502197 29.4 -2.627105712890625 30.2 -2.7278695106506348
		 31 -2.8587779998779297 31.8 -3.010573148727417 32.6 -3.1529653072357178 33.4 -3.250563383102417
		 34.2 -3.2859947681427002 35 -3.2673904895782471 35.8 -3.2106978893280029 36.6 -3.120072603225708
		 37.4 -2.9979946613311768 38.2 -2.8596122264862061 39 -2.7154507637023926 39.8 -2.5577316284179687
		 40.6 -2.3837549686431885 41.4 -2.2098915576934814 42.2 -2.0545580387115479 43 -1.9314908981323238
		 43.8 -1.8419688940048216 44.6 -1.7753739356994629 45.4 -1.739595890045166 46.2 -1.7428078651428225
		 47 -1.7591829299926758 47.8 -1.7641581296920776 48.6 -1.7587490081787109 49.4 -1.7484405040740969
		 50.2 -1.739434599876404 51 -1.7350414991378784 51.8 -1.7316372394561768 52.6 -1.7278871536254885
		 53.4 -1.7261717319488523 54.2 -1.7235976457595823 55 -1.7143882513046269 55.8 -1.696816682815552
		 56.6 -1.6677746772766111 57.4 -1.6077187061309814 58.2 -1.5041911602020264 59 -1.3615334033966069
		 59.8 -1.1952452659606934 60.6 -1.027849078178406 61.4 -0.85859167575836171 62.2 -0.67407357692718506
		 63 -0.49166965484619146 63.8 -0.33612608909606934 64.6 -0.21721276640892029 65.4 -0.14665523171424866
		 66.2 -0.12489610910415649 67 -0.13408505916595459 67.8 -0.15813057124614716 68.6 -0.18985569477081299
		 69.4 -0.21656654775142672;
createNode animCurveTA -n "Bip01_R_Forearm_rotateY9";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 87 ".ktv[0:86]"  0.6 2.147009134292603 1.4 2.0594477653503418
		 2.2 1.9540139436721802 3 1.8620718717575075 3.8 1.8053761720657353 4.6 1.7987003326416009
		 5.4 1.8383307456970219 6.2 1.9053932428359981 7 1.9718612432479856 7.8 2.0113463401794434
		 8.6 2.0217809677124023 9.4 2.019289493560791 10.2 2.0167407989501953 11 2.0241048336029053
		 11.8 2.0494511127471924 12.6 2.0819196701049805 13.4 2.1063690185546875 14.2 2.1195557117462158
		 15 2.1233704090118408 15.8 2.1217706203460693 16.6 2.1209197044372559 17.4 2.125230073928833
		 18.2 2.1337926387786865 19 2.1420242786407471 19.8 2.1455817222595215 20.6 2.143845796585083
		 21.4 2.1402442455291748 22.2 2.13877272605896 23 2.140413761138916 23.8 2.1433949470520024
		 24.6 2.1452696323394775 25.4 2.1440749168395996 26.2 2.1411042213439941 27 2.1380865573883057
		 27.8 2.1346509456634521 28.6 2.1270408630371103 29.4 2.111210823059082 30.2 2.0836453437805176
		 31 2.0396111011505127 31.8 1.9770079851150513 32.6 1.9060750007629397 33.4 1.8501660823822019
		 34.2 1.829037666320801 35 1.8422755002975464 35.8 1.8778119087219236 36.6 1.9279836416244505
		 37.4 1.985961318016052 38.2 2.0411500930786133 39 2.0883402824401855 39.8 2.1285061836242676
		 40.6 2.1593582630157471 41.4 2.1765060424804687 42.2 2.1802101135253902 43 2.1756513118743896
		 43.8 2.1679995059967041 44.6 2.1582245826721191 45.4 2.1502285003662109 46.2 2.1490283012390137
		 47 2.1508824825286865 47.8 2.1507804393768311 48.6 2.1508803367614746 49.4 2.1506192684173584
		 50.2 2.1503419876098633 51 2.1503784656524658 51.8 2.1499817371368408 52.6 2.1488850116729736
		 53.4 2.1477406024932861 54.2 2.1463942527770996 55 2.1451478004455566 55.8 2.142174243927002
		 56.6 2.1376361846923828 57.4 2.1253845691680908 58.2 2.0983700752258301 59 2.0511238574981689
		 59.8 1.9817451238632202 60.6 1.8963567018508911 61.4 1.7914469242095947 62.2 1.6481697559356687
		 63 1.4683239459991455 63.8 1.2730257511138916 64.6 1.0819243192672727 65.4 0.94178098440170277
		 66.2 0.89408451318740867 67 0.91223156452178955 67.8 0.95697903633117676 68.6 1.0180432796478271
		 69.4 1.0732102394104004;
createNode animCurveTA -n "Bip01_R_Forearm_rotateZ9";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 87 ".ktv[0:86]"  0.6 -89.830657958984375 1.4 -99.162605285644531
		 2.2 -106.27047729492187 3 -111.19031524658205 3.8 -113.91436767578124 4.6 -114.23521423339844
		 5.4 -112.34999084472656 6.2 -108.84613800048828 7 -104.91201782226562 7.8 -102.41568756103516
		 8.6 -101.9290771484375 9.4 -102.15603637695312 10.2 -102.12054443359376 11 -101.29949951171876
		 11.8 -99.145553588867202 12.6 -96.430374145507813 13.4 -94.499671936035156 14.2 -93.511848449707017
		 15 -93.05908203125 15.8 -92.901931762695327 16.6 -92.741073608398438 17.4 -92.367752075195312
		 18.2 -91.876815795898437 19 -91.487983703613281 19.8 -91.280853271484375 20.6 -91.156562805175781
		 21.4 -90.998130798339844 22.2 -90.784805297851563 23 -90.567886352539062 23.8 -90.387023925781236
		 24.6 -90.317398071289063 25.4 -90.505393981933594 26.2 -90.758331298828125 27 -90.873336791992188
		 27.8 -91.128967285156236 28.6 -92.133934020996094 29.4 -94.060935974121094 30.2 -96.815231323242202
		 31 -100.45629882812501 31.8 -104.78598022460936 32.6 -108.98131561279295 33.4 -111.94699096679687
		 34.2 -113.03523254394533 35 -112.4368896484375 35.8 -110.67714691162108 36.6 -107.95034027099608
		 37.4 -104.38981628417967 38.2 -100.46414947509766 39 -96.467315673828125 39.8 -92.180564880371094
		 40.6 -87.529548645019531 41.4 -82.934814453125 42.2 -78.8533935546875 43 -75.622383117675781
		 43.8 -73.266830444335938 44.6 -71.498443603515625 45.4 -70.532302856445327 46.2 -70.606552124023437
		 47 -71.037307739257813 47.8 -71.164443969726563 48.6 -71.026290893554688 49.4 -70.76059722900392
		 50.2 -70.528839111328125 51 -70.41693115234375 51.8 -70.327560424804688 52.6 -70.224693298339844
		 53.4 -70.173011779785156 54.2 -70.098297119140625 55 -69.855239868164062 55.8 -69.387672424316406
		 56.6 -68.619377136230469 57.4 -67.012611389160156 58.2 -64.202957153320313 59 -60.25403976440429
		 59.8 -55.519466400146477 60.6 -50.583423614501953 61.4 -45.358390808105469 62.2 -39.224834442138672
		 63 -32.493942260742187 63.8 -25.928850173950195 64.6 -20.003973007202148 65.4 -15.872051239013674
		 66.2 -14.490887641906738 67 -15.027419090270998 67.8 -16.363080978393555 68.6 -18.177125930786133
		 69.4 -19.794851303100582;
createNode animCurveTA -n "Bip01_L_Clavicle_rotateX9";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 87 ".ktv[0:86]"  0.6 -0.21569281816482544 1.4 -0.21006040275096893
		 2.2 -0.20520071685314176 3 -0.20000703632831571 3.8 -0.19852803647518161 4.6 -0.19398047029972076
		 5.4 -0.19055382907390592 6.2 -0.18759578466415405 7 -0.18690618872642517 7.8 -0.18536509573459625
		 8.6 -0.1849847286939621 9.4 -0.18552207946777344 10.2 -0.18722087144851685 11 -0.1882120817899704
		 11.8 -0.19233135879039764 12.6 -0.19403848052024841 13.4 -0.20012511312961578 14.2 -0.20210617780685425
		 15 -0.20846408605575562 15.8 -0.21439410746097565 16.6 -0.22099961340427399 17.4 -0.2292666286230087
		 18.2 -0.23182229697704315 19 -0.24058675765991211 19.8 -0.24816146492958069 20.6 -0.25536262989044189
		 21.4 -0.26257303357124329 22.2 -0.26975563168525696 23 -0.27681317925453186 23.8 -0.28376194834709167
		 24.6 -0.29088127613067627 25.4 -0.29890298843383789 26.2 -0.30123433470726013 27 -0.30889260768890381
		 27.8 -0.31592452526092535 28.6 -0.31800341606140137 29.4 -0.32435756921768188 30.2 -0.32605654001235962
		 31 -0.32991212606430054 31.8 -0.33248579502105713 32.6 -0.33424139022827148 33.4 -0.33542236685752869
		 34.2 -0.33600792288780212 35 -0.33586758375167847 35.8 -0.33564728498458862 36.6 -0.33417567610740662
		 37.4 -0.33360609412193298 38.2 -0.33147406578063965 39 -0.32920020818710327 39.8 -0.32679879665374756
		 40.6 -0.32402628660202026 41.4 -0.3207278847694397 42.2 -0.31659230589866644 43 -0.31530368328094482
		 43.8 -0.31070464849472046 44.6 -0.30636021494865417 45.4 -0.3012126088142395 46.2 -0.29960238933563232
		 47 -0.29377743601799011 47.8 -0.28759381175041199 48.6 -0.28580740094184875 49.4 -0.27941828966140747
		 50.2 -0.27284961938858032 51 -0.27064961194992065 51.8 -0.26326853036880493 52.6 -0.26117235422134399
		 53.4 -0.25487783551216125 54.2 -0.24903057515621185 55 -0.24739271402359012 55.8 -0.24230347573757172
		 56.6 -0.23846070468425754 57.4 -0.23526924848556524 58.2 -0.23249068856239319 59 -0.23028093576431274
		 59.8 -0.22858886420726776 60.6 -0.22735588252544409 61.4 -0.22664736211299888 62.2 -0.22636821866035461
		 63 -0.22638255357742312 63.8 -0.22693328559398657 64.6 -0.22717231512069705 65.4 -0.22798524796962735
		 66.2 -0.22878283262252808 67 -0.22955591976642611 67.8 -0.23025326430797577 68.6 -0.23078338801860812
		 69.4 -0.23107416927814484;
createNode animCurveTA -n "Bip01_L_Clavicle_rotateY9";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 87 ".ktv[0:86]"  0.6 -78.235183715820327 1.4 -78.245758056640625
		 2.2 -78.255020141601563 3 -78.264785766601591 3.8 -78.267524719238281 4.6 -78.276092529296875
		 5.4 -78.282600402832031 6.2 -78.2882080078125 7 -78.289466857910156 7.8 -78.292381286621094
		 8.6 -78.29315185546875 9.4 -78.29205322265625 10.2 -78.288848876953125 11 -78.287010192871094
		 11.8 -78.279273986816406 12.6 -78.276054382324219 13.4 -78.264488220214844 14.2 -78.260818481445312
		 15 -78.248878479003906 15.8 -78.237625122070327 16.6 -78.22515869140625 17.4 -78.209571838378906
		 18.2 -78.204750061035156 19 -78.188201904296875 19.8 -78.173843383789063 20.6 -78.160278320312514
		 21.4 -78.146652221679673 22.2 -78.13309478759767 23 -78.119720458984375 23.8 -78.106605529785156
		 24.6 -78.093109130859375 25.4 -78.07794189453125 26.2 -78.073585510253906 27 -78.05908203125
		 27.8 -78.04571533203125 28.6 -78.04180908203125 29.4 -78.029701232910156 30.2 -78.026550292968764
		 31 -78.019210815429702 31.8 -78.014350891113295 32.6 -78.01102447509767 33.4 -78.008796691894531
		 34.2 -78.007652282714844 35 -78.007904052734375 35.8 -78.008377075195312 36.6 -78.011146545410156
		 37.4 -78.012237548828125 38.2 -78.01629638671875 39 -78.020477294921875 39.8 -78.02508544921875
		 40.6 -78.030342102050781 41.4 -78.036598205566406 42.2 -78.044479370117188 43 -78.046974182128906
		 43.8 -78.055610656738281 44.6 -78.063873291015625 45.4 -78.073554992675781 46.2 -78.076599121093764
		 47 -78.087562561035156 47.8 -78.099250793457017 48.6 -78.10269927978517 49.4 -78.114830017089844
		 50.2 -78.127174377441406 51 -78.131317138671875 51.8 -78.145317077636719 52.6 -78.14935302734375
		 53.4 -78.161216735839844 54.2 -78.172332763671875 55 -78.175369262695312 55.8 -78.184906005859375
		 56.6 -78.192230224609389 57.4 -78.198257446289062 58.2 -78.203414916992188 59 -78.207618713378906
		 59.8 -78.210853576660156 60.6 -78.213142395019531 61.4 -78.214500427246108 62.2 -78.215095520019531
		 63 -78.214927673339844 63.8 -78.213943481445313 64.6 -78.213554382324233 65.4 -78.211929321289063
		 66.2 -78.21051025390625 67 -78.209075927734375 67.8 -78.207756042480469 68.6 -78.20674896240233
		 69.4 -78.206146240234375;
createNode animCurveTA -n "Bip01_L_Clavicle_rotateZ9";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 87 ".ktv[0:86]"  0.6 173.30825805664062 1.4 173.30717468261719
		 2.2 173.30628967285156 3 173.30528259277344 3.8 173.30500793457031 4.6 173.30412292480469
		 5.4 173.30349731445315 6.2 173.30294799804687 7 173.30281066894531 7.8 173.30250549316409
		 8.6 173.30245971679687 9.4 173.30253601074219 10.2 173.30284118652344 11 173.30307006835935
		 11.8 173.30384826660156 12.6 173.30415344238281 13.4 173.30528259277344 14.2 173.30567932128906
		 15 173.30690002441406 15.8 173.30799865722656 16.6 173.30924987792969 17.4 173.31082153320312
		 18.2 173.311279296875 19 173.31294250488281 19.8 173.31434631347656 20.6 173.31570434570312
		 21.4 173.31704711914065 22.2 173.31840515136719 23 173.3197021484375 23.8 173.32101440429687
		 24.6 173.32232666015625 25.4 173.32380676269534 26.2 173.32427978515625 27 173.32568359375
		 27.8 173.32696533203125 28.6 173.32736206054687 29.4 173.32850646972656 30.2 173.328857421875
		 31 173.32954406738281 31.8 173.33001708984375 32.6 173.33033752441406 33.4 173.33056640625
		 34.2 173.33067321777344 35 173.33062744140625 35.8 173.33061218261719 36.6 173.33033752441406
		 37.4 173.33023071289062 38.2 173.32984924316406 39 173.32939147949219 39.8 173.32896423339844
		 40.6 173.32844543457031 41.4 173.32785034179687 42.2 173.32710266113281 43 173.32688903808594
		 43.8 173.32601928710935 44.6 173.32521057128906 45.4 173.32424926757813 46.2 173.32392883300781
		 47 173.32283020019531 47.8 173.32168579101562 48.6 173.32138061523435 49.4 173.32020568847656
		 50.2 173.3189697265625 51 173.31852722167969 51.8 173.31716918945312 52.6 173.31681823730469
		 53.4 173.31561279296875 54.2 173.31454467773435 55 173.31422424316406 55.8 173.31324768066406
		 56.6 173.31254577636719 57.4 173.31193542480469 58.2 173.3114013671875 59 173.31100463867187
		 59.8 173.31068420410156 60.6 173.31044006347656 61.4 173.31031799316406 62.2 173.31028747558594
		 63 173.31024169921875 63.8 173.31036376953125 64.6 173.31042480468753 65.4 173.310546875
		 66.2 173.31072998046875 67 173.31088256835935 67.8 173.31101989746094 68.6 173.31111145019531
		 69.4 173.31115722656253;
createNode animCurveTA -n "Bip01_L_UpperArm_rotateX9";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 87 ".ktv[0:86]"  0.6 21.617467880249023 1.4 20.854717254638672
		 2.2 20.550167083740231 3 20.126066207885746 3.8 19.879543304443359 4.6 19.819185256958008
		 5.4 19.416864395141602 6.2 18.850568771362305 7 18.704126358032227 7.8 19.033733367919922
		 8.6 19.359949111938477 9.4 19.117889404296875 10.2 18.45014762878418 11 18.265081405639648
		 11.8 18.91887092590332 12.6 19.632835388183594 13.4 19.752452850341797 14.2 19.829063415527344
		 15 20.629281997680664 15.8 21.611923217773441 16.6 21.846761703491211 17.4 21.386344909667969
		 18.2 20.824377059936523 19 20.692295074462891 19.8 21.163091659545895 20.6 21.773220062255859
		 21.4 21.873334884643555 22.2 21.508012771606445 23 21.227787017822266 23.8 21.129920959472656
		 24.6 21.185487747192386 25.4 21.432376861572266 26.2 21.588521957397461 27 21.51848030090332
		 27.8 21.194730758666992 28.6 20.736949920654297 29.4 20.638515472412109 30.2 20.96282958984375
		 31 21.370096206665039 31.8 21.949697494506836 32.6 22.667367935180664 33.4 22.91670036315918
		 34.2 22.698001861572269 35 22.781759262084961 35.8 23.198043823242191 36.6 23.278667449951175
		 37.4 22.825283050537109 38.2 22.241588592529297 39 22.163475036621097 39.8 22.645383834838867
		 40.6 22.84388542175293 41.4 22.468339920043945 42.2 22.199712753295895 43 22.330005645751953
		 43.8 22.510475158691406 44.6 22.534387588500977 45.4 22.480825424194336 46.2 22.522390365600582
		 47 22.845005035400394 47.8 23.263387680053711 48.6 23.261318206787109 49.4 22.886564254760746
		 50.2 22.686580657958984 51 22.729862213134769 51.8 22.822893142700195 52.6 23.02418327331543
		 53.4 23.248804092407227 54.2 23.251737594604492 55 22.944282531738281 55.8 22.462644577026371
		 56.6 22.149728775024418 57.4 22.092018127441406 58.2 22.040491104125977 59 22.065542221069336
		 59.8 22.316745758056641 60.6 22.599237442016602 61.4 22.827970504760746 62.2 22.978496551513672
		 63 22.894027709960937 63.8 22.573257446289066 64.6 22.211172103881836 65.4 22.187496185302734
		 66.2 22.55061149597168 67 22.757844924926761 67.8 22.774101257324219 68.6 23.001245498657227
		 69.4 23.290195465087891;
createNode animCurveTA -n "Bip01_L_UpperArm_rotateY9";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 87 ".ktv[0:86]"  0.6 76.580879211425781 1.4 76.672821044921875
		 2.2 76.703651428222656 3 76.792236328125 3.8 76.927841186523438 4.6 77.013526916503906
		 5.4 77.053489685058594 6.2 77.069046020507813 7 77.039482116699219 7.8 76.991691589355469
		 8.6 76.964950561523452 9.4 76.977119445800781 10.2 77.030616760253906 11 77.073493957519531
		 11.8 77.0400390625 12.6 76.950515747070327 13.4 76.8868408203125 14.2 76.83758544921875
		 15 76.723220825195313 15.8 76.569892883300781 16.6 76.47003173828125 17.4 76.43804931640625
		 18.2 76.442451477050781 19 76.423957824707017 19.8 76.3218994140625 20.6 76.168708801269531
		 21.4 76.0557861328125 22.2 75.999076843261719 23 75.943733215332031 23.8 75.867218017578125
		 24.6 75.74908447265625 25.4 75.56939697265625 26.2 75.382881164550781 27 75.235687255859375
		 27.8 75.142967224121094 28.6 75.098365783691406 29.4 75.034721374511719 30.2 74.926628112792983
		 31 74.793312072753906 31.8 74.62933349609375 32.6 74.472496032714844 33.4 74.374786376953125
		 34.2 74.270950317382812 35 74.069564819335938 35.8 73.813583374023438 36.6 73.625541687011719
		 37.4 73.56585693359375 38.2 73.589897155761719 39 73.609458923339844 39.8 73.575912475585937
		 40.6 73.534439086914063 41.4 73.545097351074219 42.2 73.567955017089844 43 73.551460266113281
		 43.8 73.517578125000014 44.6 73.486915588378906 45.4 73.447410583496094 46.2 73.392181396484375
		 47 73.317756652832031 47.8 73.246795654296875 48.6 73.238716125488281 49.4 73.293899536132812
		 50.2 73.329360961914062 51 73.310592651367188 51.8 73.269111633300781 52.6 73.236038208007813
		 53.4 73.246337890625 54.2 73.31878662109375 55 73.433975219726562 55.8 73.554374694824219
		 56.6 73.6510009765625 57.4 73.738555908203125 58.2 73.830902099609375 59 73.890861511230469
		 59.8 73.924415588378906 60.6 73.991165161132827 61.4 74.101890563964858 62.2 74.22955322265625
		 63 74.346633911132813 63.8 74.463165283203125 64.6 74.612136840820327 65.4 74.726074218750014
		 66.2 74.746574401855469 67 74.751319885253906 67.8 74.780509948730469 68.6 74.809318542480469
		 69.4 74.855232238769531;
createNode animCurveTA -n "Bip01_L_UpperArm_rotateZ9";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 87 ".ktv[0:86]"  0.6 -17.969844818115234 1.4 -18.947725296020508
		 2.2 -19.330612182617188 3 -19.886619567871097 3.8 -20.244041442871097 4.6 -20.347248077392575
		 5.4 -20.846469879150391 6.2 -21.532155990600582 7 -21.67234992980957 7.8 -21.198362350463867
		 8.6 -20.716375350952148 9.4 -20.942037582397461 10.2 -21.707626342773441 11 -21.867391586303711
		 11.8 -20.949054718017575 12.6 -19.913127899169918 13.4 -19.594612121582031 14.2 -19.306373596191406
		 15 -18.067813873291016 15.8 -16.556909561157227 16.6 -15.958290100097656 17.4 -16.207174301147461
		 18.2 -16.557252883911133 19 -16.32935905456543 19.8 -15.281046867370605 20.6 -13.999560356140137
		 21.4 -13.340573310852053 22.2 -13.264758110046388 23 -13.06166934967041 23.8 -12.602795600891112
		 24.6 -11.924008369445801 25.4 -10.973546981811523 26.2 -10.129615783691406 27 -9.5964584350585938
		 27.8 -9.4393386840820312 28.6 -9.5195398330688477 29.4 -9.1883306503295898 30.2 -8.3544502258300781
		 31 -7.453684329986574 31.8 -6.3703002929687509 32.6 -5.172327995300293 33.4 -4.6510734558105469
		 34.2 -4.7595643997192383 35 -4.473752498626709 35.8 -3.777902603149415 36.6 -3.5843007564544687
		 37.4 -4.1763448715209961 38.2 -5.0096840858459473 39 -5.2246518135070801 39.8 -4.7124819755554199
		 40.6 -4.5560770034790039 41.4 -5.1668400764465332 42.2 -5.6513152122497559 43 -5.5895113945007333
		 43.8 -5.4452261924743652 44.6 -5.5063924789428711 45.4 -5.6655769348144531 46.2 -5.7015671730041504
		 47 -5.3806381225585938 47.8 -4.9557943344116211 48.6 -5.1245388984680176 49.4 -5.8317275047302246
		 50.2 -6.3300542831420907 51 -6.5096487998962402 51.8 -6.6374859809875488 52.6 -6.6604485511779785
		 53.4 -6.7099933624267578 54.2 -7.1092171669006348 55 -7.9542808532714835 55.8 -9.0424299240112305
		 56.6 -9.9184637069702166 57.4 -10.475335121154783 58.2 -11.023326873779297 59 -11.433436393737791
		 59.8 -11.514397621154783 60.6 -11.558568000793455 61.4 -11.683383941650392 62.2 -11.903148651123049
		 63 -12.400282859802246 63.8 -13.17970085144043 64.6 -14.007248878479004 65.4 -14.364150047302248
		 66.2 -14.162659645080566 67 -14.13267993927002 67.8 -14.340803146362303 68.6 -14.262111663818359
		 69.4 -14.08629035949707;
createNode animCurveTA -n "Bip01_L_Forearm_rotateX9";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 87 ".ktv[0:86]"  0.6 -0.022418024018406868 1.4 -0.027220005169510841
		 2.2 -0.027990033850073811 3 -0.026789277791976929 3.8 -0.023779084905982018 4.6 -0.021544866263866425
		 5.4 -0.023272920399904251 6.2 -0.026814578101038933 7 -0.028227129951119419 7.8 -0.026822580024600029
		 8.6 -0.024984210729598999 9.4 -0.025451855733990669 10.2 -0.02762637659907341 11 -0.027625089511275291
		 11.8 -0.024107120931148529 12.6 -0.020587610080838203 13.4 -0.020054109394550323
		 14.2 -0.019049642607569695 15 -0.01242744456976652 15.8 -0.0029083762783557177 16.6 0.0018233172595500948
		 17.4 0.00016002240590751171 18.2 -0.003120659152045846 19 -0.0034152879379689693
		 19.8 0.00010403861233498901 20.6 0.0042459573596715927 21.4 0.0049796570092439651
		 22.2 0.0030325900297611952 23 0.0029232031665742397 23.8 0.005914812907576561 24.6 0.011312308721244335
		 25.4 0.01805480569601059 26.2 0.023000568151473999 27 0.024729909375309944 27.8 0.023815227672457695
		 28.6 0.022150248289108276 29.4 0.023387355729937553 30.2 0.027062207460403439 31 0.030174093320965763
		 31.8 0.034304134547710419 32.6 0.040771279484033585 33.4 0.044600062072277069 34.2 0.044424433261156082
		 35 0.045392435044050224 35.8 0.04693884402513504 36.6 0.042466674000024802 37.4 0.030541092157363888
		 38.2 0.017161158844828606 39 0.0096482047811150551 39.8 0.010030649602413176 40.6 0.012974827550351621
		 41.4 0.014206275343894958 42.2 0.014820128679275513 43 0.014821653254330158 43.8 0.012842831201851368
		 44.6 0.010123908519744871 45.4 0.0094922939315438271 46.2 0.012037784792482851 47 0.017229454591870308
		 47.8 0.022214427590370175 48.6 0.022788599133491516 49.4 0.019409742206335068 50.2 0.015783123672008514
		 51 0.01253888662904501 51.8 0.0091927433386445045 52.6 0.0057290378026664266 53.4 0.00028428752557374537
		 54.2 -0.008199695497751236 55 -0.017373889684677124 55.8 -0.024318370968103409 56.6 -0.027773626148700714
		 57.4 -0.028785340487957001 58.2 -0.029400167986750599 59 -0.029832346364855766 59.8 -0.029604556038975712
		 60.6 -0.029622966423630714 61.4 -0.030051512643694881 62.2 -0.030535990372300151
		 63 -0.031575873494148254 63.8 -0.033363007009029388 64.6 -0.035156358033418655 65.4 -0.035320837050676346
		 66.2 -0.033113259822130203 67 -0.03035157173871994 67.8 -0.028938053175806999 68.6 -0.029140589758753777
		 69.4 -0.030687203630805016;
createNode animCurveTA -n "Bip01_L_Forearm_rotateY9";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 87 ".ktv[0:86]"  0.6 -0.45040568709373474 1.4 -0.43944898247718805
		 2.2 -0.4418140053749085 3 -0.44408503174781799 3.8 -0.4535318017005921 4.6 -0.46331214904785156
		 5.4 -0.45697814226150513 6.2 -0.4409404993057251 7 -0.43143400549888611 7.8 -0.43431875109672546
		 8.6 -0.44356361031532288 9.4 -0.44518962502479553 10.2 -0.43621253967285151 11 -0.43425807356834417
		 11.8 -0.45018181204795843 12.6 -0.46929022669792175 13.4 -0.47569373250007629 14.2 -0.48038762807846069
		 15 -0.50156331062316895 15.8 -0.5277634859085083 16.6 -0.5376085638999939 17.4 -0.53240203857421875
		 18.2 -0.52464884519577026 19 -0.52539658546447754 19.8 -0.54015284776687622 20.6 -0.56026393175125122
		 21.4 -0.5697898268699646 22.2 -0.56799882650375366 23 -0.5670202374458313 23.8 -0.56926655769348145
		 24.6 -0.57654643058776867 25.4 -0.59114766120910656 26.2 -0.60462385416030884 27 -0.61294740438461304
		 27.8 -0.6124071478843689 28.6 -0.60241210460662842 29.4 -0.59905803203582764 30.2 -0.60815697908401489
		 31 -0.62096792459487915 31.8 -0.6401362419128418 32.6 -0.66326141357421875 33.4 -0.66931575536727905
		 34.2 -0.65869283676147461 35 -0.65824031829833984 35.8 -0.67099952697753906 36.6 -0.67405736446380615
		 37.4 -0.65796738862991333 38.2 -0.63555526733398438 39 -0.627308189868927 39.8 -0.63434725999832153
		 40.6 -0.631317138671875 41.4 -0.61032450199127197 42.2 -0.59396994113922119 43 -0.59300428628921509
		 43.8 -0.59510868787765503 44.6 -0.59076392650604248 45.4 -0.58152753114700317 46.2 -0.57446086406707764
		 47 -0.57833343744277954 47.8 -0.58682483434677124 48.6 -0.58154278993606567 49.4 -0.56479746103286743
		 50.2 -0.55325686931610107 51 -0.54730129241943359 51.8 -0.54310965538024902 52.6 -0.54400736093521118
		 53.4 -0.54335540533065796 54.2 -0.52962005138397217 55 -0.5021635890007019 55.8 -0.4708915650844574
		 56.6 -0.45018836855888372 57.4 -0.44305303692817688 58.2 -0.43817436695098883 59 -0.43474939465522766
		 59.8 -0.43654918670654297 60.6 -0.43640348315238953 61.4 -0.43298599123954779 62.2 -0.4290081262588501
		 63 -0.42034453153610229 63.8 -0.40468183159828186 64.6 -0.38690245151519786 65.4 -0.3791167140007019
		 66.2 -0.38367351889610291 67 -0.38662302494049072 67.8 -0.38848185539245605 68.6 -0.39815583825111395
		 69.4 -0.40659314393997192;
createNode animCurveTA -n "Bip01_L_Forearm_rotateZ9";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 87 ".ktv[0:86]"  0.6 -2.1632125377655029 1.4 -1.8588162660598753
		 2.2 -1.9183840751647947 3 -1.9817126989364622 3.8 -2.2387666702270508 4.6 -2.5023257732391357
		 5.4 -2.3306365013122559 6.2 -1.8993314504623413 7 -1.6461243629455566 7.8 -1.7263550758361816
		 8.6 -1.9743645191192627 9.4 -2.0148637294769287 10.2 -1.77299964427948 11 -1.7219691276550293
		 11.8 -2.1499454975128178 12.6 -2.6616585254669189 13.4 -2.8309862613677979 14.2 -2.9572947025299072
		 15 -3.5367612838745117 15.8 -4.2612266540527344 16.6 -4.5395035743713379 17.4 -4.3962588310241699
		 18.2 -4.1782078742980957 19 -4.1959004402160645 19.8 -4.5953884124755859 20.6 -5.1366376876831055
		 21.4 -5.3870368003845215 22.2 -5.3308353424072275 23 -5.3052835464477539 23.8 -5.3790745735168457
		 24.6 -5.5969972610473633 25.4 -6.0130367279052734 26.2 -6.3894157409667969 27 -6.6135864257812509
		 27.8 -6.5934982299804687 28.6 -6.3272967338562012 29.4 -6.2505006790161133 30.2 -6.5083041191101074
		 31 -6.8561892509460449 31.8 -7.3728251457214364 32.6 -8.0069150924682617 33.4 -8.1894540786743164
		 34.2 -7.9205112457275391 35 -7.9171552658081064 35.8 -8.2511978149414062 36.6 -8.2964792251586914
		 37.4 -7.8062243461608878 38.2 -7.153012752532959 39 -6.9031891822814941 39.8 -7.0876545906066895
		 40.6 -7.0233416557312012 41.4 -6.4861054420471191 42.2 -6.0657410621643066 43 -6.0401477813720712
		 43.8 -6.08310890197754 44.6 -5.9559779167175293 45.4 -5.7149405479431152 46.2 -5.5486612319946289
		 47 -5.6804628372192383 47.8 -5.9312577247619629 48.6 -5.8006916046142578 49.4 -5.3501181602478027
		 50.2 -5.030174732208252 51 -4.8550996780395508 51.8 -4.7258238792419434 52.6 -4.728264331817627
		 53.4 -4.6821599006652832 54.2 -4.2850785255432129 55 -3.5326347351074219 55.8 -2.6905696392059326
		 56.6 -2.1381886005401611 57.4 -1.9480304718017578 58.2 -1.8185611963272095 59 -1.727703332901001
		 59.8 -1.7752138376235964 60.6 -1.7714029550552368 61.4 -1.6806329488754272 62.2 -1.5751755237579346
		 63 -1.3450411558151243 63.8 -0.9294096231460568 64.6 -0.45866569876670832 65.4 -0.25458014011383057
		 66.2 -0.38134509325027466 67 -0.46862059831619263 67.8 -0.52234506607055664 68.6 -0.77440935373306274
		 69.4 -0.98889458179473877;
createNode animCurveTA -n "Bip01_L_Thigh_rotateX9";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 87 ".ktv[0:86]"  0.6 -0.1584877073764801 1.4 -0.16526325047016144
		 2.2 -0.15285962820053101 3 -0.15717630088329315 3.8 -0.15438959002494812 4.6 -0.14619335532188416
		 5.4 -0.14794188737869263 6.2 -0.1494172066450119 7 -0.14854294061660769 7.8 -0.15324211120605469
		 8.6 -0.15772272646427157 9.4 -0.14906203746795654 10.2 -0.15152090787887573 11 -0.15878823399543762
		 11.8 -0.16078265011310575 12.6 -0.16427969932556152 13.4 -0.16701178252696991 14.2 -0.17791275680065155
		 15 -0.1740058958530426 15.8 -0.17892362177371979 16.6 -0.18905963003635409 17.4 -0.18692860007286072
		 18.2 -0.18373207747936249 19 -0.18433313071727753 19.8 -0.1852620393037796 20.6 -0.19493359327316284
		 21.4 -0.19747442007064819 22.2 -0.19709192216396332 23 -0.1954253613948822 23.8 -0.20053434371948239
		 24.6 -0.20127201080322263 25.4 -0.19318506121635437 26.2 -0.20138128101825711 27 -0.20367622375488281
		 27.8 -0.20291124284267423 28.6 -0.19635426998138428 29.4 -0.19990596175193787 30.2 -0.1965181976556778
		 31 -0.19460573792457581 31.8 -0.18854053318500519 32.6 -0.19149117171764377 33.4 -0.19039833545684817
		 34.2 -0.19138188660144809 35 -0.1908901184797287 35.8 -0.18482491374015808 36.6 -0.17485283315181732
		 37.4 -0.1708366870880127 38.2 -0.16121977567672727 39 -0.16318687796592712 39.8 -0.16378793120384216
		 40.6 -0.15515457093715668 41.4 -0.15291427075862885 42.2 -0.1400735080242157 43 -0.135128453373909
		 43.8 -0.13302475214004517 44.6 -0.13419954478740692 45.4 -0.14111170172691345 46.2 -0.14089313149452207
		 47 -0.13133087754249573 47.8 -0.11870867758989334 48.6 -0.11991079151630402 49.4 -0.11608588695526124
		 50.2 -0.11862672120332718 51 -0.11127743870019911 51.8 -0.1124795526266098 52.6 -0.110840305685997
		 53.4 -0.11114083230495451 54.2 -0.10242550820112228 55 -0.11094958335161208 55.8 -0.11835350841283795
		 56.6 -0.11340845376253128 57.4 -0.11575803905725481 58.2 -0.12034792453050612 59 -0.11373630166053773
		 59.8 -0.12062113732099534 60.6 -0.12269750982522964 61.4 -0.12750595808029175 62.2 -0.134991854429245
		 63 -0.13837961852550509 63.8 -0.15261374413967133 64.6 -0.14698566496372223 65.4 -0.15919804573059082
		 66.2 -0.15799592435359958 67 -0.16362400352954865 67.8 -0.16564573347568512 68.6 -0.17078204452991488
		 69.4 -0.17209343612194061;
createNode animCurveTA -n "Bip01_L_Thigh_rotateY9";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 87 ".ktv[0:86]"  0.6 -181.33653259277344 1.4 -181.43724060058591
		 2.2 -181.51243591308591 3 -181.54600524902344 3.8 -181.57733154296875 4.6 -181.61460876464844
		 5.4 -181.65721130371097 6.2 -181.71127319335935 7 -181.73411560058591 7.8 -181.73347473144531
		 8.6 -181.75373840332034 9.4 -181.78865051269531 10.2 -181.80007934570312 11 -181.78128051757815
		 11.8 -181.75617980957031 12.6 -181.730712890625 13.4 -181.70243835449219 14.2 -181.65309143066409
		 15 -181.58052062988281 15.8 -181.50215148925781 16.6 -181.43571472167969 17.4 -181.37767028808591
		 18.2 -181.32472229003903 19 -181.27391052246097 19.8 -181.20101928710935 20.6 -181.1041259765625
		 21.4 -181.01155090332031 22.2 -180.95606994628903 23 -180.90702819824219 23.8 -180.84584045410156
		 24.6 -180.78375244140625 25.4 -180.72187805175781 26.2 -180.65432739257812 27 -180.58900451660156
		 27.8 -180.53761291503903 28.6 -180.51499938964844 29.4 -180.50907897949219 30.2 -180.49786376953125
		 31 -180.51022338867187 31.8 -180.52357482910159 32.6 -180.55282592773435 33.4 -180.60545349121097
		 34.2 -180.66510009765625 35 -180.73748779296875 35.8 -180.82514953613281 36.6 -180.91070556640625
		 37.4 -180.96751403808591 38.2 -181.02516174316409 39 -181.11537170410156 39.8 -181.22895812988281
		 40.6 -181.30972290039068 41.4 -181.37814331054687 42.2 -181.46058654785156 43 -181.54409790039065
		 43.8 -181.59796142578125 44.6 -181.64573669433591 45.4 -181.69866943359375 46.2 -181.74040222167969
		 47 -181.76730346679687 47.8 -181.771240234375 48.6 -181.77421569824219 49.4 -181.778564453125
		 50.2 -181.78030395507812 51 -181.77484130859375 51.8 -181.76458740234375 52.6 -181.77273559570312
		 53.4 -181.79045104980469 54.2 -181.77806091308591 55 -181.72694396972656 55.8 -181.68693542480469
		 56.6 -181.67559814453125 57.4 -181.64558410644531 58.2 -181.55601501464844 59 -181.47425842285156
		 59.8 -181.41687011718753 60.6 -181.33590698242187 61.4 -181.24951171875003 62.2 -181.23007202148435
		 63 -181.23208618164065 63.8 -181.18544006347656 64.6 -181.12664794921875 65.4 -181.08218383789065
		 66.2 -181.01960754394531 67 -180.92997741699219 67.8 -180.83410644531253 68.6 -180.76242065429688
		 69.4 -180.70761108398435;
createNode animCurveTA -n "Bip01_L_Thigh_rotateZ9";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 87 ".ktv[0:86]"  0.6 2.7058064937591553 1.4 2.819857120513916
		 2.2 2.931954145431519 3 3.0566461086273193 3.8 3.2189860343933105 4.6 3.5249238014221191
		 5.4 3.7494184970855713 6.2 3.6833021640777583 7 3.6490421295166016 7.8 3.8145511150360112
		 8.6 3.9128921031951904 9.4 3.7775042057037358 10.2 3.6112027168273926 11 3.5772020816802983
		 11.8 3.6495883464813232 12.6 3.7109644412994385 13.4 3.6923043727874751 14.2 3.6419658660888672
		 15 3.6052742004394536 15.8 3.615014076232911 16.6 3.6183061599731454 17.4 3.6157789230346671
		 18.2 3.6308326721191402 19 3.7086286544799818 19.8 3.7815885543823242 20.6 3.8260941505432138
		 21.4 3.7596228122711177 22.2 3.6232647895813006 23 3.6041541099548344 23.8 3.7663028240203857
		 24.6 3.9396529197692871 25.4 3.9620969295501713 26.2 3.8998601436614995 27 3.7707424163818359
		 27.8 3.6871135234832759 28.6 3.8326785564422607 29.4 4.04998779296875 30.2 4.0536899566650391
		 31 4.0529656410217285 31.8 4.0991239547729492 32.6 4.0735654830932617 33.4 4.0495643615722656
		 34.2 4.2430906295776367 35 4.5203690528869629 35.8 4.7073249816894531 36.6 4.8492426872253418
		 37.4 4.9618864059448242 38.2 4.9915838241577157 39 5.052741527557373 39.8 5.1446347236633301
		 40.6 5.2298483848571777 41.4 5.2641630172729492 42.2 5.2481393814086914 43 5.2083740234375
		 43.8 5.1350998878479004 44.6 5.1060714721679687 45.4 5.2001094818115234 46.2 5.4159574508667001
		 47 5.6202893257141113 47.8 5.6694393157958984 48.6 5.6192646026611328 49.4 5.6209173202514648
		 50.2 5.7600350379943848 51 5.93316650390625 51.8 5.9621810913085938 52.6 5.9519085884094238
		 53.4 6.0767507553100595 54.2 6.3916087150573739 55 6.5904903411865234 55.8 6.5031318664550781
		 56.6 6.294497013092041 57.4 6.1262693405151367 58.2 5.8297300338745126 59 5.5224671363830566
		 59.8 5.3562202453613281 60.6 5.2515411376953125 61.4 5.0263495445251465 62.2 4.7010278701782227
		 63 4.3943114280700684 63.8 4.1852936744689941 64.6 4.0364503860473633 65.4 3.8865277767181401
		 66.2 3.7419190406799312 67 3.5441849231719971 67.8 3.2862224578857422 68.6 3.0822455883026127
		 69.4 2.9834811687469478;
createNode animCurveTA -n "Bip01_L_Calf_rotateX9";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 87 ".ktv[0:86]"  0.6 -0.055901523679494858 1.4 -0.055331047624349594
		 2.2 -0.054817859083414078 3 -0.054913058876991272 3.8 -0.056178174912929535 4.6 -0.060871370136737823
		 5.4 -0.063981391489505768 6.2 -0.060227964073419571 7 -0.056639526039361947 7.8 -0.058446317911148071
		 8.6 -0.059441670775413506 9.4 -0.055597260594367981 10.2 -0.050704389810562134 11 -0.048681873828172698
		 11.8 -0.049219679087400429 12.6 -0.050045985728502274 13.4 -0.049685467034578323
		 14.2 -0.048435322940349579 15 -0.047507859766483307 15.8 -0.047552008181810379 16.6 -0.048072781413793578
		 17.4 -0.048872735351324081 18.2 -0.050245899707078934 19 -0.052206870168447495 19.8 -0.054377846419811249
		 20.6 -0.055306211113929749 21.4 -0.053517173975706107 22.2 -0.049901667982339859
		 23 -0.048978019505739219 23.8 -0.052870709449052811 24.6 -0.056571938097476952 25.4 -0.05675847455859185
		 26.2 -0.054513037204742432 27 -0.050023037940263741 27.8 -0.04694347083568573 28.6 -0.049449525773525245
		 29.4 -0.052869636565446854 30.2 -0.051774784922599792 31 -0.04944276437163353 31.8 -0.048495072871446609
		 32.6 -0.04541398212313652 33.4 -0.042238827794790275 34.2 -0.044799752533435822 35 -0.049783904105424881
		 35.8 -0.052058126777410507 36.6 -0.05280357226729393 37.4 -0.052711281925439835 38.2 -0.050949238240718842
		 39 -0.049391649663448334 39.8 -0.049159400165081024 40.6 -0.048995334655046463 41.4 -0.047657337039709091
		 42.2 -0.045262061059474945 43 -0.041490841656923294 43.8 -0.037776518613100045 44.6 -0.034679792821407318
		 45.4 -0.035153571516275406 46.2 -0.038656719028949738 47 -0.041916809976100922 47.8 -0.041872624307870865
		 48.6 -0.039564263075590134 49.4 -0.03893449530005455 50.2 -0.042549729347229011 51 -0.046560384333133698
		 51.8 -0.046984236687421799 52.6 -0.046482902020215981 53.4 -0.049285095185041421
		 54.2 -0.055635984987020493 55 -0.059728700667619705 55.8 -0.057079281657934189 56.6 -0.053719446063041687
		 57.4 -0.054271355271339417 58.2 -0.054758023470640182 59 -0.054310854524374008 59.8 -0.055274918675422675
		 60.6 -0.056351143866777427 61.4 -0.054749704897403717 62.2 -0.050361741334199905
		 63 -0.047078188508749008 63.8 -0.04684925451874733 64.6 -0.047606289386749268 65.4 -0.048300143331289291
		 66.2 -0.049616385251283646 67 -0.051559999585151672 67.8 -0.053549874573945999 68.6 -0.055014554411172867
		 69.4 -0.054681189358234406;
createNode animCurveTA -n "Bip01_L_Calf_rotateY9";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 87 ".ktv[0:86]"  0.6 -1.0538966655731199 1.4 -1.0538252592086792
		 2.2 -1.0537928342819214 3 -1.0538214445114136 3.8 -1.0539288520812988 4.6 -1.0541204214096067
		 5.4 -1.0542086362838743 6.2 -1.0540484189987185 7 -1.0538632869720459 7.8 -1.053946852684021
		 8.6 -1.0539885759353638 9.4 -1.0538153648376465 10.2 -1.0536071062088013 11 -1.053513765335083
		 11.8 -1.0535460710525513 12.6 -1.0535871982574463 13.4 -1.0535743236541748 14.2 -1.0534803867340088
		 15 -1.0534403324127195 15.8 -1.0534337759017944 16.6 -1.053491473197937 17.4 -1.0535362958908081
		 18.2 -1.0536258220672607 19 -1.0536770820617678 19.8 -1.0538103580474854 20.6 -1.0538722276687622
		 21.4 -1.0538123846054075 22.2 -1.0536254644393921 23 -1.0535756349563601 23.8 -1.0538110733032229
		 24.6 -1.0539772510528564 25.4 -1.0539991855621338 26.2 -1.0538923740386963 27 -1.053659200668335
		 27.8 -1.0535067319869995 28.6 -1.0536507368087769 29.4 -1.0538150072097778 30.2 -1.0537872314453125
		 31 -1.0536481142044067 31.8 -1.0535908937454224 32.6 -1.05342698097229 33.4 -1.0532510280609133
		 34.2 -1.053418755531311 35 -1.0537328720092771 35.8 -1.0538476705551147 36.6 -1.0538486242294312
		 37.4 -1.0538284778594973 38.2 -1.0537861585617063 39 -1.0536860227584841 39.8 -1.0536806583404541
		 40.6 -1.0536469221115112 41.4 -1.053564786911011 42.2 -1.0534639358520508 43 -1.0532177686691284
		 43.8 -1.0530190467834473 44.6 -1.0528277158737185 45.4 -1.0528419017791748 46.2 -1.0530389547348022
		 47 -1.0532437562942505 47.8 -1.0532901287078855 48.6 -1.0531582832336426 49.4 -1.0530873537063601
		 50.2 -1.0533239841461182 51 -1.053554892539978 51.8 -1.0535769462585447 52.6 -1.0535334348678589
		 53.4 -1.0536895990371704 54.2 -1.0539889335632324 55 -1.0542234182357788 55.8 -1.0540847778320312
		 56.6 -1.053901195526123 57.4 -1.0538979768753052 58.2 -1.0539863109588623 59 -1.0539895296096802
		 59.8 -1.0540270805358889 60.6 -1.0540560483932495 61.4 -1.0540096759796145 62.2 -1.0537800788879397
		 63 -1.0535843372344973 63.8 -1.0535516738891604 64.6 -1.0535717010498049 65.4 -1.0536304712295532
		 66.2 -1.0537132024765017 67 -1.0538052320480349 67.8 -1.0539224147796633 68.6 -1.0540010929107666
		 69.4 -1.0539915561676023;
createNode animCurveTA -n "Bip01_L_Calf_rotateZ9";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 87 ".ktv[0:86]"  0.6 -0.30243697762489319 1.4 -0.30533131957054144
		 2.2 -0.33239707350730896 3 -0.35199099779129028 3.8 -0.30017471313476563 4.6 0.029250184074044228
		 5.4 0.27623394131660461 6.2 0.028762836009263992 7 -0.19492310285568237 7.8 -0.075671009719371796
		 8.6 -0.00018555707356426865 9.4 -0.27009442448616028 10.2 -0.62751621007919312 11 -0.78061622381210316
		 11.8 -0.74673688411712635 12.6 -0.68887126445770264 13.4 -0.71796190738677979 14.2 -0.77859246730804443
		 15 -0.85007822513580322 15.8 -0.84283947944641124 16.6 -0.83021926879882813 17.4 -0.78010338544845581
		 18.2 -0.69881445169448853 19 -0.51970940828323364 19.8 -0.39702180027961725 20.6 -0.34998291730880737
		 21.4 -0.49369731545448303 22.2 -0.7352370023727417 23 -0.79595285654068004 23.8 -0.56271761655807495
		 24.6 -0.29944118857383728 25.4 -0.30063822865486145 26.2 -0.45443364977836609 27 -0.75076580047607422
		 27.8 -0.97333574295043956 28.6 -0.81205636262893677 29.4 -0.56771272420883179 30.2 -0.66338002681732178
		 31 -0.80777406692504883 31.8 -0.87134855985641468 32.6 -1.0832604169845581 33.4 -1.3064864873886108
		 34.2 -1.1485083103179932 35 -0.84115368127822876 35.8 -0.68833786249160767 36.6 -0.60411816835403442
		 37.4 -0.59662139415740967 38.2 -0.75726234912872314 39 -0.85094028711318981 39.8 -0.87138450145721436
		 40.6 -0.86011147499084484 41.4 -0.94237929582595825 42.2 -1.1327219009399414 43 -1.3694896697998049
		 43.8 -1.6483472585678101 44.6 -1.8723970651626587 45.4 -1.8240032196044915 46.2 -1.5573328733444214
		 47 -1.3420480489730835 47.8 -1.3850443363189695 48.6 -1.551321268081665 49.4 -1.5688587427139284
		 50.2 -1.3325704336166382 51 -1.0601050853729248 51.8 -1.0308982133865356 52.6 -1.0496608018875122
		 53.4 -0.8636133074760437 54.2 -0.41233885288238525 55 -0.17440690100193024 55.8 -0.3386823832988739
		 56.6 -0.54945617914199829 57.4 -0.48279887437820435 58.2 -0.50459057092666626 59 -0.55821084976196289
		 59.8 -0.48605048656463623 60.6 -0.393257737159729 61.4 -0.52466052770614624 62.2 -0.8194955587387085
		 63 -1.027097225189209 63.8 -1.0244086980819702 64.6 -0.95126825571060147 65.4 -0.92493242025375344
		 66.2 -0.84628921747207653 67 -0.70692116022109996 67.8 -0.58301335573196411 68.6 -0.48938381671905523
		 69.4 -0.5201261043548584;
createNode animCurveTA -n "Bip01_L_Foot_rotateY9";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 87 ".ktv[0:86]"  0.6 -0.47206848859786982 1.4 -0.36817669868469238
		 2.2 -0.26394325494766235 3 -0.30577993392944336 3.8 -0.36460864543914789 4.6 -0.36978831887245178
		 5.4 -0.38584291934967041 6.2 -0.2720472514629364 7 -0.22771739959716797 7.8 -0.28226464986801147
		 8.6 -0.31311628222465515 9.4 -0.2576579749584198 10.2 -0.28651958703994757 11 -0.38952824473381042
		 11.8 -0.42717966437339783 12.6 -0.38480105996131897 13.4 -0.365744948387146 14.2 -0.38484305143356318
		 15 -0.45967265963554382 15.8 -0.51371234655380249 16.6 -0.51177620887756359 17.4 -0.52292144298553467
		 18.2 -0.50137639045715332 19 -0.41264644265174866 19.8 -0.35323438048362732 20.6 -0.43044114112853998
		 21.4 -0.54383504390716553 22.2 -0.47134834527969349 23 -0.42851191759109497 23.8 -0.45988529920577997
		 24.6 -0.49405211210250854 25.4 -0.51901847124099731 26.2 -0.54563015699386597 27 -0.59926903247833252
		 27.8 -0.63385087251663208 28.6 -0.64482128620147705 29.4 -0.63811266422271729 30.2 -0.59316372871398926
		 31 -0.52266687154769897 31.8 -0.49326631426811218 32.6 -0.52104687690734863 33.4 -0.51020997762680054
		 34.2 -0.4930409193038941 35 -0.45042097568511957 35.8 -0.36081987619400024 36.6 -0.37016907334327698
		 37.4 -0.40119794011116033 38.2 -0.40847638249397278 39 -0.36517971754074102 39.8 -0.27141162753105164
		 40.6 -0.1979590505361557 41.4 -0.15749923884868622 42.2 -0.13943623006343842 43 -0.11175390332937242
		 43.8 -0.1510855108499527 44.6 -0.14002613723278046 45.4 -0.047769166529178619 46.2 -0.0024321922101080418
		 47 -0.021576765924692154 47.8 -0.073078736662864699 48.6 -0.10749145597219469 49.4 -0.13828524947166443
		 50.2 -0.17090596258640289 51 -0.18307086825370789 51.8 -0.17050875723361969 52.6 -0.12851157784461975
		 53.4 -0.029047291725873947 54.2 -0.018118372187018391 55 -0.084758788347244263 55.8 -0.10650604963302614
		 56.6 0.0049186432734131813 57.4 0.027149893343448639 58.2 -0.16658692061901093 59 -0.37312477827072144
		 59.8 -0.37112551927566528 60.6 -0.3106018602848053 61.4 -0.270597904920578 62.2 -0.25449800491333008
		 63 -0.26708906888961792 63.8 -0.3194859921932221 64.6 -0.33671870827674866 65.4 -0.32301419973373413
		 66.2 -0.32386675477027893 67 -0.39585623145103455 67.8 -0.45694026350975037 68.6 -0.44001880288124079
		 69.4 -0.40089228749275208;
createNode animCurveTA -n "Bip01_L_Foot_rotateZ9";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 87 ".ktv[0:86]"  0.6 0.3362540602684021 1.4 0.37934121489524841
		 2.2 0.4373012781143189 3 0.53541934490203857 3.8 0.64012622833251953 4.6 0.54094713926315308
		 5.4 0.32813787460327148 6.2 0.30768388509750366 7 0.34737393260002136 7.8 0.17972098290920258
		 8.6 0.0041394135914742947 9.4 0.12916427850723269 10.2 0.41491600871086121 11 0.54952937364578247
		 11.8 0.56121844053268433 12.6 0.61926865577697754 13.4 0.67755579948425293 14.2 0.68612790107727051
		 15 0.68315815925598145 15.8 0.6719452738761903 16.6 0.64517629146575928 17.4 0.56727778911590576
		 18.2 0.50526010990142822 19 0.47903725504875183 19.8 0.44719371199607855 20.6 0.49106556177139282
		 21.4 0.66272604465484619 22.2 0.8880544900894165 23 1.0370961427688601 23.8 1.0762530565261841
		 24.6 1.0497186183929443 25.4 0.97936677932739269 26.2 0.93808490037918091 27 1.0508465766906738
		 27.8 1.1821256875991819 28.6 1.168516993522644 29.4 1.1223552227020264 30.2 1.1729240417480471
		 31 1.2630668878555298 31.8 1.3255668878555298 32.6 1.4699487686157229 33.4 1.6802867650985718
		 34.2 1.8210833072662351 35 1.8587479591369631 35.8 1.7882558107376103 36.6 1.6709433794021606
		 37.4 1.6949987411499023 38.2 1.9214582443237307 39 2.0945975780487061 39.8 2.0871338844299316
		 40.6 2.0432939529418945 41.4 2.1325705051422119 42.2 2.3251657485961914 43 2.5518920421600342
		 43.8 2.7287609577178955 44.6 2.7940187454223637 45.4 2.720658540725708 46.2 2.6158204078674316
		 47 2.5827980041503902 47.8 2.6427278518676758 48.6 2.772702693939209 49.4 2.8322937488555908
		 50.2 2.7173755168914795 51 2.6150879859924316 51.8 2.6681764125823975 52.6 2.7011995315551758
		 53.4 2.5418834686279297 54.2 2.2912840843200684 55 2.1905941963195801 55.8 2.2611641883850098
		 56.6 2.2606945037841797 57.4 2.1836373805999756 58.2 2.1865737438201904 59 2.168503999710083
		 59.8 2.0655415058135982 60.6 2.0393509864807129 61.4 2.1120476722717285 62.2 2.1933622360229492
		 63 2.1646058559417725 63.8 2.0311119556427002 64.6 1.9656774997711179 65.4 2.0176029205322266
		 66.2 2.0520598888397217 67 1.9583331346511843 67.8 1.8196935653686523 68.6 1.7895746231079102
		 69.4 1.8558504581451418;
createNode animCurveTA -n "Bip01_L_Foot_rotateX9";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 87 ".ktv[0:86]"  0.6 -6.0635051727294922 1.4 -6.2379989624023437
		 2.2 -6.4077973365783691 3 -6.3510298728942871 3.8 -6.2378926277160645 4.6 -6.1706886291503906
		 5.4 -6.2152838706970215 6.2 -6.4043312072753906 7 -6.5351357460021973 7.8 -6.3912343978881836
		 8.6 -6.3277530670166016 9.4 -6.4081788063049316 10.2 -6.3956198692321777 11 -6.189267635345459
		 11.8 -6.1126151084899902 12.6 -6.1942071914672852 13.4 -6.2418937683105469 14.2 -6.1418037414550781
		 15 -6.0502843856811523 15.8 -5.9822301864624023 16.6 -5.974306583404541 17.4 -5.9570116996765137
		 18.2 -5.9675498008728036 19 -6.1427068710327148 19.8 -6.2740845680236816 20.6 -6.0913376808166504
		 21.4 -5.8973598480224609 22.2 -5.9768791198730469 23 -6.1293525695800781 23.8 -6.0517292022705087
		 24.6 -5.9635539054870605 25.4 -5.9523897171020508 26.2 -5.8975338935852051 27 -5.7839183807373047
		 27.8 -5.7234907150268555 28.6 -5.7085175514221191 29.4 -5.6954159736633301 30.2 -5.788935661315918
		 31 -5.9337968826293945 31.8 -5.9801244735717773 32.6 -5.9535212516784668 33.4 -5.9383268356323242
		 34.2 -6.0020236968994141 35 -6.1290802955627441 35.8 -6.2144351005554199 36.6 -6.2427525520324707
		 37.4 -6.182448387145997 38.2 -6.1786551475524902 39 -6.2776789665222168 39.8 -6.4605426788330078
		 40.6 -6.5606245994567871 41.4 -6.6580500602722168 42.2 -6.744636058807373 43 -6.7506279945373535
		 43.8 -6.6803622245788574 44.6 -6.7135858535766602 45.4 -6.8823461532592773 46.2 -6.9702229499816895
		 47 -6.9229235649108887 47.8 -6.8123655319213867 48.6 -6.752070426940918 49.4 -6.7291522026062012
		 50.2 -6.6440043449401855 51 -6.6204633712768555 51.8 -6.6509943008422852 52.6 -6.7399344444274902
		 53.4 -6.8982505798339844 54.2 -6.9355454444885254 55 -6.7843074798583984 55.8 -6.7578091621398926
		 56.6 -6.9883441925048828 57.4 -7.0212998390197754 58.2 -6.6048383712768555 59 -6.2527937889099121
		 59.8 -6.2508759498596191 60.6 -6.3597297668457031 61.4 -6.4232044219970703 62.2 -6.4664664268493652
		 63 -6.4335637092590332 63.8 -6.3283195495605478 64.6 -6.3446788787841797 65.4 -6.3594532012939453
		 66.2 -6.3587679862976074 67 -6.2282943725585938 67.8 -6.1088643074035645 68.6 -6.1541004180908203
		 69.4 -6.1972370147705078;
createNode animCurveTA -n "Bip01_R_Thigh_rotateX9";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 87 ".ktv[0:86]"  0.6 0.23793645203113556 1.4 0.25360491871833801
		 2.2 0.2538507878780365 3 0.24859155714511874 3.8 0.24135154485702515 4.6 0.2290435582399368
		 5.4 0.23266355693340307 6.2 0.25607743859291077 7 0.25264868140220642 7.8 0.25301751494407654
		 8.6 0.25382348895072937 9.4 0.24076415598392492 10.2 0.25013518333435059 11 0.2412832528352738
		 11.8 0.24790853261947629 12.6 0.23382468521594998 13.4 0.23165267705917361 14.2 0.21105283498764041
		 15 0.21580664813518527 15.8 0.2118997722864151 16.6 0.20605313777923584 17.4 0.20730988681316376
		 18.2 0.21658529341220856 19 0.20073924958705905 19.8 0.21381223201751709 20.6 0.20274733006954193
		 21.4 0.19937321543693545 22.2 0.19531607627868652 23 0.19546633958816528 23.8 0.18728378415107727
		 24.6 0.19956445693969729 25.4 0.19619034230709079 26.2 0.18438777327537537 27 0.18783019483089447
		 27.8 0.19190098345279696 28.6 0.18696959316730499 29.4 0.18747502565383911 30.2 0.19012513756752017
		 31 0.19871751964092255 31.8 0.20039774477481839 32.6 0.2010534405708313 33.4 0.20072558522224423
		 34.2 0.20236483216285703 35 0.19609472155570984 35.8 0.19933223724365232 36.6 0.2128286808729172
		 37.4 0.22359305620193479 38.2 0.24677471816539767 39 0.24490325152873996 39.8 0.26738822460174561
		 40.6 0.26809856295585632 41.4 0.25994333624839783 42.2 0.26931434869766235 43 0.25524416565895081
		 43.8 0.31537714600563049 44.6 0.33793041110038757 45.4 0.3451157808303833 46.2 0.34928220510482788
		 47 0.36210930347442621 47.8 0.37914377450942993 48.6 0.3957274854183197 49.4 0.40990695357322698
		 50.2 0.40901902318000793 51 0.4252475500106811 51.8 0.41875886917114258 52.6 0.4325831830501557
		 53.4 0.42974182963371282 54.2 0.42879924178123474 55 0.43196845054626465 55.8 0.42506995797157288
		 56.6 0.41971510648727417 57.4 0.40896439552307129 58.2 0.39055019617080688 59 0.38865140080451965
		 59.8 0.37333813309669495 60.6 0.37003231048583984 61.4 0.36256009340286255 62.2 0.35010182857513428
		 63 0.34271156787872314 63.8 0.33211109042167664 64.6 0.32585465908050537 65.4 0.32137402892112732
		 66.2 0.30466741323471069 67 0.27093991637229919 67.8 0.27723735570907593 68.6 0.28025630116462708
		 69.4 0.27580302953720093;
createNode animCurveTA -n "Bip01_R_Thigh_rotateZ9";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 87 ".ktv[0:86]"  0.6 1.7880887985229492 1.4 1.9216599464416506
		 2.2 2.0973050594329834 3 2.2430202960968018 3.8 2.399090051651001 4.6 2.5078678131103516
		 5.4 2.6004304885864258 6.2 2.7330727577209473 7 2.9864180088043213 7.8 3.2068829536437988
		 8.6 3.0858519077301025 9.4 2.6943182945251465 10.2 2.5266644954681396 11 2.6436655521392822
		 11.8 2.7761437892913814 12.6 2.7739856243133545 13.4 2.6693196296691899 14.2 2.5768663883209229
		 15 2.5299975872039795 15.8 2.4458906650543213 16.6 2.3339438438415527 17.4 2.1276311874389648
		 18.2 1.8930824995040891 19 1.8124178647994995 19.8 1.8901318311691284 20.6 1.9804952144622801
		 21.4 1.9537072181701665 22.2 1.9321920871734619 23 1.9419592618942261 23.8 1.9519176483154297
		 24.6 1.948611855506897 25.4 1.9169744253158567 26.2 1.84219753742218 27 1.7976920604705815
		 27.8 1.8935605287551882 28.6 2.0715005397796631 29.4 2.229032039642334 30.2 2.3434650897979741
		 31 2.3897874355316162 31.8 2.438814640045166 32.6 2.5552146434783936 33.4 2.6675848960876465
		 34.2 2.6697843074798584 35 2.739506721496582 35.8 2.773944616317749 36.6 2.7475664615631108
		 37.4 2.7543282508850098 38.2 2.9844510555267334 39 3.3191986083984375 39.8 3.4968926906585693
		 40.6 3.3901779651641855 41.4 3.293107271194458 42.2 3.2665925025939941 43 3.1695764064788814
		 43.8 3.0630118846893311 44.6 3.0737898349761963 45.4 3.10076904296875 46.2 3.1564624309539795
		 47 3.2106533050537109 47.8 3.2905800342559814 48.6 3.2189586162567139 49.4 3.0720551013946533
		 50.2 3.0763442516326904 51 3.2437386512756352 51.8 3.310701847076416 52.6 3.2323868274688721
		 53.4 3.2190816402435307 54.2 3.3175182342529297 55 3.4744486808776855 55.8 3.5431878566741943
		 56.6 3.5164816379547119 57.4 3.2801163196563725 58.2 2.9056990146636963 59 2.6207981109619141
		 59.8 2.5028679370880127 60.6 2.529655933380127 61.4 2.5903217792510982 62.2 2.5190145969390869
		 63 2.2884957790374756 63.8 1.9946473836898804 64.6 1.8109016418457033 65.4 1.7688003778457642
		 66.2 1.7655218839645386 67 1.8790532350540157 67.8 2.1296529769897461 68.6 2.4056062698364258
		 69.4 2.7207784652709961;
createNode animCurveTA -n "Bip01_R_Thigh_rotateY9";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 87 ".ktv[0:86]"  0.6 179.58607482910156 1.4 179.52320861816406
		 2.2 179.51217651367187 3 179.51986694335935 3.8 179.48751831054687 4.6 179.43756103515625
		 5.4 179.38392639160156 6.2 179.3388671875 7 179.32391357421875 7.8 179.3275146484375
		 8.6 179.31739807128906 9.4 179.30386352539062 10.2 179.28370666503906 11 179.279541015625
		 11.8 179.31881713867187 12.6 179.38792419433594 13.4 179.44108581542969 14.2 179.49526977539062
		 15 179.55377197265625 15.8 179.63575744628906 16.6 179.72279357910156 17.4 179.78097534179687
		 18.2 179.82102966308594 19 179.88389587402344 19.8 179.9649658203125 20.6 180.06152343750003
		 21.4 180.149169921875 22.2 180.20549011230469 23 180.26429748535159 23.8 180.33412170410156
		 24.6 180.40312194824219 25.4 180.46203613281253 26.2 180.53497314453125 27 180.59297180175781
		 27.8 180.61283874511719 28.6 180.6317443847656 29.4 180.61878967285156 30.2 180.60797119140625
		 31 180.59941101074219 31.8 180.58135986328125 32.6 180.52922058105469 33.4 180.46368408203125
		 34.2 180.4129638671875 35 180.35479736328128 35.8 180.26416015625 36.6 180.1664123535156
		 37.4 180.08880615234378 38.2 180.04237365722656 39 179.98399353027344 39.8 179.88941955566406
		 40.6 179.82318115234375 41.4 179.79243469238281 42.2 179.73818969726562 43 179.64328002929687
		 43.8 179.5409851074219 44.6 179.48524475097656 45.4 179.4652099609375 46.2 179.44313049316406
		 47 179.40031433105469 47.8 179.36460876464844 48.6 179.34890747070312 49.4 179.34449768066406
		 50.2 179.33100891113281 51 179.32673645019531 51.8 179.33442687988281 52.6 179.3277587890625
		 53.4 179.30950927734375 54.2 179.3160400390625 55 179.34844970703125 55.8 179.37362670898435
		 56.6 179.39564514160156 57.4 179.41670227050781 58.2 179.46168518066406 59 179.51284790039062
		 59.8 179.5780029296875 60.6 179.65971374511719 61.4 179.73336791992187 62.2 179.72796630859378
		 63 179.70077514648435 63.8 179.71919250488281 64.6 179.76617431640625 65.4 179.81535339355469
		 66.2 179.87222290039062 67 179.95335388183594 67.8 180.02583312988281 68.6 180.08567810058594
		 69.4 180.11956787109375;
createNode animCurveTA -n "Bip01_R_Calf_rotateX9";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 87 ".ktv[0:86]"  0.6 -0.048842195421457291 1.4 -0.048252947628498077
		 2.2 -0.047903154045343399 3 -0.048152018338441849 3.8 -0.048362795263528824 4.6 -0.049165207892656326
		 5.4 -0.050776630640029907 6.2 -0.050067074596881873 7 -0.045297086238861091 7.8 -0.042664419859647751
		 8.6 -0.050241783261299133 9.4 -0.063086874783039093 10.2 -0.068883597850799561 11 -0.066963933408260345
		 11.8 -0.064881235361099243 12.6 -0.066006429493427277 13.4 -0.069356836378574371
		 14.2 -0.072392746806144714 15 -0.074370920658111572 15.8 -0.07689232379198073 16.6 -0.080175220966339097
		 17.4 -0.085917159914970398 18.2 -0.092521697282791138 19 -0.094678975641727461 19.8 -0.092261739075183868
		 20.6 -0.090454690158367157 21.4 -0.090619534254074097 22.2 -0.091136127710342393
		 23 -0.091153457760810852 23.8 -0.091401882469654083 24.6 -0.091786943376064301 25.4 -0.093546330928802476
		 26.2 -0.096082210540771484 27 -0.097395613789558411 27.8 -0.094680838286876678 28.6 -0.090620338916778564
		 29.4 -0.087652921676635742 30.2 -0.085922740399837494 31 -0.085777804255485535 31.8 -0.085509918630123138
		 32.6 -0.083840109407901764 33.4 -0.083060339093208327 34.2 -0.084130562841892242
		 35 -0.084716781973838806 35.8 -0.085435502231121049 36.6 -0.088536337018013 37.4 -0.090631701052188873
		 38.2 -0.086683869361877441 39 -0.079328477382659898 39.8 -0.076866522431373596 40.6 -0.081838168203830719
		 41.4 -0.086576327681541443 42.2 -0.088903583586215973 43 -0.093734517693519578 43.8 -0.098618991672992706
		 44.6 -0.099792890250682831 45.4 -0.10044465959072112 46.2 -0.10172604024410248 47 -0.10149649530649184
		 47.8 -0.10098028182983398 48.6 -0.10358771681785585 49.4 -0.10793058574199677 50.2 -0.10757509618997574
		 51 -0.10276680439710616 51.8 -0.10081882774829865 52.6 -0.1026034876704216 53.4 -0.10365948081016541
		 54.2 -0.10227786749601364 55 -0.099047809839248657 55.8 -0.096116781234741211 56.6 -0.094450652599334717
		 57.4 -0.094761833548545837 58.2 -0.09635452926158905 59 -0.095995992422103923 59.8 -0.092263378202915206
		 60.6 -0.086067318916320801 61.4 -0.079294882714748383 62.2 -0.075356535613536835
		 63 -0.075686551630496979 63.8 -0.077332265675067902 64.6 -0.076527908444404602 65.4 -0.072495296597480774
		 66.2 -0.065243400633335114 67 -0.052815705537796027 67.8 -0.036077626049518585 68.6 -0.020116269588470459
		 69.4 -0.0071769068017601976;
createNode animCurveTA -n "Bip01_R_Calf_rotateZ9";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 87 ".ktv[0:86]"  0.6 -7.2855062484741202 1.4 -7.3089160919189462
		 2.2 -7.2208890914916992 3 -7.2079629898071325 3.8 -7.2084412574768084 4.6 -7.3172359466552734
		 5.4 -7.39335060119629 6.2 -7.3073706626892099 7 -7.0322299003601083 7.8 -6.9095277786254883
		 8.6 -7.3522748947143564 9.4 -8.1519832611083984 10.2 -8.5332736968994141 11 -8.4432697296142578
		 11.8 -8.2921876907348633 12.6 -8.3393383026123047 13.4 -8.533935546875 14.2 -8.7082509994506836
		 15 -8.8008584976196289 15.8 -8.9648876190185547 16.6 -9.1549158096313477 17.4 -9.5203008651733398
		 18.2 -9.9538812637329102 19 -10.106314659118652 19.8 -9.9839181900024414 20.6 -9.8621368408203125
		 21.4 -9.8870744705200195 22.2 -9.90997314453125 23 -9.8984022140502912 23.8 -9.925445556640625
		 24.6 -9.9810924530029297 25.4 -10.091492652893066 26.2 -10.216567993164062 27 -10.288660049438477
		 27.8 -10.13909912109375 28.6 -9.8919906616210937 29.4 -9.7161798477172852 30.2 -9.6208229064941406
		 31 -9.6261663436889648 31.8 -9.6251010894775391 32.6 -9.501215934753418 33.4 -9.4233579635620117
		 34.2 -9.5631856918334961 35 -9.5776662826538086 35.8 -9.6649293899536133 36.6 -9.8557596206665039
		 37.4 -9.9977817535400391 38.2 -9.7140245437622088 39 -9.2451267242431641 39.8 -9.0652475357055664
		 40.6 -9.3721399307250977 41.4 -9.6159973144531232 42.2 -9.7456464767456055 43 -10.059611320495604
		 43.8 -10.386983871459959 44.6 -10.481064796447754 45.4 -10.542969703674316 46.2 -10.572381019592283
		 47 -10.605865478515623 47.8 -10.572092056274414 48.6 -10.734659194946287 49.4 -10.975096702575684
		 50.2 -10.958964347839355 51 -10.67231369018555 51.8 -10.557890892028809 52.6 -10.674817085266113
		 53.4 -10.71869945526123 54.2 -10.640935897827148 55 -10.437305450439451 55.8 -10.286137580871582
		 56.6 -10.165008544921877 57.4 -10.204229354858398 58.2 -10.301568031311035 59 -10.220302581787108
		 59.8 -10.013513565063477 60.6 -9.6421079635620117 61.4 -9.1992063522338867 62.2 -8.9569816589355469
		 63 -8.9895143508911133 63.8 -9.1330833435058594 64.6 -9.0829753875732422 65.4 -8.7925662994384766
		 66.2 -8.3833541870117187 67 -7.618888854980467 67.8 -6.5098185539245614 68.6 -5.4950823783874512
		 69.4 -4.6517329216003418;
createNode animCurveTA -n "Bip01_R_Calf_rotateY9";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 87 ".ktv[0:86]"  0.6 1.0434651374816897 1.4 1.0436304807662964
		 2.2 1.0436041355133057 3 1.0435349941253662 3.8 1.0434831380844116 4.6 1.043428897857666
		 5.4 1.0431485176086426 6.2 1.0432111024856567 7 1.043972373008728 7.8 1.0444221496582031
		 8.6 1.0432294607162476 9.4 1.0411348342895508 10.2 1.0401564836502075 11 1.0405315160751345
		 11.8 1.040863037109375 12.6 1.0406427383422852 13.4 1.0400393009185791 14.2 1.0394922494888306
		 15 1.039095401763916 15.8 1.0386465787887571 16.6 1.0380241870880127 17.4 1.036949634552002
		 18.2 1.0356817245483398 19 1.0352640151977539 19.8 1.0357877016067505 20.6 1.0361337661743164
		 21.4 1.0361230373382568 22.2 1.036013126373291 23 1.0359891653060913 23.8 1.0359543561935425
		 24.6 1.0359164476394651 25.4 1.0355668067932129 26.2 1.0350152254104614 27 1.0347352027893066
		 27.8 1.0353155136108398 28.6 1.0361273288726809 29.4 1.036716103553772 30.2 1.0370599031448364
		 31 1.0371075868606567 31.8 1.0371806621551514 32.6 1.0374748706817627 33.4 1.0375912189483647
		 34.2 1.0374728441238406 35 1.0373351573944092 35.8 1.0372519493103027 36.6 1.0366487503051758
		 37.4 1.0362523794174194 38.2 1.0369775295257568 39 1.0383517742156982 39.8 1.0387742519378662
		 40.6 1.0378493070602417 41.4 1.0368813276290894 42.2 1.0364121198654177 43 1.0354779958724976
		 43.8 1.0345162153244021 44.6 1.0342972278594973 45.4 1.0341945886611938 46.2 1.0338603258132937
		 47 1.0339680910110474 47.8 1.0340695381164553 48.6 1.0335314273834229 49.4 1.0325716733932495
		 50.2 1.0326584577560425 51 1.0336892604827881 51.8 1.0341014862060549 52.6 1.0337347984313965
		 53.4 1.0334911346435549 54.2 1.0337895154953003 55 1.0344514846801758 55.8 1.0350923538208008
		 56.6 1.0354111194610596 57.4 1.0353713035583496 58.2 1.0350420475006104 59 1.035045862197876
		 59.8 1.0358246564865112 60.6 1.0370515584945681 61.4 1.0383023023605349 62.2 1.0390301942825315
		 63 1.0389862060546875 63.8 1.0387381315231323 64.6 1.0388823747634888 65.4 1.0395609140396118
		 66.2 1.0408812761306765 67 1.0429458618164065 67.8 1.045390248298645 68.6 1.0475142002105713
		 69.4 1.0490282773971558;
createNode animCurveTA -n "Bip01_R_Foot_rotateX9";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 87 ".ktv[0:86]"  0.6 12.222915649414064 1.4 12.421622276306152
		 2.2 12.841940879821776 3 13.282554626464846 3.8 13.365928649902344 4.6 13.235000610351562
		 5.4 13.093379020690918 6.2 12.906852722167969 7 12.869687080383301 7.8 12.923675537109377
		 8.6 12.997210502624512 9.4 13.076318740844728 10.2 12.933363914489746 11 12.85441780090332
		 11.8 13.068632125854492 12.6 13.415581703186035 13.4 13.5816650390625 14.2 13.628808975219728
		 15 13.669754981994627 15.8 13.809249877929688 16.6 13.937532424926758 17.4 13.933945655822754
		 18.2 13.773653984069824 19 13.715641975402832 19.8 13.719912528991701 20.6 13.770753860473633
		 21.4 13.803436279296877 22.2 13.765897750854492 23 13.701199531555176 23.8 13.823777198791506
		 24.6 13.903403282165527 25.4 13.883962631225588 26.2 13.958678245544434 27 14.052728652954102
		 27.8 13.891419410705566 28.6 13.641302108764648 29.4 13.478130340576172 30.2 13.349878311157228
		 31 13.231189727783203 31.8 13.10811710357666 32.6 12.976266860961914 33.4 12.927017211914064
		 34.2 12.896640777587892 35 12.86758518218994 35.8 12.751615524291992 36.6 12.576320648193359
		 37.4 12.509410858154297 38.2 12.69374942779541 39 12.982015609741213 39.8 12.980769157409668
		 40.6 13.076742172241213 41.4 13.351665496826172 42.2 13.492574691772459 43 13.31874942779541
		 43.8 13.017848968505859 44.6 12.951567649841309 45.4 13.051278114318848 46.2 13.145402908325195
		 47 13.114590644836426 47.8 13.044665336608888 48.6 13.086155891418455 49.4 13.131327629089355
		 50.2 13.034673690795898 51 12.865528106689451 51.8 12.789200782775881 52.6 12.730251312255859
		 53.4 12.651266098022459 54.2 12.634652137756348 55 12.607500076293944 55.8 12.467646598815918
		 56.6 12.27109432220459 57.4 12.072510719299316 58.2 12.051631927490234 59 12.206757545471191
		 59.8 12.37263298034668 60.6 12.344894409179688 61.4 12.12282657623291 62.2 11.888692855834959
		 63 11.741211891174316 63.8 11.637969017028809 64.6 11.612143516540527 65.4 11.645785331726074
		 66.2 11.611688613891602 67 11.576878547668455 67.8 11.639540672302246 68.6 11.609889030456545
		 69.4 11.513944625854492;
createNode animCurveTA -n "Bip01_R_Foot_rotateY9";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 87 ".ktv[0:86]"  0.6 -3.0813708305358891 1.4 -3.2070739269256592
		 2.2 -3.4790971279144292 3 -3.7574443817138676 3.8 -3.8176836967468266 4.6 -3.746906995773315
		 5.4 -3.6716303825378418 6.2 -3.5379080772399902 7 -3.5066921710968018 7.8 -3.548323392868042
		 8.6 -3.6035256385803223 9.4 -3.6784498691558842 10.2 -3.610415935516357 11 -3.5478785037994385
		 11.8 -3.6769447326660152 12.6 -3.8797240257263175 13.4 -3.9970135688781743 14.2 -4.0121049880981445
		 15 -3.9897458553314205 15.8 -4.0791935920715341 16.6 -4.2075457572937012 17.4 -4.2000060081481934
		 18.2 -4.1241979598999023 19 -4.0892753601074219 19.8 -4.076810359954834 20.6 -4.1031045913696289
		 21.4 -4.147303581237793 22.2 -4.106299877166748 23 -4.0836348533630371 23.8 -4.1456723213195801
		 24.6 -4.2328324317932129 25.4 -4.2026023864746103 26.2 -4.2254700660705566 27 -4.3057422637939462
		 27.8 -4.1969108581542969 28.6 -4.0265231132507324 29.4 -3.9341814517974858 30.2 -3.8387203216552734
		 31 -3.7716903686523433 31.8 -3.6741101741790776 32.6 -3.6012730598449707 33.4 -3.540916919708252
		 34.2 -3.5666403770446777 35 -3.5553650856018062 35.8 -3.492246150970459 36.6 -3.4061589241027836
		 37.4 -3.358889102935791 38.2 -3.457181453704834 39 -3.6382238864898682 39.8 -3.6520788669586186
		 40.6 -3.6989061832427974 41.4 -3.8518691062927242 42.2 -3.956508874893188 43 -3.8302435874938978
		 43.8 -3.6676867008209233 44.6 -3.6387543678283687 45.4 -3.7369179725646977 46.2 -3.8011043071746831
		 47 -3.7526187896728516 47.8 -3.724783182144165 48.6 -3.7395825386047359 49.4 -3.7865452766418457
		 50.2 -3.7141427993774414 51 -3.6050794124603271 51.8 -3.5644502639770508 52.6 -3.519706249237061
		 53.4 -3.4740755558013916 54.2 -3.4506518840789795 55 -3.4330935478210449 55.8 -3.3225183486938477
		 56.6 -3.1904194355010982 57.4 -3.0727858543395996 58.2 -3.0467820167541504 59 -3.1282353401184082
		 59.8 -3.1942873001098633 60.6 -3.1858453750610352 61.4 -3.0432558059692383 62.2 -2.9052660465240479
		 63 -2.7950286865234379 63.8 -2.7613511085510254 64.6 -2.7369356155395508 65.4 -2.7301709651947021
		 66.2 -2.7129600048065186 67 -2.6929259300231938 67.8 -2.6944940090179443 68.6 -2.6884851455688477
		 69.4 -2.6130273342132568;
createNode animCurveTA -n "Bip01_R_Foot_rotateZ9";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 87 ".ktv[0:86]"  0.6 8.4083518981933594 1.4 8.5609226226806641
		 2.2 8.6236600875854492 3 8.7060165405273437 3.8 8.8663120269775391 4.6 8.9911651611328125
		 5.4 8.9825220108032227 6.2 8.8037662506103516 7 8.6814451217651367 7.8 8.8312625885009766
		 8.6 9.1700906753540039 9.4 9.4347410202026367 10.2 9.5970649719238281 11 9.5971555709838867
		 11.8 9.4548931121826172 12.6 9.3572673797607422 13.4 9.3736371994018555 14.2 9.3624897003173828
		 15 9.2960414886474609 15.8 9.2225360870361328 16.6 9.222285270690918 17.4 9.3540239334106445
		 18.2 9.6056423187255859 19 9.8139734268188477 19.8 9.8411226272583008 20.6 9.7657260894775391
		 21.4 9.7000789642333984 22.2 9.6530485153198242 23 9.6733236312866211 23.8 9.8791542053222656
		 24.6 10.079943656921388 25.4 10.042316436767578 26.2 9.9246025085449219 27 9.8468561172485352
		 27.8 9.7364768981933594 28.6 9.6152801513671875 29.4 9.5095558166503906 30.2 9.4721269607543945
		 31 9.4880924224853516 31.8 9.4233427047729492 32.6 9.2884044647216797 33.4 9.3087024688720703
		 34.2 9.4933938980102539 35 9.6594543457031232 35.8 9.7759037017822266 36.6 9.918701171875
		 37.4 10.008696556091309 38.2 9.9638595581054687 39 9.8458518981933594 39.8 9.7803039550781232
		 40.6 9.771794319152832 41.4 9.7176828384399414 42.2 9.6998176574707031 43 9.8462390899658203
		 43.8 10.069293975830078 44.6 10.170347213745115 45.4 10.268188476562502 46.2 10.360007286071776
		 47 10.438532829284668 47.8 10.451579093933104 48.6 10.40754508972168 49.4 10.377114295959473
		 50.2 10.282493591308594 51 10.167306900024414 51.8 10.182614326477053 52.6 10.22785472869873
		 53.4 10.141738891601562 54.2 9.9801883697509766 55 9.8048858642578125 55.8 9.6499872207641602
		 56.6 9.5251913070678729 57.4 9.4172563552856445 58.2 9.2323913574218768 59 9.0095386505126953
		 59.8 8.9180641174316406 60.6 8.830601692199707 61.4 8.6015853881835938 62.2 8.3852567672729492
		 63 8.3947429656982422 63.8 8.4939956665039063 64.6 8.4652862548828125 65.4 8.3548069000244141
		 66.2 8.2436609268188477 67 8.0310916900634766 67.8 7.6877532005310076 68.6 7.239452838897706
		 69.4 6.7725319862365723;
select -ne :time1;
	setAttr ".o" 24.2;
	setAttr ".unw" 24.2;
select -ne :renderPartition;
	setAttr -s 126 ".st";
select -ne :initialShadingGroup;
	setAttr ".ro" yes;
select -ne :initialParticleSE;
	setAttr ".ro" yes;
select -ne :defaultShaderList1;
	setAttr -s 64 ".s";
select -ne :defaultTextureList1;
	setAttr -s 31 ".tx";
select -ne :postProcessList1;
	setAttr -s 2 ".p";
select -ne :defaultRenderUtilityList1;
	setAttr -s 31 ".u";
select -ne :defaultRenderingList1;
select -ne :renderGlobalsList1;
select -ne :defaultResolution;
	setAttr ".pa" 1;
select -ne :hardwareRenderGlobals;
	setAttr ".ctrs" 256;
	setAttr ".btrs" 512;
select -ne :hardwareRenderingGlobals;
	setAttr ".otfna" -type "stringArray" 18 "NURBS Curves" "NURBS Surfaces" "Polygons" "Subdiv Surfaces" "Particles" "Fluids" "Image Planes" "UI:" "Lights" "Cameras" "Locators" "Joints" "IK Handles" "Deformers" "Motion Trails" "Components" "Misc. UI" "Ornaments"  ;
	setAttr ".otfva" -type "Int32Array" 18 0 1 1 1 1 1
		 1 0 0 0 0 0 0 0 0 0 0 0 ;
select -ne :defaultHardwareRenderGlobals;
	setAttr ".fn" -type "string" "im";
	setAttr ".res" -type "string" "ntsc_4d 646 485 1.333";
select -ne :hyperGraphLayout;
	setAttr -s 4 ".hyp";
	setAttr ".hyp[1].isc" yes;
	setAttr ".hyp[2].isc" yes;
	setAttr ".hyp[3].isc" yes;
	setAttr ".hyp[4].isc" yes;
connectAttr "Bip01_Spine_translateX9.o" "|Stand_Talk_5|Bip01_Spine.tx";
connectAttr "Bip01_Spine_translateY9.o" "|Stand_Talk_5|Bip01_Spine.ty";
connectAttr "Bip01_Spine_translateZ9.o" "|Stand_Talk_5|Bip01_Spine.tz";
connectAttr "Bip01_Spine_rotateX9.o" "|Stand_Talk_5|Bip01_Spine.rx";
connectAttr "Bip01_Spine_rotateY9.o" "|Stand_Talk_5|Bip01_Spine.ry";
connectAttr "Bip01_Spine_rotateZ9.o" "|Stand_Talk_5|Bip01_Spine.rz";
connectAttr "Bip01_Spine1_rotateX9.o" "|Stand_Talk_5|Bip01_Spine|Bip01_Spine1.rx"
		;
connectAttr "Bip01_Spine1_rotateY9.o" "|Stand_Talk_5|Bip01_Spine|Bip01_Spine1.ry"
		;
connectAttr "Bip01_Spine1_rotateZ9.o" "|Stand_Talk_5|Bip01_Spine|Bip01_Spine1.rz"
		;
connectAttr "|Stand_Talk_5|Bip01_Spine.s" "|Stand_Talk_5|Bip01_Spine|Bip01_Spine1.is"
		;
connectAttr "Bip01_Spine2_rotateX9.o" "|Stand_Talk_5|Bip01_Spine|Bip01_Spine1|Bip01_Spine2.rx"
		;
connectAttr "Bip01_Spine2_rotateY9.o" "|Stand_Talk_5|Bip01_Spine|Bip01_Spine1|Bip01_Spine2.ry"
		;
connectAttr "Bip01_Spine2_rotateZ9.o" "|Stand_Talk_5|Bip01_Spine|Bip01_Spine1|Bip01_Spine2.rz"
		;
connectAttr "|Stand_Talk_5|Bip01_Spine|Bip01_Spine1.s" "|Stand_Talk_5|Bip01_Spine|Bip01_Spine1|Bip01_Spine2.is"
		;
connectAttr "|Stand_Talk_5|Bip01_Spine|Bip01_Spine1|Bip01_Spine2.s" "|Stand_Talk_5|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck.is"
		;
connectAttr "|Stand_Talk_5|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck.s" "|Stand_Talk_5|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_Head.is"
		;
connectAttr "|Stand_Talk_5|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_Head.s" "|Stand_Talk_5|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_Head|_notused_Bip01_HeadNub.is"
		;
connectAttr "Bip01_R_Clavicle_rotateX9.o" "|Stand_Talk_5|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_R_Clavicle.rx"
		;
connectAttr "Bip01_R_Clavicle_rotateY9.o" "|Stand_Talk_5|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_R_Clavicle.ry"
		;
connectAttr "Bip01_R_Clavicle_rotateZ9.o" "|Stand_Talk_5|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_R_Clavicle.rz"
		;
connectAttr "|Stand_Talk_5|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck.s" "|Stand_Talk_5|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_R_Clavicle.is"
		;
connectAttr "Bip01_R_UpperArm_rotateX9.o" "|Stand_Talk_5|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_R_Clavicle|Bip01_R_UpperArm.rx"
		;
connectAttr "Bip01_R_UpperArm_rotateY9.o" "|Stand_Talk_5|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_R_Clavicle|Bip01_R_UpperArm.ry"
		;
connectAttr "Bip01_R_UpperArm_rotateZ9.o" "|Stand_Talk_5|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_R_Clavicle|Bip01_R_UpperArm.rz"
		;
connectAttr "|Stand_Talk_5|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_R_Clavicle.s" "|Stand_Talk_5|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_R_Clavicle|Bip01_R_UpperArm.is"
		;
connectAttr "Bip01_R_Forearm_rotateX9.o" "|Stand_Talk_5|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_R_Clavicle|Bip01_R_UpperArm|Bip01_R_Forearm.rx"
		;
connectAttr "Bip01_R_Forearm_rotateY9.o" "|Stand_Talk_5|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_R_Clavicle|Bip01_R_UpperArm|Bip01_R_Forearm.ry"
		;
connectAttr "Bip01_R_Forearm_rotateZ9.o" "|Stand_Talk_5|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_R_Clavicle|Bip01_R_UpperArm|Bip01_R_Forearm.rz"
		;
connectAttr "|Stand_Talk_5|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_R_Clavicle|Bip01_R_UpperArm.s" "|Stand_Talk_5|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_R_Clavicle|Bip01_R_UpperArm|Bip01_R_Forearm.is"
		;
connectAttr "|Stand_Talk_5|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_R_Clavicle|Bip01_R_UpperArm|Bip01_R_Forearm.s" "|Stand_Talk_5|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_R_Clavicle|Bip01_R_UpperArm|Bip01_R_Forearm|Bip01_R_Hand.is"
		;
connectAttr "|Stand_Talk_5|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_R_Clavicle|Bip01_R_UpperArm|Bip01_R_Forearm|Bip01_R_Hand.s" "|Stand_Talk_5|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_R_Clavicle|Bip01_R_UpperArm|Bip01_R_Forearm|Bip01_R_Hand|Bip01_R_Finger0.is"
		;
connectAttr "|Stand_Talk_5|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_R_Clavicle|Bip01_R_UpperArm|Bip01_R_Forearm|Bip01_R_Hand|Bip01_R_Finger0.s" "|Stand_Talk_5|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_R_Clavicle|Bip01_R_UpperArm|Bip01_R_Forearm|Bip01_R_Hand|Bip01_R_Finger0|Bip01_R_Finger01.is"
		;
connectAttr "|Stand_Talk_5|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_R_Clavicle|Bip01_R_UpperArm|Bip01_R_Forearm|Bip01_R_Hand|Bip01_R_Finger0|Bip01_R_Finger01.s" "|Stand_Talk_5|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_R_Clavicle|Bip01_R_UpperArm|Bip01_R_Forearm|Bip01_R_Hand|Bip01_R_Finger0|Bip01_R_Finger01|_notused_Bip01_R_Finger0Nub.is"
		;
connectAttr "|Stand_Talk_5|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_R_Clavicle|Bip01_R_UpperArm|Bip01_R_Forearm|Bip01_R_Hand.s" "|Stand_Talk_5|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_R_Clavicle|Bip01_R_UpperArm|Bip01_R_Forearm|Bip01_R_Hand|Bip01_R_Finger2.is"
		;
connectAttr "|Stand_Talk_5|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_R_Clavicle|Bip01_R_UpperArm|Bip01_R_Forearm|Bip01_R_Hand|Bip01_R_Finger2.s" "|Stand_Talk_5|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_R_Clavicle|Bip01_R_UpperArm|Bip01_R_Forearm|Bip01_R_Hand|Bip01_R_Finger2|Bip01_R_Finger21.is"
		;
connectAttr "|Stand_Talk_5|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_R_Clavicle|Bip01_R_UpperArm|Bip01_R_Forearm|Bip01_R_Hand|Bip01_R_Finger2|Bip01_R_Finger21.s" "|Stand_Talk_5|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_R_Clavicle|Bip01_R_UpperArm|Bip01_R_Forearm|Bip01_R_Hand|Bip01_R_Finger2|Bip01_R_Finger21|_notused_Bip01_R_Finger2Nub.is"
		;
connectAttr "|Stand_Talk_5|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_R_Clavicle|Bip01_R_UpperArm|Bip01_R_Forearm|Bip01_R_Hand.s" "|Stand_Talk_5|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_R_Clavicle|Bip01_R_UpperArm|Bip01_R_Forearm|Bip01_R_Hand|Bip01_R_Finger1.is"
		;
connectAttr "|Stand_Talk_5|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_R_Clavicle|Bip01_R_UpperArm|Bip01_R_Forearm|Bip01_R_Hand|Bip01_R_Finger1.s" "|Stand_Talk_5|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_R_Clavicle|Bip01_R_UpperArm|Bip01_R_Forearm|Bip01_R_Hand|Bip01_R_Finger1|Bip01_R_Finger11.is"
		;
connectAttr "|Stand_Talk_5|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_R_Clavicle|Bip01_R_UpperArm|Bip01_R_Forearm|Bip01_R_Hand|Bip01_R_Finger1|Bip01_R_Finger11.s" "|Stand_Talk_5|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_R_Clavicle|Bip01_R_UpperArm|Bip01_R_Forearm|Bip01_R_Hand|Bip01_R_Finger1|Bip01_R_Finger11|_notused_Bip01_R_Finger1Nub.is"
		;
connectAttr "|Stand_Talk_5|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_R_Clavicle|Bip01_R_UpperArm|Bip01_R_Forearm|Bip01_R_Hand.s" "|Stand_Talk_5|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_R_Clavicle|Bip01_R_UpperArm|Bip01_R_Forearm|Bip01_R_Hand|Bip01_R_Finger3.is"
		;
connectAttr "|Stand_Talk_5|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_R_Clavicle|Bip01_R_UpperArm|Bip01_R_Forearm|Bip01_R_Hand|Bip01_R_Finger3.s" "|Stand_Talk_5|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_R_Clavicle|Bip01_R_UpperArm|Bip01_R_Forearm|Bip01_R_Hand|Bip01_R_Finger3|Bip01_R_Finger31.is"
		;
connectAttr "|Stand_Talk_5|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_R_Clavicle|Bip01_R_UpperArm|Bip01_R_Forearm|Bip01_R_Hand|Bip01_R_Finger3|Bip01_R_Finger31.s" "|Stand_Talk_5|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_R_Clavicle|Bip01_R_UpperArm|Bip01_R_Forearm|Bip01_R_Hand|Bip01_R_Finger3|Bip01_R_Finger31|_notused_Bip01_R_Finger3Nub.is"
		;
connectAttr "|Stand_Talk_5|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_R_Clavicle|Bip01_R_UpperArm|Bip01_R_Forearm|Bip01_R_Hand.s" "|Stand_Talk_5|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_R_Clavicle|Bip01_R_UpperArm|Bip01_R_Forearm|Bip01_R_Hand|Bip01_R_Finger4.is"
		;
connectAttr "|Stand_Talk_5|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_R_Clavicle|Bip01_R_UpperArm|Bip01_R_Forearm|Bip01_R_Hand|Bip01_R_Finger4.s" "|Stand_Talk_5|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_R_Clavicle|Bip01_R_UpperArm|Bip01_R_Forearm|Bip01_R_Hand|Bip01_R_Finger4|Bip01_R_Finger41.is"
		;
connectAttr "|Stand_Talk_5|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_R_Clavicle|Bip01_R_UpperArm|Bip01_R_Forearm|Bip01_R_Hand|Bip01_R_Finger4|Bip01_R_Finger41.s" "|Stand_Talk_5|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_R_Clavicle|Bip01_R_UpperArm|Bip01_R_Forearm|Bip01_R_Hand|Bip01_R_Finger4|Bip01_R_Finger41|_notused_Bip01_R_Finger4Nub.is"
		;
connectAttr "Bip01_L_Clavicle_rotateX9.o" "|Stand_Talk_5|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_L_Clavicle.rx"
		;
connectAttr "Bip01_L_Clavicle_rotateY9.o" "|Stand_Talk_5|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_L_Clavicle.ry"
		;
connectAttr "Bip01_L_Clavicle_rotateZ9.o" "|Stand_Talk_5|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_L_Clavicle.rz"
		;
connectAttr "|Stand_Talk_5|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck.s" "|Stand_Talk_5|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_L_Clavicle.is"
		;
connectAttr "Bip01_L_UpperArm_rotateX9.o" "|Stand_Talk_5|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_L_Clavicle|Bip01_L_UpperArm.rx"
		;
connectAttr "Bip01_L_UpperArm_rotateY9.o" "|Stand_Talk_5|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_L_Clavicle|Bip01_L_UpperArm.ry"
		;
connectAttr "Bip01_L_UpperArm_rotateZ9.o" "|Stand_Talk_5|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_L_Clavicle|Bip01_L_UpperArm.rz"
		;
connectAttr "|Stand_Talk_5|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_L_Clavicle.s" "|Stand_Talk_5|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_L_Clavicle|Bip01_L_UpperArm.is"
		;
connectAttr "Bip01_L_Forearm_rotateX9.o" "|Stand_Talk_5|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_L_Clavicle|Bip01_L_UpperArm|Bip01_L_Forearm.rx"
		;
connectAttr "Bip01_L_Forearm_rotateY9.o" "|Stand_Talk_5|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_L_Clavicle|Bip01_L_UpperArm|Bip01_L_Forearm.ry"
		;
connectAttr "Bip01_L_Forearm_rotateZ9.o" "|Stand_Talk_5|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_L_Clavicle|Bip01_L_UpperArm|Bip01_L_Forearm.rz"
		;
connectAttr "|Stand_Talk_5|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_L_Clavicle|Bip01_L_UpperArm.s" "|Stand_Talk_5|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_L_Clavicle|Bip01_L_UpperArm|Bip01_L_Forearm.is"
		;
connectAttr "|Stand_Talk_5|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_L_Clavicle|Bip01_L_UpperArm|Bip01_L_Forearm.s" "|Stand_Talk_5|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_L_Clavicle|Bip01_L_UpperArm|Bip01_L_Forearm|Bip01_L_Hand.is"
		;
connectAttr "|Stand_Talk_5|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_L_Clavicle|Bip01_L_UpperArm|Bip01_L_Forearm|Bip01_L_Hand.s" "|Stand_Talk_5|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_L_Clavicle|Bip01_L_UpperArm|Bip01_L_Forearm|Bip01_L_Hand|Bip01_L_Finger0.is"
		;
connectAttr "|Stand_Talk_5|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_L_Clavicle|Bip01_L_UpperArm|Bip01_L_Forearm|Bip01_L_Hand|Bip01_L_Finger0.s" "|Stand_Talk_5|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_L_Clavicle|Bip01_L_UpperArm|Bip01_L_Forearm|Bip01_L_Hand|Bip01_L_Finger0|Bip01_L_Finger01.is"
		;
connectAttr "|Stand_Talk_5|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_L_Clavicle|Bip01_L_UpperArm|Bip01_L_Forearm|Bip01_L_Hand|Bip01_L_Finger0|Bip01_L_Finger01.s" "|Stand_Talk_5|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_L_Clavicle|Bip01_L_UpperArm|Bip01_L_Forearm|Bip01_L_Hand|Bip01_L_Finger0|Bip01_L_Finger01|_notused_Bip01_L_Finger0Nub.is"
		;
connectAttr "|Stand_Talk_5|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_L_Clavicle|Bip01_L_UpperArm|Bip01_L_Forearm|Bip01_L_Hand.s" "|Stand_Talk_5|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_L_Clavicle|Bip01_L_UpperArm|Bip01_L_Forearm|Bip01_L_Hand|Bip01_L_Finger2.is"
		;
connectAttr "|Stand_Talk_5|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_L_Clavicle|Bip01_L_UpperArm|Bip01_L_Forearm|Bip01_L_Hand|Bip01_L_Finger2.s" "|Stand_Talk_5|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_L_Clavicle|Bip01_L_UpperArm|Bip01_L_Forearm|Bip01_L_Hand|Bip01_L_Finger2|Bip01_L_Finger21.is"
		;
connectAttr "|Stand_Talk_5|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_L_Clavicle|Bip01_L_UpperArm|Bip01_L_Forearm|Bip01_L_Hand|Bip01_L_Finger2|Bip01_L_Finger21.s" "|Stand_Talk_5|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_L_Clavicle|Bip01_L_UpperArm|Bip01_L_Forearm|Bip01_L_Hand|Bip01_L_Finger2|Bip01_L_Finger21|_notused_Bip01_L_Finger2Nub.is"
		;
connectAttr "|Stand_Talk_5|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_L_Clavicle|Bip01_L_UpperArm|Bip01_L_Forearm|Bip01_L_Hand.s" "|Stand_Talk_5|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_L_Clavicle|Bip01_L_UpperArm|Bip01_L_Forearm|Bip01_L_Hand|Bip01_L_Finger1.is"
		;
connectAttr "|Stand_Talk_5|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_L_Clavicle|Bip01_L_UpperArm|Bip01_L_Forearm|Bip01_L_Hand|Bip01_L_Finger1.s" "|Stand_Talk_5|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_L_Clavicle|Bip01_L_UpperArm|Bip01_L_Forearm|Bip01_L_Hand|Bip01_L_Finger1|Bip01_L_Finger11.is"
		;
connectAttr "|Stand_Talk_5|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_L_Clavicle|Bip01_L_UpperArm|Bip01_L_Forearm|Bip01_L_Hand|Bip01_L_Finger1|Bip01_L_Finger11.s" "|Stand_Talk_5|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_L_Clavicle|Bip01_L_UpperArm|Bip01_L_Forearm|Bip01_L_Hand|Bip01_L_Finger1|Bip01_L_Finger11|_notused_Bip01_L_Finger1Nub.is"
		;
connectAttr "|Stand_Talk_5|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_L_Clavicle|Bip01_L_UpperArm|Bip01_L_Forearm|Bip01_L_Hand.s" "|Stand_Talk_5|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_L_Clavicle|Bip01_L_UpperArm|Bip01_L_Forearm|Bip01_L_Hand|Bip01_L_Finger3.is"
		;
connectAttr "|Stand_Talk_5|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_L_Clavicle|Bip01_L_UpperArm|Bip01_L_Forearm|Bip01_L_Hand|Bip01_L_Finger3.s" "|Stand_Talk_5|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_L_Clavicle|Bip01_L_UpperArm|Bip01_L_Forearm|Bip01_L_Hand|Bip01_L_Finger3|Bip01_L_Finger31.is"
		;
connectAttr "|Stand_Talk_5|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_L_Clavicle|Bip01_L_UpperArm|Bip01_L_Forearm|Bip01_L_Hand|Bip01_L_Finger3|Bip01_L_Finger31.s" "|Stand_Talk_5|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_L_Clavicle|Bip01_L_UpperArm|Bip01_L_Forearm|Bip01_L_Hand|Bip01_L_Finger3|Bip01_L_Finger31|_notused_Bip01_L_Finger3Nub.is"
		;
connectAttr "|Stand_Talk_5|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_L_Clavicle|Bip01_L_UpperArm|Bip01_L_Forearm|Bip01_L_Hand.s" "|Stand_Talk_5|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_L_Clavicle|Bip01_L_UpperArm|Bip01_L_Forearm|Bip01_L_Hand|Bip01_L_Finger4.is"
		;
connectAttr "|Stand_Talk_5|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_L_Clavicle|Bip01_L_UpperArm|Bip01_L_Forearm|Bip01_L_Hand|Bip01_L_Finger4.s" "|Stand_Talk_5|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_L_Clavicle|Bip01_L_UpperArm|Bip01_L_Forearm|Bip01_L_Hand|Bip01_L_Finger4|Bip01_L_Finger41.is"
		;
connectAttr "|Stand_Talk_5|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_L_Clavicle|Bip01_L_UpperArm|Bip01_L_Forearm|Bip01_L_Hand|Bip01_L_Finger4|Bip01_L_Finger41.s" "|Stand_Talk_5|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_L_Clavicle|Bip01_L_UpperArm|Bip01_L_Forearm|Bip01_L_Hand|Bip01_L_Finger4|Bip01_L_Finger41|_notused_Bip01_L_Finger4Nub.is"
		;
connectAttr "Bip01_L_Thigh_rotateX9.o" "|Stand_Talk_5|Bip01_Spine|Bip01_L_Thigh.rx"
		;
connectAttr "Bip01_L_Thigh_rotateY9.o" "|Stand_Talk_5|Bip01_Spine|Bip01_L_Thigh.ry"
		;
connectAttr "Bip01_L_Thigh_rotateZ9.o" "|Stand_Talk_5|Bip01_Spine|Bip01_L_Thigh.rz"
		;
connectAttr "|Stand_Talk_5|Bip01_Spine.s" "|Stand_Talk_5|Bip01_Spine|Bip01_L_Thigh.is"
		;
connectAttr "Bip01_L_Calf_rotateX9.o" "|Stand_Talk_5|Bip01_Spine|Bip01_L_Thigh|Bip01_L_Calf.rx"
		;
connectAttr "Bip01_L_Calf_rotateY9.o" "|Stand_Talk_5|Bip01_Spine|Bip01_L_Thigh|Bip01_L_Calf.ry"
		;
connectAttr "Bip01_L_Calf_rotateZ9.o" "|Stand_Talk_5|Bip01_Spine|Bip01_L_Thigh|Bip01_L_Calf.rz"
		;
connectAttr "|Stand_Talk_5|Bip01_Spine|Bip01_L_Thigh.s" "|Stand_Talk_5|Bip01_Spine|Bip01_L_Thigh|Bip01_L_Calf.is"
		;
connectAttr "Bip01_L_Foot_rotateY9.o" "|Stand_Talk_5|Bip01_Spine|Bip01_L_Thigh|Bip01_L_Calf|Bip01_L_Foot.ry"
		;
connectAttr "Bip01_L_Foot_rotateZ9.o" "|Stand_Talk_5|Bip01_Spine|Bip01_L_Thigh|Bip01_L_Calf|Bip01_L_Foot.rz"
		;
connectAttr "Bip01_L_Foot_rotateX9.o" "|Stand_Talk_5|Bip01_Spine|Bip01_L_Thigh|Bip01_L_Calf|Bip01_L_Foot.rx"
		;
connectAttr "|Stand_Talk_5|Bip01_Spine|Bip01_L_Thigh|Bip01_L_Calf.s" "|Stand_Talk_5|Bip01_Spine|Bip01_L_Thigh|Bip01_L_Calf|Bip01_L_Foot.is"
		;
connectAttr "|Stand_Talk_5|Bip01_Spine|Bip01_L_Thigh|Bip01_L_Calf|Bip01_L_Foot.s" "|Stand_Talk_5|Bip01_Spine|Bip01_L_Thigh|Bip01_L_Calf|Bip01_L_Foot|Bip01_L_Toe0.is"
		;
connectAttr "|Stand_Talk_5|Bip01_Spine|Bip01_L_Thigh|Bip01_L_Calf|Bip01_L_Foot|Bip01_L_Toe0.s" "|Stand_Talk_5|Bip01_Spine|Bip01_L_Thigh|Bip01_L_Calf|Bip01_L_Foot|Bip01_L_Toe0|_notused_Bip01_L_Toe0Nub.is"
		;
connectAttr "Bip01_R_Thigh_rotateX9.o" "|Stand_Talk_5|Bip01_Spine|Bip01_R_Thigh.rx"
		;
connectAttr "Bip01_R_Thigh_rotateZ9.o" "|Stand_Talk_5|Bip01_Spine|Bip01_R_Thigh.rz"
		;
connectAttr "Bip01_R_Thigh_rotateY9.o" "|Stand_Talk_5|Bip01_Spine|Bip01_R_Thigh.ry"
		;
connectAttr "|Stand_Talk_5|Bip01_Spine.s" "|Stand_Talk_5|Bip01_Spine|Bip01_R_Thigh.is"
		;
connectAttr "Bip01_R_Calf_rotateX9.o" "|Stand_Talk_5|Bip01_Spine|Bip01_R_Thigh|Bip01_R_Calf.rx"
		;
connectAttr "Bip01_R_Calf_rotateZ9.o" "|Stand_Talk_5|Bip01_Spine|Bip01_R_Thigh|Bip01_R_Calf.rz"
		;
connectAttr "Bip01_R_Calf_rotateY9.o" "|Stand_Talk_5|Bip01_Spine|Bip01_R_Thigh|Bip01_R_Calf.ry"
		;
connectAttr "|Stand_Talk_5|Bip01_Spine|Bip01_R_Thigh.s" "|Stand_Talk_5|Bip01_Spine|Bip01_R_Thigh|Bip01_R_Calf.is"
		;
connectAttr "Bip01_R_Foot_rotateX9.o" "|Stand_Talk_5|Bip01_Spine|Bip01_R_Thigh|Bip01_R_Calf|Bip01_R_Foot.rx"
		;
connectAttr "Bip01_R_Foot_rotateY9.o" "|Stand_Talk_5|Bip01_Spine|Bip01_R_Thigh|Bip01_R_Calf|Bip01_R_Foot.ry"
		;
connectAttr "Bip01_R_Foot_rotateZ9.o" "|Stand_Talk_5|Bip01_Spine|Bip01_R_Thigh|Bip01_R_Calf|Bip01_R_Foot.rz"
		;
connectAttr "|Stand_Talk_5|Bip01_Spine|Bip01_R_Thigh|Bip01_R_Calf.s" "|Stand_Talk_5|Bip01_Spine|Bip01_R_Thigh|Bip01_R_Calf|Bip01_R_Foot.is"
		;
connectAttr "|Stand_Talk_5|Bip01_Spine|Bip01_R_Thigh|Bip01_R_Calf|Bip01_R_Foot.s" "|Stand_Talk_5|Bip01_Spine|Bip01_R_Thigh|Bip01_R_Calf|Bip01_R_Foot|Bip01_R_Toe0.is"
		;
connectAttr "|Stand_Talk_5|Bip01_Spine|Bip01_R_Thigh|Bip01_R_Calf|Bip01_R_Foot|Bip01_R_Toe0.s" "|Stand_Talk_5|Bip01_Spine|Bip01_R_Thigh|Bip01_R_Calf|Bip01_R_Foot|Bip01_R_Toe0|_notused_Bip01_R_Toe0Nub.is"
		;
// End of Animation v5 - Stand Talk 5.ma
