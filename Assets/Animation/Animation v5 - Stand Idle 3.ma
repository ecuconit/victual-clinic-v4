//Maya ASCII 2014 scene
//Name: Animation v5 - Stand Idle 3.ma
//Last modified: Tue, Apr 14, 2015 07:28:52 AM
//Codeset: 1252
requires maya "2014";
currentUnit -l centimeter -a degree -t film;
fileInfo "application" "maya";
fileInfo "product" "Maya 2014";
fileInfo "version" "2014 x64";
fileInfo "cutIdentifier" "201303010241-864206";
fileInfo "osv" "Microsoft Windows 7 Enterprise Edition, 64-bit Windows 7 Service Pack 1 (Build 7601)\n";
fileInfo "license" "education";
createNode transform -n "Stand_Idle_3";
	setAttr ".v" no;
createNode joint -n "Bip01_Spine" -p "Stand_Idle_3";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".s" -type "double3" 1 0.99999999999999956 1 ;
	setAttr ".jo" -type "double3" 90.000171 0 89.999999999999986 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" 0.00041359021872633406 0.99974830939735237 0.022430934003442312 0
		 0.70710082718146761 -0.016153575523873306 0.70692820158561975 0 0.70711261418702975 0.015568553398769388 -0.70692953750897569 0
		 -6.7139604917915256e-006 99.443700000000007 -2.2495999999899592 1;
	setAttr -k on ".liw";
	setAttr ".fbxID" 5;
createNode joint -n "Bip01_Spine1" -p "|Stand_Idle_3|Bip01_Spine";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".t" -type "double3" 13.3984 -0.0107 0 ;
	setAttr ".s" -type "double3" 0.99999999999999989 0.99999999999999989 1 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" 0.0038255966723317676 0.015307468004230922 0.99987551536848818 0
		 0.011000641830761762 -0.999822973655216 0.015264574365256002 0 0.99993217304445803 0.010940876315020786 -0.0039933114137866372 0
		 9.9999999999999995e-007 2.2603360000000001 112.84212599999999 1;
	setAttr -k on ".liw";
	setAttr ".fbxID" 5;
createNode joint -n "Bip01_Spine2" -p "|Stand_Idle_3|Bip01_Spine|Bip01_Spine1";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".t" -type "double3" 13.3983 -0.0107 0 ;
	setAttr ".s" -type "double3" 0.99999999999999989 1 1 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" 0 0 1 0 0 -1 0 0 1 0 0 0 -3.9999999999999998e-006 -9.6239790000000003 -19.074804 1;
	setAttr -k on ".liw";
	setAttr ".fbxID" 5;
createNode joint -n "Bip01_Neck" -p "|Stand_Idle_3|Bip01_Spine|Bip01_Spine1|Bip01_Spine2";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".t" -type "double3" 13.3985 -0.007899999999999999 0 ;
	setAttr ".r" -type "double3" 1.1848489498583726e-023 -5.9999999999999985e-006 1.1479959999999994 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" 0 -0.020830999999999999 0.99978299999999998 0 0 -0.99978299999999998 -0.020830999999999999 0
		 1 0 0 0 -1.0000000000000001e-005 -9.6266999999999996 -5.6762839999999999 1;
	setAttr -k on ".liw";
	setAttr ".fbxID" 5;
createNode joint -n "Bip01_Head" -p "|Stand_Idle_3|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".t" -type "double3" 9.9829 0 0 ;
	setAttr ".r" -type "double3" 9.4787915988671744e-023 -1.3000000000000004e-005 4.622390999999995 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" 0 -0.10133399999999999 0.99485299999999999 0 0 -0.99485199999999996 -0.10133399999999999 0
		 1 0 0 0 -4.4000000000000012e-005 -9.8346470000000004 4.3044190000000002 1;
	setAttr -k on ".liw";
	setAttr ".fbxID" 5;
createNode joint -n "_notused_Bip01_HeadNub" -p "|Stand_Idle_3|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_Head";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".t" -type "double3" 18.3537 0 0 ;
	setAttr ".ssc" no;
	setAttr ".fbxID" 5;
createNode joint -n "Bip01_R_Clavicle" -p "|Stand_Idle_3|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".t" -type "double3" -0.8268 -0.6612 -3.9165 ;
	setAttr ".s" -type "double3" 1 1.0000000000000002 1 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" -0.99454799999999999 -9.9454800000000002e-007 -0.104286 0
		 -9.8906500000000008e-007 1 -1.0428599999999999e-007 0 0.104286 -5.7179000000000015e-010 -0.99454799999999999 0
		 -3.9165049999999999 2.9251839999999998 138.82619800000001 1;
	setAttr -k on ".liw";
	setAttr ".fbxID" 5;
createNode joint -n "Bip01_R_UpperArm" -p "|Stand_Idle_3|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_R_Clavicle";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".t" -type "double3" 10.6745 0 0 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" -0.75332600000000005 0.10000199999999999 -0.65000000000000002 0
		 0.071782399999999996 0.99496899999999999 0.0698825 0 0.65371900000000005 0.0059857399999999998 -0.75671500000000003 0
		 -14.532831 2.9251239999999998 137.712976 1;
	setAttr -k on ".liw";
	setAttr ".fbxID" 5;
createNode joint -n "Bip01_R_Forearm" -p "|Stand_Idle_3|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_R_Clavicle|Bip01_R_UpperArm";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".t" -type "double3" 25.3375 0 0 ;
	setAttr ".s" -type "double3" 0.99999999999999989 1 1 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" -0.752861 -0.095945600000000006 -0.65114899999999998 0
		 -0.076500899999999997 0.99536800000000003 -0.058215000000000003 0 0.65371900000000005 0.0059856600000000003 -0.756714 0
		 -33.620182 5.4589869999999996 121.243574 1;
	setAttr -k on ".liw";
	setAttr ".fbxID" 5;
createNode joint -n "Bip01_R_Hand" -p "|Stand_Idle_3|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_R_Clavicle|Bip01_R_UpperArm|Bip01_R_Forearm";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".t" -type "double3" 24.9881 0 0 ;
	setAttr ".r" -type "double3" 90.133325000000013 -6.3004020000000009 2.5653330000000008 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" -0.67922700000000003 -0.050331500000000001 -0.73219999999999996 0
		 0.73278100000000002 0.0092545500000000003 -0.68040299999999998 0 0.0410219 -0.99868900000000005 0.030596000000000002 0
		 -52.432737000000003 3.0614050000000002 104.972679 1;
	setAttr -k on ".liw";
	setAttr ".fbxID" 5;
createNode joint -n "Bip01_R_Finger0" -p "|Stand_Idle_3|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_R_Clavicle|Bip01_R_UpperArm|Bip01_R_Forearm|Bip01_R_Hand";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".t" -type "double3" 2.3036000000000003 1.3941 2.9222 ;
	setAttr ".r" -type "double3" -81.93018 -23.314181 18.295908 ;
	setAttr ".s" -type "double3" 0.99999999999999989 1 1 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" -0.36474400000000001 -0.436471 -0.82246900000000001 0
		 -0.072261699999999998 0.89392799999999994 -0.44234699999999999 0 0.92829899999999999 -0.10191 -0.357595 0
		 -52.855969000000002 0.0400224 102.426821 1;
	setAttr -k on ".liw";
	setAttr ".fbxID" 5;
createNode joint -n "Bip01_R_Finger01" -p "|Stand_Idle_3|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_R_Clavicle|Bip01_R_UpperArm|Bip01_R_Forearm|Bip01_R_Hand|Bip01_R_Finger0";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".t" -type "double3" 4.0806 0 0 ;
	setAttr ".r" -type "double3" 9.4787915988669464e-023 3.0000000000000005e-006 24.064549 ;
	setAttr ".s" -type "double3" 1 1 0.99999999999999989 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" -0.36250900000000003 -0.034022900000000002 -0.93135900000000005 0
		 0.082749500000000004 0.99421099999999996 -0.068527199999999996 0 0.92830000000000001 -0.101911 -0.357595 0
		 -54.344414999999998 -1.7410159999999999 99.070615000000004 1;
	setAttr -k on ".liw";
	setAttr ".fbxID" 5;
createNode joint -n "_notused_Bip01_R_Finger0Nub" -p "|Stand_Idle_3|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_R_Clavicle|Bip01_R_UpperArm|Bip01_R_Forearm|Bip01_R_Hand|Bip01_R_Finger0|Bip01_R_Finger01";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".t" -type "double3" 3.3413 0 0 ;
	setAttr ".r" -type "double3" 1e-006 0 -180 ;
	setAttr ".ssc" no;
	setAttr ".fbxID" 5;
createNode joint -n "Bip01_R_Finger2" -p "|Stand_Idle_3|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_R_Clavicle|Bip01_R_UpperArm|Bip01_R_Forearm|Bip01_R_Hand";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".t" -type "double3" 7.3709 -0.4816 0.6341 ;
	setAttr ".r" -type "double3" 0.043615000000000029 -0.013407000000000007 17.086613999999997 ;
	setAttr ".s" -type "double3" 0.99999999999999989 0.99999999999999989 0.99999999999999989 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" -0.43393399999999999 -0.045623999999999998 -0.89978899999999995 0
		 0.90003599999999995 0.022875199999999998 -0.43521399999999999 0 0.040439000000000003 -0.99869600000000003 0.031137000000000001 0
		 -57.766043000000003 2.0527600000000001 99.922762000000006 1;
	setAttr -k on ".liw";
	setAttr ".fbxID" 5;
createNode joint -n "Bip01_R_Finger21" -p "|Stand_Idle_3|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_R_Clavicle|Bip01_R_UpperArm|Bip01_R_Forearm|Bip01_R_Hand|Bip01_R_Finger2";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".t" -type "double3" 3.8208 0 0 ;
	setAttr ".r" -type "double3" 0 0 29.946389999999987 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" 0.0732873 -0.028113900000000001 -0.99691399999999997 0
		 0.99648999999999999 0.042598999999999998 0.072054900000000005 0 0.0404418 -0.99869600000000003 0.0311372 0
		 -59.424045999999997 1.8784400000000001 96.484806000000006 1;
	setAttr -k on ".liw";
	setAttr ".fbxID" 5;
createNode joint -n "_notused_Bip01_R_Finger2Nub" -p "|Stand_Idle_3|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_R_Clavicle|Bip01_R_UpperArm|Bip01_R_Forearm|Bip01_R_Hand|Bip01_R_Finger2|Bip01_R_Finger21";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".t" -type "double3" 4.7174000000000005 0 0 ;
	setAttr ".r" -type "double3" 0 0 180 ;
	setAttr ".ssc" no;
	setAttr ".fbxID" 5;
createNode joint -n "Bip01_R_Finger1" -p "|Stand_Idle_3|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_R_Clavicle|Bip01_R_UpperArm|Bip01_R_Forearm|Bip01_R_Hand";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".t" -type "double3" 7.3263 -0.211 2.2784 ;
	setAttr ".r" -type "double3" -0.73310399999999953 -5.3145149999999983 22.646732999999994 ;
	setAttr ".s" -type "double3" 1 1 0.99999999999999989 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" -0.33941900000000003 -0.13520499999999999 -0.93086800000000003 0
		 0.93680799999999997 0.040593299999999999 -0.34748099999999998 0 0.084767999999999996 -0.98998600000000003 0.112883 0
		 -57.470132999999997 0.41539700000000002 99.821579 1;
	setAttr -k on ".liw";
	setAttr ".fbxID" 5;
createNode joint -n "Bip01_R_Finger11" -p "|Stand_Idle_3|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_R_Clavicle|Bip01_R_UpperArm|Bip01_R_Forearm|Bip01_R_Hand|Bip01_R_Finger1";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".t" -type "double3" 3.5597999999999996 0 0 ;
	setAttr ".r" -type "double3" -2e-006 3.999999999999999e-006 22.627677999999996 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" 0.047135799999999999 -0.10918 -0.99290400000000001 0
		 0.995286 0.089482699999999998 0.037409400000000002 0 0.084763400000000003 -0.98998600000000003 0.112883 0
		 -58.678344000000003 -0.065903900000000001 96.507863999999998 1;
	setAttr -k on ".liw";
	setAttr ".fbxID" 5;
createNode joint -n "_notused_Bip01_R_Finger1Nub" -p "|Stand_Idle_3|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_R_Clavicle|Bip01_R_UpperArm|Bip01_R_Forearm|Bip01_R_Hand|Bip01_R_Finger1|Bip01_R_Finger11";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".t" -type "double3" 4.1404000000000005 0 0 ;
	setAttr ".r" -type "double3" 0 0 180 ;
	setAttr ".ssc" no;
	setAttr ".fbxID" 5;
createNode joint -n "Bip01_R_Finger3" -p "|Stand_Idle_3|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_R_Clavicle|Bip01_R_UpperArm|Bip01_R_Forearm|Bip01_R_Hand";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".t" -type "double3" 7.2345 -0.5997 -0.9332 ;
	setAttr ".r" -type "double3" 0.043363999999999805 3.826455 18.509659999999997 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" -0.41327900000000001 0.0219578 -0.91034000000000004 0
		 0.91051400000000005 0.0239971 -0.41277999999999998 0 0.0127817 -0.999471 -0.0299104 0
		 -57.824306 3.6237469999999998 100.05504999999999 1;
	setAttr -k on ".liw";
	setAttr ".fbxID" 5;
createNode joint -n "Bip01_R_Finger31" -p "|Stand_Idle_3|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_R_Clavicle|Bip01_R_UpperArm|Bip01_R_Forearm|Bip01_R_Hand|Bip01_R_Finger3";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".t" -type "double3" 3.7241 0 0 ;
	setAttr ".r" -type "double3" 3.0000000000000013e-006 3.0000000000000005e-006 27.984287000000009 ;
	setAttr ".s" -type "double3" 1 0.99999999999999989 0.99999999999999989 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" 0.0622821 0.0306512 -0.99758800000000003 0 0.99797599999999997 0.0108983 0.062641199999999994 0
		 0.012792100000000001 -0.999471 -0.0299104 0 -59.363360999999998 3.705527 96.664748000000003 1;
	setAttr -k on ".liw";
	setAttr ".fbxID" 5;
createNode joint -n "_notused_Bip01_R_Finger3Nub" -p "|Stand_Idle_3|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_R_Clavicle|Bip01_R_UpperArm|Bip01_R_Forearm|Bip01_R_Hand|Bip01_R_Finger3|Bip01_R_Finger31";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".t" -type "double3" 4.2397 0 0 ;
	setAttr ".r" -type "double3" 3e-006 -5e-006 180 ;
	setAttr ".ssc" no;
	setAttr ".fbxID" 5;
createNode joint -n "Bip01_R_Finger4" -p "|Stand_Idle_3|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_R_Clavicle|Bip01_R_UpperArm|Bip01_R_Forearm|Bip01_R_Hand";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".t" -type "double3" 7.2292 -0.4187 -2.3639 ;
	setAttr ".r" -type "double3" -0.17713400000000007 5.5619560000000012 16.615208 ;
	setAttr ".s" -type "double3" 0.99999999999999989 0.99999999999999989 1 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" -0.44323200000000001 0.051426199999999998 -0.89493100000000003 0
		 0.89640699999999995 0.026346700000000001 -0.44244899999999998 0 0.00082491599999999997 -0.99832900000000002 -0.057776500000000001 0
		 -57.746822000000002 5.0544450000000003 99.892002000000005 1;
	setAttr -k on ".liw";
	setAttr ".fbxID" 5;
createNode joint -n "Bip01_R_Finger41" -p "|Stand_Idle_3|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_R_Clavicle|Bip01_R_UpperArm|Bip01_R_Forearm|Bip01_R_Hand|Bip01_R_Finger4";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".t" -type "double3" 3.4791000000000003 0 0 ;
	setAttr ".r" -type "double3" -1.9999999999999995e-006 -3.0000000000000005e-006 29.444368 ;
	setAttr ".s" -type "double3" 1 0.99999999999999989 1 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" 0.054672699999999998 0.057735700000000001 -0.996834 0
		 0.99850399999999995 -0.0023359700000000001 0.054628999999999997 0 0.00082546799999999999 -0.99832900000000002 -0.057777000000000002 0
		 -59.288860999999997 5.2333790000000002 96.778379000000001 1;
	setAttr -k on ".liw";
	setAttr ".fbxID" 5;
createNode joint -n "_notused_Bip01_R_Finger4Nub" -p "|Stand_Idle_3|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_R_Clavicle|Bip01_R_UpperArm|Bip01_R_Forearm|Bip01_R_Hand|Bip01_R_Finger4|Bip01_R_Finger41";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".t" -type "double3" 3.3974 0 0 ;
	setAttr ".r" -type "double3" 0 0 -180 ;
	setAttr ".ssc" no;
	setAttr ".fbxID" 5;
createNode joint -n "Bip01_L_Clavicle" -p "|Stand_Idle_3|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".t" -type "double3" -0.8268 -0.6612 3.9165 ;
	setAttr ".s" -type "double3" 1 1.0000000000000002 1 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" 0.99454799999999999 9.9454800000000002e-007 -0.104286 0
		 -9.8906500000000008e-007 1 1.0428599999999999e-007 0 0.104286 -5.7179000000000015e-010 0.99454799999999999 0
		 3.9165070000000002 2.9252039999999999 138.82619700000001 1;
	setAttr -k on ".liw";
	setAttr ".fbxID" 5;
createNode joint -n "Bip01_L_UpperArm" -p "|Stand_Idle_3|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_L_Clavicle";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".t" -type "double3" 10.6745 0 0 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" 0.75332500000000002 0.100004 -0.65000000000000002 0
		 -0.071785600000000005 0.99496899999999999 0.069882299999999994 0 0.65371900000000005 -0.0059834299999999997 0.756714 0
		 14.532825000000001 2.925173 137.712974 1;
	setAttr -k on ".liw";
	setAttr ".fbxID" 5;
createNode joint -n "Bip01_L_Forearm" -p "|Stand_Idle_3|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_L_Clavicle|Bip01_L_UpperArm";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".t" -type "double3" 25.3374 0 0 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" 0.752861 -0.095943299999999995 -0.65114899999999998 0
		 0.076497700000000002 0.99536899999999995 -0.058215299999999998 0 0.65371900000000005 -0.0059834199999999997 0.756714 0
		 33.620189000000003 5.4590990000000001 121.243576 1;
	setAttr -k on ".liw";
	setAttr ".fbxID" 5;
createNode joint -n "Bip01_L_Hand" -p "|Stand_Idle_3|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_L_Clavicle|Bip01_L_UpperArm|Bip01_L_Forearm";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".t" -type "double3" 24.9881 0 0 ;
	setAttr ".r" -type "double3" -90.133325 6.300397000000002 2.565332999999999 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" 0.67922800000000005 -0.050329499999999999 -0.73219999999999996 0
		 -0.73278100000000002 0.0092525000000000003 -0.68040299999999998 0 0.041019 0.99868999999999997 -0.030595899999999999 0
		 52.432744999999997 3.0615610000000002 104.972662 1;
	setAttr -k on ".liw";
	setAttr ".fbxID" 5;
createNode joint -n "Bip01_L_Finger0" -p "|Stand_Idle_3|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_L_Clavicle|Bip01_L_UpperArm|Bip01_L_Forearm|Bip01_L_Hand";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".t" -type "double3" 2.3036000000000003 1.3941 -2.9222 ;
	setAttr ".r" -type "double3" 81.930180000000021 23.314181000000019 18.295909 ;
	setAttr ".s" -type "double3" 0.99999999999999989 1 1.0000000000000002 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" 0.36474600000000001 -0.43647000000000002 -0.82246900000000001 0
		 0.072258799999999998 0.89392799999999994 -0.44234699999999999 0 0.92830000000000001 0.101913 0.357595 0
		 52.856034999999999 0.0402022 102.42681 1;
	setAttr -k on ".liw";
	setAttr ".fbxID" 5;
createNode joint -n "Bip01_L_Finger01" -p "|Stand_Idle_3|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_L_Clavicle|Bip01_L_UpperArm|Bip01_L_Forearm|Bip01_L_Hand|Bip01_L_Finger0";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".t" -type "double3" 4.0806 0 0 ;
	setAttr ".r" -type "double3" 0 0 24.06455200000001 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" 0.36250900000000003 -0.034021900000000001 -0.93135900000000005 0
		 -0.082751900000000003 0.99421199999999998 -0.068527000000000005 0 0.92829899999999999 0.101913 0.357595 0
		 54.344436000000002 -1.7408459999999999 99.070606999999995 1;
	setAttr -k on ".liw";
	setAttr ".fbxID" 5;
createNode joint -n "_notused_Bip01_L_Finger0Nub" -p "|Stand_Idle_3|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_L_Clavicle|Bip01_L_UpperArm|Bip01_L_Forearm|Bip01_L_Hand|Bip01_L_Finger0|Bip01_L_Finger01";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".t" -type "double3" 3.3413 0 0 ;
	setAttr ".ssc" no;
	setAttr ".fbxID" 5;
createNode joint -n "Bip01_L_Finger2" -p "|Stand_Idle_3|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_L_Clavicle|Bip01_L_UpperArm|Bip01_L_Forearm|Bip01_L_Hand";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".t" -type "double3" 7.3709 -0.4816 -0.6341 ;
	setAttr ".r" -type "double3" -0.043610000000000031 0.01341 17.086619000000006 ;
	setAttr ".s" -type "double3" 1 0.99999999999999989 0.99999999999999989 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" 0.43393500000000002 -0.045622700000000002 -0.89978899999999995 0
		 -0.90003599999999995 0.022871599999999999 -0.43521399999999999 0 0.040435199999999998 0.99869699999999995 -0.031137399999999999 0
		 57.766060000000003 2.0529269999999999 99.922754999999995 1;
	setAttr -k on ".liw";
	setAttr ".fbxID" 5;
createNode joint -n "Bip01_L_Finger21" -p "|Stand_Idle_3|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_L_Clavicle|Bip01_L_UpperArm|Bip01_L_Forearm|Bip01_L_Hand|Bip01_L_Finger2";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".t" -type "double3" 3.8208 0 0 ;
	setAttr ".r" -type "double3" -4.0000000000000024e-006 -4.0000000000000015e-006 29.946390000000019 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" -0.0732873 -0.0281141 -0.99691399999999997 0 -0.99649100000000002 0.042592999999999999 0.072054999999999994 0
		 0.040435899999999997 0.99869699999999995 -0.031137000000000001 0 59.424058000000002 1.878614 96.484800000000007 1;
	setAttr -k on ".liw";
	setAttr ".fbxID" 5;
createNode joint -n "_notused_Bip01_L_Finger2Nub" -p "|Stand_Idle_3|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_L_Clavicle|Bip01_L_UpperArm|Bip01_L_Forearm|Bip01_L_Hand|Bip01_L_Finger2|Bip01_L_Finger21";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".t" -type "double3" 4.7174000000000005 0 0 ;
	setAttr ".ssc" no;
	setAttr ".fbxID" 5;
createNode joint -n "Bip01_L_Finger1" -p "|Stand_Idle_3|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_L_Clavicle|Bip01_L_UpperArm|Bip01_L_Forearm|Bip01_L_Hand";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".t" -type "double3" 7.3263 -0.211 -2.2784 ;
	setAttr ".r" -type "double3" 0.73310799999999965 5.3145129999999989 22.646742999999997 ;
	setAttr ".s" -type "double3" 1 0.99999999999999989 0.99999999999999989 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" 0.33942 -0.13520399999999999 -0.93086800000000003 0
		 -0.93680799999999997 0.040590800000000003 -0.34748099999999998 0 0.084765400000000005 0.98998600000000003 -0.112883 0
		 57.470143999999998 0.415574 99.821611000000004 1;
	setAttr -k on ".liw";
	setAttr ".fbxID" 5;
createNode joint -n "Bip01_L_Finger11" -p "|Stand_Idle_3|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_L_Clavicle|Bip01_L_UpperArm|Bip01_L_Forearm|Bip01_L_Hand|Bip01_L_Finger1";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".t" -type "double3" 3.5597999999999996 0 0 ;
	setAttr ".r" -type "double3" -2.0000000000000012e-006 3.0000000000000005e-006 22.627677000000009 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" -0.047135499999999997 -0.10918 -0.99290400000000001 0
		 -0.99528499999999998 0.089479600000000006 0.037409299999999999 0 0.0847604 0.98998699999999995 -0.112883 0
		 58.678319000000002 -0.065726099999999996 96.507845000000003 1;
	setAttr -k on ".liw";
	setAttr ".fbxID" 5;
createNode joint -n "_notused_Bip01_L_Finger1Nub" -p "|Stand_Idle_3|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_L_Clavicle|Bip01_L_UpperArm|Bip01_L_Forearm|Bip01_L_Hand|Bip01_L_Finger1|Bip01_L_Finger11";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".t" -type "double3" 4.1404000000000005 0 0 ;
	setAttr ".ssc" no;
	setAttr ".fbxID" 5;
createNode joint -n "Bip01_L_Finger3" -p "|Stand_Idle_3|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_L_Clavicle|Bip01_L_UpperArm|Bip01_L_Forearm|Bip01_L_Hand";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".t" -type "double3" 7.2345 -0.5997 0.9332 ;
	setAttr ".r" -type "double3" -0.043362000000000123 -3.826458 18.509666999999997 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" 0.41327900000000001 0.021958700000000001 -0.91034000000000004 0
		 -0.91051400000000005 0.0239966 -0.41277999999999998 0 0.012781000000000001 0.999471 0.029911 0
		 57.824314000000001 3.6238670000000002 100.055049 1;
	setAttr -k on ".liw";
	setAttr ".fbxID" 5;
createNode joint -n "Bip01_L_Finger31" -p "|Stand_Idle_3|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_L_Clavicle|Bip01_L_UpperArm|Bip01_L_Forearm|Bip01_L_Hand|Bip01_L_Finger3";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".t" -type "double3" 3.7241 0 0 ;
	setAttr ".r" -type "double3" 2e-006 -9.4787915988669346e-023 27.984288 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" -0.062282200000000003 0.030651100000000001 -0.99758800000000003 0
		 -0.99797599999999997 0.0108933 0.062641199999999994 0 0.012787099999999999 0.999471 0.029910599999999999 0
		 59.363363 3.7056490000000002 96.664751999999993 1;
	setAttr -k on ".liw";
	setAttr ".fbxID" 5;
createNode joint -n "_notused_Bip01_L_Finger3Nub" -p "|Stand_Idle_3|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_L_Clavicle|Bip01_L_UpperArm|Bip01_L_Forearm|Bip01_L_Hand|Bip01_L_Finger3|Bip01_L_Finger31";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".t" -type "double3" 4.2397 0 0 ;
	setAttr ".ssc" no;
	setAttr ".fbxID" 5;
createNode joint -n "Bip01_L_Finger4" -p "|Stand_Idle_3|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_L_Clavicle|Bip01_L_UpperArm|Bip01_L_Forearm|Bip01_L_Hand";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".t" -type "double3" 7.2292 -0.4187 2.3639 ;
	setAttr ".r" -type "double3" 0.17713199999999998 -5.5619539999999983 16.615218000000002 ;
	setAttr ".s" -type "double3" 1 0.99999999999999989 1 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" 0.44323200000000001 0.051427599999999997 -0.89493100000000003 0
		 -0.89640600000000004 0.026343800000000001 -0.44244899999999998 0 0.00082181400000000003 0.99832900000000002 0.057776399999999999 0
		 57.746775 5.054627 99.891990000000007 1;
	setAttr -k on ".liw";
	setAttr ".fbxID" 5;
createNode joint -n "Bip01_L_Finger41" -p "|Stand_Idle_3|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_L_Clavicle|Bip01_L_UpperArm|Bip01_L_Forearm|Bip01_L_Hand|Bip01_L_Finger4";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".t" -type "double3" 3.4791000000000003 0 0 ;
	setAttr ".r" -type "double3" 2e-006 -3.0000000000000009e-006 29.444363999999997 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" -0.054672800000000001 0.057735500000000002 -0.996834 0
		 -0.99850399999999995 -0.00234096 0.054628799999999998 0 0.00082047800000000003 0.99832900000000002 0.057777200000000001 0
		 59.288862000000002 5.2335589999999996 96.778389000000004 1;
	setAttr -k on ".liw";
	setAttr ".fbxID" 5;
createNode joint -n "_notused_Bip01_L_Finger4Nub" -p "|Stand_Idle_3|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_L_Clavicle|Bip01_L_UpperArm|Bip01_L_Forearm|Bip01_L_Hand|Bip01_L_Finger4|Bip01_L_Finger41";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".t" -type "double3" 3.3974 0 0 ;
	setAttr ".ssc" no;
	setAttr ".fbxID" 5;
createNode joint -n "Bip01_L_Thigh" -p "|Stand_Idle_3|Bip01_Spine";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".t" -type "double3" -11.0456 2.2584 8.5201999999999991 ;
	setAttr ".s" -type "double3" 0.99999999999999989 0.99999999999999989 0.99999999999999989 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" 0.059694200000000003 0.0067868399999999997 -0.998193 0
		 0.0013515700000000001 -0.99997599999999998 -0.00671814 0 -0.99821599999999999 -0.00094809699999999996 -0.059701999999999998 0
		 8.5202019999999994 4.3064900000000002e-005 88.396292000000003 1;
	setAttr -k on ".liw";
	setAttr ".fbxID" 5;
createNode joint -n "Bip01_L_Calf" -p "|Stand_Idle_3|Bip01_Spine|Bip01_L_Thigh";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".t" -type "double3" 36.1527 0 0 ;
	setAttr ".s" -type "double3" 1 0.99999999999999989 0.99999999999999989 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" 0.059157700000000001 0.119882 -0.99102400000000002 0
		 0.0080950799999999993 -0.99278699999999998 -0.119612 0 -0.99821599999999999 -0.00094644599999999998 -0.059701400000000002 0
		 10.678299000000001 0.245397 52.308965999999998 1;
	setAttr -k on ".liw";
	setAttr ".fbxID" 5;
createNode joint -n "Bip01_L_Foot" -p "|Stand_Idle_3|Bip01_Spine|Bip01_L_Thigh|Bip01_L_Calf";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".t" -type "double3" 43.2543 0 0 ;
	setAttr ".s" -type "double3" 0.99999999999999989 1 1 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" 0 0 -1 0 0.123471 -0.99234800000000001 0 0 -0.99234800000000001 -0.123471 0 0
		 13.237131 5.4308170000000002 9.4429110000000005 1;
	setAttr -k on ".liw";
	setAttr ".fbxID" 5;
createNode joint -n "Bip01_L_Toe0" -p "|Stand_Idle_3|Bip01_Spine|Bip01_L_Thigh|Bip01_L_Calf|Bip01_L_Foot";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".t" -type "double3" 9.4714999999999989 11.2377 0 ;
	setAttr ".r" -type "double3" -5.000000000000003e-006 1.9999999999999995e-006 89.999979999999979 ;
	setAttr ".s" -type "double3" 0.99999999999999989 1 1 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" 0.123471 -0.99234800000000001 0 0 0 0 1 0 -0.99234800000000001 -0.123471 0 0
		 14.624665999999999 -5.7209120000000002 -0.028559000000000001 1;
	setAttr -k on ".liw";
	setAttr ".fbxID" 5;
createNode joint -n "_notused_Bip01_L_Toe0Nub" -p "|Stand_Idle_3|Bip01_Spine|Bip01_L_Thigh|Bip01_L_Calf|Bip01_L_Foot|Bip01_L_Toe0";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".t" -type "double3" 7.2685 0 0 ;
	setAttr ".r" -type "double3" 0 0 180 ;
	setAttr ".ssc" no;
	setAttr ".fbxID" 5;
createNode joint -n "Bip01_R_Thigh" -p "|Stand_Idle_3|Bip01_Spine";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".t" -type "double3" -11.0456 2.2585 -8.5201999999999991 ;
	setAttr ".s" -type "double3" 0.99999999999999956 1 0.99999999999999989 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" -0.059694200000000003 0.0067826700000000002 -0.998193 0
		 -0.00134545 -0.99997599999999998 -0.0067143300000000001 0 -0.99821599999999999 0.00094221699999999997 0.059701900000000002 0
		 -8.5201879999999992 1.40303e-005 88.396298000000002 1;
	setAttr -k on ".liw";
	setAttr ".fbxID" 5;
createNode joint -n "Bip01_R_Calf" -p "|Stand_Idle_3|Bip01_Spine|Bip01_R_Thigh";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".t" -type "double3" 36.1527 0 0 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" -0.059158000000000002 0.11988699999999999 -0.99102400000000002 0
		 -0.0080972100000000005 -0.99278699999999998 -0.119617 0 -0.99821599999999999 0.00094822899999999998 0.059701999999999998 0
		 -10.678288 0.24518599999999999 52.308996999999998 1;
	setAttr -k on ".liw";
	setAttr ".fbxID" 5;
createNode joint -n "Bip01_R_Foot" -p "|Stand_Idle_3|Bip01_Spine|Bip01_R_Thigh|Bip01_R_Calf";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".t" -type "double3" 43.2543 0 0 ;
	setAttr ".s" -type "double3" 1 1 0.99999999999999989 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" 0 0 -1 0 -0.123469 -0.99234800000000001 0 0 -0.99234800000000001 0.123469 0 0
		 -13.237124 5.4308209999999999 9.442933 1;
	setAttr -k on ".liw";
	setAttr ".fbxID" 5;
createNode joint -n "Bip01_R_Toe0" -p "|Stand_Idle_3|Bip01_Spine|Bip01_R_Thigh|Bip01_R_Calf|Bip01_R_Foot";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".t" -type "double3" 9.4714999999999989 11.2377 0 ;
	setAttr ".r" -type "double3" 1.9999999999999991e-006 -1.9999999999999995e-006 89.999986 ;
	setAttr ".s" -type "double3" 0.99999999999999989 0.99999999999999989 1 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" -0.12346799999999999 -0.99234800000000001 0 0 0 0 1 0
		 -0.99234800000000001 0.12346799999999999 0 0 -14.624625999999999 -5.7209019999999997 -0.028538999999999998 1;
	setAttr -k on ".liw";
	setAttr ".fbxID" 5;
createNode joint -n "_notused_Bip01_R_Toe0Nub" -p "|Stand_Idle_3|Bip01_Spine|Bip01_R_Thigh|Bip01_R_Calf|Bip01_R_Foot|Bip01_R_Toe0";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".t" -type "double3" 7.2685 0 0 ;
	setAttr ".ssc" no;
	setAttr ".fbxID" 5;
createNode animCurveTL -n "Bip01_Spine_translateX5";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 91 ".ktv[0:90]"  0.4 3.1558022499084473 1.2 2.9487178325653076
		 2 2.6513671875 2.8 2.2646172046661377 3.6 1.8779704570770264 4.4 1.4783130884170532
		 5.2 0.99091196060180675 6 0.50245893001556396 6.8 0.12747305631637573 7.6 -0.18593877553939819
		 8.4 -0.52067983150482178 9.2 -0.85466593503952037 10 -1.1162289381027222 10.8 -1.2880566120147705
		 11.6 -1.424567699432373 12.4 -1.5505443811416626 13.2 -1.6399819850921633 14 -1.6949288845062256
		 14.8 -1.7340004444122314 15.6 -1.7567633390426636 16.4 -1.7666840553283691 17.2 -1.7723032236099243
		 18 -1.7618440389633181 18.8 -1.7229324579238892 19.6 -1.6699897050857544 20.4 -1.6180938482284546
		 21.2 -1.5553282499313354 22 -1.4598944187164309 22.8 -1.3249771595001221 23.6 -1.1476441621780396
		 24.4 -0.92114055156707764 25.2 -0.6457248330116272 26 -0.33622947335243225 26.8 -0.0092161353677511215
		 27.6 0.34794509410858154 28.4 0.7038845419883728 29.2 1.0008034706115725 30 1.3503711223602295
		 30.8 1.8525217771530151 31.6 2.3376209735870361 32.4 2.6972389221191406 33.2 3.0489096641540527
		 34 3.4171695709228516 34.8 3.7359087467193599 35.6 4.0184712409973145 36.4 4.2854247093200684
		 37.2 4.5301089286804199 38 4.762791633605957 38.8 5.02703857421875 39.6 5.3879218101501465
		 40.4 5.8298201560974121 41.2 6.2521939277648926 42 6.6519556045532227 42.8 7.0684905052185059
		 43.6 7.4036111831665039 44.4 7.5890092849731436 45.2 7.711845874786377 46 7.8303689956665039
		 46.8 7.9225525856018066 47.6 7.9934701919555664 48.4 8.0609579086303711 49.2 8.1106243133544922
		 50 8.1337680816650391 50.8 8.1445226669311523 51.6 8.1544589996337891 52.4 8.1735877990722656
		 53.2 8.205510139465332 54 8.2305698394775391 54.8 8.2267723083496094 55.6 8.2012310028076172
		 56.4 8.176081657409668 57.2 8.145599365234375 58 8.0906639099121094 58.8 8.0261716842651367
		 59.6 7.9765567779541025 60.4 7.9245181083679199 61.2 7.8446125984191886 62 7.7518367767333984
		 62.8 7.6445331573486337 63.6 7.4771728515625 64.4 7.264899730682373 65.2 7.0799984931945801
		 66 6.9021759033203125 66.8 6.6521034240722656 67.6 6.3342013359069824 68.4 5.9944319725036621
		 69.2 5.6462883949279785 70 5.2870817184448242 70.8 4.9319009780883789 71.6 4.6071557998657227
		 72.4 4.314178466796875;
createNode animCurveTL -n "Bip01_Spine_translateY5";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 91 ".ktv[0:90]"  0.4 102.07866668701172 1.2 102.07486724853516
		 2 102.07107543945312 2.8 102.06062316894533 3.6 102.04049682617187 4.4 102.00936126708984
		 5.2 101.97030639648436 6 101.93183135986328 6.8 101.89916229248048 7.6 101.8687744140625
		 8.4 101.83432006835936 9.2 101.79502868652344 10 101.75791168212892 10.8 101.72883605957033
		 11.6 101.70485687255859 12.4 101.68536376953124 13.2 101.67222595214844 14 101.66159057617187
		 14.8 101.652587890625 15.6 101.64664459228516 16.4 101.64249420166016 17.2 101.64183807373048
		 18 101.6447296142578 18.8 101.6482391357422 19.6 101.65282440185548 20.4 101.65703582763672
		 21.2 101.66230010986328 22 101.67396545410156 22.8 101.68833923339844 23.6 101.70522308349608
		 24.4 101.7317352294922 25.2 101.76369476318359 26 101.79367828369141 26.8 101.82453918457033
		 27.6 101.86058807373048 28.4 101.89659881591795 29.2 101.92189788818359 30 101.93555450439452
		 30.8 101.94454956054687 31.6 101.95237731933594 32.4 101.9597625732422 33.2 101.96535491943359
		 34 101.96620941162108 34.8 101.96341705322266 35.6 101.95888519287108 36.4 101.9502410888672
		 37.2 101.93658447265624 38 101.92145538330078 38.8 101.90292358398436 39.6 101.87289428710936
		 40.4 101.83504486083984 41.2 101.80203247070312 42 101.77211761474608 42.8 101.74142456054687
		 43.6 101.71958160400392 44.4 101.70777130126952 45.2 101.69680786132812 46 101.68665313720705
		 46.8 101.6824951171875 47.6 101.682861328125 48.4 101.68158721923828 49.2 101.67697906494141
		 50 101.67580413818359 50.8 101.68167877197266 51.6 101.68806457519533 52.4 101.69205474853516
		 53.2 101.69732666015624 54 101.70628356933594 54.8 101.71941375732422 55.6 101.73247528076172
		 56.4 101.74335479736328 57.2 101.75749969482422 58 101.77536010742187 58.8 101.79222869873048
		 59.6 101.80867767333984 60.4 101.82895660400392 61.2 101.85479736328124 62 101.88056182861328
		 62.8 101.90290832519533 63.6 101.9286651611328 64.4 101.95594787597656 65.2 101.97568511962892
		 66 101.99221038818359 66.8 102.01475524902344 67.6 102.04268646240234 68.4 102.06960296630859
		 69.2 102.09164428710936 70 102.10971069335936 70.8 102.12091827392578 71.6 102.1176300048828
		 72.4 102.10532379150392;
createNode animCurveTL -n "Bip01_Spine_translateZ5";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 91 ".ktv[0:90]"  0.4 31.158744812011719 1.2 31.420717239379883
		 2 31.778215408325195 2.8 32.216880798339844 3.6 32.636009216308594 4.4 33.048175811767578
		 5.2 33.533557891845703 6 34.013790130615234 6.8 34.379856109619141 7.6 34.670841217041016
		 8.4 34.946277618408203 9.2 35.188705444335938 10 35.360633850097656 10.8 35.463909149169922
		 11.6 35.542373657226563 12.4 35.612709045410156 13.2 35.654888153076172 14 35.668716430664062
		 14.8 35.675006866455078 15.6 35.684391021728516 16.4 35.679859161376953 17.2 35.652671813964844
		 18 35.615451812744141 18.8 35.556632995605469 19.6 35.461887359619141 20.4 35.347797393798828
		 21.2 35.223880767822266 22 35.083530426025391 22.8 34.91845703125 23.6 34.688026428222656
		 24.4 34.361537933349609 25.2 33.983600616455078 26 33.61328125 26.8 33.257865905761719
		 27.6 32.876590728759766 28.4 32.472511291503906 29.2 32.101299285888672 30 31.700981140136719
		 30.8 31.210563659667969 31.6 30.775327682495117 32.4 30.464532852172852 33.2 30.141096115112305
		 34 29.77886962890625 34.8 29.474361419677734 35.6 29.235700607299805 36.4 29.020305633544918
		 37.2 28.820684432983398 38 28.633087158203125 38.8 28.412731170654297 39.6 28.094696044921875
		 40.4 27.697797775268555 41.2 27.317939758300781 42 26.964744567871097 42.8 26.614625930786133
		 43.6 26.345130920410156 44.4 26.195640563964844 45.2 26.093500137329105 46 25.991474151611328
		 46.8 25.907072067260746 47.6 25.852497100830082 48.4 25.811557769775391 49.2 25.768787384033203
		 50 25.734928131103516 50.8 25.731044769287109 51.6 25.753311157226559 52.4 25.778505325317383
		 53.2 25.800277709960937 54 25.831148147583008 54.8 25.873838424682617 55.6 25.922172546386719
		 56.4 25.983955383300781 57.2 26.07282829284668 58 26.173046112060547 58.8 26.253747940063477
		 59.6 26.325090408325195 60.4 26.421806335449219 61.2 26.546157836914062 62 26.680807113647461
		 62.8 26.84686279296875 63.6 27.089387893676761 64.4 27.374357223510746 65.2 27.606687545776367
		 66 27.793510437011719 66.8 28.01953125 67.6 28.281503677368164 68.4 28.531396865844727
		 69.2 28.776176452636719 70 29.031589508056641 70.8 29.273807525634769 71.6 29.472246170043945
		 72.4 29.63507080078125;
createNode animCurveTA -n "Bip01_Spine_rotateX5";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 91 ".ktv[0:90]"  0.4 52.732543945312507 1.2 53.231098175048835
		 2 53.506427764892578 2.8 53.793159484863281 3.6 54.087886810302734 4.4 54.263206481933594
		 5.2 54.249107360839837 6 54.109222412109375 6.8 53.964401245117188 7.6 53.893608093261719
		 8.4 53.903873443603523 9.2 53.950839996337891 10 53.993888854980469 10.8 54.021999359130859
		 11.6 54.029609680175781 12.4 54.012554168701165 13.2 53.96295166015625 14 53.840618133544922
		 14.8 53.584918975830078 15.6 53.211143493652351 16.4 52.839168548583984 17.2 52.556766510009766
		 18 52.350006103515625 18.8 52.178417205810547 19.6 52.015663146972656 20.4 51.842548370361328
		 21.2 51.637157440185547 22 51.307331085205078 22.8 50.729129791259766 23.6 49.993984222412109
		 24.4 49.389888763427734 25.2 49.036914825439453 26 48.819255828857422 26.8 48.621738433837891
		 27.6 48.403961181640625 28.4 48.156475067138672 29.2 47.906013488769538 30 47.686920166015625
		 30.8 47.455703735351562 31.6 47.082118988037109 32.4 46.528251647949219 33.2 45.978763580322266
		 34 45.609714508056641 34.8 45.371471405029297 35.6 45.156528472900391 36.4 44.950309753417969
		 37.2 44.759380340576179 38 44.578342437744141 38.8 44.410037994384766 39.6 44.251274108886719
		 40.4 44.079856872558594 41.2 43.857387542724609 42 43.582405090332031 42.8 43.333938598632813
		 43.6 43.181613922119155 44.4 43.105850219726562 45.2 43.067256927490234 46 43.054988861083984
		 46.8 43.048675537109375 47.6 43.020854949951165 48.4 42.973052978515625 49.2 42.925727844238281
		 50 42.886966705322266 50.8 42.857158660888672 51.6 42.842796325683594 52.4 42.843173980712891
		 53.2 42.847068786621094 54 42.848941802978509 54.8 42.852165222167969 55.6 42.862945556640625
		 56.4 42.883075714111328 57.2 42.907691955566406 58 42.934104919433601 58.8 42.963901519775391
		 59.6 42.9986572265625 60.4 43.036197662353523 61.2 43.069572448730469 62 43.097572326660163
		 62.8 43.134757995605469 63.6 43.19866943359375 64.4 43.273563385009766 65.2 43.320602416992195
		 66 43.343994140625 66.8 43.383544921875 67.6 43.447376251220703 68.4 43.524009704589837
		 69.2 43.625282287597656 70 43.785507202148445 70.8 44.053485870361328 71.6 44.413303375244141
		 72.4 44.744117736816406;
createNode animCurveTA -n "Bip01_Spine_rotateY5";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 91 ".ktv[0:90]"  0.4 -1.7574138641357422 1.2 -1.8144758939743044
		 2 -1.8378489017486579 2.8 -1.8569421768188477 3.6 -1.8746960163116453 4.4 -1.9029529094696047
		 5.2 -1.927936315536499 6 -1.9234257936477657 6.8 -1.8991227149963379 7.6 -1.8786414861679077
		 8.4 -1.8705947399139404 9.2 -1.8588908910751341 10 -1.8262962102890012 10.8 -1.784614086151123
		 11.6 -1.7461676597595217 12.4 -1.7061870098114014 13.2 -1.659929633140564 14 -1.5980077981948853
		 14.8 -1.5099235773086548 15.6 -1.4090579748153689 16.4 -1.3218437433242798 17.2 -1.2516056299209597
		 18 -1.1875306367874146 18.8 -1.1314940452575684 19.6 -1.0849738121032717 20.4 -1.0392787456512451
		 21.2 -0.99149936437606812 22 -0.92415100336074829 22.8 -0.79354572296142578 23.6 -0.61273610591888428
		 24.4 -0.46843269467353815 25.2 -0.39784827828407288 26 -0.36120805144309998 26.8 -0.32873564958572393
		 27.6 -0.29522073268890386 28.4 -0.25469362735748291 29.2 -0.20624245703220367 30 -0.15100774168968201
		 30.8 -0.075154818594455705 31.6 0.050845984369516373 32.4 0.21703706681728363 33.2 0.35179364681243896
		 34 0.4242871105670929 34.8 0.47729018330574025 35.6 0.53771930932998657 36.4 0.60066115856170654
		 37.2 0.66564112901687622 38 0.72932898998260498 38.8 0.7913888692855835 39.6 0.85947322845458984
		 40.4 0.91691845655441284 41.2 0.95268756151199341 42 0.98952853679656971 42.8 1.0229103565216064
		 43.6 1.022546172142029 44.4 0.98734933137893699 45.2 0.93460506200790416 46 0.87478142976760864
		 46.8 0.81521552801132224 47.6 0.76211643218994141 48.4 0.70949405431747437 49.2 0.64455926418304443
		 50 0.56691449880599976 50.8 0.4835213422775268 51.6 0.39594352245330811 52.4 0.30360400676727295
		 53.2 0.2094118595123291 54 0.11743929237127303 54.8 0.024303926154971123 55.6 -0.078811563551425934
		 56.4 -0.19046244025230408 57.2 -0.30119386315345764 58 -0.40948867797851563 58.8 -0.51787436008453369
		 59.6 -0.62423253059387207 60.4 -0.72660869359970093 61.2 -0.82615840435028076 62 -0.92425590753555298
		 62.8 -1.0212043523788452 63.6 -1.1139508485794067 64.4 -1.1937246322631836 65.2 -1.2604258060455322
		 66 -1.3286389112472534 66.8 -1.3995015621185305 67.6 -1.4566645622253418 68.4 -1.4999254941940308
		 69.2 -1.5462650060653689 70 -1.5969909429550171 70.8 -1.6389650106430054 71.6 -1.6684263944625854
		 72.4 -1.6881765127182009;
createNode animCurveTA -n "Bip01_Spine_rotateZ5";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 91 ".ktv[0:90]"  0.4 2.1165554523468018 1.2 2.1001322269439697
		 2 2.0712192058563232 2.8 2.0177862644195557 3.6 1.9481946229934699 4.4 1.8651361465454106
		 5.2 1.7865693569183352 6 1.7298508882522583 6.8 1.6837973594665527 7.6 1.6337080001831057
		 8.4 1.5732870101928711 9.2 1.5098671913146973 10 1.4582957029342651 10.8 1.4134106636047363
		 11.6 1.3627630472183228 12.4 1.3089114427566528 13.2 1.2573473453521729 14 1.2009601593017578
		 14.8 1.130597710609436 15.6 1.0600440502166748 16.4 1.0116387605667114 17.2 0.97676628828048706
		 18 0.93471902608871471 18.8 0.88958078622818004 19.6 0.85310095548629761 20.4 0.82660412788391124
		 21.2 0.80192416906356812 22 0.76017642021179199 22.8 0.69305282831192017 23.6 0.62729877233505249
		 24.4 0.59598767757415771 25.2 0.59647500514984131 26 0.60762518644332886 26.8 0.61639201641082775
		 27.6 0.615317702293396 28.4 0.60667151212692261 29.2 0.59883689880371094 30 0.59509950876235962
		 30.8 0.59333372116088867 31.6 0.58511626720428467 32.4 0.55524241924285889 33.2 0.4927434921264649
		 34 0.41451400518417364 34.8 0.3529401421546936 35.6 0.31336823105812073 36.4 0.27917146682739258
		 37.2 0.24189938604831698 38 0.20479957759380341 38.8 0.17919065058231354 39.6 0.16933445632457733
		 40.4 0.1524057537317276 41.2 0.10552343726158142 42 0.0415521040558815 42.8 -0.0080460086464881897
		 43.6 -0.022194966673851013 44.4 -0.00043651796295307577 45.2 0.044613555073738098
		 46 0.1038532555103302 46.8 0.16956193745136261 47.6 0.23472483456134799 48.4 0.30385097861289978
		 49.2 0.3849856853485108 50 0.4761563241481781 50.8 0.57191985845565796 51.6 0.67073321342468262
		 52.4 0.77147471904754639 53.2 0.87519031763076804 54 0.9862942099571228 54.8 1.1034559011459353
		 55.6 1.220730185508728 56.4 1.3371204137802124 57.2 1.4539060592651367 58 1.5702698230743408
		 58.8 1.684418797492981 59.6 1.7938451766967773 60.4 1.8985530138015747 61.2 2.0043654441833496
		 62 2.1155781745910645 62.8 2.2252252101898193 63.6 2.3178231716156006 64.4 2.381289005279541
		 65.2 2.4233274459838867 66 2.4688081741333008 66.8 2.527428150177002 67.6 2.5844409465789795
		 68.4 2.6280303001403809 69.2 2.6594200134277344 70 2.6739127635955811 70.8 2.657686710357666
		 71.6 2.6131234169006348 72.4 2.5610692501068115;
createNode animCurveTA -n "Bip01_Spine1_rotateX5";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 91 ".ktv[0:90]"  0.4 1.8465458154678345 1.2 1.8803180456161499
		 2 1.9477623701095579 2.8 1.9737640619277956 3.6 1.9982186555862429 4.4 2.048363208770752
		 5.2 2.0669574737548828 6 2.0859482288360596 6.8 2.099846363067627 7.6 2.1196179389953613
		 8.4 2.1240520477294922 9.2 2.126821756362915 10 2.12491774559021 10.8 2.1153874397277832
		 11.6 2.1098394393920894 12.4 2.084420919418335 13.2 2.0684077739715576 14 2.0456507205963135
		 14.8 2.0185613632202148 15.6 1.9885567426681523 16.4 1.9559820890426636 17.2 1.9211506843566899
		 18 1.8844108581542971 18.8 1.8461253643035889 19.6 1.8066848516464231 20.4 1.766426682472229
		 21.2 1.7256462574005127 22 1.6846623420715334 22.8 1.643808126449585 23.6 1.6033563613891602
		 24.4 1.5635359287261963 25.2 1.5245662927627563 26 1.4888225793838501 26.8 1.4608451128005979
		 27.6 1.4046050310134888 28.4 1.3872133493423462 29.2 1.3368045091629028 30 1.3205746412277224
		 30.8 1.2735061645507812 31.6 1.2601020336151123 32.4 1.2214183807373049 33.2 1.1823717355728147
		 34 1.1650428771972656 34.8 1.1477677822113037 35.6 1.1104080677032473 36.4 1.0753815174102783
		 37.2 1.0639076232910156 38 1.0234217643737793 38.8 1.0059120655059814 39.6 0.98855298757553089
		 40.4 0.94790500402450562 41.2 0.92942541837692261 42 0.90822899341583252 42.8 0.8905785083770752
		 43.6 0.8564334511756897 44.4 0.846385657787323 45.2 0.81866443157196067 46 0.8102622628211974
		 46.8 0.78704726696014404 47.6 0.77816879749298107 48.4 0.7690359354019165 49.2 0.76180768013000477
		 50 0.7575049400329591 50.8 0.75618070363998413 51.6 0.75790369510650635 52.4 0.76272457838058461
		 53.2 0.77060168981552124 54 0.78134369850158691 54.8 0.7947065830230714 55.6 0.81050312519073486
		 56.4 0.82856440544128418 57.2 0.84873813390731812 58 0.87082791328430176 58.8 0.89460015296936035
		 59.6 0.91986882686615012 60.4 0.94467049837112427 61.2 0.96524620056152366 62 1.0121153593063354
		 62.8 1.0343167781829834 63.6 1.0604065656661987 64.4 1.0828095674514773 65.2 1.1295064687728882
		 66 1.1444530487060549 66.8 1.1892279386520386 67.6 1.2040050029754641 68.4 1.249408483505249
		 69.2 1.2704811096191406 70 1.2961591482162476 70.8 1.3228368759155271 71.6 1.3487280607223513
		 72.4 1.3738192319869995;
createNode animCurveTA -n "Bip01_Spine1_rotateY5";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 91 ".ktv[0:90]"  0.4 -0.6009824275970459 1.2 -0.60680830478668213
		 2 -0.61275047063827515 2.8 -0.61276626586914063 3.6 -0.61148446798324585 4.4 -0.60291898250579834
		 5.2 -0.59623622894287109 6 -0.58682268857955933 6.8 -0.57740211486816406 7.6 -0.55124789476394653
		 8.4 -0.53819918632507324 9.2 -0.52570855617523193 10 -0.49436181783676142 10.8 -0.46081924438476563
		 11.6 -0.44864499568939198 12.4 -0.4029956459999085 13.2 -0.3796057403087616 14 -0.34932821989059448
		 14.8 -0.31599834561347961 15.6 -0.28162267804145813 16.4 -0.24636965990066526 17.2 -0.21044613420963287
		 18 -0.17409972846508026 18.8 -0.13761471211910248 19.6 -0.10125816613435744 20.4 -0.065324664115905762
		 21.2 -0.030127415433526039 22 0.0040295715443789959 22.8 0.036876894533634186 23.6 0.068202793598175049
		 24.4 0.097841717302799239 25.2 0.12566913664340973 26 0.15019382536411283 26.8 0.16853806376457214
		 27.6 0.20229622721672055 28.4 0.21171745657920835 29.2 0.23605938255786901 30 0.24286724627017975
		 30.8 0.25939458608627319 31.6 0.26337972283363342 32.4 0.27112972736358643 33.2 0.27454712986946106
		 34 0.27417507767677307 34.8 0.27350881695747375 35.6 0.2680986225605011 36.4 0.25899842381477356
		 37.2 0.25482308864593506 38 0.23740482330322268 38.8 0.2283478528261185 39.6 0.21851576864719391
		 40.4 0.19202296435832977 41.2 0.17822714149951938 42 0.16127213835716248 42.8 0.14607405662536621
		 43.6 0.11200490593910221 44.4 0.10052837431430817 45.2 0.064604572951793671 46 0.052256673574447632
		 46.8 0.012498957104980946 47.6 -0.007082086056470871 48.4 -0.031873412430286407 49.2 -0.058536600321531303
		 50 -0.085285782814025879 50.8 -0.11190988123416902 51.6 -0.13821130990982056 52.4 -0.16396887600421906
		 53.2 -0.18896006047725675 54 -0.21296653151512143 54.8 -0.23577219247817993 55.6 -0.25712081789970398
		 56.4 -0.27673798799514771 57.2 -0.29438245296478271 58 -0.30989143252372742 58.8 -0.32320019602775574
		 59.6 -0.3342844545841217 60.4 -0.34283915162086487 61.2 -0.34830260276794434 62 -0.35509765148162842
		 62.8 -0.35609665513038635 63.6 -0.3561217188835144 64.4 -0.35530748963356018 65.2 -0.35072466731071472
		 66 -0.34863400459289551 66.8 -0.34067106246948242 67.6 -0.33760339021682739 68.4 -0.32688364386558533
		 69.2 -0.3215630054473877 70 -0.3149360716342926 70.8 -0.30813440680503845 71.6 -0.3018328845500946
		 72.4 -0.29626467823982239;
createNode animCurveTA -n "Bip01_Spine1_rotateZ5";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 91 ".ktv[0:90]"  0.4 -0.72120898962020885 1.2 -0.73130470514297485
		 2 -0.74893087148666404 2.8 -0.75464212894439697 3.6 -0.75936001539230358 4.4 -0.76590812206268299
		 5.2 -0.76648426055908203 6 -0.76578611135482788 6.8 -0.76401418447494507 7.6 -0.7552228569984436
		 8.4 -0.74975091218948364 9.2 -0.7442094087600708 10 -0.72881895303726196 10.8 -0.71115285158157349
		 11.6 -0.70454567670822144 12.4 -0.6797337532043457 13.2 -0.66717350482940674 14 -0.65103489160537731
		 14.8 -0.63352662324905396 15.6 -0.61591726541519165 16.4 -0.59847992658615112 17.2 -0.58141952753067017
		 18 -0.56487590074539185 18.8 -0.54894477128982544 19.6 -0.53374361991882324 20.4 -0.51935696601867676
		 21.2 -0.50583839416503906 22 -0.49315083026885981 22.8 -0.48119282722473133 23.6 -0.46984437108039862
		 24.4 -0.45899662375450134 25.2 -0.44852659106254578 26 -0.43889796733856201 26.8 -0.43119797110557551
		 27.6 -0.41473937034606934 28.4 -0.40922597050666809 29.2 -0.39190489053726191 30 -0.38578525185585022
		 30.8 -0.36629948019981384 31.6 -0.3603644073009491 32.4 -0.34146353602409363 33.2 -0.32064804434776306
		 34 -0.31078362464904785 34.8 -0.30087056756019592 35.6 -0.27888992428779602 36.4 -0.25835815072059631
		 37.2 -0.25185063481330872 38 -0.22983275353908539 38.8 -0.22121445834636688 39.6 -0.21333809196949005
		 40.4 -0.19853942096233368 41.2 -0.194199338555336 42 -0.1907212883234024 42.8 -0.189290851354599
		 43.6 -0.19273887574672699 44.4 -0.19587139785289764 45.2 -0.21113884449005127 46 -0.21831102669239044
		 46.8 -0.24766971170902247 47.6 -0.26568120718002319 48.4 -0.2913205623626709 49.2 -0.32219210267066956
		 50 -0.35692960023880005 50.8 -0.39533469080924993 51.6 -0.4372003972530365 52.4 -0.48218229413032537
		 53.2 -0.52979230880737305 54 -0.57941210269927979 54.8 -0.6304364800453186 55.6 -0.68227148056030273
		 56.4 -0.7343091368675233 57.2 -0.78592991828918468 58 -0.83648687601089478 58.8 -0.88535350561141968
		 59.6 -0.93186986446380626 60.4 -0.97314566373825073 61.2 -1.0040781497955322 62 -1.0625054836273191
		 62.8 -1.0849794149398804 63.6 -1.1084214448928833 64.4 -1.125962495803833 65.2 -1.1520721912384031
		 66 -1.1574585437774658 66.8 -1.1653817892074585 67.6 -1.1653099060058594 68.4 -1.155977725982666
		 69.2 -1.146609902381897 70 -1.131150007247925 70.8 -1.1105453968048096 71.6 -1.0854823589324951
		 72.4 -1.0563700199127197;
createNode animCurveTA -n "Bip01_Spine2_rotateX5";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 91 ".ktv[0:90]"  0.4 6.1924433708190918 1.2 6.3406295776367187
		 2 6.4794316291809082 2.8 6.6122922897338867 3.6 6.7471504211425781 4.4 6.8773012161254883
		 5.2 6.9797167778015137 6 7.0442638397216797 6.8 7.0788969993591309 7.6 7.0961995124816895
		 8.4 7.1073946952819824 9.2 7.1135239601135254 10 7.1065640449523935 10.8 7.0813593864440918
		 11.6 7.0391788482666016 12.4 6.9830851554870605 13.2 6.9141964912414551 14 6.8330278396606454
		 14.8 6.7407898902893066 15.6 6.6388735771179199 16.4 6.5284771919250488 17.2 6.4106402397155762
		 18 6.2864809036254883 18.8 6.1572976112365723 19.6 6.0243768692016602 20.4 5.8888311386108398
		 21.2 5.7516613006591797 22 5.6139488220214844 22.8 5.4767704010009766 23.6 5.3410444259643555
		 24.4 5.2075390815734863 25.2 5.0769453048706055 26 4.9497332572937012 26.8 4.8260841369628906
		 27.6 4.706085205078125 28.4 4.5898947715759277 29.2 4.4776701927185059 30 4.3693561553955078
		 30.8 4.2647261619567871 31.6 4.1636033058166504 32.4 4.0657634735107422 33.2 3.9707217216491695
		 34 3.8778038024902344 34.8 3.7864820957183842 35.6 3.6965343952178946 36.4 3.6078939437866215
		 37.2 3.5205395221710205 38 3.4344995021820068 38.8 3.3497378826141357 39.6 3.2659621238708496
		 40.4 3.1829104423522949 41.2 3.1004202365875244 42 3.0185451507568359 42.8 2.939274787902832
		 43.6 2.8667855262756352 44.4 2.8023450374603271 45.2 2.7431776523590088 46 2.6880671977996826
		 46.8 2.6387121677398682 47.6 2.5969021320343018 48.4 2.5638632774353027 49.2 2.540369987487793
		 50 2.5267877578735352 50.8 2.5232765674591064 51.6 2.5300669670104985 52.4 2.5473618507385258
		 53.2 2.5749893188476558 54 2.6123001575469971 54.8 2.658503532409668 55.6 2.7128891944885254
		 56.4 2.7749013900756836 57.2 2.8439574241638188 58 2.9193739891052246 58.8 3.0003609657287602
		 59.6 3.0862102508544922 60.4 3.1763954162597656 61.2 3.2705538272857666 62 3.3680605888366704
		 62.8 3.4673576354980473 63.6 3.5668468475341797 64.4 3.6662545204162602 65.2 3.7658429145812993
		 66 3.8653583526611337 66.8 3.9643735885620117 67.6 4.0623230934143066 68.4 4.1585135459899902
		 69.2 4.2522530555725098 70 4.3431591987609863 70.8 4.4312090873718262 71.6 4.5165910720825195
		 72.4 4.599341869354248;
createNode animCurveTA -n "Bip01_Spine2_rotateY5";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 91 ".ktv[0:90]"  0.4 -1.9106671810150146 1.2 -1.9292138814926147
		 2 -1.9378126859664919 2.8 -1.939026951789856 3.6 -1.9378229379653928 4.4 -1.9313044548034668
		 5.2 -1.9086155891418457 6 -1.8666464090347288 6.8 -1.8112149238586424 7.6 -1.7484387159347534
		 8.4 -1.6825186014175415 9.2 -1.6135673522949221 10 -1.5387172698974607 10.8 -1.4565207958221436
		 11.6 -1.3677573204040527 12.4 -1.2736343145370483 13.2 -1.174702525138855 14 -1.0712815523147583
		 14.8 -0.96391624212265015 15.6 -0.8531360626220702 16.4 -0.73945051431655884 17.2 -0.62348842620849609
		 18 -0.50607240200042725 18.8 -0.38807499408721918 19.6 -0.27037575840950012 20.4 -0.15393801033496857
		 21.2 -0.039794828742742538 22 0.071035392582416534 22.8 0.17766532301902771 23.6 0.27936890721321106
		 24.4 0.37560799717903137 25.2 0.46592080593109136 26 0.54979389905929565 26.8 0.62659537792205811
		 27.6 0.69561052322387695 28.4 0.7562720775604248 29.2 0.80830329656600963 30 0.85165965557098389
		 30.8 0.88629359006881714 31.6 0.91208750009536721 32.4 0.92894589900970459 33.2 0.93699079751968373
		 34 0.93658196926116943 34.8 0.92810142040252719 35.6 0.91179668903350819 36.4 0.88787788152694702
		 37.2 0.85669535398483276 38 0.81880611181259155 38.8 0.77477055788040161 39.6 0.72500103712081909
		 40.4 0.66979753971099854 41.2 0.61067873239517212 42 0.55083155632019054 42.8 0.48866704106330872
		 43.6 0.41636785864830017 44.4 0.33331328630447393 45.2 0.24820089340209961 46 0.16492965817451477
		 46.8 0.081184200942516327 47.6 -0.0042153014801442623 48.4 -0.090870961546897888
		 49.2 -0.1782345324754715 50 -0.26563411951065063 50.8 -0.35239434242248535 51.6 -0.43781805038452148
		 52.4 -0.52116554975509644 53.2 -0.60166412591934204 54 -0.67863082885742188 54.8 -0.75133919715881348
		 55.6 -0.81894207000732411 56.4 -0.8805398941040038 57.2 -0.93534612655639648 58 -0.9828563928604126
		 58.8 -1.0228728055953979 59.6 -1.0553618669509888 60.4 -1.0802438259124756 61.2 -1.0989127159118652
		 62 -1.1129430532455444 62.8 -1.1183677911758425 63.6 -1.111540675163269 64.4 -1.0979318618774414
		 65.2 -1.0829380750656128 66 -1.065758228302002 66.8 -1.0454630851745603 67.6 -1.022723913192749
		 68.4 -0.99843049049377441 69.2 -0.97364318370819092 70 -0.94937288761138927 70.8 -0.92649298906326294
		 71.6 -0.90582996606826771 72.4 -0.88813507556915283;
createNode animCurveTA -n "Bip01_Spine2_rotateZ5";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 91 ".ktv[0:90]"  0.4 -2.4791524410247803 1.2 -2.5244235992431641
		 2 -2.562532901763916 2.8 -2.5950663089752197 3.6 -2.6253757476806641 4.4 -2.6500632762908936
		 5.2 -2.659785270690918 6 -2.6522912979125977 6.8 -2.6323208808898926 7.6 -2.604586124420166
		 8.4 -2.5727312564849858 9.2 -2.537179708480835 10 -2.4957613945007324 10.8 -2.4477074146270752
		 11.6 -2.3942677974700928 12.4 -2.3370864391326904 13.2 -2.2771666049957275 14 -2.2150986194610596
		 14.8 -2.1516444683074951 15.6 -2.0878026485443115 16.4 -2.0245323181152344 17.2 -1.9625657796859743
		 18 -1.9023977518081665 18.8 -1.8444545269012453 19.6 -1.7891461849212646 20.4 -1.7368065118789675
		 21.2 -1.6876126527786257 22 -1.6414879560470581 22.8 -1.5981305837631226 23.6 -1.5571537017822266
		 24.4 -1.5181887149810791 25.2 -1.4807921648025513 26 -1.4443345069885254 26.8 -1.408053994178772
		 27.6 -1.3711968660354614 28.4 -1.3332192897796633 29.2 -1.2936912775039673 30 -1.2522199153900146
		 30.8 -1.2085464000701904 31.6 -1.1626241207122805 32.4 -1.114640474319458 33.2 -1.064842700958252
		 34 -1.0135844945907593 34.8 -0.96144598722457886 35.6 -0.90935391187667836 36.4 -0.85833817720413219
		 37.2 -0.80940783023834229 38 -0.76369470357894897 38.8 -0.72245192527770996 39.6 -0.68690085411071777
		 40.4 -0.65819311141967773 41.2 -0.63478183746337891 42 -0.61153316497802734 42.8 -0.59404069185256958
		 43.6 -0.60224956274032593 44.4 -0.63988834619522095 45.2 -0.68889427185058594 46 -0.7415996789932251
		 46.8 -0.80445700883865356 47.6 -0.88117742538452148 48.4 -0.97176361083984364 49.2 -1.0759103298187256
		 50 -1.1929758787155151 50.8 -1.322264552116394 51.6 -1.4630459547042849 52.4 -1.6142584085464478
		 53.2 -1.7742512226104736 54 -1.9409971237182617 54.8 -2.1124341487884521 55.6 -2.2865777015686035
		 56.4 -2.4614114761352539 57.2 -2.6348409652709961 58 -2.8047244548797607 58.8 -2.9688966274261479
		 59.6 -3.1251993179321289 60.4 -3.2714645862579346 61.2 -3.4086196422576904 62 -3.5380175113677979
		 62.8 -3.649646520614624 63.6 -3.7335994243621822 64.4 -3.7974867820739746 65.2 -3.8492331504821773
		 66 -3.8855521678924561 66.8 -3.9034683704376225 67.6 -3.9031238555908203 68.4 -3.8847932815551758
		 69.2 -3.848919391632081 70 -3.7961571216583248 70.8 -3.7274210453033456 71.6 -3.6438298225402836
		 72.4 -3.5467567443847656;
createNode animCurveTA -n "Bip01_R_Clavicle_rotateX5";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 91 ".ktv[0:90]"  0.4 0.022963738068938255 1.2 0.024181928485631946
		 2 0.024607043713331223 2.8 0.025904364883899689 3.6 0.027117429301142693 4.4 0.028435854241251949
		 5.2 0.029895029962062832 6 0.031396776437759399 6.8 0.032812278717756271 7.6 0.034193176776170731
		 8.4 0.035622980445623398 9.2 0.036026924848556519 10 0.037131518125534058 10.8 0.03789533302187921
		 11.6 0.03803064301609993 12.4 0.038250871002674103 13.2 0.038157515227794647 14 0.037703361362218857
		 14.8 0.036924872547388077 15.6 0.035732056945562363 16.4 0.034064467996358871 17.2 0.031644131988286972
		 18 0.030818305909633636 18.8 0.027822844684123993 19.6 0.024963796138763428 20.4 0.022013500332832336
		 21.2 0.018526090309023861 22 0.014182930812239649 22.8 0.012799734249711037 23.6 0.0081249326467514038
		 24.4 0.0039450423792004585 25.2 -0.00018552846449892968 26 -0.0044701979495584965
		 26.8 -0.0089812008664011955 27.6 -0.013528811745345592 28.4 -0.018198642879724503
		 29.2 -0.023283235728740696 30 -0.029317270964384079 30.8 -0.031129686161875725 31.6 -0.037555582821369171
		 32.4 -0.043902128934860229 33.2 -0.045700404793024063 34 -0.051687546074390411 34.8 -0.056682690978050239
		 35.6 -0.061174940317869193 36.4 -0.065546780824661255 37.2 -0.06995091587305069 38 -0.074883334338665009
		 38.8 -0.076387427747249603 39.6 -0.08159445226192473 40.4 -0.083024896681308746 41.2 -0.086643554270267487
		 42 -0.089287541806697845 42.8 -0.091376230120658875 43.6 -0.09301359206438066 44.4 -0.094316810369491577
		 45.2 -0.095127172768115997 46 -0.095654129981994629 46.8 -0.095732197165489197 47.6 -0.095475122332572951
		 48.4 -0.094921000301837921 49.2 -0.094194188714027405 50 -0.0930013507604599 50.8 -0.091197744011878967
		 51.6 -0.090631872415542603 52.4 -0.088586241006851196 53.2 -0.08678369969129561 54 -0.084950849413871765
		 54.8 -0.082567371428012862 55.6 -0.081828966736793518 56.4 -0.079349711537361159
		 57.2 -0.077219933271408081 58 -0.07528793066740036 58.8 -0.073459476232528687 59.6 -0.071534596383571625
		 60.4 -0.069340825080871582 61.2 -0.068686783313751221 62 -0.066530883312225342 62.8 -0.064500033855438232
		 63.6 -0.062077786773443222 64.4 -0.061288937926292419 65.2 -0.058409828692674637
		 66 -0.055743157863616943 66.8 -0.052820783108472824 67.6 -0.049055781215429313 68.4 -0.047863814979791641
		 69.2 -0.04346746951341629 70 -0.039329726248979568 70.8 -0.03499903529882431 71.6 -0.030235247686505321
		 72.4 -0.025095563381910324;
createNode animCurveTA -n "Bip01_R_Clavicle_rotateY5";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 91 ".ktv[0:90]"  0.4 78.594505310058594 1.2 78.592247009277344
		 2 78.591537475585938 2.8 78.589057922363281 3.6 78.586769104003906 4.4 78.584289550781264
		 5.2 78.581558227539062 6 78.578727722167969 6.8 78.576026916503906 7.6 78.573471069335938
		 8.4 78.57086181640625 9.2 78.57012939453125 10 78.567970275878906 10.8 78.56658935546875
		 11.6 78.566329956054688 12.4 78.565902709960937 13.2 78.566062927246094 14 78.566947937011733
		 14.8 78.568435668945313 15.6 78.570602416992188 16.4 78.573745727539063 17.2 78.578315734863281
		 18 78.579833984375 18.8 78.585411071777344 19.6 78.590766906738295 20.4 78.596351623535156
		 21.2 78.602851867675781 22 78.610992431640639 22.8 78.613555908203125 23.6 78.622383117675781
		 24.4 78.630203247070327 25.2 78.637886047363281 26 78.645904541015625 26.8 78.654312133789063
		 27.6 78.662872314453125 28.4 78.671585083007812 29.2 78.681007385253906 30 78.692329406738281
		 30.8 78.695724487304688 31.6 78.707656860351563 32.4 78.719596862792983 33.2 78.722976684570327
		 34 78.734107971191406 34.8 78.743385314941406 35.6 78.751823425292997 36.4 78.760002136230469
		 37.2 78.768203735351563 38 78.777305603027344 38.8 78.780288696289062 39.6 78.789924621582031
		 40.4 78.792549133300781 41.2 78.799293518066406 42 78.80419921875 42.8 78.808067321777344
		 43.6 78.811164855957017 44.4 78.813545227050781 45.2 78.815185546875 46 78.8160400390625
		 46.8 78.816215515136719 47.6 78.815742492675781 48.4 78.814720153808594 49.2 78.813255310058594
		 50 78.811042785644531 50.8 78.807830810546875 51.6 78.8067626953125 52.4 78.802993774414063
		 53.2 78.799652099609389 54 78.796134948730469 54.8 78.791709899902344 55.6 78.790328979492188
		 56.4 78.785659790039063 57.2 78.781700134277344 58 78.77813720703125 58.8 78.77469635009767
		 59.6 78.771110534667969 60.4 78.767013549804688 61.2 78.765815734863281 62 78.761795043945313
		 62.8 78.757987976074219 63.6 78.753463745117188 64.4 78.751998901367188 65.2 78.746650695800781
		 66 78.741691589355469 66.8 78.736213684082031 67.6 78.729194641113281 68.4 78.726951599121094
		 69.2 78.718795776367188 70 78.711044311523438 70.8 78.702911376953125 71.6 78.694023132324219
		 72.4 78.684440612792983;
createNode animCurveTA -n "Bip01_R_Clavicle_rotateZ5";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 91 ".ktv[0:90]"  0.4 173.26992797851565 1.2 173.27017211914062
		 2 173.27029418945312 2.8 173.27052307128906 3.6 173.270751953125 4.4 173.27101135253906
		 5.2 173.27130126953125 6 173.27159118652344 6.8 173.2718505859375 7.6 173.27214050292969
		 8.4 173.27243041992187 9.2 173.27253723144531 10 173.272705078125 10.8 173.27287292480469
		 11.6 173.27288818359375 12.4 173.27294921875 13.2 173.27291870117187 14 173.2728271484375
		 14.8 173.27268981933594 15.6 173.27243041992187 16.4 173.27212524414065 17.2 173.27165222167972
		 18 173.271484375 18.8 173.2708740234375 19.6 173.27032470703125 20.4 173.269775390625
		 21.2 173.26904296875 22 173.26821899414062 22.8 173.26792907714844 23.6 173.26704406738281
		 24.4 173.26620483398435 25.2 173.265380859375 26 173.2645263671875 26.8 173.26362609863281
		 27.6 173.26274108886719 28.4 173.26179504394534 29.2 173.26077270507812 30 173.25958251953125
		 30.8 173.25923156738281 31.6 173.25791931152344 32.4 173.25666809082031 33.2 173.25633239746097
		 34 173.25511169433594 34.8 173.25408935546875 35.6 173.25323486328125 36.4 173.25234985351562
		 37.2 173.25146484375 38 173.25042724609375 38.8 173.25019836425781 39.6 173.24911499023435
		 40.4 173.24882507324219 41.2 173.24807739257812 42 173.24754333496094 42.8 173.24711608886719
		 43.6 173.24681091308594 44.4 173.24652099609375 45.2 173.24641418457031 46 173.24624633789065
		 46.8 173.24624633789065 47.6 173.24629211425781 48.4 173.24642944335937 49.2 173.24652099609375
		 50 173.24676513671875 50.8 173.2471923828125 51.6 173.24728393554687 52.4 173.24772644042969
		 53.2 173.24809265136719 54 173.24842834472656 54.8 173.24893188476562 55.6 173.24906921386719
		 56.4 173.24954223632812 57.2 173.24998474121094 58 173.25038146972656 58.8 173.25074768066406
		 59.6 173.25112915039062 60.4 173.25155639648435 61.2 173.25169372558594 62 173.25213623046875
		 62.8 173.25254821777344 63.6 173.25302124023435 64.4 173.25318908691409 65.2 173.25376892089844
		 66 173.25430297851565 66.8 173.25489807128906 67.6 173.25564575195312 68.4 173.25587463378906
		 69.2 173.25677490234375 70 173.25758361816406 70.8 173.25845336914062 71.6 173.2593994140625
		 72.4 173.26042175292969;
createNode animCurveTA -n "Bip01_R_UpperArm_rotateX5";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 91 ".ktv[0:90]"  0.4 -57.443252563476555 1.2 -57.412982940673821
		 2 -57.245750427246094 2.8 -57.211441040039063 3.6 -57.243370056152344 4.4 -57.127883911132812
		 5.2 -56.739162445068359 6 -56.187580108642578 6.8 -55.753402709960938 7.6 -55.587421417236328
		 8.4 -55.664310455322266 9.2 -55.945957183837891 10 -56.293785095214851 10.8 -56.55313873291017
		 11.6 -56.779659271240234 12.4 -56.993476867675781 13.2 -57.083648681640625 14 -56.969226837158203
		 14.8 -56.643791198730469 15.6 -56.248912811279297 16.4 -55.907588958740234 17.2 -55.617290496826179
		 18 -55.448020935058594 18.8 -55.399341583251953 19.6 -55.321014404296875 20.4 -55.211826324462891
		 21.2 -55.189731597900391 22 -55.275974273681641 22.8 -55.391819000244141 23.6 -55.344680786132812
		 24.4 -55.028148651123054 25.2 -54.662948608398438 26 -54.434906005859382 26.8 -54.24864578247071
		 27.6 -53.999523162841811 28.4 -53.700489044189453 29.2 -53.485565185546875 30 -53.426746368408203
		 30.8 -53.407024383544922 31.6 -53.33795166015625 32.4 -53.212245941162109 33.2 -53.055446624755859
		 34 -52.976207733154297 34.8 -53.029758453369141 35.6 -53.115531921386719 36.4 -53.143875122070313
		 37.2 -53.158367156982422 38 -53.199241638183594 38.8 -53.244083404541016 39.6 -53.286628723144531
		 40.4 -53.282073974609382 41.2 -53.143428802490234 42 -52.880550384521491 42.8 -52.659030914306648
		 43.6 -52.6119384765625 44.4 -52.708629608154297 45.2 -52.956066131591797 46 -53.387992858886719
		 46.8 -53.879737854003913 47.6 -54.294826507568359 48.4 -54.622299194335938 49.2 -54.9052734375
		 50 -55.240753173828125 50.8 -55.695899963378906 51.6 -56.175941467285149 52.4 -56.609169006347656
		 53.2 -57.072650909423821 54 -57.590034484863281 54.8 -58.070693969726563 55.6 -58.516017913818359
		 56.4 -59.051647186279304 57.2 -59.673923492431634 58 -60.189918518066406 58.8 -60.511741638183594
		 59.6 -60.769973754882813 60.4 -61.045387268066406 61.2 -61.25706863403321 62 -61.342395782470703
		 62.8 -61.374969482421882 63.6 -61.454669952392578 64.4 -61.549617767333984 65.2 -61.565914154052734
		 66 -61.531448364257813 66.8 -61.523490905761719 67.6 -61.43944168090821 68.4 -61.150768280029297
		 69.2 -60.755157470703118 70 -60.412364959716797 70.8 -60.168258666992195 71.6 -59.922893524169915
		 72.4 -59.528278350830078;
createNode animCurveTA -n "Bip01_R_UpperArm_rotateY5";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 91 ".ktv[0:90]"  0.4 -65.550155639648437 1.2 -65.663726806640625
		 2 -66.008758544921875 2.8 -66.437911987304688 3.6 -66.822639465332045 4.4 -67.281524658203125
		 5.2 -67.9091796875 6 -68.486770629882813 6.8 -68.836051940917969 7.6 -69.024696350097656
		 8.4 -69.088722229003906 9.2 -68.931747436523438 10 -68.577507019042969 10.8 -68.211318969726563
		 11.6 -67.938865661621094 12.4 -67.758644104003906 13.2 -67.611137390136719 14 -67.492950439453125
		 14.8 -67.487777709960938 15.6 -67.501052856445313 16.4 -67.445892333984375 17.2 -67.468238830566406
		 18 -67.542633056640625 18.8 -67.51556396484375 19.6 -67.469657897949219 20.4 -67.463691711425781
		 21.2 -67.430717468261719 22 -67.394081115722656 22.8 -67.385650634765639 23.6 -67.454925537109375
		 24.4 -67.632026672363281 25.2 -67.725593566894531 26 -67.578506469726577 26.8 -67.297012329101563
		 27.6 -67.060768127441406 28.4 -67.063392639160156 29.2 -67.330848693847656 30 -67.5142822265625
		 30.8 -67.350318908691406 31.6 -67.043609619140625 32.4 -66.884918212890625 33.2 -66.879409790039063
		 34 -66.894676208496094 34.8 -66.888191223144531 35.6 -66.877067565917969 36.4 -66.8348388671875
		 37.2 -66.737503051757813 38 -66.624404907226563 38.8 -66.472381591796875 39.6 -66.164039611816406
		 40.4 -65.753303527832031 41.2 -65.466690063476563 42 -65.358306884765625 42.8 -65.355911254882813
		 43.6 -65.479446411132813 44.4 -65.668533325195313 45.2 -65.728950500488281 46 -65.638236999511719
		 46.8 -65.593894958496094 47.6 -65.654922485351563 48.4 -65.69580078125 49.2 -65.704498291015625
		 50 -65.729240417480469 50.8 -65.763328552246094 51.6 -65.813674926757812 52.4 -65.849266052246094
		 53.2 -65.835403442382812 54 -65.837692260742187 54.8 -65.918899536132813 55.6 -66.016632080078125
		 56.4 -66.033157348632813 57.2 -66.032707214355469 58 -66.153900146484375 58.8 -66.296676635742187
		 59.6 -66.294486999511719 60.4 -66.248245239257813 61.2 -66.242950439453125 62 -66.167716979980469
		 62.8 -66.000961303710937 63.6 -65.829116821289062 64.4 -65.728591918945327 65.2 -65.739273071289062
		 66 -65.793258666992188 66.8 -65.862403869628906 67.6 -66.078330993652344 68.4 -66.477760314941406
		 69.2 -66.879432678222656 70 -67.120925903320313 70.8 -67.164077758789063 71.6 -67.064285278320313
		 72.4 -66.961479187011719;
createNode animCurveTA -n "Bip01_R_UpperArm_rotateZ5";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 91 ".ktv[0:90]"  0.4 24.524148941040039 1.2 24.579126358032227
		 2 24.219797134399418 2.8 23.934335708618168 3.6 23.768653869628903 4.4 23.342113494873047
		 5.2 22.406990051269531 6 21.309602737426761 6.8 20.55949592590332 7.6 20.272317886352539
		 8.4 20.380084991455082 9.2 20.927356719970703 10 21.729497909545895 10.8 22.428277969360352
		 11.6 22.988447189331055 12.4 23.429971694946289 13.2 23.67247200012207 14 23.619489669799808
		 14.8 23.184671401977539 15.6 22.637901306152344 16.4 22.21173095703125 17.2 21.763816833496097
		 18 21.413139343261719 18.8 21.307487487792969 19.6 21.177865982055664 20.4 20.965612411499023
		 21.2 20.880868911743164 22 20.92835807800293 22.8 20.984224319458011 23.6 20.762044906616211
		 24.4 20.098497390747074 25.2 19.454713821411133 26 19.210433959960938 26.8 19.142221450805664
		 27.6 18.947622299194336 28.4 18.45208740234375 29.2 17.800497055053711 30 17.422286987304688
		 30.8 17.418420791625977 31.6 17.478240966796875 32.4 17.314546585083008 33.2 16.95758056640625
		 34 16.67357063293457 34.8 16.570718765258789 35.6 16.499866485595703 36.4 16.372343063354492
		 37.2 16.270055770874027 38 16.208122253417969 38.8 16.176427841186523 39.6 16.271816253662109
		 40.4 16.385244369506836 41.2 16.19218635559082 42 15.649842262268066 42.8 15.03853702545166
		 43.6 14.522579193115234 44.4 14.128621101379396 45.2 14.040131568908691 46 14.310940742492676
		 46.8 14.593162536621096 47.6 14.65183734893799 48.4 14.585318565368651 49.2 14.466361999511722
		 50 14.383625030517578 50.8 14.427661895751953 51.6 14.463083267211912 52.4 14.426622390747072
		 53.2 14.455503463745124 54 14.525914192199709 54.8 14.470111846923828 55.6 14.35404109954834
		 56.4 14.43526077270508 57.2 14.655544281005859 58 14.643481254577637 58.8 14.382320404052734
		 59.6 14.197758674621582 60.4 14.110474586486816 61.2 13.951706886291504 62 13.753850936889648
		 62.8 13.642011642456056 63.6 13.672359466552734 64.4 13.733490943908691 65.2 13.680831909179688
		 66 13.625565528869627 66.8 13.682825088500977 67.6 13.594539642333984 68.4 13.167937278747562
		 69.2 12.692923545837402 70 12.51752758026123 70.8 12.731491088867188 71.6 13.149097442626951
		 72.4 13.441858291625977;
createNode animCurveTA -n "Bip01_R_Forearm_rotateX5";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 91 ".ktv[0:90]"  0.4 -0.21534927189350128 1.2 -0.21405056118965149
		 2 -0.20934607088565826 2.8 -0.20550811290740967 3.6 -0.20278196036815643 4.4 -0.19746662676334381
		 5.2 -0.18824659287929535 6 -0.17981192469596863 6.8 -0.17608889937400821 7.6 -0.17737264931201935
		 8.4 -0.18502892553806305 9.2 -0.2020018994808197 10 -0.22450396418571472 10.8 -0.2437905669212341
		 11.6 -0.25769448280334473 12.4 -0.26662725210189819 13.2 -0.27140557765960693 14 -0.27296933531761169
		 14.8 -0.26870432496070862 15.6 -0.26282027363777161 16.4 -0.25956514477729797 17.2 -0.25346675515174866
		 18 -0.24626541137695315 18.8 -0.24432754516601565 19.6 -0.24456875026226041 20.4 -0.24535217881202692
		 21.2 -0.24838986992836001 22 -0.25165438652038574 22.8 -0.2546316385269165 23.6 -0.25212270021438599
		 24.4 -0.23933798074722293 25.2 -0.2267217934131622 26 -0.22387690842151642 26.8 -0.22476625442504888
		 27.6 -0.21980774402618408 28.4 -0.20545299351215368 29.2 -0.18843021988868713 30 -0.17890132963657379
		 30.8 -0.17870114743709564 31.6 -0.18328805267810824 32.4 -0.18924662470817569 33.2 -0.18708698451519012
		 34 -0.1832646578550339 34.8 -0.18144968152046204 35.6 -0.1807129830121994 36.4 -0.18014919757843015
		 37.2 -0.18078576028347018 38 -0.18273209035396576 38.8 -0.18684077262878415 39.6 -0.19455930590629575
		 40.4 -0.20145672559738159 41.2 -0.20071497559547424 42 -0.1921120285987854 42.8 -0.17980413138866425
		 43.6 -0.16769824922084808 44.4 -0.15961562097072601 45.2 -0.1594005674123764 46 -0.16658318042755127
		 46.8 -0.17315399646759033 47.6 -0.17431354522705078 48.4 -0.17463360726833344 49.2 -0.17661666870117188
		 50 -0.17902515828609469 50.8 -0.18125356733798981 51.6 -0.1818864494562149 52.4 -0.18168631196022031
		 53.2 -0.18353272974491119 54 -0.18586872518062592 54.8 -0.18470977246761322 55.6 -0.18191203474998471
		 56.4 -0.18303008377552032 57.2 -0.18673358857631683 58 -0.18640762567520144 58.8 -0.1838181763887406
		 59.6 -0.18474915623664859 60.4 -0.18663258850574491 61.2 -0.18709591031074524 62 -0.18972276151180267
		 62.8 -0.19619554281234741 63.6 -0.20585188269615173 64.4 -0.21488989889621735 65.2 -0.21777069568634036
		 66 -0.21740517020225525 66.8 -0.21911175549030304 67.6 -0.21815931797027588 68.4 -0.21036313474178311
		 69.2 -0.20205794274806979 70 -0.20010325312614441 70.8 -0.20565386116504669 71.6 -0.21549415588378903
		 72.4 -0.22388352453708651;
createNode animCurveTA -n "Bip01_R_Forearm_rotateY5";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 91 ".ktv[0:90]"  0.4 1.0791143178939819 1.2 1.0710505247116089
		 2 1.0562707185745239 2.8 1.0450820922851565 3.6 1.0383015871047974 4.4 1.0260218381881714
		 5.2 1.0031960010528564 6 0.980965256690979 6.8 0.97127151489257813 7.6 0.97590315341949452
		 8.4 0.99551761150360096 9.2 1.0324597358703611 10 1.0761771202087402 10.8 1.1115082502365112
		 11.6 1.138803243637085 12.4 1.1594177484512329 13.2 1.1723337173461914 14 1.1776227951049807
		 14.8 1.1714191436767578 15.6 1.1630258560180664 16.4 1.1600903272628784 17.2 1.1508407592773435
		 18 1.1380608081817627 18.8 1.1343629360198977 19.6 1.1337356567382812 20.4 1.1330292224884031
		 21.2 1.1362406015396118 22 1.1406679153442385 22.8 1.1455980539321899 23.6 1.1431177854537964
		 24.4 1.120853066444397 25.2 1.0963355302810669 26 1.0899088382720949 26.8 1.0933709144592283
		 27.6 1.0895025730133057 28.4 1.0679135322570803 29.2 1.0366934537887571 30 1.0164847373962402
		 30.8 1.0143740177154541 31.6 1.022377610206604 32.4 1.0256006717681885 33.2 1.0188088417053225
		 34 1.0095632076263428 34.8 1.0055916309356687 35.6 1.0037236213684082 36.4 1.0027099847793579
		 37.2 1.0058777332305908 38 1.0120463371276855 38.8 1.0206952095031738 39.6 1.0352945327758789
		 40.4 1.0496786832809448 41.2 1.0519798994064331 42 1.0396007299423218 42.8 1.0167545080184937
		 43.6 0.99041622877120961 44.4 0.97116279602050748 45.2 0.96946120262145996 46 0.98407864570617665
		 46.8 0.99688726663589478 47.6 0.99848502874374401 48.4 0.99886977672576915 49.2 1.0020599365234375
		 50 1.0057386159896853 50.8 1.0111182928085327 51.6 1.0155543088912964 52.4 1.0176063776016235
		 53.2 1.0206609964370728 54 1.0232919454574585 54.8 1.0207982063293457 55.6 1.0169153213500977
		 56.4 1.0204272270202637 57.2 1.0267201662063601 58 1.0220903158187866 58.8 1.0116163492202761
		 59.6 1.0094499588012695 60.4 1.0110623836517334 61.2 1.0116420984268188 62 1.0179243087768557
		 62.8 1.0313800573348999 63.6 1.0500444173812868 64.4 1.0675475597381592 65.2 1.0740381479263306
		 66 1.0755670070648191 66.8 1.0816617012023926 67.6 1.0812650918960571 68.4 1.0655927658081059
		 69.2 1.0485972166061399 70 1.0458401441574097 70.8 1.0589745044708252 71.6 1.0789804458618164
		 72.4 1.0941164493560791;
createNode animCurveTA -n "Bip01_R_Forearm_rotateZ5";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 91 ".ktv[0:90]"  0.4 -19.919282913208008 1.2 -19.71363639831543
		 2 -19.309883117675781 2.8 -19.000789642333984 3.6 -18.808700561523437 4.4 -18.456260681152344
		 5.2 -17.809150695800781 6 -17.184808731079102 6.8 -16.911880493164063 7.6 -17.036029815673828
		 8.4 -17.589422225952148 9.2 -18.662492752075195 10 -19.961597442626953 10.8 -21.024665832519531
		 11.6 -21.832986831665039 12.4 -22.422464370727539 13.2 -22.781049728393555 14 -22.923124313354492
		 14.8 -22.725856781005859 15.6 -22.457809448242188 16.4 -22.349666595458984 17.2 -22.059394836425781
		 18 -21.672981262207031 18.8 -21.562963485717773 19.6 -21.551212310791016 20.4 -21.543910980224613
		 21.2 -21.655818939208984 22 -21.798908233642575 22.8 -21.950496673583984 23.6 -21.861940383911133
		 24.4 -21.18565559387207 25.2 -20.458969116210937 26 -20.27391242980957 26.8 -20.366313934326172
		 27.6 -20.217575073242188 28.4 -19.541353225708008 29.2 -18.606681823730469 30 -18.017755508422852
		 30.8 -17.964776992797852 31.6 -18.206546783447266 32.4 -18.349143981933594 33.2 -18.163045883178711
		 34 -17.899496078491211 34.8 -17.784368515014648 35.6 -17.731359481811527 36.4 -17.700935363769531
		 37.2 -17.783634185791016 38 -17.952459335327148 38.8 -18.20526123046875 39.6 -18.640251159667969
		 40.4 -19.061050415039063 41.2 -19.107662200927734 42 -18.716279983520508 42.8 -18.033781051635746
		 43.6 -17.26994514465332 44.4 -16.719379425048828 45.2 -16.675952911376953 46 -17.10491943359375
		 46.8 -17.483974456787109 47.6 -17.534820556640625 48.4 -17.54768180847168 49.2 -17.645900726318359
		 50 -17.760566711425781 50.8 -17.913984298706055 51.6 -18.027454376220703 52.4 -18.07457160949707
		 53.2 -18.168039321899418 54 -18.256576538085938 54.8 -18.183931350708008 55.6 -18.060445785522461
		 56.4 -18.156843185424805 57.2 -18.3482666015625 58 -18.233835220336911 58.8 -17.954866409301758
		 59.6 -17.914058685302734 60.4 -17.974033355712891 61.2 -17.993322372436523 62 -18.172687530517575
		 62.8 -18.566648483276367 63.6 -19.121038436889648 64.4 -19.640785217285156 65.2 -19.827812194824219
		 66 -19.859432220458984 66.8 -20.023275375366211 67.6 -20.002555847167969 68.4 -19.540946960449219
		 69.2 -19.04229736328125 70 -18.954082489013672 70.8 -19.32984733581543 71.6 -19.917438507080082
		 72.4 -20.373079299926761;
createNode animCurveTA -n "Bip01_L_Clavicle_rotateX5";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 91 ".ktv[0:90]"  0.4 -0.08931906521320343 1.2 -0.087348617613315596
		 2 -0.081327535212039948 2.8 -0.079773068428039551 3.6 -0.076240450143814087 4.4 -0.074008315801620483
		 5.2 -0.072559252381324768 6 -0.071646027266979218 6.8 -0.071273759007453918 7.6 -0.071472190320491791
		 8.4 -0.072143703699111938 9.2 -0.07330022007226944 10 -0.074862822890281677 10.8 -0.076828539371490479
		 11.6 -0.079099349677562714 12.4 -0.082097336649894714 13.2 -0.08630119264125824 14 -0.087622694671154022
		 14.8 -0.092467889189720154 15.6 -0.096963301301002502 16.4 -0.10170347988605501 17.2 -0.10684530436992644
		 18 -0.11296554654836652 18.8 -0.12074278295040126 19.6 -0.12365652620792389 20.4 -0.13397966325283053
		 21.2 -0.13761119544506073 22 -0.14893136918544769 22.8 -0.15280249714851379 23.6 -0.16472180187702182
		 24.4 -0.16826839745044708 25.2 -0.17851263284683228 26 -0.18718996644020081 26.8 -0.19589181244373319
		 27.6 -0.20592519640922544 28.4 -0.20885027945041656 29.2 -0.21836215257644653 30 -0.22608546912670138
		 30.8 -0.23299990594387057 31.6 -0.23951730132102969 32.4 -0.24558882415294647 33.2 -0.25122004747390747
		 34 -0.25644227862358093 34.8 -0.261262446641922 35.6 -0.2656916081905365 36.4 -0.26967328786849976
		 37.2 -0.27335190773010254 38 -0.27693954110145569 38.8 -0.28075176477432251 39.6 -0.28185579180717468
		 40.4 -0.2852838933467865 41.2 -0.28805571794509888 42 -0.29057803750038147 42.8 -0.29298394918441772
		 43.6 -0.29524910449981689 44.4 -0.29757276177406311 45.2 -0.29992640018463135 46 -0.30262139439582825
		 46.8 -0.30576950311660767 47.6 -0.30673331022262573 48.4 -0.3098888099193573 49.2 -0.3126780092716217
		 50 -0.31563800573348999 50.8 -0.31658929586410522 51.6 -0.31991004943847656 52.4 -0.32090514898300171
		 53.2 -0.32362157106399536 54 -0.32438564300537109 54.8 -0.32606974244117742 55.6 -0.32711568474769592
		 56.4 -0.32770234346389771 57.2 -0.32777896523475647 58 -0.32741159200668335 58.8 -0.32659497857093811
		 59.6 -0.32523122429847717 60.4 -0.32312563061714172 61.2 -0.31982338428497314 62 -0.31841325759887701
		 62.8 -0.31308737397193909 63.6 -0.31106856465339661 64.4 -0.30429533123970032 65.2 -0.30214610695838928
		 66 -0.29531905055046082 66.8 -0.28855478763580322 67.6 -0.28048032522201538 68.4 -0.27754080295562744
		 69.2 -0.26717051863670349 70 -0.26409527659416199 70.8 -0.25501188635826111 71.6 -0.24653798341751101
		 72.4 -0.23701222240924835;
createNode animCurveTA -n "Bip01_L_Clavicle_rotateY5";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 91 ".ktv[0:90]"  0.4 -78.473060607910156 1.2 -78.476768493652358
		 2 -78.488075256347642 2.8 -78.490997314453125 3.6 -78.497596740722656 4.4 -78.501823425292983
		 5.2 -78.504562377929702 6 -78.506256103515625 6.8 -78.506919860839844 7.6 -78.506576538085937
		 8.4 -78.505294799804688 9.2 -78.50311279296875 10 -78.500152587890625 10.8 -78.496551513671875
		 11.6 -78.492233276367188 12.4 -78.486587524414063 13.2 -78.47879791259767 14 -78.476226806640625
		 14.8 -78.467124938964844 15.6 -78.458656311035156 16.4 -78.449821472167969 17.2 -78.440116882324219
		 18 -78.428665161132812 18.8 -78.414039611816406 19.6 -78.408592224121094 20.4 -78.389106750488281
		 21.2 -78.382286071777344 22 -78.360992431640625 22.8 -78.353721618652358 23.6 -78.33123779296875
		 24.4 -78.324615478515625 25.2 -78.305343627929673 26 -78.288871765136719 26.8 -78.272514343261719
		 27.6 -78.253631591796875 28.4 -78.248085021972656 29.2 -78.230133056640625 30 -78.21551513671875
		 30.8 -78.202468872070298 31.6 -78.19014739990233 32.4 -78.1787109375 33.2 -78.168067932128906
		 34 -78.158256530761719 34.8 -78.149139404296875 35.6 -78.140792846679673 36.4 -78.133216857910156
		 37.2 -78.126228332519531 38 -78.119499206542983 38.8 -78.112251281738281 39.6 -78.110176086425781
		 40.4 -78.103683471679673 41.2 -78.098457336425781 42 -78.093727111816406 42.8 -78.089210510253906
		 43.6 -78.08477783203125 44.4 -78.080474853515625 45.2 -78.075935363769531 46 -78.070884704589844
		 46.8 -78.064918518066406 47.6 -78.063133239746094 48.4 -78.057182312011719 49.2 -78.051948547363281
		 50 -78.046333312988281 50.8 -78.04443359375 51.6 -78.038162231445313 52.4 -78.036231994628906
		 53.2 -78.031021118164063 54 -78.029678344726563 54.8 -78.026504516601562 55.6 -78.024513244628906
		 56.4 -78.023468017578125 57.2 -78.023284912109375 58 -78.023925781250014 58.8 -78.0255126953125
		 59.6 -78.028106689453125 60.4 -78.032119750976562 61.2 -78.03839111328125 62 -78.041061401367188
		 62.8 -78.051116943359375 63.6 -78.054893493652358 64.4 -78.0677490234375 65.2 -78.071800231933608
		 66 -78.084724426269531 66.8 -78.097541809082031 67.6 -78.112777709960938 68.4 -78.118362426757813
		 69.2 -78.137924194335938 70 -78.143760681152358 70.8 -78.160942077636719 71.6 -78.17694091796875
		 72.4 -78.195045471191406;
createNode animCurveTA -n "Bip01_L_Clavicle_rotateZ5";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 91 ".ktv[0:90]"  0.4 173.2840576171875 1.2 173.28369140625
		 2 173.28253173828125 2.8 173.28221130371094 3.6 173.28153991699219 4.4 173.28111267089844
		 5.2 173.28082275390625 6 173.2806396484375 6.8 173.28054809570312 7.6 173.28060913085935
		 8.4 173.2807312011719 9.2 173.28096008300781 10 173.28125 10.8 173.28167724609378
		 11.6 173.28208923339844 12.4 173.28265380859375 13.2 173.28350830078125 14 173.28373718261719
		 14.8 173.28466796875 15.6 173.2855224609375 16.4 173.28648376464844 17.2 173.28744506835935
		 18 173.28866577148435 18.8 173.2901611328125 19.6 173.29072570800781 20.4 173.29267883300781
		 21.2 173.29339599609375 22 173.29556274414062 22.8 173.29629516601562 23.6 173.29856872558594
		 24.4 173.29927062988281 25.2 173.30123901367187 26 173.30282592773435 26.8 173.30450439453125
		 27.6 173.30641174316406 28.4 173.30694580078125 29.2 173.30874633789065 30 173.3101806640625
		 30.8 173.31149291992187 31.6 173.31271362304687 32.4 173.31385803222656 33.2 173.31492614746094
		 34 173.31590270996094 34.8 173.31681823730469 35.6 173.31767272949219 36.4 173.31837463378906
		 37.2 173.31904602050781 38 173.31974792480469 38.8 173.32041931152344 39.6 173.32064819335935
		 40.4 173.3212890625 41.2 173.32180786132812 42 173.32229614257812 42.8 173.32275390625
		 43.6 173.32312011718753 44.4 173.32356262207031 45.2 173.32398986816406 46 173.32450866699219
		 46.8 173.32508850097656 47.6 173.32525634765625 48.4 173.32588195800781 49.2 173.326416015625
		 50 173.32693481445312 50.8 173.32707214355469 51.6 173.32771301269531 52.4 173.32786560058594
		 53.2 173.32835388183594 54 173.32852172851562 54.8 173.32884216308597 55.6 173.32902526855469
		 56.4 173.32917785644531 57.2 173.32914733886719 58 173.32907104492187 58.8 173.32894897460935
		 59.6 173.32868957519531 60.4 173.32832336425781 61.2 173.32771301269531 62 173.32745361328125
		 62.8 173.32646179199219 63.6 173.32606506347656 64.4 173.3248291015625 65.2 173.32441711425781
		 66 173.32315063476562 66.8 173.3218994140625 67.6 173.32040405273438 68.4 173.31985473632812
		 69.2 173.31790161132812 70 173.31733703613281 70.8 173.31564331054687 71.6 173.31404113769531
		 72.4 173.31231689453128;
createNode animCurveTA -n "Bip01_L_UpperArm_rotateX5";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 91 ".ktv[0:90]"  0.4 17.35692024230957 1.2 17.315958023071289
		 2 18.440227508544922 2.8 20.118524551391602 3.6 21.458293914794918 4.4 22.470546722412113
		 5.2 23.161840438842773 6 23.355781555175781 6.8 23.626747131347656 7.6 24.019607543945313
		 8.4 23.629911422729492 9.2 22.465682983398437 10 21.423248291015625 10.8 20.883634567260746
		 11.6 20.676088333129883 12.4 20.553571701049805 13.2 20.483087539672852 14 20.544034957885746
		 14.8 20.799230575561523 15.6 21.28289794921875 16.4 21.692110061645508 17.2 21.837259292602539
		 18 22.126440048217773 18.8 22.811822891235352 19.6 23.293125152587891 20.4 22.961137771606445
		 21.2 22.21331787109375 22 21.85247802734375 22.8 22.32597541809082 23.6 23.216033935546875
		 24.4 23.453964233398441 25.2 23.022388458251953 26 22.790130615234375 26.8 22.982852935791016
		 27.6 23.361843109130859 28.4 23.613447189331055 29.2 23.486444473266602 30 23.108160018920895
		 30.8 22.861043930053711 31.6 23.17078971862793 32.4 23.929039001464844 33.2 24.235433578491211
		 34 23.783662796020508 34.8 23.654268264770508 35.6 24.504793167114254 36.4 25.529375076293945
		 37.2 25.941352844238281 38 25.9017333984375 38.8 25.990711212158203 39.6 26.177982330322266
		 40.4 26.028120040893555 41.2 25.681856155395508 42 25.709442138671875 42.8 26.422893524169918
		 43.6 27.470211029052734 44.4 28.010887145996097 45.2 27.850372314453125 46 27.186655044555664
		 46.8 26.390308380126953 47.6 26.430521011352543 48.4 27.429176330566406 49.2 28.052566528320313
		 50 27.819503784179688 50.8 27.489629745483398 51.6 27.272127151489258 52.4 27.112440109252933
		 53.2 27.342050552368164 54 28.034151077270508 54.8 28.67404747009277 55.6 28.790994644165039
		 56.4 28.770349502563477 57.2 29.035095214843739 58 29.028715133666992 58.8 28.304254531860352
		 59.6 27.471578598022464 60.4 27.075599670410156 61.2 26.836065292358398 62 26.647647857666016
		 62.8 26.6060791015625 63.6 26.377799987792969 64.4 25.972930908203125 65.2 25.96501350402832
		 66 26.163114547729496 66.8 25.737550735473633 67.6 24.568664550781257 68.4 23.247224807739254
		 69.2 22.492033004760746 70 22.496791839599613 70.8 22.601099014282227 71.6 22.329538345336911
		 72.4 21.969720840454105;
createNode animCurveTA -n "Bip01_L_UpperArm_rotateY5";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 91 ".ktv[0:90]"  0.4 73.642166137695312 1.2 73.749565124511719
		 2 73.888679504394545 2.8 74.020851135253906 3.6 74.044700622558594 4.4 73.85009765625
		 5.2 73.460960388183594 6 73.089729309082031 6.8 72.874397277832031 7.6 72.793479919433594
		 8.4 72.8387451171875 9.2 72.962440490722656 10 73.080352783203125 10.8 73.14788818359375
		 11.6 73.13470458984375 12.4 73.091941833496094 13.2 73.093025207519531 14 73.128738403320327
		 14.8 73.159965515136719 15.6 73.172286987304687 16.4 73.199417114257813 17.2 73.25201416015625
		 18 73.285224914550781 18.8 73.291145324707017 19.6 73.314041137695312 20.4 73.407913208007813
		 21.2 73.586074829101563 22 73.81231689453125 22.8 74.018417358398438 23.6 74.189338684082031
		 24.4 74.384376525878906 25.2 74.615287780761719 26 74.868423461914062 26.8 75.117240905761719
		 27.6 75.287673950195313 28.4 75.363761901855469 29.2 75.432716369628906 30 75.582046508789063
		 30.8 75.749397277832031 31.6 75.800216674804688 32.4 75.784645080566406 33.2 75.872261047363281
		 34 76.035499572753906 34.8 76.0787353515625 35.6 75.96588134765625 36.4 75.851577758789063
		 37.2 75.840835571289063 38 75.882560729980469 38.8 75.939674377441406 39.6 76.087677001953125
		 40.4 76.31475830078125 41.2 76.503700256347642 42 76.635513305664063 42.8 76.7440185546875
		 43.6 76.740776062011719 44.4 76.649971008300781 45.2 76.611289978027344 46 76.655410766601562
		 46.8 76.67340087890625 47.6 76.5679931640625 48.4 76.379013061523438 49.2 76.240554809570327
		 50 76.179306030273438 50.8 76.114898681640625 51.6 76.072883605957017 52.4 76.076751708984375
		 53.2 76.038131713867202 54 75.895820617675781 54.8 75.714859008789063 55.6 75.60662841796875
		 56.4 75.53778076171875 57.2 75.414825439453125 58 75.32073974609375 58.8 75.3770751953125
		 59.6 75.487548828125 60.4 75.515533447265625 61.2 75.484512329101563 62 75.429733276367187
		 62.8 75.321861267089844 63.6 75.160751342773438 64.4 74.965713500976563 65.2 74.729248046875
		 66 74.470443725585937 66.8 74.273887634277344 67.6 74.186302185058594 68.4 74.177146911621094
		 69.2 74.215400695800781 70 74.2724609375 70.8 74.312599182128906 71.6 74.314659118652358
		 72.4 74.251571655273437;
createNode animCurveTA -n "Bip01_L_UpperArm_rotateZ5";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 91 ".ktv[0:90]"  0.4 -22.625961303710938 1.2 -22.723287582397461
		 2 -21.316118240356445 2.8 -19.198205947875977 3.6 -17.477819442749023 4.4 -16.074872970581055
		 5.2 -14.980790138244629 6 -14.528993606567385 6.8 -14.053889274597168 7.6 -13.490819931030272
		 8.4 -14.013671875000002 9.2 -15.584181785583496 10 -16.992271423339844 10.8 -17.722743988037109
		 11.6 -17.986434936523438 12.4 -18.128122329711911 13.2 -18.220962524414063 14 -18.156213760375977
		 14.8 -17.836463928222656 15.6 -17.210876464843754 16.4 -16.690156936645511 17.2 -16.525638580322266
		 18 -16.161373138427734 18.8 -15.267983436584474 19.6 -14.650770187377928 20.4 -15.12580680847168
		 21.2 -16.178157806396484 22 -16.745563507080078 22.8 -16.227832794189453 23.6 -15.167967796325685
		 24.4 -14.963475227355955 25.2 -15.632973670959473 26 -16.052431106567383 26.8 -15.929598808288574
		 27.6 -15.537702560424805 28.4 -15.257394790649416 29.2 -15.453061103820806 30 -16.015304565429688
		 30.8 -16.425939559936523 31.6 -16.081357955932621 32.4 -15.145586967468262 33.2 -14.830963134765623
		 34 -15.50540828704834 34.8 -15.715489387512212 35.6 -14.615227699279785 36.4 -13.291108131408691
		 37.2 -12.788455009460447 38 -12.87593936920166 38.8 -12.80884838104248 39.6 -12.665383338928226
		 40.4 -12.97734546661377 41.2 -13.516033172607422 42 -13.548884391784668 42.8 -12.73143482208252
		 43.6 -11.448285102844238 44.4 -10.729632377624512 45.2 -10.89055061340332 46 -11.70350456237793
		 46.8 -12.647709846496582 47.6 -12.48991584777832 48.4 -11.110378265380859 49.2 -10.217171669006348
		 50 -10.411305427551271 50.8 -10.715357780456545 51.6 -10.886064529418944 52.4 -11.00095272064209
		 53.2 -10.602258682250977 54 -9.5624904632568359 54.8 -8.5479335784912109 55.6 -8.2024173736572266
		 56.4 -8.0338850021362322 57.2 -7.4656782150268564 58 -7.2323698997497523 58.8 -7.9605731964111328
		 59.6 -8.838313102722168 60.4 -9.1128339767456055 61.2 -9.1488895416259766 62 -9.1013660430908203
		 62.8 -8.8326330184936523 63.6 -8.7537393569946289 64.4 -8.8729705810546875 65.2 -8.4827680587768555
		 66 -7.8365387916564915 66.8 -8.0252037048339844 67.6 -9.2391681671142578 68.4 -10.725813865661619
		 69.2 -11.555156707763672 70 -11.466283798217773 70.8 -11.279001235961914 71.6 -11.58529567718506
		 72.4 -12.013123512268066;
createNode animCurveTA -n "Bip01_L_Forearm_rotateX5";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 91 ".ktv[0:90]"  0.4 -0.027931490913033485 1.2 -0.031260304152965546
		 2 -0.030401213094592094 2.8 -0.026183249428868297 3.6 -0.022368807345628735 4.4 -0.020146796479821205
		 5.2 -0.019811736419796944 6 -0.021538013592362404 6.8 -0.022535501047968864 7.6 -0.022867040708661079
		 8.4 -0.026236686855554584 9.2 -0.031553912907838821 10 -0.035214871168136604 10.8 -0.036573193967342377
		 11.6 -0.036463376134634025 12.4 -0.036039702594280243 13.2 -0.035584662109613419
		 14 -0.034613572061061859 14.8 -0.03316209465265274 15.6 -0.031688939779996872 16.4 -0.030874503776431087
		 17.2 -0.030013510957360271 18 -0.026904752478003505 18.8 -0.021959695965051651 19.6 -0.019801922142505649
		 20.4 -0.023650733754038811 21.2 -0.029891233891248706 22 -0.034102689474821091 22.8 -0.035269502550363541
		 23.6 -0.034545838832855225 24.4 -0.034362524747848511 25.2 -0.035198211669921875
		 26 -0.035036031156778336 26.8 -0.032113406807184219 27.6 -0.025951260700821877 28.4 -0.019789230078458786
		 29.2 -0.018799411132931709 30 -0.022644329816102985 30.8 -0.026006642729043961 31.6 -0.025665078312158585
		 32.4 -0.023697149008512497 33.2 -0.024070218205451965 34 -0.026366248726844788 34.8 -0.025710454210639
		 35.6 -0.020022345706820488 36.4 -0.013390518724918364 37.2 -0.011842384934425354
		 38 -0.015588435344398022 38.8 -0.019443018361926079 39.6 -0.02035477384924889 40.4 -0.018799608573317528
		 41.2 -0.015057999640703201 42 -0.0099511034786701202 42.8 0.0017578700790181754 43.6 0.0043851519003510475
		 44.4 0.0031645600683987141 45.2 -0.00048585401964373887 46 -0.0043813046067953118
		 46.8 -0.0071291378699243069 47.6 -0.0054942225106060505 48.4 0.00063648930517956614
		 49.2 0.0053305840119719505 50 0.0064257248304784298 50.8 0.0071000028401613235 51.6 0.0059947655536234379
		 52.4 0.002399772172793746 53.2 0.00096035480964928865 54 0.0046970578841865063 54.8 0.010288936085999012
		 55.6 0.012311487458646296 56.4 0.01141021866351366 57.2 0.010844379663467407 58 0.0084173185750842094
		 58.8 0.0010638188105076551 59.6 -0.0073105408810079089 60.4 -0.012716466560959816
		 61.2 -0.016297932714223862 62 -0.01868821494281292 62.8 -0.019102012738585472 63.6 -0.018912006169557571
		 64.4 -0.017946021631360054 65.2 -0.013634521514177322 66 -0.0086669465526938438 66.8 -0.0097614349797368032
		 67.6 -0.016001993790268898 68.4 -0.019565179944038391 69.2 -0.016614275053143501
		 70 -0.01069191750138998 70.8 -0.0078318752348423004 71.6 -0.009771532379090786 72.4 -0.013564677909016607;
createNode animCurveTA -n "Bip01_L_Forearm_rotateY5";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 91 ".ktv[0:90]"  0.4 -0.40640681982040405 1.2 -0.40055856108665466
		 2 -0.41509991884231578 2.8 -0.44329643249511719 3.6 -0.46742498874664312 4.4 -0.48712682723999023
		 5.2 -0.49762317538261419 6 -0.49223122000694269 6.8 -0.48771375417709367 7.6 -0.48576739430427568
		 8.4 -0.46219283342361456 9.2 -0.42054742574691778 10 -0.38722693920135498 10.8 -0.37343701720237726
		 11.6 -0.37466889619827271 12.4 -0.37861016392707825 13.2 -0.38084110617637634 14 -0.38379105925559998
		 14.8 -0.38876351714134216 15.6 -0.39766442775726324 16.4 -0.40463864803314209 17.2 -0.40456321835517878
		 18 -0.40962782502174383 18.8 -0.42979392409324646 19.6 -0.44716271758079529 20.4 -0.43923142552375793
		 21.2 -0.41410696506500239 22 -0.39216247200965881 22.8 -0.38601669669151306 23.6 -0.39373227953910833
		 24.4 -0.39539077877998352 25.2 -0.3865533173084259 26 -0.38125431537628174 26.8 -0.38596126437187189
		 27.6 -0.40150198340415955 28.4 -0.4185028076171875 29.2 -0.42348393797874451 30 -0.42009413242340088
		 30.8 -0.4212763905525207 31.6 -0.43250742554664606 32.4 -0.44628319144248962 33.2 -0.44600912928581238
		 34 -0.43123844265937816 34.8 -0.42874857783317566 35.6 -0.45216149091720576 36.4 -0.47977972030639654
		 37.2 -0.48921471834182745 38 -0.4851966500282287 38.8 -0.48581093549728394 39.6 -0.49228021502494823
		 40.4 -0.4929930567741394 41.2 -0.48873463273048401 42 -0.49160695075988775 42.8 -0.51074135303497314
		 43.6 -0.54364639520645142 44.4 -0.56407809257507324 45.2 -0.56298768520355225 46 -0.54575836658477783
		 46.8 -0.52495276927947998 47.6 -0.52573269605636597 48.4 -0.55153840780258179 49.2 -0.56867754459381104
		 50 -0.56303441524505615 50.8 -0.55109316110610962 51.6 -0.53602516651153564 52.4 -0.52089887857437134
		 53.2 -0.52131021022796642 54 -0.53959697484970093 54.8 -0.55840003490447998 55.6 -0.56176811456680298
		 56.4 -0.55960452556610107 57.2 -0.56645768880844116 58 -0.5681723952293396 58.8 -0.5479850172996521
		 59.6 -0.52224624156951904 60.4 -0.51032859086990356 61.2 -0.50526148080825806 62 -0.50304734706878662
		 62.8 -0.5067937970161438 63.6 -0.50511348247528076 64.4 -0.4966055154800415 65.2 -0.50326859951019287
		 66 -0.52372896671295166 66.8 -0.52620857954025269 67.6 -0.50105607509613037 68.4 -0.46779310703277582
		 69.2 -0.44689178466796869 70 -0.44377860426902765 70.8 -0.44654420018196106 71.6 -0.44590774178504949
		 72.4 -0.44685128331184387;
createNode animCurveTA -n "Bip01_L_Forearm_rotateZ5";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 91 ".ktv[0:90]"  0.4 -0.99398362636566162 1.2 -0.8290839195251466
		 2 -1.2116949558258057 2.8 -1.9627832174301143 3.6 -2.6065032482147217 4.4 -3.129143238067627
		 5.2 -3.4048194885253902 6 -3.2581822872161865 6.8 -3.136624813079834 7.6 -3.0846998691558842
		 8.4 -2.4569211006164551 9.2 -1.3503856658935551 10 -0.46695166826248158 10.8 -0.1019490659236908
		 11.6 -0.1344500333070755 12.4 -0.23872688412666321 13.2 -0.29883873462677002 14 -0.37916123867034912
		 14.8 -0.51377320289611816 15.6 -0.75163304805755615 16.4 -0.93649041652679443 17.2 -0.93806695938110352
		 18 -1.0821542739868164 18.8 -1.6275849342346191 19.6 -2.0896096229553223 20.4 -1.8669061660766604
		 21.2 -1.1873780488967896 22 -0.59970003366470337 22.8 -0.43517976999282842 23.6 -0.63960105180740356
		 24.4 -0.68353927135467529 25.2 -0.44953164458274841 26 -0.31171295046806335 26.8 -0.44542798399925237
		 27.6 -0.87546664476394653 28.4 -1.3450987339019775 29.2 -1.4796546697616575 30 -1.373818039894104
		 30.8 -1.3896771669387815 31.6 -1.683000922203064 32.4 -2.0498535633087158 33.2 -2.0412776470184326
		 34 -1.6473362445831301 34.8 -1.5850977897644043 35.6 -2.2181460857391357 36.4 -2.9650518894195557
		 37.2 -3.2178442478179932 38 -3.0977075099945073 38.8 -3.0982909202575684 39.6 -3.2637035846710205
		 40.4 -3.2883346080780029 41.2 -3.1932077407836914 42 -3.2894399166107178 42.8 -3.8491549491882329
		 43.6 -4.7122564315795898 44.4 -5.2325382232666016 45.2 -5.1847209930419922 46 -4.7193479537963867
		 46.8 -4.1668119430541992 47.6 -4.1943821907043457 48.4 -4.8926124572753906 49.2 -5.3597807884216309
		 50 -5.2209734916687012 50.8 -4.9179034233093262 51.6 -4.5233817100524902 52.4 -4.1126518249511719
		 53.2 -4.1146321296691895 54 -4.6071610450744629 54.8 -5.1241736412048349 55.6 -5.2231130599975586
		 56.4 -5.1621227264404306 57.2 -5.3346295356750488 58 -5.3640670776367187 58.8 -4.8037614822387695
		 59.6 -4.0958051681518555 60.4 -3.7625849246978769 61.2 -3.6167860031127934 62 -3.5505013465881348
		 62.8 -3.6471431255340572 63.6 -3.6039791107177734 64.4 -3.385098934173584 65.2 -3.5751161575317383
		 66 -4.128150463104248 66.8 -4.1884489059448242 67.6 -3.5080435276031494 68.4 -2.6299383640289307
		 69.2 -2.1016216278076172 70 -2.0506958961486816 70.8 -2.1368775367736816 71.6 -2.1091198921203618
		 72.4 -2.1123931407928471;
createNode animCurveTA -n "Bip01_L_Thigh_rotateX5";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 91 ".ktv[0:90]"  0.4 -0.14619335532188416 1.2 -0.14559230208396912
		 2 -0.1393904983997345 2.8 -0.13400830328464508 3.6 -0.12635849416255951 4.4 -0.11974687129259107
		 5.2 -0.11130475997924805 6 -0.11340845376253128 6.8 -0.11329916864633562 7.6 -0.10728860646486282
		 8.4 -0.10209766030311584 9.2 -0.088847093284130096 10 -0.080350339412689223 10.8 -0.071225211024284363
		 11.6 -0.071771621704101563 12.4 -0.075377963483333588 13.2 -0.072973735630512238
		 14 -0.080814793705940247 14.8 -0.090404376387596116 15.6 -0.10939230024814606 16.4 -0.12827093899250033
		 17.2 -0.12766988575458532 18 -0.12816166877746582 18.8 -0.14507320523262024 19.6 -0.15613812208175659
		 20.4 -0.16130173206329346 21.2 -0.17324091494083405 22 -0.17477087676525116 22.8 -0.19362218677997589
		 23.6 -0.22066974639892575 24.4 -0.24017675220966339 25.2 -0.24356453120708466 26 -0.23859214782714841
		 26.8 -0.22749993205070493 27.6 -0.22646173834800712 28.4 -0.21159924566745761 29.2 -0.20263804495334625
		 30 -0.19482430815696716 30.8 -0.19526143372058868 31.6 -0.19875849783420563 32.4 -0.19772030413150787
		 33.2 -0.19550731778144836 34 -0.194878950715065 34.8 -0.19334898889064789 35.6 -0.1911633312702179
		 36.4 -0.18012574315071106 37.2 -0.17318627238273621 38 -0.16993509232997894 38.8 -0.16335079073905945
		 39.6 -0.15706701576709747 40.4 -0.14253237843513489 41.2 -0.12616725265979767 42 -0.11258883029222487
		 42.8 -0.10075894743204115 43.6 -0.088847093284130096 44.4 -0.083464905619621277 45.2 -0.083191700279712677
		 46 -0.07999517023563385 46.8 -0.08778158575296402 47.6 -0.097234569489955902 48.4 -0.088819772005081191
		 49.2 -0.08917494118213655 50 -0.093901433050632477 50.8 -0.098409362137317657 51.6 -0.1064143404364586
		 52.4 -0.10034913569688796 53.2 -0.092535398900508881 54 -0.097425810992717743 54.8 -0.099365584552288042
		 55.6 -0.10108679533004761 56.4 -0.10756181180477142 57.2 -0.11368165910243988 58 -0.10870928317308429
		 58.8 -0.11335381120443346 59.6 -0.1152389422059059 60.4 -0.11780709773302075 61.2 -0.13766928017139435
		 62 -0.13228709995746613 62.8 -0.1334892064332962 63.6 -0.14187668263912201 64.4 -0.14384377002716064
		 65.2 -0.14373449981212616 66 -0.13974566757678986 66.8 -0.1289539635181427 67.6 -0.1381610631942749
		 68.4 -0.14788724482059479 69.2 -0.14592015743255615 70 -0.1389806866645813 70.8 -0.14362521469593048
		 71.6 -0.13892604410648346 72.4 -0.13805177807807922;
createNode animCurveTA -n "Bip01_L_Thigh_rotateY5";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 91 ".ktv[0:90]"  0.4 -178.73870849609375 1.2 -178.47477722167969
		 2 -178.1397705078125 2.8 -177.70704650878906 3.6 -177.26501464843753 4.4 -176.80232238769531
		 5.2 -176.272216796875 6 -175.77033996582031 6.8 -175.39016723632812 7.6 -175.07647705078125
		 8.4 -174.74485778808594 9.2 -174.41783142089844 10 -174.19117736816406 10.8 -174.06355285644531
		 11.6 -173.96061706542969 12.4 -173.85569763183594 13.2 -173.78533935546875 14 -173.75616455078125
		 14.8 -173.73675537109375 15.6 -173.71672058105469 16.4 -173.7158203125 17.2 -173.72634887695312
		 18 -173.73684692382815 18.8 -173.77691650390625 19.6 -173.84745788574219 20.4 -173.93330383300781
		 21.2 -174.01956176757812 22 -174.11868286132812 22.8 -174.2803955078125 23.6 -174.57243347167972
		 24.4 -174.96200561523435 25.2 -175.35186767578125 26 -175.70152282714844 26.8 -176.04493713378906
		 27.6 -176.41169738769534 28.4 -176.77204895019531 29.2 -177.08819580078125 30 -177.46907043457031
		 30.8 -178.00093078613281 31.6 -178.54554748535156 32.4 -178.99159240722656 33.2 -179.37667846679687
		 34 -179.71357727050781 34.8 -180.00106811523435 35.6 -180.28511047363281 36.4 -180.55532836914065
		 37.2 -180.77259826660156 38 -180.95100402832031 38.8 -181.18096923828125 39.6 -181.55868530273435
		 40.4 -182.01802062988281 41.2 -182.41375732421875 42 -182.77128601074219 42.8 -183.16093444824219
		 43.6 -183.47865295410156 44.4 -183.64454650878903 45.2 -183.73226928710935 46 -183.8213806152344
		 46.8 -183.88566589355469 47.6 -183.94694519042969 48.4 -184.03083801269531 49.2 -184.08538818359375
		 50 -184.09965515136719 50.8 -184.07145690917969 51.6 -184.0487365722656 52.4 -184.05929565429687
		 53.2 -184.09486389160159 54 -184.11724853515625 54.8 -184.1175537109375 55.6 -184.10556030273435
		 56.4 -184.0731506347656 57.2 -184.01875305175781 58 -183.93150329589844 58.8 -183.84892272949219
		 59.6 -183.80255126953125 60.4 -183.7529296875 61.2 -183.66331481933594 62 -183.57048034667969
		 62.8 -183.46435546875 63.6 -183.26222229003903 64.4 -182.98670959472656 65.2 -182.75592041015625
		 66 -182.57626342773435 66.8 -182.35040283203128 67.6 -182.06181335449219 68.4 -181.77653503417969
		 69.2 -181.49134826660156 70 -181.17936706542972 70.8 -180.848876953125 71.6 -180.53642272949219
		 72.4 -180.26516723632812;
createNode animCurveTA -n "Bip01_L_Thigh_rotateZ5";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 91 ".ktv[0:90]"  0.4 -1.1880429983139038 1.2 -1.2937197685241699
		 2 -1.2931733131408691 2.8 -1.3148386478424072 3.6 -1.3978117704391479 4.4 -1.4842547178268433
		 5.2 -1.5060019493103027 6 -1.4626439809799197 6.8 -1.4082756042480469 7.6 -1.3850803375244141
		 8.4 -1.4335473775863647 9.2 -1.5726100206375122 10 -1.7195136547088625 10.8 -1.7813951969146729
		 11.6 -1.7763955593109133 12.4 -1.7119458913803101 13.2 -1.6540257930755615 14 -1.5984554290771484
		 14.8 -1.4275914430618286 15.6 -1.1376636028289795 16.4 -0.86500245332717884 17.2 -0.73025643825531006
		 18 -0.5676979422569276 18.8 -0.40240740776062017 19.6 -0.25389176607131958 20.4 -0.10034913569688796
		 21.2 0.044191323220729821 22 0.13060687482357025 22.8 0.42287063598632818 23.6 0.86985188722610485
		 24.4 1.222426176071167 25.2 1.3068473339080813 26 1.3071341514587402 26.8 1.4200098514556885
		 27.6 1.5140479803085327 28.4 1.414914608001709 29.2 1.1498076915740969 30 1.0690748691558838
		 30.8 1.3485387563705444 31.6 1.7581179141998291 32.4 2.12489914894104 33.2 2.3438065052032471
		 34 2.4242799282073975 34.8 2.5203807353973389 35.6 2.6735818386077881 36.4 2.7843811511993408
		 37.2 2.9044694900512695 38 3.0451440811157227 38.8 3.120890855789185 39.6 3.1553559303283691
		 40.4 3.2597076892852783 41.2 3.4070894718170166 42 3.59818434715271 42.8 3.7895252704620361
		 43.6 3.9448301792144775 44.4 3.8979477882385254 45.2 3.6896679401397705 46 3.4676322937011719
		 46.8 3.3154010772705078 47.6 3.2689282894134521 48.4 3.2420175075531006 49.2 3.0699377059936523
		 50 2.7515416145324707 50.8 2.5480155944824219 51.6 2.4019861221313477 52.4 2.3227560520172119
		 53.2 2.3342990875244141 54 2.3113632202148442 54.8 2.1123042106628418 55.6 1.8945304155349731
		 56.4 1.7561098337173462 57.2 1.6884363889694214 58 1.5803827047348022 58.8 1.3854081630706787
		 59.6 1.1671016216278076 60.4 0.96110320091247547 61.2 0.77802681922912587 62 0.64489275217056274
		 62.8 0.53239953517913818 63.6 0.53603321313858032 64.4 0.70152866840362549 65.2 0.90576499700546276
		 66 0.91601026058197055 66.8 0.6481165885925293 67.6 0.35202792286872864 68.4 0.16396550834178925
		 69.2 0.031241282820701603 70 -0.05660860612988472 70.8 -0.097507774829864502 71.6 -0.20184573531150815
		 72.4 -0.31416136026382446;
createNode animCurveTA -n "Bip01_L_Calf_rotateX5";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 91 ".ktv[0:90]"  0.4 -0.053342428058385849 1.2 -0.053726494312286377
		 2 -0.055489245802164078 2.8 -0.056736923754215247 3.6 -0.05629466846585273 4.4 -0.055356476455926895
		 5.2 -0.056143723428249352 6 -0.058206763118505478 6.8 -0.059716720134019866 7.6 -0.060470174998044968
		 8.4 -0.059767842292785651 9.2 -0.057222172617912286 10 -0.054113619029521942 10.8 -0.052257169038057327
		 11.6 -0.051877949386835098 12.4 -0.052835080772638321 13.2 -0.053291331976652145
		 14 -0.05225803330540657 14.8 -0.052217502146959305 15.6 -0.054625831544399261 16.4 -0.0566517673432827
		 17.2 -0.057038631290197372 18 -0.058020252734422698 18.8 -0.05959763377904892 19.6 -0.060944795608520522
		 20.4 -0.06316143274307251 21.2 -0.064388476312160492 22 -0.062661983072757721 22.8 -0.061887800693511963
		 23.6 -0.064805753529071808 24.4 -0.067479938268661499 25.2 -0.066751562058925629
		 26 -0.065373867750167847 26.8 -0.066248215734958649 27.6 -0.066733837127685561 28.4 -0.061382692307233817
		 29.2 -0.05162728950381279 30 -0.047187354415655136 30.8 -0.051212940365076065 31.6 -0.056761078536510468
		 32.4 -0.059253510087728507 33.2 -0.058280207216739655 34 -0.055614743381738663 34.8 -0.054755553603172302
		 35.6 -0.055525574833154678 36.4 -0.055661667138338096 37.2 -0.055753175169229507
		 38 -0.056160274893045425 38.8 -0.055429745465517037 39.6 -0.053758207708597183 40.4 -0.054623156785964966
		 41.2 -0.056646965444087982 42 -0.058297060430049896 42.8 -0.061099059879779809 43.6 -0.064180888235569
		 44.4 -0.063688255846500397 45.2 -0.060084715485572822 46 -0.056771941483020782 46.8 -0.055720046162605286
		 47.6 -0.05728457868099212 48.4 -0.058858059346675873 49.2 -0.056949514895677567 50 -0.052859082818031311
		 50.8 -0.050777178257703781 51.6 -0.050945628434419632 52.4 -0.052255928516387939
		 53.2 -0.055683717131614685 54 -0.058558296412229538 54.8 -0.057253692299127579 55.6 -0.055010605603456497
		 56.4 -0.055864699184894562 57.2 -0.058311935514211662 58 -0.059550169855356209 58.8 -0.058777101337909705
		 59.6 -0.056913275271654136 60.4 -0.055389620363712311 61.2 -0.054522067308425903
		 62 -0.054188903421163559 62.8 -0.05518655106425286 63.6 -0.058176085352897651 64.4 -0.064494125545024872
		 65.2 -0.072091639041900635 66 -0.074169725179672241 66.8 -0.069863036274909973 67.6 -0.065352283418178558
		 68.4 -0.063239119946956635 69.2 -0.062736809253692627 70 -0.063507512211799622 70.8 -0.065315991640090956
		 71.6 -0.06515009701251985 72.4 -0.063630074262619019;
createNode animCurveTA -n "Bip01_L_Calf_rotateY5";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 91 ".ktv[0:90]"  0.4 -1.0540242195129397 1.2 -1.0540517568588257
		 2 -1.0541375875473022 2.8 -1.0541988611221311 3.6 -1.0541938543319702 4.4 -1.0541654825210571
		 5.2 -1.0542196035385132 6 -1.0543228387832642 6.8 -1.0543839931488037 7.6 -1.0544215440750122
		 8.4 -1.0543583631515503 9.2 -1.0542316436767578 10 -1.0540889501571655 10.8 -1.0539796352386477
		 11.6 -1.0539497137069702 12.4 -1.0540027618408203 13.2 -1.0540359020233154 14 -1.0539695024490356
		 14.8 -1.0539422035217283 15.6 -1.0540962219238279 16.4 -1.0541737079620359 17.2 -1.054240345954895
		 18 -1.0542974472045898 18.8 -1.0543867349624634 19.6 -1.0544368028640747 20.4 -1.0545419454574585
		 21.2 -1.0545847415924072 22 -1.0545603036880491 22.8 -1.0544779300689695 23.6 -1.0546209812164309
		 24.4 -1.0547032356262207 25.2 -1.0546815395355225 26 -1.0546566247940063 26.8 -1.0546311140060425
		 27.6 -1.0546334981918335 28.4 -1.0544028282165527 29.2 -1.0539127588272097 30 -1.0536638498306274
		 30.8 -1.0538713932037354 31.6 -1.0541478395462036 32.4 -1.0542339086532593 33.2 -1.0541731119155884
		 34 -1.054049015045166 34.8 -1.0540112257003784 35.6 -1.0540025234222414 36.4 -1.0540103912353516
		 37.2 -1.0540452003479004 38 -1.0540837049484253 38.8 -1.0540628433227539 39.6 -1.0539811849594116
		 40.4 -1.0540224313735962 41.2 -1.0541411638259888 42 -1.054167628288269 42.8 -1.0543175935745239
		 43.6 -1.054412841796875 44.4 -1.0543653964996338 45.2 -1.0542005300521853 46 -1.0540741682052612
		 46.8 -1.0540523529052734 47.6 -1.0541331768035889 48.4 -1.0541772842407229 49.2 -1.0540519952774048
		 50 -1.0539385080337524 50.8 -1.0538449287414553 51.6 -1.0538918972015381 52.4 -1.0539480447769165
		 53.2 -1.0540825128555298 54 -1.0541853904724121 54.8 -1.054099440574646 55.6 -1.0539143085479736
		 56.4 -1.0539661645889282 57.2 -1.0541192293167114 58 -1.0541988611221311 58.8 -1.0541700124740601
		 59.6 -1.0540639162063601 60.4 -1.0540164709091189 61.2 -1.0539728403091433 62 -1.05387282371521
		 62.8 -1.0539505481719973 63.6 -1.0541117191314695 64.4 -1.054388165473938 65.2 -1.0546965599060061
		 66 -1.0547009706497192 66.8 -1.054547905921936 67.6 -1.0544166564941406 68.4 -1.0543135404586792
		 69.2 -1.0542700290679932 70 -1.0543091297149658 70.8 -1.054374098777771 71.6 -1.0544154644012451
		 72.4 -1.0543711185455322;
createNode animCurveTA -n "Bip01_L_Calf_rotateZ5";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 91 ".ktv[0:90]"  0.4 -0.69264680147171021 1.2 -0.67631196975708008
		 2 -0.55770635604858398 2.8 -0.4749462902545929 3.6 -0.51657956838607788 4.4 -0.59300470352172852
		 5.2 -0.55456382036209106 6 -0.42217338085174561 6.8 -0.31797331571578979 7.6 -0.26487681269645691
		 8.4 -0.28610050678253174 9.2 -0.44805115461349487 10 -0.66762983798980713 10.8 -0.77606797218322754
		 11.6 -0.79140973091125488 12.4 -0.7320423126220702 13.2 -0.7120203971862793 14 -0.76922357082366943
		 14.8 -0.74790966510772716 15.6 -0.61700481176376343 16.4 -0.46419578790664678 17.2 -0.47537416219711298
		 18 -0.41901174187660217 18.8 -0.33169424533843994 19.6 -0.2306766360998154 20.4 -0.092604734003543854
		 21.2 -0.0045467996969819069 22 -0.15725281834602356 22.8 -0.16816210746765137 23.6 0.0062280860729515553
		 24.4 0.20451977849006656 25.2 0.1479702889919281 26 0.03450993075966835 26.8 0.14236092567443848
		 27.6 0.19021038711071017 28.4 -0.15635654330253601 29.2 -0.79163646697998047 30 -1.082866430282593
		 30.8 -0.80103659629821777 31.6 -0.42696243524551392 32.4 -0.23413956165313723 33.2 -0.28686830401420593
		 34 -0.46550843119621271 34.8 -0.52701336145401001 35.6 -0.43655756115913402 36.4 -0.43081453442573542
		 37.2 -0.44934281706810003 38 -0.44039520621299744 38.8 -0.49733075499534607 39.6 -0.61108022928237915
		 40.4 -0.5536961555480957 41.2 -0.4354548454284668 42 -0.27930226922035223 42.8 -0.11212111264467239
		 43.6 0.11875826865434649 44.4 0.11675990372896196 45.2 -0.11867115646600726 46 -0.36124023795127874
		 46.8 -0.45970660448074341 47.6 -0.36026862263679504 48.4 -0.2283623218536377 49.2 -0.32508957386016846
		 50 -0.67482513189315796 50.8 -0.82815849781036388 51.6 -0.84913861751556396 52.4 -0.74764913320541371
		 53.2 -0.4882014691829682 54 -0.26634413003921509 54.8 -0.33259904384613037 55.6 -0.41728943586349498
		 56.4 -0.36842185258865362 57.2 -0.24094963073730469 58 -0.17321226000785828 58.8 -0.23522298038005829
		 59.6 -0.34323185682296753 60.4 -0.46294716000556946 61.2 -0.52143466472625732 62 -0.47049537301063538
		 62.8 -0.42729067802429199 63.6 -0.24378277361392978 64.4 0.17332418262958529 65.2 0.66106796264648438
		 66 0.84680670499801636 66.8 0.57193410396575928 67.6 0.23852913081645968 68.4 0.11182396113872528
		 69.2 0.093780465424060822 70 0.14101958274841309 70.8 0.26767662167549133 71.6 0.21737205982208252
		 72.4 0.10130536556243896;
createNode animCurveTA -n "Bip01_L_Foot_rotateY5";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 91 ".ktv[0:90]"  0.4 -1.5731785297393801 1.2 -1.6527364253997805
		 2 -1.6332154273986816 2.8 -1.6340785026550293 3.6 -1.7088605165481567 4.4 -1.7055854797363279
		 5.2 -1.6153508424758911 6 -1.5308864116668699 6.8 -1.468927264213562 7.6 -1.4272844791412354
		 8.4 -1.385188102722168 9.2 -1.4555132389068604 10 -1.501111626625061 10.8 -1.464940071105957
		 11.6 -1.4045586585998535 12.4 -1.3915317058563232 13.2 -1.3750777244567871 14 -1.3254927396774292
		 14.8 -1.2445378303527832 15.6 -1.1635333299636841 16.4 -1.1179141998291016 17.2 -1.1032946109771729
		 18 -1.1992144584655762 18.8 -1.192884087562561 19.6 -1.2238550186157229 20.4 -1.2153139114379885
		 21.2 -1.2600336074829102 22 -1.3469256162643433 22.8 -1.3086502552032473 23.6 -1.0090764760971067
		 24.4 -0.66629195213317871 25.2 -0.47862431406974781 26 -0.54696577787399292 26.8 -0.63126462697982788
		 27.6 -0.67087346315383922 28.4 -0.74904441833496094 29.2 -0.86972689628601074 30 -0.9404568076133728
		 30.8 -0.91611891984939553 31.6 -0.79903250932693493 32.4 -0.62124812602996826 33.2 -0.50261718034744263
		 34 -0.545806884765625 34.8 -0.58271443843841553 35.6 -0.42864072322845453 36.4 -0.32357922196388245
		 37.2 -0.43072754144668574 38 -0.64695405960083008 38.8 -0.79269063472747803 39.6 -0.77219092845916748
		 40.4 -0.66880220174789429 41.2 -0.59649050235748291 42 -0.55469447374343872 42.8 -0.4489530622959137
		 43.6 -0.30622264742851257 44.4 -0.21675516664981839 45.2 -0.23927205801010132 46 -0.26933816075325012
		 46.8 -0.33304473757743835 47.6 -0.36930939555168152 48.4 -0.29937401413917542 49.2 -0.23020020127296456
		 50 -0.33738315105438232 50.8 -0.54835414886474609 51.6 -0.70878291130065918 52.4 -0.68097913265228271
		 53.2 -0.58670473098754883 54 -0.49095425009727478 54.8 -0.42370441555976862 55.6 -0.31307575106620789
		 56.4 -0.27084633708000183 57.2 -0.30911588668823242 58 -0.42235875129699707 58.8 -0.46287629008293146
		 59.6 -0.38036707043647766 60.4 -0.28512662649154663 61.2 -0.25607019662857056 62 -0.20934595167636871
		 62.8 -0.12945401668548587 63.6 -0.14744217693805697 64.4 -0.26740929484367371 65.2 -0.33177226781845093
		 66 -0.24965633451938629 66.8 -0.16811603307723999 67.6 -0.16948986053466797 68.4 -0.12092521041631701
		 69.2 -0.050294898450374603 70 -0.04078838974237442 70.8 -0.11256927251815796 71.6 -0.24534688889980313
		 72.4 -0.31825113296508789;
createNode animCurveTA -n "Bip01_L_Foot_rotateZ5";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 91 ".ktv[0:90]"  0.4 1.4173158407211304 1.2 1.3872938156127932
		 2 1.3166185617446902 2.8 1.2225650548934937 3.6 1.2537027597427368 4.4 1.4317688941955566
		 5.2 1.5060114860534668 6 1.3643959760665894 6.8 1.1965227127075195 7.6 1.1134318113327026
		 8.4 1.1267508268356323 9.2 1.1615532636642456 10 1.1639127731323242 10.8 1.1799499988555908
		 11.6 1.220583438873291 12.4 1.2175143957138062 13.2 1.1550619602203369 14 1.1074798107147217
		 14.8 1.0958468914031982 15.6 1.0581251382827761 16.4 1.0322662591934204 17.2 1.0950945615768433
		 18 1.1489356756210327 18.8 1.1572502851486206 19.6 1.1459474563598633 20.4 1.1306798458099363
		 21.2 1.1641794443130491 22 1.2833726406097412 22.8 1.3874472379684448 23.6 1.4320809841156006
		 24.4 1.5563532114028933 25.2 1.7851133346557615 26 1.8249082565307613 26.8 1.5477718114852905
		 27.6 1.3640983104705815 28.4 1.4843168258666992 29.2 1.7320998907089231 30 1.8109980821609497
		 30.8 1.7071282863616943 31.6 1.62542724609375 32.4 1.6010160446166992 33.2 1.6434861421585083
		 34 1.7234351634979248 34.8 1.7288893461227417 35.6 1.6261544227600098 36.4 1.6123384237289429
		 37.2 1.7769365310668943 38 1.9701796770095823 38.8 2.0957355499267578 39.6 2.1769735813140869
		 40.4 2.190946102142334 41.2 2.1550860404968266 42 2.1612660884857182 42.8 2.2029383182525635
		 43.6 2.1354737281799316 44.4 2.0167849063873291 45.2 2.0244090557098389 46 2.1823177337646484
		 46.8 2.3341133594512939 47.6 2.2819552421569824 48.4 2.045259952545166 49.2 2.0282144546508789
		 50 2.2875354290008545 50.8 2.4387369155883789 51.6 2.4206538200378418 52.4 2.3875637054443359
		 53.2 2.2778525352478027 54 2.0868625640869141 54.8 1.9430198669433585 55.6 1.8705413341522215
		 56.4 1.8775224685668941 57.2 1.912221431732178 58 1.8968081474304199 58.8 1.9207448959350584
		 59.6 2.0409200191497803 60.4 2.107841968536377 61.2 1.9947311878204348 62 1.8629008531570435
		 62.8 1.8223332166671753 63.6 1.8137516975402832 64.4 1.7136818170547483 65.2 1.5248450040817261
		 66 1.3575960397720337 66.8 1.358745813369751 67.6 1.4145416021347048 68.4 1.4289935827255249
		 69.2 1.3973557949066162 70 1.2715922594070437 70.8 1.1205723285675049 71.6 1.2328389883041382
		 72.4 1.5381013154983521;
createNode animCurveTA -n "Bip01_L_Foot_rotateX5";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 91 ".ktv[0:90]"  0.4 -3.9386701583862305 1.2 -3.7674553394317627
		 2 -3.8000712394714351 2.8 -3.7653055191040039 3.6 -3.6572268009185791 4.4 -3.6801407337188716
		 5.2 -3.8638603687286377 6 -4.0282444953918457 6.8 -4.1345453262329102 7.6 -4.2642817497253418
		 8.4 -4.2877774238586426 9.2 -4.1527433395385742 10 -4.0543828010559082 10.8 -4.1131687164306641
		 11.6 -4.234774112701416 12.4 -4.2775640487670898 13.2 -4.3063712120056152 14 -4.4014334678649902
		 14.8 -4.5441789627075195 15.6 -4.6813554763793945 16.4 -4.7858080863952637 17.2 -4.8075408935546875
		 18 -4.6807432174682617 18.8 -4.6220722198486328 19.6 -4.6359400749206543 20.4 -4.6426029205322266
		 21.2 -4.5101675987243652 22 -4.3278236389160156 22.8 -4.4408745765686035 23.6 -5.0119609832763672
		 24.4 -5.7404947280883789 25.2 -6.0601248741149911 26 -5.963129997253418 26.8 -5.7678351402282715
		 27.6 -5.667292594909668 28.4 -5.5123906135559082 29.2 -5.2921600341796884 30 -5.1738643646240234
		 30.8 -5.2016034126281738 31.6 -5.4327726364135742 32.4 -5.8130135536193848 33.2 -5.9881463050842285
		 34 -5.9475970268249512 34.8 -5.886505126953125 35.6 -6.0658731460571289 36.4 -6.3303790092468262
		 37.2 -6.1456789970397949 38 -5.7040443420410156 38.8 -5.4645700454711914 39.6 -5.5022730827331543
		 40.4 -5.7127838134765625 41.2 -5.8347625732421875 42 -5.8833446502685547 42.8 -6.0776009559631348
		 43.6 -6.3736639022827148 44.4 -6.5232124328613281 45.2 -6.5194764137268066 46 -6.4369621276855469
		 46.8 -6.2891874313354492 47.6 -6.2136688232421875 48.4 -6.3516254425048828 49.2 -6.450007438659668
		 50 -6.3028130531311035 50.8 -5.9035034179687509 51.6 -5.6082892417907715 52.4 -5.6599926948547372
		 53.2 -5.8442263603210449 54 -6.0149188041687012 54.8 -6.1911740303039551 55.6 -6.3407621383666992
		 56.4 -6.4325437545776367 57.2 -6.365847110748291 58 -6.1388492584228516 58.8 -6.0282778739929199
		 59.6 -6.2107510566711426 60.4 -6.4093170166015625 61.2 -6.450444221496582 62 -6.5630688667297372
		 62.8 -6.704808235168457 63.6 -6.6641182899475098 64.4 -6.4236083030700684 65.2 -6.3332686424255371
		 66 -6.4724564552307129 66.8 -6.6292233467102051 67.6 -6.6353540420532227 68.4 -6.7032318115234375
		 69.2 -6.8485984802246094 70 -6.8959474563598633 70.8 -6.7671647071838379 71.6 -6.5152239799499512
		 72.4 -6.3641200065612793;
createNode animCurveTA -n "Bip01_R_Thigh_rotateX5";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 91 ".ktv[0:90]"  0.4 0.2263934314250946 1.2 0.22692619264125824
		 2 0.22252754867076871 2.8 0.21583396196365356 3.6 0.20937260985374451 4.4 0.21133969724178311
		 5.2 0.20808853209018707 6 0.20651759207248688 6.8 0.20784264802932739 7.6 0.20889449119567871
		 8.4 0.20685909688472748 9.2 0.20885351300239563 10 0.2066815197467804 10.8 0.20575261116027832
		 11.6 0.20871691405773163 12.4 0.20952287316322327 13.2 0.20677714049816132 14 0.20034310221672055
		 14.8 0.19802083075046539 15.6 0.18874543905258179 16.4 0.1900021880865097 17.2 0.18022136390209201
		 18 0.17722973227500916 18.8 0.17951102554798126 19.6 0.17869140207767489 20.4 0.19338996708393097
		 21.2 0.1892098933458328 22 0.18345886468887329 22.8 0.18530301749706268 23.6 0.18692860007286072
		 24.4 0.17862309515476227 25.2 0.17498943209648132 26 0.17740732431411743 26.8 0.18481124937534332
		 27.6 0.18919622898101807 28.4 0.1909584254026413 29.2 0.1950838565826416 30 0.20192770659923551
		 30.8 0.21248717606067655 31.6 0.22430339455604556 32.4 0.22957630455493927 33.2 0.23592838644981384
		 34 0.24663811922073359 34.8 0.25719758868217468 35.6 0.26278468966484075 36.4 0.26658228039741516
		 37.2 0.27102190256118774 38 0.27719637751579285 38.8 0.28167697787284851 39.6 0.29973599314689636
		 40.4 0.31040474772453308 41.2 0.32597759366035461 42 0.31534981727600098 42.8 0.33040356636047363
		 43.6 0.3558255136013031 44.4 0.36589321494102478 45.2 0.35525178909301758 46 0.34466499090194702
		 46.8 0.33364105224609375 47.6 0.33713811635971069 48.4 0.31750816106796265 49.2 0.29899835586547852
		 50 0.25617307424545288 50.8 0.24805879592895513 51.6 0.25525781512260437 52.4 0.27520197629928589
		 53.2 0.27375397086143494 54 0.27449163794517517 54.8 0.26170551776885986 55.6 0.25148755311965942
		 56.4 0.24229411780834204 57.2 0.2385784983634949 58 0.21692679822444916 58.8 0.2055886834859848
		 59.6 0.18634121119976044 60.4 0.18579480051994324 61.2 0.1707000732421875 62 0.17165631055831909
		 62.8 0.15464913845062256 63.6 0.16287268698215485 64.4 0.15703970193862915 65.2 0.16503101587295532
		 66 0.15899313986301425 66.8 0.14097510278224945 67.6 0.14545570313930511 68.4 0.14045600593090057
		 69.2 0.14004619419574738 70 0.13747803866863251 70.8 0.14010083675384519 71.6 0.14582453668117523
		 72.4 0.1406882256269455;
createNode animCurveTA -n "Bip01_R_Thigh_rotateZ5";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 91 ".ktv[0:90]"  0.4 -0.14143954217433927 1.2 -0.06961328536272049
		 2 -0.025708829984068871 2.8 -0.036801058799028404 3.6 -0.026856303215026855 4.4 -0.065323926508426666
		 5.2 -0.1502368301153183 6 -0.18285781145095828 6.8 -0.20269268751144409 7.6 -0.25744348764419556
		 8.4 -0.30700331926345825 9.2 -0.28405389189720154 10 -0.2462419718503952 10.8 -0.26323547959327698
		 11.6 -0.32804030179977423 12.4 -0.33850416541099548 13.2 -0.21280136704444885 14 -0.049150038510560989
		 14.8 0.0674276202917099 15.6 0.22834686934947965 16.4 0.40130090713500977 17.2 0.53800028562545776
		 18 0.72675943374633789 18.8 0.84713464975357067 19.6 0.66594338417053223 20.4 0.47441124916076666
		 21.2 0.42177781462669384 22 0.53323286771774292 22.8 0.69342809915542603 23.6 0.70647376775741577
		 24.4 0.51249641180038452 25.2 0.40468868613243114 26 0.5158158540725708 26.8 0.64598560333251953
		 27.6 0.77202987670898438 28.4 0.84475773572921764 29.2 0.89673548936843872 30 0.98987191915512096
		 30.8 1.1406415700912476 31.6 1.2581890821456907 32.4 1.3234174251556396 33.2 1.3593441247940063
		 34 1.3295098543167114 34.8 1.2847175598144531 35.6 1.4074423313140869 36.4 1.5695773363113403
		 37.2 1.7167270183563232 38 1.8751873970031741 38.8 1.994633674621582 39.6 2.0358061790466309
		 40.4 2.0732355117797852 41.2 2.0969090461730961 42 2.0418713092803955 42.8 2.0651350021362305
		 43.6 2.2397828102111816 44.4 2.2880175113677979 45.2 2.167560338973999 46 1.9817246198654177
		 46.8 1.8807061910629279 47.6 1.8120900392532349 48.4 1.7336385250091553 49.2 1.4869594573974607
		 50 1.1098237037658691 50.8 0.81242364645004272 51.6 0.75201743841171265 52.4 0.75913453102111805
		 53.2 0.75532323122024547 54 0.65450966358184814 54.8 0.52946257591247559 55.6 0.40877312421798706
		 56.4 0.28581607341766357 57.2 0.16247652471065524 58 -0.066416755318641663 58.8 -0.33506172895431519
		 59.6 -0.57064861059188843 60.4 -0.68605148792266846 61.2 -0.81249195337295532 62 -0.99745345115661599
		 62.8 -1.1614326238632202 63.6 -1.2129595279693604 64.4 -1.1181565523147583 65.2 -1.0648810863494873
		 66 -1.2337507009506226 66.8 -1.4639279842376709 67.6 -1.6171427965164185 68.4 -1.7289392948150637
		 69.2 -1.8665267229080198 70 -2.0318446159362793 70.8 -2.1997306346893311 71.6 -2.329094409942627
		 72.4 -2.486461877822876;
createNode animCurveTA -n "Bip01_R_Thigh_rotateY5";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 91 ".ktv[0:90]"  0.4 178.70684814453125 1.2 178.98333740234375
		 2 179.35235595703125 2.8 179.81962585449219 3.6 180.28240966796875 4.4 180.762939453125
		 5.2 181.31410217285156 6 181.82550048828125 6.8 182.20285034179687 7.6 182.51229858398435
		 8.4 182.83984375 9.2 183.15042114257812 10 183.35734558105469 10.8 183.47764587402344
		 11.6 183.60896301269531 12.4 183.74760437011719 13.2 183.83543395996097 14 183.86131286621097
		 14.8 183.869140625 15.6 183.88755798339844 16.4 183.88868713378903 17.2 183.86552429199219
		 18 183.8358154296875 18.8 183.80607604980469 19.6 183.75880432128903 20.4 183.68527221679687
		 21.2 183.583251953125 22 183.46371459960935 22.8 183.29180908203128 23.6 183.01922607421875
		 24.4 182.6611328125 25.2 182.29130554199219 26 181.9500732421875 26.8 181.61839294433594
		 27.6 181.25361633300781 28.4 180.85820007324219 29.2 180.49252319335935 30 180.07986450195312
		 30.8 179.53785705566406 31.6 179.00709533691406 32.4 178.58830261230472 33.2 178.21273803710935
		 34 177.85905456542969 34.8 177.56164550781253 35.6 177.30464172363281 36.4 177.05149841308594
		 37.2 176.80723571777344 38 176.59846496582034 38.8 176.37199401855469 39.6 176.00021362304687
		 40.4 175.52293395996094 41.2 175.09844970703125 42 174.72947692871094 42.8 174.37922668457031
		 43.6 174.11183166503906 44.4 173.9674072265625 45.2 173.86260986328125 46 173.74151611328125
		 46.8 173.67561340332031 47.6 173.62896728515625 48.4 173.56101989746094 49.2 173.48825073242187
		 50 173.45405578613281 50.8 173.4500732421875 51.6 173.45458984375 52.4 173.45368957519531
		 53.2 173.43400573730469 54 173.42649841308594 54.8 173.44259643554687 55.6 173.47259521484375
		 56.4 173.5179443359375 57.2 173.58924865722656 58 173.66876220703125 58.8 173.72492980957031
		 59.6 173.78912353515625 60.4 173.87126159667969 61.2 173.95428466796875 62 174.03466796875
		 62.8 174.15513610839847 63.6 174.37525939941406 64.4 174.64739990234375 65.2 174.86935424804687
		 66 175.05473327636719 66.8 175.29641723632812 67.6 175.59616088867187 68.4 175.9005126953125
		 69.2 176.20852661132812 70 176.55067443847656 70.8 176.89262390136719 71.6 177.18867492675781
		 72.4 177.44650268554687;
createNode animCurveTA -n "Bip01_R_Calf_rotateX5";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 91 ".ktv[0:90]"  0.4 0.062929876148700714 1.2 0.06423410028219223
		 2 0.064497046172618866 2.8 0.063666462898254395 3.6 0.062792927026748657 4.4 0.061456229537725449
		 5.2 0.06000162661075592 6 0.059787072241306312 6.8 0.059239927679300294 7.6 0.057169113308191299
		 8.4 0.056212145835161216 9.2 0.057298652827739723 10 0.058221202343702323 10.8 0.05718496069312097
		 11.6 0.054026547819375992 12.4 0.052359674125909805 13.2 0.054886266589164734 14 0.057301942259073264
		 14.8 0.057260066270828247 15.6 0.058351796120405204 16.4 0.060377836227416992 17.2 0.062451478093862554
		 18 0.066431932151317596 18.8 0.067489057779312134 19.6 0.061373729258775711 20.4 0.054554272443056107
		 21.2 0.052671048790216446 22 0.054351501166820526 22.8 0.055662266910076141 23.6 0.051822729408741011
		 24.4 0.044299576431512833 25.2 0.041222605854272842 26 0.04414813220500946 26.8 0.048022933304309845
		 27.6 0.051011737436056137 28.4 0.053228214383125305 29.2 0.054841678589582443 30 0.057379927486181245
		 30.8 0.059967029839754105 31.6 0.060167077928781502 32.4 0.059036463499069207 33.2 0.057439301162958152
		 34 0.054074816405773163 34.8 0.051477212458848953 35.6 0.052923254668712616 36.4 0.056004166603088379
		 37.2 0.058428723365068443 38 0.061135053634643548 38.8 0.063169747591018677 39.6 0.062938600778579712
		 40.4 0.062396354973316186 41.2 0.061700776219367974 42 0.059445001184940338 42.8 0.059674065560102463
		 43.6 0.064414359629154205 44.4 0.067605547606945038 45.2 0.066069774329662323 46 0.06336820125579834
		 46.8 0.062722422182559967 47.6 0.064131192862987518 48.4 0.064296789467334747 49.2 0.060373574495315559
		 50 0.054029364138841629 50.8 0.050215084105730057 51.6 0.051727771759033203 52.4 0.056298572570085546
		 53.2 0.059912201017141342 54 0.061166275292634964 54.8 0.061637450009584434 55.6 0.062868468463420868
		 56.4 0.064381733536720276 57.2 0.064627721905708313 58 0.062533952295780182 58.8 0.058909270912408829
		 59.6 0.056823570281267173 60.4 0.057766474783420563 61.2 0.058635830879211433 62 0.057274568825960152
		 62.8 0.056341271847486489 63.6 0.058330744504928589 64.4 0.062913469970226288 65.2 0.065978154540061951
		 66 0.06351315975189209 66.8 0.059366472065448761 67.6 0.05834956094622612 68.4 0.058271750807762139
		 69.2 0.056640125811099999 70 0.053762242197990417 70.8 0.050169013440608978 71.6 0.045663710683584213
		 72.4 0.040625963360071182;
createNode animCurveTA -n "Bip01_R_Calf_rotateZ5";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 91 ".ktv[0:90]"  0.4 0.13963772356510162 1.2 0.21530687808990481
		 2 0.2677653431892395 2.8 0.21368913352489471 3.6 0.17942368984222412 4.4 0.08370873332023622
		 5.2 -0.02341313473880291 6 -0.034521959722042091 6.8 -0.074970163404941573 7.6 -0.19682477414607999
		 8.4 -0.25245499610900879 9.2 -0.15822383761405945 10 -0.103625550866127 10.8 -0.20049479603767395
		 11.6 -0.3912183940410614 12.4 -0.47758024930953968 13.2 -0.33512410521507263 14 -0.18315108120441437
		 14.8 -0.19975659251213071 15.6 -0.137440025806427 16.4 0.03790351003408432 17.2 0.19961133599281311
		 18 0.43496519327163696 18.8 0.52215558290481567 19.6 0.075789794325828538 20.4 -0.37185376882553106
		 21.2 -0.54738324880599976 22 -0.42607483267784119 22.8 -0.32624664902687073 23.6 -0.58959388732910156
		 24.4 -1.1302074193954468 25.2 -1.3588411808013916 26 -1.1485699415206907 26.8 -0.91278368234634399
		 27.6 -0.66947090625762939 28.4 -0.51084458827972412 29.2 -0.38161110877990723 30 -0.2178799510002136
		 30.8 -0.063192971050739288 31.6 -0.068678058683872223 32.4 -0.18076515197753903 33.2 -0.29770547151565552
		 34 -0.49212849140167236 34.8 -0.69990301132202148 35.6 -0.5821683406829834 36.4 -0.40214225649833679
		 37.2 -0.25257581472396851 38 -0.0761222243309021 38.8 0.05166136845946312 39.6 0.031612802296876907
		 40.4 0.012513790279626848 41.2 -0.014949935488402843 42 -0.17527279257774353 42.8 -0.18821604549884796
		 43.6 0.1461111307144165 44.4 0.32096940279006958 45.2 0.23796404898166659 46 0.052319806069135666
		 46.8 0.092672511935234084 47.6 0.17603424191474917 48.4 0.2211420089006424 49.2 -0.037922684103250504
		 50 -0.46005868911743159 50.8 -0.73332357406616211 51.6 -0.60878098011016846 52.4 -0.33406871557235718
		 53.2 -0.075365051627159119 54 -0.025024162605404857 54.8 0.004649811889976263 55.6 0.079653017222881317
		 56.4 0.1525639146566391 57.2 0.1911497563123703 58 0.01103070005774498 58.8 -0.22697746753692624
		 59.6 -0.35004124045372015 60.4 -0.2750403881072998 61.2 -0.23176246881484985 62 -0.30110570788383484
		 62.8 -0.36034670472145081 63.6 -0.25529232621192932 64.4 0.080108277499675751 65.2 0.29104328155517578
		 66 0.094214640557765975 66.8 -0.16274760663509369 67.6 -0.22866015136241913 68.4 -0.22561831772327426
		 69.2 -0.30865049362182617 70 -0.47730469703674328 70.8 -0.73765307664871216 71.6 -1.0322164297103882
		 72.4 -1.3704534769058228;
createNode animCurveTA -n "Bip01_R_Calf_rotateY5";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 91 ".ktv[0:90]"  0.4 1.0541678667068479 1.2 1.0542367696762085
		 2 1.0542037487030027 2.8 1.0541728734970093 3.6 1.0541034936904907 4.4 1.0540549755096438
		 5.2 1.0540049076080322 6 1.0539915561676023 6.8 1.0539702177047727 7.6 1.0538558959960937
		 8.4 1.0538022518157959 9.2 1.0538283586502075 10 1.0538786649703979 10.8 1.0538614988327026
		 11.6 1.0536834001541138 12.4 1.0535711050033567 13.2 1.0537307262420654 14 1.0538569688796997
		 14.8 1.0538744926452637 15.6 1.0539346933364868 16.4 1.053980827331543 17.2 1.0540411472320557
		 18 1.0542387962341309 18.8 1.0542546510696411 19.6 1.0540565252304075 20.4 1.0537277460098269
		 21.2 1.053691029548645 22 1.0537674427032473 22.8 1.0538175106048584 23.6 1.0536314249038696
		 24.4 1.0532609224319458 25.2 1.0530960559844973 26 1.0532586574554443 26.8 1.0535043478012085
		 27.6 1.053618311882019 28.4 1.0537251234054563 29.2 1.0537798404693604 30 1.0539060831069946
		 30.8 1.0540496110916138 31.6 1.0540722608566284 32.4 1.0540659427642822 33.2 1.0540069341659546
		 34 1.0538085699081421 34.8 1.0537185668945312 35.6 1.0537635087966919 36.4 1.053949236869812
		 37.2 1.0540798902511597 38 1.0542038679122925 38.8 1.0543003082275393 39.6 1.0542973279953003
		 40.4 1.0542459487915039 41.2 1.0542010068893433 42 1.0541167259216309 42.8 1.0541583299636841
		 43.6 1.0543354749679563 44.4 1.0545068979263306 45.2 1.0544254779815674 46 1.0543214082717896
		 46.8 1.0541962385177612 47.6 1.054265022277832 48.4 1.0542359352111816 49.2 1.0540655851364136
		 50 1.0537631511688237 50.8 1.0535882711410522 51.6 1.0536438226699829 52.4 1.0539038181304932
		 53.2 1.0540540218353271 54 1.0541489124298096 54.8 1.0541672706604004 55.6 1.0542309284210205
		 56.4 1.0543282032012939 57.2 1.0543090105056765 58 1.0542677640914917 58.8 1.0541051626205444
		 59.6 1.0539950132369995 60.4 1.0540181398391724 61.2 1.0540783405303955 62 1.053992748260498
		 62.8 1.053938627243042 63.6 1.0540667772293095 64.4 1.0542359352111816 65.2 1.0543521642684937
		 66 1.0542948246002195 66.8 1.0540866851806641 67.6 1.054039478302002 68.4 1.0540258884429932
		 69.2 1.0539239645004272 70 1.0537513494491575 70.8 1.0535868406295776 71.6 1.053330659866333
		 72.4 1.0530272722244265;
createNode animCurveTA -n "Bip01_R_Foot_rotateX5";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 91 ".ktv[0:90]"  0.4 10.798530578613279 1.2 11.000628471374512
		 2 11.170655250549316 2.8 11.33434009552002 3.6 11.481361389160156 4.4 11.532802581787108
		 5.2 11.449816703796388 6 11.27706241607666 6.8 11.063475608825684 7.6 10.855507850646973
		 8.4 10.842275619506836 9.2 10.802871704101564 10 10.635410308837892 10.8 10.50991153717041
		 11.6 10.643450736999512 12.4 10.925130844116213 13.2 11.037426948547363 14 10.929135322570801
		 14.8 10.687405586242676 15.6 10.570354461669922 16.4 10.51697826385498 17.2 10.511303901672363
		 18 10.482721328735352 18.8 10.50893020629883 19.6 10.663675308227541 20.4 10.72429370880127
		 21.2 10.741211891174316 22 10.701676368713381 22.8 10.511576652526855 23.6 10.111766815185549
		 24.4 9.7511997222900391 25.2 9.6297531127929687 26 9.7070169448852539 26.8 9.830815315246582
		 27.6 9.8701524734497088 28.4 9.7731685638427734 29.2 9.6303110122680664 30 9.5238962173461914
		 30.8 9.4860811233520508 31.6 9.3719692230224609 32.4 9.1467113494873047 33.2 8.8683366775512695
		 34 8.5996913909912109 34.8 8.4491949081420898 35.6 8.5418243408203125 36.4 8.6000347137451172
		 37.2 8.5542240142822266 38 8.6675176620483398 38.8 8.9354515075683594 39.6 8.925816535949707
		 40.4 8.5502910614013672 41.2 8.0680665969848633 42 7.8312740325927725 42.8 7.8724184036254883
		 43.6 7.982977867126464 44.4 8.0712928771972656 45.2 8.0556573867797852 46 8.0492734909057617
		 46.8 8.2985210418701172 47.6 8.499053955078125 48.4 8.4335241317749023 49.2 8.2769193649291992
		 50 8.2441616058349627 50.8 8.2745676040649432 51.6 8.29052734375 52.4 8.2577199935913086
		 53.2 8.217165946960451 54 8.1715593338012695 54.8 8.2300901412963867 55.6 8.2469310760498047
		 56.4 8.2798242568969727 57.2 8.4022684097290039 58 8.4586877822875977 58.8 8.3605442047119141
		 59.6 8.3366794586181641 60.4 8.3260583877563477 61.2 8.1490602493286133 62 7.9093518257141104
		 62.8 7.9270906448364276 63.6 8.1119775772094727 64.4 8.2865781784057617 65.2 8.1883068084716797
		 66 7.9752264022827148 66.8 7.9403796195983869 67.6 8.1418609619140625 68.4 8.3011302947998047
		 69.2 8.2864360809326172 70 8.418400764465332 70.8 8.6421241760253906 71.6 8.7712688446044922
		 72.4 8.8235073089599609;
createNode animCurveTA -n "Bip01_R_Foot_rotateY5";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 91 ".ktv[0:90]"  0.4 -2.0271115303039551 1.2 -2.0991110801696777
		 2 -2.1975359916687012 2.8 -2.2964327335357666 3.6 -2.3806121349334717 4.4 -2.4165782928466797
		 5.2 -2.3888537883758545 6 -2.3017997741699219 6.8 -2.1849193572998047 7.6 -2.1067438125610352
		 8.4 -2.0765674114227295 9.2 -2.0191962718963623 10 -1.9628976583480835 10.8 -1.8747615814208984
		 11.6 -1.9926049709320068 12.4 -2.0999152660369873 13.2 -2.1770546436309814 14 -2.1063625812530522
		 14.8 -1.9980615377426143 15.6 -1.9372299909591677 16.4 -1.9138317108154299 17.2 -1.8843338489532475
		 18 -1.8722563982009888 18.8 -1.9211801290512087 19.6 -1.9820802211761477 20.4 -2.0343718528747559
		 21.2 -2.0422625541687012 22 -2.0217251777648926 22.8 -1.9039046764373779 23.6 -1.7037602663040159
		 24.4 -1.4995638132095337 25.2 -1.435429573059082 26 -1.4813783168792725 26.8 -1.5377891063690186
		 27.6 -1.5586956739425659 28.4 -1.4817122220993042 29.2 -1.3916590213775637 30 -1.3366470336914062
		 30.8 -1.2763484716415403 31.6 -1.2528423070907593 32.4 -1.146894097328186 33.2 -0.98814964294433605
		 34 -0.8449978232383728 34.8 -0.76303917169570923 35.6 -0.81814450025558461 36.4 -0.84760618209838867
		 37.2 -0.81827825307846069 38 -0.87825304269790661 38.8 -1.0018347501754761 39.6 -1.0139961242675779
		 40.4 -0.81026965379714955 41.2 -0.54455119371414185 42 -0.44760894775390625 42.8 -0.45847907662391668
		 43.6 -0.53997361660003662 44.4 -0.53985005617141724 45.2 -0.56489676237106323 46 -0.54083657264709473
		 46.8 -0.67371368408203125 47.6 -0.79058158397674561 48.4 -0.71731764078140259 49.2 -0.65185624361038208
		 50 -0.63581293821334839 50.8 -0.68014985322952271 51.6 -0.65015047788619995 52.4 -0.63480067253112793
		 53.2 -0.65118473768234253 54 -0.61184942722320557 54.8 -0.6475098729133606 55.6 -0.69862586259841919
		 56.4 -0.66193592548370361 57.2 -0.73599135875701904 58 -0.76540464162826549 58.8 -0.71929562091827393
		 59.6 -0.70412999391555786 60.4 -0.70767962932586681 61.2 -0.60549283027648926 62 -0.50116389989852905
		 62.8 -0.48735028505325323 63.6 -0.6064179539680481 64.4 -0.65555065870285034 65.2 -0.6181519627571106
		 66 -0.51468735933303833 66.8 -0.49935916066169744 67.6 -0.60135650634765625 68.4 -0.67415922880172729
		 69.2 -0.68342643976211548 70 -0.74385106563568115 70.8 -0.84286648035049438 71.6 -0.9194689989089968
		 72.4 -0.97099137306213368;
createNode animCurveTA -n "Bip01_R_Foot_rotateZ5";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 91 ".ktv[0:90]"  0.4 2.2416768074035645 1.2 2.3123948574066162
		 2 2.2901589870452885 2.8 2.2766001224517822 3.6 2.3368477821350098 4.4 2.4865796566009521
		 5.2 2.6159803867340088 6 2.6328179836273193 6.8 2.6353559494018555 7.6 2.6711418628692627
		 8.4 2.6031897068023682 9.2 2.4232470989227295 10 2.3459255695343018 10.8 2.4260869026184082
		 11.6 2.5636937618255615 12.4 2.6405491828918457 13.2 2.5741748809814453 14 2.5692727565765381
		 14.8 2.7096095085144043 15.6 2.7279298305511475 16.4 2.5587475299835205 17.2 2.3343582153320313
		 18 2.1604654788970947 18.8 2.2478122711181641 19.6 2.616621732711792 20.4 3.0781481266021729
		 21.2 3.2862341403961186 22 3.0618312358856201 22.8 2.8157908916473389 23.6 2.9775242805480957
		 24.4 3.2924759387969971 25.2 3.3642952442169189 26 3.2722153663635254 26.8 3.1123819351196289
		 27.6 2.800180196762085 28.4 2.419161319732666 29.2 2.1099646091461186 30 1.9193639755249023
		 30.8 1.9031448364257813 31.6 2.032306432723999 32.4 2.106776237487793 33.2 2.0496740341186523
		 34 2.0207126140594478 34.8 2.0427875518798828 35.6 1.9895983934402468 36.4 1.8937581777572634
		 37.2 1.8460427522659304 38 1.808297872543335 38.8 1.7750049829483032 39.6 1.7753159999847412
		 40.4 1.727590799331665 41.2 1.641838550567627 42 1.6199604272842407 42.8 1.5862537622451782
		 43.6 1.4345437288284302 44.4 1.337037205696106 45.2 1.3626694679260254 46 1.3375751972198486
		 46.8 1.1239430904388428 47.6 0.84819704294204701 48.4 0.81686305999755859 49.2 1.1163537502288818
		 50 1.4140493869781494 50.8 1.5282983779907229 51.6 1.477774977684021 52.4 1.2158143520355225
		 53.2 0.96672922372818004 54 0.98540234565734863 54.8 1.1052838563919067 55.6 1.1493808031082151
		 56.4 1.1458742618560791 57.2 1.204220175743103 58 1.4792364835739136 58.8 1.7738122940063477
		 59.6 1.7047162055969238 60.4 1.4526610374450684 61.2 1.3390966653823857 62 1.3304460048675537
		 62.8 1.3252935409545898 63.6 1.222029447555542 64.4 0.98161327838897716 65.2 0.88968008756637573
		 66 1.1537871360778809 66.8 1.4020118713378906 67.6 1.3463703393936157 68.4 1.2467788457870483
		 69.2 1.2762898206710815 70 1.372663140296936 70.8 1.5653544664382937 71.6 1.946141242980957
		 72.4 2.2809207439422607;
select -ne :time1;
	setAttr ".o" 24.2;
	setAttr ".unw" 24.2;
select -ne :renderPartition;
	setAttr -s 126 ".st";
select -ne :initialShadingGroup;
	setAttr ".ro" yes;
select -ne :initialParticleSE;
	setAttr ".ro" yes;
select -ne :defaultShaderList1;
	setAttr -s 64 ".s";
select -ne :defaultTextureList1;
	setAttr -s 31 ".tx";
select -ne :postProcessList1;
	setAttr -s 2 ".p";
select -ne :defaultRenderUtilityList1;
	setAttr -s 31 ".u";
select -ne :defaultRenderingList1;
select -ne :renderGlobalsList1;
select -ne :defaultResolution;
	setAttr ".pa" 1;
select -ne :hardwareRenderGlobals;
	setAttr ".ctrs" 256;
	setAttr ".btrs" 512;
select -ne :hardwareRenderingGlobals;
	setAttr ".otfna" -type "stringArray" 18 "NURBS Curves" "NURBS Surfaces" "Polygons" "Subdiv Surfaces" "Particles" "Fluids" "Image Planes" "UI:" "Lights" "Cameras" "Locators" "Joints" "IK Handles" "Deformers" "Motion Trails" "Components" "Misc. UI" "Ornaments"  ;
	setAttr ".otfva" -type "Int32Array" 18 0 1 1 1 1 1
		 1 0 0 0 0 0 0 0 0 0 0 0 ;
select -ne :defaultHardwareRenderGlobals;
	setAttr ".fn" -type "string" "im";
	setAttr ".res" -type "string" "ntsc_4d 646 485 1.333";
select -ne :hyperGraphLayout;
	setAttr -s 4 ".hyp";
	setAttr ".hyp[1].isc" yes;
	setAttr ".hyp[2].isc" yes;
	setAttr ".hyp[3].isc" yes;
	setAttr ".hyp[4].isc" yes;
connectAttr "Bip01_Spine_translateX5.o" "|Stand_Idle_3|Bip01_Spine.tx";
connectAttr "Bip01_Spine_translateY5.o" "|Stand_Idle_3|Bip01_Spine.ty";
connectAttr "Bip01_Spine_translateZ5.o" "|Stand_Idle_3|Bip01_Spine.tz";
connectAttr "Bip01_Spine_rotateX5.o" "|Stand_Idle_3|Bip01_Spine.rx";
connectAttr "Bip01_Spine_rotateY5.o" "|Stand_Idle_3|Bip01_Spine.ry";
connectAttr "Bip01_Spine_rotateZ5.o" "|Stand_Idle_3|Bip01_Spine.rz";
connectAttr "Bip01_Spine1_rotateX5.o" "|Stand_Idle_3|Bip01_Spine|Bip01_Spine1.rx"
		;
connectAttr "Bip01_Spine1_rotateY5.o" "|Stand_Idle_3|Bip01_Spine|Bip01_Spine1.ry"
		;
connectAttr "Bip01_Spine1_rotateZ5.o" "|Stand_Idle_3|Bip01_Spine|Bip01_Spine1.rz"
		;
connectAttr "|Stand_Idle_3|Bip01_Spine.s" "|Stand_Idle_3|Bip01_Spine|Bip01_Spine1.is"
		;
connectAttr "Bip01_Spine2_rotateX5.o" "|Stand_Idle_3|Bip01_Spine|Bip01_Spine1|Bip01_Spine2.rx"
		;
connectAttr "Bip01_Spine2_rotateY5.o" "|Stand_Idle_3|Bip01_Spine|Bip01_Spine1|Bip01_Spine2.ry"
		;
connectAttr "Bip01_Spine2_rotateZ5.o" "|Stand_Idle_3|Bip01_Spine|Bip01_Spine1|Bip01_Spine2.rz"
		;
connectAttr "|Stand_Idle_3|Bip01_Spine|Bip01_Spine1.s" "|Stand_Idle_3|Bip01_Spine|Bip01_Spine1|Bip01_Spine2.is"
		;
connectAttr "|Stand_Idle_3|Bip01_Spine|Bip01_Spine1|Bip01_Spine2.s" "|Stand_Idle_3|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck.is"
		;
connectAttr "|Stand_Idle_3|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck.s" "|Stand_Idle_3|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_Head.is"
		;
connectAttr "|Stand_Idle_3|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_Head.s" "|Stand_Idle_3|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_Head|_notused_Bip01_HeadNub.is"
		;
connectAttr "Bip01_R_Clavicle_rotateX5.o" "|Stand_Idle_3|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_R_Clavicle.rx"
		;
connectAttr "Bip01_R_Clavicle_rotateY5.o" "|Stand_Idle_3|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_R_Clavicle.ry"
		;
connectAttr "Bip01_R_Clavicle_rotateZ5.o" "|Stand_Idle_3|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_R_Clavicle.rz"
		;
connectAttr "|Stand_Idle_3|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck.s" "|Stand_Idle_3|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_R_Clavicle.is"
		;
connectAttr "Bip01_R_UpperArm_rotateX5.o" "|Stand_Idle_3|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_R_Clavicle|Bip01_R_UpperArm.rx"
		;
connectAttr "Bip01_R_UpperArm_rotateY5.o" "|Stand_Idle_3|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_R_Clavicle|Bip01_R_UpperArm.ry"
		;
connectAttr "Bip01_R_UpperArm_rotateZ5.o" "|Stand_Idle_3|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_R_Clavicle|Bip01_R_UpperArm.rz"
		;
connectAttr "|Stand_Idle_3|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_R_Clavicle.s" "|Stand_Idle_3|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_R_Clavicle|Bip01_R_UpperArm.is"
		;
connectAttr "Bip01_R_Forearm_rotateX5.o" "|Stand_Idle_3|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_R_Clavicle|Bip01_R_UpperArm|Bip01_R_Forearm.rx"
		;
connectAttr "Bip01_R_Forearm_rotateY5.o" "|Stand_Idle_3|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_R_Clavicle|Bip01_R_UpperArm|Bip01_R_Forearm.ry"
		;
connectAttr "Bip01_R_Forearm_rotateZ5.o" "|Stand_Idle_3|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_R_Clavicle|Bip01_R_UpperArm|Bip01_R_Forearm.rz"
		;
connectAttr "|Stand_Idle_3|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_R_Clavicle|Bip01_R_UpperArm.s" "|Stand_Idle_3|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_R_Clavicle|Bip01_R_UpperArm|Bip01_R_Forearm.is"
		;
connectAttr "|Stand_Idle_3|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_R_Clavicle|Bip01_R_UpperArm|Bip01_R_Forearm.s" "|Stand_Idle_3|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_R_Clavicle|Bip01_R_UpperArm|Bip01_R_Forearm|Bip01_R_Hand.is"
		;
connectAttr "|Stand_Idle_3|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_R_Clavicle|Bip01_R_UpperArm|Bip01_R_Forearm|Bip01_R_Hand.s" "|Stand_Idle_3|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_R_Clavicle|Bip01_R_UpperArm|Bip01_R_Forearm|Bip01_R_Hand|Bip01_R_Finger0.is"
		;
connectAttr "|Stand_Idle_3|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_R_Clavicle|Bip01_R_UpperArm|Bip01_R_Forearm|Bip01_R_Hand|Bip01_R_Finger0.s" "|Stand_Idle_3|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_R_Clavicle|Bip01_R_UpperArm|Bip01_R_Forearm|Bip01_R_Hand|Bip01_R_Finger0|Bip01_R_Finger01.is"
		;
connectAttr "|Stand_Idle_3|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_R_Clavicle|Bip01_R_UpperArm|Bip01_R_Forearm|Bip01_R_Hand|Bip01_R_Finger0|Bip01_R_Finger01.s" "|Stand_Idle_3|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_R_Clavicle|Bip01_R_UpperArm|Bip01_R_Forearm|Bip01_R_Hand|Bip01_R_Finger0|Bip01_R_Finger01|_notused_Bip01_R_Finger0Nub.is"
		;
connectAttr "|Stand_Idle_3|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_R_Clavicle|Bip01_R_UpperArm|Bip01_R_Forearm|Bip01_R_Hand.s" "|Stand_Idle_3|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_R_Clavicle|Bip01_R_UpperArm|Bip01_R_Forearm|Bip01_R_Hand|Bip01_R_Finger2.is"
		;
connectAttr "|Stand_Idle_3|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_R_Clavicle|Bip01_R_UpperArm|Bip01_R_Forearm|Bip01_R_Hand|Bip01_R_Finger2.s" "|Stand_Idle_3|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_R_Clavicle|Bip01_R_UpperArm|Bip01_R_Forearm|Bip01_R_Hand|Bip01_R_Finger2|Bip01_R_Finger21.is"
		;
connectAttr "|Stand_Idle_3|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_R_Clavicle|Bip01_R_UpperArm|Bip01_R_Forearm|Bip01_R_Hand|Bip01_R_Finger2|Bip01_R_Finger21.s" "|Stand_Idle_3|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_R_Clavicle|Bip01_R_UpperArm|Bip01_R_Forearm|Bip01_R_Hand|Bip01_R_Finger2|Bip01_R_Finger21|_notused_Bip01_R_Finger2Nub.is"
		;
connectAttr "|Stand_Idle_3|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_R_Clavicle|Bip01_R_UpperArm|Bip01_R_Forearm|Bip01_R_Hand.s" "|Stand_Idle_3|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_R_Clavicle|Bip01_R_UpperArm|Bip01_R_Forearm|Bip01_R_Hand|Bip01_R_Finger1.is"
		;
connectAttr "|Stand_Idle_3|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_R_Clavicle|Bip01_R_UpperArm|Bip01_R_Forearm|Bip01_R_Hand|Bip01_R_Finger1.s" "|Stand_Idle_3|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_R_Clavicle|Bip01_R_UpperArm|Bip01_R_Forearm|Bip01_R_Hand|Bip01_R_Finger1|Bip01_R_Finger11.is"
		;
connectAttr "|Stand_Idle_3|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_R_Clavicle|Bip01_R_UpperArm|Bip01_R_Forearm|Bip01_R_Hand|Bip01_R_Finger1|Bip01_R_Finger11.s" "|Stand_Idle_3|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_R_Clavicle|Bip01_R_UpperArm|Bip01_R_Forearm|Bip01_R_Hand|Bip01_R_Finger1|Bip01_R_Finger11|_notused_Bip01_R_Finger1Nub.is"
		;
connectAttr "|Stand_Idle_3|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_R_Clavicle|Bip01_R_UpperArm|Bip01_R_Forearm|Bip01_R_Hand.s" "|Stand_Idle_3|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_R_Clavicle|Bip01_R_UpperArm|Bip01_R_Forearm|Bip01_R_Hand|Bip01_R_Finger3.is"
		;
connectAttr "|Stand_Idle_3|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_R_Clavicle|Bip01_R_UpperArm|Bip01_R_Forearm|Bip01_R_Hand|Bip01_R_Finger3.s" "|Stand_Idle_3|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_R_Clavicle|Bip01_R_UpperArm|Bip01_R_Forearm|Bip01_R_Hand|Bip01_R_Finger3|Bip01_R_Finger31.is"
		;
connectAttr "|Stand_Idle_3|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_R_Clavicle|Bip01_R_UpperArm|Bip01_R_Forearm|Bip01_R_Hand|Bip01_R_Finger3|Bip01_R_Finger31.s" "|Stand_Idle_3|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_R_Clavicle|Bip01_R_UpperArm|Bip01_R_Forearm|Bip01_R_Hand|Bip01_R_Finger3|Bip01_R_Finger31|_notused_Bip01_R_Finger3Nub.is"
		;
connectAttr "|Stand_Idle_3|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_R_Clavicle|Bip01_R_UpperArm|Bip01_R_Forearm|Bip01_R_Hand.s" "|Stand_Idle_3|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_R_Clavicle|Bip01_R_UpperArm|Bip01_R_Forearm|Bip01_R_Hand|Bip01_R_Finger4.is"
		;
connectAttr "|Stand_Idle_3|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_R_Clavicle|Bip01_R_UpperArm|Bip01_R_Forearm|Bip01_R_Hand|Bip01_R_Finger4.s" "|Stand_Idle_3|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_R_Clavicle|Bip01_R_UpperArm|Bip01_R_Forearm|Bip01_R_Hand|Bip01_R_Finger4|Bip01_R_Finger41.is"
		;
connectAttr "|Stand_Idle_3|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_R_Clavicle|Bip01_R_UpperArm|Bip01_R_Forearm|Bip01_R_Hand|Bip01_R_Finger4|Bip01_R_Finger41.s" "|Stand_Idle_3|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_R_Clavicle|Bip01_R_UpperArm|Bip01_R_Forearm|Bip01_R_Hand|Bip01_R_Finger4|Bip01_R_Finger41|_notused_Bip01_R_Finger4Nub.is"
		;
connectAttr "Bip01_L_Clavicle_rotateX5.o" "|Stand_Idle_3|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_L_Clavicle.rx"
		;
connectAttr "Bip01_L_Clavicle_rotateY5.o" "|Stand_Idle_3|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_L_Clavicle.ry"
		;
connectAttr "Bip01_L_Clavicle_rotateZ5.o" "|Stand_Idle_3|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_L_Clavicle.rz"
		;
connectAttr "|Stand_Idle_3|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck.s" "|Stand_Idle_3|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_L_Clavicle.is"
		;
connectAttr "Bip01_L_UpperArm_rotateX5.o" "|Stand_Idle_3|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_L_Clavicle|Bip01_L_UpperArm.rx"
		;
connectAttr "Bip01_L_UpperArm_rotateY5.o" "|Stand_Idle_3|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_L_Clavicle|Bip01_L_UpperArm.ry"
		;
connectAttr "Bip01_L_UpperArm_rotateZ5.o" "|Stand_Idle_3|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_L_Clavicle|Bip01_L_UpperArm.rz"
		;
connectAttr "|Stand_Idle_3|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_L_Clavicle.s" "|Stand_Idle_3|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_L_Clavicle|Bip01_L_UpperArm.is"
		;
connectAttr "Bip01_L_Forearm_rotateX5.o" "|Stand_Idle_3|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_L_Clavicle|Bip01_L_UpperArm|Bip01_L_Forearm.rx"
		;
connectAttr "Bip01_L_Forearm_rotateY5.o" "|Stand_Idle_3|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_L_Clavicle|Bip01_L_UpperArm|Bip01_L_Forearm.ry"
		;
connectAttr "Bip01_L_Forearm_rotateZ5.o" "|Stand_Idle_3|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_L_Clavicle|Bip01_L_UpperArm|Bip01_L_Forearm.rz"
		;
connectAttr "|Stand_Idle_3|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_L_Clavicle|Bip01_L_UpperArm.s" "|Stand_Idle_3|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_L_Clavicle|Bip01_L_UpperArm|Bip01_L_Forearm.is"
		;
connectAttr "|Stand_Idle_3|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_L_Clavicle|Bip01_L_UpperArm|Bip01_L_Forearm.s" "|Stand_Idle_3|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_L_Clavicle|Bip01_L_UpperArm|Bip01_L_Forearm|Bip01_L_Hand.is"
		;
connectAttr "|Stand_Idle_3|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_L_Clavicle|Bip01_L_UpperArm|Bip01_L_Forearm|Bip01_L_Hand.s" "|Stand_Idle_3|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_L_Clavicle|Bip01_L_UpperArm|Bip01_L_Forearm|Bip01_L_Hand|Bip01_L_Finger0.is"
		;
connectAttr "|Stand_Idle_3|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_L_Clavicle|Bip01_L_UpperArm|Bip01_L_Forearm|Bip01_L_Hand|Bip01_L_Finger0.s" "|Stand_Idle_3|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_L_Clavicle|Bip01_L_UpperArm|Bip01_L_Forearm|Bip01_L_Hand|Bip01_L_Finger0|Bip01_L_Finger01.is"
		;
connectAttr "|Stand_Idle_3|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_L_Clavicle|Bip01_L_UpperArm|Bip01_L_Forearm|Bip01_L_Hand|Bip01_L_Finger0|Bip01_L_Finger01.s" "|Stand_Idle_3|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_L_Clavicle|Bip01_L_UpperArm|Bip01_L_Forearm|Bip01_L_Hand|Bip01_L_Finger0|Bip01_L_Finger01|_notused_Bip01_L_Finger0Nub.is"
		;
connectAttr "|Stand_Idle_3|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_L_Clavicle|Bip01_L_UpperArm|Bip01_L_Forearm|Bip01_L_Hand.s" "|Stand_Idle_3|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_L_Clavicle|Bip01_L_UpperArm|Bip01_L_Forearm|Bip01_L_Hand|Bip01_L_Finger2.is"
		;
connectAttr "|Stand_Idle_3|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_L_Clavicle|Bip01_L_UpperArm|Bip01_L_Forearm|Bip01_L_Hand|Bip01_L_Finger2.s" "|Stand_Idle_3|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_L_Clavicle|Bip01_L_UpperArm|Bip01_L_Forearm|Bip01_L_Hand|Bip01_L_Finger2|Bip01_L_Finger21.is"
		;
connectAttr "|Stand_Idle_3|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_L_Clavicle|Bip01_L_UpperArm|Bip01_L_Forearm|Bip01_L_Hand|Bip01_L_Finger2|Bip01_L_Finger21.s" "|Stand_Idle_3|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_L_Clavicle|Bip01_L_UpperArm|Bip01_L_Forearm|Bip01_L_Hand|Bip01_L_Finger2|Bip01_L_Finger21|_notused_Bip01_L_Finger2Nub.is"
		;
connectAttr "|Stand_Idle_3|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_L_Clavicle|Bip01_L_UpperArm|Bip01_L_Forearm|Bip01_L_Hand.s" "|Stand_Idle_3|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_L_Clavicle|Bip01_L_UpperArm|Bip01_L_Forearm|Bip01_L_Hand|Bip01_L_Finger1.is"
		;
connectAttr "|Stand_Idle_3|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_L_Clavicle|Bip01_L_UpperArm|Bip01_L_Forearm|Bip01_L_Hand|Bip01_L_Finger1.s" "|Stand_Idle_3|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_L_Clavicle|Bip01_L_UpperArm|Bip01_L_Forearm|Bip01_L_Hand|Bip01_L_Finger1|Bip01_L_Finger11.is"
		;
connectAttr "|Stand_Idle_3|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_L_Clavicle|Bip01_L_UpperArm|Bip01_L_Forearm|Bip01_L_Hand|Bip01_L_Finger1|Bip01_L_Finger11.s" "|Stand_Idle_3|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_L_Clavicle|Bip01_L_UpperArm|Bip01_L_Forearm|Bip01_L_Hand|Bip01_L_Finger1|Bip01_L_Finger11|_notused_Bip01_L_Finger1Nub.is"
		;
connectAttr "|Stand_Idle_3|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_L_Clavicle|Bip01_L_UpperArm|Bip01_L_Forearm|Bip01_L_Hand.s" "|Stand_Idle_3|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_L_Clavicle|Bip01_L_UpperArm|Bip01_L_Forearm|Bip01_L_Hand|Bip01_L_Finger3.is"
		;
connectAttr "|Stand_Idle_3|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_L_Clavicle|Bip01_L_UpperArm|Bip01_L_Forearm|Bip01_L_Hand|Bip01_L_Finger3.s" "|Stand_Idle_3|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_L_Clavicle|Bip01_L_UpperArm|Bip01_L_Forearm|Bip01_L_Hand|Bip01_L_Finger3|Bip01_L_Finger31.is"
		;
connectAttr "|Stand_Idle_3|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_L_Clavicle|Bip01_L_UpperArm|Bip01_L_Forearm|Bip01_L_Hand|Bip01_L_Finger3|Bip01_L_Finger31.s" "|Stand_Idle_3|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_L_Clavicle|Bip01_L_UpperArm|Bip01_L_Forearm|Bip01_L_Hand|Bip01_L_Finger3|Bip01_L_Finger31|_notused_Bip01_L_Finger3Nub.is"
		;
connectAttr "|Stand_Idle_3|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_L_Clavicle|Bip01_L_UpperArm|Bip01_L_Forearm|Bip01_L_Hand.s" "|Stand_Idle_3|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_L_Clavicle|Bip01_L_UpperArm|Bip01_L_Forearm|Bip01_L_Hand|Bip01_L_Finger4.is"
		;
connectAttr "|Stand_Idle_3|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_L_Clavicle|Bip01_L_UpperArm|Bip01_L_Forearm|Bip01_L_Hand|Bip01_L_Finger4.s" "|Stand_Idle_3|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_L_Clavicle|Bip01_L_UpperArm|Bip01_L_Forearm|Bip01_L_Hand|Bip01_L_Finger4|Bip01_L_Finger41.is"
		;
connectAttr "|Stand_Idle_3|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_L_Clavicle|Bip01_L_UpperArm|Bip01_L_Forearm|Bip01_L_Hand|Bip01_L_Finger4|Bip01_L_Finger41.s" "|Stand_Idle_3|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_L_Clavicle|Bip01_L_UpperArm|Bip01_L_Forearm|Bip01_L_Hand|Bip01_L_Finger4|Bip01_L_Finger41|_notused_Bip01_L_Finger4Nub.is"
		;
connectAttr "Bip01_L_Thigh_rotateX5.o" "|Stand_Idle_3|Bip01_Spine|Bip01_L_Thigh.rx"
		;
connectAttr "Bip01_L_Thigh_rotateY5.o" "|Stand_Idle_3|Bip01_Spine|Bip01_L_Thigh.ry"
		;
connectAttr "Bip01_L_Thigh_rotateZ5.o" "|Stand_Idle_3|Bip01_Spine|Bip01_L_Thigh.rz"
		;
connectAttr "|Stand_Idle_3|Bip01_Spine.s" "|Stand_Idle_3|Bip01_Spine|Bip01_L_Thigh.is"
		;
connectAttr "Bip01_L_Calf_rotateX5.o" "|Stand_Idle_3|Bip01_Spine|Bip01_L_Thigh|Bip01_L_Calf.rx"
		;
connectAttr "Bip01_L_Calf_rotateY5.o" "|Stand_Idle_3|Bip01_Spine|Bip01_L_Thigh|Bip01_L_Calf.ry"
		;
connectAttr "Bip01_L_Calf_rotateZ5.o" "|Stand_Idle_3|Bip01_Spine|Bip01_L_Thigh|Bip01_L_Calf.rz"
		;
connectAttr "|Stand_Idle_3|Bip01_Spine|Bip01_L_Thigh.s" "|Stand_Idle_3|Bip01_Spine|Bip01_L_Thigh|Bip01_L_Calf.is"
		;
connectAttr "Bip01_L_Foot_rotateY5.o" "|Stand_Idle_3|Bip01_Spine|Bip01_L_Thigh|Bip01_L_Calf|Bip01_L_Foot.ry"
		;
connectAttr "Bip01_L_Foot_rotateZ5.o" "|Stand_Idle_3|Bip01_Spine|Bip01_L_Thigh|Bip01_L_Calf|Bip01_L_Foot.rz"
		;
connectAttr "Bip01_L_Foot_rotateX5.o" "|Stand_Idle_3|Bip01_Spine|Bip01_L_Thigh|Bip01_L_Calf|Bip01_L_Foot.rx"
		;
connectAttr "|Stand_Idle_3|Bip01_Spine|Bip01_L_Thigh|Bip01_L_Calf.s" "|Stand_Idle_3|Bip01_Spine|Bip01_L_Thigh|Bip01_L_Calf|Bip01_L_Foot.is"
		;
connectAttr "|Stand_Idle_3|Bip01_Spine|Bip01_L_Thigh|Bip01_L_Calf|Bip01_L_Foot.s" "|Stand_Idle_3|Bip01_Spine|Bip01_L_Thigh|Bip01_L_Calf|Bip01_L_Foot|Bip01_L_Toe0.is"
		;
connectAttr "|Stand_Idle_3|Bip01_Spine|Bip01_L_Thigh|Bip01_L_Calf|Bip01_L_Foot|Bip01_L_Toe0.s" "|Stand_Idle_3|Bip01_Spine|Bip01_L_Thigh|Bip01_L_Calf|Bip01_L_Foot|Bip01_L_Toe0|_notused_Bip01_L_Toe0Nub.is"
		;
connectAttr "Bip01_R_Thigh_rotateX5.o" "|Stand_Idle_3|Bip01_Spine|Bip01_R_Thigh.rx"
		;
connectAttr "Bip01_R_Thigh_rotateZ5.o" "|Stand_Idle_3|Bip01_Spine|Bip01_R_Thigh.rz"
		;
connectAttr "Bip01_R_Thigh_rotateY5.o" "|Stand_Idle_3|Bip01_Spine|Bip01_R_Thigh.ry"
		;
connectAttr "|Stand_Idle_3|Bip01_Spine.s" "|Stand_Idle_3|Bip01_Spine|Bip01_R_Thigh.is"
		;
connectAttr "Bip01_R_Calf_rotateX5.o" "|Stand_Idle_3|Bip01_Spine|Bip01_R_Thigh|Bip01_R_Calf.rx"
		;
connectAttr "Bip01_R_Calf_rotateZ5.o" "|Stand_Idle_3|Bip01_Spine|Bip01_R_Thigh|Bip01_R_Calf.rz"
		;
connectAttr "Bip01_R_Calf_rotateY5.o" "|Stand_Idle_3|Bip01_Spine|Bip01_R_Thigh|Bip01_R_Calf.ry"
		;
connectAttr "|Stand_Idle_3|Bip01_Spine|Bip01_R_Thigh.s" "|Stand_Idle_3|Bip01_Spine|Bip01_R_Thigh|Bip01_R_Calf.is"
		;
connectAttr "Bip01_R_Foot_rotateX5.o" "|Stand_Idle_3|Bip01_Spine|Bip01_R_Thigh|Bip01_R_Calf|Bip01_R_Foot.rx"
		;
connectAttr "Bip01_R_Foot_rotateY5.o" "|Stand_Idle_3|Bip01_Spine|Bip01_R_Thigh|Bip01_R_Calf|Bip01_R_Foot.ry"
		;
connectAttr "Bip01_R_Foot_rotateZ5.o" "|Stand_Idle_3|Bip01_Spine|Bip01_R_Thigh|Bip01_R_Calf|Bip01_R_Foot.rz"
		;
connectAttr "|Stand_Idle_3|Bip01_Spine|Bip01_R_Thigh|Bip01_R_Calf.s" "|Stand_Idle_3|Bip01_Spine|Bip01_R_Thigh|Bip01_R_Calf|Bip01_R_Foot.is"
		;
connectAttr "|Stand_Idle_3|Bip01_Spine|Bip01_R_Thigh|Bip01_R_Calf|Bip01_R_Foot.s" "|Stand_Idle_3|Bip01_Spine|Bip01_R_Thigh|Bip01_R_Calf|Bip01_R_Foot|Bip01_R_Toe0.is"
		;
connectAttr "|Stand_Idle_3|Bip01_Spine|Bip01_R_Thigh|Bip01_R_Calf|Bip01_R_Foot|Bip01_R_Toe0.s" "|Stand_Idle_3|Bip01_Spine|Bip01_R_Thigh|Bip01_R_Calf|Bip01_R_Foot|Bip01_R_Toe0|_notused_Bip01_R_Toe0Nub.is"
		;
// End of Animation v5 - Stand Idle 3.ma
