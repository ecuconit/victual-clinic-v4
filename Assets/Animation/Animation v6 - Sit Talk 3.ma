//Maya ASCII 2014 scene
//Name: Animation v6 - Sit Talk 3.ma
//Last modified: Fri, Apr 17, 2015 12:52:25 PM
//Codeset: 1252
requires maya "2014";
requires -nodeType "mentalrayFramebuffer" -nodeType "mentalrayOutputPass" -nodeType "mentalrayRenderPass"
		 -nodeType "mentalrayUserBuffer" -nodeType "mentalraySubdivApprox" -nodeType "mentalrayCurveApprox"
		 -nodeType "mentalraySurfaceApprox" -nodeType "mentalrayDisplaceApprox" -nodeType "mentalrayOptions"
		 -nodeType "mentalrayGlobals" -nodeType "mentalrayItemsList" -nodeType "mentalrayShader"
		 -nodeType "mentalrayUserData" -nodeType "mentalrayText" -nodeType "mentalrayTessellation"
		 -nodeType "mentalrayPhenomenon" -nodeType "mentalrayLightProfile" -nodeType "mentalrayVertexColors"
		 -nodeType "mentalrayIblShape" -nodeType "mapVizShape" -nodeType "mentalrayCCMeshProxy"
		 -nodeType "cylindricalLightLocator" -nodeType "discLightLocator" -nodeType "rectangularLightLocator"
		 -nodeType "sphericalLightLocator" -nodeType "abcimport" -nodeType "mia_physicalsun"
		 -nodeType "mia_physicalsky" -nodeType "mia_material" -nodeType "mia_material_x" -nodeType "mia_roundcorners"
		 -nodeType "mia_exposure_simple" -nodeType "mia_portal_light" -nodeType "mia_light_surface"
		 -nodeType "mia_exposure_photographic" -nodeType "mia_exposure_photographic_rev" -nodeType "mia_lens_bokeh"
		 -nodeType "mia_envblur" -nodeType "mia_ciesky" -nodeType "mia_photometric_light"
		 -nodeType "mib_texture_vector" -nodeType "mib_texture_remap" -nodeType "mib_texture_rotate"
		 -nodeType "mib_bump_basis" -nodeType "mib_bump_map" -nodeType "mib_passthrough_bump_map"
		 -nodeType "mib_bump_map2" -nodeType "mib_lookup_spherical" -nodeType "mib_lookup_cube1"
		 -nodeType "mib_lookup_cube6" -nodeType "mib_lookup_background" -nodeType "mib_lookup_cylindrical"
		 -nodeType "mib_texture_lookup" -nodeType "mib_texture_lookup2" -nodeType "mib_texture_filter_lookup"
		 -nodeType "mib_texture_checkerboard" -nodeType "mib_texture_polkadot" -nodeType "mib_texture_polkasphere"
		 -nodeType "mib_texture_turbulence" -nodeType "mib_texture_wave" -nodeType "mib_reflect"
		 -nodeType "mib_refract" -nodeType "mib_transparency" -nodeType "mib_continue" -nodeType "mib_opacity"
		 -nodeType "mib_twosided" -nodeType "mib_refraction_index" -nodeType "mib_dielectric"
		 -nodeType "mib_ray_marcher" -nodeType "mib_illum_lambert" -nodeType "mib_illum_phong"
		 -nodeType "mib_illum_ward" -nodeType "mib_illum_ward_deriv" -nodeType "mib_illum_blinn"
		 -nodeType "mib_illum_cooktorr" -nodeType "mib_illum_hair" -nodeType "mib_volume"
		 -nodeType "mib_color_alpha" -nodeType "mib_color_average" -nodeType "mib_color_intensity"
		 -nodeType "mib_color_interpolate" -nodeType "mib_color_mix" -nodeType "mib_color_spread"
		 -nodeType "mib_geo_cube" -nodeType "mib_geo_torus" -nodeType "mib_geo_sphere" -nodeType "mib_geo_cone"
		 -nodeType "mib_geo_cylinder" -nodeType "mib_geo_square" -nodeType "mib_geo_instance"
		 -nodeType "mib_geo_instance_mlist" -nodeType "mib_geo_add_uv_texsurf" -nodeType "mib_photon_basic"
		 -nodeType "mib_light_infinite" -nodeType "mib_light_point" -nodeType "mib_light_spot"
		 -nodeType "mib_light_photometric" -nodeType "mib_cie_d" -nodeType "mib_blackbody"
		 -nodeType "mib_shadow_transparency" -nodeType "mib_lens_stencil" -nodeType "mib_lens_clamp"
		 -nodeType "mib_lightmap_write" -nodeType "mib_lightmap_sample" -nodeType "mib_amb_occlusion"
		 -nodeType "mib_fast_occlusion" -nodeType "mib_map_get_scalar" -nodeType "mib_map_get_integer"
		 -nodeType "mib_map_get_vector" -nodeType "mib_map_get_color" -nodeType "mib_map_get_transform"
		 -nodeType "mib_map_get_scalar_array" -nodeType "mib_map_get_integer_array" -nodeType "mib_fg_occlusion"
		 -nodeType "mib_bent_normal_env" -nodeType "mib_glossy_reflection" -nodeType "mib_glossy_refraction"
		 -nodeType "builtin_bsdf_architectural" -nodeType "builtin_bsdf_architectural_comp"
		 -nodeType "builtin_bsdf_carpaint" -nodeType "builtin_bsdf_ashikhmin" -nodeType "builtin_bsdf_lambert"
		 -nodeType "builtin_bsdf_mirror" -nodeType "builtin_bsdf_phong" -nodeType "contour_store_function"
		 -nodeType "contour_store_function_simple" -nodeType "contour_contrast_function_levels"
		 -nodeType "contour_contrast_function_simple" -nodeType "contour_shader_simple" -nodeType "contour_shader_silhouette"
		 -nodeType "contour_shader_maxcolor" -nodeType "contour_shader_curvature" -nodeType "contour_shader_factorcolor"
		 -nodeType "contour_shader_depthfade" -nodeType "contour_shader_framefade" -nodeType "contour_shader_layerthinner"
		 -nodeType "contour_shader_widthfromcolor" -nodeType "contour_shader_widthfromlightdir"
		 -nodeType "contour_shader_widthfromlight" -nodeType "contour_shader_combi" -nodeType "contour_only"
		 -nodeType "contour_composite" -nodeType "contour_ps" -nodeType "mi_metallic_paint"
		 -nodeType "mi_metallic_paint_x" -nodeType "mi_bump_flakes" -nodeType "mi_car_paint_phen"
		 -nodeType "mi_metallic_paint_output_mixer" -nodeType "mi_car_paint_phen_x" -nodeType "physical_lens_dof"
		 -nodeType "physical_light" -nodeType "dgs_material" -nodeType "dgs_material_photon"
		 -nodeType "dielectric_material" -nodeType "dielectric_material_photon" -nodeType "oversampling_lens"
		 -nodeType "path_material" -nodeType "parti_volume" -nodeType "parti_volume_photon"
		 -nodeType "transmat" -nodeType "transmat_photon" -nodeType "mip_rayswitch" -nodeType "mip_rayswitch_advanced"
		 -nodeType "mip_rayswitch_environment" -nodeType "mip_card_opacity" -nodeType "mip_motionblur"
		 -nodeType "mip_motion_vector" -nodeType "mip_matteshadow" -nodeType "mip_cameramap"
		 -nodeType "mip_mirrorball" -nodeType "mip_grayball" -nodeType "mip_gamma_gain" -nodeType "mip_render_subset"
		 -nodeType "mip_matteshadow_mtl" -nodeType "mip_binaryproxy" -nodeType "mip_rayswitch_stage"
		 -nodeType "mip_fgshooter" -nodeType "mib_ptex_lookup" -nodeType "misss_physical"
		 -nodeType "misss_physical_phen" -nodeType "misss_fast_shader" -nodeType "misss_fast_shader_x"
		 -nodeType "misss_fast_shader2" -nodeType "misss_fast_shader2_x" -nodeType "misss_skin_specular"
		 -nodeType "misss_lightmap_write" -nodeType "misss_lambert_gamma" -nodeType "misss_call_shader"
		 -nodeType "misss_set_normal" -nodeType "misss_fast_lmap_maya" -nodeType "misss_fast_simple_maya"
		 -nodeType "misss_fast_skin_maya" -nodeType "misss_fast_skin_phen" -nodeType "misss_fast_skin_phen_d"
		 -nodeType "misss_mia_skin2_phen" -nodeType "misss_mia_skin2_phen_d" -nodeType "misss_lightmap_phen"
		 -nodeType "misss_mia_skin2_surface_phen" -nodeType "surfaceSampler" -nodeType "mib_data_bool"
		 -nodeType "mib_data_int" -nodeType "mib_data_scalar" -nodeType "mib_data_vector"
		 -nodeType "mib_data_color" -nodeType "mib_data_string" -nodeType "mib_data_texture"
		 -nodeType "mib_data_shader" -nodeType "mib_data_bool_array" -nodeType "mib_data_int_array"
		 -nodeType "mib_data_scalar_array" -nodeType "mib_data_vector_array" -nodeType "mib_data_color_array"
		 -nodeType "mib_data_string_array" -nodeType "mib_data_texture_array" -nodeType "mib_data_shader_array"
		 -nodeType "mib_data_get_bool" -nodeType "mib_data_get_int" -nodeType "mib_data_get_scalar"
		 -nodeType "mib_data_get_vector" -nodeType "mib_data_get_color" -nodeType "mib_data_get_string"
		 -nodeType "mib_data_get_texture" -nodeType "mib_data_get_shader" -nodeType "mib_data_get_shader_bool"
		 -nodeType "mib_data_get_shader_int" -nodeType "mib_data_get_shader_scalar" -nodeType "mib_data_get_shader_vector"
		 -nodeType "mib_data_get_shader_color" -nodeType "user_ibl_env" -nodeType "user_ibl_rect"
		 -nodeType "mia_material_x_passes" -nodeType "mi_metallic_paint_x_passes" -nodeType "mi_car_paint_phen_x_passes"
		 -nodeType "misss_fast_shader_x_passes" -dataType "byteArray" "Mayatomr" "2014.0 - 3.11.1.4 ";
currentUnit -l centimeter -a degree -t film;
fileInfo "application" "maya";
fileInfo "product" "Maya 2014";
fileInfo "version" "2014 x64";
fileInfo "cutIdentifier" "201303010241-864206";
fileInfo "osv" "Microsoft Windows 7 Enterprise Edition, 64-bit Windows 7 Service Pack 1 (Build 7601)\n";
fileInfo "license" "education";
createNode transform -s -n "persp";
	setAttr ".v" no;
	setAttr ".t" -type "double3" -2.4476375745599324 107.06590113908216 256.24408400513551 ;
	setAttr ".r" -type "double3" -9.938352729617602 -2.5999999999930528 2.4873689175987728e-016 ;
createNode camera -s -n "perspShape" -p "persp";
	setAttr -k off ".v" no;
	setAttr ".fl" 34.999999999999986;
	setAttr ".coi" 249.78317031800549;
	setAttr ".imn" -type "string" "persp";
	setAttr ".den" -type "string" "persp_depth";
	setAttr ".man" -type "string" "persp_mask";
	setAttr ".hc" -type "string" "viewSet -p %camera";
createNode transform -s -n "top";
	setAttr ".v" no;
	setAttr ".t" -type "double3" 0 100.1 0 ;
	setAttr ".r" -type "double3" -89.999999999999986 0 0 ;
createNode camera -s -n "topShape" -p "top";
	setAttr -k off ".v" no;
	setAttr ".rnd" no;
	setAttr ".coi" 100.1;
	setAttr ".ow" 30;
	setAttr ".imn" -type "string" "top";
	setAttr ".den" -type "string" "top_depth";
	setAttr ".man" -type "string" "top_mask";
	setAttr ".hc" -type "string" "viewSet -t %camera";
	setAttr ".o" yes;
createNode transform -s -n "front";
	setAttr ".v" no;
	setAttr ".t" -type "double3" 0 0 100.1 ;
createNode camera -s -n "frontShape" -p "front";
	setAttr -k off ".v" no;
	setAttr ".rnd" no;
	setAttr ".coi" 100.1;
	setAttr ".ow" 30;
	setAttr ".imn" -type "string" "front";
	setAttr ".den" -type "string" "front_depth";
	setAttr ".man" -type "string" "front_mask";
	setAttr ".hc" -type "string" "viewSet -f %camera";
	setAttr ".o" yes;
createNode transform -s -n "side";
	setAttr ".v" no;
	setAttr ".t" -type "double3" 100.1 0 0 ;
	setAttr ".r" -type "double3" 0 89.999999999999986 0 ;
createNode camera -s -n "sideShape" -p "side";
	setAttr -k off ".v" no;
	setAttr ".rnd" no;
	setAttr ".coi" 100.1;
	setAttr ".ow" 30;
	setAttr ".imn" -type "string" "side";
	setAttr ".den" -type "string" "side_depth";
	setAttr ".man" -type "string" "side_mask";
	setAttr ".hc" -type "string" "viewSet -s %camera";
	setAttr ".o" yes;
createNode transform -n "Sit_Talk_3";
createNode joint -n "Bip01_Spine" -p "Sit_Talk_3";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".s" -type "double3" 0.99999999999999956 0.99999999999999978 0.99999999999999978 ;
	setAttr ".jo" -type "double3" 90.000171 0 89.999999999999986 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" -0.0081722831436215043 0.99872757760529951 0.04976383746125676 0
		 -0.031468606931617168 -0.049997711134416305 0.99825345261566845 0 0.99947133053730142 0.006592011223395697 0.031837161023350762 0
		 -6.7139604917915256e-006 99.443700000000007 -2.2495999999899592 1;
	setAttr -k on ".liw";
	setAttr ".fbxID" 5;
createNode joint -n "Bip01_Spine1" -p "Bip01_Spine";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".t" -type "double3" 13.398399999999999 -0.0107 0 ;
	setAttr ".s" -type "double3" 1 1 0.99999999999999978 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" -0.0087295896663812361 0.015254963731834248 0.99984552824213657 0
		 -0.0050586545493994011 -0.99987150655113555 0.015211193290218554 0 0.99994910084384103 -0.0049250856543791108 0.0088056375629807955 0
		 9.9999999999999995e-007 2.2603360000000001 112.84212599999999 1;
	setAttr -k on ".liw";
	setAttr ".fbxID" 5;
createNode joint -n "Bip01_Spine2" -p "Bip01_Spine1";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".t" -type "double3" 13.398299999999999 -0.0107 0 ;
	setAttr ".s" -type "double3" 1 0.99999999999999978 0.99999999999999978 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" 0 0 1 0 0 -1 0 0 1 0 0 0 -3.9999999999999998e-006 -9.6239790000000003 -19.074804 1;
	setAttr -k on ".liw";
	setAttr ".fbxID" 5;
createNode joint -n "Bip01_Neck" -p "Bip01_Spine2";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".t" -type "double3" 13.398499999999999 -0.007899999999999999 0 ;
	setAttr ".s" -type "double3" 0.99999999999999956 0.99999999999999956 0.99999999999999978 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" 0 -0.020830999999999999 0.99978299999999998 0 0 -0.99978299999999998 -0.020830999999999999 0
		 1 0 0 0 -1.0000000000000001e-005 -9.6266999999999996 -5.6762839999999999 1;
	setAttr -k on ".liw";
	setAttr ".fbxID" 5;
createNode joint -n "Bip01_Head" -p "Bip01_Neck";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".t" -type "double3" 9.9829 0 0 ;
	setAttr ".s" -type "double3" 0.99999999999999978 1 1 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" 0 -0.10133399999999999 0.99485299999999999 0 0 -0.99485199999999996 -0.10133399999999999 0
		 1 0 0 0 -4.4000000000000006e-005 -9.8346470000000004 4.3044190000000002 1;
	setAttr -k on ".liw";
	setAttr ".fbxID" 5;
createNode joint -n "_notused_Bip01_HeadNub" -p "Bip01_Head";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".t" -type "double3" 18.3537 0 0 ;
	setAttr ".ssc" no;
	setAttr ".fbxID" 5;
createNode joint -n "Bip01_R_Clavicle" -p "Bip01_Neck";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".t" -type "double3" -0.8268 -0.6612 -3.9164999999999996 ;
	setAttr ".s" -type "double3" 1 1 0.99999999999999989 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" -0.99454799999999999 -9.9454800000000002e-007 -0.104286 0
		 -9.8906500000000008e-007 1 -1.0428599999999999e-007 0 0.104286 -5.7179000000000005e-010 -0.99454799999999999 0
		 -3.9165049999999999 2.9251839999999998 138.82619800000001 1;
	setAttr -k on ".liw";
	setAttr ".fbxID" 5;
createNode joint -n "Bip01_R_UpperArm" -p "Bip01_R_Clavicle";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".t" -type "double3" 10.6745 0 0 ;
	setAttr ".s" -type "double3" 0.99999999999999989 0.99999999999999989 1 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" -0.75332600000000005 0.10000199999999999 -0.65000000000000002 0
		 0.071782399999999996 0.99496899999999999 0.0698825 0 0.65371900000000005 0.0059857399999999998 -0.75671500000000003 0
		 -14.532831 2.9251239999999998 137.712976 1;
	setAttr -k on ".liw";
	setAttr ".fbxID" 5;
createNode joint -n "Bip01_R_Forearm" -p "Bip01_R_UpperArm";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".t" -type "double3" 25.337500000000002 0 0 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" -0.752861 -0.095945600000000006 -0.65114899999999998 0
		 -0.076500899999999997 0.99536800000000003 -0.058215000000000003 0 0.65371900000000005 0.0059856600000000003 -0.756714 0
		 -33.620182 5.4589869999999996 121.243574 1;
	setAttr -k on ".liw";
	setAttr ".fbxID" 5;
createNode joint -n "Bip01_R_Hand" -p "Bip01_R_Forearm";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".t" -type "double3" 24.9881 0 0 ;
	setAttr ".r" -type "double3" 90.133325000000013 -6.3004020000000009 2.5653330000000008 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" -0.67922700000000003 -0.050331500000000001 -0.73219999999999996 0
		 0.73278100000000002 0.0092545500000000003 -0.68040299999999998 0 0.0410219 -0.99868900000000005 0.030596000000000002 0
		 -52.432737000000003 3.0614050000000002 104.972679 1;
	setAttr -k on ".liw";
	setAttr ".fbxID" 5;
createNode joint -n "Bip01_R_Finger0" -p "Bip01_R_Hand";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".t" -type "double3" 2.3036000000000003 1.3941000000000001 2.9222 ;
	setAttr ".r" -type "double3" -81.93018 -23.314181 18.295908 ;
	setAttr ".s" -type "double3" 0.99999999999999989 1 1 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" -0.36474400000000001 -0.436471 -0.82246900000000001 0
		 -0.072261699999999998 0.89392799999999994 -0.44234699999999999 0 0.92829899999999999 -0.10191 -0.357595 0
		 -52.855969000000002 0.0400224 102.426821 1;
	setAttr -k on ".liw";
	setAttr ".fbxID" 5;
createNode joint -n "Bip01_R_Finger01" -p "Bip01_R_Finger0";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".t" -type "double3" 4.0806000000000004 0 0 ;
	setAttr ".r" -type "double3" 9.4787915988669428e-023 3.0000000000000005e-006 24.064549 ;
	setAttr ".s" -type "double3" 1 1 0.99999999999999989 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" -0.36250900000000003 -0.034022900000000002 -0.93135900000000005 0
		 0.082749500000000004 0.99421099999999996 -0.068527199999999996 0 0.92830000000000001 -0.101911 -0.357595 0
		 -54.344414999999998 -1.7410159999999999 99.070615000000004 1;
	setAttr -k on ".liw";
	setAttr ".fbxID" 5;
createNode joint -n "_notused_Bip01_R_Finger0Nub" -p "Bip01_R_Finger01";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".t" -type "double3" 3.3413 0 0 ;
	setAttr ".r" -type "double3" 1e-006 0 -180 ;
	setAttr ".ssc" no;
	setAttr ".fbxID" 5;
createNode joint -n "Bip01_R_Finger2" -p "Bip01_R_Hand";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".t" -type "double3" 7.3709 -0.4816 0.6341 ;
	setAttr ".r" -type "double3" 0.043615000000000029 -0.013407000000000007 17.086613999999997 ;
	setAttr ".s" -type "double3" 0.99999999999999989 0.99999999999999978 0.99999999999999989 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" -0.43393399999999999 -0.045623999999999998 -0.89978899999999995 0
		 0.90003599999999995 0.022875199999999998 -0.43521399999999999 0 0.040439000000000003 -0.99869600000000003 0.031137000000000001 0
		 -57.766043000000003 2.0527600000000001 99.922762000000006 1;
	setAttr -k on ".liw";
	setAttr ".fbxID" 5;
createNode joint -n "Bip01_R_Finger21" -p "Bip01_R_Finger2";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".t" -type "double3" 3.8207999999999998 0 0 ;
	setAttr ".r" -type "double3" 0 0 29.946389999999994 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" 0.0732873 -0.028113900000000001 -0.99691399999999997 0
		 0.99648999999999999 0.042598999999999998 0.072054900000000005 0 0.0404418 -0.99869600000000003 0.0311372 0
		 -59.424045999999997 1.8784400000000001 96.484806000000006 1;
	setAttr -k on ".liw";
	setAttr ".fbxID" 5;
createNode joint -n "_notused_Bip01_R_Finger2Nub" -p "Bip01_R_Finger21";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".t" -type "double3" 4.7174000000000005 0 0 ;
	setAttr ".r" -type "double3" 0 0 180 ;
	setAttr ".ssc" no;
	setAttr ".fbxID" 5;
createNode joint -n "Bip01_R_Finger1" -p "Bip01_R_Hand";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".t" -type "double3" 7.3263 -0.211 2.2784 ;
	setAttr ".r" -type "double3" -0.73310399999999953 -5.3145149999999983 22.646732999999994 ;
	setAttr ".s" -type "double3" 1 1 0.99999999999999989 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" -0.33941900000000003 -0.13520499999999999 -0.93086800000000003 0
		 0.93680799999999997 0.040593299999999999 -0.34748099999999998 0 0.084767999999999996 -0.98998600000000003 0.112883 0
		 -57.470132999999997 0.41539700000000002 99.821579 1;
	setAttr -k on ".liw";
	setAttr ".fbxID" 5;
createNode joint -n "Bip01_R_Finger11" -p "Bip01_R_Finger1";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".t" -type "double3" 3.5597999999999996 0 0 ;
	setAttr ".r" -type "double3" -2e-006 3.999999999999999e-006 22.627677999999996 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" 0.047135799999999999 -0.10918 -0.99290400000000001 0
		 0.995286 0.089482699999999998 0.037409400000000002 0 0.084763400000000003 -0.98998600000000003 0.112883 0
		 -58.678344000000003 -0.065903900000000001 96.507863999999998 1;
	setAttr -k on ".liw";
	setAttr ".fbxID" 5;
createNode joint -n "_notused_Bip01_R_Finger1Nub" -p "Bip01_R_Finger11";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".t" -type "double3" 4.1404000000000005 0 0 ;
	setAttr ".r" -type "double3" 0 0 180 ;
	setAttr ".ssc" no;
	setAttr ".fbxID" 5;
createNode joint -n "Bip01_R_Finger3" -p "Bip01_R_Hand";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".t" -type "double3" 7.2345000000000006 -0.5997 -0.9332 ;
	setAttr ".r" -type "double3" 0.043363999999999812 3.826455 18.509659999999997 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" -0.41327900000000001 0.0219578 -0.91034000000000004 0
		 0.91051400000000005 0.0239971 -0.41277999999999998 0 0.0127817 -0.999471 -0.0299104 0
		 -57.824306 3.6237469999999998 100.05504999999999 1;
	setAttr -k on ".liw";
	setAttr ".fbxID" 5;
createNode joint -n "Bip01_R_Finger31" -p "Bip01_R_Finger3";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".t" -type "double3" 3.7241000000000004 0 0 ;
	setAttr ".r" -type "double3" 3.0000000000000013e-006 3.0000000000000005e-006 27.984287000000009 ;
	setAttr ".s" -type "double3" 1 0.99999999999999989 0.99999999999999989 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" 0.0622821 0.0306512 -0.99758800000000003 0 0.99797599999999997 0.0108983 0.062641199999999994 0
		 0.012792100000000001 -0.999471 -0.0299104 0 -59.363360999999998 3.705527 96.664748000000003 1;
	setAttr -k on ".liw";
	setAttr ".fbxID" 5;
createNode joint -n "_notused_Bip01_R_Finger3Nub" -p "Bip01_R_Finger31";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".t" -type "double3" 4.2397 0 0 ;
	setAttr ".r" -type "double3" 3e-006 -5e-006 180 ;
	setAttr ".ssc" no;
	setAttr ".fbxID" 5;
createNode joint -n "Bip01_R_Finger4" -p "Bip01_R_Hand";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".t" -type "double3" 7.2292 -0.41869999999999996 -2.3639 ;
	setAttr ".r" -type "double3" -0.17713400000000007 5.5619560000000012 16.615208 ;
	setAttr ".s" -type "double3" 0.99999999999999989 0.99999999999999978 1 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" -0.44323200000000001 0.051426199999999998 -0.89493100000000003 0
		 0.89640699999999995 0.026346700000000001 -0.44244899999999998 0 0.00082491599999999997 -0.99832900000000002 -0.057776500000000001 0
		 -57.746822000000002 5.0544450000000003 99.892002000000005 1;
	setAttr -k on ".liw";
	setAttr ".fbxID" 5;
createNode joint -n "Bip01_R_Finger41" -p "Bip01_R_Finger4";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".t" -type "double3" 3.4791000000000003 0 0 ;
	setAttr ".r" -type "double3" -1.9999999999999995e-006 -3.0000000000000005e-006 29.444368 ;
	setAttr ".s" -type "double3" 1 0.99999999999999989 1 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" 0.054672699999999998 0.057735700000000001 -0.996834 0
		 0.99850399999999995 -0.0023359700000000001 0.054628999999999997 0 0.00082546799999999999 -0.99832900000000002 -0.057777000000000002 0
		 -59.288860999999997 5.2333790000000002 96.778379000000001 1;
	setAttr -k on ".liw";
	setAttr ".fbxID" 5;
createNode joint -n "_notused_Bip01_R_Finger4Nub" -p "Bip01_R_Finger41";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".t" -type "double3" 3.3973999999999998 0 0 ;
	setAttr ".r" -type "double3" 0 0 -180 ;
	setAttr ".ssc" no;
	setAttr ".fbxID" 5;
createNode joint -n "Bip01_L_Clavicle" -p "Bip01_Neck";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".t" -type "double3" -0.8268 -0.6612 3.9164999999999996 ;
	setAttr ".s" -type "double3" 0.99999999999999989 0.99999999999999956 0.99999999999999989 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" 0.99454799999999999 9.9454800000000002e-007 -0.104286 0
		 -9.8906500000000008e-007 1 1.0428599999999999e-007 0 0.104286 -5.7179000000000005e-010 0.99454799999999999 0
		 3.9165070000000002 2.9252039999999999 138.82619700000001 1;
	setAttr -k on ".liw";
	setAttr ".fbxID" 5;
createNode joint -n "Bip01_L_UpperArm" -p "Bip01_L_Clavicle";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".t" -type "double3" 10.6745 0 0 ;
	setAttr ".s" -type "double3" 1 1 0.99999999999999989 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" 0.75332500000000002 0.100004 -0.65000000000000002 0
		 -0.071785600000000005 0.99496899999999999 0.069882299999999994 0 0.65371900000000005 -0.0059834299999999997 0.756714 0
		 14.532825000000001 2.925173 137.712974 1;
	setAttr -k on ".liw";
	setAttr ".fbxID" 5;
createNode joint -n "Bip01_L_Forearm" -p "Bip01_L_UpperArm";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".t" -type "double3" 25.3374 0 0 ;
	setAttr ".s" -type "double3" 1 1 0.99999999999999978 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" 0.752861 -0.095943299999999995 -0.65114899999999998 0
		 0.076497700000000002 0.99536899999999995 -0.058215299999999998 0 0.65371900000000005 -0.0059834199999999997 0.756714 0
		 33.620189000000003 5.4590990000000001 121.243576 1;
	setAttr -k on ".liw";
	setAttr ".fbxID" 5;
createNode joint -n "Bip01_L_Hand" -p "Bip01_L_Forearm";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".t" -type "double3" 24.9881 0 0 ;
	setAttr ".r" -type "double3" -90.133325 6.300397000000002 2.565332999999999 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" 0.67922800000000005 -0.050329499999999999 -0.73219999999999996 0
		 -0.73278100000000002 0.0092525000000000003 -0.68040299999999998 0 0.041019 0.99868999999999997 -0.030595899999999999 0
		 52.432744999999997 3.0615610000000002 104.972662 1;
	setAttr -k on ".liw";
	setAttr ".fbxID" 5;
createNode joint -n "Bip01_L_Finger0" -p "Bip01_L_Hand";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".t" -type "double3" 2.3036000000000003 1.3941000000000001 -2.9222 ;
	setAttr ".r" -type "double3" 81.930180000000021 23.314181000000019 18.295909 ;
	setAttr ".s" -type "double3" 0.99999999999999989 1 1.0000000000000002 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" 0.36474600000000001 -0.43647000000000002 -0.82246900000000001 0
		 0.072258799999999998 0.89392799999999994 -0.44234699999999999 0 0.92830000000000001 0.101913 0.357595 0
		 52.856034999999999 0.0402022 102.42681 1;
	setAttr -k on ".liw";
	setAttr ".fbxID" 5;
createNode joint -n "Bip01_L_Finger01" -p "Bip01_L_Finger0";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".t" -type "double3" 4.0806000000000004 0 0 ;
	setAttr ".r" -type "double3" 0 0 24.064552000000006 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" 0.36250900000000003 -0.034021900000000001 -0.93135900000000005 0
		 -0.082751900000000003 0.99421199999999998 -0.068527000000000005 0 0.92829899999999999 0.101913 0.357595 0
		 54.344436000000002 -1.7408459999999999 99.070606999999995 1;
	setAttr -k on ".liw";
	setAttr ".fbxID" 5;
createNode joint -n "_notused_Bip01_L_Finger0Nub" -p "Bip01_L_Finger01";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".t" -type "double3" 3.3413 0 0 ;
	setAttr ".ssc" no;
	setAttr ".fbxID" 5;
createNode joint -n "Bip01_L_Finger2" -p "Bip01_L_Hand";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".t" -type "double3" 7.3709 -0.4816 -0.6341 ;
	setAttr ".r" -type "double3" -0.043610000000000031 0.01341 17.086619000000006 ;
	setAttr ".s" -type "double3" 1 0.99999999999999989 0.99999999999999978 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" 0.43393500000000002 -0.045622700000000002 -0.89978899999999995 0
		 -0.90003599999999995 0.022871599999999999 -0.43521399999999999 0 0.040435199999999998 0.99869699999999995 -0.031137399999999999 0
		 57.766060000000003 2.0529269999999999 99.922754999999995 1;
	setAttr -k on ".liw";
	setAttr ".fbxID" 5;
createNode joint -n "Bip01_L_Finger21" -p "Bip01_L_Finger2";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".t" -type "double3" 3.8207999999999998 0 0 ;
	setAttr ".r" -type "double3" -4.0000000000000024e-006 -4.0000000000000015e-006 29.946390000000019 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" -0.0732873 -0.0281141 -0.99691399999999997 0 -0.99649100000000002 0.042592999999999999 0.072054999999999994 0
		 0.040435899999999997 0.99869699999999995 -0.031137000000000001 0 59.424058000000002 1.878614 96.484800000000007 1;
	setAttr -k on ".liw";
	setAttr ".fbxID" 5;
createNode joint -n "_notused_Bip01_L_Finger2Nub" -p "Bip01_L_Finger21";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".t" -type "double3" 4.7174000000000005 0 0 ;
	setAttr ".ssc" no;
	setAttr ".fbxID" 5;
createNode joint -n "Bip01_L_Finger1" -p "Bip01_L_Hand";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".t" -type "double3" 7.3263 -0.211 -2.2784 ;
	setAttr ".r" -type "double3" 0.73310799999999965 5.3145129999999989 22.646742999999997 ;
	setAttr ".s" -type "double3" 1 0.99999999999999989 0.99999999999999989 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" 0.33942 -0.13520399999999999 -0.93086800000000003 0
		 -0.93680799999999997 0.040590800000000003 -0.34748099999999998 0 0.084765400000000005 0.98998600000000003 -0.112883 0
		 57.470143999999998 0.415574 99.821611000000004 1;
	setAttr -k on ".liw";
	setAttr ".fbxID" 5;
createNode joint -n "Bip01_L_Finger11" -p "Bip01_L_Finger1";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".t" -type "double3" 3.5597999999999996 0 0 ;
	setAttr ".r" -type "double3" -2.0000000000000012e-006 3.0000000000000005e-006 22.627677000000006 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" -0.047135499999999997 -0.10918 -0.99290400000000001 0
		 -0.99528499999999998 0.089479600000000006 0.037409299999999999 0 0.0847604 0.98998699999999995 -0.112883 0
		 58.678319000000002 -0.065726099999999996 96.507845000000003 1;
	setAttr -k on ".liw";
	setAttr ".fbxID" 5;
createNode joint -n "_notused_Bip01_L_Finger1Nub" -p "Bip01_L_Finger11";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".t" -type "double3" 4.1404000000000005 0 0 ;
	setAttr ".ssc" no;
	setAttr ".fbxID" 5;
createNode joint -n "Bip01_L_Finger3" -p "Bip01_L_Hand";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".t" -type "double3" 7.2345000000000006 -0.5997 0.9332 ;
	setAttr ".r" -type "double3" -0.043362000000000123 -3.826458 18.509666999999997 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" 0.41327900000000001 0.021958700000000001 -0.91034000000000004 0
		 -0.91051400000000005 0.0239966 -0.41277999999999998 0 0.012781000000000001 0.999471 0.029911 0
		 57.824314000000001 3.6238670000000002 100.055049 1;
	setAttr -k on ".liw";
	setAttr ".fbxID" 5;
createNode joint -n "Bip01_L_Finger31" -p "Bip01_L_Finger3";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".t" -type "double3" 3.7241000000000004 0 0 ;
	setAttr ".r" -type "double3" 2e-006 -9.4787915988669299e-023 27.984288 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" -0.062282200000000003 0.030651100000000001 -0.99758800000000003 0
		 -0.99797599999999997 0.0108933 0.062641199999999994 0 0.012787099999999999 0.999471 0.029910599999999999 0
		 59.363363 3.7056490000000002 96.664751999999993 1;
	setAttr -k on ".liw";
	setAttr ".fbxID" 5;
createNode joint -n "_notused_Bip01_L_Finger3Nub" -p "Bip01_L_Finger31";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".t" -type "double3" 4.2397 0 0 ;
	setAttr ".ssc" no;
	setAttr ".fbxID" 5;
createNode joint -n "Bip01_L_Finger4" -p "Bip01_L_Hand";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".t" -type "double3" 7.2292 -0.41869999999999996 2.3639 ;
	setAttr ".r" -type "double3" 0.17713199999999998 -5.5619539999999983 16.615218000000002 ;
	setAttr ".s" -type "double3" 1 0.99999999999999989 1 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" 0.44323200000000001 0.051427599999999997 -0.89493100000000003 0
		 -0.89640600000000004 0.026343800000000001 -0.44244899999999998 0 0.00082181400000000003 0.99832900000000002 0.057776399999999999 0
		 57.746775 5.054627 99.891990000000007 1;
	setAttr -k on ".liw";
	setAttr ".fbxID" 5;
createNode joint -n "Bip01_L_Finger41" -p "Bip01_L_Finger4";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".t" -type "double3" 3.4791000000000003 0 0 ;
	setAttr ".r" -type "double3" 2e-006 -3.0000000000000009e-006 29.444363999999997 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" -0.054672800000000001 0.057735500000000002 -0.996834 0
		 -0.99850399999999995 -0.00234096 0.054628799999999998 0 0.00082047800000000003 0.99832900000000002 0.057777200000000001 0
		 59.288862000000002 5.2335589999999996 96.778389000000004 1;
	setAttr -k on ".liw";
	setAttr ".fbxID" 5;
createNode joint -n "_notused_Bip01_L_Finger4Nub" -p "Bip01_L_Finger41";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".t" -type "double3" 3.3973999999999998 0 0 ;
	setAttr ".ssc" no;
	setAttr ".fbxID" 5;
createNode joint -n "Bip01_L_Thigh" -p "Bip01_Spine";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".t" -type "double3" -11.0456 2.2584 8.5201999999999991 ;
	setAttr ".s" -type "double3" 1 0.99999999999999978 1.0000000000000002 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" 0.059694200000000003 0.0067868399999999997 -0.998193 0
		 0.0013515700000000001 -0.99997599999999998 -0.00671814 0 -0.99821599999999999 -0.00094809699999999996 -0.059701999999999998 0
		 8.5202019999999994 4.3064900000000002e-005 88.396292000000003 1;
	setAttr -k on ".liw";
	setAttr ".fbxID" 5;
createNode joint -n "Bip01_L_Calf" -p "Bip01_L_Thigh";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".t" -type "double3" 36.152699999999996 0 0 ;
	setAttr ".s" -type "double3" 0.99999999999999989 0.99999999999999989 0.99999999999999989 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" 0.059157700000000001 0.119882 -0.99102400000000002 0
		 0.0080950799999999993 -0.99278699999999998 -0.119612 0 -0.99821599999999999 -0.00094644599999999998 -0.059701400000000002 0
		 10.678299000000001 0.245397 52.308965999999998 1;
	setAttr -k on ".liw";
	setAttr ".fbxID" 5;
createNode joint -n "Bip01_L_Foot" -p "Bip01_L_Calf";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".t" -type "double3" 43.2543 0 0 ;
	setAttr ".s" -type "double3" 1 1 0.99999999999999989 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" 0 0 -1 0 0.123471 -0.99234800000000001 0 0 -0.99234800000000001 -0.123471 0 0
		 13.237131 5.4308170000000002 9.4429110000000005 1;
	setAttr -k on ".liw";
	setAttr ".fbxID" 5;
createNode joint -n "Bip01_L_Toe0" -p "Bip01_L_Foot";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".t" -type "double3" 9.4714999999999989 11.2377 0 ;
	setAttr ".r" -type "double3" -5.0000000000000021e-006 1.9999999999999995e-006 89.999979999999979 ;
	setAttr ".s" -type "double3" 0.99999999999999989 1 1 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" 0.123471 -0.99234800000000001 0 0 0 0 1 0 -0.99234800000000001 -0.123471 0 0
		 14.624665999999999 -5.7209120000000002 -0.028559000000000001 1;
	setAttr -k on ".liw";
	setAttr ".fbxID" 5;
createNode joint -n "_notused_Bip01_L_Toe0Nub" -p "Bip01_L_Toe0";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".t" -type "double3" 7.2684999999999995 0 0 ;
	setAttr ".r" -type "double3" 0 0 180 ;
	setAttr ".ssc" no;
	setAttr ".fbxID" 5;
createNode joint -n "Bip01_R_Thigh" -p "Bip01_Spine";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".t" -type "double3" -11.0456 2.2585 -8.5201999999999991 ;
	setAttr ".s" -type "double3" 1 0.99999999999999989 1 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" -0.059694200000000003 0.0067826700000000002 -0.998193 0
		 -0.00134545 -0.99997599999999998 -0.0067143300000000001 0 -0.99821599999999999 0.00094221699999999997 0.059701900000000002 0
		 -8.5201879999999992 1.40303e-005 88.396298000000002 1;
	setAttr -k on ".liw";
	setAttr ".fbxID" 5;
createNode joint -n "Bip01_R_Calf" -p "Bip01_R_Thigh";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".t" -type "double3" 36.152699999999996 0 0 ;
	setAttr ".s" -type "double3" 0.99999999999999989 1 1 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" -0.059158000000000002 0.11988699999999999 -0.99102400000000002 0
		 -0.0080972100000000005 -0.99278699999999998 -0.119617 0 -0.99821599999999999 0.00094822899999999998 0.059701999999999998 0
		 -10.678288 0.24518599999999999 52.308996999999998 1;
	setAttr -k on ".liw";
	setAttr ".fbxID" 5;
createNode joint -n "Bip01_R_Foot" -p "Bip01_R_Calf";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".t" -type "double3" 43.2543 0 0 ;
	setAttr ".s" -type "double3" 0.99999999999999978 1 1 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" 0 0 -1 0 -0.123469 -0.99234800000000001 0 0 -0.99234800000000001 0.123469 0 0
		 -13.237124 5.4308209999999999 9.442933 1;
	setAttr -k on ".liw";
	setAttr ".fbxID" 5;
createNode joint -n "Bip01_R_Toe0" -p "Bip01_R_Foot";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".t" -type "double3" 9.4714999999999989 11.2377 0 ;
	setAttr ".r" -type "double3" 1.9999999999999991e-006 -1.9999999999999995e-006 89.999985999999993 ;
	setAttr ".s" -type "double3" 0.99999999999999978 0.99999999999999978 1 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" -0.12346799999999999 -0.99234800000000001 0 0 0 0 1 0
		 -0.99234800000000001 0.12346799999999999 0 0 -14.624625999999999 -5.7209019999999997 -0.028538999999999998 1;
	setAttr -k on ".liw";
	setAttr ".fbxID" 5;
createNode joint -n "_notused_Bip01_R_Toe0Nub" -p "Bip01_R_Toe0";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".t" -type "double3" 7.2684999999999995 0 0 ;
	setAttr ".ssc" no;
	setAttr ".fbxID" 5;
createNode lightLinker -s -n "lightLinker1";
	setAttr -s 130 ".lnk";
	setAttr -s 130 ".slnk";
createNode displayLayerManager -n "layerManager";
createNode displayLayer -n "defaultLayer";
createNode renderLayerManager -n "renderLayerManager";
createNode renderLayer -n "defaultRenderLayer";
	setAttr ".g" yes;
createNode phong -n "business05_f_highpolyPhong";
	setAttr ".sc" -type "float3" 0 0 0 ;
createNode shadingEngine -n "business05_f_highpolySG";
	setAttr ".ihi" 0;
	setAttr ".ro" yes;
createNode materialInfo -n "materialInfo1";
createNode shadingEngine -n "business05_f_highpolySG1";
	setAttr ".ihi" 0;
	setAttr ".ro" yes;
createNode materialInfo -n "materialInfo2";
createNode file -n "Map1";
	setAttr ".ftn" -type "string" "C:\\Users\\Kuan\\Desktop\\iPi Record 20150227\\business05_f\\business05_f_35.tga";
createNode place2dTexture -n "place2dTexture1";
createNode phong -n "business05_f_highpoly_hairPhong";
	setAttr ".c" -type "float3" 0 0 0 ;
	setAttr ".sc" -type "float3" 0 0 0 ;
createNode shadingEngine -n "business05_f_highpoly_hairSG";
	setAttr ".ihi" 0;
	setAttr ".ro" yes;
createNode materialInfo -n "materialInfo3";
createNode shadingEngine -n "business05_f_highpoly_hairSG1";
	setAttr ".ihi" 0;
	setAttr ".ro" yes;
createNode materialInfo -n "materialInfo4";
createNode phong -n "MoCap_Result_Animations_v1_business05_f_highpolyPhong";
	setAttr ".sc" -type "float3" 0 0 0 ;
createNode shadingEngine -n "MoCap_Result_Animations_v1_business05_f_highpolySG";
	setAttr ".ihi" 0;
	setAttr ".ro" yes;
createNode materialInfo -n "MoCap_Result_Animations_v1_materialInfo1";
createNode shadingEngine -n "MoCap_Result_Animations_v1_business05_f_highpolySG1";
	setAttr ".ihi" 0;
	setAttr ".ro" yes;
createNode materialInfo -n "MoCap_Result_Animations_v1_materialInfo2";
createNode file -n "MoCap_Result_Animations_v1_Map1";
	setAttr ".ftn" -type "string" "C:\\Users\\Kuan\\Desktop\\iPi Record 20150227\\business05_f\\business05_f_35.tga";
createNode place2dTexture -n "MoCap_Result_Animations_v1_place2dTexture1";
createNode phong -n "MoCap_Result_Animations_v1_business05_f_highpoly_hairPhong";
	setAttr ".c" -type "float3" 0 0 0 ;
	setAttr ".sc" -type "float3" 0 0 0 ;
createNode shadingEngine -n "MoCap_Result_Animations_v1_business05_f_highpoly_hairSG";
	setAttr ".ihi" 0;
	setAttr ".ro" yes;
createNode materialInfo -n "MoCap_Result_Animations_v1_materialInfo3";
createNode shadingEngine -n "MoCap_Result_Animations_v1_business05_f_highpoly_hairSG1";
	setAttr ".ihi" 0;
	setAttr ".ro" yes;
createNode materialInfo -n "MoCap_Result_Animations_v1_materialInfo4";
createNode phong -n "idle_v1:business05_f_highpolyPhong";
	setAttr ".sc" -type "float3" 0 0 0 ;
createNode shadingEngine -n "idle_v1:business05_f_highpolySG";
	setAttr ".ihi" 0;
	setAttr ".ro" yes;
createNode materialInfo -n "idle_v1:materialInfo1";
createNode shadingEngine -n "idle_v1:business05_f_highpolySG1";
	setAttr ".ihi" 0;
	setAttr ".ro" yes;
createNode materialInfo -n "idle_v1:materialInfo2";
createNode file -n "idle_v1:Map1";
	setAttr ".ftn" -type "string" "C:\\Users\\Kuan\\Desktop\\iPi Record 20150227\\business05_f\\business05_f_35.tga";
createNode place2dTexture -n "idle_v1:place2dTexture1";
createNode phong -n "idle_v1:business05_f_highpoly_hairPhong";
	setAttr ".c" -type "float3" 0 0 0 ;
	setAttr ".sc" -type "float3" 0 0 0 ;
createNode shadingEngine -n "idle_v1:business05_f_highpoly_hairSG";
	setAttr ".ihi" 0;
	setAttr ".ro" yes;
createNode materialInfo -n "idle_v1:materialInfo3";
createNode shadingEngine -n "idle_v1:business05_f_highpoly_hairSG1";
	setAttr ".ihi" 0;
	setAttr ".ro" yes;
createNode materialInfo -n "idle_v1:materialInfo4";
createNode script -n "idle_v1:uiConfigurationScriptNode";
	setAttr ".b" -type "string" (
		"// Maya Mel UI Configuration File.\n//\n//  This script is machine generated.  Edit at your own risk.\n//\n//\n\nglobal string $gMainPane;\nif (`paneLayout -exists $gMainPane`) {\n\n\tglobal int $gUseScenePanelConfig;\n\tint    $useSceneConfig = $gUseScenePanelConfig;\n\tint    $menusOkayInPanels = `optionVar -q allowMenusInPanels`;\tint    $nVisPanes = `paneLayout -q -nvp $gMainPane`;\n\tint    $nPanes = 0;\n\tstring $editorName;\n\tstring $panelName;\n\tstring $itemFilterName;\n\tstring $panelConfig;\n\n\t//\n\t//  get current state of the UI\n\t//\n\tsceneUIReplacement -update $gMainPane;\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"modelPanel\" (localizedPanelLabel(\"Top View\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `modelPanel -unParent -l (localizedPanelLabel(\"Top View\")) -mbv $menusOkayInPanels `;\n\t\t\t$editorName = $panelName;\n            modelEditor -e \n                -camera \"top\" \n                -useInteractiveMode 0\n                -displayLights \"default\" \n                -displayAppearance \"wireframe\" \n"
		+ "                -activeOnly 0\n                -ignorePanZoom 0\n                -wireframeOnShaded 0\n                -headsUpDisplay 1\n                -selectionHiliteDisplay 1\n                -useDefaultMaterial 0\n                -bufferMode \"double\" \n                -twoSidedLighting 1\n                -backfaceCulling 0\n                -xray 0\n                -jointXray 0\n                -activeComponentsXray 0\n                -displayTextures 0\n                -smoothWireframe 0\n                -lineWidth 1\n                -textureAnisotropic 0\n                -textureHilight 1\n                -textureSampling 2\n                -textureDisplay \"modulate\" \n                -textureMaxSize 16384\n                -fogging 0\n                -fogSource \"fragment\" \n                -fogMode \"linear\" \n                -fogStart 0\n                -fogEnd 100\n                -fogDensity 0.1\n                -fogColor 0.5 0.5 0.5 1 \n                -maxConstantTransparency 1\n                -rendererName \"base_OpenGL_Renderer\" \n"
		+ "                -objectFilterShowInHUD 1\n                -isFiltered 0\n                -colorResolution 4 4 \n                -bumpResolution 4 4 \n                -textureCompression 0\n                -transparencyAlgorithm \"frontAndBackCull\" \n                -transpInShadows 0\n                -cullingOverride \"none\" \n                -lowQualityLighting 0\n                -maximumNumHardwareLights 0\n                -occlusionCulling 0\n                -shadingModel 0\n                -useBaseRenderer 0\n                -useReducedRenderer 0\n                -smallObjectCulling 0\n                -smallObjectThreshold -1 \n                -interactiveDisableShadows 0\n                -interactiveBackFaceCull 0\n                -sortTransparent 1\n                -nurbsCurves 1\n                -nurbsSurfaces 1\n                -polymeshes 1\n                -subdivSurfaces 1\n                -planes 1\n                -lights 1\n                -cameras 1\n                -controlVertices 1\n                -hulls 1\n                -grid 1\n"
		+ "                -imagePlane 1\n                -joints 1\n                -ikHandles 1\n                -deformers 1\n                -dynamics 1\n                -fluids 1\n                -hairSystems 1\n                -follicles 1\n                -nCloths 1\n                -nParticles 1\n                -nRigids 1\n                -dynamicConstraints 1\n                -locators 1\n                -manipulators 1\n                -pluginShapes 1\n                -dimensions 1\n                -handles 1\n                -pivots 1\n                -textures 1\n                -strokes 1\n                -motionTrails 1\n                -clipGhosts 1\n                -greasePencils 1\n                -shadows 0\n                $editorName;\n            modelEditor -e -viewSelected 0 $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tmodelPanel -edit -l (localizedPanelLabel(\"Top View\")) -mbv $menusOkayInPanels  $panelName;\n\t\t$editorName = $panelName;\n        modelEditor -e \n            -camera \"top\" \n            -useInteractiveMode 0\n"
		+ "            -displayLights \"default\" \n            -displayAppearance \"wireframe\" \n            -activeOnly 0\n            -ignorePanZoom 0\n            -wireframeOnShaded 0\n            -headsUpDisplay 1\n            -selectionHiliteDisplay 1\n            -useDefaultMaterial 0\n            -bufferMode \"double\" \n            -twoSidedLighting 1\n            -backfaceCulling 0\n            -xray 0\n            -jointXray 0\n            -activeComponentsXray 0\n            -displayTextures 0\n            -smoothWireframe 0\n            -lineWidth 1\n            -textureAnisotropic 0\n            -textureHilight 1\n            -textureSampling 2\n            -textureDisplay \"modulate\" \n            -textureMaxSize 16384\n            -fogging 0\n            -fogSource \"fragment\" \n            -fogMode \"linear\" \n            -fogStart 0\n            -fogEnd 100\n            -fogDensity 0.1\n            -fogColor 0.5 0.5 0.5 1 \n            -maxConstantTransparency 1\n            -rendererName \"base_OpenGL_Renderer\" \n            -objectFilterShowInHUD 1\n"
		+ "            -isFiltered 0\n            -colorResolution 4 4 \n            -bumpResolution 4 4 \n            -textureCompression 0\n            -transparencyAlgorithm \"frontAndBackCull\" \n            -transpInShadows 0\n            -cullingOverride \"none\" \n            -lowQualityLighting 0\n            -maximumNumHardwareLights 0\n            -occlusionCulling 0\n            -shadingModel 0\n            -useBaseRenderer 0\n            -useReducedRenderer 0\n            -smallObjectCulling 0\n            -smallObjectThreshold -1 \n            -interactiveDisableShadows 0\n            -interactiveBackFaceCull 0\n            -sortTransparent 1\n            -nurbsCurves 1\n            -nurbsSurfaces 1\n            -polymeshes 1\n            -subdivSurfaces 1\n            -planes 1\n            -lights 1\n            -cameras 1\n            -controlVertices 1\n            -hulls 1\n            -grid 1\n            -imagePlane 1\n            -joints 1\n            -ikHandles 1\n            -deformers 1\n            -dynamics 1\n            -fluids 1\n"
		+ "            -hairSystems 1\n            -follicles 1\n            -nCloths 1\n            -nParticles 1\n            -nRigids 1\n            -dynamicConstraints 1\n            -locators 1\n            -manipulators 1\n            -pluginShapes 1\n            -dimensions 1\n            -handles 1\n            -pivots 1\n            -textures 1\n            -strokes 1\n            -motionTrails 1\n            -clipGhosts 1\n            -greasePencils 1\n            -shadows 0\n            $editorName;\n        modelEditor -e -viewSelected 0 $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"modelPanel\" (localizedPanelLabel(\"Side View\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `modelPanel -unParent -l (localizedPanelLabel(\"Side View\")) -mbv $menusOkayInPanels `;\n\t\t\t$editorName = $panelName;\n            modelEditor -e \n                -camera \"side\" \n                -useInteractiveMode 0\n                -displayLights \"default\" \n"
		+ "                -displayAppearance \"wireframe\" \n                -activeOnly 0\n                -ignorePanZoom 0\n                -wireframeOnShaded 0\n                -headsUpDisplay 1\n                -selectionHiliteDisplay 1\n                -useDefaultMaterial 0\n                -bufferMode \"double\" \n                -twoSidedLighting 1\n                -backfaceCulling 0\n                -xray 0\n                -jointXray 0\n                -activeComponentsXray 0\n                -displayTextures 0\n                -smoothWireframe 0\n                -lineWidth 1\n                -textureAnisotropic 0\n                -textureHilight 1\n                -textureSampling 2\n                -textureDisplay \"modulate\" \n                -textureMaxSize 16384\n                -fogging 0\n                -fogSource \"fragment\" \n                -fogMode \"linear\" \n                -fogStart 0\n                -fogEnd 100\n                -fogDensity 0.1\n                -fogColor 0.5 0.5 0.5 1 \n                -maxConstantTransparency 1\n"
		+ "                -rendererName \"base_OpenGL_Renderer\" \n                -objectFilterShowInHUD 1\n                -isFiltered 0\n                -colorResolution 4 4 \n                -bumpResolution 4 4 \n                -textureCompression 0\n                -transparencyAlgorithm \"frontAndBackCull\" \n                -transpInShadows 0\n                -cullingOverride \"none\" \n                -lowQualityLighting 0\n                -maximumNumHardwareLights 0\n                -occlusionCulling 0\n                -shadingModel 0\n                -useBaseRenderer 0\n                -useReducedRenderer 0\n                -smallObjectCulling 0\n                -smallObjectThreshold -1 \n                -interactiveDisableShadows 0\n                -interactiveBackFaceCull 0\n                -sortTransparent 1\n                -nurbsCurves 1\n                -nurbsSurfaces 1\n                -polymeshes 1\n                -subdivSurfaces 1\n                -planes 1\n                -lights 1\n                -cameras 1\n                -controlVertices 1\n"
		+ "                -hulls 1\n                -grid 1\n                -imagePlane 1\n                -joints 1\n                -ikHandles 1\n                -deformers 1\n                -dynamics 1\n                -fluids 1\n                -hairSystems 1\n                -follicles 1\n                -nCloths 1\n                -nParticles 1\n                -nRigids 1\n                -dynamicConstraints 1\n                -locators 1\n                -manipulators 1\n                -pluginShapes 1\n                -dimensions 1\n                -handles 1\n                -pivots 1\n                -textures 1\n                -strokes 1\n                -motionTrails 1\n                -clipGhosts 1\n                -greasePencils 1\n                -shadows 0\n                $editorName;\n            modelEditor -e -viewSelected 0 $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tmodelPanel -edit -l (localizedPanelLabel(\"Side View\")) -mbv $menusOkayInPanels  $panelName;\n\t\t$editorName = $panelName;\n        modelEditor -e \n"
		+ "            -camera \"side\" \n            -useInteractiveMode 0\n            -displayLights \"default\" \n            -displayAppearance \"wireframe\" \n            -activeOnly 0\n            -ignorePanZoom 0\n            -wireframeOnShaded 0\n            -headsUpDisplay 1\n            -selectionHiliteDisplay 1\n            -useDefaultMaterial 0\n            -bufferMode \"double\" \n            -twoSidedLighting 1\n            -backfaceCulling 0\n            -xray 0\n            -jointXray 0\n            -activeComponentsXray 0\n            -displayTextures 0\n            -smoothWireframe 0\n            -lineWidth 1\n            -textureAnisotropic 0\n            -textureHilight 1\n            -textureSampling 2\n            -textureDisplay \"modulate\" \n            -textureMaxSize 16384\n            -fogging 0\n            -fogSource \"fragment\" \n            -fogMode \"linear\" \n            -fogStart 0\n            -fogEnd 100\n            -fogDensity 0.1\n            -fogColor 0.5 0.5 0.5 1 \n            -maxConstantTransparency 1\n            -rendererName \"base_OpenGL_Renderer\" \n"
		+ "            -objectFilterShowInHUD 1\n            -isFiltered 0\n            -colorResolution 4 4 \n            -bumpResolution 4 4 \n            -textureCompression 0\n            -transparencyAlgorithm \"frontAndBackCull\" \n            -transpInShadows 0\n            -cullingOverride \"none\" \n            -lowQualityLighting 0\n            -maximumNumHardwareLights 0\n            -occlusionCulling 0\n            -shadingModel 0\n            -useBaseRenderer 0\n            -useReducedRenderer 0\n            -smallObjectCulling 0\n            -smallObjectThreshold -1 \n            -interactiveDisableShadows 0\n            -interactiveBackFaceCull 0\n            -sortTransparent 1\n            -nurbsCurves 1\n            -nurbsSurfaces 1\n            -polymeshes 1\n            -subdivSurfaces 1\n            -planes 1\n            -lights 1\n            -cameras 1\n            -controlVertices 1\n            -hulls 1\n            -grid 1\n            -imagePlane 1\n            -joints 1\n            -ikHandles 1\n            -deformers 1\n            -dynamics 1\n"
		+ "            -fluids 1\n            -hairSystems 1\n            -follicles 1\n            -nCloths 1\n            -nParticles 1\n            -nRigids 1\n            -dynamicConstraints 1\n            -locators 1\n            -manipulators 1\n            -pluginShapes 1\n            -dimensions 1\n            -handles 1\n            -pivots 1\n            -textures 1\n            -strokes 1\n            -motionTrails 1\n            -clipGhosts 1\n            -greasePencils 1\n            -shadows 0\n            $editorName;\n        modelEditor -e -viewSelected 0 $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"modelPanel\" (localizedPanelLabel(\"Front View\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `modelPanel -unParent -l (localizedPanelLabel(\"Front View\")) -mbv $menusOkayInPanels `;\n\t\t\t$editorName = $panelName;\n            modelEditor -e \n                -camera \"front\" \n                -useInteractiveMode 0\n                -displayLights \"default\" \n"
		+ "                -displayAppearance \"wireframe\" \n                -activeOnly 0\n                -ignorePanZoom 0\n                -wireframeOnShaded 0\n                -headsUpDisplay 1\n                -selectionHiliteDisplay 1\n                -useDefaultMaterial 0\n                -bufferMode \"double\" \n                -twoSidedLighting 1\n                -backfaceCulling 0\n                -xray 0\n                -jointXray 0\n                -activeComponentsXray 0\n                -displayTextures 0\n                -smoothWireframe 0\n                -lineWidth 1\n                -textureAnisotropic 0\n                -textureHilight 1\n                -textureSampling 2\n                -textureDisplay \"modulate\" \n                -textureMaxSize 16384\n                -fogging 0\n                -fogSource \"fragment\" \n                -fogMode \"linear\" \n                -fogStart 0\n                -fogEnd 100\n                -fogDensity 0.1\n                -fogColor 0.5 0.5 0.5 1 \n                -maxConstantTransparency 1\n"
		+ "                -rendererName \"base_OpenGL_Renderer\" \n                -objectFilterShowInHUD 1\n                -isFiltered 0\n                -colorResolution 4 4 \n                -bumpResolution 4 4 \n                -textureCompression 0\n                -transparencyAlgorithm \"frontAndBackCull\" \n                -transpInShadows 0\n                -cullingOverride \"none\" \n                -lowQualityLighting 0\n                -maximumNumHardwareLights 0\n                -occlusionCulling 0\n                -shadingModel 0\n                -useBaseRenderer 0\n                -useReducedRenderer 0\n                -smallObjectCulling 0\n                -smallObjectThreshold -1 \n                -interactiveDisableShadows 0\n                -interactiveBackFaceCull 0\n                -sortTransparent 1\n                -nurbsCurves 1\n                -nurbsSurfaces 1\n                -polymeshes 1\n                -subdivSurfaces 1\n                -planes 1\n                -lights 1\n                -cameras 1\n                -controlVertices 1\n"
		+ "                -hulls 1\n                -grid 1\n                -imagePlane 1\n                -joints 1\n                -ikHandles 1\n                -deformers 1\n                -dynamics 1\n                -fluids 1\n                -hairSystems 1\n                -follicles 1\n                -nCloths 1\n                -nParticles 1\n                -nRigids 1\n                -dynamicConstraints 1\n                -locators 1\n                -manipulators 1\n                -pluginShapes 1\n                -dimensions 1\n                -handles 1\n                -pivots 1\n                -textures 1\n                -strokes 1\n                -motionTrails 1\n                -clipGhosts 1\n                -greasePencils 1\n                -shadows 0\n                $editorName;\n            modelEditor -e -viewSelected 0 $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tmodelPanel -edit -l (localizedPanelLabel(\"Front View\")) -mbv $menusOkayInPanels  $panelName;\n\t\t$editorName = $panelName;\n        modelEditor -e \n"
		+ "            -camera \"front\" \n            -useInteractiveMode 0\n            -displayLights \"default\" \n            -displayAppearance \"wireframe\" \n            -activeOnly 0\n            -ignorePanZoom 0\n            -wireframeOnShaded 0\n            -headsUpDisplay 1\n            -selectionHiliteDisplay 1\n            -useDefaultMaterial 0\n            -bufferMode \"double\" \n            -twoSidedLighting 1\n            -backfaceCulling 0\n            -xray 0\n            -jointXray 0\n            -activeComponentsXray 0\n            -displayTextures 0\n            -smoothWireframe 0\n            -lineWidth 1\n            -textureAnisotropic 0\n            -textureHilight 1\n            -textureSampling 2\n            -textureDisplay \"modulate\" \n            -textureMaxSize 16384\n            -fogging 0\n            -fogSource \"fragment\" \n            -fogMode \"linear\" \n            -fogStart 0\n            -fogEnd 100\n            -fogDensity 0.1\n            -fogColor 0.5 0.5 0.5 1 \n            -maxConstantTransparency 1\n            -rendererName \"base_OpenGL_Renderer\" \n"
		+ "            -objectFilterShowInHUD 1\n            -isFiltered 0\n            -colorResolution 4 4 \n            -bumpResolution 4 4 \n            -textureCompression 0\n            -transparencyAlgorithm \"frontAndBackCull\" \n            -transpInShadows 0\n            -cullingOverride \"none\" \n            -lowQualityLighting 0\n            -maximumNumHardwareLights 0\n            -occlusionCulling 0\n            -shadingModel 0\n            -useBaseRenderer 0\n            -useReducedRenderer 0\n            -smallObjectCulling 0\n            -smallObjectThreshold -1 \n            -interactiveDisableShadows 0\n            -interactiveBackFaceCull 0\n            -sortTransparent 1\n            -nurbsCurves 1\n            -nurbsSurfaces 1\n            -polymeshes 1\n            -subdivSurfaces 1\n            -planes 1\n            -lights 1\n            -cameras 1\n            -controlVertices 1\n            -hulls 1\n            -grid 1\n            -imagePlane 1\n            -joints 1\n            -ikHandles 1\n            -deformers 1\n            -dynamics 1\n"
		+ "            -fluids 1\n            -hairSystems 1\n            -follicles 1\n            -nCloths 1\n            -nParticles 1\n            -nRigids 1\n            -dynamicConstraints 1\n            -locators 1\n            -manipulators 1\n            -pluginShapes 1\n            -dimensions 1\n            -handles 1\n            -pivots 1\n            -textures 1\n            -strokes 1\n            -motionTrails 1\n            -clipGhosts 1\n            -greasePencils 1\n            -shadows 0\n            $editorName;\n        modelEditor -e -viewSelected 0 $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"modelPanel\" (localizedPanelLabel(\"Persp View\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `modelPanel -unParent -l (localizedPanelLabel(\"Persp View\")) -mbv $menusOkayInPanels `;\n\t\t\t$editorName = $panelName;\n            modelEditor -e \n                -camera \"persp\" \n                -useInteractiveMode 0\n                -displayLights \"default\" \n"
		+ "                -displayAppearance \"smoothShaded\" \n                -activeOnly 0\n                -ignorePanZoom 0\n                -wireframeOnShaded 0\n                -headsUpDisplay 1\n                -selectionHiliteDisplay 1\n                -useDefaultMaterial 0\n                -bufferMode \"double\" \n                -twoSidedLighting 1\n                -backfaceCulling 0\n                -xray 0\n                -jointXray 1\n                -activeComponentsXray 0\n                -displayTextures 1\n                -smoothWireframe 0\n                -lineWidth 1\n                -textureAnisotropic 0\n                -textureHilight 1\n                -textureSampling 2\n                -textureDisplay \"modulate\" \n                -textureMaxSize 16384\n                -fogging 0\n                -fogSource \"fragment\" \n                -fogMode \"linear\" \n                -fogStart 0\n                -fogEnd 100\n                -fogDensity 0.1\n                -fogColor 0.5 0.5 0.5 1 \n                -maxConstantTransparency 1\n"
		+ "                -rendererName \"base_OpenGL_Renderer\" \n                -objectFilterShowInHUD 1\n                -isFiltered 0\n                -colorResolution 4 4 \n                -bumpResolution 4 4 \n                -textureCompression 0\n                -transparencyAlgorithm \"frontAndBackCull\" \n                -transpInShadows 0\n                -cullingOverride \"none\" \n                -lowQualityLighting 0\n                -maximumNumHardwareLights 1\n                -occlusionCulling 0\n                -shadingModel 0\n                -useBaseRenderer 0\n                -useReducedRenderer 0\n                -smallObjectCulling 0\n                -smallObjectThreshold -1 \n                -interactiveDisableShadows 0\n                -interactiveBackFaceCull 0\n                -sortTransparent 1\n                -nurbsCurves 1\n                -nurbsSurfaces 1\n                -polymeshes 1\n                -subdivSurfaces 1\n                -planes 1\n                -lights 1\n                -cameras 1\n                -controlVertices 1\n"
		+ "                -hulls 1\n                -grid 1\n                -imagePlane 1\n                -joints 1\n                -ikHandles 1\n                -deformers 1\n                -dynamics 1\n                -fluids 1\n                -hairSystems 1\n                -follicles 1\n                -nCloths 1\n                -nParticles 1\n                -nRigids 1\n                -dynamicConstraints 1\n                -locators 1\n                -manipulators 1\n                -pluginShapes 1\n                -dimensions 1\n                -handles 1\n                -pivots 1\n                -textures 1\n                -strokes 1\n                -motionTrails 1\n                -clipGhosts 1\n                -greasePencils 1\n                -shadows 0\n                $editorName;\n            modelEditor -e -viewSelected 0 $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tmodelPanel -edit -l (localizedPanelLabel(\"Persp View\")) -mbv $menusOkayInPanels  $panelName;\n\t\t$editorName = $panelName;\n        modelEditor -e \n"
		+ "            -camera \"persp\" \n            -useInteractiveMode 0\n            -displayLights \"default\" \n            -displayAppearance \"smoothShaded\" \n            -activeOnly 0\n            -ignorePanZoom 0\n            -wireframeOnShaded 0\n            -headsUpDisplay 1\n            -selectionHiliteDisplay 1\n            -useDefaultMaterial 0\n            -bufferMode \"double\" \n            -twoSidedLighting 1\n            -backfaceCulling 0\n            -xray 0\n            -jointXray 1\n            -activeComponentsXray 0\n            -displayTextures 1\n            -smoothWireframe 0\n            -lineWidth 1\n            -textureAnisotropic 0\n            -textureHilight 1\n            -textureSampling 2\n            -textureDisplay \"modulate\" \n            -textureMaxSize 16384\n            -fogging 0\n            -fogSource \"fragment\" \n            -fogMode \"linear\" \n            -fogStart 0\n            -fogEnd 100\n            -fogDensity 0.1\n            -fogColor 0.5 0.5 0.5 1 \n            -maxConstantTransparency 1\n            -rendererName \"base_OpenGL_Renderer\" \n"
		+ "            -objectFilterShowInHUD 1\n            -isFiltered 0\n            -colorResolution 4 4 \n            -bumpResolution 4 4 \n            -textureCompression 0\n            -transparencyAlgorithm \"frontAndBackCull\" \n            -transpInShadows 0\n            -cullingOverride \"none\" \n            -lowQualityLighting 0\n            -maximumNumHardwareLights 1\n            -occlusionCulling 0\n            -shadingModel 0\n            -useBaseRenderer 0\n            -useReducedRenderer 0\n            -smallObjectCulling 0\n            -smallObjectThreshold -1 \n            -interactiveDisableShadows 0\n            -interactiveBackFaceCull 0\n            -sortTransparent 1\n            -nurbsCurves 1\n            -nurbsSurfaces 1\n            -polymeshes 1\n            -subdivSurfaces 1\n            -planes 1\n            -lights 1\n            -cameras 1\n            -controlVertices 1\n            -hulls 1\n            -grid 1\n            -imagePlane 1\n            -joints 1\n            -ikHandles 1\n            -deformers 1\n            -dynamics 1\n"
		+ "            -fluids 1\n            -hairSystems 1\n            -follicles 1\n            -nCloths 1\n            -nParticles 1\n            -nRigids 1\n            -dynamicConstraints 1\n            -locators 1\n            -manipulators 1\n            -pluginShapes 1\n            -dimensions 1\n            -handles 1\n            -pivots 1\n            -textures 1\n            -strokes 1\n            -motionTrails 1\n            -clipGhosts 1\n            -greasePencils 1\n            -shadows 0\n            $editorName;\n        modelEditor -e -viewSelected 0 $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"outlinerPanel\" (localizedPanelLabel(\"Outliner\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `outlinerPanel -unParent -l (localizedPanelLabel(\"Outliner\")) -mbv $menusOkayInPanels `;\n\t\t\t$editorName = $panelName;\n            outlinerEditor -e \n                -docTag \"isolOutln_fromSeln\" \n                -showShapes 0\n                -showReferenceNodes 0\n"
		+ "                -showReferenceMembers 0\n                -showAttributes 0\n                -showConnected 0\n                -showAnimCurvesOnly 0\n                -showMuteInfo 0\n                -organizeByLayer 1\n                -showAnimLayerWeight 1\n                -autoExpandLayers 1\n                -autoExpand 0\n                -showDagOnly 1\n                -showAssets 1\n                -showContainedOnly 1\n                -showPublishedAsConnected 0\n                -showContainerContents 1\n                -ignoreDagHierarchy 0\n                -expandConnections 0\n                -showUpstreamCurves 1\n                -showUnitlessCurves 1\n                -showCompounds 1\n                -showLeafs 1\n                -showNumericAttrsOnly 0\n                -highlightActive 1\n                -autoSelectNewObjects 0\n                -doNotSelectNewObjects 0\n                -dropIsParent 1\n                -transmitFilters 0\n                -setFilter \"defaultSetFilter\" \n                -showSetMembers 1\n                -allowMultiSelection 1\n"
		+ "                -alwaysToggleSelect 0\n                -directSelect 0\n                -displayMode \"DAG\" \n                -expandObjects 0\n                -setsIgnoreFilters 1\n                -containersIgnoreFilters 0\n                -editAttrName 0\n                -showAttrValues 0\n                -highlightSecondary 0\n                -showUVAttrsOnly 0\n                -showTextureNodesOnly 0\n                -attrAlphaOrder \"default\" \n                -animLayerFilterOptions \"allAffecting\" \n                -sortOrder \"none\" \n                -longNames 0\n                -niceNames 1\n                -showNamespace 1\n                -showPinIcons 0\n                -mapMotionTrails 0\n                $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\toutlinerPanel -edit -l (localizedPanelLabel(\"Outliner\")) -mbv $menusOkayInPanels  $panelName;\n\t\t$editorName = $panelName;\n        outlinerEditor -e \n            -docTag \"isolOutln_fromSeln\" \n            -showShapes 0\n            -showReferenceNodes 0\n"
		+ "            -showReferenceMembers 0\n            -showAttributes 0\n            -showConnected 0\n            -showAnimCurvesOnly 0\n            -showMuteInfo 0\n            -organizeByLayer 1\n            -showAnimLayerWeight 1\n            -autoExpandLayers 1\n            -autoExpand 0\n            -showDagOnly 1\n            -showAssets 1\n            -showContainedOnly 1\n            -showPublishedAsConnected 0\n            -showContainerContents 1\n            -ignoreDagHierarchy 0\n            -expandConnections 0\n            -showUpstreamCurves 1\n            -showUnitlessCurves 1\n            -showCompounds 1\n            -showLeafs 1\n            -showNumericAttrsOnly 0\n            -highlightActive 1\n            -autoSelectNewObjects 0\n            -doNotSelectNewObjects 0\n            -dropIsParent 1\n            -transmitFilters 0\n            -setFilter \"defaultSetFilter\" \n            -showSetMembers 1\n            -allowMultiSelection 1\n            -alwaysToggleSelect 0\n            -directSelect 0\n            -displayMode \"DAG\" \n"
		+ "            -expandObjects 0\n            -setsIgnoreFilters 1\n            -containersIgnoreFilters 0\n            -editAttrName 0\n            -showAttrValues 0\n            -highlightSecondary 0\n            -showUVAttrsOnly 0\n            -showTextureNodesOnly 0\n            -attrAlphaOrder \"default\" \n            -animLayerFilterOptions \"allAffecting\" \n            -sortOrder \"none\" \n            -longNames 0\n            -niceNames 1\n            -showNamespace 1\n            -showPinIcons 0\n            -mapMotionTrails 0\n            $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"graphEditor\" (localizedPanelLabel(\"Graph Editor\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"graphEditor\" -l (localizedPanelLabel(\"Graph Editor\")) -mbv $menusOkayInPanels `;\n\n\t\t\t$editorName = ($panelName+\"OutlineEd\");\n            outlinerEditor -e \n                -showShapes 1\n                -showReferenceNodes 0\n"
		+ "                -showReferenceMembers 0\n                -showAttributes 1\n                -showConnected 1\n                -showAnimCurvesOnly 1\n                -showMuteInfo 0\n                -organizeByLayer 1\n                -showAnimLayerWeight 1\n                -autoExpandLayers 1\n                -autoExpand 1\n                -showDagOnly 0\n                -showAssets 1\n                -showContainedOnly 0\n                -showPublishedAsConnected 0\n                -showContainerContents 0\n                -ignoreDagHierarchy 0\n                -expandConnections 1\n                -showUpstreamCurves 1\n                -showUnitlessCurves 1\n                -showCompounds 0\n                -showLeafs 1\n                -showNumericAttrsOnly 1\n                -highlightActive 0\n                -autoSelectNewObjects 1\n                -doNotSelectNewObjects 0\n                -dropIsParent 1\n                -transmitFilters 1\n                -setFilter \"0\" \n                -showSetMembers 0\n                -allowMultiSelection 1\n"
		+ "                -alwaysToggleSelect 0\n                -directSelect 0\n                -displayMode \"DAG\" \n                -expandObjects 0\n                -setsIgnoreFilters 1\n                -containersIgnoreFilters 0\n                -editAttrName 0\n                -showAttrValues 0\n                -highlightSecondary 0\n                -showUVAttrsOnly 0\n                -showTextureNodesOnly 0\n                -attrAlphaOrder \"default\" \n                -animLayerFilterOptions \"allAffecting\" \n                -sortOrder \"none\" \n                -longNames 0\n                -niceNames 1\n                -showNamespace 1\n                -showPinIcons 1\n                -mapMotionTrails 1\n                $editorName;\n\n\t\t\t$editorName = ($panelName+\"GraphEd\");\n            animCurveEditor -e \n                -displayKeys 1\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 1\n                -displayInfinities 0\n                -autoFit 0\n                -snapTime \"integer\" \n"
		+ "                -snapValue \"none\" \n                -showResults \"off\" \n                -showBufferCurves \"off\" \n                -smoothness \"fine\" \n                -resultSamples 1\n                -resultScreenSamples 0\n                -resultUpdate \"delayed\" \n                -showUpstreamCurves 1\n                -stackedCurves 0\n                -stackedCurvesMin -1\n                -stackedCurvesMax 1\n                -stackedCurvesSpace 0.2\n                -displayNormalized 0\n                -preSelectionHighlight 0\n                -constrainDrag 0\n                -classicMode 1\n                $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Graph Editor\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = ($panelName+\"OutlineEd\");\n            outlinerEditor -e \n                -showShapes 1\n                -showReferenceNodes 0\n                -showReferenceMembers 0\n                -showAttributes 1\n                -showConnected 1\n                -showAnimCurvesOnly 1\n"
		+ "                -showMuteInfo 0\n                -organizeByLayer 1\n                -showAnimLayerWeight 1\n                -autoExpandLayers 1\n                -autoExpand 1\n                -showDagOnly 0\n                -showAssets 1\n                -showContainedOnly 0\n                -showPublishedAsConnected 0\n                -showContainerContents 0\n                -ignoreDagHierarchy 0\n                -expandConnections 1\n                -showUpstreamCurves 1\n                -showUnitlessCurves 1\n                -showCompounds 0\n                -showLeafs 1\n                -showNumericAttrsOnly 1\n                -highlightActive 0\n                -autoSelectNewObjects 1\n                -doNotSelectNewObjects 0\n                -dropIsParent 1\n                -transmitFilters 1\n                -setFilter \"0\" \n                -showSetMembers 0\n                -allowMultiSelection 1\n                -alwaysToggleSelect 0\n                -directSelect 0\n                -displayMode \"DAG\" \n                -expandObjects 0\n"
		+ "                -setsIgnoreFilters 1\n                -containersIgnoreFilters 0\n                -editAttrName 0\n                -showAttrValues 0\n                -highlightSecondary 0\n                -showUVAttrsOnly 0\n                -showTextureNodesOnly 0\n                -attrAlphaOrder \"default\" \n                -animLayerFilterOptions \"allAffecting\" \n                -sortOrder \"none\" \n                -longNames 0\n                -niceNames 1\n                -showNamespace 1\n                -showPinIcons 1\n                -mapMotionTrails 1\n                $editorName;\n\n\t\t\t$editorName = ($panelName+\"GraphEd\");\n            animCurveEditor -e \n                -displayKeys 1\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 1\n                -displayInfinities 0\n                -autoFit 0\n                -snapTime \"integer\" \n                -snapValue \"none\" \n                -showResults \"off\" \n                -showBufferCurves \"off\" \n                -smoothness \"fine\" \n"
		+ "                -resultSamples 1\n                -resultScreenSamples 0\n                -resultUpdate \"delayed\" \n                -showUpstreamCurves 1\n                -stackedCurves 0\n                -stackedCurvesMin -1\n                -stackedCurvesMax 1\n                -stackedCurvesSpace 0.2\n                -displayNormalized 0\n                -preSelectionHighlight 0\n                -constrainDrag 0\n                -classicMode 1\n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"dopeSheetPanel\" (localizedPanelLabel(\"Dope Sheet\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"dopeSheetPanel\" -l (localizedPanelLabel(\"Dope Sheet\")) -mbv $menusOkayInPanels `;\n\n\t\t\t$editorName = ($panelName+\"OutlineEd\");\n            outlinerEditor -e \n                -showShapes 1\n                -showReferenceNodes 0\n                -showReferenceMembers 0\n                -showAttributes 1\n"
		+ "                -showConnected 1\n                -showAnimCurvesOnly 1\n                -showMuteInfo 0\n                -organizeByLayer 1\n                -showAnimLayerWeight 1\n                -autoExpandLayers 1\n                -autoExpand 0\n                -showDagOnly 0\n                -showAssets 1\n                -showContainedOnly 0\n                -showPublishedAsConnected 0\n                -showContainerContents 0\n                -ignoreDagHierarchy 0\n                -expandConnections 1\n                -showUpstreamCurves 1\n                -showUnitlessCurves 0\n                -showCompounds 1\n                -showLeafs 1\n                -showNumericAttrsOnly 1\n                -highlightActive 0\n                -autoSelectNewObjects 0\n                -doNotSelectNewObjects 1\n                -dropIsParent 1\n                -transmitFilters 0\n                -setFilter \"0\" \n                -showSetMembers 0\n                -allowMultiSelection 1\n                -alwaysToggleSelect 0\n                -directSelect 0\n"
		+ "                -displayMode \"DAG\" \n                -expandObjects 0\n                -setsIgnoreFilters 1\n                -containersIgnoreFilters 0\n                -editAttrName 0\n                -showAttrValues 0\n                -highlightSecondary 0\n                -showUVAttrsOnly 0\n                -showTextureNodesOnly 0\n                -attrAlphaOrder \"default\" \n                -animLayerFilterOptions \"allAffecting\" \n                -sortOrder \"none\" \n                -longNames 0\n                -niceNames 1\n                -showNamespace 1\n                -showPinIcons 0\n                -mapMotionTrails 1\n                $editorName;\n\n\t\t\t$editorName = ($panelName+\"DopeSheetEd\");\n            dopeSheetEditor -e \n                -displayKeys 1\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 0\n                -displayInfinities 0\n                -autoFit 0\n                -snapTime \"integer\" \n                -snapValue \"none\" \n                -outliner \"dopeSheetPanel1OutlineEd\" \n"
		+ "                -showSummary 1\n                -showScene 0\n                -hierarchyBelow 0\n                -showTicks 1\n                -selectionWindow 0 0 0 0 \n                $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Dope Sheet\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = ($panelName+\"OutlineEd\");\n            outlinerEditor -e \n                -showShapes 1\n                -showReferenceNodes 0\n                -showReferenceMembers 0\n                -showAttributes 1\n                -showConnected 1\n                -showAnimCurvesOnly 1\n                -showMuteInfo 0\n                -organizeByLayer 1\n                -showAnimLayerWeight 1\n                -autoExpandLayers 1\n                -autoExpand 0\n                -showDagOnly 0\n                -showAssets 1\n                -showContainedOnly 0\n                -showPublishedAsConnected 0\n                -showContainerContents 0\n                -ignoreDagHierarchy 0\n"
		+ "                -expandConnections 1\n                -showUpstreamCurves 1\n                -showUnitlessCurves 0\n                -showCompounds 1\n                -showLeafs 1\n                -showNumericAttrsOnly 1\n                -highlightActive 0\n                -autoSelectNewObjects 0\n                -doNotSelectNewObjects 1\n                -dropIsParent 1\n                -transmitFilters 0\n                -setFilter \"0\" \n                -showSetMembers 0\n                -allowMultiSelection 1\n                -alwaysToggleSelect 0\n                -directSelect 0\n                -displayMode \"DAG\" \n                -expandObjects 0\n                -setsIgnoreFilters 1\n                -containersIgnoreFilters 0\n                -editAttrName 0\n                -showAttrValues 0\n                -highlightSecondary 0\n                -showUVAttrsOnly 0\n                -showTextureNodesOnly 0\n                -attrAlphaOrder \"default\" \n                -animLayerFilterOptions \"allAffecting\" \n                -sortOrder \"none\" \n"
		+ "                -longNames 0\n                -niceNames 1\n                -showNamespace 1\n                -showPinIcons 0\n                -mapMotionTrails 1\n                $editorName;\n\n\t\t\t$editorName = ($panelName+\"DopeSheetEd\");\n            dopeSheetEditor -e \n                -displayKeys 1\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 0\n                -displayInfinities 0\n                -autoFit 0\n                -snapTime \"integer\" \n                -snapValue \"none\" \n                -outliner \"dopeSheetPanel1OutlineEd\" \n                -showSummary 1\n                -showScene 0\n                -hierarchyBelow 0\n                -showTicks 1\n                -selectionWindow 0 0 0 0 \n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"clipEditorPanel\" (localizedPanelLabel(\"Trax Editor\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n"
		+ "\t\t\t$panelName = `scriptedPanel -unParent  -type \"clipEditorPanel\" -l (localizedPanelLabel(\"Trax Editor\")) -mbv $menusOkayInPanels `;\n\n\t\t\t$editorName = clipEditorNameFromPanel($panelName);\n            clipEditor -e \n                -displayKeys 0\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 0\n                -displayInfinities 0\n                -autoFit 0\n                -snapTime \"none\" \n                -snapValue \"none\" \n                -manageSequencer 0 \n                $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Trax Editor\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = clipEditorNameFromPanel($panelName);\n            clipEditor -e \n                -displayKeys 0\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 0\n                -displayInfinities 0\n                -autoFit 0\n                -snapTime \"none\" \n"
		+ "                -snapValue \"none\" \n                -manageSequencer 0 \n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"sequenceEditorPanel\" (localizedPanelLabel(\"Camera Sequencer\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"sequenceEditorPanel\" -l (localizedPanelLabel(\"Camera Sequencer\")) -mbv $menusOkayInPanels `;\n\n\t\t\t$editorName = sequenceEditorNameFromPanel($panelName);\n            clipEditor -e \n                -displayKeys 0\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 0\n                -displayInfinities 0\n                -autoFit 0\n                -snapTime \"none\" \n                -snapValue \"none\" \n                -manageSequencer 1 \n                $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Camera Sequencer\")) -mbv $menusOkayInPanels  $panelName;\n"
		+ "\t\t\t$editorName = sequenceEditorNameFromPanel($panelName);\n            clipEditor -e \n                -displayKeys 0\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 0\n                -displayInfinities 0\n                -autoFit 0\n                -snapTime \"none\" \n                -snapValue \"none\" \n                -manageSequencer 1 \n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"hyperGraphPanel\" (localizedPanelLabel(\"Hypergraph Hierarchy\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"hyperGraphPanel\" -l (localizedPanelLabel(\"Hypergraph Hierarchy\")) -mbv $menusOkayInPanels `;\n\n\t\t\t$editorName = ($panelName+\"HyperGraphEd\");\n            hyperGraph -e \n                -graphLayoutStyle \"hierarchicalLayout\" \n                -orientation \"horiz\" \n                -mergeConnections 1\n                -zoom 1\n"
		+ "                -animateTransition 0\n                -showRelationships 1\n                -showShapes 0\n                -showDeformers 0\n                -showExpressions 0\n                -showConstraints 0\n                -showConnectionFromSelected 0\n                -showConnectionToSelected 0\n                -showUnderworld 0\n                -showInvisible 0\n                -transitionFrames 1\n                -opaqueContainers 0\n                -freeform 0\n                -imagePosition 0 0 \n                -imageScale 1\n                -imageEnabled 0\n                -graphType \"DAG\" \n                -heatMapDisplay 0\n                -updateSelection 1\n                -updateNodeAdded 1\n                -useDrawOverrideColor 0\n                -limitGraphTraversal -1\n                -range 0 0 \n                -iconSize \"smallIcons\" \n                -showCachedConnections 0\n                $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Hypergraph Hierarchy\")) -mbv $menusOkayInPanels  $panelName;\n"
		+ "\t\t\t$editorName = ($panelName+\"HyperGraphEd\");\n            hyperGraph -e \n                -graphLayoutStyle \"hierarchicalLayout\" \n                -orientation \"horiz\" \n                -mergeConnections 1\n                -zoom 1\n                -animateTransition 0\n                -showRelationships 1\n                -showShapes 0\n                -showDeformers 0\n                -showExpressions 0\n                -showConstraints 0\n                -showConnectionFromSelected 0\n                -showConnectionToSelected 0\n                -showUnderworld 0\n                -showInvisible 0\n                -transitionFrames 1\n                -opaqueContainers 0\n                -freeform 0\n                -imagePosition 0 0 \n                -imageScale 1\n                -imageEnabled 0\n                -graphType \"DAG\" \n                -heatMapDisplay 0\n                -updateSelection 1\n                -updateNodeAdded 1\n                -useDrawOverrideColor 0\n                -limitGraphTraversal -1\n                -range 0 0 \n"
		+ "                -iconSize \"smallIcons\" \n                -showCachedConnections 0\n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"hyperShadePanel\" (localizedPanelLabel(\"Hypershade\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"hyperShadePanel\" -l (localizedPanelLabel(\"Hypershade\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Hypershade\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"visorPanel\" (localizedPanelLabel(\"Visor\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"visorPanel\" -l (localizedPanelLabel(\"Visor\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n"
		+ "\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Visor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"nodeEditorPanel\" (localizedPanelLabel(\"Node Editor\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"nodeEditorPanel\" -l (localizedPanelLabel(\"Node Editor\")) -mbv $menusOkayInPanels `;\n\n\t\t\t$editorName = ($panelName+\"NodeEditorEd\");\n            nodeEditor -e \n                -allAttributes 0\n                -allNodes 0\n                -autoSizeNodes 1\n                -createNodeCommand \"nodeEdCreateNodeCommand\" \n                -defaultPinnedState 0\n                -ignoreAssets 1\n                -additiveGraphingMode 0\n                -settingsChangedCallback \"nodeEdSyncControls\" \n                -traversalDepthLimit -1\n                -keyPressCommand \"nodeEdKeyPressCommand\" \n                -keyReleaseCommand \"nodeEdKeyReleaseCommand\" \n                -nodeTitleMode \"name\" \n"
		+ "                -gridSnap 0\n                -gridVisibility 1\n                -popupMenuScript \"nodeEdBuildPanelMenus\" \n                -island 0\n                -showNamespace 1\n                -showShapes 1\n                -showSGShapes 0\n                -showTransforms 1\n                -syncedSelection 1\n                -extendToShapes 1\n                $editorName;;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Node Editor\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = ($panelName+\"NodeEditorEd\");\n            nodeEditor -e \n                -allAttributes 0\n                -allNodes 0\n                -autoSizeNodes 1\n                -createNodeCommand \"nodeEdCreateNodeCommand\" \n                -defaultPinnedState 0\n                -ignoreAssets 1\n                -additiveGraphingMode 0\n                -settingsChangedCallback \"nodeEdSyncControls\" \n                -traversalDepthLimit -1\n                -keyPressCommand \"nodeEdKeyPressCommand\" \n"
		+ "                -keyReleaseCommand \"nodeEdKeyReleaseCommand\" \n                -nodeTitleMode \"name\" \n                -gridSnap 0\n                -gridVisibility 1\n                -popupMenuScript \"nodeEdBuildPanelMenus\" \n                -island 0\n                -showNamespace 1\n                -showShapes 1\n                -showSGShapes 0\n                -showTransforms 1\n                -syncedSelection 1\n                -extendToShapes 1\n                $editorName;;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"createNodePanel\" (localizedPanelLabel(\"Create Node\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"createNodePanel\" -l (localizedPanelLabel(\"Create Node\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Create Node\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n"
		+ "\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"polyTexturePlacementPanel\" (localizedPanelLabel(\"UV Texture Editor\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"polyTexturePlacementPanel\" -l (localizedPanelLabel(\"UV Texture Editor\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"UV Texture Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"renderWindowPanel\" (localizedPanelLabel(\"Render View\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"renderWindowPanel\" -l (localizedPanelLabel(\"Render View\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Render View\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n"
		+ "\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"blendShapePanel\" (localizedPanelLabel(\"Blend Shape\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\tblendShapePanel -unParent -l (localizedPanelLabel(\"Blend Shape\")) -mbv $menusOkayInPanels ;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tblendShapePanel -edit -l (localizedPanelLabel(\"Blend Shape\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"dynRelEdPanel\" (localizedPanelLabel(\"Dynamic Relationships\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"dynRelEdPanel\" -l (localizedPanelLabel(\"Dynamic Relationships\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Dynamic Relationships\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n"
		+ "\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"relationshipPanel\" (localizedPanelLabel(\"Relationship Editor\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"relationshipPanel\" -l (localizedPanelLabel(\"Relationship Editor\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Relationship Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"referenceEditorPanel\" (localizedPanelLabel(\"Reference Editor\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"referenceEditorPanel\" -l (localizedPanelLabel(\"Reference Editor\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Reference Editor\")) -mbv $menusOkayInPanels  $panelName;\n"
		+ "\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"componentEditorPanel\" (localizedPanelLabel(\"Component Editor\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"componentEditorPanel\" -l (localizedPanelLabel(\"Component Editor\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Component Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"dynPaintScriptedPanelType\" (localizedPanelLabel(\"Paint Effects\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"dynPaintScriptedPanelType\" -l (localizedPanelLabel(\"Paint Effects\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Paint Effects\")) -mbv $menusOkayInPanels  $panelName;\n"
		+ "\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"scriptEditorPanel\" (localizedPanelLabel(\"Script Editor\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"scriptEditorPanel\" -l (localizedPanelLabel(\"Script Editor\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Script Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\tif ($useSceneConfig) {\n        string $configName = `getPanel -cwl (localizedPanelLabel(\"Current Layout\"))`;\n        if (\"\" != $configName) {\n\t\t\tpanelConfiguration -edit -label (localizedPanelLabel(\"Current Layout\")) \n\t\t\t\t-defaultImage \"\"\n\t\t\t\t-image \"\"\n\t\t\t\t-sc false\n\t\t\t\t-configString \"global string $gMainPane; paneLayout -e -cn \\\"vertical2\\\" -ps 1 20 100 -ps 2 80 100 $gMainPane;\"\n\t\t\t\t-removeAllPanels\n\t\t\t\t-ap false\n\t\t\t\t\t(localizedPanelLabel(\"Outliner\")) \n"
		+ "\t\t\t\t\t\"outlinerPanel\"\n\t\t\t\t\t\"$panelName = `outlinerPanel -unParent -l (localizedPanelLabel(\\\"Outliner\\\")) -mbv $menusOkayInPanels `;\\n$editorName = $panelName;\\noutlinerEditor -e \\n    -docTag \\\"isolOutln_fromSeln\\\" \\n    -showShapes 0\\n    -showReferenceNodes 0\\n    -showReferenceMembers 0\\n    -showAttributes 0\\n    -showConnected 0\\n    -showAnimCurvesOnly 0\\n    -showMuteInfo 0\\n    -organizeByLayer 1\\n    -showAnimLayerWeight 1\\n    -autoExpandLayers 1\\n    -autoExpand 0\\n    -showDagOnly 1\\n    -showAssets 1\\n    -showContainedOnly 1\\n    -showPublishedAsConnected 0\\n    -showContainerContents 1\\n    -ignoreDagHierarchy 0\\n    -expandConnections 0\\n    -showUpstreamCurves 1\\n    -showUnitlessCurves 1\\n    -showCompounds 1\\n    -showLeafs 1\\n    -showNumericAttrsOnly 0\\n    -highlightActive 1\\n    -autoSelectNewObjects 0\\n    -doNotSelectNewObjects 0\\n    -dropIsParent 1\\n    -transmitFilters 0\\n    -setFilter \\\"defaultSetFilter\\\" \\n    -showSetMembers 1\\n    -allowMultiSelection 1\\n    -alwaysToggleSelect 0\\n    -directSelect 0\\n    -displayMode \\\"DAG\\\" \\n    -expandObjects 0\\n    -setsIgnoreFilters 1\\n    -containersIgnoreFilters 0\\n    -editAttrName 0\\n    -showAttrValues 0\\n    -highlightSecondary 0\\n    -showUVAttrsOnly 0\\n    -showTextureNodesOnly 0\\n    -attrAlphaOrder \\\"default\\\" \\n    -animLayerFilterOptions \\\"allAffecting\\\" \\n    -sortOrder \\\"none\\\" \\n    -longNames 0\\n    -niceNames 1\\n    -showNamespace 1\\n    -showPinIcons 0\\n    -mapMotionTrails 0\\n    $editorName\"\n"
		+ "\t\t\t\t\t\"outlinerPanel -edit -l (localizedPanelLabel(\\\"Outliner\\\")) -mbv $menusOkayInPanels  $panelName;\\n$editorName = $panelName;\\noutlinerEditor -e \\n    -docTag \\\"isolOutln_fromSeln\\\" \\n    -showShapes 0\\n    -showReferenceNodes 0\\n    -showReferenceMembers 0\\n    -showAttributes 0\\n    -showConnected 0\\n    -showAnimCurvesOnly 0\\n    -showMuteInfo 0\\n    -organizeByLayer 1\\n    -showAnimLayerWeight 1\\n    -autoExpandLayers 1\\n    -autoExpand 0\\n    -showDagOnly 1\\n    -showAssets 1\\n    -showContainedOnly 1\\n    -showPublishedAsConnected 0\\n    -showContainerContents 1\\n    -ignoreDagHierarchy 0\\n    -expandConnections 0\\n    -showUpstreamCurves 1\\n    -showUnitlessCurves 1\\n    -showCompounds 1\\n    -showLeafs 1\\n    -showNumericAttrsOnly 0\\n    -highlightActive 1\\n    -autoSelectNewObjects 0\\n    -doNotSelectNewObjects 0\\n    -dropIsParent 1\\n    -transmitFilters 0\\n    -setFilter \\\"defaultSetFilter\\\" \\n    -showSetMembers 1\\n    -allowMultiSelection 1\\n    -alwaysToggleSelect 0\\n    -directSelect 0\\n    -displayMode \\\"DAG\\\" \\n    -expandObjects 0\\n    -setsIgnoreFilters 1\\n    -containersIgnoreFilters 0\\n    -editAttrName 0\\n    -showAttrValues 0\\n    -highlightSecondary 0\\n    -showUVAttrsOnly 0\\n    -showTextureNodesOnly 0\\n    -attrAlphaOrder \\\"default\\\" \\n    -animLayerFilterOptions \\\"allAffecting\\\" \\n    -sortOrder \\\"none\\\" \\n    -longNames 0\\n    -niceNames 1\\n    -showNamespace 1\\n    -showPinIcons 0\\n    -mapMotionTrails 0\\n    $editorName\"\n"
		+ "\t\t\t\t-ap false\n\t\t\t\t\t(localizedPanelLabel(\"Persp View\")) \n\t\t\t\t\t\"modelPanel\"\n"
		+ "\t\t\t\t\t\"$panelName = `modelPanel -unParent -l (localizedPanelLabel(\\\"Persp View\\\")) -mbv $menusOkayInPanels `;\\n$editorName = $panelName;\\nmodelEditor -e \\n    -cam `findStartUpCamera persp` \\n    -useInteractiveMode 0\\n    -displayLights \\\"default\\\" \\n    -displayAppearance \\\"smoothShaded\\\" \\n    -activeOnly 0\\n    -ignorePanZoom 0\\n    -wireframeOnShaded 0\\n    -headsUpDisplay 1\\n    -selectionHiliteDisplay 1\\n    -useDefaultMaterial 0\\n    -bufferMode \\\"double\\\" \\n    -twoSidedLighting 1\\n    -backfaceCulling 0\\n    -xray 0\\n    -jointXray 1\\n    -activeComponentsXray 0\\n    -displayTextures 1\\n    -smoothWireframe 0\\n    -lineWidth 1\\n    -textureAnisotropic 0\\n    -textureHilight 1\\n    -textureSampling 2\\n    -textureDisplay \\\"modulate\\\" \\n    -textureMaxSize 16384\\n    -fogging 0\\n    -fogSource \\\"fragment\\\" \\n    -fogMode \\\"linear\\\" \\n    -fogStart 0\\n    -fogEnd 100\\n    -fogDensity 0.1\\n    -fogColor 0.5 0.5 0.5 1 \\n    -maxConstantTransparency 1\\n    -rendererName \\\"base_OpenGL_Renderer\\\" \\n    -objectFilterShowInHUD 1\\n    -isFiltered 0\\n    -colorResolution 4 4 \\n    -bumpResolution 4 4 \\n    -textureCompression 0\\n    -transparencyAlgorithm \\\"frontAndBackCull\\\" \\n    -transpInShadows 0\\n    -cullingOverride \\\"none\\\" \\n    -lowQualityLighting 0\\n    -maximumNumHardwareLights 1\\n    -occlusionCulling 0\\n    -shadingModel 0\\n    -useBaseRenderer 0\\n    -useReducedRenderer 0\\n    -smallObjectCulling 0\\n    -smallObjectThreshold -1 \\n    -interactiveDisableShadows 0\\n    -interactiveBackFaceCull 0\\n    -sortTransparent 1\\n    -nurbsCurves 1\\n    -nurbsSurfaces 1\\n    -polymeshes 1\\n    -subdivSurfaces 1\\n    -planes 1\\n    -lights 1\\n    -cameras 1\\n    -controlVertices 1\\n    -hulls 1\\n    -grid 1\\n    -imagePlane 1\\n    -joints 1\\n    -ikHandles 1\\n    -deformers 1\\n    -dynamics 1\\n    -fluids 1\\n    -hairSystems 1\\n    -follicles 1\\n    -nCloths 1\\n    -nParticles 1\\n    -nRigids 1\\n    -dynamicConstraints 1\\n    -locators 1\\n    -manipulators 1\\n    -pluginShapes 1\\n    -dimensions 1\\n    -handles 1\\n    -pivots 1\\n    -textures 1\\n    -strokes 1\\n    -motionTrails 1\\n    -clipGhosts 1\\n    -greasePencils 1\\n    -shadows 0\\n    $editorName;\\nmodelEditor -e -viewSelected 0 $editorName\"\n"
		+ "\t\t\t\t\t\"modelPanel -edit -l (localizedPanelLabel(\\\"Persp View\\\")) -mbv $menusOkayInPanels  $panelName;\\n$editorName = $panelName;\\nmodelEditor -e \\n    -cam `findStartUpCamera persp` \\n    -useInteractiveMode 0\\n    -displayLights \\\"default\\\" \\n    -displayAppearance \\\"smoothShaded\\\" \\n    -activeOnly 0\\n    -ignorePanZoom 0\\n    -wireframeOnShaded 0\\n    -headsUpDisplay 1\\n    -selectionHiliteDisplay 1\\n    -useDefaultMaterial 0\\n    -bufferMode \\\"double\\\" \\n    -twoSidedLighting 1\\n    -backfaceCulling 0\\n    -xray 0\\n    -jointXray 1\\n    -activeComponentsXray 0\\n    -displayTextures 1\\n    -smoothWireframe 0\\n    -lineWidth 1\\n    -textureAnisotropic 0\\n    -textureHilight 1\\n    -textureSampling 2\\n    -textureDisplay \\\"modulate\\\" \\n    -textureMaxSize 16384\\n    -fogging 0\\n    -fogSource \\\"fragment\\\" \\n    -fogMode \\\"linear\\\" \\n    -fogStart 0\\n    -fogEnd 100\\n    -fogDensity 0.1\\n    -fogColor 0.5 0.5 0.5 1 \\n    -maxConstantTransparency 1\\n    -rendererName \\\"base_OpenGL_Renderer\\\" \\n    -objectFilterShowInHUD 1\\n    -isFiltered 0\\n    -colorResolution 4 4 \\n    -bumpResolution 4 4 \\n    -textureCompression 0\\n    -transparencyAlgorithm \\\"frontAndBackCull\\\" \\n    -transpInShadows 0\\n    -cullingOverride \\\"none\\\" \\n    -lowQualityLighting 0\\n    -maximumNumHardwareLights 1\\n    -occlusionCulling 0\\n    -shadingModel 0\\n    -useBaseRenderer 0\\n    -useReducedRenderer 0\\n    -smallObjectCulling 0\\n    -smallObjectThreshold -1 \\n    -interactiveDisableShadows 0\\n    -interactiveBackFaceCull 0\\n    -sortTransparent 1\\n    -nurbsCurves 1\\n    -nurbsSurfaces 1\\n    -polymeshes 1\\n    -subdivSurfaces 1\\n    -planes 1\\n    -lights 1\\n    -cameras 1\\n    -controlVertices 1\\n    -hulls 1\\n    -grid 1\\n    -imagePlane 1\\n    -joints 1\\n    -ikHandles 1\\n    -deformers 1\\n    -dynamics 1\\n    -fluids 1\\n    -hairSystems 1\\n    -follicles 1\\n    -nCloths 1\\n    -nParticles 1\\n    -nRigids 1\\n    -dynamicConstraints 1\\n    -locators 1\\n    -manipulators 1\\n    -pluginShapes 1\\n    -dimensions 1\\n    -handles 1\\n    -pivots 1\\n    -textures 1\\n    -strokes 1\\n    -motionTrails 1\\n    -clipGhosts 1\\n    -greasePencils 1\\n    -shadows 0\\n    $editorName;\\nmodelEditor -e -viewSelected 0 $editorName\"\n"
		+ "\t\t\t\t$configName;\n\n            setNamedPanelLayout (localizedPanelLabel(\"Current Layout\"));\n        }\n\n        panelHistory -e -clear mainPanelHistory;\n        setFocus `paneLayout -q -p1 $gMainPane`;\n        sceneUIReplacement -deleteRemaining;\n        sceneUIReplacement -clear;\n\t}\n\n\ngrid -spacing 5 -size 12 -divisions 5 -displayAxes yes -displayGridLines yes -displayDivisionLines yes -displayPerspectiveLabels no -displayOrthographicLabels no -displayAxesBold yes -perspectiveLabelPosition axis -orthographicLabelPosition edge;\nviewManip -drawCompass 0 -compassAngle 0 -frontParameters \"\" -homeParameters \"\" -selectionLockParameters \"\";\n}\n");
	setAttr ".st" 3;
createNode script -n "idle_v1:sceneConfigurationScriptNode";
	setAttr ".b" -type "string" "playbackOptions -min 0 -max 100 -ast 0 -aet 6000 ";
	setAttr ".st" 6;
createNode phong -n "idle_v1_business05_f_highpolyPhong";
	setAttr ".sc" -type "float3" 0 0 0 ;
createNode shadingEngine -n "idle_v1_business05_f_highpolySG";
	setAttr ".ihi" 0;
	setAttr ".ro" yes;
createNode materialInfo -n "idle_v1_materialInfo1";
createNode shadingEngine -n "idle_v1_business05_f_highpolySG1";
	setAttr ".ihi" 0;
	setAttr ".ro" yes;
createNode materialInfo -n "idle_v1_materialInfo2";
createNode file -n "idle_v1_Map1";
	setAttr ".ftn" -type "string" "C:\\Users\\Kuan\\Desktop\\iPi Record 20150227\\business05_f\\business05_f_35.tga";
createNode place2dTexture -n "idle_v1_place2dTexture1";
createNode phong -n "idle_v1_business05_f_highpoly_hairPhong";
	setAttr ".c" -type "float3" 0 0 0 ;
	setAttr ".sc" -type "float3" 0 0 0 ;
createNode shadingEngine -n "idle_v1_business05_f_highpoly_hairSG";
	setAttr ".ihi" 0;
	setAttr ".ro" yes;
createNode materialInfo -n "idle_v1_materialInfo3";
createNode shadingEngine -n "idle_v1_business05_f_highpoly_hairSG1";
	setAttr ".ihi" 0;
	setAttr ".ro" yes;
createNode materialInfo -n "idle_v1_materialInfo4";
createNode phong -n "idle_v1_business05_f_highpolyPhong1";
	setAttr ".sc" -type "float3" 0 0 0 ;
createNode shadingEngine -n "idle_v1_business05_f_highpolySG2";
	setAttr ".ihi" 0;
	setAttr ".ro" yes;
createNode materialInfo -n "idle_v1_materialInfo5";
createNode shadingEngine -n "idle_v1_business05_f_highpolySG3";
	setAttr ".ihi" 0;
	setAttr ".ro" yes;
createNode materialInfo -n "idle_v1_materialInfo6";
createNode file -n "idle_v1_Map2";
	setAttr ".ftn" -type "string" "C:\\Users\\Kuan\\Desktop\\iPi Record 20150227\\business05_f\\business05_f_35.tga";
createNode place2dTexture -n "idle_v1_place2dTexture2";
createNode phong -n "idle_v1_business05_f_highpoly_hairPhong1";
	setAttr ".c" -type "float3" 0 0 0 ;
	setAttr ".sc" -type "float3" 0 0 0 ;
createNode shadingEngine -n "idle_v1_business05_f_highpoly_hairSG2";
	setAttr ".ihi" 0;
	setAttr ".ro" yes;
createNode materialInfo -n "idle_v1_materialInfo7";
createNode shadingEngine -n "idle_v1_business05_f_highpoly_hairSG3";
	setAttr ".ihi" 0;
	setAttr ".ro" yes;
createNode materialInfo -n "idle_v1_materialInfo8";
createNode phong -n "stand_v1_business05_f_highpolyPhong";
	setAttr ".sc" -type "float3" 0 0 0 ;
createNode shadingEngine -n "stand_v1_business05_f_highpolySG";
	setAttr ".ihi" 0;
	setAttr ".ro" yes;
createNode materialInfo -n "stand_v1_materialInfo1";
createNode shadingEngine -n "stand_v1_business05_f_highpolySG1";
	setAttr ".ihi" 0;
	setAttr ".ro" yes;
createNode materialInfo -n "stand_v1_materialInfo2";
createNode file -n "stand_v1_Map1";
	setAttr ".ftn" -type "string" "C:\\Users\\Kuan\\Desktop\\iPi Record 20150227\\business05_f\\business05_f_35.tga";
createNode place2dTexture -n "stand_v1_place2dTexture1";
createNode phong -n "stand_v1_business05_f_highpoly_hairPhong";
	setAttr ".c" -type "float3" 0 0 0 ;
	setAttr ".sc" -type "float3" 0 0 0 ;
createNode shadingEngine -n "stand_v1_business05_f_highpoly_hairSG";
	setAttr ".ihi" 0;
	setAttr ".ro" yes;
createNode materialInfo -n "stand_v1_materialInfo3";
createNode shadingEngine -n "stand_v1_business05_f_highpoly_hairSG1";
	setAttr ".ihi" 0;
	setAttr ".ro" yes;
createNode materialInfo -n "stand_v1_materialInfo4";
createNode phong -n "sit_idle_v1_business05_f_highpolyPhong";
	setAttr ".sc" -type "float3" 0 0 0 ;
createNode shadingEngine -n "sit_idle_v1_business05_f_highpolySG";
	setAttr ".ihi" 0;
	setAttr ".ro" yes;
createNode materialInfo -n "sit_idle_v1_materialInfo1";
createNode shadingEngine -n "sit_idle_v1_business05_f_highpolySG1";
	setAttr ".ihi" 0;
	setAttr ".ro" yes;
createNode materialInfo -n "sit_idle_v1_materialInfo2";
createNode file -n "sit_idle_v1_Map1";
	setAttr ".ftn" -type "string" "C:\\Users\\Kuan\\Desktop\\iPi Record 20150227\\business05_f\\business05_f_35.tga";
createNode place2dTexture -n "sit_idle_v1_place2dTexture1";
createNode phong -n "sit_idle_v1_business05_f_highpoly_hairPhong";
	setAttr ".c" -type "float3" 0 0 0 ;
	setAttr ".sc" -type "float3" 0 0 0 ;
createNode shadingEngine -n "sit_idle_v1_business05_f_highpoly_hairSG";
	setAttr ".ihi" 0;
	setAttr ".ro" yes;
createNode materialInfo -n "sit_idle_v1_materialInfo3";
createNode shadingEngine -n "sit_idle_v1_business05_f_highpoly_hairSG1";
	setAttr ".ihi" 0;
	setAttr ".ro" yes;
createNode materialInfo -n "sit_idle_v1_materialInfo4";
createNode phong -n "sit_talk_v1_business05_f_highpolyPhong";
	setAttr ".sc" -type "float3" 0 0 0 ;
createNode shadingEngine -n "sit_talk_v1_business05_f_highpolySG";
	setAttr ".ihi" 0;
	setAttr ".ro" yes;
createNode materialInfo -n "sit_talk_v1_materialInfo1";
createNode shadingEngine -n "sit_talk_v1_business05_f_highpolySG1";
	setAttr ".ihi" 0;
	setAttr ".ro" yes;
createNode materialInfo -n "sit_talk_v1_materialInfo2";
createNode file -n "sit_talk_v1_Map1";
	setAttr ".ftn" -type "string" "C:\\Users\\Kuan\\Desktop\\iPi Record 20150227\\business05_f\\business05_f_35.tga";
createNode place2dTexture -n "sit_talk_v1_place2dTexture1";
createNode phong -n "sit_talk_v1_business05_f_highpoly_hairPhong";
	setAttr ".c" -type "float3" 0 0 0 ;
	setAttr ".sc" -type "float3" 0 0 0 ;
createNode shadingEngine -n "sit_talk_v1_business05_f_highpoly_hairSG";
	setAttr ".ihi" 0;
	setAttr ".ro" yes;
createNode materialInfo -n "sit_talk_v1_materialInfo3";
createNode shadingEngine -n "sit_talk_v1_business05_f_highpoly_hairSG1";
	setAttr ".ihi" 0;
	setAttr ".ro" yes;
createNode materialInfo -n "sit_talk_v1_materialInfo4";
createNode mentalrayItemsList -s -n "mentalrayItemsList";
createNode mentalrayGlobals -s -n "mentalrayGlobals";
createNode mentalrayOptions -s -n "miDefaultOptions";
	addAttr -ci true -m -sn "stringOptions" -ln "stringOptions" -at "compound" -nc 
		3;
	addAttr -ci true -sn "name" -ln "name" -dt "string" -p "stringOptions";
	addAttr -ci true -sn "value" -ln "value" -dt "string" -p "stringOptions";
	addAttr -ci true -sn "type" -ln "type" -dt "string" -p "stringOptions";
	addAttr -ci true -sn "miSamplesQualityR" -ln "miSamplesQualityR" -dv 0.25 -min 0.01 
		-max 9999999.9000000004 -smn 0.1 -smx 2 -at "double";
	addAttr -ci true -sn "miSamplesQualityG" -ln "miSamplesQualityG" -dv 0.25 -min 0.01 
		-max 9999999.9000000004 -smn 0.1 -smx 2 -at "double";
	addAttr -ci true -sn "miSamplesQualityB" -ln "miSamplesQualityB" -dv 0.25 -min 0.01 
		-max 9999999.9000000004 -smn 0.1 -smx 2 -at "double";
	addAttr -ci true -sn "miSamplesQualityA" -ln "miSamplesQualityA" -dv 0.25 -min 0.01 
		-max 9999999.9000000004 -smn 0.1 -smx 2 -at "double";
	addAttr -ci true -sn "miSamplesMin" -ln "miSamplesMin" -dv 1 -min 0.1 -at "double";
	addAttr -ci true -sn "miSamplesMax" -ln "miSamplesMax" -dv 100 -min 0.1 -at "double";
	addAttr -ci true -sn "miSamplesErrorCutoffR" -ln "miSamplesErrorCutoffR" -min 0 
		-max 1 -at "double";
	addAttr -ci true -sn "miSamplesErrorCutoffG" -ln "miSamplesErrorCutoffG" -min 0 
		-max 1 -at "double";
	addAttr -ci true -sn "miSamplesErrorCutoffB" -ln "miSamplesErrorCutoffB" -min 0 
		-max 1 -at "double";
	addAttr -ci true -sn "miSamplesErrorCutoffA" -ln "miSamplesErrorCutoffA" -min 0 
		-max 1 -at "double";
	addAttr -ci true -sn "miSamplesPerObject" -ln "miSamplesPerObject" -min 0 -max 1 
		-at "bool";
	addAttr -ci true -sn "miRastShadingSamples" -ln "miRastShadingSamples" -dv 1 -min 
		0.25 -at "double";
	addAttr -ci true -sn "miRastSamples" -ln "miRastSamples" -dv 3 -min 1 -at "long";
	addAttr -ci true -sn "miContrastAsColor" -ln "miContrastAsColor" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "miProgMaxTime" -ln "miProgMaxTime" -min 0 -at "long";
	addAttr -ci true -sn "miProgSubsampleSize" -ln "miProgSubsampleSize" -dv 4 -min 
		1 -at "long";
	addAttr -ci true -sn "miTraceCameraMotionVectors" -ln "miTraceCameraMotionVectors" 
		-min 0 -max 1 -at "bool";
	addAttr -ci true -sn "miTraceCameraClip" -ln "miTraceCameraClip" -min 0 -max 1 -at "bool";
	setAttr -s 45 ".stringOptions";
	setAttr ".stringOptions[0].name" -type "string" "rast motion factor";
	setAttr ".stringOptions[0].value" -type "string" "1.0";
	setAttr ".stringOptions[0].type" -type "string" "scalar";
	setAttr ".stringOptions[1].name" -type "string" "rast transparency depth";
	setAttr ".stringOptions[1].value" -type "string" "8";
	setAttr ".stringOptions[1].type" -type "string" "integer";
	setAttr ".stringOptions[2].name" -type "string" "rast useopacity";
	setAttr ".stringOptions[2].value" -type "string" "true";
	setAttr ".stringOptions[2].type" -type "string" "boolean";
	setAttr ".stringOptions[3].name" -type "string" "importon";
	setAttr ".stringOptions[3].value" -type "string" "false";
	setAttr ".stringOptions[3].type" -type "string" "boolean";
	setAttr ".stringOptions[4].name" -type "string" "importon density";
	setAttr ".stringOptions[4].value" -type "string" "1.0";
	setAttr ".stringOptions[4].type" -type "string" "scalar";
	setAttr ".stringOptions[5].name" -type "string" "importon merge";
	setAttr ".stringOptions[5].value" -type "string" "0.0";
	setAttr ".stringOptions[5].type" -type "string" "scalar";
	setAttr ".stringOptions[6].name" -type "string" "importon trace depth";
	setAttr ".stringOptions[6].value" -type "string" "0";
	setAttr ".stringOptions[6].type" -type "string" "integer";
	setAttr ".stringOptions[7].name" -type "string" "importon traverse";
	setAttr ".stringOptions[7].value" -type "string" "true";
	setAttr ".stringOptions[7].type" -type "string" "boolean";
	setAttr ".stringOptions[8].name" -type "string" "shadowmap pixel samples";
	setAttr ".stringOptions[8].value" -type "string" "3";
	setAttr ".stringOptions[8].type" -type "string" "integer";
	setAttr ".stringOptions[9].name" -type "string" "ambient occlusion";
	setAttr ".stringOptions[9].value" -type "string" "false";
	setAttr ".stringOptions[9].type" -type "string" "boolean";
	setAttr ".stringOptions[10].name" -type "string" "ambient occlusion rays";
	setAttr ".stringOptions[10].value" -type "string" "256";
	setAttr ".stringOptions[10].type" -type "string" "integer";
	setAttr ".stringOptions[11].name" -type "string" "ambient occlusion cache";
	setAttr ".stringOptions[11].value" -type "string" "false";
	setAttr ".stringOptions[11].type" -type "string" "boolean";
	setAttr ".stringOptions[12].name" -type "string" "ambient occlusion cache density";
	setAttr ".stringOptions[12].value" -type "string" "1.0";
	setAttr ".stringOptions[12].type" -type "string" "scalar";
	setAttr ".stringOptions[13].name" -type "string" "ambient occlusion cache points";
	setAttr ".stringOptions[13].value" -type "string" "64";
	setAttr ".stringOptions[13].type" -type "string" "integer";
	setAttr ".stringOptions[14].name" -type "string" "irradiance particles";
	setAttr ".stringOptions[14].value" -type "string" "false";
	setAttr ".stringOptions[14].type" -type "string" "boolean";
	setAttr ".stringOptions[15].name" -type "string" "irradiance particles rays";
	setAttr ".stringOptions[15].value" -type "string" "256";
	setAttr ".stringOptions[15].type" -type "string" "integer";
	setAttr ".stringOptions[16].name" -type "string" "irradiance particles interpolate";
	setAttr ".stringOptions[16].value" -type "string" "1";
	setAttr ".stringOptions[16].type" -type "string" "integer";
	setAttr ".stringOptions[17].name" -type "string" "irradiance particles interppoints";
	setAttr ".stringOptions[17].value" -type "string" "64";
	setAttr ".stringOptions[17].type" -type "string" "integer";
	setAttr ".stringOptions[18].name" -type "string" "irradiance particles indirect passes";
	setAttr ".stringOptions[18].value" -type "string" "0";
	setAttr ".stringOptions[18].type" -type "string" "integer";
	setAttr ".stringOptions[19].name" -type "string" "irradiance particles scale";
	setAttr ".stringOptions[19].value" -type "string" "1.0";
	setAttr ".stringOptions[19].type" -type "string" "scalar";
	setAttr ".stringOptions[20].name" -type "string" "irradiance particles env";
	setAttr ".stringOptions[20].value" -type "string" "true";
	setAttr ".stringOptions[20].type" -type "string" "boolean";
	setAttr ".stringOptions[21].name" -type "string" "irradiance particles env rays";
	setAttr ".stringOptions[21].value" -type "string" "256";
	setAttr ".stringOptions[21].type" -type "string" "integer";
	setAttr ".stringOptions[22].name" -type "string" "irradiance particles env scale";
	setAttr ".stringOptions[22].value" -type "string" "1";
	setAttr ".stringOptions[22].type" -type "string" "integer";
	setAttr ".stringOptions[23].name" -type "string" "irradiance particles rebuild";
	setAttr ".stringOptions[23].value" -type "string" "true";
	setAttr ".stringOptions[23].type" -type "string" "boolean";
	setAttr ".stringOptions[24].name" -type "string" "irradiance particles file";
	setAttr ".stringOptions[24].value" -type "string" "";
	setAttr ".stringOptions[24].type" -type "string" "string";
	setAttr ".stringOptions[25].name" -type "string" "geom displace motion factor";
	setAttr ".stringOptions[25].value" -type "string" "1.0";
	setAttr ".stringOptions[25].type" -type "string" "scalar";
	setAttr ".stringOptions[26].name" -type "string" "contrast all buffers";
	setAttr ".stringOptions[26].value" -type "string" "true";
	setAttr ".stringOptions[26].type" -type "string" "boolean";
	setAttr ".stringOptions[27].name" -type "string" "finalgather normal tolerance";
	setAttr ".stringOptions[27].value" -type "string" "25.842";
	setAttr ".stringOptions[27].type" -type "string" "scalar";
	setAttr ".stringOptions[28].name" -type "string" "trace camera clip";
	setAttr ".stringOptions[28].value" -type "string" "false";
	setAttr ".stringOptions[28].type" -type "string" "boolean";
	setAttr ".stringOptions[29].name" -type "string" "unified sampling";
	setAttr ".stringOptions[29].value" -type "string" "true";
	setAttr ".stringOptions[29].type" -type "string" "boolean";
	setAttr ".stringOptions[30].name" -type "string" "samples quality";
	setAttr ".stringOptions[30].value" -type "string" "0.5 0.5 0.5 0.5";
	setAttr ".stringOptions[30].type" -type "string" "color";
	setAttr ".stringOptions[31].name" -type "string" "samples min";
	setAttr ".stringOptions[31].value" -type "string" "1.0";
	setAttr ".stringOptions[31].type" -type "string" "scalar";
	setAttr ".stringOptions[32].name" -type "string" "samples max";
	setAttr ".stringOptions[32].value" -type "string" "100.0";
	setAttr ".stringOptions[32].type" -type "string" "scalar";
	setAttr ".stringOptions[33].name" -type "string" "samples error cutoff";
	setAttr ".stringOptions[33].value" -type "string" "0.0 0.0 0.0 0.0";
	setAttr ".stringOptions[33].type" -type "string" "color";
	setAttr ".stringOptions[34].name" -type "string" "samples per object";
	setAttr ".stringOptions[34].value" -type "string" "false";
	setAttr ".stringOptions[34].type" -type "string" "boolean";
	setAttr ".stringOptions[35].name" -type "string" "progressive";
	setAttr ".stringOptions[35].value" -type "string" "false";
	setAttr ".stringOptions[35].type" -type "string" "boolean";
	setAttr ".stringOptions[36].name" -type "string" "progressive max time";
	setAttr ".stringOptions[36].value" -type "string" "0";
	setAttr ".stringOptions[36].type" -type "string" "integer";
	setAttr ".stringOptions[37].name" -type "string" "progressive subsampling size";
	setAttr ".stringOptions[37].value" -type "string" "1";
	setAttr ".stringOptions[37].type" -type "string" "integer";
	setAttr ".stringOptions[38].name" -type "string" "iray";
	setAttr ".stringOptions[38].value" -type "string" "false";
	setAttr ".stringOptions[38].type" -type "string" "boolean";
	setAttr ".stringOptions[39].name" -type "string" "light relative scale";
	setAttr ".stringOptions[39].value" -type "string" "0.31831";
	setAttr ".stringOptions[39].type" -type "string" "scalar";
	setAttr ".stringOptions[40].name" -type "string" "trace camera motion vectors";
	setAttr ".stringOptions[40].value" -type "string" "false";
	setAttr ".stringOptions[40].type" -type "string" "boolean";
	setAttr ".stringOptions[41].name" -type "string" "ray differentials";
	setAttr ".stringOptions[41].value" -type "string" "true";
	setAttr ".stringOptions[41].type" -type "string" "boolean";
	setAttr ".stringOptions[42].name" -type "string" "environment lighting mode";
	setAttr ".stringOptions[42].value" -type "string" "off";
	setAttr ".stringOptions[42].type" -type "string" "string";
	setAttr ".stringOptions[43].name" -type "string" "environment lighting quality";
	setAttr ".stringOptions[43].value" -type "string" "0.167";
	setAttr ".stringOptions[43].type" -type "string" "scalar";
	setAttr ".stringOptions[44].name" -type "string" "environment lighting shadow";
	setAttr ".stringOptions[44].value" -type "string" "transparent";
	setAttr ".stringOptions[44].type" -type "string" "string";
createNode mentalrayFramebuffer -s -n "miDefaultFramebuffer";
createNode phong -n "Animations_v3_business05_f_highpolyPhong";
	setAttr ".sc" -type "float3" 0 0 0 ;
createNode shadingEngine -n "Animations_v3_business05_f_highpolySG";
	setAttr ".ihi" 0;
	setAttr ".ro" yes;
createNode materialInfo -n "Animations_v3_materialInfo1";
createNode shadingEngine -n "Animations_v3_business05_f_highpolySG1";
	setAttr ".ihi" 0;
	setAttr ".ro" yes;
createNode materialInfo -n "Animations_v3_materialInfo2";
createNode file -n "Animations_v3_Map1";
	setAttr ".ftn" -type "string" "C:\\Users\\Kuan\\Desktop\\iPi Record 20150227\\business05_f\\business05_f_35.tga";
createNode place2dTexture -n "Animations_v3_place2dTexture1";
createNode phong -n "Animations_v3_business05_f_highpoly_hairPhong";
	setAttr ".c" -type "float3" 0 0 0 ;
	setAttr ".sc" -type "float3" 0 0 0 ;
createNode shadingEngine -n "Animations_v3_business05_f_highpoly_hairSG";
	setAttr ".ihi" 0;
	setAttr ".ro" yes;
createNode materialInfo -n "Animations_v3_materialInfo3";
createNode shadingEngine -n "Animations_v3_business05_f_highpoly_hairSG1";
	setAttr ".ihi" 0;
	setAttr ".ro" yes;
createNode materialInfo -n "Animations_v3_materialInfo4";
createNode phong -n "Jessica_Take_2_2_business05_f_highpolyPhong";
	setAttr ".sc" -type "float3" 0 0 0 ;
createNode shadingEngine -n "Jessica_Take_2_2_business05_f_highpolySG";
	setAttr ".ihi" 0;
	setAttr ".ro" yes;
createNode materialInfo -n "Jessica_Take_2_2_materialInfo1";
createNode shadingEngine -n "Jessica_Take_2_2_business05_f_highpolySG1";
	setAttr ".ihi" 0;
	setAttr ".ro" yes;
createNode materialInfo -n "Jessica_Take_2_2_materialInfo2";
createNode file -n "Jessica_Take_2_2_Map1";
	setAttr ".ftn" -type "string" "C:\\Users\\Kuan\\Desktop\\iPi Record 20150227\\business05_f\\business05_f_35.tga";
createNode place2dTexture -n "Jessica_Take_2_2_place2dTexture1";
createNode phong -n "Jessica_Take_2_2_business05_f_highpoly_hairPhong";
	setAttr ".c" -type "float3" 0 0 0 ;
	setAttr ".sc" -type "float3" 0 0 0 ;
createNode shadingEngine -n "Jessica_Take_2_2_business05_f_highpoly_hairSG";
	setAttr ".ihi" 0;
	setAttr ".ro" yes;
createNode materialInfo -n "Jessica_Take_2_2_materialInfo3";
createNode shadingEngine -n "Jessica_Take_2_2_business05_f_highpoly_hairSG1";
	setAttr ".ihi" 0;
	setAttr ".ro" yes;
createNode materialInfo -n "Jessica_Take_2_2_materialInfo4";
createNode phong -n "Jessica_Take_2_2_business05_f_highpolyPhong1";
	setAttr ".sc" -type "float3" 0 0 0 ;
createNode shadingEngine -n "Jessica_Take_2_2_business05_f_highpolySG2";
	setAttr ".ihi" 0;
	setAttr ".ro" yes;
createNode materialInfo -n "Jessica_Take_2_2_materialInfo5";
createNode shadingEngine -n "Jessica_Take_2_2_business05_f_highpolySG3";
	setAttr ".ihi" 0;
	setAttr ".ro" yes;
createNode materialInfo -n "Jessica_Take_2_2_materialInfo6";
createNode file -n "Jessica_Take_2_2_Map2";
	setAttr ".ftn" -type "string" "C:\\Users\\Kuan\\Desktop\\iPi Record 20150227\\business05_f\\business05_f_35.tga";
createNode place2dTexture -n "Jessica_Take_2_2_place2dTexture2";
createNode phong -n "Jessica_Take_2_2_business05_f_highpoly_hairPhong1";
	setAttr ".c" -type "float3" 0 0 0 ;
	setAttr ".sc" -type "float3" 0 0 0 ;
createNode shadingEngine -n "Jessica_Take_2_2_business05_f_highpoly_hairSG2";
	setAttr ".ihi" 0;
	setAttr ".ro" yes;
createNode materialInfo -n "Jessica_Take_2_2_materialInfo7";
createNode shadingEngine -n "Jessica_Take_2_2_business05_f_highpoly_hairSG3";
	setAttr ".ihi" 0;
	setAttr ".ro" yes;
createNode materialInfo -n "Jessica_Take_2_2_materialInfo8";
createNode phong -n "Jessica_Take_2_2_business05_f_highpolyPhong2";
	setAttr ".sc" -type "float3" 0 0 0 ;
createNode shadingEngine -n "Jessica_Take_2_2_business05_f_highpolySG4";
	setAttr ".ihi" 0;
	setAttr ".ro" yes;
createNode materialInfo -n "Jessica_Take_2_2_materialInfo9";
createNode shadingEngine -n "Jessica_Take_2_2_business05_f_highpolySG5";
	setAttr ".ihi" 0;
	setAttr ".ro" yes;
createNode materialInfo -n "Jessica_Take_2_2_materialInfo10";
createNode file -n "Jessica_Take_2_2_Map3";
	setAttr ".ftn" -type "string" "C:\\Users\\Kuan\\Desktop\\iPi Record 20150227\\business05_f\\business05_f_35.tga";
createNode place2dTexture -n "Jessica_Take_2_2_place2dTexture3";
createNode phong -n "Jessica_Take_2_2_business05_f_highpoly_hairPhong2";
	setAttr ".c" -type "float3" 0 0 0 ;
	setAttr ".sc" -type "float3" 0 0 0 ;
createNode shadingEngine -n "Jessica_Take_2_2_business05_f_highpoly_hairSG4";
	setAttr ".ihi" 0;
	setAttr ".ro" yes;
createNode materialInfo -n "Jessica_Take_2_2_materialInfo11";
createNode shadingEngine -n "Jessica_Take_2_2_business05_f_highpoly_hairSG5";
	setAttr ".ihi" 0;
	setAttr ".ro" yes;
createNode materialInfo -n "Jessica_Take_2_2_materialInfo12";
createNode phong -n "Jessica_Take_2_2_business05_f_highpolyPhong3";
	setAttr ".sc" -type "float3" 0 0 0 ;
createNode shadingEngine -n "Jessica_Take_2_2_business05_f_highpolySG6";
	setAttr ".ihi" 0;
	setAttr ".ro" yes;
createNode materialInfo -n "Jessica_Take_2_2_materialInfo13";
createNode shadingEngine -n "Jessica_Take_2_2_business05_f_highpolySG7";
	setAttr ".ihi" 0;
	setAttr ".ro" yes;
createNode materialInfo -n "Jessica_Take_2_2_materialInfo14";
createNode file -n "Jessica_Take_2_2_Map4";
	setAttr ".ftn" -type "string" "C:\\Users\\Kuan\\Desktop\\iPi Record 20150227\\business05_f\\business05_f_35.tga";
createNode place2dTexture -n "Jessica_Take_2_2_place2dTexture4";
createNode phong -n "Jessica_Take_2_2_business05_f_highpoly_hairPhong3";
	setAttr ".c" -type "float3" 0 0 0 ;
	setAttr ".sc" -type "float3" 0 0 0 ;
createNode shadingEngine -n "Jessica_Take_2_2_business05_f_highpoly_hairSG6";
	setAttr ".ihi" 0;
	setAttr ".ro" yes;
createNode materialInfo -n "Jessica_Take_2_2_materialInfo15";
createNode shadingEngine -n "Jessica_Take_2_2_business05_f_highpoly_hairSG7";
	setAttr ".ihi" 0;
	setAttr ".ro" yes;
createNode materialInfo -n "Jessica_Take_2_2_materialInfo16";
createNode phong -n "Jessica_Take_2_2_business05_f_highpolyPhong4";
	setAttr ".sc" -type "float3" 0 0 0 ;
createNode shadingEngine -n "Jessica_Take_2_2_business05_f_highpolySG8";
	setAttr ".ihi" 0;
	setAttr ".ro" yes;
createNode materialInfo -n "Jessica_Take_2_2_materialInfo17";
createNode shadingEngine -n "Jessica_Take_2_2_business05_f_highpolySG9";
	setAttr ".ihi" 0;
	setAttr ".ro" yes;
createNode materialInfo -n "Jessica_Take_2_2_materialInfo18";
createNode file -n "Jessica_Take_2_2_Map5";
	setAttr ".ftn" -type "string" "C:\\Users\\Kuan\\Desktop\\iPi Record 20150227\\business05_f\\business05_f_35.tga";
createNode place2dTexture -n "Jessica_Take_2_2_place2dTexture5";
createNode phong -n "Jessica_Take_2_2_business05_f_highpoly_hairPhong4";
	setAttr ".c" -type "float3" 0 0 0 ;
	setAttr ".sc" -type "float3" 0 0 0 ;
createNode shadingEngine -n "Jessica_Take_2_2_business05_f_highpoly_hairSG8";
	setAttr ".ihi" 0;
	setAttr ".ro" yes;
createNode materialInfo -n "Jessica_Take_2_2_materialInfo19";
createNode shadingEngine -n "Jessica_Take_2_2_business05_f_highpoly_hairSG9";
	setAttr ".ihi" 0;
	setAttr ".ro" yes;
createNode materialInfo -n "Jessica_Take_2_2_materialInfo20";
createNode phong -n "Jessica_Take_2_2_business05_f_highpolyPhong5";
	setAttr ".sc" -type "float3" 0 0 0 ;
createNode shadingEngine -n "Jessica_Take_2_2_business05_f_highpolySG10";
	setAttr ".ihi" 0;
	setAttr ".ro" yes;
createNode materialInfo -n "Jessica_Take_2_2_materialInfo21";
createNode shadingEngine -n "Jessica_Take_2_2_business05_f_highpolySG11";
	setAttr ".ihi" 0;
	setAttr ".ro" yes;
createNode materialInfo -n "Jessica_Take_2_2_materialInfo22";
createNode file -n "Jessica_Take_2_2_Map6";
	setAttr ".ftn" -type "string" "C:\\Users\\Kuan\\Desktop\\iPi Record 20150227\\business05_f\\business05_f_35.tga";
createNode place2dTexture -n "Jessica_Take_2_2_place2dTexture6";
createNode phong -n "Jessica_Take_2_2_business05_f_highpoly_hairPhong5";
	setAttr ".c" -type "float3" 0 0 0 ;
	setAttr ".sc" -type "float3" 0 0 0 ;
createNode shadingEngine -n "Jessica_Take_2_2_business05_f_highpoly_hairSG10";
	setAttr ".ihi" 0;
	setAttr ".ro" yes;
createNode materialInfo -n "Jessica_Take_2_2_materialInfo23";
createNode shadingEngine -n "Jessica_Take_2_2_business05_f_highpoly_hairSG11";
	setAttr ".ihi" 0;
	setAttr ".ro" yes;
createNode materialInfo -n "Jessica_Take_2_2_materialInfo24";
createNode phong -n "Candice_Take_1_business05_f_highpolyPhong";
	setAttr ".sc" -type "float3" 0 0 0 ;
createNode shadingEngine -n "Candice_Take_1_business05_f_highpolySG";
	setAttr ".ihi" 0;
	setAttr ".ro" yes;
createNode materialInfo -n "Candice_Take_1_materialInfo1";
createNode shadingEngine -n "Candice_Take_1_business05_f_highpolySG1";
	setAttr ".ihi" 0;
	setAttr ".ro" yes;
createNode materialInfo -n "Candice_Take_1_materialInfo2";
createNode file -n "Candice_Take_1_Map1";
	setAttr ".ftn" -type "string" "C:\\Users\\Kuan\\Desktop\\iPi Record 20150227\\business05_f\\business05_f_35.tga";
createNode place2dTexture -n "Candice_Take_1_place2dTexture1";
createNode phong -n "Candice_Take_1_business05_f_highpoly_hairPhong";
	setAttr ".c" -type "float3" 0 0 0 ;
	setAttr ".sc" -type "float3" 0 0 0 ;
createNode shadingEngine -n "Candice_Take_1_business05_f_highpoly_hairSG";
	setAttr ".ihi" 0;
	setAttr ".ro" yes;
createNode materialInfo -n "Candice_Take_1_materialInfo3";
createNode shadingEngine -n "Candice_Take_1_business05_f_highpoly_hairSG1";
	setAttr ".ihi" 0;
	setAttr ".ro" yes;
createNode materialInfo -n "Candice_Take_1_materialInfo4";
createNode phong -n "Candice_Take_1_business05_f_highpolyPhong1";
	setAttr ".sc" -type "float3" 0 0 0 ;
createNode shadingEngine -n "Candice_Take_1_business05_f_highpolySG2";
	setAttr ".ihi" 0;
	setAttr ".ro" yes;
createNode materialInfo -n "Candice_Take_1_materialInfo5";
createNode shadingEngine -n "Candice_Take_1_business05_f_highpolySG3";
	setAttr ".ihi" 0;
	setAttr ".ro" yes;
createNode materialInfo -n "Candice_Take_1_materialInfo6";
createNode file -n "Candice_Take_1_Map2";
	setAttr ".ftn" -type "string" "C:\\Users\\Kuan\\Desktop\\iPi Record 20150227\\business05_f\\business05_f_35.tga";
createNode place2dTexture -n "Candice_Take_1_place2dTexture2";
createNode phong -n "Candice_Take_1_business05_f_highpoly_hairPhong1";
	setAttr ".c" -type "float3" 0 0 0 ;
	setAttr ".sc" -type "float3" 0 0 0 ;
createNode shadingEngine -n "Candice_Take_1_business05_f_highpoly_hairSG2";
	setAttr ".ihi" 0;
	setAttr ".ro" yes;
createNode materialInfo -n "Candice_Take_1_materialInfo7";
createNode shadingEngine -n "Candice_Take_1_business05_f_highpoly_hairSG3";
	setAttr ".ihi" 0;
	setAttr ".ro" yes;
createNode materialInfo -n "Candice_Take_1_materialInfo8";
createNode phong -n "Candice_Take_1_business05_f_highpolyPhong2";
	setAttr ".sc" -type "float3" 0 0 0 ;
createNode shadingEngine -n "Candice_Take_1_business05_f_highpolySG4";
	setAttr ".ihi" 0;
	setAttr ".ro" yes;
createNode materialInfo -n "Candice_Take_1_materialInfo9";
createNode shadingEngine -n "Candice_Take_1_business05_f_highpolySG5";
	setAttr ".ihi" 0;
	setAttr ".ro" yes;
createNode materialInfo -n "Candice_Take_1_materialInfo10";
createNode file -n "Candice_Take_1_Map3";
	setAttr ".ftn" -type "string" "C:\\Users\\Kuan\\Desktop\\iPi Record 20150227\\business05_f\\business05_f_35.tga";
createNode place2dTexture -n "Candice_Take_1_place2dTexture3";
createNode phong -n "Candice_Take_1_business05_f_highpoly_hairPhong2";
	setAttr ".c" -type "float3" 0 0 0 ;
	setAttr ".sc" -type "float3" 0 0 0 ;
createNode shadingEngine -n "Candice_Take_1_business05_f_highpoly_hairSG4";
	setAttr ".ihi" 0;
	setAttr ".ro" yes;
createNode materialInfo -n "Candice_Take_1_materialInfo11";
createNode shadingEngine -n "Candice_Take_1_business05_f_highpoly_hairSG5";
	setAttr ".ihi" 0;
	setAttr ".ro" yes;
createNode materialInfo -n "Candice_Take_1_materialInfo12";
createNode phong -n "Candice_Take_1_business05_f_highpolyPhong3";
	setAttr ".sc" -type "float3" 0 0 0 ;
createNode shadingEngine -n "Candice_Take_1_business05_f_highpolySG6";
	setAttr ".ihi" 0;
	setAttr ".ro" yes;
createNode materialInfo -n "Candice_Take_1_materialInfo13";
createNode shadingEngine -n "Candice_Take_1_business05_f_highpolySG7";
	setAttr ".ihi" 0;
	setAttr ".ro" yes;
createNode materialInfo -n "Candice_Take_1_materialInfo14";
createNode file -n "Candice_Take_1_Map4";
	setAttr ".ftn" -type "string" "C:\\Users\\Kuan\\Desktop\\iPi Record 20150227\\business05_f\\business05_f_35.tga";
createNode place2dTexture -n "Candice_Take_1_place2dTexture4";
createNode phong -n "Candice_Take_1_business05_f_highpoly_hairPhong3";
	setAttr ".c" -type "float3" 0 0 0 ;
	setAttr ".sc" -type "float3" 0 0 0 ;
createNode shadingEngine -n "Candice_Take_1_business05_f_highpoly_hairSG6";
	setAttr ".ihi" 0;
	setAttr ".ro" yes;
createNode materialInfo -n "Candice_Take_1_materialInfo15";
createNode shadingEngine -n "Candice_Take_1_business05_f_highpoly_hairSG7";
	setAttr ".ihi" 0;
	setAttr ".ro" yes;
createNode materialInfo -n "Candice_Take_1_materialInfo16";
createNode phong -n "Candice_Take_1_business05_f_highpolyPhong4";
	setAttr ".sc" -type "float3" 0 0 0 ;
createNode shadingEngine -n "Candice_Take_1_business05_f_highpolySG8";
	setAttr ".ihi" 0;
	setAttr ".ro" yes;
createNode materialInfo -n "Candice_Take_1_materialInfo17";
createNode shadingEngine -n "Candice_Take_1_business05_f_highpolySG9";
	setAttr ".ihi" 0;
	setAttr ".ro" yes;
createNode materialInfo -n "Candice_Take_1_materialInfo18";
createNode file -n "Candice_Take_1_Map5";
	setAttr ".ftn" -type "string" "C:\\Users\\Kuan\\Desktop\\iPi Record 20150227\\business05_f\\business05_f_35.tga";
createNode place2dTexture -n "Candice_Take_1_place2dTexture5";
createNode phong -n "Candice_Take_1_business05_f_highpoly_hairPhong4";
	setAttr ".c" -type "float3" 0 0 0 ;
	setAttr ".sc" -type "float3" 0 0 0 ;
createNode shadingEngine -n "Candice_Take_1_business05_f_highpoly_hairSG8";
	setAttr ".ihi" 0;
	setAttr ".ro" yes;
createNode materialInfo -n "Candice_Take_1_materialInfo19";
createNode shadingEngine -n "Candice_Take_1_business05_f_highpoly_hairSG9";
	setAttr ".ihi" 0;
	setAttr ".ro" yes;
createNode materialInfo -n "Candice_Take_1_materialInfo20";
createNode phong -n "Candice_Take_1_business05_f_highpolyPhong5";
	setAttr ".sc" -type "float3" 0 0 0 ;
createNode shadingEngine -n "Candice_Take_1_business05_f_highpolySG10";
	setAttr ".ihi" 0;
	setAttr ".ro" yes;
createNode materialInfo -n "Candice_Take_1_materialInfo21";
createNode shadingEngine -n "Candice_Take_1_business05_f_highpolySG11";
	setAttr ".ihi" 0;
	setAttr ".ro" yes;
createNode materialInfo -n "Candice_Take_1_materialInfo22";
createNode file -n "Candice_Take_1_Map6";
	setAttr ".ftn" -type "string" "C:\\Users\\Kuan\\Desktop\\iPi Record 20150227\\business05_f\\business05_f_35.tga";
createNode place2dTexture -n "Candice_Take_1_place2dTexture6";
createNode phong -n "Candice_Take_1_business05_f_highpoly_hairPhong5";
	setAttr ".c" -type "float3" 0 0 0 ;
	setAttr ".sc" -type "float3" 0 0 0 ;
createNode shadingEngine -n "Candice_Take_1_business05_f_highpoly_hairSG10";
	setAttr ".ihi" 0;
	setAttr ".ro" yes;
createNode materialInfo -n "Candice_Take_1_materialInfo23";
createNode shadingEngine -n "Candice_Take_1_business05_f_highpoly_hairSG11";
	setAttr ".ihi" 0;
	setAttr ".ro" yes;
createNode materialInfo -n "Candice_Take_1_materialInfo24";
createNode phong -n "Candice_Take_1_business05_f_highpolyPhong6";
	setAttr ".sc" -type "float3" 0 0 0 ;
createNode shadingEngine -n "Candice_Take_1_business05_f_highpolySG12";
	setAttr ".ihi" 0;
	setAttr ".ro" yes;
createNode materialInfo -n "Candice_Take_1_materialInfo25";
createNode shadingEngine -n "Candice_Take_1_business05_f_highpolySG13";
	setAttr ".ihi" 0;
	setAttr ".ro" yes;
createNode materialInfo -n "Candice_Take_1_materialInfo26";
createNode file -n "Candice_Take_1_Map7";
	setAttr ".ftn" -type "string" "C:\\Users\\Kuan\\Desktop\\iPi Record 20150227\\business05_f\\business05_f_35.tga";
createNode place2dTexture -n "Candice_Take_1_place2dTexture7";
createNode phong -n "Candice_Take_1_business05_f_highpoly_hairPhong6";
	setAttr ".c" -type "float3" 0 0 0 ;
	setAttr ".sc" -type "float3" 0 0 0 ;
createNode shadingEngine -n "Candice_Take_1_business05_f_highpoly_hairSG12";
	setAttr ".ihi" 0;
	setAttr ".ro" yes;
createNode materialInfo -n "Candice_Take_1_materialInfo27";
createNode shadingEngine -n "Candice_Take_1_business05_f_highpoly_hairSG13";
	setAttr ".ihi" 0;
	setAttr ".ro" yes;
createNode materialInfo -n "Candice_Take_1_materialInfo28";
createNode phong -n "Candice_Take_1_business05_f_highpolyPhong7";
	setAttr ".sc" -type "float3" 0 0 0 ;
createNode shadingEngine -n "Candice_Take_1_business05_f_highpolySG14";
	setAttr ".ihi" 0;
	setAttr ".ro" yes;
createNode materialInfo -n "Candice_Take_1_materialInfo29";
createNode shadingEngine -n "Candice_Take_1_business05_f_highpolySG15";
	setAttr ".ihi" 0;
	setAttr ".ro" yes;
createNode materialInfo -n "Candice_Take_1_materialInfo30";
createNode file -n "Candice_Take_1_Map8";
	setAttr ".ftn" -type "string" "C:\\Users\\Kuan\\Desktop\\iPi Record 20150227\\business05_f\\business05_f_35.tga";
createNode place2dTexture -n "Candice_Take_1_place2dTexture8";
createNode phong -n "Candice_Take_1_business05_f_highpoly_hairPhong7";
	setAttr ".c" -type "float3" 0 0 0 ;
	setAttr ".sc" -type "float3" 0 0 0 ;
createNode shadingEngine -n "Candice_Take_1_business05_f_highpoly_hairSG14";
	setAttr ".ihi" 0;
	setAttr ".ro" yes;
createNode materialInfo -n "Candice_Take_1_materialInfo31";
createNode shadingEngine -n "Candice_Take_1_business05_f_highpoly_hairSG15";
	setAttr ".ihi" 0;
	setAttr ".ro" yes;
createNode materialInfo -n "Candice_Take_1_materialInfo32";
createNode phong -n "Animations_v3_business05_f_highpolyPhong1";
	setAttr ".sc" -type "float3" 0 0 0 ;
createNode shadingEngine -n "Animations_v3_business05_f_highpolySG2";
	setAttr ".ihi" 0;
	setAttr ".ro" yes;
createNode materialInfo -n "Animations_v3_materialInfo5";
createNode shadingEngine -n "Animations_v3_business05_f_highpolySG3";
	setAttr ".ihi" 0;
	setAttr ".ro" yes;
createNode materialInfo -n "Animations_v3_materialInfo6";
createNode file -n "Animations_v3_Map2";
	setAttr ".ftn" -type "string" "C:\\Users\\Kuan\\Desktop\\iPi Record 20150227\\business05_f\\business05_f_35.tga";
createNode place2dTexture -n "Animations_v3_place2dTexture2";
createNode phong -n "Animations_v3_business05_f_highpoly_hairPhong1";
	setAttr ".c" -type "float3" 0 0 0 ;
	setAttr ".sc" -type "float3" 0 0 0 ;
createNode shadingEngine -n "Animations_v3_business05_f_highpoly_hairSG2";
	setAttr ".ihi" 0;
	setAttr ".ro" yes;
createNode materialInfo -n "Animations_v3_materialInfo7";
createNode shadingEngine -n "Animations_v3_business05_f_highpoly_hairSG3";
	setAttr ".ihi" 0;
	setAttr ".ro" yes;
createNode materialInfo -n "Animations_v3_materialInfo8";
createNode phong -n "Kuan_take_1_business05_f_highpolyPhong";
	setAttr ".sc" -type "float3" 0 0 0 ;
createNode shadingEngine -n "Kuan_take_1_business05_f_highpolySG";
	setAttr ".ihi" 0;
	setAttr ".ro" yes;
createNode materialInfo -n "Kuan_take_1_materialInfo1";
createNode shadingEngine -n "Kuan_take_1_business05_f_highpolySG1";
	setAttr ".ihi" 0;
	setAttr ".ro" yes;
createNode materialInfo -n "Kuan_take_1_materialInfo2";
createNode file -n "Kuan_take_1_Map1";
	setAttr ".ftn" -type "string" "C:\\Users\\Kuan\\Desktop\\iPi Record 20150227\\business05_f\\business05_f_35.tga";
createNode place2dTexture -n "Kuan_take_1_place2dTexture1";
createNode phong -n "Kuan_take_1_business05_f_highpoly_hairPhong";
	setAttr ".c" -type "float3" 0 0 0 ;
	setAttr ".sc" -type "float3" 0 0 0 ;
createNode shadingEngine -n "Kuan_take_1_business05_f_highpoly_hairSG";
	setAttr ".ihi" 0;
	setAttr ".ro" yes;
createNode materialInfo -n "Kuan_take_1_materialInfo3";
createNode shadingEngine -n "Kuan_take_1_business05_f_highpoly_hairSG1";
	setAttr ".ihi" 0;
	setAttr ".ro" yes;
createNode materialInfo -n "Kuan_take_1_materialInfo4";
createNode phong -n "Kuan_take_1_business05_f_highpolyPhong1";
	setAttr ".sc" -type "float3" 0 0 0 ;
createNode shadingEngine -n "Kuan_take_1_business05_f_highpolySG2";
	setAttr ".ihi" 0;
	setAttr ".ro" yes;
createNode materialInfo -n "Kuan_take_1_materialInfo5";
createNode shadingEngine -n "Kuan_take_1_business05_f_highpolySG3";
	setAttr ".ihi" 0;
	setAttr ".ro" yes;
createNode materialInfo -n "Kuan_take_1_materialInfo6";
createNode file -n "Kuan_take_1_Map2";
	setAttr ".ftn" -type "string" "C:\\Users\\Kuan\\Desktop\\iPi Record 20150227\\business05_f\\business05_f_35.tga";
createNode place2dTexture -n "Kuan_take_1_place2dTexture2";
createNode phong -n "Kuan_take_1_business05_f_highpoly_hairPhong1";
	setAttr ".c" -type "float3" 0 0 0 ;
	setAttr ".sc" -type "float3" 0 0 0 ;
createNode shadingEngine -n "Kuan_take_1_business05_f_highpoly_hairSG2";
	setAttr ".ihi" 0;
	setAttr ".ro" yes;
createNode materialInfo -n "Kuan_take_1_materialInfo7";
createNode shadingEngine -n "Kuan_take_1_business05_f_highpoly_hairSG3";
	setAttr ".ihi" 0;
	setAttr ".ro" yes;
createNode materialInfo -n "Kuan_take_1_materialInfo8";
createNode phong -n "Kuan_take_1_business05_f_highpolyPhong2";
	setAttr ".sc" -type "float3" 0 0 0 ;
createNode shadingEngine -n "Kuan_take_1_business05_f_highpolySG4";
	setAttr ".ihi" 0;
	setAttr ".ro" yes;
createNode materialInfo -n "Kuan_take_1_materialInfo9";
createNode shadingEngine -n "Kuan_take_1_business05_f_highpolySG5";
	setAttr ".ihi" 0;
	setAttr ".ro" yes;
createNode materialInfo -n "Kuan_take_1_materialInfo10";
createNode file -n "Kuan_take_1_Map3";
	setAttr ".ftn" -type "string" "C:\\Users\\Kuan\\Desktop\\iPi Record 20150227\\business05_f\\business05_f_35.tga";
createNode place2dTexture -n "Kuan_take_1_place2dTexture3";
createNode phong -n "Kuan_take_1_business05_f_highpoly_hairPhong2";
	setAttr ".c" -type "float3" 0 0 0 ;
	setAttr ".sc" -type "float3" 0 0 0 ;
createNode shadingEngine -n "Kuan_take_1_business05_f_highpoly_hairSG4";
	setAttr ".ihi" 0;
	setAttr ".ro" yes;
createNode materialInfo -n "Kuan_take_1_materialInfo11";
createNode shadingEngine -n "Kuan_take_1_business05_f_highpoly_hairSG5";
	setAttr ".ihi" 0;
	setAttr ".ro" yes;
createNode materialInfo -n "Kuan_take_1_materialInfo12";
createNode phong -n "Kuan_take_1_business05_f_highpolyPhong3";
	setAttr ".sc" -type "float3" 0 0 0 ;
createNode shadingEngine -n "Kuan_take_1_business05_f_highpolySG6";
	setAttr ".ihi" 0;
	setAttr ".ro" yes;
createNode materialInfo -n "Kuan_take_1_materialInfo13";
createNode shadingEngine -n "Kuan_take_1_business05_f_highpolySG7";
	setAttr ".ihi" 0;
	setAttr ".ro" yes;
createNode materialInfo -n "Kuan_take_1_materialInfo14";
createNode file -n "Kuan_take_1_Map4";
	setAttr ".ftn" -type "string" "C:\\Users\\Kuan\\Desktop\\iPi Record 20150227\\business05_f\\business05_f_35.tga";
createNode place2dTexture -n "Kuan_take_1_place2dTexture4";
createNode phong -n "Kuan_take_1_business05_f_highpoly_hairPhong3";
	setAttr ".c" -type "float3" 0 0 0 ;
	setAttr ".sc" -type "float3" 0 0 0 ;
createNode shadingEngine -n "Kuan_take_1_business05_f_highpoly_hairSG6";
	setAttr ".ihi" 0;
	setAttr ".ro" yes;
createNode materialInfo -n "Kuan_take_1_materialInfo15";
createNode shadingEngine -n "Kuan_take_1_business05_f_highpoly_hairSG7";
	setAttr ".ihi" 0;
	setAttr ".ro" yes;
createNode materialInfo -n "Kuan_take_1_materialInfo16";
createNode phong -n "Kuan_take_1_business05_f_highpolyPhong4";
	setAttr ".sc" -type "float3" 0 0 0 ;
createNode shadingEngine -n "Kuan_take_1_business05_f_highpolySG8";
	setAttr ".ihi" 0;
	setAttr ".ro" yes;
createNode materialInfo -n "Kuan_take_1_materialInfo17";
createNode shadingEngine -n "Kuan_take_1_business05_f_highpolySG9";
	setAttr ".ihi" 0;
	setAttr ".ro" yes;
createNode materialInfo -n "Kuan_take_1_materialInfo18";
createNode file -n "Kuan_take_1_Map5";
	setAttr ".ftn" -type "string" "C:\\Users\\Kuan\\Desktop\\iPi Record 20150227\\business05_f\\business05_f_35.tga";
createNode place2dTexture -n "Kuan_take_1_place2dTexture5";
createNode phong -n "Kuan_take_1_business05_f_highpoly_hairPhong4";
	setAttr ".c" -type "float3" 0 0 0 ;
	setAttr ".sc" -type "float3" 0 0 0 ;
createNode shadingEngine -n "Kuan_take_1_business05_f_highpoly_hairSG8";
	setAttr ".ihi" 0;
	setAttr ".ro" yes;
createNode materialInfo -n "Kuan_take_1_materialInfo19";
createNode shadingEngine -n "Kuan_take_1_business05_f_highpoly_hairSG9";
	setAttr ".ihi" 0;
	setAttr ".ro" yes;
createNode materialInfo -n "Kuan_take_1_materialInfo20";
createNode phong -n "Kuan_take_1_business05_f_highpolyPhong5";
	setAttr ".sc" -type "float3" 0 0 0 ;
createNode shadingEngine -n "Kuan_take_1_business05_f_highpolySG10";
	setAttr ".ihi" 0;
	setAttr ".ro" yes;
createNode materialInfo -n "Kuan_take_1_materialInfo21";
createNode shadingEngine -n "Kuan_take_1_business05_f_highpolySG11";
	setAttr ".ihi" 0;
	setAttr ".ro" yes;
createNode materialInfo -n "Kuan_take_1_materialInfo22";
createNode file -n "Kuan_take_1_Map6";
	setAttr ".ftn" -type "string" "C:\\Users\\Kuan\\Desktop\\iPi Record 20150227\\business05_f\\business05_f_35.tga";
createNode place2dTexture -n "Kuan_take_1_place2dTexture6";
createNode phong -n "Kuan_take_1_business05_f_highpoly_hairPhong5";
	setAttr ".c" -type "float3" 0 0 0 ;
	setAttr ".sc" -type "float3" 0 0 0 ;
createNode shadingEngine -n "Kuan_take_1_business05_f_highpoly_hairSG10";
	setAttr ".ihi" 0;
	setAttr ".ro" yes;
createNode materialInfo -n "Kuan_take_1_materialInfo23";
createNode shadingEngine -n "Kuan_take_1_business05_f_highpoly_hairSG11";
	setAttr ".ihi" 0;
	setAttr ".ro" yes;
createNode materialInfo -n "Kuan_take_1_materialInfo24";
createNode phong -n "Kuan_take_1_business05_f_highpolyPhong6";
	setAttr ".sc" -type "float3" 0 0 0 ;
createNode shadingEngine -n "Kuan_take_1_business05_f_highpolySG12";
	setAttr ".ihi" 0;
	setAttr ".ro" yes;
createNode materialInfo -n "Kuan_take_1_materialInfo25";
createNode shadingEngine -n "Kuan_take_1_business05_f_highpolySG13";
	setAttr ".ihi" 0;
	setAttr ".ro" yes;
createNode materialInfo -n "Kuan_take_1_materialInfo26";
createNode file -n "Kuan_take_1_Map7";
	setAttr ".ftn" -type "string" "C:\\Users\\Kuan\\Desktop\\iPi Record 20150227\\business05_f\\business05_f_35.tga";
createNode place2dTexture -n "Kuan_take_1_place2dTexture7";
createNode phong -n "Kuan_take_1_business05_f_highpoly_hairPhong6";
	setAttr ".c" -type "float3" 0 0 0 ;
	setAttr ".sc" -type "float3" 0 0 0 ;
createNode shadingEngine -n "Kuan_take_1_business05_f_highpoly_hairSG12";
	setAttr ".ihi" 0;
	setAttr ".ro" yes;
createNode materialInfo -n "Kuan_take_1_materialInfo27";
createNode shadingEngine -n "Kuan_take_1_business05_f_highpoly_hairSG13";
	setAttr ".ihi" 0;
	setAttr ".ro" yes;
createNode materialInfo -n "Kuan_take_1_materialInfo28";
createNode phong -n "Candice_take_2_business05_f_highpolyPhong";
	setAttr ".sc" -type "float3" 0 0 0 ;
createNode shadingEngine -n "Candice_take_2_business05_f_highpolySG";
	setAttr ".ihi" 0;
	setAttr ".ro" yes;
createNode materialInfo -n "Candice_take_2_materialInfo1";
createNode shadingEngine -n "Candice_take_2_business05_f_highpolySG1";
	setAttr ".ihi" 0;
	setAttr ".ro" yes;
createNode materialInfo -n "Candice_take_2_materialInfo2";
createNode file -n "Candice_take_2_Map1";
	setAttr ".ftn" -type "string" "C:\\Users\\Kuan\\Desktop\\iPi Record 20150227\\business05_f\\business05_f_35.tga";
createNode place2dTexture -n "Candice_take_2_place2dTexture1";
createNode phong -n "Candice_take_2_business05_f_highpoly_hairPhong";
	setAttr ".c" -type "float3" 0 0 0 ;
	setAttr ".sc" -type "float3" 0 0 0 ;
createNode shadingEngine -n "Candice_take_2_business05_f_highpoly_hairSG";
	setAttr ".ihi" 0;
	setAttr ".ro" yes;
createNode materialInfo -n "Candice_take_2_materialInfo3";
createNode shadingEngine -n "Candice_take_2_business05_f_highpoly_hairSG1";
	setAttr ".ihi" 0;
	setAttr ".ro" yes;
createNode materialInfo -n "Candice_take_2_materialInfo4";
createNode dagPose -n "Candice_take_2_bindPose1";
	setAttr -s 41 ".wm";
	setAttr -s 41 ".xm";
	setAttr ".xm[0]" -type "matrix" "xform" 0.99999999999999978 0.99999999999999989 0.99999999999999989 -0.031477835323015443
		 0.008172522638072232 0.049786039459409515 0 -6.7139604917915256e-006 99.443700000000007
		 -2.2495999999899592 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1 0.50000074612769874 0.50000074612769851 0.49999925387118799 0.49999925387118815 1
		 1 1 no;
	setAttr ".xm[1]" -type "matrix" "xform" 1 1 1 0.0050883162760835179 3.0824270143757082
		 -0.009304841955685296 0 -11.0456 2.2585000000000002 -8.5201999999999991 0 0
		 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1 0 0 0 1 1 1 1 no;
	setAttr ".xm[2]" -type "matrix" "xform" 1 1 1 -0.0050588688875271371 0.0087297005443174487
		 -0.015256136826277262 0 13.398399999999981 -0.010699999999998155 -2.2204460492503131e-016 0
		 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1 0 0 0 1 1 1 1 no;
	setAttr ".xm[3]" -type "matrix" "xform" 1 1 1 -0.0025120920227392598 0.0043748244895647388
		 -0.0076224185476989172 0 13.398299999999992 -0.010699999999999044 0 0 0 0 0
		 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1 0 0 0 1 1 1 1 no;
	setAttr ".xm[4]" -type "matrix" "xform" 1 1 1 0.0050765936030839664 -0.020497166581500588
		 0.026306058172797835 0 13.398500000000013 -0.0079000000000002402 -4.4408920985006262e-016 0
		 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1 0 0 0 1 1 1 1 no;
	setAttr ".xm[5]" -type "matrix" "xform" 1 1 1 0.0038959319697258591 1.5545757612389408
		 3.116811187141622 0 -0.82679999999999998 -0.66120000000000001 -3.9164999999999996 0
		 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1 0 0 0 1 1 1 1 no;
	setAttr ".xm[6]" -type "matrix" "xform" 1 1 1 -0.01079151751894906 -1.5871910968592686
		 3.1100211752730513 0 -0.82679999999999998 -0.66120000000000001 3.9164999999999996 0
		 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1 0 0 0 1 1 1 1 no;
	setAttr ".xm[7]" -type "matrix" "xform" 1 1 1 -0.042544984479588502 -0.073657079117805058
		 -0.078578304309381342 0 10.6745 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1 0 0 0 1 1
		 1 1 no;
	setAttr ".xm[8]" -type "matrix" "xform" 1 1 1 0.00061246442168623363 0.0063304837584073487
		 -0.0095870695856821175 0 25.337500000000002 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0
		 0 0 0 0 1 0 0 0 1 1 1 1 no;
	setAttr ".xm[9]" -type "matrix" "xform" 1 1 1 1.5731232870201179 -0.10996275909923578
		 0.04477350726006369 0 24.988099999999999 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 
		0 0 0 1 0 0 0 1 1 1 1 no;
	setAttr ".xm[10]" -type "matrix" "xform" 1 1 1 -1.4299513977516076 -0.40690922085590414
		 0.31932383424197069 0 2.3036000000000003 1.3941000000000001 2.9222000000000001 0
		 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1 0 0 0 1 1 1 1 no;
	setAttr ".xm[11]" -type "matrix" "xform" 1 1 1 0.0012733218263955525 -3.1410410748591597
		 0.0047307375472734458 0 -11.0456 2.2584 8.5201999999999991 0 0 0 0 0 0 0 0 0 0
		 0 0 0 0 0 0 0 0 1 0 0 0 1 1 1 1 no;
	setAttr ".xm[12]" -type "matrix" "xform" 1 1 1 0.00069637070586297243 0.0072415354483972285
		 0.046612934387209086 0 9.9828999999999724 1.7763568394002505e-015 0 0 0 0 0
		 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1 0 0 0 1 1 1 1 no;
	setAttr ".xm[13]" -type "matrix" "xform" 1 1 1 -0.0010048335376841679 -0.017751881137635522
		 0.00013960295156272007 0 36.152699999999996 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0
		 0 0 0 0 1 0 0 0 1 1 1 1 no;
	setAttr ".xm[14]" -type "matrix" "xform" 1 1 1 1.7453292519943295e-008 5.2359877559829887e-008
		 0.42000561305750889 0 4.0806000000000004 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 
		0 0 0 1 0 0 0 1 1 1 1 no;
	setAttr ".xm[15]" -type "matrix" "xform" 1 1 1 0.00075684457683482102 0.066784238429399623
		 0.32305451042469363 0 7.2345000000000006 -0.59970000000000001 -0.93320000000000003 0
		 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1 0 0 0 1 1 1 1 no;
	setAttr ".xm[16]" -type "matrix" "xform" 1 1 1 -0.0030915715172276366 0.09707444505105374
		 0.28999008550370198 0 7.2291999999999996 -0.41869999999999996 -2.3639000000000001 0
		 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1 0 0 0 1 1 1 1 no;
	setAttr ".xm[17]" -type "matrix" "xform" 1 1 1 -0.012795078559540509 -0.092755784896626448
		 0.39526005567005301 0 7.3262999999999998 -0.21099999999999999 2.2784 0 0 0 0
		 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1 0 0 0 1 1 1 1 no;
	setAttr ".xm[18]" -type "matrix" "xform" 1 1 1 -3.4906585039886589e-008 6.9813170079773178e-008
		 0.39492748318108545 0 3.5597999999999996 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 
		0 0 0 1 0 0 0 1 1 1 1 no;
	setAttr ".xm[19]" -type "matrix" "xform" 1 1 1 0.00076122535325732685 -0.00023399629281487978
		 0.29821767231735841 0 7.3708999999999998 -0.48159999999999997 0.6341 0 0 0 0
		 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1 0 0 0 1 1 1 1 no;
	setAttr ".xm[20]" -type "matrix" "xform" 1 1 1 -0 1.7453292519943295e-008 0.52266310458630472 0 3.8207999999999998
		 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1 0 0 0 1 1 1 1 no;
	setAttr ".xm[21]" -type "matrix" "xform" 1 1 1 5.2359877559829887e-008 5.2359877559829887e-008
		 0.48841794697304636 0 3.7241000000000004 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 
		0 0 0 1 0 0 0 1 1 1 1 no;
	setAttr ".xm[22]" -type "matrix" "xform" 1 1 1 -3.4906585039886589e-008 -5.2359877559829887e-008
		 0.51390116776885775 0 3.4791000000000003 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 
		0 0 0 1 0 0 0 1 1 1 1 no;
	setAttr ".xm[23]" -type "matrix" "xform" 1 1 1 0.00080974782994424341 0.017746305866645683
		 -0.01748442879410439 0 36.152699999999996 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 
		0 0 0 1 0 0 0 1 1 1 1 no;
	setAttr ".xm[24]" -type "matrix" "xform" 1 1 1 0.13313121731825217 -0.0037738809693896522
		 0.006181593345489067 0 43.254300000000001 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 
		0 0 0 1 0 0 0 1 1 1 1 no;
	setAttr ".xm[25]" -type "matrix" "xform" 1 1 1 3.4906585039886589e-008 -3.4906585039886589e-008
		 1.5707960824488014 0 9.4714999999999989 11.2377 0 0 0 0 0 0 0 0 0 0 0 0 0 0
		 0 0 0 0 0 1 0 0 0 1 1 1 1 no;
	setAttr ".xm[26]" -type "matrix" "xform" 1 1 1 -0.14017175257925901 0.0071074362622883802
		 -0.021033110803417271 0 43.254300000000001 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 
		0 0 0 1 0 0 0 1 1 1 1 no;
	setAttr ".xm[27]" -type "matrix" "xform" 1 1 1 -8.7266462599716483e-008 3.4906585039886589e-008
		 1.5707959777290461 0 9.4714999999999989 11.2377 0 0 0 0 0 0 0 0 0 0 0 0 0 0
		 0 0 0 0 0 1 0 0 0 1 1 1 1 no;
	setAttr ".xm[28]" -type "matrix" "xform" 1 1 1 -0.76977917831270626 0.2541816572843722
		 0.088813235547226796 0 10.6745 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1 0 0 0 1 1
		 1 1 no;
	setAttr ".xm[29]" -type "matrix" "xform" 1 1 1 0.0019995222573986424 -0.014896069688982414
		 -0.24584669691331823 0 25.337399999999999 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 
		0 0 0 1 0 0 0 1 1 1 1 no;
	setAttr ".xm[30]" -type "matrix" "xform" 1 1 1 -1.5731232870201179 0.10996267183277318
		 0.04477350726006369 0 24.988099999999999 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 
		0 0 0 1 0 0 0 1 1 1 1 no;
	setAttr ".xm[31]" -type "matrix" "xform" 1 1 1 1.4299513977516076 0.40690922085590414
		 0.31932385169526323 0 2.3036000000000003 1.3941000000000001 -2.9222000000000001 0
		 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1 0 0 0 1 1 1 1 no;
	setAttr ".xm[32]" -type "matrix" "xform" 1 1 1 1.7453292519943295e-008 -1.7453292519943295e-008
		 0.42000566541738643 0 4.0806000000000004 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 
		0 0 0 1 0 0 0 1 1 1 1 no;
	setAttr ".xm[33]" -type "matrix" "xform" 1 1 1 -0.00075680967024978113 -0.066784290789277181
		 0.32305463259774125 0 7.2345000000000006 -0.59970000000000001 0.93320000000000003 0
		 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1 0 0 0 1 1 1 1 no;
	setAttr ".xm[34]" -type "matrix" "xform" 1 1 1 0.0030915366106425961 -0.097074410144468687
		 0.2899902600366272 0 7.2291999999999996 -0.41869999999999996 2.3639000000000001 0
		 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1 0 0 0 1 1 1 1 no;
	setAttr ".xm[35]" -type "matrix" "xform" 1 1 1 0.012795148372710589 0.092755749990041395
		 0.39526023020297818 0 7.3262999999999998 -0.21099999999999999 -2.2784 0 0 0 0
		 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1 0 0 0 1 1 1 1 no;
	setAttr ".xm[36]" -type "matrix" "xform" 1 1 1 -3.4906585039886589e-008 5.2359877559829887e-008
		 0.39492746572779291 0 3.5597999999999996 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 
		0 0 0 1 0 0 0 1 1 1 1 no;
	setAttr ".xm[37]" -type "matrix" "xform" 1 1 1 -0.00076113808679472718 0.00023404865269243958
		 0.29821775958382096 0 7.3708999999999998 -0.48159999999999997 -0.6341 0 0 0 0
		 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1 0 0 0 1 1 1 1 no;
	setAttr ".xm[38]" -type "matrix" "xform" 1 1 1 -6.9813170079773178e-008 -6.9813170079773178e-008
		 0.52266310458630472 0 3.8207999999999998 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 
		0 0 0 1 0 0 0 1 1 1 1 no;
	setAttr ".xm[39]" -type "matrix" "xform" 1 1 1 3.4906585039886589e-008 0 0.48841796442633889 0 3.7241000000000004
		 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1 0 0 0 1 1 1 1 no;
	setAttr ".xm[40]" -type "matrix" "xform" 1 1 1 3.4906585039886589e-008 -5.2359877559829887e-008
		 0.51390109795568761 0 3.4791000000000003 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 
		0 0 0 1 0 0 0 1 1 1 1 no;
	setAttr -s 41 ".m";
	setAttr -s 41 ".p";
	setAttr ".bp" yes;
createNode animCurveTL -n "Bip01_Spine_translateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 291 ".ktv";
	setAttr ".ktv[0:249]"  0.688 -2.1163110733032227 1.72 -2.2056770324707031
		 2.756 -2.2980983257293701 3.788 -2.3749229907989502 4.824 -2.4190909862518311 5.86 -2.4316065311431885
		 6.892 -2.4276113510131836 7.928 -2.419137716293335 8.964 -2.4119713306427002 9.996 -2.4026718139648437
		 11.032 -2.3836779594421387 12.064 -2.3566880226135254 13.1 -2.3284962177276611 14.136 -2.3088369369506836
		 15.168 -2.3001108169555664 16.204 -2.2822818756103516 17.24 -2.2511289119720459 18.272 -2.2339551448822021
		 19.308 -2.2341804504394531 20.34 -2.2248103618621826 21.376 -2.1952028274536133 22.412 -2.1652824878692627
		 23.444 -2.153162956237793 24.48 -2.1469557285308838 25.512 -2.1307275295257568 26.548 -2.1090481281280518
		 27.584 -2.086904764175415 28.616 -2.0605428218841553 29.652 -2.0314111709594727 30.688 -2.0027174949645996
		 31.72 -1.9740418195724487 32.756 -1.9451689720153809 33.788 -1.9139926433563232 34.824 -1.8830080032348633
		 35.86 -1.858522891998291 36.892 -1.8381167650222778 37.928 -1.8174794912338257 38.964 -1.8102235794067383
		 39.996 -1.8355453014373779 41.032 -1.8802056312561035 42.064 -1.9096031188964844
		 43.1 -1.9082118272781372 44.136 -1.8847535848617554 45.168 -1.86434006690979 46.204 -1.8834851980209351
		 47.24 -1.9522429704666138 48.272 -2.0216600894927979 49.308 -2.0448770523071289 50.34 -2.0406992435455322
		 51.376 -2.0486235618591309 52.412 -2.0613336563110352 53.444 -2.0378594398498535
		 54.48 -1.9658522605895996 55.512 -1.8708435297012329 56.548 -1.7827436923980713 57.584 -1.7250827550888062
		 58.616 -1.7013949155807495 59.652 -1.684252142906189 60.688 -1.6506452560424805 61.72 -1.6074032783508301
		 62.756 -1.5682418346405029 63.788 -1.5446010828018188 64.824 -1.5463918447494507
		 65.86 -1.561053991317749 66.892 -1.5692209005355835 67.928 -1.5758450031280518 68.964 -1.5878190994262695
		 69.996 -1.6025650501251221 71.032 -1.621055006980896 72.064 -1.6367048025131226 73.1 -1.6474525928497314
		 74.136 -1.6752225160598755 75.168 -1.7247611284255981 76.204 -1.7663754224777222
		 77.24 -1.7847203016281128 78.272 -1.7777601480484009 79.308 -1.7347468137741089 80.34 -1.6706961393356323
		 81.376 -1.6215237379074097 82.412 -1.5893017053604126 83.444 -1.5442872047424316
		 84.48 -1.475690484046936 85.512 -1.4084949493408203 86.548 -1.3735320568084717 87.584 -1.3765431642532349
		 88.616 -1.3906732797622681 89.652 -1.3792352676391602 90.688 -1.3291885852813721
		 91.72 -1.2614110708236694 92.756 -1.2036105394363403 93.788 -1.1487646102905273 94.824 -1.07421875
		 95.86 -0.99429404735565186 96.892 -0.93506109714508057 97.928 -0.88841569423675537
		 98.964 -0.84397351741790771 99.996 -0.8223838210105896 101.032 -0.8429253101348877
		 102.064 -0.87823480367660522 103.1 -0.8831447958946228 104.136 -0.87315917015075684
		 105.168 -0.89315909147262573 106.204 -0.93707561492919922 107.24 -0.97603601217269897
		 108.272 -1.0038919448852539 109.308 -1.0381729602813721 110.34 -1.0916627645492554
		 111.376 -1.1419247388839722 112.412 -1.1694673299789429 113.444 -1.1915876865386963
		 114.48 -1.2211736440658569 115.512 -1.2473068237304687 116.548 -1.267677903175354
		 117.584 -1.2900009155273437 118.616 -1.31524658203125 119.652 -1.3382717370986938
		 120.688 -1.3423992395401001 121.72 -1.3207182884216309 122.756 -1.302295446395874
		 123.788 -1.313402533531189 124.824 -1.3453153371810913 125.86 -1.376207709312439
		 126.892 -1.3925279378890991 127.928 -1.3902469873428345 128.964 -1.3687158823013306
		 129.996 -1.3359581232070923 131.032 -1.3087778091430664 132.064 -1.2989765405654907
		 133.1 -1.3085662126541138 134.136 -1.329209566116333 135.168 -1.348871111869812 136.204 -1.3662084341049194
		 137.236 -1.382536768913269 138.272 -1.3985576629638672 139.308 -1.4214283227920532
		 140.34 -1.4450514316558838 141.376 -1.4637408256530762 142.412 -1.4923958778381348
		 143.444 -1.5329544544219971 144.48 -1.5799624919891357 145.512 -1.6386834383010864
		 146.548 -1.7117172479629517 147.584 -1.8143525123596191 148.616 -1.9496308565139771
		 149.652 -2.0747208595275879 150.688 -2.1618297100067139 151.72 -2.2264580726623535
		 152.756 -2.279383659362793 153.788 -2.3217751979827881 154.824 -2.3591244220733643
		 155.86 -2.3844952583312988 156.892 -2.3948793411254883 157.928 -2.4066767692565918
		 158.964 -2.4225227832794189 159.996 -2.4285581111907959 161.032 -2.4158275127410889
		 162.064 -2.3885507583618164 163.1 -2.369173526763916 164.136 -2.3716409206390381
		 165.168 -2.3826544284820557 166.204 -2.3908395767211914 167.236 -2.3978145122528076
		 168.272 -2.4024732112884521 169.308 -2.3994824886322021 170.34 -2.3971991539001465
		 171.376 -2.4294364452362061 172.412 -2.5197949409484863 173.444 -2.6411809921264648
		 174.48 -2.7415597438812256 175.512 -2.808835506439209 176.548 -2.871753454208374
		 177.584 -2.94498610496521 178.616 -3.0263428688049316 179.652 -3.1099939346313477
		 180.688 -3.1814086437225342 181.72 -3.2334818840026855 182.756 -3.2703683376312256
		 183.788 -3.2956428527832031 184.824 -3.3103559017181396 185.86 -3.3179106712341309
		 186.892 -3.3147308826446533 187.928 -3.2756435871124268 188.964 -3.1982784271240234
		 189.996 -3.1244704723358154 191.032 -3.0699958801269531 192.064 -3.0113708972930908
		 193.1 -2.941171407699585 194.136 -2.8705480098724365 195.168 -2.8026182651519775
		 196.204 -2.7317113876342773 197.236 -2.6518549919128418 198.272 -2.5637581348419189
		 199.308 -2.4745719432830811 200.34 -2.3851866722106934 201.376 -2.2907745838165283
		 202.412 -2.1918962001800537 203.444 -2.096815824508667 204.48 -2.0113556385040283
		 205.512 -1.9299591779708862 206.548 -1.8509985208511353 207.584 -1.780774712562561
		 208.616 -1.7179005146026611 209.652 -1.6594299077987671 210.688 -1.6092613935470581
		 211.72 -1.5711677074432373 212.756 -1.5443961620330811 213.788 -1.5248101949691772
		 214.824 -1.5086996555328369 215.86 -1.4997737407684326 216.892 -1.5045760869979858
		 217.928 -1.5173633098602295 218.964 -1.5314151048660278 219.996 -1.5534731149673462
		 221.032 -1.5866445302963257 222.064 -1.624426007270813 223.1 -1.6642839908599854
		 224.136 -1.7088152170181274 225.168 -1.7603956460952759 226.204 -1.8154385089874268
		 227.236 -1.8620434999465942 228.272 -1.8951292037963867 229.308 -1.9242335557937622
		 230.34 -1.9562952518463135 231.376 -1.9896173477172852 232.412 -2.0221161842346191
		 233.444 -2.0469212532043457 234.48 -2.0577616691589355 235.512 -2.0623714923858643
		 236.548 -2.0679583549499512 237.584 -2.0742835998535156 238.616 -2.0858871936798096
		 239.652 -2.1026747226715088 240.688 -2.1176822185516357 241.72 -2.1275358200073242
		 242.756 -2.1250429153442383 243.788 -2.1042807102203369 244.824 -2.0751035213470459
		 245.86 -2.0503349304199219 246.892 -2.027390718460083 247.928 -1.9983628988265991
		 248.96 -1.9692317247390747 249.996 -1.9512145519256592 251.032 -1.9427338838577271
		 252.064 -1.9338018894195557 253.1 -1.9185436964035034 254.136 -1.9030214548110962
		 255.168 -1.9059931039810181 256.204 -1.9343574047088623 257.236 -1.9691448211669922
		 258.272 -2.0000884532928467;
	setAttr ".ktv[250:290]" 259.308 -2.0362062454223633 260.34 -2.0782155990600586
		 261.376 -2.1201496124267578 262.412 -2.1605713367462158 263.444 -2.2010231018066406
		 264.48 -2.2435545921325684 265.512 -2.2877051830291748 266.548 -2.3287923336029053
		 267.584 -2.3617174625396729 268.616 -2.3890330791473389 269.652 -2.4142041206359863
		 270.688 -2.4374194145202637 271.72 -2.4598443508148193 272.756 -2.4770243167877197
		 273.788 -2.4824361801147461 274.824 -2.4737021923065186 275.86 -2.4534940719604492
		 276.892 -2.432370662689209 277.928 -2.4129385948181152 278.96 -2.3845739364624023
		 279.996 -2.3474128246307373 281.032 -2.3146905899047852 282.064 -2.2901771068572998
		 283.1 -2.2754206657409668 284.136 -2.2520401477813721 285.168 -2.2289354801177979
		 286.204 -2.201397180557251 287.236 -2.1715340614318848 288.272 -2.1502523422241211
		 289.308 -2.1447463035583496 290.34 -2.1506273746490479 291.376 -2.1554446220397949
		 292.412 -2.1550517082214355 293.444 -2.1598381996154785 294.48 -2.1777744293212891
		 295.512 -2.2051951885223389 296.548 -2.2322061061859131 297.584 -2.2501368522644043
		 298.616 -2.2612733840942383 299.652 -2.2747182846069336 300.688 -2.2938039302825928;
createNode animCurveTL -n "Bip01_Spine_translateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 291 ".ktv";
	setAttr ".ktv[0:249]"  0.688 67.289604187011719 1.72 67.264602661132813 2.756 67.230796813964844
		 3.788 67.195442199707031 4.824 67.169029235839844 5.86 67.1473388671875 6.892 67.120170593261719
		 7.928 67.08587646484375 8.964 67.050010681152344 9.996 67.012969970703125 11.032 66.972610473632813
		 12.064 66.931632995605469 13.1 66.882682800292969 14.136 66.812454223632812 15.168 66.723434448242188
		 16.204 66.619598388671875 17.24 66.5050048828125 18.272 66.390113830566406 19.308 66.279060363769531
		 20.34 66.178512573242188 21.376 66.090126037597656 22.412 66.000373840332031 23.444 65.904434204101562
		 24.48 65.817100524902344 25.512 65.756385803222656 26.548 65.720893859863281 27.584 65.694725036621094
		 28.616 65.674331665039063 29.652 65.672592163085937 30.688 65.693901062011719 31.72 65.717071533203125
		 32.756 65.725990295410156 33.788 65.734321594238281 34.824 65.756752014160156 35.86 65.792678833007812
		 36.892 65.838462829589844 37.928 65.891311645507813 38.964 65.948509216308594 39.996 66.010467529296875
		 41.032 66.081817626953125 42.064 66.161346435546875 43.1 66.250076293945313 44.136 66.354347229003906
		 45.168 66.466415405273438 46.204 66.594406127929688 47.24 66.759048461914062 48.272 66.943183898925781
		 49.308 67.118873596191406 50.34 67.279159545898437 51.376 67.43316650390625 52.412 67.594497680664063
		 53.444 67.755630493164062 54.48 67.907318115234375 55.512 68.064498901367188 56.548 68.217872619628906
		 57.584 68.330833435058594 58.616 68.40533447265625 59.652 68.46636962890625 60.688 68.519699096679687
		 61.72 68.563766479492187 62.756 68.599082946777344 63.788 68.622901916503906 64.824 68.633087158203125
		 65.86 68.642120361328125 66.892 68.663475036621094 67.928 68.690330505371094 68.964 68.713691711425781
		 69.996 68.726577758789063 71.032 68.724555969238281 72.064 68.725967407226563 73.1 68.743743896484375
		 74.136 68.758102416992188 75.168 68.755744934082031 76.204 68.749214172363281 77.24 68.740776062011719
		 78.272 68.722602844238281 79.308 68.703018188476562 80.34 68.690216064453125 81.376 68.682563781738281
		 82.412 68.677993774414063 83.444 68.67266845703125 84.48 68.662467956542969 85.512 68.643394470214844
		 86.548 68.615966796875 87.584 68.596847534179687 88.616 68.601432800292969 89.652 68.603179931640625
		 90.688 68.558555603027344 91.72 68.484298706054688 92.756 68.426345825195312 93.788 68.358184814453125
		 94.824 68.241127014160156 95.86 68.111007690429688 96.892 67.990943908691406 97.928 67.866706848144531
		 98.964 67.763107299804688 99.996 67.718276977539063 101.032 67.726234436035156 102.064 67.748817443847656
		 103.1 67.755874633789063 104.136 67.738662719726563 105.168 67.703262329101563 106.204 67.665542602539063
		 107.24 67.631690979003906 108.272 67.5958251953125 109.308 67.560043334960937 110.34 67.527458190917969
		 111.376 67.494094848632813 112.412 67.461555480957031 113.444 67.435997009277344
		 114.48 67.417617797851563 115.512 67.398956298828125 116.548 67.37548828125 117.584 67.354667663574219
		 118.616 67.338127136230469 119.652 67.318115234375 120.688 67.297508239746094 121.72 67.282501220703125
		 122.756 67.274314880371094 123.788 67.274787902832031 124.824 67.27752685546875 125.86 67.273712158203125
		 126.892 67.269577026367188 127.928 67.274421691894531 128.964 67.29156494140625 129.996 67.327339172363281
		 131.032 67.381683349609375 132.064 67.439300537109375 133.1 67.499153137207031 134.136 67.577804565429688
		 135.168 67.668197631835938 136.204 67.748329162597656 137.236 67.814620971679688
		 138.272 67.868881225585938 139.308 67.904403686523438 140.34 67.927169799804688 141.376 67.970329284667969
		 142.412 68.051994323730469 143.444 68.144805908203125 144.48 68.235687255859375 145.512 68.345344543457031
		 146.548 68.481117248535156 147.584 68.655929565429688 148.616 68.881027221679688
		 149.652 69.114509582519531 150.688 69.314308166503906 151.72 69.47674560546875 152.756 69.594833374023438
		 153.788 69.667938232421875 154.824 69.714836120605469 155.86 69.746482849121094 156.892 69.768692016601563
		 157.928 69.786567687988281 158.964 69.795440673828125 159.996 69.7967529296875 161.032 69.804107666015625
		 162.064 69.819267272949219 163.1 69.830032348632813 164.136 69.833412170410156 165.168 69.8336181640625
		 166.204 69.829750061035156 167.236 69.820526123046875 168.272 69.808364868164063
		 169.308 69.798721313476563 170.34 69.7919921875 171.376 69.784156799316406 172.412 69.773345947265625
		 173.444 69.752586364746094 174.48 69.722602844238281 175.512 69.702003479003906 176.548 69.692062377929687
		 177.584 69.671554565429687 178.616 69.644866943359375 179.652 69.638572692871094
		 180.688 69.647018432617188 181.72 69.633987426757812 182.756 69.588577270507812 183.788 69.529083251953125
		 184.824 69.443214416503906 185.86 69.314590454101563 186.892 69.176918029785156 187.928 69.044517517089844
		 188.964 68.915534973144531 189.996 68.827690124511719 191.032 68.794395446777344
		 192.064 68.79010009765625 193.1 68.800033569335938 194.136 68.821502685546875 195.168 68.848983764648438
		 196.204 68.868850708007812 197.236 68.871337890625 198.272 68.861717224121094 199.308 68.852821350097656
		 200.34 68.848335266113281 201.376 68.838859558105469 202.412 68.822006225585937 203.444 68.808258056640625
		 204.48 68.8011474609375 205.512 68.794754028320312 206.548 68.787918090820313 207.584 68.783065795898438
		 208.616 68.782051086425781 209.652 68.783645629882813 210.688 68.780548095703125
		 211.72 68.771522521972656 212.756 68.763748168945313 213.788 68.759368896484375 214.824 68.756202697753906
		 215.86 68.75567626953125 216.892 68.759468078613281 217.928 68.761672973632812 218.964 68.760147094726563
		 219.996 68.759513854980469 221.032 68.758567810058594 222.064 68.765907287597656
		 223.1 68.791236877441406 224.136 68.815284729003906 225.168 68.821090698242188 226.204 68.825859069824219
		 227.236 68.845382690429687 228.272 68.865364074707031 229.308 68.872711181640625
		 230.34 68.873855590820313 231.376 68.873992919921875 232.412 68.869499206542969 233.444 68.85430908203125
		 234.48 68.827285766601563 235.512 68.787590026855469 236.548 68.727668762207031 237.584 68.648292541503906
		 238.616 68.5643310546875 239.652 68.492576599121094 240.688 68.439987182617188 241.72 68.399681091308594
		 242.756 68.367439270019531 243.788 68.345947265625 244.824 68.329780578613281 245.86 68.312957763671875
		 246.892 68.299468994140625 247.928 68.289634704589844 248.96 68.2708740234375 249.996 68.237701416015625
		 251.032 68.200408935546875 252.064 68.1663818359375 253.1 68.143257141113281 254.136 68.141845703125
		 255.168 68.162483215332031 256.204 68.1947021484375 257.236 68.2275390625 258.272 68.258476257324219;
	setAttr ".ktv[250:290]" 259.308 68.286590576171875 260.34 68.311874389648438
		 261.376 68.340507507324219 262.412 68.369369506835938 263.444 68.393463134765625
		 264.48 68.421676635742187 265.512 68.463081359863281 266.548 68.523063659667969 267.584 68.596054077148438
		 268.616 68.660003662109375 269.652 68.703529357910156 270.688 68.73095703125 271.72 68.744270324707031
		 272.756 68.747222900390625 273.788 68.7459716796875 274.824 68.737472534179688 275.86 68.721427917480469
		 276.892 68.707290649414062 277.928 68.696731567382812 278.96 68.685325622558594 279.996 68.67266845703125
		 281.032 68.660995483398438 282.064 68.65557861328125 283.1 68.654998779296875 284.136 68.6494140625
		 285.168 68.648345947265625 286.204 68.671653747558594 287.236 68.710685729980469
		 288.272 68.74359130859375 289.308 68.764907836914062 290.34 68.7828369140625 291.376 68.80511474609375
		 292.412 68.829666137695312 293.444 68.851829528808594 294.48 68.869163513183594 295.512 68.880508422851562
		 296.548 68.888885498046875 297.584 68.894004821777344 298.616 68.891777038574219
		 299.652 68.885871887207031 300.688 68.879051208496094;
createNode animCurveTL -n "Bip01_Spine_translateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 291 ".ktv";
	setAttr ".ktv[0:249]"  0.688 -1.1871140003204346 1.72 -1.1070411205291748
		 2.756 -1.0233724117279053 3.788 -0.90156090259552002 4.824 -0.75519883632659912 5.86 -0.61214441061019897
		 6.892 -0.48657101392745972 7.928 -0.38084986805915833 8.964 -0.28332230448722839
		 9.996 -0.17947934567928314 11.032 -0.06169457733631134 12.064 0.062157616019248962
		 13.1 0.17093390226364136 14.136 0.25255107879638672 15.168 0.31745797395706177 16.204 0.38438719511032104
		 17.24 0.45258495211601257 18.272 0.50985580682754517 19.308 0.55693256855010986 20.34 0.60244572162628174
		 21.376 0.65027964115142822 22.412 0.6956641674041748 23.444 0.7322431206703186 24.48 0.75551909208297729
		 25.512 0.76076960563659668 26.548 0.74920558929443359 27.584 0.72591495513916016
		 28.616 0.69489932060241699 29.652 0.66129153966903687 30.688 0.62421637773513794
		 31.72 0.57660222053527832 32.756 0.5207514762878418 33.788 0.465963214635849 34.824 0.41185396909713745
		 35.86 0.35419785976409912 36.892 0.30227953195571899 37.928 0.26692798733711243 38.964 0.24480044841766357
		 39.996 0.22496072947978973 41.032 0.19761696457862854 42.064 0.16102093458175659
		 43.1 0.1095920130610466 44.136 0.032469920814037323 45.168 -0.052868958562612534
		 46.204 -0.10937248170375824 47.24 -0.13318303227424622 48.272 -0.15434280037879944
		 49.308 -0.19420559704303741 50.34 -0.24860204756259918 51.376 -0.3030393123626709
		 52.412 -0.3606438934803009 53.444 -0.43446844816207886 54.48 -0.51935774087905884
		 55.512 -0.60692960023880005 56.548 -0.69871574640274048 57.584 -0.79063236713409424
		 58.616 -0.87157917022705078 59.652 -0.94361966848373413 60.688 -1.0199829339981079
		 61.72 -1.0967239141464233 62.756 -1.1659424304962158 63.788 -1.2338982820510864 64.824 -1.3015402555465698
		 65.86 -1.3706984519958496 66.892 -1.4543945789337158 67.928 -1.5533615350723267 68.964 -1.6537189483642578
		 69.996 -1.7475943565368652 71.032 -1.8337286710739136 72.064 -1.9172199964523315
		 73.1 -2.0075469017028809 74.136 -2.0992498397827148 75.168 -2.1800172328948975 76.204 -2.2526786327362061
		 77.24 -2.3291287422180176 78.272 -2.4141011238098145 79.308 -2.4938919544219971 80.34 -2.555504322052002
		 81.376 -2.6077947616577148 82.412 -2.6558449268341064 83.444 -2.6875097751617432
		 84.48 -2.7087841033935547 85.512 -2.7536942958831787 86.548 -2.8282961845397949 87.584 -2.8977575302124023
		 88.616 -2.9518516063690186 89.652 -3.0187804698944092 90.688 -3.1139514446258545
		 91.72 -3.2036831378936768 92.756 -3.2440981864929199 93.788 -3.2648758888244629 94.824 -3.3230690956115723
		 95.86 -3.4037854671478271 96.892 -3.4815676212310791 97.928 -3.5730845928192139 98.964 -3.6635091304779053
		 99.996 -3.7115981578826904 101.032 -3.7300474643707275 102.064 -3.7643439769744873
		 103.1 -3.8276069164276123 104.136 -3.9027726650238037 105.168 -3.9714906215667725
		 106.204 -4.0301671028137207 107.24 -4.0839018821716309 108.272 -4.1287088394165039
		 109.308 -4.1609811782836914 110.34 -4.1898245811462402 111.376 -4.2216715812683105
		 112.412 -4.2507996559143066 113.444 -4.2688937187194824 114.48 -4.2775435447692871
		 115.512 -4.2856802940368652 116.548 -4.29339599609375 117.584 -4.2933230400085449
		 118.616 -4.2823171615600586 119.652 -4.2654323577880859 120.688 -4.2574372291564941
		 121.72 -4.2694578170776367 122.756 -4.2870926856994629 123.788 -4.2709050178527832
		 124.824 -4.2048649787902832 125.86 -4.130246639251709 126.892 -4.0816850662231445
		 127.928 -4.0493645668029785 128.964 -4.0204968452453613 129.996 -3.9746150970458984
		 131.032 -3.8991053104400635 132.064 -3.8215067386627197 133.1 -3.7421865463256836
		 134.136 -3.6249375343322754 135.168 -3.4812507629394531 136.204 -3.3449904918670654
		 137.236 -3.2139284610748291 138.272 -3.0889544486999512 139.308 -2.9961748123168945
		 140.34 -2.946774959564209 141.376 -2.9081249237060547 142.412 -2.8371477127075195
		 143.444 -2.7167770862579346 144.48 -2.5306012630462646 145.512 -2.3033990859985352
		 146.548 -2.1074612140655518 147.584 -1.9249582290649414 148.616 -1.7084630727767944
		 149.652 -1.5066229104995728 150.688 -1.335777759552002 151.72 -1.1541845798492432
		 152.756 -0.98210608959197998 153.788 -0.86055922508239746 154.824 -0.77672618627548218
		 155.86 -0.70026606321334839 156.892 -0.62260669469833374 157.928 -0.54308050870895386
		 158.964 -0.46081775426864624 159.996 -0.38059765100479126 161.032 -0.3066307008266449
		 162.064 -0.23808753490447998 163.1 -0.17050661146640778 164.136 -0.10413337498903275
		 165.168 -0.044994965195655823 166.204 0.01001653540879488 167.236 0.069113865494728088
		 168.272 0.12823054194450378 169.308 0.17675414681434631 170.34 0.21203154325485229
		 171.376 0.23961840569972992 172.412 0.2657255232334137 173.444 0.28870537877082825
		 174.48 0.29985189437866211 175.512 0.29328987002372742 176.548 0.27425315976142883
		 177.584 0.25659513473510742 178.616 0.2484007328748703 179.652 0.24856868386268616
		 180.688 0.24938036501407623 181.72 0.22541707754135132 182.756 0.15540926158428192
		 183.788 0.061966747045516968 184.824 -0.037294670939445496 185.86 -0.15688712894916534
		 186.892 -0.29046222567558289 187.928 -0.43634301424026489 188.964 -0.59228640794754028
		 189.996 -0.70885759592056274 191.032 -0.76572901010513306 192.064 -0.79656267166137695
		 193.1 -0.81525945663452148 194.136 -0.82468217611312866 195.168 -0.8459201455116272
		 196.204 -0.88240945339202881 197.236 -0.91350913047790527 198.272 -0.93109720945358276
		 199.308 -0.93874049186706543 200.34 -0.93800938129425049 201.376 -0.93436366319656372
		 202.412 -0.92985934019088745 203.444 -0.91633206605911255 204.48 -0.89330971240997314
		 205.512 -0.87407463788986206 206.548 -0.86566537618637085 207.584 -0.86114120483398438
		 208.616 -0.8513454794883728 209.652 -0.83357882499694824 210.688 -0.80923002958297729
		 211.72 -0.77989131212234497 212.756 -0.74727725982666016 213.788 -0.71329951286315918
		 214.824 -0.67680895328521729 215.86 -0.63785392045974731 216.892 -0.60464662313461304
		 217.928 -0.57608515024185181 218.964 -0.53782963752746582 219.996 -0.48413416743278503
		 221.032 -0.4184286892414093 222.064 -0.34693282842636108 223.1 -0.2729010283946991
		 224.136 -0.19244018197059631 225.168 -0.10915657877922058 226.204 -0.033890899270772934
		 227.236 0.036160513758659363 228.272 0.11130509525537491 229.308 0.19150592386722565
		 230.34 0.27565202116966248 231.376 0.36445128917694092 232.412 0.45126119256019592
		 233.444 0.52856618165969849 234.48 0.59492218494415283 235.512 0.65723919868469238
		 236.548 0.72030502557754517 237.584 0.77764087915420532 238.616 0.82700371742248535
		 239.652 0.86991655826568604 240.688 0.90318793058395386 241.72 0.92415004968643188
		 242.756 0.92770719528198242 243.788 0.91602241992950439 244.824 0.90318578481674194
		 245.86 0.89532387256622314 246.892 0.88894462585449219 247.928 0.88014990091323853
		 248.96 0.86502861976623535 249.996 0.84497243165969849 251.032 0.82594811916351318
		 252.064 0.81074374914169312 253.1 0.79989618062973022 254.136 0.78880167007446289
		 255.168 0.76806962490081787 256.204 0.73796910047531128 257.236 0.70875543355941772
		 258.272 0.68646043539047241;
	setAttr ".ktv[250:290]" 259.308 0.66518431901931763 260.34 0.6376570463180542
		 261.376 0.61110919713973999 262.412 0.59046775102615356 263.444 0.56771653890609741
		 264.48 0.54661047458648682 265.512 0.53491067886352539 266.548 0.52535861730575562
		 267.584 0.51570218801498413 268.616 0.51609963178634644 269.652 0.52805036306381226
		 270.688 0.53781569004058838 271.72 0.5371508002281189 272.756 0.53018659353256226
		 273.788 0.52084827423095703 274.824 0.51282596588134766 275.86 0.50627100467681885
		 276.892 0.49860551953315735 277.928 0.48932039737701416 278.96 0.47141158580780029
		 279.996 0.4405810534954071 281.032 0.40495195984840393 282.064 0.37163591384887695
		 283.1 0.34162092208862305 284.136 0.32447069883346558 285.168 0.30922335386276245
		 286.204 0.28075584769248962 287.236 0.2376864105463028 288.272 0.19998776912689209
		 289.308 0.18401910364627838 290.34 0.17893745005130768 291.376 0.16547989845275879
		 292.412 0.14873097836971283 293.444 0.14516860246658325 294.48 0.14847436547279358
		 295.512 0.14087490737438202 296.548 0.12426555156707764 297.584 0.11626876890659332
		 298.616 0.1184350997209549 299.652 0.11439797282218933 300.688 0.10420683771371841;
createNode animCurveTA -n "Bip01_Spine_rotateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 291 ".ktv";
	setAttr ".ktv[0:249]"  0.688 0.36041885614395142 1.72 0.14804576337337494
		 2.756 0.095542162656784058 3.788 0.16114284098148346 4.824 0.26640883088111877 5.86 0.36631053686141968
		 6.892 0.45767137408256536 7.928 0.60554546117782593 8.964 0.82294690608978271 9.996 1.0387864112854004
		 11.032 1.2207694053649902 12.064 1.3853667974472046 13.1 1.5417369604110718 14.136 1.6855605840682983
		 15.168 1.8178377151489256 16.204 1.9473884105682373 17.24 2.0694165229797363 18.272 2.1674566268920898
		 19.308 2.2575855255126953 20.34 2.3696341514587402 21.376 2.4648478031158447 22.412 2.4901115894317627
		 23.444 2.4792647361755371 24.48 2.4812366962432861 25.512 2.4879376888275146 26.548 2.4802663326263428
		 27.584 2.4593281745910645 28.616 2.4403219223022461 29.652 2.4400062561035156 30.688 2.4557695388793945
		 31.72 2.4744751453399658 32.756 2.5099797248840332 33.788 2.5823369026184082 34.824 2.669116735458374
		 35.86 2.7446582317352295 36.892 2.824653148651123 37.928 2.9153633117675781 38.964 3.001490592956543
		 39.996 3.0773684978485107 41.032 3.1326892375946045 42.064 3.1553757190704346 43.1 3.1241068840026855
		 44.136 3.0049457550048828 45.168 2.7962241172790527 46.204 2.5481612682342529 47.24 2.3341026306152344
		 48.272 2.1804563999176025 49.308 2.055772066116333 50.34 1.9247516393661499 51.376 1.7407660484313965
		 52.412 1.4994758367538452 53.444 1.3021363019943237 54.48 1.2378479242324829 55.512 1.3257683515548706
		 56.548 1.5810172557830811 57.584 1.9919769763946533 58.616 2.4829559326171875 59.652 2.9310817718505859
		 60.688 3.2979869842529297 61.72 3.7244725227355961 62.756 4.2537145614624023 63.788 4.6948161125183105
		 64.824 4.9456868171691895 65.86 5.0892081260681152 66.892 5.180964469909668 67.928 5.2104072570800781
		 68.964 5.1977601051330566 69.996 5.1914935111999512 71.032 5.2144985198974609 72.064 5.257746696472168
		 73.1 5.2939414978027344 74.136 5.309363842010498 75.168 5.3172378540039062 76.204 5.3247389793395996
		 77.24 5.3292379379272461 78.272 5.3477878570556641 79.308 5.4080328941345215 80.34 5.4967923164367676
		 81.376 5.5688176155090332 82.412 5.628727912902832 83.444 5.7226715087890625 84.48 5.8362345695495605
		 85.512 5.9197144508361816 86.548 5.9748601913452148 87.584 6.0131959915161133 88.616 6.0289559364318848
		 89.652 6.0257506370544434 90.688 6.0139570236206055 91.72 6.0338335037231445 92.756 6.1375236511230469
		 93.788 6.2990517616271973 94.824 6.4251518249511719 95.86 6.4759502410888672 96.892 6.4922943115234375
		 97.928 6.5355730056762695 98.964 6.6763825416564941 99.996 6.9471311569213867 101.032 7.2511625289916983
		 102.064 7.4185762405395508 103.1 7.4221358299255371 104.136 7.4636459350585938 105.168 7.6789917945861825
		 106.204 7.9338202476501465 107.24 8.1032581329345703 108.272 8.2432012557983398 109.308 8.4586334228515625
		 110.34 8.7511491775512695 111.376 8.9538650512695313 112.412 8.9929447174072266 113.444 9.0136899948120117
		 114.48 9.0414876937866211 115.512 8.9445457458496094 116.548 8.7442474365234375 117.584 8.5836267471313477
		 118.616 8.4990701675415039 119.652 8.4195175170898437 120.688 8.201817512512207 121.72 7.6993389129638672
		 122.756 7.0717473030090332 123.788 6.752312183380127 124.824 6.8953084945678711 125.86 7.194997787475585
		 126.892 7.3054280281066895 127.928 7.0972075462341309 128.964 6.5329036712646484
		 129.996 5.7529549598693848 131.032 5.0823798179626465 132.064 4.6021103858947754
		 133.1 4.1911888122558594 134.136 3.8932068347930908 135.168 3.7642054557800293 136.204 3.7241694927215572
		 137.236 3.7212927341461186 138.272 3.7525122165679932 139.308 3.7999448776245117
		 140.34 3.8667585849761963 141.376 3.9688844680786128 142.412 4.0786828994750977 143.444 4.151397705078125
		 144.48 4.1934847831726074 145.512 4.2512316703796387 146.548 4.3450384140014648 147.584 4.4535493850708008
		 148.616 4.5572328567504883 149.652 4.6381449699401855 150.688 4.661766529083252 151.72 4.6254544258117676
		 152.756 4.5609121322631836 153.788 4.4807672500610352 154.824 4.3867015838623047
		 155.86 4.2908797264099121 156.892 4.1992182731628418 157.928 4.1079010963439941 158.964 3.9910240173339839
		 159.996 3.7505304813385005 161.032 3.3060028553009033 162.064 2.7742016315460205
		 163.1 2.328188419342041 164.136 1.9676520824432373 165.168 1.6333043575286865 166.204 1.2767015695571899
		 167.236 0.81278049945831299 168.272 0.31384408473968506 169.308 0.00011187205382157117
		 170.34 -0.12501107156276703 171.376 -0.23837846517562869 172.412 -0.35498914122581482
		 173.444 -0.42296826839447021 174.48 -0.52843189239501953 175.512 -0.70722377300262451
		 176.548 -0.86631381511688232 177.584 -0.97558039426803589 178.616 -1.0768125057220459
		 179.652 -1.1584491729736328 180.688 -1.1763525009155273 181.72 -1.1448266506195068
		 182.756 -1.104034423828125 183.788 -1.0506327152252197 184.824 -0.96803498268127441
		 185.86 -0.86505275964736938 186.892 -0.75542545318603516 187.928 -0.63469654321670532
		 188.964 -0.49343466758728027 189.996 -0.33881017565727234 191.032 -0.18072168529033661
		 192.064 -0.0097678415477275848 193.1 0.18003152310848236 194.136 0.37270033359527588
		 195.168 0.55744057893753052 196.204 0.73714762926101685 197.236 0.91487050056457531
		 198.272 1.0939807891845703 199.308 1.2710984945297241 200.34 1.4424440860748291 201.376 1.6209503412246704
		 202.412 1.804158568382263 203.444 1.9590975046157837 204.48 2.0782027244567871 205.512 2.1852564811706543
		 206.548 2.2849931716918945 207.584 2.3697757720947266 208.616 2.4438230991363525
		 209.652 2.506965160369873 210.688 2.5476763248443604 211.72 2.5646648406982422 212.756 2.5701797008514404
		 213.788 2.5698645114898682 214.824 2.5629401206970215 215.86 2.5493142604827881 216.892 2.5266637802124023
		 217.928 2.490530252456665 218.964 2.4359478950500488 219.996 2.3669064044952393 221.032 2.296053409576416
		 222.064 2.230992317199707 223.1 2.1698687076568604 224.136 2.1035399436950684 225.168 2.0297443866729736
		 226.204 1.9587252140045166 227.236 1.8957597017288208 228.272 1.837510347366333 229.308 1.7813242673873901
		 230.34 1.7252037525177002 231.376 1.6650946140289307 232.412 1.5958493947982788 233.444 1.5153878927230835
		 234.48 1.4296852350234985 235.512 1.3478766679763794 236.548 1.2706581354141235 237.584 1.1920901536941528
		 238.616 1.1076817512512207 239.652 1.0124210119247437 240.688 0.90154510736465443
		 241.72 0.78129899501800537 242.756 0.66568851470947266 243.788 0.55710870027542114
		 244.824 0.44580698013305664 245.86 0.32832828164100647 246.892 0.212236687541008
		 247.928 0.1062098890542984 248.96 0.011100537143647671 249.996 -0.07447969913482666
		 251.032 -0.14722834527492523 252.064 -0.2079085111618042 253.1 -0.25737631320953369
		 254.136 -0.28840047121047974 255.168 -0.2977251410484314 256.204 -0.28806188702583313
		 257.236 -0.26072019338607788 258.272 -0.22031886875629425;
	setAttr ".ktv[250:290]" 259.308 -0.17485328018665314 260.34 -0.12464982271194457
		 261.376 -0.060904558748006821 262.412 0.012711900286376476 263.444 0.078398346900939941
		 264.48 0.13661929965019226 265.512 0.19856125116348267 266.548 0.25740408897399902
		 267.584 0.30295705795288086 268.616 0.33605995774269104 269.652 0.35796549916267395
		 270.688 0.36546686291694641 271.72 0.3579278290271759 272.756 0.34209686517715454
		 273.788 0.32586002349853516 274.824 0.31396540999412537 275.86 0.30421295762062073
		 276.892 0.28398239612579346 277.928 0.2516142725944519 278.96 0.22335667908191681
		 279.996 0.20669074356555939 281.032 0.19844718277454376 282.064 0.20034325122833252
		 283.1 0.21795456111431122 284.136 0.23208884894847873 285.168 0.25120508670806885
		 286.204 0.29116204380989075 287.236 0.35485264658927917 288.272 0.43041485548019409
		 289.308 0.50683319568634033 290.34 0.58282065391540527 291.376 0.66589391231536865
		 292.412 0.75552558898925781 293.444 0.84423428773880005 294.48 0.92116171121597301
		 295.512 0.96571993827819824 296.548 1.0053378343582153 297.584 1.1253283023834229
		 298.616 1.3264830112457275 299.652 1.5129086971282959 300.688 1.6475543975830078;
createNode animCurveTA -n "Bip01_Spine_rotateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 291 ".ktv";
	setAttr ".ktv[0:249]"  0.688 -1.4465878009796143 1.72 -1.6058425903320312
		 2.756 -1.7855679988861084 3.788 -1.9339724779129028 4.824 -2.0228950977325439 5.86 -2.0701026916503906
		 6.892 -2.1006748676300049 7.928 -2.1182770729064941 8.964 -2.1250636577606201 9.996 -2.1209385395050049
		 11.032 -2.0967051982879639 12.064 -2.0551326274871826 13.1 -2.0128574371337891 14.136 -1.9794768095016482
		 15.168 -1.9484351873397827 16.204 -1.9110019207000732 17.24 -1.8703511953353884 18.272 -1.8354407548904421
		 19.308 -1.8092000484466553 20.34 -1.7861499786376953 21.376 -1.762903094291687 22.412 -1.7457065582275391
		 23.444 -1.7355973720550537 24.48 -1.7243976593017578 25.512 -1.7093740701675415 26.548 -1.6922224760055542
		 27.584 -1.6738629341125488 28.616 -1.6576149463653564 29.652 -1.6490706205368042
		 30.688 -1.6520665884017944 31.72 -1.6665899753570557 32.756 -1.6906180381774902 33.788 -1.7253155708312988
		 34.824 -1.7676777839660645 35.86 -1.8238676786422729 36.892 -1.9337376356124878 37.928 -2.1165804862976074
		 38.964 -2.3661048412322998 39.996 -2.704526424407959 41.032 -3.0985841751098633 42.064 -3.4294142723083496
		 43.1 -3.6230552196502686 44.136 -3.7169444561004639 45.168 -3.832301139831543 46.204 -4.0892181396484375
		 47.24 -4.4832801818847656 48.272 -4.877629280090332 49.308 -5.157264232635498 50.34 -5.3391880989074707
		 51.376 -5.5145430564880371 52.412 -5.7216253280639648 53.444 -5.9036679267883301
		 54.48 -5.9987459182739258 55.512 -5.9830141067504883 56.548 -5.8497943878173828 57.584 -5.6767945289611816
		 58.616 -5.5895371437072754 59.652 -5.5983810424804687 60.688 -5.6052403450012207
		 61.72 -5.5330853462219238 62.756 -5.4062676429748535 63.788 -5.3371710777282715 64.824 -5.4002585411071777
		 65.86 -5.5417952537536621 66.892 -5.6642656326293945 67.928 -5.7395529747009277 68.964 -5.7877187728881836
		 69.996 -5.8191690444946289 71.032 -5.8250136375427246 72.064 -5.7982168197631836
		 73.1 -5.8039531707763672 74.136 -5.9236197471618652 75.168 -6.088498592376709 76.204 -6.1686954498291016
		 77.24 -6.1419291496276855 78.272 -5.9771280288696289 79.308 -5.5972771644592285 80.34 -5.1053762435913086
		 81.376 -4.721806526184082 82.412 -4.4364814758300781 83.444 -4.0386509895324707 84.48 -3.5107035636901855
		 85.512 -3.1139152050018311 86.548 -3.0346798896789551 87.584 -3.172173023223877 88.616 -3.2995517253875732
		 89.652 -3.3014864921569824 90.688 -3.2176117897033691 91.72 -3.1066775321960449 92.756 -2.9846222400665283
		 93.788 -2.8536338806152344 94.824 -2.7118310928344727 95.86 -2.5794336795806885 96.892 -2.4902288913726807
		 97.928 -2.434370756149292 98.964 -2.383070707321167 99.996 -2.3655087947845459 101.032 -2.4268665313720703
		 102.064 -2.5184412002563477 103.1 -2.5559711456298828 104.136 -2.5808999538421631
		 105.168 -2.6778192520141602 106.204 -2.8090391159057617 107.24 -2.9103019237518311
		 108.272 -2.997128963470459 109.308 -3.1213550567626953 110.34 -3.2898876667022705
		 111.376 -3.4206409454345703 112.412 -3.4690115451812744 113.444 -3.5081613063812256
		 114.48 -3.5663399696350098 115.512 -3.5817987918853764 116.548 -3.5515537261962891
		 117.584 -3.5327818393707275 118.616 -3.5358972549438477 119.652 -3.5312883853912354
		 120.688 -3.4912326335906982 121.72 -3.4045910835266113 122.756 -3.2970685958862305
		 123.788 -3.2167391777038574 124.824 -3.1843764781951904 125.86 -3.1718518733978271
		 126.892 -3.1377401351928711 127.928 -3.0708398818969727 128.964 -2.9807887077331543
		 129.996 -2.8635039329528809 131.032 -2.7425973415374756 132.064 -2.6806075572967529
		 133.1 -2.6792311668395996 134.136 -2.67502760887146 135.168 -2.6477031707763672 136.204 -2.6300373077392578
		 137.236 -2.6327109336853027 138.272 -2.6394996643066406 139.308 -2.6376898288726807
		 140.34 -2.6153213977813721 141.376 -2.5791366100311279 142.412 -2.5523591041564941
		 143.444 -2.5325689315795898 144.48 -2.5010008811950684 145.512 -2.4609482288360596
		 146.548 -2.4362239837646484 147.584 -2.4299395084381104 148.616 -2.4116203784942627
		 149.652 -2.3730783462524414 150.688 -2.3418171405792236 151.72 -2.3296570777893066
		 152.756 -2.3233828544616699 153.788 -2.3165872097015381 154.824 -2.3164129257202148
		 155.86 -2.3241569995880127 156.892 -2.3340432643890381 157.928 -2.342759370803833
		 158.964 -2.3459837436676025 159.996 -2.3302321434020996 161.032 -2.2738852500915527
		 162.064 -2.1855812072753906 163.1 -2.1203103065490723 164.136 -2.1062452793121338
		 165.168 -2.1138794422149658 166.204 -2.1154186725616455 167.236 -2.1067700386047363
		 168.272 -2.0684173107147217 169.308 -1.9609236717224119 170.34 -1.8357704877853394
		 171.376 -1.8570207357406618 172.412 -2.0715382099151611 173.444 -2.3461062908172607
		 174.48 -2.5788922309875488 175.512 -2.7645440101623535 176.548 -2.930034875869751
		 177.584 -3.1275177001953125 178.616 -3.4092447757720947 179.652 -3.7144896984100337
		 180.688 -3.8982086181640625 181.72 -3.9697616100311279 182.756 -4.0697779655456543
		 183.788 -4.2329540252685547 184.824 -4.420252799987793 185.86 -4.6374001502990723
		 186.892 -4.8729796409606934 187.928 -5.0654006004333496 188.964 -5.183565616607666
		 189.996 -5.2529969215393066 191.032 -5.3046889305114746 192.064 -5.351036548614502
		 193.1 -5.3961071968078613 194.136 -5.4364509582519531 195.168 -5.4642562866210938
		 196.204 -5.4794182777404785 197.236 -5.4835071563720703 198.272 -5.476313591003418
		 199.308 -5.4648528099060059 200.34 -5.4550104141235352 201.376 -5.4452924728393555
		 202.412 -5.4296998977661133 203.444 -5.4007725715637207 204.48 -5.3609647750854492
		 205.512 -5.3238091468811035 206.548 -5.299644947052002 207.584 -5.2886886596679687
		 208.616 -5.2876572608947754 209.652 -5.2952613830566406 210.688 -5.31109619140625
		 211.72 -5.336491584777832 212.756 -5.3705534934997559 213.788 -5.4068965911865234
		 214.824 -5.4424090385437012 215.86 -5.4844746589660645 216.892 -5.542447566986084
		 217.928 -5.6120290756225586 218.964 -5.6845684051513672 219.996 -5.7643294334411621
		 221.032 -5.8527307510375977 222.064 -5.9418392181396484 223.1 -6.0297551155090332
		 224.136 -6.1215553283691406 225.168 -6.2223696708679199 226.204 -6.3275594711303711
		 227.236 -6.4225492477416992 228.272 -6.5027976036071777 229.308 -6.5824732780456543
		 230.34 -6.6746463775634766 231.376 -6.7731857299804687 232.412 -6.86627197265625
		 233.444 -6.9514641761779785 234.48 -7.0278840065002441 235.512 -7.0954203605651855
		 236.548 -7.1549220085144043 237.584 -7.2057971954345703 238.616 -7.2561149597167969
		 239.652 -7.3109402656555167 240.688 -7.358264923095704 241.72 -7.3913469314575204
		 242.756 -7.4168720245361319 243.788 -7.4391922950744629 244.824 -7.4627285003662109
		 245.86 -7.4927878379821768 246.892 -7.5170454978942862 247.928 -7.511376380920411
		 248.96 -7.4709081649780273 249.996 -7.4125590324401847 251.032 -7.3519005775451651
		 252.064 -7.2925944328308097 253.1 -7.2277569770812988 254.136 -7.1506209373474121
		 255.168 -7.0669150352478027 256.204 -6.9756870269775391 257.236 -6.8614459037780762
		 258.272 -6.727592945098877;
	setAttr ".ktv[250:290]" 259.308 -6.5887513160705566 260.34 -6.4394521713256836
		 261.376 -6.2708969116210937 262.412 -6.088965892791748 263.444 -5.9046750068664551
		 264.48 -5.7218499183654785 265.512 -5.5384807586669922 266.548 -5.3602776527404785
		 267.584 -5.1968269348144531 268.616 -5.0462551116943359 269.652 -4.9005775451660156
		 270.688 -4.7545933723449707 271.72 -4.6046113967895508 272.756 -4.4556140899658203
		 273.788 -4.3214330673217773 274.824 -4.202704906463623 275.86 -4.0895204544067383
		 276.892 -3.9858787059783936 277.928 -3.8953421115875244 278.96 -3.8085951805114746
		 279.996 -3.7276852130889893 281.032 -3.6658480167388912 282.064 -3.6257438659667964
		 283.1 -3.5674006938934326 284.136 -3.5457322597503662 285.168 -3.525825023651123
		 286.204 -3.5003907680511475 287.236 -3.4690842628479004 288.272 -3.4454126358032227
		 289.308 -3.4385058879852295 290.34 -3.4411916732788086 291.376 -3.4375486373901367
		 292.412 -3.4237356185913086 293.444 -3.4133815765380859 294.48 -3.4197678565979004
		 295.512 -3.4454078674316406 296.548 -3.468092679977417 297.584 -3.4511198997497559
		 298.616 -3.4008769989013672 299.652 -3.3588461875915527 300.688 -3.3341236114501953;
createNode animCurveTA -n "Bip01_Spine_rotateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 291 ".ktv";
	setAttr ".ktv[0:249]"  0.688 1.2269382476806641 1.72 2.3533961772918701 2.756 3.5906615257263184
		 3.788 4.5048503875732422 4.824 5.0856547355651855 5.86 5.552067756652832 6.892 5.9733457565307617
		 7.928 6.3536133766174316 8.964 6.6868371963500977 9.996 6.9155316352844238 11.032 7.0240144729614258
		 12.064 7.0663366317749023 13.1 7.0825901031494141 14.136 7.0779366493225098 15.168 7.0601444244384766
		 16.204 7.0386857986450195 17.24 7.0083060264587402 18.272 6.9608912467956543 19.308 6.8992476463317871
		 20.34 6.8296566009521484 21.376 6.7446422576904297 22.412 6.6328492164611816 23.444 6.504234790802002
		 24.48 6.3753499984741211 25.512 6.2459783554077148 26.548 6.1166882514953613 27.584 6.000819206237793
		 28.616 5.8889951705932617 29.652 5.7433943748474121 30.688 5.5570058822631836 31.72 5.3507266044616699
		 32.756 5.1131649017333984 33.788 4.8252143859863281 34.824 4.5236225128173828 35.86 4.214871883392334
		 36.892 3.7865583896636963 37.928 3.1849241256713867 38.964 2.4810314178466797 39.996 1.7429139614105225
		 41.032 1.067841649055481 42.064 0.56440877914428711 43.1 0.30109107494354248 44.136 0.2732013463973999
		 45.168 0.27242276072502136 46.204 0.015640242025256157 47.24 -0.51810282468795776
		 48.272 -1.0711795091629028 49.308 -1.4406064748764038 50.34 -1.6461246013641357 51.376 -1.7840836048126221
		 52.412 -1.9173123836517334 53.444 -2.0643377304077148 54.48 -2.2118480205535889 55.512 -2.3512980937957764
		 56.548 -2.4978063106536865 57.584 -2.7417824268341064 58.616 -3.1965510845184326
		 59.652 -3.7684235572814946 60.688 -4.2849826812744141 61.72 -4.8182172775268555 62.756 -5.4238548278808594
		 63.788 -5.9579825401306152 64.824 -6.4141335487365723 65.86 -6.8764896392822266 66.892 -7.2705311775207528
		 67.928 -7.5197935104370117 68.964 -7.6907653808593759 69.996 -7.8835005760192871
		 71.032 -8.1414270401000977 72.064 -8.4467735290527344 73.1 -8.7395105361938477 74.136 -9.0090703964233398
		 75.168 -9.2757024765014648 76.204 -9.4869384765625 77.24 -9.6138219833374023 78.272 -9.7462053298950195
		 79.308 -10.000590324401855 80.34 -10.364477157592773 81.376 -10.70828914642334 82.412 -11.005233764648438
		 83.444 -11.364398002624512 84.48 -11.790197372436523 85.512 -12.119853019714355 86.548 -12.249457359313965
		 87.584 -12.249544143676758 88.616 -12.232627868652344 89.652 -12.210975646972656
		 90.688 -12.16709041595459 91.72 -12.218887329101563 92.756 -12.508193969726563 93.788 -12.949687957763672
		 94.824 -13.325501441955566 95.86 -13.567885398864746 96.892 -13.752703666687012 97.928 -13.966581344604492
		 98.964 -14.285185813903809 99.996 -14.658438682556152 101.032 -14.882015228271484
		 102.064 -14.870847702026367 103.1 -14.77791213989258 104.136 -14.759494781494139
		 105.168 -14.812203407287596 106.204 -14.868250846862793 107.24 -14.898383140563963
		 108.272 -14.918590545654295 109.308 -14.958535194396973 110.34 -14.996218681335449
		 111.376 -15.002067565917969 112.412 -15.031710624694824 113.444 -15.108686447143553
		 114.48 -15.112530708312987 115.512 -14.926040649414061 116.548 -14.621581077575684
		 117.584 -14.375622749328613 118.616 -14.230587005615234 119.652 -14.101548194885254
		 120.688 -13.85212230682373 121.72 -13.335988998413086 122.756 -12.692401885986328
		 123.788 -12.355362892150879 124.824 -12.496636390686035 125.86 -12.803915977478027
		 126.892 -12.914360046386719 127.928 -12.694073677062988 128.964 -12.088858604431152
		 129.996 -11.174290657043457 131.032 -10.207496643066406 132.064 -9.3420343399047852
		 133.1 -8.592350959777832 134.136 -7.9906282424926749 135.168 -7.5431852340698242
		 136.204 -7.2166910171508789 137.236 -6.9775652885437012 138.272 -6.7450785636901855
		 139.308 -6.4026312828063965 140.34 -5.889183521270752 141.376 -5.272397518157959
		 142.412 -4.7471432685852051 143.444 -4.4958796501159668 144.48 -4.4875388145446777
		 145.512 -4.4152865409851074 146.548 -3.97832202911377 147.584 -3.2448620796203613
		 148.616 -2.4951996803283691 149.652 -1.8640255928039553 150.688 -1.3662484884262085
		 151.72 -0.96072101593017589 152.756 -0.5272676944732666 153.788 -0.044960141181945801
		 154.824 0.33890432119369507 155.86 0.54859858751296997 156.892 0.66479694843292236
		 157.928 0.76107919216156006 158.964 0.86054742336273193 159.996 1.0052694082260132
		 161.032 1.2156122922897339 162.064 1.4260430335998535 163.1 1.5704785585403442 164.136 1.6631171703338623
		 165.168 1.7372616529464722 166.204 1.8002505302429199 167.236 1.8570277690887453
		 168.272 1.9057494401931763 169.308 1.9406569004058838 170.34 1.9561549425125122 171.376 1.9194403886795044
		 172.412 1.8176959753036501 173.444 1.6952282190322876 174.48 1.5912877321243286 175.512 1.510433554649353
		 176.548 1.438389778137207 177.584 1.3409736156463623 178.616 1.1788218021392822 179.652 1.0035862922668457
		 180.688 0.94263595342636097 181.72 0.98816597461700439 182.756 1.0084787607192993
		 183.788 0.96050322055816639 184.824 0.87504112720489502 185.86 0.75748586654663086
		 186.892 0.62376725673675537 187.928 0.51262986660003662 188.964 0.43419268727302551
		 189.996 0.36824926733970642 191.032 0.30089080333709717 192.064 0.23145180940628049
		 193.1 0.1592579185962677 194.136 0.086654327809810638 195.168 0.023951197043061256
		 196.204 -0.023155614733695984 197.236 -0.061304524540901184 198.272 -0.093842089176177979
		 199.308 -0.10825427621603012 200.34 -0.098276488482952118 201.376 -0.085245095193386078
		 202.412 -0.092771567404270172 203.444 -0.11443944275379181 204.48 -0.098235949873924255
		 205.512 0.026852462440729141 206.548 0.2475135326385498 207.584 0.46780416369438166
		 208.616 0.6514439582824707 209.652 0.8382602334022522 210.688 1.0529575347900391
		 211.72 1.281658411026001 212.756 1.5076267719268799 213.788 1.7333911657333374 214.824 1.9607102870941164
		 215.86 2.2003793716430664 216.892 2.4871273040771484 217.928 2.8000752925872803 218.964 3.0565447807312012
		 219.996 3.2342269420623779 221.032 3.3763058185577393 222.064 3.5041201114654541
		 223.1 3.6185810565948486 224.136 3.7371058464050289 225.168 3.8688318729400635 226.204 3.9924519062042236
		 227.236 4.0967535972595215 228.272 4.1953978538513184 229.308 4.2921795845031738
		 230.34 4.3815960884094238 231.376 4.4605069160461426 232.412 4.5224051475524902 233.444 4.5704092979431152
		 234.48 4.6148719787597656 235.512 4.6542983055114746 236.548 4.6792564392089844 237.584 4.6848669052124023
		 238.616 4.6747517585754395 239.652 4.655789852142334 240.688 4.6291589736938477 241.72 4.5886411666870117
		 242.756 4.5327897071838379 243.788 4.4686684608459473 244.824 4.3981680870056152
		 245.86 4.3182849884033203 246.892 4.2271103858947754 247.928 4.1232361793518066 248.96 4.0112528800964355
		 249.996 3.8970439434051518 251.032 3.7747538089752197 252.064 3.638566255569458 253.1 3.4955217838287354
		 254.136 3.3557186126708984 255.168 3.222097635269165 256.204 3.0924463272094727 257.236 2.9659318923950195
		 258.272 2.844346284866333;
	setAttr ".ktv[250:290]" 259.308 2.7328567504882813 260.34 2.6385972499847412
		 261.376 2.5644958019256592 262.412 2.5084099769592285 263.444 2.4645404815673828
		 264.48 2.4248282909393311 265.512 2.3879776000976562 266.548 2.3680300712585449 267.584 2.375831127166748
		 268.616 2.4008388519287109 269.652 2.4332365989685059 270.688 2.4733302593231201
		 271.72 2.5139226913452148 272.756 2.5474021434783936 273.788 2.5798993110656738 274.824 2.6170530319213867
		 275.86 2.6493432521820068 276.892 2.6705911159515381 277.928 2.6871035099029541 278.96 2.6977384090423584
		 279.996 2.6951642036437988 281.032 2.6811561584472656 282.064 2.6623091697692871
		 283.1 2.6260139942169189 284.136 2.5938069820404053 285.168 2.5564985275268555 286.204 2.5120003223419189
		 287.236 2.4593534469604492 288.272 2.4133107662200928 289.308 2.3860688209533691
		 290.34 2.3717937469482422 291.376 2.3629512786865234 292.412 2.360459566116333 293.444 2.3653738498687744
		 294.48 2.3720171451568604 295.512 2.3727893829345703 296.548 2.3843381404876709 297.584 2.4466445446014404
		 298.616 2.5610907077789307 299.652 2.6793015003204346 300.688 2.774167537689209;
createNode animCurveTA -n "Bip01_Spine1_rotateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 291 ".ktv";
	setAttr ".ktv[0:249]"  0.688 -7.0396461486816406 1.72 -6.9203243255615234
		 2.756 -6.8054356575012207 3.788 -6.6982293128967285 4.824 -6.601524829864502 5.86 -6.5175251960754395
		 6.892 -6.44775390625 7.928 -6.3930158615112305 8.964 -6.3533797264099121 9.996 -6.328148365020752
		 11.032 -6.3159890174865723 12.064 -6.3149757385253906 13.1 -6.3227815628051758 14.136 -6.3368973731994629
		 15.168 -6.3547673225402832 16.204 -6.3740100860595703 17.24 -6.3924922943115234 18.272 -6.4084663391113281
		 19.308 -6.4204668998718262 20.34 -6.4275455474853516 21.376 -6.4329977035522461 22.412 -6.4316315650939941
		 23.444 -6.4260210990905762 24.48 -6.4172072410583496 25.512 -6.405360221862793 26.548 -6.4015111923217773
		 27.584 -6.3898944854736328 28.616 -6.3874506950378418 29.652 -6.3851680755615234
		 30.688 -6.3877501487731934 31.72 -6.3937454223632813 32.756 -6.4030570983886719 33.788 -6.4151425361633301
		 34.824 -6.4290785789489746 35.86 -6.4437694549560547 36.892 -6.4580202102661133 37.928 -6.4706892967224121
		 38.964 -6.4807868003845215 39.996 -6.4876055717468262 41.032 -6.4907550811767578
		 42.064 -6.4902234077453613 43.1 -6.486325740814209 44.136 -6.4797172546386719 45.168 -6.4713521003723145
		 46.204 -6.4623608589172363 47.24 -6.4539628028869629 48.272 -6.4473056793212891 49.308 -6.4434471130371094
		 50.34 -6.4431653022766113 51.376 -6.4469552040100098 52.412 -6.4549212455749512 53.444 -6.4667830467224121
		 54.48 -6.4819126129150391 55.512 -6.4993901252746582 56.548 -6.518073558807373 57.584 -6.536674976348877
		 58.616 -6.5538549423217773 59.652 -6.5683503150939941 60.688 -6.5790395736694336
		 61.72 -6.5850996971130371 62.756 -6.5860028266906738 63.788 -6.581566333770752 64.824 -6.5719509124755859
		 65.86 -6.5576019287109375 66.892 -6.5392704010009766 67.928 -6.5179266929626465 68.964 -6.4946646690368652
		 69.996 -6.4706621170043945 71.032 -6.4471096992492676 72.064 -6.425196647644043 73.1 -6.4060606956481934
		 74.136 -6.390810489654541 75.168 -6.380551815032959 76.204 -6.3762607574462891 77.24 -6.378847599029541
		 78.272 -6.3891468048095703 79.308 -6.4078373908996582 80.34 -6.4355287551879883 81.376 -6.4726781845092773
		 82.412 -6.5196061134338379 83.444 -6.5764021873474121 84.48 -6.6429319381713867 85.512 -6.7188310623168945
		 86.548 -6.8034725189208984 87.584 -6.8959951400756836 88.616 -6.9953136444091797
		 89.652 -7.1001377105712891 90.688 -7.2089695930480957 91.72 -7.320155143737793 92.756 -7.4319448471069327
		 93.788 -7.5424470901489249 94.824 -7.6497054100036621 95.86 -7.7516751289367676 96.892 -7.8463101387023917
		 97.928 -7.9315748214721671 98.964 -8.0055637359619141 99.996 -8.0664529800415039
		 101.032 -8.1126279830932617 102.064 -8.1427488327026367 103.1 -8.1558122634887695
		 104.136 -8.1513032913208008 105.168 -8.1291980743408203 106.204 -8.0900182723999023
		 107.24 -8.0348186492919922 108.272 -7.9651741981506357 109.308 -7.8831734657287607
		 110.34 -7.7913541793823251 111.376 -7.6925797462463388 112.412 -7.5899243354797363
		 113.444 -7.486541748046875 114.48 -7.3855881690979004 115.512 -7.2900629043579102
		 116.548 -7.2027540206909189 117.584 -7.126105785369873 118.616 -7.0621480941772461
		 119.652 -7.0124421119689941 120.688 -6.978090763092041 121.72 -6.9597072601318359
		 122.756 -6.9573798179626465 123.788 -6.9706974029541016 124.824 -6.9987010955810547
		 125.86 -7.0400004386901855 126.892 -7.0927605628967285 127.928 -7.1547856330871582
		 128.964 -7.2235455513000488 129.996 -7.2962818145751953 131.032 -7.3701195716857919
		 132.064 -7.442141056060791 133.1 -7.5095229148864746 134.136 -7.5696487426757812
		 135.168 -7.6185669898986825 136.204 -7.650249481201171 137.236 -7.6904153823852539
		 138.272 -7.7061982154846191 139.308 -7.7061066627502441 140.34 -7.6907839775085449
		 141.376 -7.6575202941894531 142.412 -7.6417245864868164 143.444 -7.5766820907592773
		 144.48 -7.5418133735656747 145.512 -7.496849536895752 146.548 -7.4520602226257324
		 147.584 -7.4174985885620108 148.616 -7.3512239456176749 149.652 -7.3268375396728507
		 150.688 -7.301450252532959 151.72 -7.2805237770080558 152.756 -7.2663774490356436
		 153.788 -7.2581939697265625 154.824 -7.2546558380126962 155.86 -7.2540292739868155
		 156.892 -7.254498004913331 157.928 -7.2543296813964844 158.964 -7.2486248016357422
		 159.996 -7.243431568145752 161.032 -7.2170310020446777 162.064 -7.2030596733093271
		 163.1 -7.1407418251037598 164.136 -7.0985660552978516 165.168 -7.0357017517089844
		 166.204 -6.9565062522888184 167.236 -6.8633170127868652 168.272 -6.7564735412597656
		 169.308 -6.6366863250732422 170.34 -6.5050239562988281 171.376 -6.362886905670166
		 172.412 -6.2119755744934082 173.444 -6.0541901588439941 174.48 -5.8916301727294922
		 175.512 -5.7265219688415527 176.548 -5.561121940612793 177.584 -5.3976492881774902
		 178.616 -5.2382454872131348 179.652 -5.0848865509033203 180.688 -4.9393129348754883
		 181.72 -4.8030767440795898 182.756 -4.6774501800537109 183.788 -4.5634346008300781
		 184.824 -4.4617366790771484 185.86 -4.3727540969848633 186.892 -4.2998056411743164
		 187.928 -4.2491426467895508 188.964 -4.1685280799865723 189.996 -4.1502151489257812
		 191.032 -4.1138548851013184 192.064 -4.0956869125366211 193.1 -4.0911397933959961
		 194.136 -4.096163272857666 195.168 -4.1070294380187988 196.204 -4.1244263648986816
		 197.236 -4.1476325988769531 198.272 -4.1754584312438965 199.308 -4.2067227363586426
		 200.34 -4.2402582168579102 201.376 -4.2749547958374023 202.412 -4.3098182678222656
		 203.444 -4.343937873840332 204.48 -4.3765482902526855 205.512 -4.4070539474487305
		 206.548 -4.4350295066833496 207.584 -4.4602489471435547 208.616 -4.4826674461364746
		 209.652 -4.5024118423461914 210.688 -4.5197572708129883 211.72 -4.5351605415344238
		 212.756 -4.5491924285888672 213.788 -4.5625166893005371 214.824 -4.5758399963378906
		 215.86 -4.5899786949157715 216.892 -4.6057615280151367 217.928 -4.6240353584289551
		 218.964 -4.6456489562988281 219.996 -4.6714067459106445 221.032 -4.7020220756530762
		 222.064 -4.7380385398864746 223.1 -4.7798953056335449 224.136 -4.8277897834777832
		 225.168 -4.8817043304443359 226.204 -4.9413466453552246 227.236 -5.0061354637145996
		 228.272 -5.0752091407775879 229.308 -5.1474647521972656 230.34 -5.2215619087219238
		 231.376 -5.2960395812988281 232.412 -5.3693513870239258 233.444 -5.4399738311767578
		 234.48 -5.5064191818237305 235.512 -5.567410945892334 236.548 -5.6218504905700684
		 237.584 -5.6689300537109375 238.616 -5.7081670761108398 239.652 -5.7394242286682129
		 240.688 -5.7629528045654297 241.72 -5.7791047096252441 242.756 -5.7881731986999512
		 243.788 -5.796205997467041 244.824 -5.7977743148803711 245.86 -5.7965970039367676
		 246.892 -5.7948932647705078 247.928 -5.7942714691162109 248.96 -5.7969150543212891
		 249.996 -5.8054647445678711 251.032 -5.8109192848205566 252.064 -5.8375082015991211
		 253.1 -5.8560085296630859 254.136 -5.8836150169372559 255.168 -5.9179849624633789
		 256.204 -5.9574708938598633 257.236 -6.0012021064758301 258.272 -6.0480546951293945;
	setAttr ".ktv[250:290]" 259.308 -6.0966873168945313 260.34 -6.1456398963928223
		 261.376 -6.1934146881103516 262.412 -6.2385072708129883 263.444 -6.2795023918151855
		 264.48 -6.3150777816772461 265.512 -6.3441648483276367 266.548 -6.3659372329711914
		 267.584 -6.3798766136169434 268.616 -6.3857760429382324 269.652 -6.3837833404541016
		 270.688 -6.3743672370910645 271.72 -6.3583431243896484 272.756 -6.3367853164672852
		 273.788 -6.3109960556030273 274.824 -6.2825064659118652 275.86 -6.2529840469360352
		 276.892 -6.2240967750549316 277.928 -6.1975245475769043 278.96 -6.1755356788635254
		 279.996 -6.1609654426574707 281.032 -6.1428756713867187 282.064 -6.1422710418701172
		 283.1 -6.1477603912353516 284.136 -6.1593189239501953 285.168 -6.1739888191223145
		 286.204 -6.2256956100463867 287.236 -6.2550196647644043 288.272 -6.2882938385009766
		 289.308 -6.3839130401611328 290.34 -6.4364848136901855 291.376 -6.5082507133483887
		 292.412 -6.5914144515991211 293.444 -6.6817536354064941 294.48 -6.7786741256713867
		 295.512 -6.8813652992248535 296.548 -6.9888896942138672 297.584 -7.1000847816467285
		 298.616 -7.2135238647460929 299.652 -7.3276629447937012 300.688 -7.4407539367675781;
createNode animCurveTA -n "Bip01_Spine1_rotateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 291 ".ktv";
	setAttr ".ktv[0:249]"  0.688 1.164785623550415 1.72 1.1602808237075806 2.756 1.1659828424453735
		 3.788 1.1802595853805542 4.824 1.2012349367141724 5.86 1.2269312143325806 6.892 1.2554275989532471
		 7.928 1.2850289344787598 8.964 1.3142945766448975 9.996 1.3421399593353271 11.032 1.3678092956542969
		 12.064 1.3909292221069336 13.1 1.411454439163208 14.136 1.4296704530715942 15.168 1.4461081027984619
		 16.204 1.4614773988723755 17.24 1.4765585660934448 18.272 1.4921692609786987 19.308 1.5077632665634155
		 20.34 1.5198541879653931 21.376 1.5469268560409546 22.412 1.5728111267089844 23.444 1.6014398336410522
		 24.48 1.6371220350265503 25.512 1.6856298446655273 26.548 1.7051657438278198 27.584 1.7782852649688721
		 28.616 1.8058410882949829 29.652 1.9030504226684573 30.688 1.9566439390182497 31.72 2.0293989181518555
		 32.756 2.1139757633209229 33.788 2.2067887783050537 34.824 2.3079733848571777 35.86 2.4175059795379639
		 36.892 2.5351662635803223 37.928 2.6605324745178223 38.964 2.7929129600524902 39.996 2.9314448833465576
		 41.032 3.0750975608825684 42.064 3.2227370738983154 43.1 3.3730742931365967 44.136 3.5247571468353271
		 45.168 3.6764020919799805 46.204 3.826650857925415 47.24 3.9741957187652592 48.272 4.1178960800170898
		 49.308 4.2567229270935059 50.34 4.3899350166320801 51.376 4.516998291015625 52.412 4.6375937461853027
		 53.444 4.7515707015991211 54.48 4.8590078353881836 55.512 4.9601240158081055 56.548 5.0552468299865723
		 57.584 5.1447138786315918 58.616 5.2287812232971191 59.652 5.3075709342956543 60.688 5.3809762001037598
		 61.72 5.4487042427062988 62.756 5.510136604309082 63.788 5.564424991607666 64.824 5.610501766204834
		 65.86 5.6470718383789062 66.892 5.6728205680847168 67.928 5.6863956451416016 68.964 5.6864919662475586
		 69.996 5.6719188690185547 71.032 5.6417112350463867 72.064 5.5952668190002441 73.1 5.5324172973632813
		 74.136 5.4535055160522461 75.168 5.3594088554382324 76.204 5.2514634132385254 77.24 5.1314492225646973
		 78.272 5.0016164779663086 79.308 4.8645334243774414 80.34 4.7229762077331543 81.376 4.5798182487487793
		 82.412 4.4379100799560547 83.444 4.2999649047851563 84.48 4.1684093475341797 85.512 4.0453119277954102
		 86.548 3.9323625564575195 87.584 3.8308527469635014 88.616 3.74164867401123 89.652 3.6651804447174072
		 90.688 3.6014831066131592 91.72 3.5503115653991699 92.756 3.5112361907958984 93.788 3.4836838245391846
		 94.824 3.466970682144165 95.86 3.460385799407959 96.892 3.4631636142730713 97.928 3.4745166301727295
		 98.964 3.4935996532440186 99.996 3.5195653438568115 101.032 3.5515172481536865 102.064 3.5884404182434078
		 103.1 3.629186630249023 104.136 3.6724350452423091 105.168 3.716713428497314 106.204 3.7604258060455322
		 107.24 3.8018317222595215 108.272 3.8391573429107662 109.308 3.8705468177795415 110.34 3.8941590785980225
		 111.376 3.9082527160644527 112.412 3.9112188816070557 113.444 3.9017446041107173
		 114.48 3.8787953853607173 115.512 3.8417186737060547 116.548 3.7902727127075191 117.584 3.7246384620666504
		 118.616 3.6454942226409917 119.652 3.5539782047271729 120.688 3.4516561031341553
		 121.72 3.3404922485351563 122.756 3.2227640151977539 123.788 3.1009275913238525 124.824 2.9776122570037842
		 125.86 2.8554463386535645 126.892 2.7370038032531738 127.928 2.6247029304504395 128.964 2.5207376480102539
		 129.996 2.4269485473632813 131.032 2.3448295593261719 132.064 2.2755184173583984
		 133.1 2.2197391986846924 134.136 2.1778769493103027 135.168 2.1499812602996826 136.204 2.135129451751709
		 137.236 2.1299068927764893 138.272 2.1449599266052246 139.308 2.1529786586761475
		 140.34 2.1921122074127197 141.376 2.2441504001617432 142.412 2.26499342918396 143.444 2.3465001583099365
		 144.48 2.3898212909698486 145.512 2.4468333721160889 146.548 2.5059976577758789 147.584 2.5549318790435791
		 148.616 2.6655380725860596 149.652 2.718022346496582 150.688 2.7844135761260986 151.72 2.855818510055542
		 152.756 2.9276177883148193 153.788 2.9993855953216553 154.824 3.0706052780151367
		 155.86 3.1406412124633789 156.892 3.2048575878143311 157.928 3.2546720504760742 158.964 3.3519887924194336
		 159.996 3.3806309700012207 161.032 3.4590966701507568 162.064 3.4822440147399902
		 163.1 3.5440621376037598 164.136 3.5674664974212646 165.168 3.5918250083923344 166.204 3.6123988628387447
		 167.236 3.6271691322326665 168.272 3.6368560791015621 169.308 3.6423227787017822
		 170.34 3.6445503234863281 171.376 3.6445674896240234 172.412 3.6434140205383301 173.444 3.6420354843139648
		 174.48 3.6412458419799805 175.512 3.6416661739349365 176.548 3.6437282562255859 177.584 3.6476955413818359
		 178.616 3.6536428928375244 179.652 3.6614422798156738 180.688 3.6708095073699951
		 181.72 3.6813125610351558 182.756 3.6924452781677246 183.788 3.7037069797515865 184.824 3.7145214080810542
		 185.86 3.7243483066558833 186.892 3.7324659824371338 187.928 3.7378652095794673 188.964 3.7450070381164551
		 189.996 3.746173620223999 191.032 3.7460541725158687 192.064 3.7423944473266602 193.1 3.7337679862976079
		 194.136 3.7272007465362544 195.168 3.7171218395233154 196.204 3.704755544662476 197.236 3.6908359527587891
		 198.272 3.6758077144622798 199.308 3.6601667404174809 200.34 3.6445329189300533 201.376 3.6295344829559326
		 202.412 3.6158053874969482 203.444 3.6040284633636479 204.48 3.5948996543884277 205.512 3.5890092849731441
		 206.548 3.5869367122650146 207.584 3.5891506671905522 208.616 3.595982551574707 209.652 3.6076395511627193
		 210.688 3.6241762638092045 211.72 3.6455698013305664 212.756 3.6716952323913574 213.788 3.7024016380310063
		 214.824 3.73740553855896 215.86 3.7763614654541016 216.892 3.8189353942871094 217.928 3.8648107051849361
		 218.964 3.9137339591979976 219.996 3.9655418395996094 221.032 4.0201244354248047
		 222.064 4.0773782730102539 223.1 4.1372890472412109 224.136 4.199854850769043 225.168 4.265141487121582
		 226.204 4.3332400321960449 227.236 4.4042787551879883 228.272 4.4783210754394531
		 229.308 4.5554280281066895 230.34 4.6355342864990234 231.376 4.7185673713684082 232.412 4.804295539855957
		 233.444 4.8923282623291016 234.48 4.9821286201477051 235.512 5.072939395904541 236.548 5.1638245582580566
		 237.584 5.2537055015563965 238.616 5.3413023948669434 239.652 5.4252119064331055
		 240.688 5.5038943290710449 241.72 5.5723772048950195 242.756 5.6196126937866211 243.788 5.6946501731872559
		 244.824 5.7418804168701172 245.86 5.7727179527282715 246.892 5.7902140617370605 247.928 5.7936410903930664
		 248.96 5.7798857688903809 249.996 5.7436337471008301 251.032 5.7239198684692383 252.064 5.6353678703308105
		 253.1 5.5795726776123047 254.136 5.4996075630187988 255.168 5.4031167030334473 256.204 5.2944650650024414
		 257.236 5.1746830940246582 258.272 5.0449719429016113;
	setAttr ".ktv[250:290]" 259.308 4.9066548347473145 260.34 4.7612714767456055
		 261.376 4.610443115234375 262.412 4.4558858871459961 263.444 4.2993030548095703 264.48 4.1424689292907715
		 265.512 3.9870142936706543 266.548 3.834477424621582 267.584 3.6862750053405757 268.616 3.5435709953308105
		 269.652 3.4073717594146729 270.688 3.2784132957458496 271.72 3.1571760177612305 272.756 3.0439927577972412
		 273.788 2.9389619827270508 274.824 2.8419959545135498 275.86 2.7528524398803711 276.892 2.6711547374725342
		 277.928 2.5965211391448975 278.96 2.5319700241088867 279.996 2.4840362071990967 281.032 2.3924088478088379
		 282.064 2.3546922206878662 283.1 2.3104162216186523 284.136 2.2683219909667969 285.168 2.2361576557159424
		 286.204 2.1703362464904785 287.236 2.1430470943450928 288.272 2.1158268451690674
		 289.308 2.0506124496459961 290.34 2.0194690227508545 291.376 1.9796401262283327 292.412 1.9360415935516357
		 293.444 1.8912485837936401 294.48 1.8455100059509277 295.512 1.7992254495620728 296.548 1.752906322479248
		 297.584 1.7071627378463745 298.616 1.6626414060592651 299.652 1.6200306415557861
		 300.688 1.5800204277038574;
createNode animCurveTA -n "Bip01_Spine1_rotateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 291 ".ktv";
	setAttr ".ktv[0:249]"  0.688 4.3749251365661621 1.72 4.1125788688659668 2.756 3.8503293991088867
		 3.788 3.5925350189208989 4.824 3.343165397644043 5.86 3.1056909561157227 6.892 2.8828163146972656
		 7.928 2.6766126155853271 8.964 2.4882650375366211 9.996 2.3182237148284912 11.032 2.166196346282959
		 12.064 2.0312502384185791 13.1 1.9119800329208374 14.136 1.806749701499939 15.168 1.7138398885726929
		 16.204 1.6315953731536865 17.24 1.558653712272644 18.272 1.493929386138916 19.308 1.4394397735595703
		 20.34 1.4024608135223389 21.376 1.3440374135971069 22.412 1.3068747520446777 23.444 1.2823305130004883
		 24.48 1.2695964574813843 25.512 1.2719411849975586 26.548 1.2797967195510864 27.584 1.323613166809082
		 28.616 1.3468989133834839 29.652 1.4496561288833618 30.688 1.5165388584136963 31.72 1.6140637397766113
		 32.756 1.7339454889297485 33.788 1.8714941740036011 34.824 2.0256285667419434 35.86 2.1949937343597412
		 36.892 2.3779563903808594 37.928 2.5727958679199219 38.964 2.7777857780456543 39.996 2.9912848472595215
		 41.032 3.2117972373962402 42.064 3.4380645751953125 43.1 3.6690707206726079 44.136 3.9041402339935303
		 45.168 4.1428337097167969 46.204 4.3849949836730957 47.24 4.6306967735290527 48.272 4.8801565170288086
		 49.308 5.1337375640869141 50.34 5.3918328285217285 51.376 5.6548099517822266 52.412 5.9229879379272461
		 53.444 6.1965780258178711 54.48 6.4756731986999512 55.512 6.760225772857666 56.548 7.0500349998474121
		 57.584 7.3447265625 58.616 7.6438374519348145 59.652 7.9466943740844727 60.688 8.2525196075439453
		 61.72 8.5604028701782227 62.756 8.8693141937255859 63.788 9.1780910491943359 64.824 9.4854841232299805
		 65.86 9.7901315689086914 66.892 10.090664863586426 67.928 10.385723114013672 68.964 10.673951148986816
		 69.996 10.954009056091309 71.032 11.224634170532227 72.064 11.484692573547363 73.1 11.733196258544922
		 74.136 11.969332695007324 75.168 12.19245433807373 76.204 12.402059555053711 77.24 12.597792625427246
		 78.272 12.779507637023926 79.308 12.947282791137695 80.34 13.101401329040527 81.376 13.242441177368164
		 82.412 13.37108039855957 83.444 13.488219261169434 84.48 13.594870567321777 85.512 13.692096710205078
		 86.548 13.781014442443848 87.584 13.862651824951172 88.616 13.9378662109375 89.652 14.007235527038574
		 90.688 14.070949554443359 91.72 14.128870964050293 92.756 14.180437088012695 93.788 14.224689483642578
		 94.824 14.260266304016113 95.86 14.285508155822754 96.892 14.298616409301758 97.928 14.297707557678223
		 98.964 14.281055450439453 99.996 14.247123718261719 101.032 14.194723129272461 102.064 14.123073577880859
		 103.1 14.03187370300293 104.136 13.921323776245117 105.168 13.792025566101074 106.204 13.644962310791016
		 107.24 13.481395721435547 108.272 13.302760124206543 109.308 13.110593795776367 110.34 12.906485557556152
		 111.376 12.692008018493652 112.412 12.468649864196777 113.444 12.237894058227539
		 114.48 12.001097679138184 115.512 11.759673118591309 116.548 11.514952659606934 117.584 11.268338203430176
		 118.616 11.02131175994873 119.652 10.775418281555176 120.688 10.532284736633301 121.72 10.293532371520996
		 122.756 10.060811042785645 123.788 9.8357114791870117 124.824 9.6197376251220703
		 125.86 9.4142389297485352 126.892 9.2203578948974609 127.928 9.0390491485595703 128.964 8.8709602355957031
		 129.996 8.7165117263793945 131.032 8.5757904052734375 132.064 8.4486627578735352
		 133.1 8.3347406387329102 134.136 8.2334413528442383 135.168 8.148219108581543 136.204 8.0903787612915039
		 137.236 7.9936075210571298 138.272 7.9158678054809561 139.308 7.8962507247924805
		 140.34 7.8350095748901358 141.376 7.7822737693786621 142.412 7.7670292854309082 143.444 7.7174739837646475
		 144.48 7.6970095634460458 145.512 7.6741056442260751 146.548 7.6536364555358896 147.584 7.639535427093505
		 148.616 7.6176309585571289 149.652 7.6120362281799325 150.688 7.6086039543151855
		 151.72 7.6091589927673331 152.756 7.6145529747009277 153.788 7.6245226860046387 154.824 7.6385798454284668
		 155.86 7.6560049057006845 156.892 7.6745281219482431 157.928 7.6903624534606934 158.964 7.7251243591308594
		 159.996 7.7359189987182608 161.032 7.7667322158813477 162.064 7.7759556770324698
		 163.1 7.8010344505310059 164.136 7.8107886314392081 165.168 7.8213057518005371 166.204 7.8308329582214355
		 167.236 7.8387975692749023 168.272 7.8457074165344247 169.308 7.8520722389221191
		 170.34 7.858374595642089 171.376 7.8649582862854004 172.412 7.8720846176147461 173.444 7.8798232078552246
		 174.48 7.8880605697631836 175.512 7.8965802192687988 176.548 7.9050822257995605 177.584 7.9132475852966309
		 178.616 7.9207181930541992 179.652 7.9270734786987305 180.688 7.9318709373474121
		 181.72 7.9346303939819327 182.756 7.9348139762878427 183.788 7.9318323135375985 184.824 7.9250340461730957
		 185.86 7.9137096405029297 186.892 7.8989043235778809 187.928 7.8834805488586417 188.964 7.8383603096008301
		 189.996 7.82309865951538 191.032 7.7684817314147949 192.064 7.7071175575256339 193.1 7.6194367408752441
		 194.136 7.564852237701416 195.168 7.4848084449768066 196.204 7.3905749320983887 197.236 7.2860989570617667
		 198.272 7.171436309814454 199.308 7.046654224395752 200.34 6.9118509292602539 201.376 6.7671933174133301
		 202.412 6.6128711700439453 203.444 6.4491558074951172 204.48 6.2764401435852051 205.512 6.0952672958374023
		 206.548 5.9063811302185059 207.584 5.7107663154602051 208.616 5.5096011161804199
		 209.652 5.304286003112793 210.688 5.0964303016662598 211.72 4.8877162933349609 212.756 4.6798720359802246
		 213.788 4.4745779037475586 214.824 4.2733807563781738 215.86 4.0775632858276367 216.892 3.8881177902221684
		 217.928 3.705686092376709 218.964 3.5305540561676025 219.996 3.3626253604888916 221.032 3.2015376091003418
		 222.064 3.0466890335083008 223.1 2.8973126411437988 224.136 2.7526051998138428 225.168 2.6117813587188721
		 226.204 2.4742248058319092 227.236 2.3395371437072754 228.272 2.2075483798980713
		 229.308 2.0783324241638184 230.34 1.9522289037704468 231.376 1.8297790288925173 232.412 1.7116793394088745
		 233.444 1.5987248420715332 234.48 1.4917789697647095 235.512 1.3916428089141846 236.548 1.2990673780441284
		 237.584 1.2146952152252197 238.616 1.1389919519424438 239.652 1.0722447633743286
		 240.688 1.0145561695098877 241.72 0.96785485744476329 242.756 0.93725502490997314
		 243.788 0.89353048801422119 244.824 0.86877846717834473 245.86 0.85466587543487549
		 246.892 0.84837847948074341 247.928 0.84955769777297974 248.96 0.85910522937774658
		 249.996 0.87918668985366821 251.032 0.88899320363998413 252.064 0.93070203065872192
		 253.1 0.95497375726699829 254.136 0.98821371793746959 255.168 1.0262774229049683
		 256.204 1.0666002035140991 257.236 1.1082053184509277 258.272 1.1500582695007324;
	setAttr ".ktv[250:290]" 259.308 1.1911180019378662 260.34 1.230351448059082
		 261.376 1.2667850255966187 262.412 1.2995727062225342 263.444 1.3279803991317749
		 264.48 1.3514970541000366 265.512 1.3697859048843384 266.548 1.3827675580978394 267.584 1.3906301259994507
		 268.616 1.3938120603561401 269.652 1.393019437789917 270.688 1.3891950845718384 271.72 1.3834812641143799
		 272.756 1.3771744966506958 273.788 1.3716628551483154 274.824 1.3683128356933594
		 275.86 1.368439793586731 276.892 1.3732056617736816 277.928 1.3835351467132568 278.96 1.3981961011886597
		 279.996 1.4137603044509888 281.032 1.4615035057067871 282.064 1.4892284870147705
		 283.1 1.5274711847305298 284.136 1.5683825016021729 285.168 1.60272216796875 286.204 1.6800209283828735
		 287.236 1.7119582891464233 288.272 1.7429211139678955 289.308 1.809262752532959 290.34 1.8347241878509524
		 291.376 1.862232565879822 292.412 1.8861461877822876 293.444 1.9037513732910156 294.48 1.9151721000671387
		 295.512 1.9208806753158569 296.548 1.9215049743652344 297.584 1.9178316593170166
		 298.616 1.9107269048690798 299.652 1.9010370969772337 300.688 1.8894830942153933;
createNode animCurveTA -n "Bip01_Spine2_rotateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 291 ".ktv";
	setAttr ".ktv[0:249]"  0.688 -3.5307700634002686 1.72 -3.4704113006591797
		 2.756 -3.4123456478118896 3.788 -3.3582019805908203 4.824 -3.30934739112854 5.86 -3.2668945789337158
		 6.892 -3.2315881252288818 7.928 -3.2038192749023437 8.964 -3.1836199760437012 9.996 -3.1706554889678955
		 11.032 -3.1642377376556396 12.064 -3.1634211540222168 13.1 -3.1670458316802979 14.136 -3.1738317012786865
		 15.168 -3.1825428009033203 16.204 -3.1919565200805664 17.24 -3.201005220413208 18.272 -3.2088332176208496
		 19.308 -3.214698314666748 20.34 -3.2181432247161865 21.376 -3.2207515239715576 22.412 -3.2199981212615967
		 23.444 -3.2171857357025146 24.48 -3.2128152847290039 25.512 -3.20701003074646 26.548 -3.2051661014556885
		 27.584 -3.1997048854827881 28.616 -3.1986367702484131 29.652 -3.1981751918792725
		 30.688 -3.1998951435089111 31.72 -3.2035253047943115 32.756 -3.2089822292327881 33.788 -3.2159926891326904
		 34.824 -3.2240958213806152 35.86 -3.2327554225921631 36.892 -3.2413876056671143 37.928 -3.2494187355041504
		 38.964 -3.2563724517822266 39.996 -3.2618923187255859 41.032 -3.2657871246337891
		 42.064 -3.2680354118347168 43.1 -3.2687888145446777 44.136 -3.2683863639831543 45.168 -3.267284631729126
		 46.204 -3.2660431861877441 47.24 -3.2652511596679687 48.272 -3.2654943466186523 49.308 -3.2672557830810547
		 50.34 -3.2709517478942871 51.376 -3.2768073081970215 52.412 -3.2848708629608154 53.444 -3.2949764728546143
		 54.48 -3.3068301677703857 55.512 -3.3199591636657715 56.548 -3.3337886333465576 57.584 -3.3476598262786865
		 58.616 -3.3609144687652588 59.652 -3.372887134552002 60.688 -3.3830118179321289 61.72 -3.3908464908599854
		 62.756 -3.3960912227630615 63.788 -3.3986203670501709 64.824 -3.3984501361846924
		 65.86 -3.395759105682373 66.892 -3.3908638954162598 67.928 -3.3841607570648193 68.964 -3.3761372566223145
		 69.996 -3.3673129081726074 71.032 -3.3582086563110352 72.064 -3.3493664264678955
		 73.1 -3.3413219451904297 74.136 -3.3346164226531982 75.168 -3.3297586441040039 76.204 -3.3272879123687744
		 77.24 -3.3276724815368652 78.272 -3.3313784599304199 79.308 -3.3388252258300781 80.34 -3.3504047393798828
		 81.376 -3.3664407730102539 82.412 -3.3871769905090332 83.444 -3.4127655029296875
		 84.48 -3.4432351589202881 85.512 -3.478492259979248 86.548 -3.5182790756225586 87.584 -3.5622384548187256
		 88.616 -3.6098854541778564 89.652 -3.6605653762817383 90.688 -3.7135753631591797
		 91.72 -3.7680635452270508 92.756 -3.8231654167175289 93.788 -3.8778982162475586 94.824 -3.9312689304351807
		 95.86 -3.9822263717651363 96.892 -4.0297074317932129 97.928 -4.0726699829101563 98.964 -4.1100945472717285
		 99.996 -4.141045093536377 101.032 -4.1646780967712402 102.064 -4.1802797317504883
		 103.1 -4.1872997283935547 104.136 -4.1854372024536133 105.168 -4.1746292114257812
		 106.204 -4.1551103591918945 107.24 -4.127347469329834 108.272 -4.0920939445495605
		 109.308 -4.050358772277832 110.34 -4.003382682800293 111.376 -3.9525675773620605
		 112.412 -3.8994214534759521 113.444 -3.8455464839935298 114.48 -3.7924914360046391
		 115.512 -3.7417883872985844 116.548 -3.6948521137237549 117.584 -3.6529483795166016
		 118.616 -3.6171374320983887 119.652 -3.5882639884948735 120.688 -3.5669422149658203
		 121.72 -3.5535430908203125 122.756 -3.5481901168823242 123.788 -3.5507371425628662
		 124.824 -3.5607748031616211 125.86 -3.57765793800354 126.892 -3.6005229949951176
		 127.928 -3.6283025741577148 128.964 -3.6597671508789067 129.996 -3.6935608386993408
		 131.032 -3.7282371520996094 132.064 -3.7623515129089355 133.1 -3.794480562210083
		 134.136 -3.8233189582824707 135.168 -3.8468959331512451 136.204 -3.8622095584869389
		 137.236 -3.8817458152770996 138.272 -3.8895406723022465 139.308 -3.8895401954650879
		 140.34 -3.8822131156921382 141.376 -3.8661947250366211 142.412 -3.8585679531097417
		 143.444 -3.8271238803863521 144.48 -3.8102848529815674 145.512 -3.7885985374450679
		 146.548 -3.7670590877532963 147.584 -3.7504780292510982 148.616 -3.718991756439209
		 149.652 -3.7076058387756348 150.688 -3.6959488391876216 151.72 -3.686622142791748
		 152.756 -3.6807165145874028 153.788 -3.6778256893157959 154.824 -3.6772801876068111
		 155.86 -3.6781895160675049 156.892 -3.6795871257781982 157.928 -3.6804020404815674
		 158.964 -3.6793651580810551 159.996 -3.6773099899291992 161.032 -3.6655924320220943
		 162.064 -3.6590504646301274 163.1 -3.6290922164916997 164.136 -3.6084680557250977
		 165.168 -3.5775284767150879 166.204 -3.5383551120758057 167.236 -3.4920852184295654
		 168.272 -3.4389028549194336 169.308 -3.3791673183441162 170.34 -3.3134591579437256
		 171.376 -3.2424867153167725 172.412 -3.1670939922332764 173.444 -3.0882782936096191
		 174.48 -3.0070903301239014 175.512 -2.9246542453765869 176.548 -2.8420867919921875
		 177.584 -2.7605311870574951 178.616 -2.681025505065918 179.652 -2.6045575141906738
		 180.688 -2.5320048332214355 181.72 -2.4641120433807373 182.756 -2.4015188217163086
		 183.788 -2.3447098731994629 184.824 -2.2940058708190918 185.86 -2.2496094703674316
		 186.892 -2.2131712436676025 187.928 -2.1878132820129395 188.964 -2.1472795009613037
		 189.996 -2.1380205154418945 191.032 -2.1194014549255371 192.064 -2.1097559928894043
		 193.1 -2.1066324710845947 194.136 -2.108588695526123 195.168 -2.1132075786590576
		 196.204 -2.1209487915039062 197.236 -2.1314888000488281 198.272 -2.1442358493804932
		 199.308 -2.1586244106292725 200.34 -2.1740763187408447 201.376 -2.1900584697723389
		 202.412 -2.2060625553131104 203.444 -2.221660852432251 204.48 -2.2364828586578369
		 205.512 -2.2502264976501465 206.548 -2.2627050876617432 207.584 -2.2737994194030762
		 208.616 -2.283505916595459 209.652 -2.2918777465820312 210.688 -2.299086332321167
		 211.72 -2.3053543567657471 212.756 -2.3109579086303711 213.788 -2.3162441253662109
		 214.824 -2.3215873241424561 215.86 -2.3273775577545166 216.892 -2.3340330123901367
		 217.928 -2.3419806957244873 218.964 -2.3516490459442139 219.996 -2.3634324073791504
		 221.032 -2.377669095993042 222.064 -2.3946533203125 223.1 -2.4145772457122803 224.136 -2.4375288486480713
		 225.168 -2.4635062217712402 226.204 -2.4923222064971924 227.236 -2.5237205028533936
		 228.272 -2.5572683811187744 229.308 -2.5923750400543213 230.34 -2.628410816192627
		 231.376 -2.6646397113800049 232.412 -2.7002911567687988 233.444 -2.7345964908599854
		 234.48 -2.7668483257293701 235.512 -2.7963995933532715 236.548 -2.8227336406707764
		 237.584 -2.8454325199127197 238.616 -2.8642771244049072 239.652 -2.8792045116424561
		 240.688 -2.8903579711914063 241.72 -2.8979144096374512 242.756 -2.9021201133728027
		 243.788 -2.905648946762085 244.824 -2.9061474800109863 245.86 -2.9053936004638672
		 246.892 -2.9044842720031738 247.928 -2.9041876792907715 248.96 -2.9056191444396973
		 249.996 -2.9101166725158691 251.032 -2.9129505157470703 252.064 -2.9266819953918457
		 253.1 -2.936176061630249 254.136 -2.9503030776977539 255.168 -2.9678118228912354
		 256.204 -2.9879052639007568 257.236 -3.0100717544555664 258.272 -3.0337655544281006;
	setAttr ".ktv[250:290]" 259.308 -3.0583219528198242 260.34 -3.0829720497131348
		 261.376 -3.1069667339324951 262.412 -3.1295502185821533 263.444 -3.1500148773193359
		 264.48 -3.1677010059356689 265.512 -3.1820964813232422 266.548 -3.1927590370178223
		 267.584 -3.1994743347167969 268.616 -3.2021541595458984 269.652 -3.2008531093597412
		 270.688 -3.1958353519439697 271.72 -3.1875033378601074 272.756 -3.1764242649078369
		 273.788 -3.1632614135742187 274.824 -3.1487851142883301 275.86 -3.1338150501251221
		 276.892 -3.1192171573638916 277.928 -3.1058075428009033 278.96 -3.0947530269622803
		 279.996 -3.0874330997467041 281.032 -3.0783979892730713 282.064 -3.0781385898590088
		 283.1 -3.0809624195098877 284.136 -3.0868229866027832 285.168 -3.0942225456237793
		 286.204 -3.1202452182769775 287.236 -3.1349723339080811 288.272 -3.1516666412353516
		 289.308 -3.1995468139648437 290.34 -3.225822925567627 291.376 -3.2616782188415527
		 292.412 -3.3032114505767822 293.444 -3.3482961654663086 294.48 -3.3966054916381836
		 295.512 -3.4478044509887695 296.548 -3.5013973712921143 297.584 -3.5567934513092041
		 298.616 -3.6133253574371333 299.652 -3.6701967716217045 300.688 -3.7265458106994629;
createNode animCurveTA -n "Bip01_Spine2_rotateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 291 ".ktv";
	setAttr ".ktv[0:249]"  0.688 0.51519054174423218 1.72 0.51802676916122437
		 2.756 0.52580606937408447 3.788 0.5376165509223938 4.824 0.55245578289031982 5.86 0.56929564476013184
		 6.892 0.58715605735778809 7.928 0.60517311096191406 8.964 0.62265187501907349 9.996 0.6390642523765564
		 11.032 0.6540534496307373 12.064 0.66747748851776123 13.1 0.67934948205947876 14.136 0.68985337018966675
		 15.168 0.69929587841033936 16.204 0.70804828405380249 17.24 0.71653789281845093 18.272 0.72519952058792114
		 19.308 0.73372340202331543 20.34 0.74025332927703857 21.376 0.75460058450698853 22.412 0.76806759834289551
		 23.444 0.78274190425872803 24.48 0.80078583955764771 25.512 0.82503187656402588 26.548 0.83470749855041504
		 27.584 0.87069177627563477 28.616 0.88414442539215088 29.652 0.93133777379989624
		 30.688 0.95719611644744873 31.72 0.9921838641166687 32.756 1.0327589511871338 33.788 1.0772037506103516
		 34.824 1.1255732774734497 35.86 1.1778974533081055 36.892 1.2340859174728394 37.928 1.2939473390579224
		 38.964 1.3571828603744507 39.996 1.4233888387680054 41.032 1.4920848608016968 42.064 1.5626987218856812
		 43.1 1.6346250772476196 44.136 1.7072083950042725 45.168 1.7797359228134155 46.204 1.8515264987945557
		 47.24 1.9219264984130859 48.272 1.9903351068496704 49.308 2.0562417507171631 50.34 2.1192238330841064
		 51.376 2.1790158748626709 52.412 2.2354381084442139 53.444 2.2884311676025391 54.48 2.338001012802124
		 55.512 2.38427734375 56.548 2.427448034286499 57.584 2.4677040576934814 58.616 2.5051846504211426
		 59.652 2.5399928092956543 60.688 2.5721509456634521 61.72 2.6014699935913086 62.756 2.627737283706665
		 63.788 2.6505441665649414 64.824 2.6693553924560547 65.86 2.6835894584655762 66.892 2.6925668716430664
		 67.928 2.6956207752227783 68.964 2.6921074390411377 69.996 2.6814267635345459 71.032 2.6630840301513672
		 72.064 2.6367239952087402 73.1 2.602294921875 74.136 2.5599393844604492 75.168 2.5100440979003906
		 76.204 2.4532406330108643 77.24 2.3904266357421875 78.272 2.3226766586303711 79.308 2.251248836517334
		 80.34 2.1775057315826416 81.376 2.1028473377227783 82.412 2.0286962985992432 83.444 1.9563665390014648
		 84.48 1.8870733976364136 85.512 1.8218234777450562 86.548 1.7614874839782715 87.584 1.7067081928253174
		 88.616 1.6579346656799316 89.652 1.6154264211654663 90.688 1.579229474067688 91.72 1.549293041229248
		 92.756 1.5254733562469482 93.788 1.5075321197509766 94.824 1.4952452182769775 95.86 1.4883440732955933
		 96.892 1.4865541458129883 97.928 1.4895752668380737 98.964 1.4971005916595459 99.996 1.5087919235229492
		 101.032 1.5242619514465332 102.064 1.5430734157562256 103.1 1.5646699666976929 104.136 1.588408350944519
		 105.168 1.6135315895080566 106.204 1.6391696929931641 107.24 1.6644036769866943 108.272 1.6882385015487671
		 109.308 1.70963454246521 110.34 1.7275533676147461 111.376 1.740991473197937 112.412 1.7490402460098267
		 113.444 1.750887393951416 114.48 1.7459356784820557 115.512 1.733747124671936 116.548 1.7141096591949463
		 117.584 1.6870599985122681 118.616 1.6528733968734741 119.652 1.6120645999908447
		 120.688 1.5654202699661255 121.72 1.5138958692550659 122.756 1.4586107730865479 123.788 1.4008251428604126
		 124.824 1.3418654203414917 125.86 1.2830640077590942 126.892 1.2257614135742187 127.928 1.1711999177932739
		 128.964 1.1204972267150879 129.996 1.0746461153030396 131.032 1.034457802772522 132.064 1.0005067586898804
		 133.1 0.97324115037918091 134.136 0.95289051532745372 135.168 0.93948489427566528
		 136.204 0.93245375156402588 137.236 0.93076574802398682 138.272 0.93932974338531494
		 139.308 0.94367158412933361 140.34 0.96452778577804565 141.376 0.9919953942298888
		 142.412 1.0029534101486206 143.444 1.0456269979476929 144.48 1.0682194232940674 145.512 1.097847580909729
		 146.548 1.1285213232040405 147.584 1.1537840366363525 148.616 1.2105590105056763
		 149.652 1.2372903823852539 150.688 1.2709709405899048 151.72 1.307023286819458 152.756 1.3430641889572144
		 153.788 1.3789341449737549 154.824 1.414381742477417 155.86 1.449130654335022 156.892 1.4809459447860718
		 157.928 1.505608081817627 158.964 1.5538147687911987 159.996 1.5680558681488037 161.032 1.6072521209716797
		 162.064 1.6189156770706177 163.1 1.6505007743835449 164.136 1.6627655029296875 165.168 1.6758583784103394
		 166.204 1.6873494386672974 167.236 1.6962144374847412 168.272 1.7027866840362549
		 169.308 1.7074712514877319 170.34 1.71075439453125 171.376 1.7131168842315674 172.412 1.7150304317474365
		 173.444 1.7169501781463623 174.48 1.7192429304122925 175.512 1.7221888303756714 176.548 1.7259664535522461
		 177.584 1.7306751012802124 178.616 1.7363109588623047 179.652 1.7427905797958374
		 180.688 1.7499405145645142 181.72 1.7575256824493408 182.756 1.7652747631072998 183.788 1.7728977203369141
		 184.824 1.7801257371902466 185.86 1.7867040634155273 186.892 1.7921489477157593 187.928 1.7958749532699585
		 188.964 1.8012397289276123 189.996 1.802271842956543 191.032 1.8033313751220703 192.064 1.8023527860641479
		 193.1 1.7988960742950437 194.136 1.7960134744644165 195.168 1.7915153503417969 196.204 1.7859026193618774
		 197.236 1.779512882232666 198.272 1.7725902795791626 199.308 1.7654358148574829 200.34 1.7583472728729248
		 201.376 1.7516670227050781 202.412 1.7457425594329834 203.444 1.7409204244613647
		 204.48 1.7375336885452271 205.512 1.7359144687652588 206.548 1.7363407611846924 207.584 1.7390227317810059
		 208.616 1.7441288232803345 209.652 1.7517440319061279 210.688 1.7618594169616699
		 211.72 1.7744483947753906 212.756 1.7894299030303955 213.788 1.8066873550415039 214.824 1.8260648250579834
		 215.86 1.8473763465881345 216.892 1.8704214096069338 217.928 1.8950420618057251 218.964 1.9211080074310301
		 219.996 1.9485270977020266 221.032 1.9772356748580935 222.064 2.0072181224822998
		 223.1 2.0384583473205566 224.136 2.070958137512207 225.168 2.1047863960266113 226.204 2.1399672031402588
		 227.236 2.1766054630279541 228.272 2.2147424221038818 229.308 2.2543857097625732
		 230.34 2.2955505847930908 231.376 2.3381648063659668 232.412 2.3821189403533936 233.444 2.4272143840789795
		 234.48 2.4731674194335937 235.512 2.5195801258087158 236.548 2.5659987926483154 237.584 2.611846923828125
		 238.616 2.6564846038818359 239.652 2.6991991996765137 240.688 2.7392063140869141
		 241.72 2.7739982604980469 242.756 2.7979874610900879 243.788 2.8360435962677002 244.824 2.8599698543548584
		 245.86 2.8755722045898437 246.892 2.884415864944458 247.928 2.8860931396484375 248.96 2.8790969848632812
		 249.996 2.8606956005096436 251.032 2.8507051467895508 252.064 2.8058540821075439
		 253.1 2.7776010036468506 254.136 2.7371304035186768 255.168 2.6883251667022705 256.204 2.6333911418914795
		 257.236 2.5728528499603271 258.272 2.5073258876800537;
	setAttr ".ktv[250:290]" 259.308 2.4375109672546387 260.34 2.3641593456268311
		 261.376 2.2881255149841309 262.412 2.2102601528167725 263.444 2.1314764022827148
		 264.48 2.0526280403137207 265.512 1.9745578765869143 266.548 1.8980486392974851 267.584 1.8237859010696413
		 268.616 1.7523791790008545 269.652 1.6842926740646362 270.688 1.6198878288269043
		 271.72 1.5594027042388916 272.756 1.5029655694961548 273.788 1.4505969285964966 274.824 1.4022489786148071
		 275.86 1.3577542304992676 276.892 1.3169313669204712 277.928 1.2795525789260864 278.96 1.247154712677002
		 279.996 1.2230207920074463 281.032 1.1766221523284912 282.064 1.1573750972747803
		 283.1 1.1347099542617798 284.136 1.113088846206665 285.168 1.0964934825897217 286.204 1.062336802482605
		 287.236 1.0481629371643066 288.272 1.0340077877044678 289.308 1.0001047849655151
		 290.34 0.98396861553192139 291.376 0.96337729692459106 292.412 0.94089102745056152
		 293.444 0.91786617040634155 294.48 0.89443039894104004 295.512 0.87077081203460693
		 296.548 0.84715193510055542 297.584 0.82386571168899536 298.616 0.80124318599700928
		 299.652 0.77962017059326172 300.688 0.75933355093002319;
createNode animCurveTA -n "Bip01_Spine2_rotateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 291 ".ktv";
	setAttr ".ktv[0:249]"  0.688 2.2052600383758545 1.72 2.0737226009368896 2.756 1.9423964023590088
		 3.788 1.813433885574341 4.824 1.6888117790222168 5.86 1.5702090263366699 6.892 1.4590026140213013
		 7.928 1.3561460971832275 8.964 1.2622859477996826 9.996 1.1775708198547363 11.032 1.1018834114074707
		 12.064 1.0347174406051636 13.1 0.97540849447250377 14.136 0.9230785369873048 15.168 0.87691342830657959
		 16.204 0.83606815338134766 17.24 0.7998613715171814 18.272 0.76776218414306641 19.308 0.74079376459121704
		 20.34 0.7224801778793335 21.376 0.69367873668670654 22.412 0.67545628547668457 23.444 0.66355735063552856
		 24.48 0.65766191482543945 25.512 0.65946346521377563 26.548 0.66364794969558716 27.584 0.68653148412704468
		 28.616 0.6985468864440918 29.652 0.75125956535339355 30.688 0.78543317317962646 31.72 0.83522534370422363
		 32.756 0.89637678861618042 33.788 0.96646445989608765 34.824 1.0450056791305542 35.86 1.1312574148178101
		 36.892 1.2244248390197754 37.928 1.3236434459686279 38.964 1.4280045032501221 39.996 1.5366737842559814
		 41.032 1.6489073038101196 42.064 1.7640231847763062 43.1 1.8815330266952515 44.136 2.0010309219360352
		 45.168 2.1223213672637939 46.204 2.245295524597168 47.24 2.3699929714202881 48.272 2.4965155124664307
		 49.308 2.6250286102294922 50.34 2.7557437419891357 51.376 2.8888485431671143 52.412 3.0245010852813721
		 53.444 3.1627905368804932 54.48 3.3037772178649902 55.512 3.4474289417266846 56.548 3.5936360359191895
		 57.584 3.7422118186950688 58.616 3.892884492874146 59.652 4.0453381538391113 60.688 4.1991634368896484
		 61.72 4.3538656234741211 62.756 4.5089497566223145 63.788 4.663780689239502 64.824 4.8177566528320313
		 65.86 4.9701776504516602 66.892 5.1203665733337402 67.928 5.2676215171813965 68.964 5.4112763404846191
		 69.996 5.5506672859191895 71.032 5.6851754188537598 72.064 5.8142399787902832 73.1 5.9373903274536133
		 74.136 6.054236888885498 75.168 6.1644783020019531 76.204 6.2678790092468262 77.24 6.3642978668212891
		 78.272 6.4536819458007812 79.308 6.5361042022705078 80.34 6.6117458343505859 81.376 6.6808891296386719
		 82.412 6.743929386138916 83.444 6.8013319969177246 84.48 6.8536181449890137 85.512 6.9013471603393555
		 86.548 6.9450778961181641 87.584 6.9853296279907227 88.616 7.0225796699523926 89.652 7.0570592880249023
		 90.688 7.0889096260070801 91.72 7.1180081367492676 92.756 7.1440834999084473 93.788 7.1666464805603027
		 94.824 7.1849942207336417 95.86 7.1982898712158194 96.892 7.2055974006652832 97.928 7.2059731483459464
		 98.964 7.1985135078430176 99.996 7.1824326515197763 101.032 7.1571002006530762 102.064 7.1221098899841309
		 103.1 7.0772919654846191 104.136 7.0226936340332031 105.168 6.9586067199707031 106.204 6.8854804039001465
		 107.24 6.8039240837097168 108.272 6.7146525382995605 109.308 6.6184115409851074 110.34 6.5159912109375
		 111.376 6.4081802368164062 112.412 6.295717716217041 113.444 6.1793508529663086 114.48 6.0598464012145996
		 115.512 5.9378542900085449 116.548 5.8140859603881836 117.584 5.6892852783203125
		 118.616 5.5642156600952148 119.652 5.4396443367004395 120.688 5.3164429664611816
		 121.72 5.1954345703125 122.756 5.077451229095459 123.788 4.9633359909057617 124.824 4.8538198471069336
		 125.86 4.7496232986450195 126.892 4.6513128280639648 127.928 4.5594034194946289 128.964 4.4742083549499512
		 129.996 4.3959746360778809 131.032 4.3247723579406738 132.064 4.2605085372924805
		 133.1 4.2030205726623535 134.136 4.1520066261291504 135.168 4.1091947555541992 136.204 4.0801730155944824
		 137.236 4.0319147109985352 138.272 3.9933586120605464 139.308 3.9836866855621338
		 140.34 3.9536294937133789 141.376 3.9279463291168217 142.412 3.9205880165100098 143.444 3.8967819213867187
		 144.48 3.8870613574981689 145.512 3.8762662410736084 146.548 3.866718053817749 147.584 3.8602249622344966
		 148.616 3.8506064414978027 149.652 3.8484635353088383 150.688 3.8476126194000244
		 151.72 3.8488471508026123 152.756 3.8525202274322514 153.788 3.858528614044189 154.824 3.8666045665740962
		 155.86 3.8763673305511475 156.892 3.8865749835968013 157.928 3.8952353000640874 158.964 3.9140141010284428
		 159.996 3.9197995662689213 161.032 3.9361588954925537 162.064 3.9410066604614253
		 163.1 3.9539787769317627 164.136 3.958859920501709 165.168 3.9639773368835445 166.204 3.9684174060821529
		 167.236 3.9718606472015381 168.272 3.974590539932251 169.308 3.9768936634063716 170.34 3.9790143966674805
		 171.376 3.981189489364624 172.412 3.9835295677185054 173.444 3.9861247539520264 174.48 3.9889347553253178
		 175.512 3.9918820858001709 176.548 3.9948360919952388 177.584 3.9976496696472172
		 178.616 4.0001716613769531 179.652 4.0022182464599609 180.688 4.0035324096679687
		 181.72 4.0039153099060059 182.756 4.003087043762207 183.788 4.0007834434509277 184.824 3.9966607093811035
		 185.86 3.9903700351715088 186.892 3.9824328422546387 187.928 3.9743630886077881 188.964 3.9512083530426025
		 189.996 3.9434363842010498 191.032 3.915844202041626 192.064 3.8850028514862056 193.1 3.8410754203796391
		 194.136 3.81379246711731 195.168 3.7738001346588135 196.204 3.7267487049102788 197.236 3.6746201515197754
		 198.272 3.6174097061157227 199.308 3.5551676750183105 200.34 3.4879469871520996 201.376 3.415802001953125
		 202.412 3.3388388156890869 203.444 3.2571811676025391 204.48 3.1710376739501953 205.512 3.0806851387023926
		 206.548 2.9864957332611084 207.584 2.8889594078063965 208.616 2.7886574268341064
		 209.652 2.6863095760345459 210.688 2.5827226638793945 211.72 2.4787237644195557 212.756 2.375211238861084
		 213.788 2.2730188369750977 214.824 2.1728975772857666 215.86 2.0755295753479004 216.892 1.9813989400863647
		 217.928 1.8908228874206541 218.964 1.8039537668228149 219.996 1.7207614183425903
		 221.032 1.6410704851150513 222.064 1.5645644664764404 223.1 1.4908978939056396 224.136 1.4196542501449585
		 225.168 1.3504577875137329 226.204 1.2829949855804443 227.236 1.2170666456222534
		 228.272 1.1525707244873047 229.308 1.0895569324493408 230.34 1.0281991958618164 231.376 0.96870273351669312
		 232.412 0.91142851114273071 233.444 0.85677075386047363 234.48 0.80511295795440674
		 235.512 0.75684386491775513 236.548 0.71230006217956543 237.584 0.6717764139175415
		 238.616 0.63549059629440308 239.652 0.6035570502281189 240.688 0.5760071873664856
		 241.72 0.55372416973114014 242.756 0.53914153575897217 243.788 0.51834213733673096
		 244.824 0.50659120082855225 245.86 0.49991625547409058 246.892 0.49698013067245489
		 247.928 0.49759778380393982 248.96 0.50222855806350708 249.996 0.51190900802612305
		 251.032 0.51663076877593994 252.064 0.5366782546043396 253.1 0.54831933975219727
		 254.136 0.56423968076705933 255.168 0.58243453502655029 256.204 0.60164934396743774
		 257.236 0.62138193845748901 258.272 0.64113116264343262;
	setAttr ".ktv[250:290]" 259.308 0.66036343574523926 260.34 0.67855954170227051
		 261.376 0.69525402784347534 262.412 0.7100059986114502 263.444 0.72249352931976318
		 264.48 0.73244106769561768 265.512 0.73971796035766602 266.548 0.74430370330810547
		 267.584 0.74630677700042725 268.616 0.74598205089569092 269.652 0.74369531869888306
		 270.688 0.73994022607803345 271.72 0.73531043529510498 272.756 0.73045903444290161
		 273.788 0.72609728574752808 274.824 0.72292846441268921 275.86 0.72159910202026367
		 276.892 0.72271347045898438 277.928 0.72671729326248169 278.96 0.73305457830429077
		 279.996 0.74012190103530884 281.032 0.76266926527023315 282.064 0.77602362632751465
		 283.1 0.79458075761795044 284.136 0.81453794240951538 285.168 0.83134263753890991
		 286.204 0.86934727430343628 287.236 0.88508838415145874 288.272 0.90035432577133179
		 289.308 0.93305844068527222 290.34 0.94558972120285034 291.376 0.95910012722015381
		 292.412 0.97079509496688854 293.444 0.97931671142578114 294.48 0.98477184772491466
		 295.512 0.98734629154205322 296.548 0.9873771071434021 297.584 0.98526644706726063
		 298.616 0.98144763708114635 299.652 0.97633773088455211 300.688 0.97031015157699585;
createNode animCurveTA -n "Bip01_Neck_rotateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 291 ".ktv";
	setAttr ".ktv[0:249]"  0.688 0.10558440536260605 1.72 0.10430417209863663
		 2.756 0.10424020141363144 3.788 0.10528562217950821 4.824 0.10738082230091095 5.86 0.11050083488225937
		 6.892 0.11463881283998491 7.928 0.11983191221952437 8.964 0.1261087954044342 9.996 0.13347440958023071
		 11.032 0.14189533889293671 12.064 0.15131999552249908 13.1 0.1616646945476532 14.136 0.17280144989490509
		 15.168 0.18462091684341431 16.204 0.19695308804512024 17.24 0.20965048670768738 18.272 0.22256363928318024
		 19.308 0.23553308844566345 20.34 0.24844481050968167 21.376 0.26117807626724243 22.412 0.27362868189811707
		 23.444 0.28573119640350342 24.48 0.2974141538143158 25.512 0.30862867832183838 26.548 0.31937232613563538
		 27.584 0.32958695292472839 28.616 0.3392452597618103 29.652 0.34830990433692932 30.688 0.35671219229698181
		 31.72 0.36436358094215393 32.756 0.37115520238876343 33.788 0.37693724036216736 34.824 0.38158038258552551
		 35.86 0.38490384817123413 36.892 0.38676744699478149 37.928 0.38725072145462036 38.964 0.38660699129104614
		 39.996 0.3817766010761261 41.032 0.37924155592918396 42.064 0.36879369616508484 43.1 0.36441254615783691
		 44.136 0.34779521822929382 45.168 0.33836188912391663 46.204 0.32552069425582886
		 47.24 0.31076911091804504 48.272 0.29497313499450684 49.308 0.27832764387130737 50.34 0.26103886961936951
		 51.376 0.2432769387960434 52.412 0.22523719072341922 53.444 0.20700220763683319 54.48 0.18869955837726593
		 55.512 0.17040060460567474 56.548 0.15217109024524689 57.584 0.13404190540313721
		 58.616 0.11603151261806487 59.652 0.098128862679004669 60.688 0.080268889665603638
		 61.72 0.062444880604743958 62.756 0.044616874307394028 63.788 0.026748787611722946
		 64.824 0.0088239181786775589 65.86 -0.0091840829700231552 66.892 -0.027233162894845009
		 67.928 -0.045273549854755402 68.964 -0.063227444887161255 69.996 -0.080978080630302429
		 71.032 -0.098394036293029785 72.064 -0.11528264731168746 73.1 -0.1314355880022049
		 74.136 -0.14657615125179291 75.168 -0.16045539081096649 76.204 -0.17277896404266357
		 77.24 -0.18328468501567841 78.272 -0.1917034238576889 79.308 -0.19782480597496033
		 80.34 -0.20146512985229492 81.376 -0.20250135660171509 82.412 -0.20088891685009003
		 83.444 -0.19662843644618988 84.48 -0.18978340923786163 85.512 -0.18048113584518433
		 86.548 -0.16891460120677948 87.584 -0.1553194522857666 88.616 -0.13994359970092773
		 89.652 -0.12313582003116609 90.688 -0.10515191406011581 91.72 -0.086314931511878967
		 92.756 -0.066884078085422516 93.788 -0.047130674123764038 94.824 -0.02723035030066967
		 95.86 -0.0073586096987128258 96.892 0.012369505129754543 97.928 0.031859550625085831
		 98.964 0.05103795975446701 99.996 0.069874241948127747 101.032 0.088321618735790253
		 102.064 0.10633236169815063 103.1 0.12383522093296051 104.136 0.14074611663818359
		 105.168 0.15693739056587219 106.204 0.17226305603981018 107.24 0.18654622137546539
		 108.272 0.19958627223968506 109.308 0.21117380261421204 110.34 0.22107146680355072
		 111.376 0.22906827926635742 112.412 0.23495806753635406 113.444 0.23858828842639926
		 114.48 0.23982289433479306 115.512 0.23857711255550387 116.548 0.234832689166069
		 117.584 0.22861576080322268 118.616 0.22006845474243164 119.652 0.20933438837528229
		 120.688 0.19664284586906433 121.72 0.1822773814201355 122.756 0.16658903658390045
		 123.788 0.14994820952415466 124.824 0.13277043402194977 125.86 0.11542187631130219
		 126.892 0.09823320060968399 127.928 0.081690602004528046 128.964 0.066586002707481384
		 129.996 0.053366906940937042 131.032 0.042044986039400101 132.064 0.032710902392864227
		 133.1 0.025754816830158234 134.136 0.021401716396212578 135.168 0.019734667614102364
		 136.204 0.020748665556311607 137.236 0.02435053326189518 138.272 0.030380880460143089
		 139.308 0.038587164133787155 140.34 0.048720374703407288 141.376 0.060440707951784134
		 142.412 0.07344229519367218 143.444 0.087372161448001862 144.48 0.10194558650255203
		 145.512 0.11685352772474289 146.548 0.13185346126556396 147.584 0.14670661091804504
		 148.616 0.16129203140735626 149.652 0.17560286819934845 150.688 0.18930788338184357
		 151.72 0.20176279544830322 152.756 0.21272085607051849 153.788 0.22221428155899048
		 154.824 0.2302249073982239 155.86 0.23664005100727081 156.892 0.2412501126527786
		 157.928 0.24371594190597531 158.964 0.24380430579185486 159.996 0.24135856330394745
		 161.032 0.23626671731472015 162.064 0.22849598526954654 163.1 0.2180943489074707
		 164.136 0.20516239106655121 165.168 0.18993578851222992 166.204 0.17266832292079926
		 167.236 0.15375636518001556 168.272 0.13358387351036072 169.308 0.1126110404729843
		 170.34 0.091315023601055145 171.376 0.0701870396733284 172.412 0.049710992723703384
		 173.444 0.03034522756934166 174.48 0.012524247169494629 175.512 -0.0033647320233285427
		 176.548 -0.016981380060315132 177.584 -0.028045492246747017 178.616 -0.036303281784057617
		 179.652 -0.041607063263654709 180.688 -0.04385661706328392 181.72 -0.043013669550418854
		 182.756 -0.039107087999582291 183.788 -0.03222859650850296 184.824 -0.022525690495967865
		 185.86 -0.010209351778030396 186.892 0.004446063656359911 187.928 0.021146250888705254
		 188.964 0.039565198123455048 189.996 0.059348169714212425 191.032 0.080136038362979889
		 192.064 0.10154732316732407 193.1 0.1232610270380974 194.136 0.14494162797927856
		 195.168 0.16629026830196381 196.204 0.18704859912395477 197.236 0.20698639750480652
		 198.272 0.22589713335037231 199.308 0.24366199970245361 200.34 0.26014164090156555
		 201.376 0.27527177333831787 202.412 0.28899413347244263 203.444 0.30129596590995789
		 204.48 0.31214508414268494 205.512 0.32154366374015808 206.548 0.32947069406509399
		 207.584 0.33570599555969238 208.616 0.33967983722686768 209.652 0.34471487998962402
		 210.688 0.34669551253318787 211.72 0.3461233377456665 212.756 0.34426704049110413
		 213.788 0.3406812846660614 214.824 0.33518245816230774 215.86 0.32772237062454224
		 216.892 0.31825631856918335 217.928 0.30680179595947266 218.964 0.29341959953308105
		 219.996 0.27820611000061035 221.032 0.26131370663642883 222.064 0.24294161796569824
		 223.1 0.22331316769123077 224.136 0.20268511772155762 225.168 0.18135872483253479
		 226.204 0.15960872173309326 227.236 0.13775892555713654 228.272 0.11606726795434952
		 229.308 0.094779737293720245 230.34 0.074125684797763824 231.376 0.054285507649183273
		 232.412 0.035387616604566574 233.444 0.017532540485262871 234.48 0 235.512 -0.01488027535378933
		 236.548 -0.029436372220516205 237.584 -0.042921878397464752 238.616 -0.055372927337884903
		 239.652 -0.066852502524852753 240.688 -0.077360048890113831 241.72 -0.086949720978736877
		 242.756 -0.095581665635108948 243.788 -0.10324675589799881 244.824 -0.1099153608083725
		 245.86 -0.11553215235471725 246.892 -0.11987877637147903 247.928 -0.12263992428779603
		 248.96 -0.12593244016170502 249.996 -0.12627723813056946 251.032 -0.12513935565948486
		 252.064 -0.12342109531164169 253.1 -0.12040921300649642 254.136 -0.11632546782493593
		 255.168 -0.11140181124210358 256.204 -0.10588602721691132 257.236 -0.10004528611898422
		 258.272 -0.094183266162872314;
	setAttr ".ktv[250:290]" 259.308 -0.088573679327964783 260.34 -0.083521142601966858
		 261.376 -0.079268641769886017 262.412 -0.07604604959487915 263.444 -0.074029617011547089
		 264.48 -0.07332165539264679 265.512 -0.073993705213069916 266.548 -0.076089568436145782
		 267.584 -0.079543530941009521 268.616 -0.08426889032125473 269.652 -0.090145647525787354
		 270.688 -0.097007006406784058 271.72 -0.10411186516284943 272.756 -0.10961945354938507
		 273.788 -0.12142840027809143 274.824 -0.13345058262348175 275.86 -0.13927832245826721
		 276.892 -0.14654847979545593 277.928 -0.15214014053344727 278.96 -0.1618259996175766
		 279.996 -0.16871865093708038 281.032 -0.17421671748161316 282.064 -0.17892378568649292
		 283.1 -0.18312197923660278 284.136 -0.18407955765724182 285.168 -0.18623241782188416
		 286.204 -0.18682053685188293 287.236 -0.18613991141319275 288.272 -0.1842581033706665
		 289.308 -0.18072961270809174 290.34 -0.17420275509357452 291.376 -0.17000727355480194
		 292.412 -0.16488760709762573 293.444 -0.14944756031036377 294.48 -0.14046791195869446
		 295.512 -0.12786048650741577 296.548 -0.1128389909863472 297.584 -0.096139691770076752
		 298.616 -0.077989339828491211 299.652 -0.058702826499938958 300.688 -0.038671728223562241;
createNode animCurveTA -n "Bip01_Neck_rotateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 291 ".ktv";
	setAttr ".ktv[0:249]"  0.688 -0.41961738467216492 1.72 -0.41298174858093262
		 2.756 -0.41125592589378357 3.788 -0.4139348566532135 4.824 -0.42076835036277771 5.86 -0.43158379197120667
		 6.892 -0.44636771082878113 7.928 -0.46520596742630005 8.964 -0.48816004395484924
		 9.996 -0.51522880792617798 11.032 -0.5462375283241272 12.064 -0.5809369683265686
		 13.1 -0.61890923976898193 14.136 -0.65965104103088379 15.168 -0.70258545875549316
		 16.204 -0.74708569049835205 17.24 -0.79248565435409546 18.272 -0.83812594413757324
		 19.308 -0.8834068775177002 20.34 -0.92780214548110962 21.376 -0.97088837623596191
		 22.412 -1.0122926235198975 23.444 -1.0517758131027222 24.48 -1.0891643762588501 25.512 -1.1243890523910522
		 26.548 -1.1574324369430542 27.584 -1.1883046627044678 28.616 -1.2169934511184692
		 29.652 -1.2434626817703247 30.688 -1.2676348686218262 31.72 -1.2893176078796387 32.756 -1.3082147836685181
		 33.788 -1.3239474296569824 34.824 -1.336106538772583 35.86 -1.3442140817642212 36.892 -1.3478313684463501
		 37.928 -1.3472940921783447 38.964 -1.3437018394470215 39.996 -1.3253440856933594
		 41.032 -1.3163212537765503 42.064 -1.2803970575332642 43.1 -1.2655520439147949 44.136 -1.2097113132476807
		 45.168 -1.1781741380691528 46.204 -1.1352789402008057 47.24 -1.0859899520874023 48.272 -1.0331603288650513
		 49.308 -0.9774041771888734 50.34 -0.91935813426971447 51.376 -0.85960793495178223
		 52.412 -0.79864495992660522 53.444 -0.73687094449996948 54.48 -0.6745564341545105
		 55.512 -0.61192429065704346 56.548 -0.54913163185119629 57.584 -0.4862409234046936
		 58.616 -0.42320090532302856 59.652 -0.35987728834152222 60.688 -0.29604926705360413
		 61.72 -0.2315671443939209 62.756 -0.16626758873462677 63.788 -0.10001580417156219
		 64.824 -0.032714180648326874 65.86 0.035660110414028168 66.892 0.10495929419994354
		 67.928 0.17490144073963165 68.964 0.24509654939174652 69.996 0.31505474448204041
		 71.032 0.38414165377616882 72.064 0.45160979032516479 73.1 0.51653563976287842 74.136 0.57790064811706543
		 75.168 0.63458341360092163 76.204 0.6854475736618042 77.24 0.72937959432601929 78.272 0.76534533500671387
		 79.308 0.79239118099212646 80.34 0.80974465608596802 81.376 0.81680762767791748 82.412 0.81324636936187744
		 83.444 0.79895859956741333 84.48 0.77409946918487549 85.512 0.73903733491897583 86.548 0.69440984725952148
		 87.584 0.64105480909347534 88.616 0.57999676465988159 89.652 0.51238924264907837
		 90.688 0.4394073486328125 91.72 0.36222654581069946 92.756 0.28196236491203308 93.788 0.199602410197258
		 94.824 0.11600761860609053 95.86 0.031834494322538376 96.892 -0.052390184253454208
		 97.928 -0.13627263903617859 98.964 -0.21951724588871002 99.996 -0.30192586779594421
		 101.032 -0.38329395651817322 102.064 -0.46340721845626837 103.1 -0.54196786880493164
		 104.136 -0.61860591173171997 105.168 -0.69278210401535034 106.204 -0.76383256912231445
		 107.24 -0.83094489574432373 108.272 -0.89322823286056519 109.308 -0.94965940713882446
		 110.34 -0.99914723634719849 111.376 -1.0406004190444946 112.412 -1.0729612112045288
		 113.444 -1.0952732563018799 114.48 -1.106723427772522 115.512 -1.1066770553588867
		 116.548 -1.0947506427764893 117.584 -1.0708394050598145 118.616 -1.0352057218551636
		 119.652 -0.98839640617370605 120.688 -0.93131649494171154 121.72 -0.8652266263961792
		 122.756 -0.79175364971160889 123.788 -0.71281158924102783 124.824 -0.63053464889526367
		 125.86 -0.54700416326522827 126.892 -0.46409162878990179 127.928 -0.38440179824829102
		 128.964 -0.31164306402206421 129.996 -0.24814201891422272 131.032 -0.1940494030714035
		 132.064 -0.14992897212505341 133.1 -0.11722633987665178 134.136 -0.096768729388713837
		 135.168 -0.088611885905265808 136.204 -0.092412188649177551 137.236 -0.10747155547142029
		 138.272 -0.13277702033519745 139.308 -0.16711486876010895 140.34 -0.2091059535741806
		 141.376 -0.25729995965957642 142.412 -0.31021630764007568 143.444 -0.36644551157951355
		 144.48 -0.42467424273490906 145.512 -0.48372000455856329 146.548 -0.54254484176635742
		 147.584 -0.60021513700485229 148.616 -0.65622711181640625 149.652 -0.71044987440109253
		 150.688 -0.76164788007736206 151.72 -0.80771827697753906 152.756 -0.84778976440429688
		 153.788 -0.88195854425430298 154.824 -0.91009050607681274 155.86 -0.93185609579086304
		 156.892 -0.9464746117591859 157.928 -0.95285242795944203 158.964 -0.9502040147781371
		 159.996 -0.93802374601364136 161.032 -0.91601282358169545 162.064 -0.88407784700393677
		 163.1 -0.84240609407424927 164.136 -0.79146522283554077 165.168 -0.73204970359802246
		 166.204 -0.66521632671356201 167.236 -0.59226113557815552 168.272 -0.51467406749725342
		 169.308 -0.43409788608551025 170.34 -0.35228446125984192 171.376 -0.27102753520011902
		 172.412 -0.19213484227657318 173.444 -0.11738193035125732 174.48 -0.048379383981227875
		 175.512 0.013344909064471722 176.548 0.066450536251068115 177.584 0.1097666397690773
		 178.616 0.1423257440328598 179.652 0.16341668367385864 180.688 0.17259189486503601
		 181.72 0.16964548826217651 182.756 0.1545996218919754 183.788 0.12771522998809814
		 184.824 0.089537240564823151 185.86 0.040843289345502853 186.892 -0.017343200743198395
		 187.928 -0.083867259323596954 188.964 -0.1574213057756424 189.996 -0.23661260306835177
		 191.032 -0.31998494267463684 192.064 -0.40606585144996643 193.1 -0.49347090721130371
		 194.136 -0.58086758852005005 195.168 -0.66707742214202881 196.204 -0.75102800130844116
		 197.236 -0.8317980170249939 198.272 -0.90861785411834728 199.308 -0.98091906309127808
		 200.34 -1.0482004880905151 201.376 -1.1101493835449219 202.412 -1.1665282249450684
		 203.444 -1.2171821594238281 204.48 -1.262019157409668 205.512 -1.3009600639343262
		 206.548 -1.3338997364044189 207.584 -1.3598276376724243 208.616 -1.3763517141342163
		 209.652 -1.3973276615142822 210.688 -1.4055076837539673 211.72 -1.4029946327209473
		 212.756 -1.3951305150985718 213.788 -1.3800400495529175 214.824 -1.3569827079772949
		 215.86 -1.325727105140686 216.892 -1.2861645221710205 217.928 -1.2383805513381958
		 218.964 -1.1826554536819458 219.996 -1.1194621324539185 221.032 -1.0494674444198608
		 222.064 -0.97353219985961914 223.1 -0.89267867803573608 224.136 -0.80805134773254395
		 225.168 -0.72090333700180054 226.204 -0.63249081373214722 227.236 -0.54407525062561035
		 228.272 -0.45680072903633118 229.308 -0.37169656157493591 230.34 -0.28965404629707336
		 231.376 -0.21138027310371399 232.412 -0.13739083707332611 233.444 -0.068009436130523682
		 234.48 0 235.512 0.05642013996839524 236.548 0.11155911535024643 237.584 0.16220249235630035
		 238.616 0.20857630670070648 239.652 0.25088584423065186 240.688 0.2893243134021759
		 241.72 0.32402068376541138 242.756 0.35504701733589172 243.788 0.38239121437072754
		 244.824 0.40599828958511353 245.86 0.42573472857475281 246.892 0.44096434116363525
		 247.928 0.45064151287078852 248.96 0.46221205592155457 249.996 0.46348515152931208
		 251.032 0.45982447266578674 252.064 0.45405265688896185 253.1 0.44387754797935486
		 254.136 0.42999553680419922 255.168 0.413196861743927 256.204 0.39427119493484497
		 257.236 0.374177485704422 258.272 0.35390499234199524;
	setAttr ".ktv[250:290]" 259.308 0.3345160186290741 260.34 0.31704637408256531
		 261.376 0.30244442820549011 262.412 0.29155352711677551 263.444 0.28507986664772034
		 264.48 0.28352522850036621 265.512 0.28721883893013 266.548 0.29625096917152405 267.584 0.3105025589466095
		 268.616 0.32965740561485291 269.652 0.35320919752120972 270.688 0.38051632046699524
		 271.72 0.40868806838989258 272.756 0.43044945597648621 273.788 0.47692251205444341
		 274.824 0.52403771877288818 275.86 0.54690498113632202 276.892 0.57538467645645142
		 277.928 0.5972449779510498 278.96 0.63519382476806641 279.996 0.66223603487014771
		 281.032 0.68393009901046753 282.064 0.70262706279754639 283.1 0.71943813562393188
		 284.136 0.72330844402313232 285.168 0.73216414451599121 286.204 0.73486965894699097
		 287.236 0.73248708248138428 288.272 0.72543317079544067 289.308 0.7118498682975769
		 290.34 0.68637758493423462 291.376 0.66992741823196411 292.412 0.64987736940383911
		 293.444 0.58926039934158325 294.48 0.55396777391433716 295.512 0.50437873601913452
		 296.548 0.44530206918716431 297.584 0.37957319617271423 298.616 0.30806103348731995
		 299.652 0.23199960589408875 300.688 0.15299221873283386;
createNode animCurveTA -n "Bip01_Neck_rotateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 291 ".ktv";
	setAttr ".ktv[0:249]"  0.688 0.94351685047149658 1.72 0.84019047021865845
		 2.756 0.7377399206161499 3.788 0.6376347541809082 4.824 0.54100668430328369 5.86 0.44859260320663452
		 6.892 0.36069023609161377 7.928 0.27713477611541748 8.964 0.19732891023159027 9.996 0.12029883265495299
		 11.032 0.044768471270799637 12.064 -0.030739622190594677 13.1 -0.10785523056983948
		 14.136 -0.18819974362850189 15.168 -0.27331545948982239 16.204 -0.3645571768283844
		 17.24 -0.46306008100509649 18.272 -0.56954866647720337 19.308 -0.68444979190826416
		 20.34 -0.80770885944366455 21.376 -0.93885588645935047 22.412 -1.0769802331924438
		 23.444 -1.2208074331283569 24.48 -1.3687697649002075 25.512 -1.5191171169281006 26.548 -1.6699517965316772
		 27.584 -1.8193161487579348 28.616 -1.9652945995330811 29.652 -2.1059975624084473
		 30.688 -2.2396843433380127 31.72 -2.3647844791412354 32.756 -2.4799091815948486 33.788 -2.5839183330535889
		 34.824 -2.675884485244751 35.86 -2.7550714015960693 36.892 -2.8209531307220459 37.928 -2.8717024326324463
		 38.964 -2.9040830135345459 39.996 -2.9447968006134033 41.032 -2.9507062435150146
		 42.064 -2.9489541053771973 43.1 -2.9423592090606689 44.136 -2.9019153118133545 45.168 -2.8722865581512451
		 46.204 -2.8273844718933105 47.24 -2.7707388401031494 48.272 -2.7041807174682617 49.308 -2.6277613639831543
		 50.34 -2.5413558483123779 51.376 -2.4446811676025391 52.412 -2.3373854160308838 53.444 -2.2191348075866699
		 54.48 -2.0897653102874756 55.512 -1.9493043422698977 56.548 -1.7982051372528076 57.584 -1.6373594999313354
		 58.616 -1.4681735038757324 59.652 -1.2925175428390503 60.688 -1.1126189231872559
		 61.72 -0.9310266375541687 62.756 -0.75043624639511108 63.788 -0.57351219654083252
		 64.824 -0.40273481607437134 65.86 -0.2401832640171051 66.892 -0.087420165538787842
		 67.928 0.054625149816274643 68.964 0.18569113314151764 69.996 0.30619615316390991
		 71.032 0.41708073019981384 72.064 0.51971280574798584 73.1 0.61577928066253662 74.136 0.70706826448440552
		 75.168 0.79533249139785767 76.204 0.88220715522766113 77.24 0.96905142068862915 78.272 1.0569385290145874
		 79.308 1.1465762853622437 80.34 1.238303542137146 81.376 1.3322272300720215 82.412 1.4282135963439941
		 83.444 1.5259866714477539 84.48 1.6251844167709351 85.512 1.7254488468170166 86.548 1.8264496326446535
		 87.584 1.927947998046875 88.616 2.0297460556030273 89.652 2.1317782402038574 90.688 2.2339508533477783
		 91.72 2.3361799716949463 92.756 2.4383037090301514 93.788 2.5402109622955322 94.824 2.6418159008026123
		 95.86 2.7430064678192139 96.892 2.843806266784668 97.928 2.944164514541626 98.964 3.0442116260528564
		 99.996 3.1441335678100586 101.032 3.2442820072174072 102.064 3.3451387882232666 103.1 3.4472625255584717
		 104.136 3.5512363910675049 105.168 3.6576263904571533 106.204 3.7669498920440674
		 107.24 3.8795852661132817 108.272 3.9957346916198735 109.308 4.1154327392578125 110.34 4.2384862899780273
		 111.376 4.3643779754638672 112.412 4.4923934936523437 113.444 4.6213703155517578
		 114.48 4.7498102188110352 115.512 4.8759012222290039 116.548 4.9974169731140137 117.584 5.1117744445800781
		 118.616 5.2160816192626953 119.652 5.3072853088378906 120.688 5.3822131156921387
		 121.72 5.43768310546875 122.756 5.470740795135498 123.788 5.4787259101867676 124.824 5.4596047401428223
		 125.86 5.413485050201416 126.892 5.3433899879455566 127.928 5.2474212646484375 128.964 5.1166129112243652
		 129.996 4.9515976905822754 131.032 4.7660775184631348 132.064 4.5685081481933594
		 133.1 4.3596200942993164 134.136 4.1417574882507324 135.168 3.9192550182342534 136.204 3.6961045265197754
		 137.236 3.4757382869720459 138.272 3.2609224319458008 139.308 3.0536396503448486
		 140.34 2.8551154136657715 141.376 2.6658856868743896 142.412 2.4858951568603516 143.444 2.3146340847015381
		 144.48 2.1512875556945801 145.512 1.9948763847351074 146.548 1.8444162607192993 147.584 1.6990554332733154
		 148.616 1.5553151369094849 149.652 1.4079488515853882 150.688 1.2617746591567993
		 151.72 1.128925085067749 152.756 1.0105853080749512 153.788 0.90000158548355091 154.824 0.79325246810913086
		 155.86 0.68868011236190796 156.892 0.58753430843353271 157.928 0.49295440316200251
		 158.964 0.40671077370643616 159.996 0.32952618598937988 161.032 0.26216498017311096
		 162.064 0.20519772171974182 163.1 0.15903930366039276 164.136 0.12394338101148605
		 165.168 0.099910922348499298 166.204 0.086750596761703491 167.236 0.08406560868024826
		 168.272 0.091230049729347229 169.308 0.10741297155618668 170.34 0.1316893994808197
		 171.376 0.1630144864320755 172.412 0.20034043490886688 173.444 0.24260246753692627
		 174.48 0.28883036971092224 175.512 0.33817633986473083 176.548 0.38994961977005005
		 177.584 0.44350695610046387 178.616 0.49835243821144098 179.652 0.55400699377059937
		 180.688 0.61004060506820679 181.72 0.66601932048797607 182.756 0.72145307064056396
		 183.788 0.77581983804702759 184.824 0.82858055830001831 185.86 0.87921398878097534
		 186.892 0.92724180221557606 187.928 0.97223204374313366 188.964 1.0139179229736328
		 189.996 1.0522264242172241 191.032 1.0872339010238647 192.064 1.1191465854644775
		 193.1 1.1484056711196899 194.136 1.1755000352859497 195.168 1.2010711431503296 196.204 1.2257176637649536
		 197.236 1.2499823570251465 198.272 1.2742408514022827 199.308 1.2986514568328857
		 200.34 1.3232636451721191 201.376 1.3478759527206421 202.412 1.372130274772644 203.444 1.3954845666885376
		 204.48 1.4173486232757568 205.512 1.4370572566986084 206.548 1.4540513753890991 207.584 1.4674065113067627
		 208.616 1.4758204221725464 209.652 1.4853405952453613 210.688 1.487155556678772 211.72 1.4814895391464233
		 212.756 1.4743348360061646 213.788 1.4618246555328369 214.824 1.444109320640564 215.86 1.4211664199829102
		 216.892 1.3927356004714966 217.928 1.3585168123245239 218.964 1.3181548118591309
		 219.996 1.271348237991333 221.032 1.2178128957748413 222.064 1.1573200225830078 223.1 1.0898152589797974
		 224.136 1.0153635740280151 225.168 0.93422865867614757 226.204 0.84677022695541382
		 227.236 0.75347203016281128 228.272 0.65493756532669067 229.308 0.55184012651443481
		 230.34 0.44491744041442871 231.376 0.33493998646736145 232.412 0.22256945073604584
		 233.444 0.10843826830387115 234.48 0 235.512 -0.12272818386554717 236.548 -0.23867046833038327
		 237.584 -0.35408365726470947 238.616 -0.46836179494857794 239.652 -0.58071249723434448
		 240.688 -0.69015061855316162 241.72 -0.79544037580490112 242.756 -0.89507681131362915
		 243.788 -0.98736131191253673 244.824 -1.0703659057617187 245.86 -1.1420279741287231
		 246.892 -1.1986085176467896 247.928 -1.2347961664199829 248.96 -1.2774186134338379
		 249.996 -1.2807003259658813 251.032 -1.2575281858444214 252.064 -1.2269607782363892
		 253.1 -1.1735942363739014 254.136 -1.0998605489730835 255.168 -1.0080279111862183
		 256.204 -0.90024459362030029 257.236 -0.77922642230987549 258.272 -0.64818847179412842;
	setAttr ".ktv[250:290]" 259.308 -0.51063573360443115 260.34 -0.37024685740470886
		 261.376 -0.23068319261074069 262.412 -0.095420911908149719 263.444 0.032379064708948135
		 264.48 0.15003086626529694 265.512 0.25542733073234558 266.548 0.34711310267448425
		 267.584 0.42429584264755249 268.616 0.4868578314781189 269.652 0.53532761335372925
		 270.688 0.57082563638687134 271.72 0.59455174207687378 272.756 0.60759860277175903
		 273.788 0.61870282888412476 274.824 0.62043321132659912 275.86 0.61923450231552124
		 276.892 0.61763232946395874 277.928 0.61639636754989624 278.96 0.6163100004196167
		 279.996 0.61933755874633789 281.032 0.62544888257980347 282.064 0.63523554801940918
		 283.1 0.64982575178146362 284.136 0.65468418598175049 285.168 0.67163616418838501
		 286.204 0.68601536750793457 287.236 0.69861745834350586 288.272 0.70968228578567505
		 289.308 0.71932178735733032 290.34 0.72868746519088745 291.376 0.73222219944000244
		 292.412 0.73591148853302002 293.444 0.74351435899734497 294.48 0.74733716249465942
		 295.512 0.75270092487335205 296.548 0.7594483494758606 297.584 0.76749151945114136
		 298.616 0.77679294347763062 299.652 0.78705942630767822 300.688 0.79775881767272949;
createNode animCurveTA -n "Bip01_Head_rotateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 291 ".ktv";
	setAttr ".ktv[0:249]"  0.688 -0.081075429916381836 1.72 -0.074885658919811249
		 2.756 -0.068918943405151367 3.788 -0.063316844403743744 4.824 -0.058214820921421051
		 5.86 -0.053709551692008972 6.892 -0.049879029393196106 7.928 -0.046809770166873932
		 8.964 -0.044536549597978592 9.996 -0.043100520968437195 11.032 -0.042490564286708832
		 12.064 -0.042700678110122681 13.1 -0.043699905276298523 14.136 -0.045413732528686523
		 15.168 -0.047781907021999359 16.204 -0.050701435655355453 17.24 -0.054077066481113434
		 18.272 -0.057786226272583008 19.308 -0.061715777963399887 20.34 -0.065754011273384094
		 21.376 -0.069781497120857239 22.412 -0.07367677241563797 23.444 -0.077340811491012573
		 24.48 -0.080684363842010498 25.512 -0.083589501678943634 26.548 -0.085924170911312103
		 27.584 -0.087463431060314178 28.616 -0.089451059699058533 29.652 -0.08977692574262619
		 30.688 -0.089644871652126312 31.72 -0.088245473802089691 32.756 -0.086895272135734558
		 33.788 -0.084698967635631561 34.824 -0.081856891512870789 35.86 -0.078482314944267273
		 36.892 -0.074684463441371918 37.928 -0.070638447999954224 38.964 -0.066426731646060944
		 39.996 -0.062219556421041489 41.032 -0.05818623676896096 42.064 -0.054402459412813187
		 43.1 -0.050991930067539215 44.136 -0.048020914196968079 45.168 -0.045540664345026016
		 46.204 -0.043575737625360489 47.24 -0.042075812816619873 48.272 -0.041011996567249298
		 49.308 -0.040341135114431381 50.34 -0.039997696876525879 51.376 -0.039879992604255676
		 52.412 -0.03995966911315918 53.444 -0.040146734565496445 54.48 -0.04039614275097847
		 55.512 -0.040640287101268768 56.548 -0.040816731750965118 57.584 -0.041007082909345627
		 58.616 -0.040980633348226547 59.652 -0.040881428867578506 60.688 -0.040194611996412277
		 61.72 -0.039592798799276352 62.756 -0.038589343428611755 63.788 -0.037174858152866364
		 64.824 -0.035344138741493225 65.86 -0.033031091094017029 66.892 -0.030203491449356083
		 67.928 -0.026852665469050407 68.964 -0.022948207333683968 69.996 -0.018522728234529495
		 71.032 -0.013580146245658398 72.064 -0.0082036741077899933 73.1 -0.0024547227658331394
		 74.136 0.0035497054923325781 75.168 0.0097103733569383621 76.204 0.015878766775131226
		 77.24 0.021900532767176628 78.272 0.027628103271126747 79.308 0.032921027392148972
		 80.34 0.037602629512548447 81.376 0.041549764573574066 82.412 0.044644467532634735
		 83.444 0.046781882643699646 84.48 0.047902055084705353 85.512 0.047963697463274002
		 86.548 0.046973560005426407 87.584 0.044968307018280029 88.616 0.042010128498077393
		 89.652 0.038218799978494644 90.688 0.033703483641147614 91.72 0.028632832691073418
		 92.756 0.023163512349128723 93.788 0.017494015395641327 94.824 0.011779974214732647
		 95.86 0.006197790615260601 96.892 0.00089117040624842048 97.928 -0.0039972229860723019
		 98.964 -0.0083778789266943932 99.996 -0.012175888754427433 101.032 -0.015350218862295151
		 102.064 -0.017881283536553383 103.1 -0.019814223051071167 104.136 -0.021174618974328041
		 105.168 -0.022048123180866241 106.204 -0.022498913109302521 107.24 -0.022631591185927391
		 108.272 -0.022531846538186073 109.308 -0.02228885143995285 110.34 -0.021987972781062126
		 111.376 -0.021725965663790703 112.412 -0.021195990964770317 113.444 -0.020992424339056015
		 114.48 -0.020789571106433868 115.512 -0.0206338781863451 116.548 -0.020375704392790794
		 117.584 -0.020275885239243507 118.616 -0.019921008497476578 119.652 -0.019737290218472481
		 120.688 -0.018954131752252579 121.72 -0.018400371074676514 122.756 -0.017541268840432167
		 123.788 -0.01640620268881321 124.824 -0.015121920965611936 125.86 -0.013911973685026169
		 126.892 -0.010627660900354385 127.928 -0.0088055254891514778 128.964 -0.0063218921422958374
		 129.996 -0.0034560852218419313 131.032 -0.00039861767436377704 132.064 0.0028173378668725491
		 133.1 0.0061291893944144249 134.136 0.0094545800238847733 135.168 0.01274101622402668
		 136.204 0.01590767502784729 137.236 0.018894979730248451 138.272 0.021678607910871506
		 139.308 0.024193614721298218 140.34 0.026399338617920876 141.376 0.028310729190707207
		 142.412 0.029905697330832485 143.444 0.03119870088994503 144.48 0.032223712652921677
		 145.512 0.032989323139190674 146.548 0.033473167568445206 147.584 0.034160230308771133
		 148.616 0.034397181123495102 149.652 0.0346556156873703 150.688 0.03494635596871376
		 151.72 0.035290144383907318 152.756 0.035747013986110687 153.788 0.036346849054098129
		 154.824 0.037118889391422272 155.86 0.038091879338026047 156.892 0.039279844611883163
		 157.928 0.040687024593353271 158.964 0.04231681302189827 159.996 0.044153846800327301
		 161.032 0.046186570078134537 162.064 0.048378013074398041 163.1 0.050681035965681076
		 164.136 0.053068101406097412 165.168 0.055478125810623169 166.204 0.057854492217302329
		 167.236 0.060126412659883506 168.272 0.062121294438838966 169.308 0.063567690551280975
		 170.34 0.065986968576908112 171.376 0.066520735621452332 172.412 0.0673527792096138
		 173.444 0.067189775407314301 174.48 0.06649511307477951 175.512 0.065152540802955627
		 176.548 0.063086435198783875 177.584 0.060306970030069351 178.616 0.05679505318403244
		 179.652 0.052618905901908875 180.688 0.047819063067436218 181.72 0.04247969388961792
		 182.756 0.036706630140542984 183.788 0.030617078766226772 184.824 0.024336408823728561
		 185.86 0.017983173951506615 186.892 0.011688642203807831 187.928 0.0055655092000961304
		 188.964 -0.00027633659192360938 189.996 -0.0057811806909739971 191.032 -0.010865140706300735
		 192.064 -0.01547738630324602 193.1 -0.019604196771979332 194.136 -0.023235723376274109
		 195.168 -0.026251843199133873 196.204 -0.028388991951942447 197.236 -0.0320851169526577
		 198.272 -0.033347927033901215 199.308 -0.034443877637386322 200.34 -0.036387119442224503
		 201.376 -0.036856383085250854 202.412 -0.037840288132429123 203.444 -0.038105946034193039
		 204.48 -0.038251664489507675 205.512 -0.038239270448684692 206.548 -0.038039803504943848
		 207.584 -0.037697523832321167 208.616 -0.037225238978862762 209.652 -0.036648716777563095
		 210.688 -0.036024041473865509 211.72 -0.035392414778470993 212.756 -0.034802667796611786
		 213.788 -0.034307733178138733 214.824 -0.033944834023714066 215.86 -0.033757284283638
		 216.892 -0.033732544630765915 217.928 -0.034190196543931961 218.964 -0.034498784691095352
		 219.996 -0.035991977900266647 221.032 -0.037001907825469971 222.064 -0.038495361804962158
		 223.1 -0.04031696543097496 224.136 -0.042359016835689545 225.168 -0.044567342847585678
		 226.204 -0.046870537102222443 227.236 -0.04920649528503418 228.272 -0.051482044160366058
		 229.308 -0.053605362772941589 230.34 -0.055502526462078094 231.376 -0.05709886550903321
		 232.412 -0.058334901928901672 233.444 -0.059163343161344535 234.48 -0.059548746794462204
		 235.512 -0.059490364044904716 236.548 -0.058973163366317749 237.584 -0.058017216622829437
		 238.616 -0.05664486438035965 239.652 -0.054875630885362625 240.688 -0.052773799747228622
		 241.72 -0.050330724567174911 242.756 -0.04761011153459549 243.788 -0.044647559523582458
		 244.824 -0.041466023772954941 245.86 -0.03807801753282547 246.892 -0.034537579864263535
		 247.928 -0.03085637092590332 248.96 -0.027047045528888702 249.996 -0.023139007389545441
		 251.032 -0.019148420542478561 252.064 -0.015113718807697294 253.1 -0.01105037797242403
		 254.136 -0.0069964337162673482 255.168 -0.0030138250440359116 256.204 0.00092451466480270017
		 257.236 0.0047546406276524067 258.272 0.0084558287635445595;
	setAttr ".ktv[250:290]" 259.308 0.012000683695077896 260.34 0.015416945330798626
		 261.376 0.018675308674573898 262.412 0.021790081635117531 263.444 0.024796325713396072
		 264.48 0.027681140229105949 265.512 0.030474010854959484 266.548 0.033169884234666824
		 267.584 0.03579123318195343 268.616 0.038320466876029968 269.652 0.040718972682952881
		 270.688 0.042844943702220917 271.72 0.044348869472742081 272.756 0.046993974596261978
		 273.788 0.049149513244628906 274.824 0.049744360148906708 275.86 0.05143515020608902
		 276.892 0.051855575293302536 277.928 0.052666429430246353 278.96 0.052788883447647095
		 279.996 0.052656501531600952 281.032 0.052332133054733276 282.064 0.051735572516918182
		 283.1 0.050898186862468719 284.136 0.049836572259664536 285.168 0.048571478575468063
		 286.204 0.047150030732154846 287.236 0.045572035014629364 288.272 0.043848369270563126
		 289.308 0.041973337531089783 290.34 0.03993142768740654 291.376 0.037720602005720139
		 292.412 0.03527405858039856 293.444 0.032560914754867554 294.48 0.029559008777141571
		 295.512 0.026227174326777458 296.548 0.022556079551577568 297.584 0.018569553270936012
		 298.616 0.014288158155977724 299.652 0.0097560873255133629 300.688 0.005056421272456646;
createNode animCurveTA -n "Bip01_Head_rotateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 291 ".ktv";
	setAttr ".ktv[0:249]"  0.688 -1.169425368309021 1.72 -1.0902509689331055
		 2.756 -1.0131572484970093 3.788 -0.94004869461059559 4.824 -0.87274914979934692 5.86 -0.81302052736282349
		 6.892 -0.76239681243896484 7.928 -0.72215378284454346 8.964 -0.69330418109893799
		 9.996 -0.67659717798233032 11.032 -0.67246514558792114 12.064 -0.68097203969955444
		 13.1 -0.70181369781494141 14.136 -0.73433035612106323 15.168 -0.77753263711929321
		 16.204 -0.83009517192840576 17.24 -0.89044082164764404 18.272 -0.95678812265396118
		 19.308 -1.0272026062011719 20.34 -1.0996135473251343 21.376 -1.1718534231185913 22.412 -1.241791844367981
		 23.444 -1.3073371648788452 24.48 -1.3665183782577515 25.512 -1.4174984693527222 26.548 -1.4575036764144897
		 27.584 -1.4829491376876831 28.616 -1.5126540660858154 29.652 -1.5165342092514038
		 30.688 -1.5079345703125 31.72 -1.4766533374786377 32.756 -1.4500935077667236 33.788 -1.4083817005157471
		 34.824 -1.3561766147613525 35.86 -1.2967120409011841 36.892 -1.232122540473938 37.928 -1.164704442024231
		 38.964 -1.0968354940414429 39.996 -1.0308088064193726 41.032 -0.96873080730438232
		 42.064 -0.91239774227142345 43.1 -0.86323767900466919 44.136 -0.82227605581283569
		 45.168 -0.79006493091583252 46.204 -0.76667916774749756 47.24 -0.7517777681350708
		 48.272 -0.74463683366775513 49.308 -0.74424242973327637 50.34 -0.74931943416595459
		 51.376 -0.75844061374664307 52.412 -0.7701256275177002 53.444 -0.78289616107940674
		 54.48 -0.79539674520492554 55.512 -0.80608224868774414 56.548 -0.81315171718597412
		 57.584 -0.82110607624053955 58.616 -0.82116830348968506 59.652 -0.81827390193939209
		 60.688 -0.8002048134803772 61.72 -0.78489696979522705 62.756 -0.76011389493942261
		 63.788 -0.72663086652755737 64.824 -0.68459248542785645 65.86 -0.63357084989547729
		 66.892 -0.57323932647705078 67.928 -0.50352603197097778 68.964 -0.42462337017059326
		 69.996 -0.33700886368751526 71.032 -0.24148742854595184 72.064 -0.13922238349914551
		 73.1 -0.031796269118785858 74.136 0.07886514812707901 75.168 0.19047778844833374
		 76.204 0.30053791403770447 77.24 0.4063836932182312 78.272 0.50531035661697388 79.308 0.59461057186126709
		 80.34 0.67175400257110596 81.376 0.73447674512863159 82.412 0.78092336654663086 83.444 0.809792160987854
		 84.48 0.82032448053359985 85.512 0.81240272521972656 86.548 0.78648334741592407 87.584 0.74368757009506226
		 88.616 0.68568098545074463 89.652 0.6146538257598877 90.688 0.53327268362045288 91.72 0.4444776177406311
		 92.756 0.35140874981880188 93.788 0.25718161463737488 94.824 0.1647789478302002 95.86 0.076885692775249481
		 96.892 -0.0042155454866588116 97.928 -0.076732508838176727 98.964 -0.13937273621559143
		 99.996 -0.19147743284702301 101.032 -0.23295159637928009 102.064 -0.26419678330421448
		 103.1 -0.28603380918502808 104.136 -0.29960179328918457 105.168 -0.306255042552948
		 106.204 -0.30745631456375122 107.24 -0.30463898181915283 108.272 -0.29916563630104065
		 109.308 -0.29222971200942993 110.34 -0.28521871566772461 111.376 -0.27965742349624634
		 112.412 -0.26877620816230774 113.444 -0.26463791728019714 114.48 -0.26033931970596313
		 115.512 -0.25702989101409912 116.548 -0.25135552883148193 117.584 -0.24946917593479156
		 118.616 -0.24314594268798828 119.652 -0.24038673937320706 120.688 -0.22934453189373014
		 121.72 -0.22215390205383301 122.756 -0.21139417588710785 123.788 -0.19749376177787781
		 124.824 -0.18201866745948792 125.86 -0.16767445206642151 126.892 -0.12912008166313171
		 127.928 -0.10783043503761292 128.964 -0.078688763082027435 129.996 -0.044841252267360687
		 131.032 -0.0080875307321548462 132.064 0.031125728040933609 133.1 0.072206377983093262
		 134.136 0.11447378993034363 135.168 0.1571945995092392 136.204 0.19960960745811462
		 137.236 0.24096859991550448 138.272 0.28044095635414124 139.308 0.31729850172996521
		 140.34 0.35091656446456909 141.376 0.38082298636436462 142.412 0.40676835179328918
		 143.444 0.4287312924861908 144.48 0.44690844416618347 145.512 0.46108976006507874
		 146.548 0.47063890099525452 147.584 0.48588752746582026 148.616 0.49158975481986994
		 149.652 0.49833798408508301 150.688 0.5058664083480835 151.72 0.51446402072906494
		 152.756 0.52493041753768921 153.788 0.53800231218338013 154.824 0.5542871356010437
		 155.86 0.57424622774124146 156.892 0.5982469916343689 157.928 0.62651950120925903
		 158.964 0.65915161371231079 159.996 0.69601589441299438 161.032 0.73675447702407837
		 162.064 0.78075873851776123 163.1 0.8272165060043335 164.136 0.87510782480239868
		 165.168 0.92326468229293812 166.204 0.9702419638633728 167.236 1.014411449432373
		 168.272 1.0524311065673828 169.308 1.0792498588562012 170.34 1.120715856552124 171.376 1.1286433935165405
		 172.412 1.1354057788848877 173.444 1.1270210742950439 174.48 1.1069633960723877 175.512 1.07426917552948
		 176.548 1.0287959575653076 177.584 0.9711587429046632 178.616 0.90240222215652466
		 179.652 0.82385915517807007 180.688 0.73721373081207275 181.72 0.64441299438476563
		 182.756 0.54757994413375854 183.788 0.4488530158996582 184.824 0.35028600692749023
		 185.86 0.25377064943313599 186.892 0.16093963384628296 187.928 0.073149487376213074
		 188.964 -0.0085727125406265259 189.996 -0.083468861877918243 191.032 -0.15113525092601776
		 192.064 -0.21140867471694946 193.1 -0.2643999457359314 194.136 -0.31043371558189392
		 195.168 -0.34825605154037476 196.204 -0.37494754791259766 197.236 -0.42122554779052734
		 198.272 -0.43713018298149109 199.308 -0.45125365257263184 200.34 -0.47701659798622126
		 201.376 -0.48354995250701904 202.412 -0.49895659089088434 203.444 -0.50395691394805908
		 204.48 -0.50841641426086426 205.512 -0.51126617193222046 206.548 -0.51207345724105835
		 207.584 -0.51105159521102905 208.616 -0.50848156213760376 209.652 -0.50470119714736938
		 210.688 -0.50012010335922241 211.72 -0.4952563345432282 212.756 -0.49072307348251348
		 213.788 -0.48717677593231201 214.824 -0.48528420925140375 215.86 -0.48528721928596497
		 216.892 -0.48700606822967524 217.928 -0.49713891744613647 218.964 -0.50262957811355591
		 219.996 -0.52722716331481934 221.032 -0.54347753524780273 222.064 -0.56723505258560181
		 223.1 -0.59639334678649902 224.136 -0.62957125902175903 225.168 -0.66620099544525146
		 226.204 -0.70554500818252563 227.236 -0.7467387318611145 228.272 -0.7888140082359314
		 229.308 -0.83068215847015381 230.34 -0.8711782693862915 231.376 -0.90915727615356445
		 232.412 -0.94350284337997437 233.444 -0.973197340965271 234.48 -0.99730598926544189
		 235.512 -1.0150009393692017 236.548 -1.0255954265594482 237.584 -1.0285392999649048
		 238.616 -1.0234163999557495 239.652 -1.0099450349807739 240.688 -0.98797184228897095
		 241.72 -0.95745247602462769 242.756 -0.91854536533355713 243.788 -0.87155288457870483
		 244.824 -0.81694090366363525 245.86 -0.75532937049865723 246.892 -0.68745708465576172
		 247.928 -0.61428815126419067 248.96 -0.53690999746322632 249.996 -0.45657363533973694
		 251.032 -0.37459477782249451 252.064 -0.29228910803794861 253.1 -0.21091391146183014
		 254.136 -0.13163892924785614 255.168 -0.055423948913812637 256.204 0.017009364441037178
		 257.236 0.085220634937286377 258.272 0.14903956651687622;
	setAttr ".ktv[250:290]" 259.308 0.20854766666889191 260.34 0.26403790712356567
		 261.376 0.31600087881088257 262.412 0.36502969264984131 263.444 0.411802738904953
		 264.48 0.4569888710975647 265.512 0.50111687183380127 266.548 0.54452967643737793
		 267.584 0.5873560905456543 268.616 0.62944322824478149 269.652 0.67043238878250122
		 270.688 0.70760351419448853 271.72 0.73409593105316162 272.756 0.78158903121948242
		 273.788 0.82028895616531372 274.824 0.83058011531829834 275.86 0.85982382297515869
		 276.892 0.86645293235778809 277.928 0.87677043676376343 278.96 0.87695962190628052
		 279.996 0.86675649881362915 281.032 0.85653424263000488 282.064 0.83994299173355103
		 283.1 0.81850343942642212 284.136 0.79337239265441895 285.168 0.76534867286682129
		 286.204 0.73511219024658203 287.236 0.7031828761100769 288.272 0.66986256837844849
		 289.308 0.63525688648223877 290.34 0.59919160604476929 291.376 0.56128591299057007
		 292.412 0.52101963758468628 293.444 0.47774940729141241 294.48 0.43092638254165649
		 295.512 0.3801475465297699 296.548 0.32522556185722351 297.584 0.26622596383094788
		 298.616 0.20350818336009979 299.652 0.13779556751251221 300.688 0.070063211023807526;
createNode animCurveTA -n "Bip01_Head_rotateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 291 ".ktv";
	setAttr ".ktv[0:249]"  0.688 4.343040943145752 1.72 4.2712559700012207 2.756 4.1987576484680176
		 3.788 4.126220703125 4.824 4.0543694496154785 5.86 3.9838895797729497 6.892 3.9153387546539311
		 7.928 3.8490943908691411 8.964 3.7854430675506587 9.996 3.7245676517486568 11.032 3.6665089130401611
		 12.064 3.6111838817596436 13.1 3.5584008693695068 14.136 3.5079965591430664 15.168 3.4598350524902344
		 16.204 3.41387939453125 17.24 3.3701779842376709 18.272 3.3289346694946289 19.308 3.2905187606811523
		 20.34 3.255455493927002 21.376 3.224280834197998 22.412 3.1976337432861328 23.444 3.1761903762817383
		 24.48 3.1605708599090576 25.512 3.1512515544891357 26.548 3.1478993892669678 27.584 3.1491978168487549
		 28.616 3.1648976802825928 29.652 3.17138671875 30.688 3.1987767219543457 31.72 3.2332968711853027
		 32.756 3.2516541481018066 33.788 3.2759034633636475 34.824 3.2993772029876709 35.86 3.3182621002197266
		 36.892 3.3303084373474121 37.928 3.3334262371063232 38.964 3.3258240222930908 39.996 3.3060915470123291
		 41.032 3.2733068466186523 42.064 3.2271571159362793 43.1 3.1679680347442627 44.136 3.0967311859130859
		 45.168 3.015017032623291 46.204 2.9249210357666016 47.24 2.8288998603820801 48.272 2.7297229766845703
		 49.308 2.630253791809082 50.34 2.5333759784698486 51.376 2.4417629241943359 52.412 2.3578066825866699
		 53.444 2.2835431098937988 54.48 2.2206075191497803 55.512 2.1716384887695312 56.548 2.1409347057342529
		 57.584 2.1043334007263184 58.616 2.0911855697631836 59.652 2.0930957794189453 60.688 2.1137404441833496
		 61.72 2.1325528621673584 62.756 2.1622202396392822 63.788 2.1995973587036133 64.824 2.2423253059387207
		 65.86 2.2889096736907959 66.892 2.3379518985748291 67.928 2.3881974220275879 68.964 2.4386453628540039
		 69.996 2.4885413646697998 71.032 2.5373795032501221 72.064 2.5848376750946045 73.1 2.6307921409606934
		 74.136 2.6753716468811035 75.168 2.7188606262207031 76.204 2.7616157531738281 77.24 2.8040354251861572
		 78.272 2.846463680267334 79.308 2.8892481327056885 80.34 2.9327011108398438 81.376 2.9770941734313965
		 82.412 3.0226655006408691 83.444 3.0695827007293701 84.48 3.1180808544158936 85.512 3.1684467792510986
		 86.548 3.2210354804992676 87.584 3.2762181758880615 88.616 3.3344273567199707 89.652 3.3961544036865234
		 90.688 3.461890697479248 91.72 3.5321788787841797 92.756 3.6075417995452881 93.788 3.6883847713470459
		 94.824 3.7748711109161381 95.86 3.8669605255126949 96.892 3.9643452167510982 97.928 4.066563606262207
		 98.964 4.1728153228759766 99.996 4.2820611000061035 101.032 4.3930516242980957 102.064 4.5042524337768555
		 103.1 4.6140246391296387 104.136 4.7206606864929199 105.168 4.8225560188293457 106.204 4.918302059173584
		 107.24 5.00677490234375 108.272 5.0873098373413086 109.308 5.1595697402954102 110.34 5.2206315994262695
		 111.376 5.2648911476135254 112.412 5.3455252647399902 113.444 5.3774895668029785
		 114.48 5.4122557640075684 115.512 5.4405813217163086 116.548 5.4951705932617187 117.584 5.5121035575866699
		 118.616 5.5616793632507324 119.652 5.5778026580810547 120.688 5.6262311935424805
		 121.72 5.6468911170959473 122.756 5.6697139739990234 123.788 5.689178466796875 124.824 5.7019548416137695
		 125.86 5.7071151733398437 126.892 5.696866512298584 127.928 5.6805262565612793 128.964 5.6510462760925293
		 129.996 5.6089887619018555 131.032 5.5551872253417969 132.064 5.4906821250915527
		 133.1 5.4170355796813965 134.136 5.3361959457397461 135.168 5.2503848075866699 136.204 5.1619668006896973
		 137.236 5.0732603073120117 138.272 4.9864702224731445 139.308 4.9035296440124512
		 140.34 4.8260345458984375 141.376 4.7551579475402832 142.412 4.691500186920166 143.444 4.6350674629211426
		 144.48 4.585334300994873 145.512 4.5435805320739746 146.548 4.5129423141479492 147.584 4.4536113739013672
		 148.616 4.4265909194946289 149.652 4.3918519020080566 150.688 4.3521156311035156
		 151.72 4.3081111907958984 152.756 4.2585721015930176 153.788 4.2025961875915527 154.824 4.1397285461425781
		 155.86 4.0700235366821289 156.892 3.9940264225006099 157.928 3.9127910137176514 158.964 3.8277783393859863
		 159.996 3.7407839298248291 161.032 3.6538035869598393 162.064 3.5689351558685303
		 163.1 3.488323450088501 164.136 3.4140157699584961 165.168 3.3479888439178467 166.204 3.2919952869415283
		 167.236 3.2474668025970459 168.272 3.2157647609710693 169.308 3.1982696056365967
		 170.34 3.1883845329284668 171.376 3.1926250457763672 172.412 3.2288374900817871 173.444 3.2584118843078613
		 174.48 3.3047158718109131 175.512 3.3641853332519531 176.548 3.4345331192016602 177.584 3.5147342681884766
		 178.616 3.6036503314971924 179.652 3.7001125812530518 180.688 3.8028557300567627
		 181.72 3.910491943359375 182.756 4.0215811729431152 183.788 4.1345858573913574 184.824 4.2479109764099121
		 185.86 4.3599157333374023 186.892 4.4689831733703613 187.928 4.5735888481140137 188.964 4.6723027229309082
		 189.996 4.7637605667114258 191.032 4.8468432426452637 192.064 4.9206194877624512
		 193.1 4.9843606948852539 194.136 5.0375394821166992 195.168 5.0785884857177734 196.204 5.1048159599304199
		 197.236 5.1378769874572754 198.272 5.1432170867919922 199.308 5.1440863609313965
		 200.34 5.1289243698120117 201.376 5.1191682815551758 202.412 5.0741128921508789 203.444 5.0455379486083984
		 204.48 5.0048375129699707 205.512 4.9566183090209961 206.548 4.9037232398986816 207.584 4.8473916053771973
		 208.616 4.7888684272766113 209.652 4.7294669151306152 210.688 4.6704425811767578
		 211.72 4.6129746437072754 212.756 4.5581178665161133 213.788 4.506676197052002 214.824 4.4591870307922363
		 215.86 4.4180731773376465 216.892 4.3876047134399414 217.928 4.3315773010253906 218.964 4.3149514198303223
		 219.996 4.2658100128173828 221.032 4.2415452003479004 222.064 4.2096347808837891
		 223.1 4.172203540802002 224.136 4.1294903755187988 225.168 4.0799455642700195 226.204 4.0222749710083008
		 227.236 3.9554302692413335 228.272 3.8787951469421387 229.308 3.7921981811523442
		 230.34 3.6959364414215092 231.376 3.5907092094421387 232.412 3.477672815322876 233.444 3.3582818508148193
		 234.48 3.234290599822998 235.512 3.1076641082763672 236.548 2.9805762767791748 237.584 2.8552286624908447
		 238.616 2.7339107990264893 239.652 2.618812084197998 240.688 2.5120794773101807 241.72 2.4157783985137939
		 242.756 2.3317978382110596 243.788 2.2619102001190186 244.824 2.2075850963592529
		 245.86 2.1699883937835693 246.892 2.1500012874603271 247.928 2.1481378078460693 248.96 2.1645524501800537
		 249.996 2.1988551616668701 251.032 2.2501559257507324 252.064 2.3170187473297119
		 253.1 2.397540807723999 254.136 2.4893364906311035 255.168 2.589597225189209 256.204 2.6952297687530518
		 257.236 2.80289626121521 258.272 2.9092564582824707;
	setAttr ".ktv[250:290]" 259.308 3.0110208988189697 260.34 3.1051571369171143
		 261.376 3.1890051364898682 262.412 3.2604506015777588 263.444 3.3179998397827148
		 264.48 3.3608448505401611 265.512 3.3889145851135254 266.548 3.4028551578521729 267.584 3.4040639400482178
		 268.616 3.394564151763916 269.652 3.3768680095672607 270.688 3.355440616607666 271.72 3.3379716873168945
		 272.756 3.3023924827575684 273.788 3.2740778923034668 274.824 3.2681961059570312
		 275.86 3.2574787139892578 276.892 3.2592518329620361 277.928 3.2811229228973389 278.96 3.2942707538604736
		 279.996 3.3547837734222412 281.032 3.3935196399688721 282.064 3.4486687183380127
		 283.1 3.5137836933135986 284.136 3.5844945907592769 285.168 3.6586892604827885 286.204 3.7342751026153564
		 287.236 3.8093059062957764 288.272 3.8820717334747314 289.308 3.9512219429016109
		 290.34 4.015749454498291 291.376 4.075047492980957 292.412 4.128908634185791 293.444 4.1774492263793945
		 294.48 4.2209343910217285 295.512 4.2597794532775879 296.548 4.2944107055664062 297.584 4.3251590728759766
		 298.616 4.3520584106445313 299.652 4.374779224395752 300.688 4.3926472663879395;
createNode animCurveTA -n "Bip01_R_Clavicle_rotateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 291 ".ktv";
	setAttr ".ktv[0:249]"  0.688 0.8142433762550354 1.72 0.8132253885269165 2.756 0.81107831001281738
		 3.788 0.81595522165298462 4.824 0.82448601722717285 5.86 0.83532720804214478 6.892 0.84801226854324341
		 7.928 0.85963070392608643 8.964 0.88395804166793823 9.996 0.9088207483291626 11.032 0.9363340139389037
		 12.064 0.96702963113784801 13.1 1.0004869699478149 14.136 1.0361655950546265 15.168 1.0733580589294434
		 16.204 1.1114755868911743 17.24 1.1497780084609985 18.272 1.1876122951507568 19.308 1.2244936227798462
		 20.34 1.2600371837615967 21.376 1.2939969301223755 22.412 1.3262487649917603 23.444 1.3567831516265869
		 24.48 1.3857674598693848 25.512 1.4132932424545288 26.548 1.4396806955337524 27.584 1.4651389122009277
		 28.616 1.4902199506759644 29.652 1.5159574747085571 30.688 1.5340672731399536 31.72 1.5584465265274048
		 32.756 1.5770752429962158 33.788 1.5945439338684082 34.824 1.6099176406860352 35.86 1.6189732551574707
		 36.892 1.6263840198516846 37.928 1.631290078163147 38.964 1.6289293766021729 39.996 1.6047258377075195
		 41.032 1.6065994501113892 42.064 1.5609227418899536 43.1 1.5499814748764038 44.136 1.4802387952804565
		 45.168 1.4471884965896606 46.204 1.3979918956756592 47.24 1.3394927978515625 48.272 1.2756901979446411
		 49.308 1.2070441246032715 50.34 1.1342358589172363 51.376 1.0575733184814453 52.412 0.97763621807098389
		 53.444 0.89449191093444824 54.48 0.80778026580810547 55.512 0.71597123146057129 56.548 0.62666845321655273
		 57.584 0.53140097856521606 58.616 0.43224218487739563 59.652 0.32642316818237305
		 60.688 0.21795432269573212 61.72 0.10326924175024033 62.756 -0.014726629480719568
		 63.788 -0.14104489982128143 64.824 -0.27319073677062988 65.86 -0.41246822476387024
		 66.892 -0.55086219310760498 67.928 -0.70234191417694092 68.964 -0.84831267595291138
		 69.996 -1.0036805868148804 71.032 -1.1580660343170166 72.064 -1.3104536533355713
		 73.1 -1.4591500759124756 74.136 -1.6014937162399292 75.168 -1.7349562644958496 76.204 -1.8571487665176392
		 77.24 -1.9607928991317747 78.272 -2.0504839420318604 79.308 -2.1195709705352783 80.34 -2.1665470600128174
		 81.376 -2.1903209686279297 82.412 -2.1902995109558105 83.444 -2.1663858890533447
		 84.48 -2.1199109554290771 85.512 -2.0533416271209717 86.548 -1.9568939208984373 87.584 -1.8518742322921755
		 88.616 -1.7302004098892212 89.652 -1.5980525016784668 90.688 -1.4415078163146973
		 91.72 -1.291350245475769 92.756 -1.1336899995803833 93.788 -0.97544646263122559 94.824 -0.80434119701385498
		 95.86 -0.63414603471755981 96.892 -0.47558188438415527 97.928 -0.30440965294837952
		 98.964 -0.14336460828781128 99.996 0.017515541985630989 101.032 0.18187375366687775
		 102.064 0.33975884318351746 103.1 0.49623814225196838 104.136 0.64893722534179688
		 105.168 0.79807049036026001 106.204 0.94186735153198242 107.24 1.0798927545547485
		 108.272 1.2032244205474854 109.308 1.3222193717956543 110.34 1.4276745319366455 111.376 1.519375205039978
		 112.412 1.5982178449630737 113.444 1.6524708271026611 114.48 1.6904633045196533 115.512 1.7090024948120117
		 116.548 1.7065094709396362 117.584 1.6824710369110107 118.616 1.6369645595550537
		 119.652 1.5694458484649658 120.688 1.4790781736373901 121.72 1.383729100227356 122.756 1.258184552192688
		 123.788 1.1199263334274292 124.824 0.98482978343963612 125.86 0.82968789339065552
		 126.892 0.68530523777008057 127.928 0.53417211771011353 128.964 0.39440444111824036
		 129.996 0.2764744758605957 131.032 0.16883330047130585 132.064 0.082963034510612488
		 133.1 0.016468340530991554 134.136 -0.026103321462869644 135.168 -0.043946456164121628
		 136.204 -0.039597824215888977 137.236 -0.012968766503036022 138.272 0.033644087612628937
		 139.308 0.097468413412570953 140.34 0.17704622447490692 141.376 0.26720461249351501
		 142.412 0.36649173498153687 143.444 0.47002676129341131 144.48 0.57833242416381836
		 145.512 0.68772083520889282 146.548 0.79194939136505127 147.584 0.89666247367858887
		 148.616 0.99626028537750233 149.652 1.0909178256988525 150.688 1.1787810325622559
		 151.72 1.2569427490234375 152.756 1.3245416879653931 153.788 1.3784859180450439 154.824 1.4238972663879395
		 155.86 1.4570960998535156 156.892 1.4773973226547241 157.928 1.4835501909255981 158.964 1.4745291471481323
		 159.996 1.4494324922561646 161.032 1.4073351621627808 162.064 1.3520005941390991
		 163.1 1.2762136459350586 164.136 1.1896926164627075 165.168 1.0844488143920898 166.204 0.96616864204406738
		 167.236 0.84094828367233276 168.272 0.70271438360214233 169.308 0.5589221715927124
		 170.34 0.41193538904190063 171.376 0.26460516452789307 172.412 0.12028186768293382
		 173.444 -0.017432836815714836 174.48 -0.14596711099147797 175.512 -0.26091834902763367
		 176.548 -0.3600594699382782 177.584 -0.44067841768264771 178.616 -0.50008553266525269
		 179.652 -0.53627872467041016 180.688 -0.55294889211654663 181.72 -0.54183846712112427
		 182.756 -0.50799155235290527 183.788 -0.45169559121131891 184.824 -0.37409111857414246
		 185.86 -0.27715408802032471 186.892 -0.16315235197544098 187.928 -0.036543969064950943
		 188.964 0.10239040851593018 189.996 0.24897244572639463 191.032 0.40123561024665833
		 192.064 0.55496150255203247 193.1 0.70830094814300537 194.136 0.85851216316223145
		 195.168 1.0070927143096924 196.204 1.1465917825698853 197.236 1.2787182331085205
		 198.272 1.4063141345977783 199.308 1.5217430591583252 200.34 1.6287491321563721 201.376 1.7266592979431152
		 202.412 1.8149192333221438 203.444 1.8933955430984497 204.48 1.9645569324493408 205.512 2.0243418216705322
		 206.548 2.075178861618042 207.584 2.1153857707977295 208.616 2.1410801410675049 209.652 2.1740906238555908
		 210.688 2.1873929500579834 211.72 2.1844813823699951 212.756 2.1724112033843994 213.788 2.1507706642150879
		 214.824 2.1170480251312256 215.86 2.068350076675415 216.892 2.0096158981323242 217.928 1.9344466924667361
		 218.964 1.8503344058990479 219.996 1.7502621412277222 221.032 1.6383213996887207
		 222.064 1.5198125839233398 223.1 1.3867374658584595 224.136 1.2502584457397461 225.168 1.1079248189926147
		 226.204 0.95725119113922119 227.236 0.80915248394012451 228.272 0.66100203990936279
		 229.308 0.51166456937789917 230.34 0.36629703640937805 231.376 0.22729061543941498
		 232.412 0.093089886009693146 233.444 -0.035079758614301682 234.48 -0.15681716799736023
		 235.512 -0.27243921160697937 236.548 -0.37750563025474548 237.584 -0.47922652959823603
		 238.616 -0.57324659824371338 239.652 -0.66032707691192627 240.688 -0.74112898111343384
		 241.72 -0.80942600965499878 242.756 -0.87448632717132568 243.788 -0.92946928739547718
		 244.824 -0.97407317161560059 245.86 -1.0101852416992187 246.892 -1.0304069519042969
		 247.928 -1.0441009998321533 248.96 -1.0459164381027222 249.996 -1.0370228290557861
		 251.032 -1.0176047086715698 252.064 -0.97402411699295044 253.1 -0.94362258911132801
		 254.136 -0.87977141141891479 255.168 -0.82995986938476563 256.204 -0.77187895774841309
		 257.236 -0.71073710918426514 258.272 -0.65026694536209106;
	setAttr ".ktv[250:290]" 259.308 -0.59280091524124146 260.34 -0.54155540466308594
		 261.376 -0.50059229135513306 262.412 -0.45305275917053217 263.444 -0.43411111831665039
		 264.48 -0.40981894731521606 265.512 -0.41116118431091309 266.548 -0.41086271405220032
		 267.584 -0.43370035290718079 268.616 -0.45860940217971802 269.652 -0.49524086713790894
		 270.688 -0.54090839624404907 271.72 -0.58925008773803711 272.756 -0.62980270385742188
		 273.788 -0.71670740842819214 274.824 -0.80755209922790527 275.86 -0.85305237770080566
		 276.892 -0.91104781627655029 277.928 -0.9576396346092223 278.96 -1.0382356643676758
		 279.996 -1.0943683385848999 281.032 -1.1457246541976929 282.064 -1.1859771013259888
		 283.1 -1.2271032333374023 284.136 -1.2408807277679443 285.168 -1.2652775049209595
		 286.204 -1.2729009389877319 287.236 -1.2739665508270264 288.272 -1.2649166584014893
		 289.308 -1.2389235496520996 290.34 -1.1922484636306763 291.376 -1.1640726327896118
		 292.412 -1.1251976490020752 293.444 -1.0090681314468384 294.48 -0.94385194778442383
		 295.512 -0.85090529918670654 296.548 -0.74044835567474365 297.584 -0.61843019723892212
		 298.616 -0.48663803935050964 299.652 -0.34752163290977478 300.688 -0.20411822199821472;
createNode animCurveTA -n "Bip01_R_Clavicle_rotateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 291 ".ktv";
	setAttr ".ktv[0:249]"  0.688 78.090843200683594 1.72 78.071723937988281 2.756 78.067893981933594
		 3.788 78.060798645019531 4.824 78.056961059570313 5.86 78.058662414550781 6.892 78.066810607910156
		 7.928 78.088020324707031 8.964 78.092666625976563 9.996 78.107063293457031 11.032 78.126136779785156
		 12.064 78.14813232421875 13.1 78.172821044921875 14.136 78.200057983398437 15.168 78.229751586914063
		 16.204 78.26153564453125 17.24 78.295112609863281 18.272 78.330009460449219 19.308 78.3656005859375
		 20.34 78.401107788085937 21.376 78.435775756835938 22.412 78.468788146972656 23.444 78.499221801757813
		 24.48 78.52630615234375 25.512 78.549369812011719 26.548 78.567695617675781 27.584 78.581031799316406
		 28.616 78.587638854980469 29.652 78.584877014160156 30.688 78.596275329589844 31.72 78.581611633300781
		 32.756 78.574783325195313 33.788 78.561569213867188 34.824 78.543693542480469 35.86 78.531990051269531
		 36.892 78.511520385742188 37.928 78.487007141113281 38.964 78.4761962890625 39.996 78.477706909179688
		 41.032 78.441482543945313 42.064 78.453720092773438 43.1 78.437339782714844 44.136 78.458244323730469
		 45.168 78.453239440917969 46.204 78.45855712890625 47.24 78.469619750976563 48.272 78.484222412109375
		 49.308 78.502296447753906 50.34 78.523750305175781 51.376 78.548675537109375 52.412 78.577079772949219
		 53.444 78.609107971191406 54.48 78.646095275878906 55.512 78.691421508789063 56.548 78.727935791015625
		 57.584 78.774162292480469 58.616 78.825103759765625 59.652 78.885452270507813 60.688 78.945449829101562
		 61.72 79.011825561523437 62.756 79.077835083007812 63.788 79.152320861816406 64.824 79.229927062988281
		 65.86 79.312538146972656 66.892 79.385894775390625 67.928 79.475479125976563 68.964 79.549636840820313
		 69.996 79.635322570800781 71.032 79.716468811035156 72.064 79.793907165527344 73.1 79.867393493652344
		 74.136 79.935874938964844 75.168 79.998649597167969 76.204 80.055473327636719 77.24 80.099319458007812
		 78.272 80.139846801757812 79.308 80.170356750488281 80.34 80.19134521484375 81.376 80.203155517578125
		 82.412 80.206001281738281 83.444 80.20037841796875 84.48 80.187744140625 85.512 80.170677185058594
		 86.548 80.133636474609375 87.584 80.104400634765625 88.616 80.069068908691406 89.652 80.033432006835938
		 90.688 79.973014831542969 91.72 79.931503295898438 92.756 79.88494873046875 93.788 79.841529846191406
		 94.824 79.777984619140625 95.86 79.715072631835938 96.892 79.671211242675781 97.928 79.600540161132812
		 98.964 79.544822692871094 99.996 79.48455810546875 101.032 79.410865783691406 102.064 79.34344482421875
		 103.1 79.271041870117187 104.136 79.197227478027344 105.168 79.119918823242188 106.204 79.040824890136719
		 107.24 78.958297729492188 108.272 78.892349243164062 109.308 78.812820434570313 110.34 78.740058898925781
		 111.376 78.670967102050781 112.412 78.599334716796875 113.444 78.554855346679688
		 114.48 78.51104736328125 115.512 78.474357604980469 116.548 78.447364807128906 117.584 78.430580139160156
		 118.616 78.424209594726563 119.652 78.430671691894531 120.688 78.454673767089844
		 121.72 78.453056335449219 122.756 78.497055053710937 123.788 78.547073364257813 124.824 78.572898864746094
		 125.86 78.63714599609375 126.892 78.675392150878906 127.928 78.736122131347656 128.964 78.79205322265625
		 129.996 78.830345153808594 131.032 78.8765869140625 132.064 78.909652709960938 133.1 78.939247131347656
		 134.136 78.958976745605469 135.168 78.967422485351563 136.204 78.968132019042969
		 137.236 78.958267211914063 138.272 78.93853759765625 139.308 78.909774780273438 140.34 78.87030029296875
		 141.376 78.82489013671875 142.412 78.771751403808594 143.444 78.716117858886719 144.48 78.65264892578125
		 145.512 78.585205078125 146.548 78.524559020996094 147.584 78.455451965332031 148.616 78.388816833496094
		 149.652 78.324066162109375 150.688 78.262451171875 151.72 78.206062316894531 152.756 78.155448913574219
		 153.788 78.118843078613281 154.824 78.08367919921875 155.86 78.058456420898437 156.892 78.04315185546875
		 157.928 78.038406372070313 158.964 78.044914245605469 159.996 78.063522338867188
		 161.032 78.095542907714844 162.064 78.130958557128906 163.1 78.187728881835938 164.136 78.242279052734375
		 165.168 78.31549072265625 166.204 78.395256042480469 167.236 78.47039794921875 168.272 78.558090209960937
		 169.308 78.645210266113281 170.34 78.731117248535156 171.376 78.814598083496094 172.412 78.893775939941406
		 173.444 78.966552734375 174.48 79.032974243164063 175.512 79.089378356933594 176.548 79.136238098144531
		 177.584 79.172325134277344 178.616 79.196426391601563 179.652 79.207328796386719
		 180.688 79.213188171386719 181.72 79.200340270996094 182.756 79.177528381347656 183.788 79.144271850585938
		 184.824 79.100929260253906 185.86 79.048240661621094 186.892 78.987007141113281 187.928 78.921707153320312
		 188.964 78.848587036132813 189.996 78.772430419921875 191.032 78.692459106445312
		 192.064 78.612937927246094 193.1 78.53369140625 194.136 78.457061767578125 195.168 78.376289367675781
		 196.204 78.305763244628906 197.236 78.239410400390625 198.272 78.168411254882813
		 199.308 78.110435485839844 200.34 78.055404663085938 201.376 78.0045166015625 202.412 77.958908081054687
		 203.444 77.919242858886719 204.48 77.878219604492187 205.512 77.847648620605469 206.548 77.820274353027344
		 207.584 77.79791259765625 208.616 77.783233642578125 209.652 77.762702941894531 210.688 77.75286865234375
		 211.72 77.7510986328125 212.756 77.757080078125 213.788 77.764381408691406 214.824 77.777481079101563
		 215.86 77.804153442382813 216.892 77.828857421875 217.928 77.870254516601563 218.964 77.9073486328125
		 219.996 77.960762023925781 221.032 78.020515441894531 222.064 78.075187683105469
		 223.1 78.148223876953125 224.136 78.214286804199219 225.168 78.28350830078125 226.204 78.364974975585937
		 227.236 78.437126159667969 228.272 78.509689331054688 229.308 78.587814331054688
		 230.34 78.663192749023438 231.376 78.733253479003906 232.412 78.80291748046875 233.444 78.870895385742187
		 234.48 78.937103271484375 235.512 79.002418518066406 236.548 79.058769226074219 237.584 79.119033813476562
		 238.616 79.175003051757812 239.652 79.22772216796875 240.688 79.278182983398438 241.72 79.315559387207031
		 242.756 79.355545043945313 243.788 79.386962890625 244.824 79.409400939941406 245.86 79.426254272460938
		 246.892 79.427177429199219 247.928 79.429313659667969 248.96 79.408622741699219 249.996 79.39190673828125
		 251.032 79.368316650390625 252.064 79.310012817382813 253.1 79.281501770019531 254.136 79.205886840820313
		 255.168 79.158638000488281 256.204 79.101409912109375 257.236 79.040847778320312
		 258.272 78.981246948242188;
	setAttr ".ktv[250:290]" 259.308 78.924095153808594 260.34 78.872856140136719
		 261.376 78.832557678222656 262.412 78.772659301757812 263.444 78.751884460449219
		 264.48 78.711097717285156 265.512 78.702796936035156 266.548 78.679946899414062 267.584 78.685226440429687
		 268.616 78.683113098144531 269.652 78.691207885742187 270.688 78.706344604492188
		 271.72 78.723793029785156 272.756 78.74261474609375 273.788 78.782798767089844 274.824 78.827072143554688
		 275.86 78.8505859375 276.892 78.8814697265625 277.928 78.908088684082031 278.96 78.952857971191406
		 279.996 78.982109069824219 281.032 79.014778137207031 282.064 79.036201477050781
		 283.1 79.062408447265625 284.136 79.074783325195313 285.168 79.092155456542969 286.204 79.097221374511719
		 287.236 79.103172302246094 288.272 79.103927612304688 289.308 79.092941284179688
		 290.34 79.075721740722656 291.376 79.067520141601562 292.412 79.050567626953125 293.444 79.000595092773438
		 294.48 78.974418640136719 295.512 78.934738159179688 296.548 78.886558532714844 297.584 78.832321166992188
		 298.616 78.772605895996094 299.652 78.708282470703125 300.688 78.640510559082031;
createNode animCurveTA -n "Bip01_R_Clavicle_rotateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 291 ".ktv";
	setAttr ".ktv[0:249]"  0.688 174.20796203613281 1.72 174.29965209960937 2.756 174.39779663085937
		 3.788 174.50088500976562 4.824 174.60710144042969 5.86 174.71530151367188 6.892 174.82514953613281
		 7.928 174.93675231933594 8.964 175.05172729492187 9.996 175.17024230957031 11.032 175.29344177246094
		 12.064 175.4224853515625 13.1 175.55830383300781 14.136 175.7017822265625 15.168 175.85345458984375
		 16.204 176.01374816894531 17.24 176.18260192871094 18.272 176.35969543457031 19.308 176.54443359375
		 20.34 176.73591613769531 21.376 176.932861328125 22.412 177.13374328613281 23.444 177.33683776855469
		 24.48 177.54031372070312 25.512 177.74215698242187 26.548 177.94053649902344 27.584 178.13352966308594
		 28.616 178.31906127929687 29.652 178.49505615234375 30.688 178.66162109375 31.72 178.81369018554687
		 32.756 178.95228576660156 33.788 179.07461547851563 34.824 179.17938232421875 35.86 179.26605224609375
		 36.892 179.33222961425781 37.928 179.37733459472656 38.964 179.40144348144531 39.996 179.41157531738281
		 41.032 179.40028381347656 42.064 179.34408569335937 43.1 179.31369018554688 44.136 179.19050598144531
		 45.168 179.11326599121094 46.204 179.00454711914062 47.24 178.874755859375 48.272 178.72982788085937
		 49.308 178.570556640625 50.34 178.397705078125 51.376 178.2115478515625 52.412 178.01251220703125
		 53.444 177.80061340332031 54.48 177.575927734375 55.512 177.33853149414062 56.548 177.08920288085937
		 57.584 176.82833862304687 58.616 176.55723571777344 59.652 176.2769775390625 60.688 175.98956298828125
		 61.72 175.69677734375 62.756 175.40126037597656 63.788 175.10441589355469 64.824 174.80891418457031
		 65.86 174.51629638671875 66.892 174.23008728027344 67.928 173.94825744628906 68.964 173.67628479003906
		 69.996 173.41053771972656 71.032 173.15383911132812 72.064 172.90643310546875 73.1 172.66860961914062
		 74.136 172.441162109375 75.168 172.2249755859375 76.204 172.02116394042969 77.24 171.8330078125
		 78.272 171.65937805175781 79.308 171.50372314453125 80.34 171.36766052246094 81.376 171.25257873535156
		 82.412 171.15940856933594 83.444 171.08872985839844 84.48 171.04013061523437 85.512 171.01248168945313
		 86.548 171.00901794433594 87.584 171.02020263671875 88.616 171.04794311523437 89.652 171.08824157714844
		 90.688 171.14419555664062 91.72 171.20332336425781 92.756 171.26899719238281 93.788 171.33757019042969
		 94.824 171.41117858886719 95.86 171.4847412109375 96.892 171.55519104003906 97.928 171.62710571289062
		 98.964 171.6953125 99.996 171.76116943359375 101.032 171.82415771484375 102.064 171.88215637207031
		 103.1 171.93472290039062 104.136 171.98028564453125 105.168 172.01744079589844 106.204 172.04432678222656
		 107.24 172.05915832519531 108.272 172.06103515625 109.308 172.04698181152344 110.34 172.01628112792969
		 111.376 171.96751403808594 112.412 171.89926147460937 113.444 171.81336975097656
		 114.48 171.70794677734375 115.512 171.58421325683594 116.548 171.44380187988281 117.584 171.28900146484375
		 118.616 171.12294006347656 119.652 170.94944763183594 120.688 170.773193359375 121.72 170.59713745117187
		 122.756 170.43032836914062 123.788 170.27650451660156 124.824 170.14140319824219
		 125.86 170.02937316894531 126.892 169.941162109375 127.928 169.8829345703125 128.964 169.87142944335937
		 129.996 169.91175842285156 131.032 169.98971557617188 132.064 170.0994873046875 133.1 170.24259948730469
		 134.136 170.41888427734375 135.168 170.62419128417969 136.204 170.85348510742187
		 137.236 171.10189819335938 138.272 171.36433410644531 139.308 171.63597106933594
		 140.34 171.91291809082031 141.376 172.19097900390625 142.412 172.46742248535156 143.444 172.73959350585937
		 144.48 173.00607299804687 145.512 173.26535034179687 146.548 173.516357421875 147.584 173.75875854492187
		 148.616 173.994873046875 149.652 174.22988891601562 150.688 174.45712280273437 151.72 174.66165161132812
		 152.756 174.84130859375 153.788 175.00334167480469 154.824 175.15133666992187 155.86 175.28678894042969
		 156.892 175.40750122070312 157.928 175.50886535644531 158.964 175.588134765625 159.996 175.643798828125
		 161.032 175.67462158203125 162.064 175.67942810058594 163.1 175.65840148925781 164.136 175.61106872558594
		 165.168 175.53868103027344 166.204 175.44232177734375 167.236 175.32420349121094
		 168.272 175.18681335449219 169.308 175.033447265625 170.34 174.8677978515625 171.376 174.69393920898437
		 172.412 174.51612854003906 173.444 174.3388671875 174.48 174.16630554199219 175.512 174.002685546875
		 176.548 173.85166931152344 177.584 173.71652221679687 178.616 173.60026550292969
		 179.652 173.505126953125 180.688 173.43147277832031 181.72 173.38224792480469 182.756 173.35662841796875
		 183.788 173.35455322265625 184.824 173.37528991699219 185.86 173.41743469238281 186.892 173.47923278808594
		 187.928 173.55801391601562 188.964 173.6517333984375 189.996 173.75727844238281 191.032 173.87178039550781
		 192.064 173.99200439453125 193.1 174.11512756347656 194.136 174.2384033203125 195.168 174.35934448242187
		 196.204 174.47563171386719 197.236 174.58552551269531 198.272 174.68742370605469
		 199.308 174.78085327148437 200.34 174.86500549316406 201.376 174.93983459472656 202.412 175.00550842285156
		 203.444 175.06271362304687 204.48 175.11126708984375 205.512 175.15287780761719 206.548 175.18739318847656
		 207.584 175.21437072753906 208.616 175.2315673828125 209.652 175.254150390625 210.688 175.26449584960937
		 211.72 175.26556396484375 212.756 175.26026916503906 213.788 175.24858093261719 214.824 175.22953796386719
		 215.86 175.2034912109375 216.892 175.16891479492187 217.928 175.1278076171875 218.964 175.07899475097656
		 219.996 175.02490234375 221.032 174.9658203125 222.064 174.90267944335938 223.1 174.83802795410156
		 224.136 174.7724609375 225.168 174.70809936523438 226.204 174.64649963378906 227.236 174.58927917480469
		 228.272 174.53781127929687 229.308 174.49288940429687 230.34 174.45571899414062 231.376 174.42704772949219
		 232.412 174.40702819824219 233.444 174.39573669433594 234.48 174.39306640625 235.512 174.39851379394531
		 236.548 174.41246032714844 237.584 174.43258666992187 238.616 174.45901489257812
		 239.652 174.4906005859375 240.688 174.52601623535156 241.72 174.56562805175781 242.756 174.60531616210937
		 243.788 174.64546203613281 244.824 174.684326171875 245.86 174.71940612792969 246.892 174.75025939941406
		 247.928 174.7698974609375 248.96 174.79737854003906 249.996 174.80194091796875 251.032 174.78955078125
		 252.064 174.7799072265625 253.1 174.74873352050781 254.136 174.71195983886719 255.168 174.65628051757812
		 256.204 174.58950805664062 257.236 174.51156616210937 258.272 174.42335510253906;
	setAttr ".ktv[250:290]" 259.308 174.32635498046875 260.34 174.22206115722656
		 261.376 174.11189270019531 262.412 174.00279235839844 263.444 173.88760375976562
		 264.48 173.7774658203125 265.512 173.66586303710937 266.548 173.56134033203125 267.584 173.458251953125
		 268.616 173.36270141601562 269.652 173.27223205566406 270.688 173.18727111816406
		 271.72 173.11209106445312 272.756 173.05836486816406 273.788 172.9600830078125 274.824 172.86869812011719
		 275.86 172.82583618164062 276.892 172.77194213867187 277.928 172.72975158691406 278.96 172.65412902832031
		 279.996 172.59730529785156 281.032 172.545654296875 282.064 172.49777221679687 283.1 172.44696044921875
		 284.136 172.43218994140625 285.168 172.39497375488281 286.204 172.37416076660156
		 287.236 172.36396789550781 288.272 172.36494445800781 289.308 172.38172912597656
		 290.34 172.42127990722656 291.376 172.44859313964844 292.412 172.48408508300781 293.444 172.59403991699219
		 294.48 172.65751647949219 295.512 172.74681091308594 296.548 172.85212707519531 297.584 172.96766662597656
		 298.616 173.091552734375 299.652 173.22154235839844 300.688 173.35499572753906;
createNode animCurveTA -n "Bip01_R_UpperArm_rotateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 291 ".ktv";
	setAttr ".ktv[0:249]"  0.688 -22.107637405395508 1.72 -20.476303100585938
		 2.756 -18.546686172485352 3.788 -16.906341552734375 4.824 -16.03260612487793 5.86 -15.64133930206299
		 6.892 -15.049827575683592 7.928 -14.237725257873535 8.964 -13.657536506652832 9.996 -13.57465934753418
		 11.032 -13.927908897399902 12.064 -14.25795841217041 13.1 -14.085019111633301 14.136 -13.343744277954102
		 15.168 -12.166996955871582 16.204 -10.504904747009277 17.24 -7.6121764183044442 18.272 -2.386244535446167
		 19.308 4.5067224502563477 20.34 10.687604904174805 21.376 15.026390075683592 22.412 17.964654922485352
		 23.444 19.878982543945313 24.48 20.926549911499023 25.512 21.645925521850586 26.548 22.474349975585938
		 27.584 23.420475006103516 28.616 24.335931777954102 29.652 25.031435012817383 30.688 25.488195419311523
		 31.72 25.906003952026367 32.756 26.374496459960937 33.788 26.762012481689453 34.824 26.961978912353516
		 35.86 26.993148803710937 36.892 26.830135345458984 37.928 26.432767868041992 38.964 25.804309844970703
		 39.996 24.883613586425781 41.032 23.751699447631836 42.064 22.516073226928711 43.1 21.109951019287109
		 44.136 19.701717376708984 45.168 18.583158493041992 46.204 17.72932243347168 47.24 16.776674270629883
		 48.272 15.344601631164551 49.308 13.549571990966797 50.34 11.879402160644531 51.376 10.586647033691406
		 52.412 9.5934257507324219 53.444 8.6824808120727539 54.48 7.690314292907714 55.512 6.6486921310424805
		 56.548 5.7299389839172363 57.584 5.0586328506469727 58.616 4.4976487159729004 59.652 3.9299945831298828
		 60.688 3.4699399471282959 61.72 3.0735085010528564 62.756 2.6391215324401855 63.788 2.2550079822540283
		 64.824 1.9776201248168943 65.86 1.826748728752136 66.892 1.9013679027557373 67.928 2.2379024028778076
		 68.964 2.7274086475372314 69.996 3.2093799114227295 71.032 3.5865352153778081 72.064 3.9300470352172852
		 73.1 4.4108791351318359 74.136 5.0684051513671875 75.168 5.7496790885925293 76.204 6.2119488716125488
		 77.24 6.4104266166687012 78.272 6.6040010452270508 79.308 6.8791060447692871 80.34 7.0750341415405273
		 81.376 7.2462687492370597 82.412 7.5103340148925781 83.444 7.7347722053527823 84.48 7.8430280685424805
		 85.512 8.0727338790893555 86.548 8.6563787460327148 87.584 9.5282230377197266 88.616 10.438796997070312
		 89.652 11.192601203918457 90.688 11.830686569213867 91.72 12.42298412322998 92.756 12.852645874023437
		 93.788 13.103994369506836 94.824 13.311899185180664 95.86 13.531407356262207 96.892 13.755414962768555
		 97.928 13.976649284362793 98.964 14.178025245666504 99.996 14.337583541870117 101.032 14.517333984375
		 102.064 14.855616569519043 103.1 15.309675216674806 104.136 15.566032409667971 105.168 15.488227844238283
		 106.204 15.373363494873047 107.24 15.348018646240236 108.272 15.133542060852049 109.308 14.646471977233887
		 110.34 14.097250938415527 111.376 13.721198081970215 112.412 13.608521461486816 113.444 13.611871719360352
		 114.48 13.56804084777832 115.512 13.378070831298828 116.548 13.146489143371582 117.584 13.121234893798828
		 118.616 13.043378829956055 119.652 12.556608200073242 120.688 11.939455986022949
		 121.72 11.508919715881348 122.756 11.155936241149902 123.788 10.694520950317383 124.824 9.9618387222290039
		 125.86 8.9100675582885742 126.892 7.8992757797241211 127.928 7.3142075538635263 128.964 7.2433228492736825
		 129.996 7.6393399238586426 131.032 8.0607089996337891 132.064 8.0999155044555664
		 133.1 7.9519524574279794 134.136 7.870140552520752 135.168 7.7805500030517587 136.204 7.4983501434326172
		 137.236 7.0868897438049316 138.272 6.7556843757629395 139.308 6.3273196220397949
		 140.34 5.6360607147216797 141.376 5.1458396911621094 142.412 5.2755799293518066 143.444 5.6069116592407227
		 144.48 5.3767290115356445 145.512 4.4236311912536621 146.548 3.2386724948883057 147.584 2.2295384407043457
		 148.616 1.5417517423629761 149.652 1.2453731298446655 150.688 1.073100209236145 151.72 0.60048198699951172
		 152.756 -0.16245445609092712 153.788 -0.80592304468154907 154.824 -1.1425628662109375
		 155.86 -1.4114367961883545 156.892 -1.7368429899215698 157.928 -2.0177531242370605
		 158.964 -2.2263469696044922 159.996 -2.3130269050598145 161.032 -2.2594895362854004
		 162.064 -2.1669445037841797 163.1 -2.0903706550598145 164.136 -2.0487434864044189
		 165.168 -2.035487174987793 166.204 -1.9825032949447634 167.236 -1.8806551694869993
		 168.272 -1.8161354064941406 169.308 -1.8829524517059324 170.34 -2.062453031539917
		 171.376 -2.1792781352996826 172.412 -2.1975140571594238 173.444 -2.2706634998321533
		 174.48 -2.310051441192627 175.512 -2.098780632019043 176.548 -1.7806417942047119
		 177.584 -1.6154323816299438 178.616 -1.4714195728302002 179.652 -1.2117640972137451
		 180.688 -1.0506020784378052 181.72 -0.94117033481597889 182.756 -0.5977821946144104
		 183.788 -0.13808803260326385 184.824 0.24247448146343231 185.86 0.6762462854385376
		 186.892 1.2094359397888184 187.928 1.7491343021392822 188.964 2.2762067317962646
		 189.996 2.6068549156188965 191.032 2.685234546661377 192.064 2.767472505569458 193.1 2.8201208114624023
		 194.136 2.6957273483276367 195.168 2.6727235317230225 196.204 2.9574120044708252
		 197.236 3.2767884731292725 198.272 3.4011130332946777 199.308 3.3365242481231689
		 200.34 3.1239597797393799 201.376 2.8967304229736328 202.412 2.7467703819274902 203.444 2.6538138389587402
		 204.48 2.6118297576904297 205.512 2.5112314224243164 206.548 2.2976911067962646 207.584 2.1585359573364258
		 208.616 2.1497762203216553 209.652 2.115189790725708 210.688 2.0111536979675293 211.72 1.9334695339202881
		 212.756 1.9016107320785522 213.788 1.8518067598342896 214.824 1.7022227048873901
		 215.86 1.4206799268722534 216.892 1.1594499349594116 217.928 1.0040819644927979 218.964 0.89098310470581055
		 219.996 0.70006108283996582 221.032 0.24674323201179504 222.064 -0.27895784378051758
		 223.1 -0.55681014060974121 224.136 -0.77283889055252075 225.168 -1.0988316535949707
		 226.204 -1.3237067461013794 227.236 -1.4177124500274658 228.272 -1.5976400375366211
		 229.308 -1.9271836280822756 230.34 -2.4118068218231201 231.376 -2.9181931018829346
		 232.412 -3.3033947944641113 233.444 -3.9195179939270015 234.48 -5.0331740379333496
		 235.512 -6.7072954177856445 236.548 -9.3246755599975586 237.584 -12.607707023620605
		 238.616 -15.512385368347168 239.652 -17.608917236328125 240.688 -19.077053070068359
		 241.72 -20.081640243530273 242.756 -20.759422302246094 243.788 -21.336528778076172
		 244.824 -21.989599227905273 245.86 -22.73652458190918 246.892 -23.74445915222168
		 247.928 -25.079004287719727 248.96 -25.951896667480469 249.996 -25.557706832885742
		 251.032 -24.890167236328125 252.064 -25.929914474487305 253.1 -28.723272323608398
		 254.136 -31.585121154785156 255.168 -33.450489044189453 256.204 -34.337760925292969
		 257.236 -34.754867553710937 258.272 -35.266387939453125;
	setAttr ".ktv[250:290]" 259.308 -36.073974609375 260.34 -36.884349822998047
		 261.376 -37.611087799072266 262.412 -38.78326416015625 263.444 -40.613471984863281
		 264.48 -42.004329681396484 265.512 -41.380821228027344 266.548 -38.433807373046875
		 267.584 -34.499824523925781 268.616 -31.23593902587891 269.652 -28.802656173706055
		 270.688 -26.435192108154297 271.72 -24.15583610534668 272.756 -22.203958511352539
		 273.788 -20.560386657714844 274.824 -19.547601699829102 275.86 -19.223342895507813
		 276.892 -19.094707489013672 277.928 -18.829444885253906 278.96 -18.339838027954102
		 279.996 -17.727130889892578 281.032 -17.224363327026367 282.064 -16.887386322021484
		 283.1 -16.701576232910156 284.136 -16.685077667236328 285.168 -16.674446105957031
		 286.204 -16.381595611572266 287.236 -15.757938385009766 288.272 -15.18510627746582
		 289.308 -15.002979278564455 290.34 -15.01845645904541 291.376 -14.852374076843263
		 292.412 -14.531156539916994 293.444 -14.311806678771973 294.48 -14.132024765014648
		 295.512 -13.795103073120117 296.548 -13.40798282623291 297.584 -13.310102462768555
		 298.616 -13.578546524047852 299.652 -13.854780197143555 300.688 -13.920482635498047;
createNode animCurveTA -n "Bip01_R_UpperArm_rotateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 291 ".ktv";
	setAttr ".ktv[0:249]"  0.688 -70.341537475585937 1.72 -70.522315979003906
		 2.756 -70.655685424804688 3.788 -70.755142211914063 4.824 -70.84124755859375 5.86 -70.910614013671875
		 6.892 -70.941963195800781 7.928 -70.942756652832031 8.964 -70.988822937011719 9.996 -71.119728088378906
		 11.032 -71.28851318359375 12.064 -71.461219787597656 13.1 -71.647048950195313 14.136 -71.845977783203125
		 15.168 -72.022918701171875 16.204 -72.122856140136719 17.24 -72.013153076171875 18.272 -71.36785888671875
		 19.308 -69.871757507324219 20.34 -67.782913208007812 21.376 -65.874603271484375 22.412 -64.720161437988281
		 23.444 -64.419158935546875 24.48 -64.678291320800781 25.512 -65.017280578613281 26.548 -65.20355224609375
		 27.584 -65.315254211425781 28.616 -65.437469482421875 29.652 -65.607383728027344
		 30.688 -65.802528381347656 31.72 -65.942695617675781 32.756 -66.013847351074219 33.788 -66.054084777832031
		 34.824 -66.079193115234375 35.86 -66.093505859375 36.892 -66.096885681152344 37.928 -66.058143615722656
		 38.964 -65.961540222167969 39.996 -65.833282470703125 41.032 -65.672218322753906
		 42.064 -65.480392456054688 43.1 -65.300689697265625 44.136 -65.171699523925781 45.168 -65.123306274414063
		 46.204 -65.1954345703125 47.24 -65.417839050292969 48.272 -65.768150329589844 49.308 -66.156768798828125
		 50.34 -66.472648620605469 51.376 -66.728271484375 52.412 -67.08465576171875 53.444 -67.634933471679688
		 54.48 -68.266929626464844 55.512 -68.804122924804688 56.548 -69.266075134277344 57.584 -69.748405456542969
		 58.616 -70.124534606933594 59.652 -70.232574462890625 60.688 -70.134788513183594
		 61.72 -69.977096557617187 62.756 -69.809440612792969 63.788 -69.647796630859375 64.824 -69.530746459960938
		 65.86 -69.443946838378906 66.892 -69.342742919921875 67.928 -69.235740661621094 68.964 -69.141464233398438
		 69.996 -69.065925598144531 71.032 -69.016754150390625 72.064 -68.965003967285156
		 73.1 -68.903923034667969 74.136 -68.894096374511719 75.168 -68.963211059570313 76.204 -69.072639465332031
		 77.24 -69.169303894042969 78.272 -69.194725036621094 79.308 -69.125885009765625 80.34 -69.030197143554687
		 81.376 -68.997634887695313 82.412 -69.024330139160156 83.444 -69.035118103027344
		 84.48 -68.997566223144531 85.512 -68.944564819335937 86.548 -68.933143615722656 87.584 -68.980232238769531
		 88.616 -69.015922546386719 89.652 -68.982933044433594 90.688 -68.901420593261719
		 91.72 -68.809783935546875 92.756 -68.738990783691406 93.788 -68.674949645996094 94.824 -68.580665588378906
		 95.86 -68.485488891601563 96.892 -68.432762145996094 97.928 -68.397491455078125 98.964 -68.341567993164062
		 99.996 -68.26995849609375 101.032 -68.219223022460938 102.064 -68.197341918945313
		 103.1 -68.153785705566406 104.136 -68.096504211425781 105.168 -68.096923828125 106.204 -68.139045715332031
		 107.24 -68.166732788085938 108.272 -68.198577880859375 109.308 -68.28973388671875
		 110.34 -68.441604614257813 111.376 -68.565864562988281 112.412 -68.594161987304688
		 113.444 -68.59197998046875 114.48 -68.656318664550781 115.512 -68.793220520019531
		 116.548 -68.925163269042969 117.584 -68.9947509765625 118.616 -69.094596862792969
		 119.652 -69.320892333984375 120.688 -69.585533142089844 121.72 -69.819839477539062
		 122.756 -70.073371887207031 123.788 -70.3134765625 124.824 -70.478668212890625 125.86 -70.6226806640625
		 126.892 -70.782112121582031 127.928 -70.940879821777344 128.964 -71.064422607421875
		 129.996 -71.072456359863281 131.032 -70.991592407226563 132.064 -70.96661376953125
		 133.1 -71.011985778808594 134.136 -71.016639709472656 135.168 -70.946556091308594
		 136.204 -70.865058898925781 137.236 -70.769851684570313 138.272 -70.62451171875 139.308 -70.519134521484375
		 140.34 -70.496559143066406 141.376 -70.410308837890625 142.412 -70.186355590820312
		 143.444 -69.968727111816406 144.48 -69.928756713867187 145.512 -70.052597045898438
		 146.548 -70.203956604003906 147.584 -70.342117309570313 148.616 -70.4708251953125
		 149.652 -70.538795471191406 150.688 -70.564552307128906 151.72 -70.614166259765625
		 152.756 -70.667640686035156 153.788 -70.689254760742188 154.824 -70.690994262695313
		 155.86 -70.672821044921875 156.892 -70.634323120117188 157.928 -70.593482971191406
		 158.964 -70.544303894042969 159.996 -70.482490539550781 161.032 -70.423042297363281
		 162.064 -70.358085632324219 163.1 -70.28643798828125 164.136 -70.226249694824219
		 165.168 -70.163589477539063 166.204 -70.087387084960938 167.236 -70.021644592285156
		 168.272 -69.960884094238281 169.308 -69.860527038574219 170.34 -69.7320556640625
		 171.376 -69.662178039550781 172.412 -69.692451477050781 173.444 -69.774909973144531
		 174.48 -69.848747253417969 175.512 -69.896308898925781 176.548 -69.94085693359375
		 177.584 -70.006927490234375 178.616 -70.097076416015625 179.652 -70.206039428710938
		 180.688 -70.326095581054687 181.72 -70.43804931640625 182.756 -70.504768371582031
		 183.788 -70.51763916015625 184.824 -70.541763305664063 185.86 -70.621826171875 186.892 -70.694068908691406
		 187.928 -70.663612365722656 188.964 -70.569252014160156 189.996 -70.563491821289063
		 191.032 -70.632720947265625 192.064 -70.61572265625 193.1 -70.52056884765625 194.136 -70.463058471679688
		 195.168 -70.440620422363281 196.204 -70.398300170898438 197.236 -70.328666687011719
		 198.272 -70.249000549316406 199.308 -70.178352355957031 200.34 -70.117050170898438
		 201.376 -70.038978576660156 202.412 -69.940597534179687 203.444 -69.863357543945313
		 204.48 -69.82470703125 205.512 -69.777442932128906 206.548 -69.694007873535156 207.584 -69.611122131347656
		 208.616 -69.555862426757812 209.652 -69.522010803222656 210.688 -69.51068115234375
		 211.72 -69.526878356933594 212.756 -69.551292419433594 213.788 -69.551277160644531
		 214.824 -69.536506652832031 215.86 -69.552734375 216.892 -69.603813171386719 217.928 -69.654678344726563
		 218.964 -69.694389343261719 219.996 -69.748695373535156 221.032 -69.829444885253906
		 222.064 -69.895469665527344 223.1 -69.928474426269531 224.136 -69.971237182617188
		 225.168 -70.037696838378906 226.204 -70.099563598632812 227.236 -70.139579772949219
		 228.272 -70.153274536132813 229.308 -70.13861083984375 230.34 -70.112533569335937
		 231.376 -70.11383056640625 232.412 -70.119522094726563 233.444 -70.032371520996094
		 234.48 -69.851524353027344 235.512 -69.669342041015625 236.548 -69.468673706054688
		 237.584 -69.171798706054688 238.616 -68.826080322265625 239.652 -68.48626708984375
		 240.688 -68.103401184082031 241.72 -67.663597106933594 242.756 -67.207618713378906
		 243.788 -66.723373413085937 244.824 -66.219413757324219 245.86 -65.732276916503906
		 246.892 -65.054054260253906 247.928 -64.014167785644531 248.96 -63.284900665283203
		 249.996 -63.8583984375 251.032 -65.549079895019531 252.064 -67.175056457519531 253.1 -68.098228454589844
		 254.136 -68.500076293945313 255.168 -68.639823913574219 256.204 -68.677360534667969
		 257.236 -68.713958740234375 258.272 -68.751541137695312;
	setAttr ".ktv[250:290]" 259.308 -68.783119201660156 260.34 -68.844703674316406
		 261.376 -68.91448974609375 262.412 -68.890365600585938 263.444 -68.723228454589844
		 264.48 -68.538825988769531 265.512 -68.633026123046875 266.548 -69.142295837402344
		 267.584 -69.794792175292969 268.616 -70.261009216308594 269.652 -70.522201538085938
		 270.688 -70.712646484375 271.72 -70.877655029296875 272.756 -71.020751953125 273.788 -71.128646850585938
		 274.824 -71.152450561523438 275.86 -71.092491149902344 276.892 -71.018928527832031
		 277.928 -70.982315063476562 278.96 -70.971603393554687 279.996 -70.965629577636719
		 281.032 -70.976425170898438 282.064 -71.003311157226563 283.1 -71.008636474609375
		 284.136 -70.990219116210937 285.168 -70.984611511230469 286.204 -71.0040283203125
		 287.236 -71.032699584960938 288.272 -71.05059814453125 289.308 -71.054924011230469
		 290.34 -71.06121826171875 291.376 -71.074371337890625 292.412 -71.085311889648438
		 293.444 -71.097145080566406 294.48 -71.116912841796875 295.512 -71.1480712890625
		 296.548 -71.187576293945313 297.584 -71.194694519042969 298.616 -71.140708923339844
		 299.652 -71.077041625976563 300.688 -71.060546875;
createNode animCurveTA -n "Bip01_R_UpperArm_rotateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 291 ".ktv";
	setAttr ".ktv[0:249]"  0.688 5.8604435920715332 1.72 4.0128388404846191 2.756 1.8529815673828125
		 3.788 0.11877323687076567 4.824 -0.58506911993026733 5.86 -0.61668848991394043 6.892 -0.84286630153656006
		 7.928 -1.3031363487243652 8.964 -1.4713574647903442 9.996 -1.0058449506759644 11.032 0.053244955837726593
		 12.064 1.1035135984420776 13.1 1.5588775873184204 14.136 1.3857269287109375 15.168 0.72690069675445557
		 16.204 -0.47423499822616577 17.24 -3.0548205375671387 18.272 -8.3190402984619141
		 19.308 -15.581072807312012 20.34 -22.076183319091797 21.376 -26.445531845092773 22.412 -29.216302871704105
		 23.444 -30.724464416503906 24.48 -31.056083679199219 25.512 -30.920005798339847 26.548 -30.944477081298828
		 27.584 -31.171201705932614 28.616 -31.400327682495117 29.652 -31.357212066650391
		 30.688 -31.016571044921875 31.72 -30.669340133666996 32.756 -30.452131271362305 33.788 -30.182144165039063
		 34.824 -29.709636688232425 35.86 -29.072189331054691 36.892 -28.238162994384766 37.928 -27.150611877441406
		 38.964 -25.845500946044922 39.996 -24.292055130004883 41.032 -22.607112884521484
		 42.064 -20.886379241943359 43.1 -19.034271240234375 44.136 -17.355745315551758 45.168 -16.313961029052734
		 46.204 -15.887519836425781 47.24 -15.545988082885742 48.272 -14.710268020629885 49.308 -13.50156307220459
		 50.34 -12.554393768310547 51.376 -12.204463958740234 52.412 -12.392534255981445 53.444 -12.834774971008301
		 54.48 -13.21041202545166 55.512 -13.52629566192627 56.548 -14.092927932739258 57.584 -14.875425338745117
		 58.616 -15.383205413818358 59.652 -15.597620964050293 60.688 -15.895519256591797
		 61.72 -16.187398910522461 62.756 -16.303277969360352 63.788 -16.39361572265625 64.824 -16.55790901184082
		 65.86 -16.820661544799805 66.892 -17.310178756713867 67.928 -18.092172622680664 68.964 -19.023509979248047
		 69.996 -19.883380889892578 71.032 -20.543497085571289 72.064 -21.097234725952148
		 73.1 -21.787355422973633 74.136 -22.695285797119141 75.168 -23.621891021728516 76.204 -24.218854904174805
		 77.24 -24.402067184448242 78.272 -24.513614654541016 79.308 -24.677389144897461 80.34 -24.701854705810547
		 81.376 -24.699348449707031 82.412 -24.838441848754883 83.444 -24.909934997558594
		 84.48 -24.799104690551758 85.512 -24.865703582763672 86.548 -25.471576690673828 87.584 -26.527603149414062
		 88.616 -27.650657653808594 89.652 -28.550666809082031 90.688 -29.298269271850586
		 91.72 -30.002700805664059 92.756 -30.502174377441406 93.788 -30.767145156860355 94.824 -30.977849960327145
		 95.86 -31.218101501464847 96.892 -31.484920501708988 97.928 -31.756027221679687 98.964 -31.993877410888672
		 99.996 -32.166957855224609 101.032 -32.372695922851563 102.064 -32.811206817626953
		 103.1 -33.405487060546875 104.136 -33.699859619140625 105.168 -33.509315490722656
		 106.204 -33.260757446289063 107.24 -33.124179840087891 108.272 -32.699024200439453
		 109.308 -31.879165649414059 110.34 -30.97325325012207 111.376 -30.298973083496094
		 112.412 -29.963560104370117 113.444 -29.772106170654297 114.48 -29.524044036865238
		 115.512 -29.086854934692383 116.548 -28.590295791625977 117.584 -28.375526428222656
		 118.616 -28.112274169921875 119.652 -27.334630966186523 120.688 -26.420787811279297
		 121.72 -25.798320770263672 122.756 -25.335918426513672 123.788 -24.77155876159668
		 124.824 -23.864294052124023 125.86 -22.574029922485352 126.892 -21.411550521850586
		 127.928 -20.893211364746094 128.964 -21.116855621337891 129.996 -21.974950790405273
		 131.032 -22.874385833740234 132.064 -23.30750846862793 133.1 -23.533790588378906
		 134.136 -23.846797943115234 135.168 -24.122936248779297 136.204 -24.122034072875977
		 137.236 -23.915401458740234 138.272 -23.761520385742188 139.308 -23.441141128540039
		 140.34 -22.744731903076172 141.376 -22.2578125 142.412 -22.527627944946289 143.444 -23.038414001464844
		 144.48 -22.800439834594727 145.512 -21.596254348754883 146.548 -20.074485778808594
		 147.584 -18.790708541870117 148.616 -17.951766967773438 149.652 -17.633859634399414
		 150.688 -17.48582649230957 151.72 -16.971426010131836 152.756 -16.106132507324219
		 153.788 -15.431239128112791 154.824 -15.203540802001953 155.86 -15.099865913391113
		 156.892 -14.949522972106935 157.928 -14.892434120178221 158.964 -14.958349227905273
		 159.996 -15.206365585327148 161.032 -15.660731315612795 162.064 -16.171186447143555
		 163.1 -16.653675079345703 164.136 -17.082050323486328 165.168 -17.450305938720703
		 166.204 -17.838630676269531 167.236 -18.26176643371582 168.272 -18.59205436706543
		 169.308 -18.674373626708984 170.34 -18.534423828125 171.376 -18.452596664428711 172.412 -18.496129989624023
		 173.444 -18.424980163574219 174.48 -18.333106994628906 175.512 -18.516901016235352
		 176.548 -18.791479110717773 177.584 -18.810516357421875 178.616 -18.760982513427734
		 179.652 -18.837074279785156 180.688 -18.747947692871094 181.72 -18.553537368774414
		 182.756 -18.636167526245117 183.788 -18.842500686645508 184.824 -18.944705963134766
		 185.86 -19.151556015014648 186.892 -19.505924224853516 187.928 -19.850864410400391
		 188.964 -20.186405181884766 189.996 -20.329860687255859 191.032 -20.202627182006836
		 192.064 -20.090129852294922 193.1 -19.952280044555664 194.136 -19.637178421020508
		 195.168 -19.522302627563477 196.204 -19.862876892089844 197.236 -20.277141571044922
		 198.272 -20.450401306152344 199.308 -20.392133712768555 200.34 -20.153827667236328
		 201.376 -19.901290893554688 202.412 -19.751369476318359 203.444 -19.688993453979492
		 204.48 -19.710788726806641 205.512 -19.640701293945313 206.548 -19.388387680053711
		 207.584 -19.228948593139648 208.616 -19.252082824707031 209.652 -19.237434387207031
		 210.688 -19.12605094909668 211.72 -19.057519912719727 212.756 -19.052871704101563
		 213.788 -19.009628295898438 214.824 -18.821048736572266 215.86 -18.467245101928711
		 216.892 -18.16656494140625 217.928 -18.027761459350586 218.964 -17.960704803466797
		 219.996 -17.811834335327148 221.032 -17.336162567138672 222.064 -16.782680511474609
		 223.1 -16.585025787353516 224.136 -16.508350372314453 225.168 -16.325733184814453
		 226.204 -16.30949592590332 227.236 -16.486980438232422 228.272 -16.56117057800293
		 229.308 -16.444549560546875 230.34 -16.061426162719727 231.376 -15.450667381286623
		 232.412 -14.927094459533691 233.444 -14.435277938842772 234.48 -13.643426895141602
		 235.512 -12.028120994567871 236.548 -8.8696746826171875 237.584 -4.670140266418457
		 238.616 -0.94924020767211914 239.652 1.6510113477706909 240.688 3.3739180564880371
		 241.72 4.609222412109375 242.756 5.630669116973877 243.788 6.4764313697814941 244.824 7.0608925819396973
		 245.86 7.2849111557006845 246.892 7.0685410499572754 247.928 6.3343305587768555 248.96 5.5121350288391113
		 249.996 5.7202906608581543 251.032 7.6364541053771973 252.064 11.070085525512695
		 253.1 15.161565780639648 254.136 18.757757186889648 255.168 21.159099578857422 256.204 22.378660202026367
		 257.236 22.997335433959961 258.272 23.736843109130859;
	setAttr ".ktv[250:290]" 259.308 24.826545715332031 260.34 25.860769271850586
		 261.376 26.753307342529297 262.412 28.226791381835938 263.444 30.575754165649411
		 264.48 32.394760131835938 265.512 31.581357955932614 266.548 27.615400314331055 267.584 22.278701782226563
		 268.616 17.864175796508789 269.652 14.611742973327637 270.688 11.476359367370605
		 271.72 8.446742057800293 272.756 5.8272686004638672 273.788 3.6142776012420654 274.824 2.2642378807067871
		 275.86 1.8569539785385132 276.892 1.7107576131820679 277.928 1.36266028881073 278.96 0.71114414930343628
		 279.996 -0.096893288195133209 281.032 -0.77044564485549927 282.064 -1.2354589700698853
		 283.1 -1.4831835031509399 284.136 -1.4896227121353149 285.168 -1.4939365386962891
		 286.204 -1.8800965547561646 287.236 -2.703449010848999 288.272 -3.4576971530914307
		 289.308 -3.7005565166473389 290.34 -3.6978416442871089 291.376 -3.9512736797332759
		 292.412 -4.4227242469787598 293.444 -4.7802615165710449 294.48 -5.1114392280578613
		 295.512 -5.674901008605957 296.548 -6.3228626251220703 297.584 -6.5787420272827148
		 298.616 -6.3201746940612793 299.652 -6.0448050498962402 300.688 -6.0678353309631348;
createNode animCurveTA -n "Bip01_R_Forearm_rotateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 291 ".ktv";
	setAttr ".ktv[0:249]"  0.688 2.1641089916229248 1.72 2.1651387214660645 2.756 2.1666772365570068
		 3.788 2.1663048267364502 4.824 2.1622369289398193 5.86 2.163252592086792 6.892 2.1660423278808594
		 7.928 2.1697864532470703 8.964 2.1728127002716064 9.996 2.174403190612793 11.032 2.1754055023193359
		 12.064 2.1760268211364746 13.1 2.1756448745727539 14.136 2.1740875244140625 15.168 2.1720001697540283
		 16.204 2.169424295425415 17.24 2.1661524772644043 18.272 2.1640057563781738 19.308 2.1632781028747559
		 20.34 2.1613833904266357 21.376 2.158165454864502 22.412 2.1552040576934814 23.444 2.1516807079315186
		 24.48 2.146841287612915 25.512 2.1424064636230469 26.548 2.140305757522583 27.584 2.1405820846557617
		 28.616 2.1416411399841309 29.652 2.1416375637054443 30.688 2.1404149532318115 31.72 2.1392710208892822
		 32.756 2.1388487815856934 33.788 2.1388072967529297 34.824 2.1385183334350586 35.86 2.1379008293151855
		 36.892 2.1375582218170166 37.928 2.1394662857055664 38.964 2.144294261932373 39.996 2.1518678665161133
		 41.032 2.1605086326599121 42.064 2.1683356761932373 43.1 2.1738502979278564 44.136 2.1770668029785156
		 45.168 2.1791789531707764 46.204 2.1792614459991455 47.24 2.176227331161499 48.272 2.1725049018859863
		 49.308 2.1703343391418457 50.34 2.1668546199798584 51.376 2.1598434448242187 52.412 2.1500368118286133
		 53.444 2.1383800506591797 54.48 2.1272132396697998 55.512 2.1177754402160645 56.548 2.1050207614898682
		 57.584 2.0863866806030273 58.616 2.0685534477233887 59.652 2.0561285018920898 60.688 2.0462245941162109
		 61.72 2.0352153778076172 62.756 2.0225703716278076 63.788 2.0117220878601074 64.824 2.0047404766082764
		 65.86 1.9955713748931885 66.892 1.9896551370620725 67.928 1.9857869148254397 68.964 1.9854621887207033
		 69.996 1.9868891239166258 71.032 1.9879581928253176 72.064 1.9856878519058225 73.1 1.9775127172470095
		 74.136 1.9651463031768797 75.168 1.9528472423553469 76.204 1.9457324743270874 77.24 1.9464153051376343
		 78.272 1.9497653245925906 79.308 1.9501584768295288 80.34 1.948875427246094 81.376 1.9434076547622681
		 82.412 1.9384919404983518 83.444 1.9333090782165527 84.48 1.9307634830474856 85.512 1.9296051263809204
		 86.548 1.9339854717254639 87.584 1.930499315261841 88.616 1.9264421463012693 89.652 1.9232276678085325
		 90.688 1.9201037883758545 91.72 1.9151221513748169 92.756 1.9086142778396609 93.788 1.9028468132019041
		 94.824 1.900415778160095 95.86 1.8998174667358398 96.892 1.8980875015258791 97.928 1.8959314823150635
		 98.964 1.8923740386962893 99.996 1.8855240345001221 101.032 1.878471851348877 102.064 1.8738222122192383
		 103.1 1.8684079647064209 104.136 1.8620237112045288 105.168 1.8578196763992307 106.204 1.852908730506897
		 107.24 1.8469967842102051 108.272 1.8458561897277832 109.308 1.8479233980178833 110.34 1.8490525484085083
		 111.376 1.8493583202362061 112.412 1.8472930192947388 113.444 1.841769218444824 114.48 1.8375416994094849
		 115.512 1.8419264554977417 116.548 1.8514112234115601 117.584 1.8547027111053469
		 118.616 1.85534143447876 119.652 1.8618719577789304 120.688 1.8727873563766482 121.72 1.8888908624649046
		 122.756 1.9072660207748415 123.788 1.9152860641479492 124.824 1.9114556312561033
		 125.86 1.9090541601181028 126.892 1.9136048555374148 127.928 1.9209525585174563 128.964 1.9298551082611086
		 129.996 1.9381623268127441 131.032 1.9447176456451416 132.064 1.9525127410888672
		 133.1 1.9599199295043945 134.136 1.9623711109161377 135.168 1.9604684114456177 136.204 1.9589984416961672
		 137.236 1.9577805995941162 138.272 1.9531289339065552 139.308 1.9504331350326536
		 140.34 1.9565150737762451 141.376 1.9638265371322634 142.412 1.961513876914978 143.444 1.9510918855667114
		 144.48 1.9418790340423586 145.512 1.9411289691925049 146.548 1.9509292840957644 147.584 1.9651385545730589
		 148.616 1.9744267463684084 149.652 1.9767323732376099 150.688 1.9758907556533813
		 151.72 1.9760338068008423 152.756 1.9803289175033567 153.788 1.9866306781768799 154.824 1.9862880706787107
		 155.86 1.9868918657302856 156.892 1.98662269115448 157.928 1.9852607250213623 158.964 1.9833071231842043
		 159.996 1.9826704263687134 161.032 1.9910173416137695 162.064 1.9970297813415527
		 163.1 1.9996794462203979 164.136 1.9985113143920898 165.168 1.9975970983505249 166.204 1.9973967075347903
		 167.236 1.997108578681946 168.272 1.9971179962158203 169.308 1.9982026815414426 170.34 2.000112771987915
		 171.376 1.999067544937134 172.412 1.9938089847564697 173.444 1.9898420572280882 174.48 1.9889501333236694
		 175.512 1.9864627122879028 176.548 1.9830374717712405 177.584 1.9836748838424683
		 178.616 1.985012888908386 179.652 1.9822977781295776 180.688 1.9810036420822144 181.72 1.9839502573013308
		 182.756 1.9855604171752932 183.788 1.984678268432617 184.824 1.9840165376663208 185.86 1.982459545135498
		 186.892 1.9791052341461184 187.928 1.9758929014205935 188.964 1.9728189706802368
		 189.996 1.9698770046234131 191.032 1.9675052165985107 192.064 1.9644250869750977
		 193.1 1.9620001316070554 194.136 1.961585998535156 195.168 1.9598968029022219 196.204 1.9553735256195071
		 197.236 1.951340913772583 198.272 1.9492279291152954 199.308 1.9478784799575808 200.34 1.9469056129455566
		 201.376 1.9443231821060181 202.412 1.9380329847335815 203.444 1.927794337272644 204.48 1.9162224531173708
		 205.512 1.9088319540023804 206.548 1.9074821472167969 207.584 1.9075493812561035
		 208.616 1.9066749811172488 209.652 1.9069085121154785 210.688 1.9091545343399048
		 211.72 1.9109816551208494 212.756 1.9096146821975708 213.788 1.9052448272705078 214.824 1.9025408029556274
		 215.86 1.9052060842514038 216.892 1.9093446731567383 217.928 1.9107975959777832 218.964 1.9099495410919189
		 219.996 1.9103137254714966 221.032 1.9165332317352295 222.064 1.923021674156189 223.1 1.9219741821289062
		 224.136 1.9195150136947632 225.168 1.9218770265579224 226.204 1.9243508577346802
		 227.236 1.9246853590011599 228.272 1.9266681671142578 229.308 1.9316844940185547
		 230.34 1.9407763481140139 231.376 1.953509569168091 232.412 1.9641757011413574 233.444 1.9710066318511963
		 234.48 1.9774357080459595 235.512 1.9890978336334231 236.548 2.012056827545166 237.584 2.0410459041595459
		 238.616 2.0638773441314697 239.652 2.0766656398773193 240.688 2.0833218097686768
		 241.72 2.0899975299835205 242.756 2.100330114364624 243.788 2.1130516529083252 244.824 2.124652624130249
		 245.86 2.1326298713684082 246.892 2.1375491619110107 247.928 2.1406304836273193 248.96 2.1403841972351074
		 249.996 2.1362111568450928 251.032 2.1331698894500732 252.064 2.1370613574981689
		 253.1 2.1458740234375 254.136 2.1542682647705078 255.168 2.1605911254882813 256.204 2.1650636196136475
		 257.236 2.1672234535217285 258.272 2.1639988422393799;
	setAttr ".ktv[250:290]" 259.308 2.159644603729248 260.34 2.1534028053283691
		 261.376 2.1485176086425781 262.412 2.1473729610443115 263.444 2.1493508815765381
		 264.48 2.1521360874176025 265.512 2.1542601585388184 266.548 2.1526875495910645 267.584 2.1442883014678955
		 268.616 2.1323301792144775 269.652 2.1187207698822021 270.688 2.1011536121368408
		 271.72 2.0827338695526123 272.756 2.0664913654327393 273.788 2.0513436794281006 274.824 2.0410799980163574
		 275.86 2.0378162860870361 276.892 2.0368411540985107 277.928 2.0354151725769043 278.96 2.0328242778778076
		 279.996 2.0284502506256104 281.032 2.023444652557373 282.064 2.0188143253326416 283.1 2.0155413150787354
		 284.136 2.0149133205413818 285.168 2.0156936645507812 286.204 2.0144937038421631
		 287.236 2.0099492073059082 288.272 2.0048112869262695 289.308 2.0021672248840332
		 290.34 2.0006554126739502 291.376 1.9968336820602415 292.412 1.9914493560791013 293.444 1.9876189231872559
		 294.48 1.9843109846115112 295.512 1.9790571928024294 296.548 1.9725457429885862 297.584 1.9713079929351807
		 298.616 1.9756811857223509 299.652 1.9804337024688721 300.688 1.9835203886032104;
createNode animCurveTA -n "Bip01_R_Forearm_rotateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 291 ".ktv";
	setAttr ".ktv[0:249]"  0.688 -1.9151108264923093 1.72 -1.9103350639343262
		 2.756 -1.9018460512161253 3.788 -1.8887397050857544 4.824 -1.8832049369812012 5.86 -1.8880006074905393
		 6.892 -1.8972172737121582 7.928 -1.909581661224365 8.964 -1.9244661331176758 9.996 -1.9405465126037595
		 11.032 -1.9576702117919922 12.064 -1.9725465774536133 13.1 -1.9834138154983518 14.136 -1.9942104816436768
		 15.168 -2.0140550136566162 16.204 -2.0501425266265869 17.24 -2.1016545295715332 18.272 -2.1723790168762207
		 19.308 -2.2637765407562256 20.34 -2.3538269996643066 21.376 -2.4121265411376953 22.412 -2.4321837425231934
		 23.444 -2.4397642612457275 24.48 -2.4571735858917236 25.512 -2.4785678386688232 26.548 -2.4928386211395264
		 27.584 -2.4985489845275879 28.616 -2.5015857219696045 29.652 -2.5083706378936768
		 30.688 -2.5145535469055176 31.72 -2.5142788887023926 32.756 -2.5119481086730957 33.788 -2.511547327041626
		 34.824 -2.5128262042999268 35.86 -2.5152873992919922 36.892 -2.5132555961608887 37.928 -2.4977846145629883
		 38.964 -2.4616055488586426 39.996 -2.4045343399047852 41.032 -2.3347330093383789
		 42.064 -2.2570910453796387 43.1 -2.174973726272583 44.136 -2.1044454574584961 45.168 -2.0530624389648437
		 46.204 -2.002049446105957 47.24 -1.9407573938369751 48.272 -1.8903785943984985 49.308 -1.8677870035171509
		 50.34 -1.857927203178406 51.376 -1.8391263484954834 52.412 -1.800976037979126 53.444 -1.7455404996871948
		 54.48 -1.6896125078201294 55.512 -1.6384841203689575 56.548 -1.5738480091094971 57.584 -1.495851993560791
		 58.616 -1.4314397573471069 59.652 -1.3902602195739746 60.688 -1.3603079319000244
		 61.72 -1.3314262628555298 62.756 -1.3030295372009277 63.788 -1.2820777893066406 64.824 -1.2708052396774292
		 65.86 -1.2643613815307617 66.892 -1.2524212598800659 67.928 -1.238273024559021 68.964 -1.2243521213531494
		 69.996 -1.2113115787506104 71.032 -1.2000846862792969 72.064 -1.1891992092132568
		 73.1 -1.1754894256591797 74.136 -1.1587845087051392 75.168 -1.1417511701583862 76.204 -1.1322906017303467
		 77.24 -1.1363040208816528 78.272 -1.1440811157226563 79.308 -1.1444550752639771 80.34 -1.1413136720657349
		 81.376 -1.1411205530166626 82.412 -1.1374726295471191 83.444 -1.1333767175674438
		 84.48 -1.1309914588928223 85.512 -1.1274685859680176 86.548 -1.1130064725875854 87.584 -1.0959993600845337
		 88.616 -1.0773992538452148 89.652 -1.0659471750259399 90.688 -1.0626180171966553
		 91.72 -1.0601385831832886 92.756 -1.0532394647598267 93.788 -1.0445688962936401 94.824 -1.0423146486282349
		 95.86 -1.0483890771865845 96.892 -1.0575524568557739 97.928 -1.0636163949966431 98.964 -1.0570558309555054
		 99.996 -1.0348320007324219 101.032 -1.0092803239822388 102.064 -0.99132752418518055
		 103.1 -0.97840511798858643 104.136 -0.96846526861190807 105.168 -0.96370184421539296
		 106.204 -0.95871871709823608 107.24 -0.95242631435394276 108.272 -0.95304471254348766
		 109.308 -0.95900803804397583 110.34 -0.96375381946563721 111.376 -0.96524161100387573
		 112.412 -0.95949316024780273 113.444 -0.94625294208526622 114.48 -0.93624812364578236
		 115.512 -0.94186967611312877 116.548 -0.95634150505065918 117.584 -0.96170037984848022
		 118.616 -0.96362650394439686 119.652 -0.97596174478530884 120.688 -0.99713480472564697
		 121.72 -1.0280500650405884 122.756 -1.0646581649780273 123.788 -1.0860836505889893
		 124.824 -1.0863015651702881 125.86 -1.0841113328933716 126.892 -1.0899050235748291
		 127.928 -1.1012970209121704 128.964 -1.1178762912750244 129.996 -1.1340996026992798
		 131.032 -1.1465065479278564 132.064 -1.1599531173706055 133.1 -1.1697169542312622
		 134.136 -1.166637659072876 135.168 -1.1550703048706055 136.204 -1.145932674407959
		 137.236 -1.139316201210022 138.272 -1.1322313547134399 139.308 -1.1360005140304565
		 140.34 -1.1562126874923706 141.376 -1.1713765859603882 142.412 -1.1616600751876831
		 143.444 -1.1382129192352295 144.48 -1.124855637550354 145.512 -1.1323800086975098
		 146.548 -1.1565001010894775 147.584 -1.1807712316513062 148.616 -1.1881853342056274
		 149.652 -1.1803650856018066 150.688 -1.1722767353057861 151.72 -1.1749129295349121
		 152.756 -1.1900835037231445 153.788 -1.2082507610321045 154.824 -1.2200723886489868
		 155.86 -1.2253662347793579 156.892 -1.2291102409362793 157.928 -1.2302863597869873
		 158.964 -1.2297272682189941 159.996 -1.2294248342514038 161.032 -1.2307387590408325
		 162.064 -1.233615517616272 163.1 -1.2331660985946655 164.136 -1.2316532135009766
		 165.168 -1.2322947978973389 166.204 -1.2326359748840332 167.236 -1.2332032918930054
		 168.272 -1.2371821403503418 169.308 -1.2450685501098633 170.34 -1.2543900012969971
		 171.376 -1.2566752433776855 172.412 -1.2493929862976074 173.444 -1.2436598539352417
		 174.48 -1.2431379556655884 175.512 -1.239230751991272 176.548 -1.2321606874465942
		 177.584 -1.2286323308944702 178.616 -1.2219836711883545 179.652 -1.2077370882034302
		 180.688 -1.2002837657928467 181.72 -1.2040177583694458 182.756 -1.2058746814727783
		 183.788 -1.2032883167266846 184.824 -1.2028249502182007 185.86 -1.2029955387115479
		 186.892 -1.2010865211486816 187.928 -1.200237512588501 188.964 -1.2005953788757324
		 189.996 -1.2010537385940552 191.032 -1.2006537914276123 192.064 -1.1958009004592896
		 193.1 -1.1888647079467773 194.136 -1.1836674213409424 195.168 -1.1750422716140747
		 196.204 -1.1591095924377441 197.236 -1.1413724422454834 198.272 -1.1267728805541992
		 199.308 -1.1164709329605103 200.34 -1.1110075712203979 201.376 -1.1068598031997681
		 202.412 -1.1007654666900635 203.444 -1.0921651124954224 204.48 -1.0835567712783813
		 205.512 -1.0801177024841309 206.548 -1.081264853477478 207.584 -1.0783733129501343
		 208.616 -1.0689215660095215 209.652 -1.0589791536331177 210.688 -1.0531915426254272
		 211.72 -1.0502883195877075 212.756 -1.0470598936080933 213.788 -1.0431581735610962
		 214.824 -1.0430837869644165 215.86 -1.0501840114593506 216.892 -1.0593525171279907
		 217.928 -1.0659432411193848 218.964 -1.0696169137954712 219.996 -1.0736639499664307
		 221.032 -1.0851492881774902 222.064 -1.0965819358825684 223.1 -1.0957634449005127
		 224.136 -1.0918751955032349 225.168 -1.0944263935089111 226.204 -1.0954047441482544
		 227.236 -1.0903618335723877 228.272 -1.0860162973403931 229.308 -1.0868251323699951
		 230.34 -1.0951874256134033 231.376 -1.10964035987854 232.412 -1.1230295896530151
		 233.444 -1.13828444480896 234.48 -1.1648250818252563 235.512 -1.2099215984344482
		 236.548 -1.2812634706497192 237.584 -1.3708231449127197 238.616 -1.4548863172531128
		 239.652 -1.518987774848938 240.688 -1.5629023313522339 241.72 -1.595125675201416
		 242.756 -1.6239019632339478 243.788 -1.6497299671173096 244.824 -1.6688669919967651
		 245.86 -1.6779435873031616 246.892 -1.6801592111587524 247.928 -1.6800262928009033
		 248.96 -1.6735634803771973 249.996 -1.6605246067047119 251.032 -1.6593441963195801
		 252.064 -1.6887663602828979 253.1 -1.7401953935623169 254.136 -1.7883683443069458
		 255.168 -1.8181388378143308 256.204 -1.8294680118560793 257.236 -1.8316016197204592
		 258.272 -1.8377720117568968;
	setAttr ".ktv[250:290]" 259.308 -1.8489605188369751 260.34 -1.8588552474975586
		 261.376 -1.8666172027587888 262.412 -1.8851549625396729 263.444 -1.9194247722625735
		 264.48 -1.9447031021118166 265.512 -1.9211258888244629 266.548 -1.837272524833679
		 267.584 -1.7293566465377808 268.616 -1.6426454782485962 269.652 -1.5805985927581787
		 270.688 -1.5218348503112793 271.72 -1.4645545482635498 272.756 -1.413110613822937
		 273.788 -1.3685139417648315 274.824 -1.3436547517776489 275.86 -1.3415553569793701
		 276.892 -1.3462828397750854 277.928 -1.3462393283843994 278.96 -1.3385765552520752
		 279.996 -1.3261531591415405 281.032 -1.3156739473342896 282.064 -1.3092688322067261
		 283.1 -1.3061834573745728 284.136 -1.306362509727478 285.168 -1.3044945001602173
		 286.204 -1.2923623323440552 287.236 -1.2707453966140747 288.272 -1.2514487504959106
		 289.308 -1.2438026666641235 290.34 -1.2433235645294189 291.376 -1.2387518882751465
		 292.412 -1.2282611131668091 293.444 -1.2182592153549194 294.48 -1.2092444896697998
		 295.512 -1.1980875730514526 296.548 -1.1870095729827881 297.584 -1.1856560707092285
		 298.616 -1.1941124200820923 299.652 -1.2029794454574585 300.688 -1.2072117328643799;
createNode animCurveTA -n "Bip01_R_Forearm_rotateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 291 ".ktv";
	setAttr ".ktv[0:249]"  0.688 -75.148101806640625 1.72 -75.027076721191406
		 2.756 -74.811264038085938 3.788 -74.467849731445313 4.824 -74.306625366210937 5.86 -74.435783386230469
		 6.892 -74.687828063964844 7.928 -75.025146484375 8.964 -75.425270080566406 9.996 -75.850257873535156
		 11.032 -76.300498962402344 12.064 -76.691200256347656 13.1 -76.97381591796875 14.136 -77.251907348632813
		 15.168 -77.768112182617188 16.204 -78.715614318847656 17.24 -80.07403564453125 18.272 -81.944435119628906
		 19.308 -84.360076904296875 20.34 -86.736335754394531 21.376 -88.275726318359375 22.412 -88.80902099609375
		 23.444 -89.016120910644531 24.48 -89.4857177734375 25.512 -90.059356689453125 26.548 -90.440147399902344
		 27.584 -90.588882446289063 28.616 -90.66485595703125 29.652 -90.842857360839844 30.688 -91.008583068847656
		 31.72 -91.004661560058594 32.756 -90.944908142089844 33.788 -90.934593200683594 34.824 -90.968902587890625
		 35.86 -91.035324096679688 36.892 -90.983467102050781 37.928 -90.572319030761719 38.964 -89.608802795410156
		 39.996 -88.089752197265625 41.032 -86.234779357910156 42.064 -84.178192138671875
		 43.1 -82.011245727539062 44.136 -80.155960083007812 45.168 -78.809822082519531 46.204 -77.474052429199219
		 47.24 -75.864082336425781 48.272 -74.540000915527344 49.308 -73.944778442382812 50.34 -73.672340393066406
		 51.376 -73.150764465332031 52.412 -72.112709045410156 53.444 -70.616462707519531
		 54.48 -69.109649658203125 55.512 -67.73687744140625 56.548 -66.0009765625 57.584 -63.889225006103523
		 58.616 -62.130573272705078 59.652 -61.000701904296882 60.688 -60.171703338623047
		 61.72 -59.357795715332031 62.756 -58.538154602050781 63.788 -57.916290283203125 64.824 -57.569297790527351
		 65.86 -57.31629943847657 66.892 -56.964645385742188 67.928 -56.581710815429687 68.964 -56.246936798095703
		 69.996 -55.958751678466797 71.032 -55.713790893554688 72.064 -55.435794830322266
		 73.1 -55.015045166015625 74.136 -54.468925476074219 75.168 -53.916103363037109 76.204 -53.605998992919922
		 77.24 -53.706912994384766 78.272 -53.929157257080078 79.308 -53.942558288574219 80.34 -53.854057312011719
		 81.376 -53.781349182128906 82.412 -53.636287689208984 83.444 -53.477836608886719
		 84.48 -53.391147613525391 85.512 -53.294902801513672 86.548 -53.012550354003906 87.584 -52.581981658935547
		 88.616 -52.111377716064453 89.652 -51.813220977783203 90.688 -51.693683624267578
		 91.72 -51.565261840820313 92.756 -51.318210601806641 93.788 -51.043209075927734 94.824 -50.958812713623047
		 95.86 -51.087471008300781 96.892 -51.272891998291016 97.928 -51.384170532226562 98.964 -51.186878204345703
		 99.996 -50.587978363037109 101.032 -49.916664123535156 102.064 -49.45428466796875
		 103.1 -49.093448638916016 104.136 -48.782474517822266 105.168 -48.615386962890625
		 106.204 -48.432819366455078 107.24 -48.206813812255859 108.272 -48.202644348144531
		 109.308 -48.3629150390625 110.34 -48.482509613037109 111.376 -48.519645690917969
		 112.412 -48.363864898681641 113.444 -47.994949340820312 114.48 -47.715461730957031
		 115.512 -47.903240203857422 116.548 -48.358558654785156 117.584 -48.523952484130859
		 118.616 -48.575294494628906 119.652 -48.940059661865234 120.688 -49.563083648681641
		 121.72 -50.477706909179687 122.756 -51.554496765136719 123.788 -52.149925231933594
		 124.824 -52.105998992919922 125.86 -52.024425506591797 126.892 -52.216098785400391
		 127.928 -52.573169708251953 128.964 -53.070522308349609 129.996 -53.5521240234375
		 131.032 -53.922981262207031 132.064 -54.332855224609375 133.1 -54.652614593505859
		 134.136 -54.612255096435547 135.168 -54.321895599365234 136.204 -54.094390869140625
		 137.236 -53.929004669189453 138.272 -53.706283569335937 139.308 -53.755802154541016
		 140.34 -54.297000885009766 141.376 -54.739677429199219 142.412 -54.485855102539063
		 143.444 -53.812431335449219 144.48 -53.386859893798828 145.512 -53.550617218017578
		 146.548 -54.233421325683594 147.584 -54.975933074951172 148.616 -55.264984130859375
		 149.652 -55.115707397460937 150.688 -54.920124053955078 151.72 -54.981330871582031
		 152.756 -55.383033752441406 153.788 -55.881351470947266 154.824 -56.152053833007813
		 155.86 -56.283710479736328 156.892 -56.369579315185547 157.928 -56.382240295410156
		 158.964 -56.34698486328125 159.996 -56.333637237548828 161.032 -56.460872650146484
		 162.064 -56.60162353515625 163.1 -56.623233795166016 164.136 -56.572879791259766
		 165.168 -56.576053619384766 166.204 -56.581249237060547 167.236 -56.590999603271484
		 168.272 -56.684127807617187 169.308 -56.882297515869141 170.34 -57.125110626220703
		 171.376 -57.167816162109375 172.412 -56.934837341308594 173.444 -56.753646850585937
		 174.48 -56.731315612792969 175.512 -56.610187530517578 176.548 -56.403133392333984
		 177.584 -56.325630187988281 178.616 -56.183475494384766 179.652 -55.815452575683594
		 180.688 -55.625293731689453 181.72 -55.749080657958984 182.756 -55.812404632568359
		 183.788 -55.741287231445313 184.824 -55.722312927246094 185.86 -55.706588745117188
		 186.892 -55.620399475097656 187.928 -55.561447143554688 188.964 -55.533443450927734
		 189.996 -55.510688781738281 191.032 -55.474288940429687 192.064 -55.3236083984375
		 193.1 -55.130176544189453 194.136 -55.001277923583984 195.168 -54.777252197265625
		 196.204 -54.349662780761719 197.236 -53.889610290527344 198.272 -53.530433654785156
		 199.308 -53.280937194824219 200.34 -53.146110534667969 201.376 -53.017208099365234
		 202.412 -52.79254150390625 203.444 -52.457550048828125 204.48 -52.105541229248047
		 205.512 -51.929695129394531 206.548 -51.938480377197266 207.584 -51.872425079345703
		 208.616 -51.644397735595703 209.652 -51.423233032226563 210.688 -51.325965881347656
		 211.72 -51.288688659667969 212.756 -51.198230743408203 213.788 -51.048294067382812
		 214.824 -51.006259918212891 215.86 -51.201194763183594 216.892 -51.463432312011719
		 217.928 -51.631576538085937 218.964 -51.702518463134766 219.996 -51.798904418945313
		 221.032 -52.143596649169922 222.064 -52.490482330322266 223.1 -52.457984924316406
		 224.136 -52.336978912353516 225.168 -52.4261474609375 226.204 -52.481582641601563
		 227.236 -52.372406005859375 228.272 -52.303184509277344 229.308 -52.393413543701172
		 230.34 -52.711460113525391 231.376 -53.2181396484375 232.412 -53.671169281005859
		 233.444 -54.105438232421875 234.48 -54.783176422119141 235.512 -55.959339141845703
		 236.548 -57.906990051269531 237.584 -60.380638122558594 238.616 -62.680171966552741
		 239.652 -64.410850524902344 240.688 -65.589576721191406 241.72 -66.466583251953125
		 242.756 -67.26934814453125 243.788 -68.004974365234375 244.824 -68.562095642089844
		 245.86 -68.843193054199219 246.892 -68.932762145996094 247.928 -68.951629638671875
		 248.96 -68.786399841308594 249.996 -68.425117492675781 251.032 -68.372642517089844
		 252.064 -69.147506713867187 253.1 -70.519355773925781 254.136 -71.807373046875 255.168 -72.610519409179688
		 256.204 -72.928184509277344 257.236 -72.995964050292969 258.272 -73.137557983398438;
	setAttr ".ktv[250:290]" 259.308 -73.409080505371094 260.34 -73.642982482910156
		 261.376 -73.831314086914063 262.412 -74.319023132324219 263.444 -75.231094360351562
		 264.48 -75.906318664550781 265.512 -75.288497924804687 266.548 -73.082077026367188
		 267.584 -70.241035461425781 268.616 -67.947334289550781 269.652 -66.278884887695312
		 270.688 -64.663742065429687 271.72 -63.07936096191407 272.756 -61.661952972412102
		 273.788 -60.424575805664063 274.824 -59.715782165527337 275.86 -59.630268096923828
		 276.892 -59.733928680419929 277.928 -59.718082427978516 278.96 -59.505180358886719
		 279.996 -59.158046722412109 281.032 -58.852088928222663 282.064 -58.648555755615227
		 283.1 -58.539764404296875 284.136 -58.537784576416009 285.168 -58.500854492187493
		 286.204 -58.195522308349609 287.236 -57.628131866455085 288.272 -57.111541748046875
		 289.308 -56.900466918945313 290.34 -56.870765686035156 291.376 -56.717250823974609
		 292.412 -56.406318664550781 293.444 -56.125408172607422 294.48 -55.874431610107422
		 295.512 -55.549633026123047 296.548 -55.210979461669922 297.584 -55.164035797119141
		 298.616 -55.415233612060547 299.652 -55.680503845214844 300.688 -55.817718505859375;
createNode animCurveTA -n "Bip01_L_Clavicle_rotateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 291 ".ktv";
	setAttr ".ktv[0:249]"  0.688 1.2446457147598267 1.72 1.2248022556304932 2.756 1.2088857889175415
		 3.788 1.207001805305481 4.824 1.2173762321472168 5.86 1.2390545606613159 6.892 1.2727272510528564
		 7.928 1.3207706212997437 8.964 1.3757089376449585 9.996 1.4431099891662598 11.032 1.5192745923995972
		 12.064 1.6238727569580078 13.1 1.7221775054931641 14.136 1.8336982727050779 15.168 1.954575300216675
		 16.204 2.0820114612579346 17.24 2.2140569686889648 18.272 2.3486931324005127 19.308 2.4839150905609131
		 20.34 2.6178679466247559 21.376 2.7468461990356445 22.412 2.8653521537780762 23.444 3.003225564956665
		 24.48 3.1021897792816162 25.512 3.2202310562133789 26.548 3.3060636520385742 27.584 3.403799295425415
		 28.616 3.4753715991973877 29.652 3.5495471954345703 30.688 3.6117150783538818 31.72 3.6632065773010254
		 32.756 3.7044305801391602 33.788 3.7339224815368657 34.824 3.7495577335357666 35.86 3.7602775096893306
		 36.892 3.744847297668457 37.928 3.7332749366760254 38.964 3.6970698833465576 39.996 3.6188590526580806
		 41.032 3.5850174427032471 42.064 3.45644211769104 43.1 3.406435489654541 44.136 3.2293617725372314
		 45.168 3.1403555870056152 46.204 2.995124340057373 47.24 2.8606560230255127 48.272 2.716991662979126
		 49.308 2.5728962421417236 50.34 2.4067533016204834 51.376 2.2629768848419189 52.412 2.1205472946166992
		 53.444 1.968268156051636 54.48 1.8224771022796633 55.512 1.6886961460113525 56.548 1.5562229156494141
		 57.584 1.4244098663330078 58.616 1.2982800006866455 59.652 1.173197865486145 60.688 1.0537763833999634
		 61.72 0.9292399287223817 62.756 0.81320267915725708 63.788 0.68722528219223022 64.824 0.57140243053436279
		 65.86 0.442903071641922 66.892 0.32590770721435547 67.928 0.19580875337123871 68.964 0.079750381410121918
		 69.996 -0.046501778066158295 71.032 -0.17041382193565369 72.064 -0.27950146794319153
		 73.1 -0.3966948390007019 74.136 -0.49467509984970093 75.168 -0.59643250703811646
		 76.204 -0.6775098443031311 77.24 -0.74654746055603027 78.272 -0.81044924259185791
		 79.308 -0.84854155778884888 80.34 -0.87882119417190552 81.376 -0.88301444053649902
		 82.412 -0.87652617692947388 83.444 -0.84530776739120483 84.48 -0.79551655054092407
		 85.512 -0.7306787371635437 86.548 -0.64703565835952759 87.584 -0.54403454065322876
		 88.616 -0.42775410413742065 89.652 -0.29633757472038269 90.688 -0.15301553905010223
		 91.72 0.00074904959183186293 92.756 0.16331936419010162 93.788 0.33302181959152222
		 94.824 0.50855928659439087 95.86 0.68882834911346436 96.892 0.87297463417053223 97.928 1.0602879524230957
		 98.964 1.2504086494445801 99.996 1.4430288076400757 101.032 1.6378934383392334 102.064 1.8348475694656374
		 103.1 2.0306174755096436 104.136 2.2288956642150879 105.168 2.4262402057647705 106.204 2.6157412528991699
		 107.24 2.8050611019134521 108.272 2.9787752628326416 109.308 3.1453936100006104 110.34 3.2947750091552734
		 111.376 3.4247260093688965 112.412 3.5258777141571045 113.444 3.599610328674316 114.48 3.6455125808715816
		 115.512 3.6534514427185059 116.548 3.6272685527801514 117.584 3.5636918544769287
		 118.616 3.4634096622467041 119.652 3.3334269523620605 120.688 3.1691672801971436
		 121.72 2.9799790382385254 122.756 2.7709567546844482 123.788 2.5481939315795898 124.824 2.3184475898742676
		 125.86 2.0871114730834961 126.892 1.8580729961395264 127.928 1.6524338722229004 128.964 1.4545649290084839
		 129.996 1.2814843654632568 131.032 1.1437896490097046 132.064 1.0315945148468018
		 133.1 0.93900048732757557 134.136 0.87757128477096558 135.168 0.85415416955947876
		 136.204 0.84947150945663452 137.236 0.87886977195739746 138.272 0.92728245258331288
		 139.308 0.99860626459121704 140.34 1.0894665718078613 141.376 1.1959776878356934
		 142.412 1.3166216611862183 143.444 1.4439315795898437 144.48 1.5832593441009521 145.512 1.719749927520752
		 146.548 1.8641120195388792 147.584 2.0064253807067871 148.616 2.1458005905151367
		 149.652 2.2818925380706787 150.688 2.4117579460144043 151.72 2.5312697887420654 152.756 2.6288490295410156
		 153.788 2.7184927463531494 154.824 2.7905409336090088 155.86 2.8448801040649414 156.892 2.8779106140136719
		 157.928 2.8900651931762695 158.964 2.8771176338195801 159.996 2.8420312404632568
		 161.032 2.7785966396331787 162.064 2.6904268264770508 163.1 2.5768682956695557 164.136 2.4472582340240479
		 165.168 2.2995114326477051 166.204 2.1302855014801025 167.236 1.9510273933410645
		 168.272 1.7631168365478516 169.308 1.5798583030700684 170.34 1.3887315988540649 171.376 1.2025061845779419
		 172.412 1.0321333408355713 173.444 0.86639642715454102 174.48 0.71673446893692017
		 175.512 0.58463555574417114 176.548 0.47150462865829473 177.584 0.37847748398780823
		 178.616 0.31190058588981628 179.652 0.26478534936904907 180.688 0.2425257861614227
		 181.72 0.2447688281536102 182.756 0.27087852358818054 183.788 0.32294830679893494
		 184.824 0.39595434069633484 185.86 0.49319490790367132 186.892 0.60981196165084839
		 187.928 0.74581992626190186 188.964 0.89893275499343883 189.996 1.0668667554855347
		 191.032 1.2470149993896484 192.064 1.4368687868118286 193.1 1.6329082250595093 194.136 1.8335978984832766
		 195.168 2.0356645584106445 196.204 2.2371494770050049 197.236 2.4326105117797852
		 198.272 2.624950647354126 199.308 2.809473991394043 200.34 2.9855690002441406 201.376 3.1466760635375977
		 202.412 3.3003518581390381 203.444 3.4408807754516602 204.48 3.5686764717102051 205.512 3.6763794422149658
		 206.548 3.774744033813477 207.584 3.8535614013671875 208.616 3.9069900512695312 209.652 3.9743208885192871
		 210.688 3.9996423721313477 211.72 4.0017857551574707 212.756 3.9886202812194824 213.788 3.9486312866210938
		 214.824 3.8917348384857182 215.86 3.8105700016021729 216.892 3.7058098316192627 217.928 3.5787398815155029
		 218.964 3.4309296607971191 219.996 3.2630112171173096 221.032 3.0791976451873779
		 222.064 2.8810892105102539 223.1 2.675154447555542 224.136 2.4599997997283936 225.168 2.2422747611999512
		 226.204 2.0244364738464355 227.236 1.808613657951355 228.272 1.6059682369232178 229.308 1.4044934511184692
		 230.34 1.214537501335144 231.376 1.036164402961731 232.412 0.868732750415802 233.444 0.71002078056335449
		 234.48 0.57137680053710938 235.512 0.44367507100105286 236.548 0.31343179941177368
		 237.584 0.20607881247997284 238.616 0.10796022415161133 239.652 0.0019420171156525612
		 240.688 -0.081943288445472717 241.72 -0.16246683895587921 242.756 -0.23776194453239441
		 243.788 -0.30695268511772156 244.824 -0.37007007002830505 245.86 -0.42704436182975769
		 246.892 -0.47662043571472174 247.928 -0.51698833703994751 248.96 -0.56035244464874268
		 249.996 -0.58633381128311157 251.032 -0.60347169637680054 252.064 -0.61482352018356323
		 253.1 -0.61401939392089844 254.136 -0.62008464336395264 255.168 -0.60076224803924561
		 256.204 -0.58923590183258057 257.236 -0.56149798631668091 258.272 -0.53751623630523682;
	setAttr ".ktv[250:290]" 259.308 -0.50997114181518555 260.34 -0.48192194104194636
		 261.376 -0.45534104108810425 262.412 -0.43116351962089539 263.444 -0.41867455840110779
		 264.48 -0.40674829483032227 265.512 -0.40341129899024963 266.548 -0.40692374110221863
		 267.584 -0.42900106310844421 268.616 -0.44993841648101807 269.652 -0.48089081048965454
		 270.688 -0.51892548799514771 271.72 -0.55735206604003906 272.756 -0.59222042560577393
		 273.788 -0.66193115711212158 274.824 -0.73975014686584473 275.86 -0.77546501159667969
		 276.892 -0.81735801696777344 277.928 -0.8492732048034668 278.96 -0.9131007194519043
		 279.996 -0.95473253726959229 281.032 -0.99069863557815541 282.064 -1.0199220180511475
		 283.1 -1.0467574596405029 284.136 -1.0514243841171265 285.168 -1.0663312673568726
		 286.204 -1.0695210695266724 287.236 -1.0642356872558594 288.272 -1.0507131814956665
		 289.308 -1.0272601842880249 290.34 -0.98186737298965454 291.376 -0.95186823606491089
		 292.412 -0.91543388366699219 293.444 -0.80819129943847656 294.48 -0.74411094188690186
		 295.512 -0.65402054786682129 296.548 -0.54584628343582153 297.584 -0.42418470978736877
		 298.616 -0.29004374146461487 299.652 -0.14517158269882202 300.688 0.0082623744383454323;
createNode animCurveTA -n "Bip01_L_Clavicle_rotateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 291 ".ktv";
	setAttr ".ktv[0:249]"  0.688 -79.755195617675781 1.72 -79.742897033691406
		 2.756 -79.725067138671875 3.788 -79.718849182128906 4.824 -79.722091674804688 5.86 -79.733680725097656
		 6.892 -79.754753112792969 7.928 -79.788558959960938 8.964 -79.823081970214844 9.996 -79.866836547851563
		 11.032 -79.9141845703125 12.064 -79.995033264160156 13.1 -80.057647705078125 14.136 -80.132087707519531
		 15.168 -80.213600158691406 16.204 -80.299140930175781 17.24 -80.387359619140625 18.272 -80.476669311523437
		 19.308 -80.565940856933594 20.34 -80.653656005859375 21.376 -80.73626708984375 22.412 -80.807945251464844
		 23.444 -80.906196594238281 24.48 -80.960090637207031 25.512 -81.040420532226563 26.548 -81.086151123046875
		 27.584 -81.149177551269531 28.616 -81.18585205078125 29.652 -81.229042053222656 30.688 -81.262435913085938
		 31.72 -81.287994384765625 32.756 -81.306785583496094 33.788 -81.317848205566406 34.824 -81.319534301757812
		 35.86 -81.322601318359375 36.892 -81.304595947265625 37.928 -81.29705810546875 38.964 -81.266914367675781
		 39.996 -81.218063354492187 41.032 -81.197586059570313 42.064 -81.1197509765625 43.1 -81.090003967285156
		 44.136 -80.986915588378906 45.168 -80.939361572265625 46.204 -80.843528747558594
		 47.24 -80.769569396972656 48.272 -80.6881103515625 49.308 -80.608604431152344 50.34 -80.501396179199219
		 51.376 -80.423690795898438 52.412 -80.347221374511719 53.444 -80.255859375 54.48 -80.171661376953125
		 55.512 -80.103118896484375 56.548 -80.034988403320313 57.584 -79.966522216796875
		 58.616 -79.905754089355469 59.652 -79.846290588378906 60.688 -79.796661376953125
		 61.72 -79.739738464355469 62.756 -79.698829650878906 63.788 -79.643226623535156 64.824 -79.6080322265625
		 65.86 -79.5526123046875 66.892 -79.520660400390625 67.928 -79.465156555175781 68.964 -79.437141418457031
		 69.996 -79.387466430664063 71.032 -79.338790893554688 72.064 -79.315498352050781
		 73.1 -79.266128540039063 74.136 -79.247138977050781 75.168 -79.203971862792969 76.204 -79.189140319824219
		 77.24 -79.179939270019531 78.272 -79.155792236328125 79.308 -79.164375305175781 80.34 -79.158615112304688
		 81.376 -79.181625366210937 82.412 -79.193443298339844 83.444 -79.2281494140625 84.48 -79.269905090332031
		 85.512 -79.310317993164063 86.548 -79.359840393066406 87.584 -79.421363830566406
		 88.616 -79.482635498046875 89.652 -79.551048278808594 90.688 -79.622024536132813
		 91.72 -79.695487976074219 92.756 -79.770790100097656 93.788 -79.847328186035156 94.824 -79.9246826171875
		 95.86 -80.002449035644531 96.892 -80.080467224121094 97.928 -80.158576965332031 98.964 -80.236747741699219
		 99.996 -80.314964294433594 101.032 -80.39337158203125 102.064 -80.472282409667969
		 103.1 -80.547500610351563 104.136 -80.625602722167969 105.168 -80.702812194824219
		 106.204 -80.772026062011719 107.24 -80.845619201660156 108.272 -80.906661987304688
		 109.308 -80.968833923339844 110.34 -81.023208618164063 111.376 -81.070724487304688
		 112.412 -81.103988647460938 113.444 -81.127960205078125 114.48 -81.144851684570313
		 115.512 -81.145408630371094 116.548 -81.135444641113281 117.584 -81.111518859863281
		 118.616 -81.073623657226563 119.652 -81.027946472167969 120.688 -80.965896606445313
		 121.72 -80.894371032714844 122.756 -80.814239501953125 123.788 -80.726913452148437
		 124.824 -80.634521484375 125.86 -80.537956237792969 126.892 -80.436553955078125 127.928 -80.354759216308594
		 128.964 -80.259407043457031 129.996 -80.170570373535156 131.032 -80.106704711914063
		 132.064 -80.052116394042969 133.1 -79.993972778320313 134.136 -79.950889587402344
		 135.168 -79.936393737792969 136.204 -79.918930053710938 137.236 -79.927970886230469
		 138.272 -79.940650939941406 139.308 -79.967201232910156 140.34 -80.005210876464844
		 141.376 -80.051971435546875 142.412 -80.108207702636719 143.444 -80.165176391601563
		 144.48 -80.234329223632813 145.512 -80.295417785644531 146.548 -80.367118835449219
		 147.584 -80.436683654785156 148.616 -80.504508972167969 149.652 -80.570655822753906
		 150.688 -80.634025573730469 151.72 -80.693458557128906 152.756 -80.736160278320313
		 153.788 -80.780921936035156 154.824 -80.815986633300781 155.86 -80.842552185058594
		 156.892 -80.857902526855469 157.928 -80.864837646484375 158.964 -80.85968017578125
		 159.996 -80.846893310546875 161.032 -80.818939208984375 162.064 -80.779891967773438
		 163.1 -80.727424621582031 164.136 -80.671890258789063 165.168 -80.607986450195313
		 166.204 -80.527679443359375 167.236 -80.442825317382812 168.272 -80.35113525390625
		 169.308 -80.268386840820312 170.34 -80.171371459960938 171.376 -80.075157165527344
		 172.412 -79.994003295898438 173.444 -79.905754089355469 174.48 -79.826171875 175.512 -79.755233764648437
		 176.548 -79.692947387695312 177.584 -79.639495849609375 178.616 -79.604804992675781
		 179.652 -79.574684143066406 180.688 -79.558586120605469 181.72 -79.555290222167969
		 182.756 -79.563545227050781 183.788 -79.587692260742187 184.824 -79.618728637695313
		 185.86 -79.664215087890625 186.892 -79.716644287109375 187.928 -79.778244018554688
		 188.964 -79.847305297851563 189.996 -79.922286987304688 191.032 -80.0018310546875
		 192.064 -80.08477783203125 193.1 -80.168716430664063 194.136 -80.254127502441406
		 195.168 -80.338966369628906 196.204 -80.422927856445313 197.236 -80.501083374023437
		 198.272 -80.579360961914063 199.308 -80.653518676757813 200.34 -80.724227905273438
		 201.376 -80.784942626953125 202.412 -80.845970153808594 203.444 -80.901268005371094
		 204.48 -80.952308654785156 205.512 -80.991546630859375 206.548 -81.031822204589844
		 207.584 -81.064224243164063 208.616 -81.0880126953125 209.652 -81.117454528808594
		 210.688 -81.128044128417969 211.72 -81.135795593261719 212.756 -81.138114929199219
		 213.788 -81.126686096191406 214.824 -81.113365173339844 215.86 -81.090309143066406
		 216.892 -81.05816650390625 217.928 -81.017257690429688 218.964 -80.967926025390625
		 219.996 -80.909042358398438 221.032 -80.842994689941406 222.064 -80.768714904785156
		 223.1 -80.6910400390625 224.136 -80.604766845703125 225.168 -80.514915466308594 226.204 -80.42138671875
		 227.236 -80.323661804199219 228.272 -80.235725402832031 229.308 -80.137458801269531
		 230.34 -80.042579650878906 231.376 -79.950225830078125 232.412 -79.858901977539062
		 233.444 -79.764480590820312 234.48 -79.6876220703125 235.512 -79.613189697265625
		 236.548 -79.516891479492187 237.584 -79.447578430175781 238.616 -79.381217956542969
		 239.652 -79.284774780273437 240.688 -79.218788146972656 241.72 -79.147056579589844
		 242.756 -79.073875427246094 243.788 -79.001350402832031 244.824 -78.930007934570312
		 245.86 -78.86029052734375 246.892 -78.793464660644531 247.928 -78.731300354003906
		 248.96 -78.667236328125 249.996 -78.614356994628906 251.032 -78.568336486816406 252.064 -78.52978515625
		 253.1 -78.5067138671875 254.136 -78.460250854492187 255.168 -78.460693359375 256.204 -78.439826965332031
		 257.236 -78.4501953125 258.272 -78.451980590820313;
	setAttr ".ktv[250:290]" 259.308 -78.463211059570313 260.34 -78.480003356933594
		 261.376 -78.500572204589844 262.412 -78.525222778320313 263.444 -78.537109375 264.48 -78.56024169921875
		 265.512 -78.579460144042969 266.548 -78.598472595214844 267.584 -78.593338012695313
		 268.616 -78.603584289550781 269.652 -78.604827880859375 270.688 -78.601211547851562
		 271.72 -78.598960876464844 272.756 -78.585899353027344 273.788 -78.567314147949219
		 274.824 -78.5318603515625 275.86 -78.518569946289063 276.892 -78.507606506347656
		 277.928 -78.499580383300781 278.96 -78.466331481933594 279.996 -78.451148986816406
		 281.032 -78.432945251464844 282.064 -78.421684265136719 283.1 -78.410545349121094
		 284.136 -78.411720275878906 285.168 -78.404518127441406 286.204 -78.406280517578125
		 287.236 -78.412445068359375 288.272 -78.423698425292969 289.308 -78.438331604003906
		 290.34 -78.467941284179688 291.376 -78.488357543945313 292.412 -78.512504577636719
		 293.444 -78.576469421386719 294.48 -78.616241455078125 295.512 -78.670440673828125
		 296.548 -78.734428405761719 297.584 -78.805465698242187 298.616 -78.88275146484375
		 299.652 -78.96527099609375 300.688 -79.05218505859375;
createNode animCurveTA -n "Bip01_L_Clavicle_rotateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 291 ".ktv";
	setAttr ".ktv[0:249]"  0.688 172.54450988769531 1.72 172.66481018066406 2.756 172.77610778808594
		 3.788 172.87429809570312 4.824 172.959228515625 5.86 173.03070068359375 6.892 173.08818054199219
		 7.928 173.130859375 8.964 173.16163635253906 9.996 173.17953491210937 11.032 173.18716430664062
		 12.064 173.17918395996094 13.1 173.17008972167969 14.136 173.15493774414062 15.168 173.13735961914062
		 16.204 173.12042236328125 17.24 173.10684204101562 18.272 173.0989990234375 19.308 173.09901428222656
		 20.34 173.1082763671875 21.376 173.12867736816406 22.412 173.16246032714844 23.444 173.19450378417969
		 24.48 173.25157165527344 25.512 173.30387878417969 26.548 173.37506103515625 27.584 173.44099426269531
		 28.616 173.51936340332031 29.652 173.59347534179687 30.688 173.66929626464844 31.72 173.74488830566406
		 32.756 173.8189697265625 33.788 173.89169311523437 34.824 173.96368408203125 35.86 174.02996826171875
		 36.892 174.10202026367188 37.928 174.1611328125 38.964 174.21821594238281 39.996 174.32341003417969
		 41.032 174.35795593261719 42.064 174.46574401855469 43.1 174.5023193359375 44.136 174.61703491210937
		 45.168 174.6685791015625 46.204 174.74507141113281 47.24 174.81080627441406 48.272 174.87399291992187
		 49.308 174.92974853515625 50.34 174.9866943359375 51.376 175.02415466308594 52.412 175.05061340332031
		 53.444 175.06997680664062 54.48 175.07490539550781 55.512 175.06349182128906 56.548 175.04032897949219
		 57.584 175.00663757324219 58.616 174.962158203125 59.652 174.91079711914062 60.688 174.85382080078125
		 61.72 174.79736328125 62.756 174.74046325683594 63.788 174.69120788574219 64.824 174.64697265625
		 65.86 174.61538696289062 66.892 174.59236145019531 67.928 174.58332824707031 68.964 174.58299255371094
		 69.996 174.59408569335938 71.032 174.61251831054687 72.064 174.63455200195312 73.1 174.65914916992187
		 74.136 174.68092346191406 75.168 174.69735717773437 76.204 174.70440673828125 77.24 174.69918823242187
		 78.272 174.6783447265625 79.308 174.64056396484375 80.34 174.58302307128906 81.376 174.50559997558594
		 82.412 174.40660095214844 83.444 174.28681945800781 84.48 174.14604187011719 85.512 173.98472595214844
		 86.548 173.80418395996094 87.584 173.60560607910156 88.616 173.39047241210937 89.652 173.16061401367188
		 90.688 172.91778564453125 91.72 172.66398620605469 92.756 172.40118408203125 93.788 172.13127136230469
		 94.824 171.85574340820312 95.86 171.57594299316406 96.892 171.29280090332031 97.928 171.00715637207031
		 98.964 170.7193603515625 99.996 170.42958068847656 101.032 170.13786315917969 102.064 169.84408569335937
		 103.1 169.54931640625 104.136 169.25216674804687 105.168 168.95413208007812 106.204 168.65885925292969
		 107.24 168.36380004882813 108.272 168.07736206054687 109.308 167.79716491699219 110.34 167.53013610839844
		 111.376 167.27973937988281 112.412 167.05337524414062 113.444 166.85317993164062
		 114.48 166.68228149414062 115.512 166.5489501953125 116.548 166.4541015625 117.584 166.40220642089844
		 118.616 166.39529418945312 119.652 166.43186950683594 120.688 166.51570129394531
		 121.72 166.64306640625 122.756 166.8116455078125 123.788 167.01791381835937 124.824 167.25730895996094
		 125.86 167.52394104003906 126.892 167.81001281738281 127.928 168.10568237304688 128.964 168.42024230957031
		 129.996 168.74362182617187 131.032 169.059326171875 132.064 169.36195373535156 133.1 169.650634765625
		 134.136 169.9190673828125 135.168 170.1617431640625 136.204 170.38069152832031 137.236 170.56988525390625
		 138.272 170.73222351074219 139.308 170.86665344238281 140.34 170.97479248046875 141.376 171.05935668945312
		 142.412 171.12278747558594 143.444 171.16986083984375 144.48 171.20137023925781 145.512 171.22502136230469
		 146.548 171.23936462402344 147.584 171.25018310546875 148.616 171.26211547851563
		 149.652 171.2808837890625 150.688 171.30459594726562 151.72 171.3255615234375 152.756 171.34912109375
		 153.788 171.37593078613281 154.824 171.41464233398437 155.86 171.4676513671875 156.892 171.53648376464844
		 157.928 171.61918640136719 158.964 171.71694946289062 159.996 171.82814025878906
		 161.032 171.9552001953125 162.064 172.09565734863281 163.1 172.24874877929687 164.136 172.40904235839844
		 165.168 172.57612609863281 166.204 172.74969482421875 167.236 172.92323303222656
		 168.272 173.09445190429688 169.308 173.25694274902344 170.34 173.41361999511719 171.376 173.5584716796875
		 172.412 173.68698120117187 173.444 173.80128479003906 174.48 173.89706420898437 175.512 173.97303771972656
		 176.548 174.02801513671875 177.584 174.06114196777344 178.616 174.0704345703125 179.652 174.05705261230469
		 180.688 174.01986694335937 181.72 173.95909118652344 182.756 173.87533569335937 183.788 173.76899719238281
		 184.824 173.64198303222656 185.86 173.49505615234375 186.892 173.33078002929687 187.928 173.15089416503906
		 188.964 172.957763671875 189.996 172.75360107421875 191.032 172.54093933105469 192.064 172.32211303710937
		 193.1 172.09970092773437 194.136 171.87544250488281 195.168 171.65159606933594 196.204 171.42973327636719
		 197.236 171.21304321289062 198.272 171.00103759765625 199.308 170.796630859375 200.34 170.60089111328125
		 201.376 170.41792297363281 202.412 170.24497985839844 203.444 170.08580017089844
		 204.48 169.94117736816406 205.512 169.8160400390625 206.548 169.70558166503906 207.584 169.61756896972656
		 208.616 169.55970764160156 209.652 169.48757934570312 210.688 169.46188354492187
		 211.72 169.4696044921875 212.756 169.4937744140625 213.788 169.54695129394531 214.824 169.6243896484375
		 215.86 169.730224609375 216.892 169.86434936523437 217.928 170.02590942382812 218.964 170.2137451171875
		 219.996 170.42686462402344 221.032 170.66212463378906 222.064 170.91740417480469
		 223.1 171.18792724609375 224.136 171.47244262695312 225.168 171.76567077636719 226.204 172.06430053710937
		 227.236 172.36503601074219 228.272 172.66160583496094 229.308 172.95640563964844
		 230.34 173.24386596679687 231.376 173.52250671386719 232.412 173.79144287109375 233.444 174.05039978027344
		 234.48 174.296142578125 235.512 174.53083801269531 236.548 174.75700378417969 237.584 174.9696044921875
		 238.616 175.17146301269531 239.652 175.36492919921875 240.688 175.54487609863281
		 241.72 175.71347045898437 242.756 175.86909484863281 243.788 176.01019287109375 244.824 176.13494873046875
		 245.86 176.24143981933594 246.892 176.32490539550781 247.928 176.37861633300781 248.96 176.441650390625
		 249.996 176.44874572753906 251.032 176.42153930664062 252.064 176.38351440429687
		 253.1 176.31547546386719 254.136 176.22242736816406 255.168 176.10488891601562 256.204 175.96922302246094
		 257.236 175.81707763671875 258.272 175.6549072265625;
	setAttr ".ktv[250:290]" 259.308 175.487060546875 260.34 175.31900024414062
		 261.376 175.15599060058594 262.412 175.00283813476562 263.444 174.86506652832031
		 264.48 174.74472045898437 265.512 174.64556884765625 266.548 174.56890869140625 267.584 174.51693725585937
		 268.616 174.48690795898437 269.652 174.47880554199219 270.688 174.49014282226562
		 271.72 174.51455688476562 272.756 174.53904724121094 273.788 174.6072998046875 274.824 174.68606567382812
		 275.86 174.7259521484375 276.892 174.77558898925781 277.928 174.81361389160156 278.96 174.877685546875
		 279.996 174.92005920410156 281.032 174.95033264160156 282.064 174.97201538085937
		 283.1 174.98583984375 284.136 174.98760986328125 285.168 174.98580932617187 286.204 174.97637939453125
		 287.236 174.96022033691406 288.272 174.93775939941406 289.308 174.90574645996094
		 290.34 174.85400390625 291.376 174.82298278808594 292.412 174.78558349609375 293.444 174.67514038085937
		 294.48 174.61090087890625 295.512 174.51985168457031 296.548 174.409912109375 297.584 174.28569030761719
		 298.616 174.14823913574219 299.652 173.99957275390625 300.688 173.84271240234375;
createNode animCurveTA -n "Bip01_L_UpperArm_rotateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 291 ".ktv";
	setAttr ".ktv[0:249]"  0.688 -164.45733642578125 1.72 -164.22474670410156
		 2.756 -164.34803771972656 3.788 -164.594482421875 4.824 -164.9378662109375 5.86 -165.50079345703125
		 6.892 -166.2579345703125 7.928 -167.11940002441406 8.964 -168.02581787109375 9.996 -168.81599426269531
		 11.032 -169.33140563964844 12.064 -169.81782531738281 13.1 -170.62872314453125 14.136 -171.6529541015625
		 15.168 -172.75364685058594 16.204 -174.49848937988281 17.24 -177.73689270019531 18.272 -181.95344543457031
		 19.308 -185.54881286621094 20.34 -187.90846252441406 21.376 -189.4945068359375 22.412 -190.77017211914062
		 23.444 -191.917236328125 24.48 -193.03477478027344 25.512 -194.15342712402344 26.548 -195.22505187988281
		 27.584 -196.30899047851563 28.616 -197.52615356445312 29.652 -198.75981140136719
		 30.688 -199.87435913085937 31.72 -200.98019409179687 32.756 -202.14584350585937 33.788 -203.25686645507812
		 34.824 -204.21940612792969 35.86 -205.090576171875 36.892 -205.97561645507812 37.928 -206.80221557617187
		 38.964 -207.4031982421875 39.996 -207.74104309082031 41.032 -207.7996826171875 42.064 -207.45265197753906
		 43.1 -206.69625854492187 44.136 -205.7171630859375 45.168 -204.59706115722656 46.204 -203.38206481933594
		 47.24 -202.48753356933594 48.272 -202.35533142089844 49.308 -202.32261657714844 50.34 -200.96485900878906
		 51.376 -198.15522766113281 52.412 -194.99163818359375 53.444 -192.22731018066406
		 54.48 -189.9564208984375 55.512 -188.08073425292969 56.548 -186.33523559570312 57.584 -184.29844665527344
		 58.616 -182.32012939453125 59.652 -181.1845703125 60.688 -180.92581176757812 61.72 -181.18255615234375
		 62.756 -181.54307556152344 63.788 -181.44468688964844 64.824 -180.8382568359375 65.86 -180.27967834472656
		 66.892 -180.06439208984375 67.928 -179.91914367675781 68.964 -179.62315368652344
		 69.996 -179.33396911621094 71.032 -179.18974304199219 72.064 -179.28074645996094
		 73.1 -179.656982421875 74.136 -180.00144958496094 75.168 -180.033935546875 76.204 -179.983642578125
		 77.24 -180.11517333984375 78.272 -180.44303894042969 79.308 -180.90757751464844 80.34 -181.37344360351562
		 81.376 -181.73287963867187 82.412 -182.01620483398438 83.444 -182.21702575683594
		 84.48 -182.32859802246094 85.512 -182.51078796386719 86.548 -182.66996765136719 87.584 -182.43345642089844
		 88.616 -181.919189453125 89.652 -181.39385986328125 90.688 -180.69857788085937 91.72 -179.76669311523437
		 92.756 -178.61639404296875 93.788 -177.24534606933594 94.824 -176.01173400878906
		 95.86 -175.23651123046875 96.892 -174.78822326660156 97.928 -174.45942687988281 98.964 -174.23796081542969
		 99.996 -174.13136291503906 101.032 -174.08041381835937 102.064 -174.14701843261719
		 103.1 -174.37214660644531 104.136 -174.59266662597656 105.168 -174.71876525878906
		 106.204 -174.79634094238281 107.24 -174.88539123535156 108.272 -175.05754089355469
		 109.308 -175.31730651855469 110.34 -175.55807495117187 111.376 -175.7117919921875
		 112.412 -175.9200439453125 113.444 -176.25225830078125 114.48 -176.44834899902344
		 115.512 -176.32554626464844 116.548 -176.01603698730469 117.584 -175.72860717773437
		 118.616 -175.56809997558594 119.652 -175.54229736328125 120.688 -175.54830932617187
		 121.72 -175.28907775878906 122.756 -174.74887084960937 123.788 -174.47496032714844
		 124.824 -174.77388000488281 125.86 -175.33030700683594 126.892 -175.68666076660156
		 127.928 -175.50442504882812 128.964 -174.64839172363281 129.996 -173.57127380371094
		 131.032 -172.93016052246094 132.064 -172.61216735839844 133.1 -172.2347412109375
		 134.136 -171.99612426757812 135.168 -172.0223388671875 136.204 -171.97286987304687
		 137.236 -171.72125244140625 138.272 -171.39042663574219 139.308 -170.99079895019531
		 140.34 -170.57603454589844 141.376 -170.20510864257812 142.412 -169.81187438964844
		 143.444 -169.3765869140625 144.48 -168.88046264648438 145.512 -168.30760192871094
		 146.548 -167.78167724609375 147.584 -167.4287109375 148.616 -167.43394470214844 149.652 -167.91419982910156
		 150.688 -168.53715515136719 151.72 -168.83302307128906 152.756 -168.69419860839844
		 153.788 -168.352294921875 154.824 -167.98345947265625 155.86 -167.61476135253906
		 156.892 -167.33236694335937 157.928 -167.11553955078125 158.964 -166.77799987792969
		 159.996 -166.26397705078125 161.032 -165.78631591796875 162.064 -165.60728454589844
		 163.1 -165.65702819824219 164.136 -165.71409606933594 165.168 -165.8013916015625
		 166.204 -165.89434814453125 167.236 -165.80137634277344 168.272 -165.65025329589844
		 169.308 -165.79608154296875 170.34 -166.22817993164062 171.376 -166.64137268066406
		 172.412 -166.93434143066406 173.444 -167.19686889648437 174.48 -167.53996276855469
		 175.512 -168.09996032714844 176.548 -168.8250732421875 177.584 -169.44889831542969
		 178.616 -169.99761962890625 179.652 -170.71583557128906 180.688 -171.42330932617187
		 181.72 -171.89106750488281 182.756 -172.4278564453125 183.788 -173.16714477539062
		 184.824 -173.69602966308594 185.86 -173.86665344238281 186.892 -173.95346069335937
		 187.928 -174.11749267578125 188.964 -174.42143249511719 189.996 -174.85601806640625
		 191.032 -175.25967407226562 192.064 -175.58280944824219 193.1 -175.80136108398437
		 194.136 -175.84010314941406 195.168 -175.8173828125 196.204 -175.77577209472656 197.236 -175.52001953125
		 198.272 -175.00018310546875 199.308 -174.31657409667969 200.34 -173.58331298828125
		 201.376 -172.97148132324219 202.412 -172.50372314453125 203.444 -171.94660949707031
		 204.48 -171.17082214355469 205.512 -170.44053649902344 206.548 -170.0220947265625
		 207.584 -169.767822265625 208.616 -169.41705322265625 209.652 -168.89952087402344
		 210.688 -168.28953552246094 211.72 -167.71505737304687 212.756 -167.21067810058594
		 213.788 -166.73756408691406 214.824 -166.32450866699219 215.86 -166.00660705566406
		 216.892 -165.71038818359375 217.928 -165.34123229980469 218.964 -164.95108032226562
		 219.996 -164.63180541992187 221.032 -164.35459899902344 222.064 -164.09477233886719
		 223.1 -163.86482238769531 224.136 -163.59796142578125 225.168 -163.30947875976562
		 226.204 -163.19004821777344 227.236 -163.2867431640625 228.272 -163.49053955078125
		 229.308 -163.81759643554687 230.34 -164.15110778808594 231.376 -164.27238464355469
		 232.412 -164.30477905273437 233.444 -164.48457336425781 234.48 -164.89271545410156
		 235.512 -165.33914184570312 236.548 -165.52752685546875 237.584 -165.53352355957031
		 238.616 -165.51823425292969 239.652 -165.50030517578125 240.688 -165.52461242675781
		 241.72 -165.5654296875 242.756 -165.62689208984375 243.788 -165.82192993164063 244.824 -166.13496398925781
		 245.86 -166.38555908203125 246.892 -166.52120971679687 247.928 -166.60572814941406
		 248.96 -166.56027221679687 249.996 -166.26446533203125 251.032 -165.82933044433594
		 252.064 -165.51197814941406 253.1 -165.42172241210937 254.136 -165.46974182128906
		 255.168 -165.545166015625 256.204 -165.61970520019531 257.236 -165.63655090332031
		 258.272 -165.47805786132812;
	setAttr ".ktv[250:290]" 259.308 -165.27938842773437 260.34 -165.27578735351562
		 261.376 -165.29380798339844 262.412 -165.14898681640625 263.444 -164.996337890625
		 264.48 -164.89802551269531 265.512 -164.77510070800781 266.548 -164.57192993164062
		 267.584 -164.21530151367187 268.616 -163.78053283691406 269.652 -163.38973999023437
		 270.688 -162.96728515625 271.72 -162.51974487304688 272.756 -162.23231506347656 273.788 -162.08244323730469
		 274.824 -161.84268188476562 275.86 -161.48027038574219 276.892 -161.12017822265625
		 277.928 -160.84512329101562 278.96 -160.71841430664062 279.996 -160.69664001464844
		 281.032 -160.66368103027344 282.064 -160.57502746582031 283.1 -160.36355590820313
		 284.136 -159.99580383300781 285.168 -159.73551940917969 286.204 -159.867431640625
		 287.236 -160.22518920898437 288.272 -160.4287109375 289.308 -160.4010009765625 290.34 -160.38645935058594
		 291.376 -160.53364562988281 292.412 -160.63600158691406 293.444 -160.4981689453125
		 294.48 -160.27024841308594 295.512 -160.15069580078125 296.548 -160.12338256835937
		 297.584 -160.04023742675781 298.616 -159.89596557617187 299.652 -159.86079406738281
		 300.688 -159.82769775390625;
createNode animCurveTA -n "Bip01_L_UpperArm_rotateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 291 ".ktv";
	setAttr ".ktv[0:249]"  0.688 113.54287719726562 1.72 113.73574066162109 2.756 113.90470123291016
		 3.788 113.95838165283203 4.824 113.88296508789062 5.86 113.74186706542969 6.892 113.57545471191406
		 7.928 113.38751220703125 8.964 113.17997741699219 9.996 112.94411468505859 11.032 112.68408203125
		 12.064 112.4271240234375 13.1 112.23045349121094 14.136 112.21880340576172 15.168 112.45052337646484
		 16.204 112.90533447265625 17.24 113.63570404052734 18.272 114.75629425048828 19.308 116.18826293945312
		 20.34 117.5305709838867 21.376 118.37211608886717 22.412 118.70870971679687 23.444 118.81427764892578
		 24.48 118.80209350585937 25.512 118.63996887207031 26.548 118.37007141113281 27.584 118.10721588134767
		 28.616 117.92790985107422 29.652 117.76830291748047 30.688 117.51422119140625 31.72 117.2138671875
		 32.756 116.98725891113281 33.788 116.81789398193359 34.824 116.64394378662108 35.86 116.47837829589844
		 36.892 116.34455871582031 37.928 116.24045562744139 38.964 116.17476654052734 39.996 116.18196105957031
		 41.032 116.26986694335939 42.064 116.40196228027344 43.1 116.55850982666017 44.136 116.7786102294922
		 45.168 117.08260345458984 46.204 117.31563568115234 47.24 117.19541168212892 48.272 116.68944549560547
		 49.308 116.3131561279297 50.34 116.50488281249999 51.376 116.89603424072264 52.412 116.79764556884766
		 53.444 115.90368652343751 54.48 114.42638397216797 55.512 112.84719085693359 56.548 111.58615875244141
		 57.584 110.88837432861328 58.616 110.69967651367187 59.652 110.70343017578125 60.688 110.68557739257813
		 61.72 110.64434814453125 62.756 110.62728118896484 63.788 110.68726348876953 64.824 110.85499572753906
		 65.86 111.10034942626953 66.892 111.36903381347656 67.928 111.63372802734375 68.964 111.89002990722656
		 69.996 112.1292724609375 71.032 112.3280029296875 72.064 112.47149658203125 73.1 112.59378051757813
		 74.136 112.75691986083984 75.168 112.95076751708984 76.204 113.12032318115234 77.24 113.2557373046875
		 78.272 113.35104370117187 79.308 113.37363433837891 80.34 113.34651947021484 81.376 113.34086608886719
		 82.412 113.35739898681641 83.444 113.32202911376953 84.48 113.23651885986328 85.512 113.23041534423828
		 86.548 113.36497497558594 87.584 113.55400085449219 88.616 113.73193359375 89.652 113.88528442382812
		 90.688 114.00328063964844 91.72 114.08023834228516 92.756 114.09368896484375 93.788 114.04369354248047
		 94.824 113.97940063476562 95.86 113.95555877685547 96.892 114.00373840332031 97.928 114.08861541748047
		 98.964 114.13004302978516 99.996 114.12006378173828 101.032 114.13550567626953 102.064 114.2020263671875
		 103.1 114.25800323486328 104.136 114.28331756591797 105.168 114.32038116455078 106.204 114.36573028564453
		 107.24 114.38993072509766 108.272 114.39703369140625 109.308 114.40052795410156 110.34 114.4066162109375
		 111.376 114.40449523925781 112.412 114.38413238525391 113.444 114.35920715332031
		 114.48 114.33594512939453 115.512 114.31377410888672 116.548 114.3076171875 117.584 114.30701446533203
		 118.616 114.27920532226562 119.652 114.23466491699219 120.688 114.22053527832031
		 121.72 114.26963043212891 122.756 114.36563873291016 123.788 114.40562438964844 124.824 114.32646942138672
		 125.86 114.23011779785156 126.892 114.17859649658203 127.928 114.11827850341797 128.964 114.07109832763672
		 129.996 114.09990692138672 131.032 114.17481994628906 132.064 114.26001739501953
		 133.1 114.34706878662109 134.136 114.40869140625 135.168 114.42201995849609 136.204 114.3919677734375
		 137.236 114.3399658203125 138.272 114.29426574707031 139.308 114.28722381591797 140.34 114.31394958496094
		 141.376 114.34027099609375 142.412 114.36290740966797 143.444 114.36676788330078
		 144.48 114.31317138671875 145.512 114.23729705810547 146.548 114.23701477050781 147.584 114.36083984375
		 148.616 114.58309173583984 149.652 114.83811187744142 150.688 115.0438919067383 151.72 115.16616058349608
		 152.756 115.23683929443359 153.788 115.29387664794923 154.824 115.34954071044922
		 155.86 115.37179565429689 156.892 115.29980468749999 157.928 115.10797119140625 158.964 114.8690185546875
		 159.996 114.71030426025389 161.032 114.70199584960937 162.064 114.85055541992187
		 163.1 115.10126495361327 164.136 115.35804748535156 165.168 115.60239410400389 166.204 115.85225677490234
		 167.236 116.02439117431639 168.272 115.99461364746094 169.308 115.77024841308592
		 170.34 115.51174163818359 171.376 115.33570098876953 172.412 115.22476959228517 173.444 115.13275146484374
		 174.48 115.02227783203124 175.512 114.8878173828125 176.548 114.78921508789062 177.584 114.76123046875001
		 178.616 114.75891876220703 179.652 114.72523498535156 180.688 114.62685394287111
		 181.72 114.48816680908203 182.756 114.38893890380859 183.788 114.33151245117187 184.824 114.23179626464844
		 185.86 114.0826416015625 186.892 113.93059539794922 187.928 113.76533508300781 188.964 113.59580993652344
		 189.996 113.46910095214844 191.032 113.38840484619141 192.064 113.32845306396484
		 193.1 113.28598022460937 194.136 113.28133392333984 195.168 113.31990051269531 196.204 113.37047576904297
		 197.236 113.40147399902344 198.272 113.42669677734375 199.308 113.48174285888672
		 200.34 113.55250549316406 201.376 113.58135223388672 202.412 113.55998229980469 203.444 113.53569793701172
		 204.48 113.54104614257812 205.512 113.6036376953125 206.548 113.72463989257813 207.584 113.84088134765625
		 208.616 113.91420745849609 209.652 113.99022674560547 210.688 114.0943603515625 211.72 114.18424987792969
		 212.756 114.23938751220703 213.788 114.28805541992187 214.824 114.33927154541016
		 215.86 114.38865661621094 216.892 114.45573425292969 217.928 114.53684997558594 218.964 114.59283447265626
		 219.996 114.61392974853516 221.032 114.62827301025392 222.064 114.63881683349611
		 223.1 114.62229156494142 224.136 114.57898712158203 225.168 114.53215789794922 226.204 114.50775146484375
		 227.236 114.48496246337891 228.272 114.40324401855469 229.308 114.27902221679687
		 230.34 114.17989349365234 231.376 114.11248016357422 232.412 114.01907348632812 233.444 113.87168121337891
		 234.48 113.71865081787109 235.512 113.58893585205078 236.548 113.5277099609375 237.584 113.68313598632812
		 238.616 114.12723541259766 239.652 114.7293167114258 240.688 115.34115600585936 241.72 115.89144897460937
		 242.756 116.27648925781251 243.788 116.43629455566406 244.824 116.44974517822266
		 245.86 116.40644073486327 246.892 116.23134613037109 247.928 115.69797515869139 248.96 114.73954772949217
		 249.996 113.56491088867187 251.032 112.37272644042969 252.064 111.32024383544922
		 253.1 110.61367034912109 254.136 110.25232696533203 255.168 110.06737518310547 256.204 109.99533081054687
		 257.236 110.01801300048828 258.272 110.09378051757812;
	setAttr ".ktv[250:290]" 259.308 110.20240020751953 260.34 110.33912658691406
		 261.376 110.48837280273437 262.412 110.63606262207031 263.444 110.79082489013672
		 264.48 110.95774841308594 265.512 111.12722015380859 266.548 111.29093933105469 267.584 111.44239044189453
		 268.616 111.58516693115234 269.652 111.73103332519531 270.688 111.88408660888672
		 271.72 112.03584289550781 272.756 112.1649169921875 273.788 112.27336120605469 274.824 112.3756103515625
		 275.86 112.46070098876953 276.892 112.53171539306641 277.928 112.60074615478516 278.96 112.66002655029297
		 279.996 112.71604156494141 281.032 112.77839660644531 282.064 112.82621765136719
		 283.1 112.86241149902344 284.136 112.9287109375 285.168 112.99584197998047 286.204 112.98935699462891
		 287.236 112.94013977050781 288.272 112.92275238037109 289.308 112.94352722167969
		 290.34 112.97942352294922 291.376 113.01669311523437 292.412 113.05099487304687 293.444 113.08177185058594
		 294.48 113.12107086181641 295.512 113.18325042724609 296.548 113.24398040771484 297.584 113.27899932861328
		 298.616 113.30956268310547 299.652 113.35138702392578 300.688 113.40702056884766;
createNode animCurveTA -n "Bip01_L_UpperArm_rotateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 291 ".ktv";
	setAttr ".ktv[0:249]"  0.688 -178.14175415039062 1.72 -177.63682556152344
		 2.756 -177.5079345703125 3.788 -177.51760864257812 4.824 -177.64698791503906 5.86 -178.01800537109375
		 6.892 -178.56980895996094 7.928 -179.18539428710937 8.964 -179.82147216796875 9.996 -180.36282348632812
		 11.032 -180.66287231445312 12.064 -180.89419555664062 13.1 -181.35285949707031 14.136 -182.048828125
		 15.168 -182.97801208496094 16.204 -184.41116333007812 17.24 -186.50102233886719 18.272 -188.64497375488281
		 19.308 -190.10618591308594 20.34 -190.79147338867187 21.376 -190.99189758300781 22.412 -191.02287292480469
		 23.444 -191.00076293945312 24.48 -191.02462768554687 25.512 -191.16746520996094 26.548 -191.3282470703125
		 27.584 -191.53355407714844 28.616 -191.93252563476562 29.652 -192.38774108886719
		 30.688 -192.7589111328125 31.72 -193.1923828125 32.756 -193.73216247558594 33.788 -194.22325134277344
		 34.824 -194.56707763671875 35.86 -194.85466003417969 36.892 -195.24807739257812 37.928 -195.65362548828125
		 38.964 -195.837890625 39.996 -195.78309631347656 41.032 -195.51493835449219 42.064 -194.84564208984375
		 43.1 -193.73748779296875 44.136 -192.4637451171875 45.168 -191.17021179199219 46.204 -190.03047180175781
		 47.24 -189.40611267089844 48.272 -189.26176452636719 49.308 -189.18449401855469 50.34 -188.74488830566406
		 51.376 -187.89712524414062 52.412 -187.15879821777344 53.444 -187.08457946777344
		 54.48 -187.68986511230469 55.512 -188.7528076171875 56.548 -190.13101196289062 57.584 -191.61610412597656
		 58.616 -193.14445495605469 59.652 -194.63772583007813 60.688 -196.036865234375 61.72 -197.58732604980469
		 62.756 -199.26445007324219 63.788 -200.51927185058594 64.824 -201.16551208496094
		 65.86 -201.76632690429687 66.892 -202.70399475097656 67.928 -203.58831787109375 68.964 -204.08781433105469
		 69.996 -204.43446350097656 71.032 -204.84773254394531 72.064 -205.48867797851562
		 73.1 -206.44273376464844 74.136 -207.23973083496094 75.168 -207.4849853515625 76.204 -207.55271911621094
		 77.24 -207.85374450683594 78.272 -208.42449951171875 79.308 -209.20292663574219 80.34 -209.99253845214844
		 81.376 -210.62127685546875 82.412 -211.14328002929687 83.444 -211.57148742675781
		 84.48 -211.8955078125 85.512 -212.32676696777344 86.548 -212.72071838378906 87.584 -212.54153442382812
		 88.616 -211.97329711914062 89.652 -211.41267395019531 90.688 -210.62196350097656
		 91.72 -209.50282287597656 92.756 -208.08908081054687 93.788 -206.37953186035156 94.824 -204.89515686035156
		 95.86 -204.08998107910156 96.892 -203.74751281738281 97.928 -203.56489562988281 98.964 -203.55120849609375
		 99.996 -203.7220458984375 101.032 -203.96023559570312 102.064 -204.35055541992187
		 103.1 -204.98123168945312 104.136 -205.61279296875 105.168 -206.09475708007812 106.204 -206.49955749511719
		 107.24 -206.92535400390625 108.272 -207.47796630859375 109.308 -208.160400390625
		 110.34 -208.80776977539062 111.376 -209.31497192382812 112.412 -209.89569091796875
		 113.444 -210.649658203125 114.48 -211.1737060546875 115.512 -211.17863464355469 116.548 -210.85858154296875
		 117.584 -210.53718566894531 118.616 -210.37269592285156 119.652 -210.36070251464844
		 120.688 -210.33651733398438 121.72 -209.84562683105469 122.756 -208.86581420898437
		 123.788 -208.25306701660156 124.824 -208.49276733398438 125.86 -209.08326721191406
		 126.892 -209.32106018066406 127.928 -208.71722412109375 128.964 -207.07569885253906
		 129.996 -205.07339477539062 131.032 -203.71284484863281 132.064 -202.84248352050781
		 133.1 -201.90351867675781 134.136 -201.22335815429687 135.168 -201.01481628417969
		 136.204 -200.77510070800781 137.236 -200.31661987304688 138.272 -199.82060241699219
		 139.308 -199.29219055175781 140.34 -198.81071472167969 141.376 -198.47550964355469
		 142.412 -198.18766784667969 143.444 -197.92225646972656 144.48 -197.66197204589844
		 145.512 -197.35115051269531 146.548 -197.1068115234375 147.584 -197.07565307617187
		 148.616 -197.53622436523437 149.652 -198.68856811523437 150.688 -200.06352233886719
		 151.72 -200.949951171875 152.756 -201.147216796875 153.788 -200.97187805175781 154.824 -200.6802978515625
		 155.86 -200.32759094238281 156.892 -200.06980895996094 157.928 -199.88626098632812
		 158.964 -199.4652099609375 159.996 -198.67135620117187 161.032 -197.79365539550781
		 162.064 -197.21968078613281 163.1 -196.87371826171875 164.136 -196.47999572753906
		 165.168 -196.08868408203125 166.204 -195.65617370605469 167.236 -194.93965148925781
		 168.272 -194.20683288574219 169.308 -194.00215148925781 170.34 -194.22065734863281
		 171.376 -194.34133911132812 172.412 -194.22645568847656 173.444 -194.037353515625
		 174.48 -193.96286010742187 175.512 -194.21311950683594 176.548 -194.68496704101562
		 177.584 -194.97163391113281 178.616 -195.14018249511719 179.652 -195.58854675292969
		 180.688 -196.07319641113281 181.72 -196.24848937988281 182.756 -196.54049682617187
		 183.788 -197.15560913085937 184.824 -197.53439331054687 185.86 -197.4716796875 186.892 -197.35997009277344
		 187.928 -197.44940185546875 188.964 -197.83393859863281 189.996 -198.48353576660156
		 191.032 -199.16455078125 192.064 -199.82003784179687 193.1 -200.41899108886719 194.136 -200.8382568359375
		 195.168 -201.24256896972656 196.204 -201.69914245605469 197.236 -201.92301940917969
		 198.272 -201.82499694824219 199.308 -201.52735900878906 200.34 -201.19010925292969
		 201.376 -201.0755615234375 202.412 -201.20806884765625 203.444 -201.21385192871094
		 204.48 -200.88125610351562 205.512 -200.57627868652344 206.548 -200.6802978515625
		 207.584 -201.00132751464844 208.616 -201.16816711425781 209.652 -201.05174255371094
		 210.688 -200.74832153320312 211.72 -200.46408081054687 212.756 -200.25520324707031
		 213.788 -200.05043029785156 214.824 -199.8897705078125 215.86 -199.82669067382812
		 216.892 -199.74101257324219 217.928 -199.48908996582031 218.964 -199.16102600097656
		 219.996 -198.89408874511719 221.032 -198.62591552734375 222.064 -198.31101989746094
		 223.1 -197.97285461425781 224.136 -197.50454711914062 225.168 -196.90948486328125
		 226.204 -196.45330810546875 227.236 -196.21134948730469 228.272 -196.04658508300781
		 229.308 -195.97074890136719 230.34 -195.77838134765625 231.376 -195.15263366699219
		 232.412 -194.31405639648437 233.444 -193.63204956054687 234.48 -193.21005249023437
		 235.512 -192.77157592773437 236.548 -191.8795166015625 237.584 -190.56781005859375
		 238.616 -189.02847290039062 239.652 -187.37886047363281 240.688 -185.79415893554687
		 241.72 -184.30038452148437 242.756 -183.00141906738281 243.788 -182.13250732421875
		 244.824 -181.6324462890625 245.86 -181.18589782714844 246.892 -180.7781982421875
		 247.928 -180.67813110351562 248.96 -180.8270263671875 249.996 -180.89891052246094
		 251.032 -180.90583801269531 252.064 -181.08135986328125 253.1 -181.41429138183594
		 254.136 -181.77880859375 255.168 -182.14424133300781 256.204 -182.51564025878906
		 257.236 -182.82569885253906 258.272 -182.940185546875;
	setAttr ".ktv[250:290]" 259.308 -183.04338073730469 260.34 -183.44462585449219
		 261.376 -183.91754150390625 262.412 -184.20774841308594 263.444 -184.4976806640625
		 264.48 -184.86526489257812 265.512 -185.20925903320312 266.548 -185.4423828125 267.584 -185.45040893554687
		 268.616 -185.32907104492188 269.652 -185.23577880859375 270.688 -185.05691528320312
		 271.72 -184.80140686035156 272.756 -184.73602294921875 273.788 -184.82774353027344
		 274.824 -184.75347900390625 275.86 -184.47605895996094 276.892 -184.167724609375
		 277.928 -183.93745422363281 278.96 -183.8818359375 279.996 -183.94186401367187 281.032 -183.95552062988281
		 282.064 -183.878662109375 283.1 -183.62236022949219 284.136 -183.11930847167969 285.168 -182.751708984375
		 286.204 -182.96186828613281 287.236 -183.50889587402344 288.272 -183.82351684570312
		 289.308 -183.79283142089844 290.34 -183.77156066894531 291.376 -183.97798156738281
		 292.412 -184.12850952148437 293.444 -183.95132446289062 294.48 -183.6478271484375
		 295.512 -183.48551940917969 296.548 -183.46153259277344 297.584 -183.38868713378906
		 298.616 -183.24571228027344 299.652 -183.26040649414062 300.688 -183.28309631347656;
createNode animCurveTA -n "Bip01_L_Forearm_rotateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 291 ".ktv";
	setAttr ".ktv[0:249]"  0.688 -2.163424015045166 1.72 -2.1625440120697021
		 2.756 -2.1636481285095215 3.788 -2.1667084693908691 4.824 -2.1714472770690918 5.86 -2.1769177913665771
		 6.892 -2.181504487991333 7.928 -2.1843867301940918 8.964 -2.1863384246826172 9.996 -2.1886956691741943
		 11.032 -2.191669225692749 12.064 -2.1935603618621826 13.1 -2.1920475959777832 14.136 -2.1862373352050781
		 15.168 -2.1768779754638672 16.204 -2.167438268661499 17.24 -2.163109302520752 18.272 -2.1643478870391846
		 19.308 -2.1662161350250244 20.34 -2.1649258136749268 21.376 -2.1607232093811035 22.412 -2.1555273532867432
		 23.444 -2.149632453918457 24.48 -2.1430819034576416 25.512 -2.1368374824523926 26.548 -2.131453275680542
		 27.584 -2.1277830600738525 28.616 -2.1268575191497803 29.652 -2.1281063556671143
		 30.688 -2.130180835723877 31.72 -2.1329131126403809 32.756 -2.1364390850067139 33.788 -2.1395554542541504
		 34.824 -2.1410868167877197 35.86 -2.1418886184692383 36.892 -2.1439709663391113 37.928 -2.1477601528167725
		 38.964 -2.1527407169342041 39.996 -2.1591696739196777 41.032 -2.1668236255645752
		 42.064 -2.1739981174468994 43.1 -2.1786487102508545 44.136 -2.181394100189209 45.168 -2.1837725639343262
		 46.204 -2.1833429336547852 47.24 -2.1781418323516846 48.272 -2.1718323230743408 49.308 -2.1676521301269531
		 50.34 -2.1638195514678955 51.376 -2.1591963768005371 52.412 -2.1538021564483643 53.444 -2.1482985019683838
		 54.48 -2.1431725025177002 55.512 -2.1339421272277832 56.548 -2.1172552108764648 57.584 -2.0955855846405029
		 58.616 -2.0718393325805664 59.652 -2.0509088039398193 60.688 -2.0363657474517822
		 61.72 -2.024155855178833 62.756 -2.0118262767791748 63.788 -2.002727746963501 64.824 -1.996549129486084
		 65.86 -1.987459659576416 66.892 -1.9742244482040405 67.928 -1.9642480611801147 68.964 -1.9610246419906618
		 69.996 -1.958546996116638 71.032 -1.9531949758529663 72.064 -1.9450031518936157 73.1 -1.9334098100662234
		 74.136 -1.922914385795593 75.168 -1.9187327623367307 76.204 -1.9172431230545044 77.24 -1.9129723310470581
		 78.272 -1.9056692123413086 79.308 -1.8962578773498537 80.34 -1.8865512609481812 81.376 -1.8792370557785032
		 82.412 -1.8736867904663084 83.444 -1.8680837154388428 84.48 -1.8625156879425049 85.512 -1.8571426868438721
		 86.548 -1.8563327789306638 87.584 -1.8645186424255371 88.616 -1.8729982376098631
		 89.652 -1.8780649900436404 90.688 -1.8903915882110596 91.72 -1.9095555543899538 92.756 -1.9280267953872681
		 93.788 -1.9469668865203857 94.824 -1.9657392501831057 95.86 -1.979712963104248 96.892 -1.9887394905090334
		 97.928 -1.9928851127624512 98.964 -1.9936462640762331 99.996 -1.9897202253341677
		 101.032 -1.9872368574142456 102.064 -1.9886378049850462 103.1 -1.9902222156524656
		 104.136 -1.9904576539993286 105.168 -1.9894801378250124 106.204 -1.9867497682571411
		 107.24 -1.9821276664733889 108.272 -1.9749249219894409 109.308 -1.9654062986373899
		 110.34 -1.9571385383605959 111.376 -1.9516235589981079 112.412 -1.9436881542205811
		 113.444 -1.9313023090362547 114.48 -1.9215092658996584 115.512 -1.9187349081039429
		 116.548 -1.9194911718368528 117.584 -1.9190318584442139 118.616 -1.916129469871521
		 119.652 -1.9118795394897461 120.688 -1.9082828760147093 121.72 -1.9111542701721194
		 122.756 -1.9201893806457522 123.788 -1.9222085475921633 124.824 -1.9104111194610596
		 125.86 -1.8933371305465698 126.892 -1.8818216323852539 127.928 -1.8824343681335449
		 128.964 -1.8982605934143066 129.996 -1.9206111431121824 131.032 -1.9368133544921873
		 132.064 -1.9493287801742554 133.1 -1.9634946584701536 134.136 -1.9728527069091795
		 135.168 -1.9739580154418948 136.204 -1.9724938869476318 137.236 -1.9716509580612183
		 138.272 -1.9726784229278567 139.308 -1.9780324697494509 140.34 -1.9865759611129763
		 141.376 -1.9949576854705811 142.412 -2.0004980564117432 143.444 -2.0001795291900635
		 144.48 -1.9941054582595825 145.512 -1.9883718490600584 146.548 -1.9886653423309326
		 147.584 -1.9913413524627686 148.616 -1.9852244853973389 149.652 -1.9649883508682251
		 150.688 -1.9372869729995725 151.72 -1.9139920473098755 152.756 -1.9018336534500122
		 153.788 -1.8971120119094849 154.824 -1.8939638137817381 155.86 -1.8908822536468506
		 156.892 -1.8918974399566648 157.928 -1.8782346248626709 158.964 -1.8650203943252566
		 159.996 -1.8615469932556155 161.032 -1.8675055503845215 162.064 -1.8820284605026245
		 163.1 -1.9025089740753174 164.136 -1.9167357683181763 165.168 -1.9214187860488894
		 166.204 -1.9287806749343872 167.236 -1.9425537586212158 168.272 -1.9521505832672119
		 169.308 -1.9518636465072632 170.34 -1.9467276334762573 171.376 -1.9429682493209839
		 172.412 -1.9429064989089966 173.444 -1.9457743167877199 174.48 -1.9633367061614992
		 175.512 -1.9622231721878052 176.548 -1.9561522006988528 177.584 -1.9507591724395752
		 178.616 -1.9472374916076658 179.652 -1.9419656991958618 180.688 -1.9401147365570066
		 181.72 -1.9454070329666138 182.756 -1.9478633403778076 183.788 -1.9428335428237917
		 184.824 -1.9404920339584351 185.86 -1.9458560943603516 186.892 -1.9521131515502927
		 187.928 -1.9570896625518797 188.964 -1.9614876508712769 189.996 -1.9616658687591555
		 191.032 -1.9568783044815063 192.064 -1.9495655298233032 193.1 -1.9433929920196533
		 194.136 -1.9421412944793703 195.168 -1.9443737268447876 196.204 -1.9476354122161867
		 197.236 -1.9525386095046997 198.272 -1.9584445953369141 199.308 -1.9641245603561399
		 200.34 -1.9680721759796143 201.376 -1.9669116735458372 202.412 -1.9612805843353274
		 203.444 -1.9571884870529175 204.48 -1.9584810733795166 205.512 -1.9620568752288818
		 206.548 -1.9633578062057495 207.584 -1.9637616872787476 208.616 -1.9666730165481565
		 209.652 -1.9718310832977295 210.688 -1.9777799844741821 211.72 -1.9836289882659912
		 212.756 -1.9894232749938967 213.788 -1.9950846433639529 214.824 -1.9982571601867676
		 215.86 -1.9972876310348511 216.892 -1.9949619770050047 217.928 -1.9946731328964236
		 218.964 -1.9955675601959229 219.996 -1.9961675405502319 221.032 -1.9973329305648804
		 222.064 -1.9975271224975588 223.1 -1.9945256710052488 224.136 -1.9918649196624754
		 225.168 -1.9928655624389648 226.204 -1.9940012693405151 227.236 -1.9930206537246704
		 228.272 -1.9924422502517698 229.308 -1.9918247461318972 230.34 -1.9910348653793333
		 231.376 -1.9927573204040527 232.412 -1.9952973127365112 233.444 -1.9960647821426392
		 234.48 -1.9953997135162351 235.512 -1.9960563182830811 236.548 -2.0020995140075684
		 237.584 -2.0128035545349121 238.616 -2.0245277881622314 239.652 -2.0356266498565674
		 240.688 -2.0463728904724121 241.72 -2.0577352046966553 242.756 -2.0684723854064941
		 243.788 -2.0763564109802246 244.824 -2.0814309120178223 245.86 -2.0851073265075684
		 246.892 -2.0863537788391113 247.928 -2.0819590091705322 248.96 -2.0729379653930664
		 249.996 -2.062673807144165 251.032 -2.0505878925323486 252.064 -2.0372095108032227
		 253.1 -2.0273067951202393 254.136 -2.0216720104217529 255.168 -2.0164787769317627
		 256.204 -2.0101771354675293 257.236 -2.0045571327209473 258.272 -2.0021460056304932;
	setAttr ".ktv[250:290]" 259.308 -2.0011379718780518 260.34 -1.9973818063735962
		 261.376 -1.9924217462539673 262.412 -1.9895110130310059 263.444 -1.9870481491088867
		 264.48 -1.9835355281829834 265.512 -1.9801656007766724 266.548 -1.9780591726303101
		 267.584 -1.9788883924484255 268.616 -1.9818371534347534 269.652 -1.9836385250091553
		 270.688 -1.9851802587509153 271.72 -1.9860891103744507 272.756 -1.9879845380783081
		 273.788 -1.987580418586731 274.824 -1.9891057014465332 275.86 -1.9942167997360229
		 276.892 -2.0003340244293213 277.928 -2.0052597522735596 278.96 -2.0081040859222412
		 279.996 -2.0095498561859131 281.032 -2.0104143619537354 282.064 -2.0107030868530273
		 283.1 -2.0042364597320557 284.136 -2.0089120864868164 285.168 -2.0145971775054932
		 286.204 -2.0164029598236084 287.236 -2.0145943164825439 288.272 -2.0132889747619629
		 289.308 -2.0136940479278564 290.34 -2.0133490562438965 291.376 -2.0108664035797119
		 292.412 -2.0091290473937988 293.444 -2.0105047225952148 294.48 -2.0161430835723877
		 295.512 -2.0211820602416992 296.548 -2.0274908542633057 297.584 -2.0333459377288818
		 298.616 -2.0359139442443848 299.652 -2.0329232215881348 300.688 -2.0282242298126221;
createNode animCurveTA -n "Bip01_L_Forearm_rotateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 291 ".ktv";
	setAttr ".ktv[0:249]"  0.688 -77.090522766113281 1.72 -78.054130554199219
		 2.756 -79.060310363769531 3.788 -79.685317993164062 4.824 -79.831382751464844 5.86 -79.689186096191406
		 6.892 -79.4427490234375 7.928 -79.215499877929688 8.964 -78.9896240234375 9.996 -78.657379150390625
		 11.032 -78.251785278320312 12.064 -77.869598388671875 13.1 -77.571044921875 14.136 -77.422615051269531
		 15.168 -77.43206787109375 16.204 -77.794273376464844 17.24 -79.043754577636719 18.272 -81.337677001953125
		 19.308 -84.136703491210937 20.34 -86.769790649414063 21.376 -88.719657897949219 22.412 -89.8985595703125
		 23.444 -90.670356750488281 24.48 -91.206069946289063 25.512 -91.457527160644531 26.548 -91.549674987792969
		 27.584 -91.589813232421875 28.616 -91.53192138671875 29.652 -91.433738708496094 30.688 -91.4132080078125
		 31.72 -91.390510559082031 32.756 -91.221763610839844 33.788 -90.950180053710938 34.824 -90.703933715820313
		 35.86 -90.404228210449219 36.892 -89.784660339355469 37.928 -88.825210571289063 38.964 -87.744400024414063
		 39.996 -86.563995361328125 41.032 -85.142860412597656 42.064 -83.286354064941406
		 43.1 -81.051239013671875 44.136 -79.045944213867188 45.168 -77.656784057617188 46.204 -76.519416809082031
		 47.24 -75.308982849121094 48.272 -74.309097290039063 49.308 -73.688568115234375 50.34 -73.205215454101563
		 51.376 -72.759361267089844 52.412 -72.170143127441406 53.444 -71.315673828125 54.48 -70.346038818359375
		 55.512 -69.040168762207031 56.548 -67.210350036621094 57.584 -65.10693359375 58.616 -63.00431823730468
		 59.652 -61.271156311035156 60.688 -60.044464111328125 61.72 -58.960414886474616 62.756 -57.893096923828125
		 63.788 -57.131824493408203 64.824 -56.705741882324219 65.86 -56.247406005859375 66.892 -55.584312438964844
		 67.928 -55.034866333007813 68.964 -54.814350128173828 69.996 -54.693073272705078
		 71.032 -54.470314025878906 72.064 -54.033969879150391 73.1 -53.302921295166016 74.136 -52.576045989990234
		 75.168 -52.221012115478516 76.204 -52.115406036376953 77.24 -51.946365356445312 78.272 -51.578460693359375
		 79.308 -50.967746734619141 80.34 -50.227817535400391 81.376 -49.581203460693359 82.412 -49.074832916259766
		 83.444 -48.651596069335938 84.48 -48.293231964111328 85.512 -47.955059051513672 86.548 -47.824169158935547
		 87.584 -48.177574157714844 88.616 -48.740566253662109 89.652 -49.349796295166016
		 90.688 -50.367168426513672 91.72 -51.675895690917969 92.756 -52.935466766357422 93.788 -54.242328643798828
		 94.824 -55.502822875976562 95.86 -56.377189636230469 96.892 -56.896678924560547 97.928 -57.265674591064453
		 98.964 -57.539878845214844 99.996 -57.562602996826172 101.032 -57.446636199951179
		 102.064 -57.226852416992187 103.1 -56.8455810546875 104.136 -56.488872528076172 105.168 -56.290824890136719
		 106.204 -56.128818511962891 107.24 -55.865642547607422 108.272 -55.443984985351562
		 109.308 -54.906585693359375 110.34 -54.419792175292969 111.376 -54.023117065429688
		 112.412 -53.446025848388672 113.444 -52.634624481201172 114.48 -52.020740509033203
		 115.512 -51.871391296386719 116.548 -51.977516174316406 117.584 -52.010707855224609
		 118.616 -51.862464904785156 119.652 -51.615440368652344 120.688 -51.410686492919922
		 121.72 -51.550334930419922 122.756 -52.018016815185547 123.788 -52.130939483642578
		 124.824 -51.503349304199219 125.86 -50.577789306640625 126.892 -49.965934753417969
		 127.928 -50.062572479248047 128.964 -51.031539916992188 129.996 -52.390853881835938
		 131.032 -53.373104095458984 132.064 -54.015434265136719 133.1 -54.653144836425781
		 134.136 -55.039852142333984 135.168 -55.042747497558594 136.204 -54.995704650878906
		 137.236 -55.06280517578125 138.272 -55.260040283203125 139.308 -55.704292297363281
		 140.34 -56.349899291992188 141.376 -56.962474822998047 142.412 -57.320972442626953
		 143.444 -57.294162750244141 144.48 -56.980205535888672 145.512 -56.716381072998047
		 146.548 -56.743839263916016 147.584 -56.80999755859375 148.616 -56.256687164306641
		 149.652 -54.809139251708984 150.688 -52.995590209960937 151.72 -51.595054626464844
		 152.756 -50.961471557617188 153.788 -50.82769775390625 154.824 -50.788414001464844
		 155.86 -50.665515899658203 156.892 -50.226497650146484 157.928 -49.331111907958984
		 158.964 -48.490131378173828 159.996 -48.251205444335938 161.032 -48.603630065917969
		 162.064 -49.444210052490234 163.1 -50.619205474853516 164.136 -51.591335296630859
		 165.168 -52.234363555908203 166.204 -53.100357055664063 167.236 -54.261116027832031
		 168.272 -55.0458984375 169.308 -55.118789672851563 170.34 -54.83624267578125 171.376 -54.572311401367187
		 172.412 -54.399456024169922 173.444 -54.296833038330078 174.48 -54.208572387695312
		 175.512 -54.001384735107422 176.548 -53.721195220947266 177.584 -53.615413665771484
		 178.616 -53.552711486816406 179.652 -53.228965759277344 180.688 -52.957656860351562
		 181.72 -53.080486297607422 182.756 -53.223400115966797 183.788 -53.148693084716797
		 184.824 -53.306571960449219 185.86 -53.899158477783203 186.892 -54.508255004882813
		 187.928 -54.913845062255859 188.964 -55.119186401367188 189.996 -55.01690673828125
		 191.032 -54.694492340087891 192.064 -54.321865081787109 193.1 -53.981864929199219
		 194.136 -53.720909118652344 195.168 -53.452632904052734 196.204 -53.199222564697266
		 197.236 -53.130626678466797 198.272 -53.25213623046875 199.308 -53.530513763427734
		 200.34 -53.925807952880859 201.376 -54.209877014160156 202.412 -54.283302307128906
		 203.444 -54.398162841796875 204.48 -54.762577056884766 205.512 -55.178764343261719
		 206.548 -55.333686828613281 207.584 -55.307518005371094 208.616 -55.378196716308594
		 209.652 -55.625221252441406 210.688 -55.972557067871094 211.72 -56.281536102294922
		 212.756 -56.496540069580078 213.788 -56.679088592529297 214.824 -56.798427581787109
		 215.86 -56.788532257080078 216.892 -56.780319213867188 217.928 -56.924037933349609
		 218.964 -57.089107513427734 219.996 -57.102836608886719 221.032 -57.029537200927734
		 222.064 -56.927288055419922 223.1 -56.78106689453125 224.136 -56.711738586425781
		 225.168 -56.7520751953125 226.204 -56.695213317871094 227.236 -56.486217498779297
		 228.272 -56.254295349121094 229.308 -55.993507385253906 230.34 -55.813190460205078
		 231.376 -55.912006378173828 232.412 -56.133563995361328 233.444 -56.268001556396484
		 234.48 -56.319141387939453 235.512 -56.468982696533203 236.548 -57.004703521728516
		 237.584 -57.947914123535156 238.616 -59.096458435058601 239.652 -60.267665863037109
		 240.688 -61.364948272705078 241.72 -62.382423400878906 242.756 -63.199920654296882
		 243.788 -63.636081695556648 244.824 -63.745502471923828 245.86 -63.763221740722656
		 246.892 -63.661437988281257 247.928 -63.171092987060547 248.96 -62.390262603759766
		 249.996 -61.614887237548828 251.032 -60.767726898193366 252.064 -59.801109313964844
		 253.1 -59.018486022949212 254.136 -58.535884857177734 255.168 -58.166706085205071
		 256.204 -57.791095733642578 257.236 -57.457885742187493 258.272 -57.281017303466797;
	setAttr ".ktv[250:290]" 259.308 -57.161640167236328 260.34 -56.876213073730469
		 261.376 -56.516765594482422 262.412 -56.272079467773438 263.444 -56.071498870849609
		 264.48 -55.859153747558594 265.512 -55.685684204101563 266.548 -55.551372528076172
		 267.584 -55.502235412597656 268.616 -55.543544769287109 269.652 -55.602840423583984
		 270.688 -55.732234954833984 271.72 -55.976718902587891 272.756 -56.096721649169922
		 273.788 -56.120189666748047 274.824 -56.285442352294922 275.86 -56.620746612548828
		 276.892 -56.956642150878906 277.928 -57.215545654296875 278.96 -57.381732940673828
		 279.996 -57.503959655761719 281.032 -57.652297973632812 282.064 -57.822566986083977
		 283.1 -58.08329772949218 284.136 -58.52180099487304 285.168 -58.867313385009773 286.204 -58.762062072753913
		 287.236 -58.3756103515625 288.272 -58.131328582763679 289.308 -58.113494873046875
		 290.34 -58.099578857421882 291.376 -57.957859039306648 292.412 -57.84685134887696
		 293.444 -57.894676208496101 294.48 -58.035163879394524 295.512 -58.054904937744134
		 296.548 -58.050457000732422 297.584 -58.144748687744141 298.616 -58.347480773925774
		 299.652 -58.475780487060547 300.688 -58.581546783447273;
createNode animCurveTA -n "Bip01_L_Forearm_rotateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 291 ".ktv";
	setAttr ".ktv[0:249]"  0.688 1.9889099597930908 1.72 2.025413990020752 2.756 2.0634114742279053
		 3.788 2.086972713470459 4.824 2.092327356338501 5.86 2.0866351127624512 6.892 2.0768895149230957
		 7.928 2.0679295063018799 8.964 2.0590310096740723 9.996 2.0458977222442627 11.032 2.0297613143920898
		 12.064 2.0146574974060059 13.1 2.0034229755401611 14.136 1.9987176656723025 15.168 2.0003752708435059
		 16.204 2.0151002407073975 17.24 2.0627012252807617 18.272 2.1494362354278564 19.308 2.2554225921630859
		 20.34 2.3552424907684326 21.376 2.4290671348571777 22.412 2.4735543727874756 23.444 2.5024726390838623
		 24.48 2.522275447845459 25.512 2.5311684608459473 26.548 2.5339574813842773 27.584 2.5349245071411133
		 28.616 2.5325312614440918 29.652 2.528994083404541 30.688 2.5285632610321045 31.72 2.5281143188476563
		 32.756 2.5221586227416992 33.788 2.5121965408325195 34.824 2.5029740333557129 35.86 2.4916303157806396
		 36.892 2.4682297706604004 37.928 2.4320769309997559 38.964 2.3914315700531006 39.996 2.347074031829834
		 41.032 2.2935628890991211 42.064 2.2232744693756104 43.1 2.1382992267608643 44.136 2.0617239475250244
		 45.168 2.0082564353942871 46.204 1.9646435976028445 47.24 1.9189484119415283 48.272 1.88155198097229
		 49.308 1.8584088087081907 50.34 1.8404635190963747 51.376 1.8241593837738039 52.412 1.8024561405181885
		 53.444 1.7705312967300415 54.48 1.7340352535247803 55.512 1.6853524446487427 56.548 1.6180696487426758
		 57.584 1.5414198637008667 58.616 1.4654380083084106 59.652 1.4032788276672363 60.688 1.3591459989547729
		 61.72 1.3197592496871948 62.756 1.2809774875640869 63.788 1.2533563375473022 64.824 1.2383772134780884
		 65.86 1.2234965562820435 66.892 1.2020400762557983 67.928 1.1837694644927979 68.964 1.1760296821594238
		 69.996 1.1721365451812744 71.032 1.1653945446014404 72.064 1.1510365009307861 73.1 1.1258862018585205
		 74.136 1.1002810001373291 75.168 1.087215781211853 76.204 1.083471417427063 77.24 1.078547477722168
		 78.272 1.0666636228561401 79.308 1.0452620983123779 80.34 1.0180665254592896 81.376 0.99320465326309215
		 82.412 0.97327619791030884 83.444 0.95715737342834462 84.48 0.94406270980834961 85.512 0.93168407678604126
		 86.548 0.92563563585281361 87.584 0.93616640567779541 88.616 0.95703250169754028
		 89.652 0.98271530866622925 90.688 1.0216720104217529 91.72 1.0685276985168457 92.756 1.112940788269043
		 93.788 1.158769965171814 94.824 1.2025911808013916 95.86 1.2325390577316284 96.892 1.2500227689743042
		 97.928 1.2633602619171143 98.964 1.2741024494171143 99.996 1.2764512300491333 101.032 1.2726911306381226
		 102.064 1.263257622718811 103.1 1.2470123767852783 104.136 1.2321542501449585 105.168 1.2243231534957886
		 106.204 1.2188316583633423 107.24 1.2099907398223877 108.272 1.1957286596298218 109.308 1.1777172088623047
		 110.34 1.161216139793396 111.376 1.1470814943313599 112.412 1.1264139413833618 113.444 1.0979577302932739
		 114.48 1.076624870300293 115.512 1.0716736316680908 116.548 1.0759713649749756 117.584 1.0777579545974731
		 118.616 1.0729416608810425 119.652 1.0645498037338257 120.688 1.0576461553573608
		 121.72 1.0620852708816528 122.756 1.0773582458496094 123.788 1.08115553855896 124.824 1.0604466199874878
		 125.86 1.0296686887741089 126.892 1.0094505548477173 127.928 1.0134332180023193 128.964 1.0468839406967163
		 129.996 1.0935255289077759 131.032 1.1271120309829712 132.064 1.1479874849319458
		 133.1 1.1677888631820679 134.136 1.1793700456619263 135.168 1.17887282371521 136.204 1.177646279335022
		 137.236 1.1810324192047119 138.272 1.1890004873275757 139.308 1.2052038908004761
		 140.34 1.2282700538635254 141.376 1.2499861717224121 142.412 1.2623838186264038 143.444 1.2614187002182007
		 144.48 1.2511115074157715 145.512 1.2427459955215454 146.548 1.2437530755996704 147.584 1.2452841997146606
		 148.616 1.2249633073806763 149.652 1.1736446619033813 150.688 1.1102809906005859
		 151.72 1.0622276067733765 152.756 1.041475772857666 153.788 1.0384974479675293 154.824 1.0387589931488037
		 155.86 1.0351606607437134 156.892 1.0143214464187622 157.928 0.98201036453247059
		 158.964 0.951679527759552 159.996 0.94276714324951172 161.032 0.95525449514389038
		 162.064 0.98456084728240967 163.1 1.025001049041748 164.136 1.0600289106369019 165.168 1.0858700275421143
		 166.204 1.1193546056747437 167.236 1.1614974737167358 168.272 1.1896312236785889
		 169.308 1.192715048789978 170.34 1.1833431720733643 171.376 1.1741498708724976 172.412 1.1670850515365601
		 173.444 1.1614899635314941 174.48 1.1481536626815796 175.512 1.1396361589431763 176.548 1.1309072971343994
		 177.584 1.1295922994613647 178.616 1.1290241479873657 179.652 1.1178270578384399
		 180.688 1.1068099737167358 181.72 1.1089524030685425 182.756 1.1138010025024414 183.788 1.1136500835418701
		 184.824 1.1220564842224121 185.86 1.1447494029998779 186.892 1.1674597263336182 187.928 1.1821084022521973
		 188.964 1.1886179447174072 189.996 1.1842629909515381 191.032 1.1730378866195679
		 192.064 1.1609137058258057 193.1 1.1495991945266724 194.136 1.1390299797058105 195.168 1.1259863376617432
		 196.204 1.1126272678375244 197.236 1.1061350107192993 198.272 1.1074984073638916
		 199.308 1.1163537502288818 200.34 1.1319395303726196 201.376 1.1458008289337158 202.412 1.1527246236801147
		 203.444 1.1601201295852661 204.48 1.1750335693359375 205.512 1.1908178329467773 206.548 1.1966803073883057
		 207.584 1.1954072713851929 208.616 1.1970096826553345 209.652 1.2049238681793213
		 210.688 1.216650128364563 211.72 1.2268131971359253 212.756 1.233045220375061 213.788 1.2379683256149292
		 214.824 1.241454005241394 215.86 1.2415190935134888 216.892 1.2423232793807983 217.928 1.2484878301620483
		 218.964 1.2549670934677124 219.996 1.2552641630172729 221.032 1.2516546249389648
		 222.064 1.2472732067108154 223.1 1.2425730228424072 224.136 1.2409327030181885 225.168 1.2421493530273437
		 226.204 1.239199161529541 227.236 1.2308006286621094 228.272 1.2211273908615112 229.308 1.2101202011108398
		 230.34 1.2026702165603638 231.376 1.2060254812240601 232.412 1.2142914533615112 233.444 1.2197414636611938
		 234.48 1.2223457098007202 235.512 1.228482723236084 236.548 1.2482250928878784 237.584 1.2827746868133545
		 238.616 1.3251051902770996 239.652 1.3683445453643799 240.688 1.408495306968689 241.72 1.4451760053634644
		 242.756 1.4741625785827637 243.788 1.4889923334121704 244.824 1.4917535781860352
		 245.86 1.4912605285644531 246.892 1.4867508411407471 247.928 1.4685509204864502 248.96 1.4403104782104492
		 249.996 1.4127447605133057 251.032 1.382996678352356 252.064 1.3488820791244507 253.1 1.3208404779434204
		 254.136 1.303314208984375 255.168 1.2903058528900146 256.204 1.2775393724441528 257.236 1.2662214040756226
		 258.272 1.2599416971206665;
	setAttr ".ktv[250:290]" 259.308 1.2554143667221069 260.34 1.2452106475830078
		 261.376 1.2324544191360474 262.412 1.2235137224197388 263.444 1.216234564781189 264.48 1.2089805603027344
		 265.512 1.2033182382583618 266.548 1.1986699104309082 267.584 1.1960752010345459
		 268.616 1.19621741771698 269.652 1.1977931261062622 270.688 1.2025759220123291 271.72 1.2126584053039551
		 272.756 1.2168123722076416 273.788 1.2180249691009521 274.824 1.2242892980575562
		 275.86 1.2359685897827148 276.892 1.247126579284668 277.928 1.2556518316268921 278.96 1.2612696886062622
		 279.996 1.2657275199890137 281.032 1.2715636491775513 282.064 1.2785525321960449
		 283.1 1.2920624017715454 284.136 1.3080047369003296 285.168 1.3197698593139648 286.204 1.3147987127304077
		 287.236 1.2997676134109497 288.272 1.2902849912643433 289.308 1.2893621921539307
		 290.34 1.2889552116394043 291.376 1.2841811180114746 292.412 1.2803453207015991 293.444 1.2817107439041138
		 294.48 1.2848702669143677 295.512 1.2830665111541748 296.548 1.2793542146682739 297.584 1.2801086902618408
		 298.616 1.2875258922576904 299.652 1.2948243618011475 300.688 1.301917552947998;
createNode animCurveTA -n "Bip01_L_Thigh_rotateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 291 ".ktv";
	setAttr ".ktv[0:249]"  0.688 -6.1176090240478516 1.72 -5.9339585304260254
		 2.756 -5.7752799987792969 3.788 -5.6943283081054687 4.824 -5.6936726570129395 5.86 -5.7265396118164062
		 6.892 -5.7660999298095703 7.928 -5.8536086082458496 8.964 -5.9872069358825684 9.996 -6.0961346626281738
		 11.032 -6.1848998069763184 12.064 -6.2714247703552246 13.1 -6.3683314323425293 14.136 -6.4649105072021484
		 15.168 -6.5599045753479004 16.204 -6.6654720306396484 17.24 -6.7897539138793945 18.272 -6.8839287757873535
		 19.308 -6.9440889358520508 20.34 -7.0164618492126465 21.376 -7.1369733810424805 22.412 -7.2493982315063468
		 23.444 -7.2675666809082031 24.48 -7.2806534767150879 25.512 -7.3139572143554687 26.548 -7.3696370124816895
		 27.584 -7.3974494934082031 28.616 -7.411492347717286 29.652 -7.428868293762207 30.688 -7.4510531425476065
		 31.72 -7.4554243087768564 32.756 -7.4399881362915039 33.788 -7.4338407516479483 34.824 -7.4271745681762695
		 35.86 -7.3903188705444336 36.892 -7.2928657531738281 37.928 -7.1583929061889648 38.964 -6.9635686874389648
		 39.996 -6.6719741821289062 41.032 -6.3034720420837402 42.064 -5.9904308319091797
		 43.1 -5.7807164192199707 44.136 -5.6391949653625488 45.168 -5.4375677108764648 46.204 -5.0720710754394531
		 47.24 -4.5766911506652832 48.272 -4.1149430274963379 49.308 -3.8049342632293706 50.34 -3.5828166007995605
		 51.376 -3.3177504539489746 52.412 -2.9713234901428223 53.444 -2.6693744659423828
		 54.48 -2.530940055847168 55.512 -2.5847072601318359 56.548 -2.7780563831329346 57.584 -3.000420093536377
		 58.616 -3.1453020572662354 59.652 -3.1946978569030762 60.688 -3.2242043018341064
		 61.72 -3.3423392772674561 62.756 -3.5160717964172363 63.788 -3.6020503044128418 64.824 -3.5050070285797119
		 65.86 -3.2997462749481201 66.892 -3.1031186580657959 67.928 -2.9369537830352783 68.964 -2.7871541976928711
		 69.996 -2.6686639785766602 71.032 -2.5844614505767822 72.064 -2.5353934764862061
		 73.1 -2.460862398147583 74.136 -2.2996153831481934 75.168 -2.1105556488037109 76.204 -2.0113813877105713
		 77.24 -1.9939507246017456 78.272 -2.0682084560394287 79.308 -2.3213624954223633 80.34 -2.6675713062286377
		 81.376 -2.9164633750915527 82.412 -3.069049596786499 83.444 -3.3077783584594727 84.48 -3.6453537940978999
		 85.512 -3.8926339149475102 86.548 -3.9260470867156982 87.584 -3.8184306621551518
		 88.616 -3.6913619041442871 89.652 -3.6455450057983398 90.688 -3.6654343605041508
		 91.72 -3.7284362316131596 92.756 -3.8012733459472652 93.788 -3.8662419319152836 94.824 -3.9453630447387695
		 95.86 -4.012080192565918 96.892 -4.0174899101257324 97.928 -3.9760713577270503 98.964 -3.9579577445983891
		 99.996 -3.9450078010559082 101.032 -3.8678267002105717 102.064 -3.7125902175903316
		 103.1 -3.5611238479614258 104.136 -3.446103572845459 105.168 -3.3192257881164551
		 106.204 -3.1575417518615723 107.24 -2.9916501045227051 108.272 -2.8437354564666748
		 109.308 -2.7231416702270508 110.34 -2.6057169437408447 111.376 -2.4891667366027832
		 112.412 -2.3958935737609863 113.444 -2.3221821784973145 114.48 -2.2601094245910645
		 115.512 -2.2286632061004639 116.548 -2.2454109191894531 117.584 -2.2968831062316895
		 118.616 -2.3660047054290771 119.652 -2.4562451839447021 120.688 -2.57254958152771
		 121.72 -2.6684727668762207 122.756 -2.7541780471801758 123.788 -2.8881044387817383
		 124.824 -3.0895130634307861 125.86 -3.3319573402404785 126.892 -3.5508513450622559
		 127.928 -3.7279717922210693 128.964 -3.8272826671600337 129.996 -3.8723344802856445
		 131.032 -3.9364290237426753 132.064 -4.0031189918518066 133.1 -3.9920268058776855
		 134.136 -3.9790768623352046 135.168 -4.0325980186462402 136.204 -4.1129755973815918
		 137.236 -4.1954026222229004 138.272 -4.2952322959899902 139.308 -4.3939151763916016
		 140.34 -4.4837455749511719 141.376 -4.6270155906677246 142.412 -4.7454237937927246
		 143.444 -4.7831811904907227 144.48 -4.7890276908874512 145.512 -4.810638427734375
		 146.548 -4.8488059043884277 147.584 -4.8670287132263184 148.616 -4.8733944892883301
		 149.652 -4.8979282379150391 150.688 -4.94000244140625 151.72 -4.9803824424743652
		 152.756 -5.0040149688720703 153.788 -5.0276474952697754 154.824 -5.0506515502929687
		 155.86 -5.0890097618103027 156.892 -5.143733024597168 157.928 -5.2065982818603516
		 158.964 -5.2816758155822754 159.996 -5.3298697471618652 161.032 -5.3133130073547363
		 162.064 -5.2959918975830078 163.1 -5.3192143440246582 164.136 -5.3327655792236328
		 165.168 -5.3313450813293457 166.204 -5.3313722610473633 167.236 -5.2946257591247559
		 168.272 -5.2565407752990723 169.308 -5.3397049903869629 170.34 -5.510350227355957
		 171.376 -5.5601563453674316 172.412 -5.4258747100830078 173.444 -5.2270889282226563
		 174.48 -5.0320734977722168 175.512 -4.8361287117004395 176.548 -4.6265239715576172
		 177.584 -4.3613214492797852 178.616 -4.0208230018615723 179.652 -3.688274621963501
		 180.688 -3.4526877403259277 181.72 -3.2956752777099609 182.756 -3.12093186378479
		 183.788 -2.9220914840698242 184.824 -2.7268025875091553 185.86 -2.506624698638916
		 186.892 -2.2581424713134766 187.928 -2.0775520801544189 188.964 -2.0156979560852051
		 189.996 -2.0081028938293457 191.032 -1.9914917945861814 192.064 -1.986519455909729
		 193.1 -2.0034582614898682 194.136 -2.0133211612701416 195.168 -2.0080482959747314
		 196.204 -2.0202879905700684 197.236 -2.0518434047698975 198.272 -2.0991356372833252
		 199.308 -2.1624650955200195 200.34 -2.2384169101715088 201.376 -2.3349409103393555
		 202.412 -2.4563817977905273 203.444 -2.5611295700073242 204.48 -2.6256611347198486
		 205.512 -2.6911218166351318 206.548 -2.7476756572723389 207.584 -2.8004319667816162
		 208.616 -2.8551554679870605 209.652 -2.902475118637085 210.688 -2.9389755725860596
		 211.72 -2.949138879776001 212.756 -2.9368445873260498 213.788 -2.9314351081848145
		 214.824 -2.92897629737854 215.86 -2.9157528877258301 216.892 -2.8741707801818848
		 217.928 -2.8158409595489502 218.964 -2.7495334148406982 219.996 -2.663090705871582
		 221.032 -2.5671126842498779 222.064 -2.4775280952453613 223.1 -2.3894460201263428
		 224.136 -2.290271520614624 225.168 -2.1626291275024414 226.204 -2.0365438461303711
		 227.236 -1.951685428619385 228.272 -1.9022622108459473 229.308 -1.844178318977356
		 230.34 -1.7876789569854736 231.376 -1.732982873916626 232.412 -1.6728771924972534
		 233.444 -1.6174980401992798 234.48 -1.5881282091140747 235.512 -1.579494833946228
		 236.548 -1.5793036222457886 237.584 -1.5967068672180176 238.616 -1.6038376092910767
		 239.652 -1.6037830114364624 240.688 -1.5868713855743408 241.72 -1.5609986782073975
		 242.756 -1.5767627954483032 243.788 -1.6670578718185425 244.824 -1.7784992456436157
		 245.86 -1.8480578660964964 246.892 -1.876908540725708 247.928 -1.9351837635040283
		 248.96 -2.0322544574737549 249.996 -2.1507718563079834 251.032 -2.2744801044464111
		 252.064 -2.3944728374481201 253.1 -2.5137553215026855 254.136 -2.6558778285980225
		 255.168 -2.8210046291351318 256.204 -2.9848744869232178 257.236 -3.144946813583374
		 258.272 -3.3081610202789307;
	setAttr ".ktv[250:290]" 259.308 -3.4603102207183838 260.34 -3.6093449592590336
		 261.376 -3.7817389965057377 262.412 -3.9661812782287602 263.444 -4.1303791999816895
		 264.48 -4.2907519340515137 265.512 -4.4554963111877441 266.548 -4.6325345039367676
		 267.584 -4.8044366836547852 268.616 -4.9596185684204102 269.652 -5.0957307815551758
		 270.688 -5.2117891311645508 271.72 -5.3318367004394531 272.756 -5.4404911994934082
		 273.788 -5.5488181114196777 274.824 -5.6556968688964844 275.86 -5.7918362617492676
		 276.892 -5.9040699005126953 277.928 -5.991386890411377 278.96 -6.0743598937988281
		 279.996 -6.1761574745178223 281.032 -6.23779296875 282.064 -6.2530379295349121 283.1 -6.2598681449890137
		 284.136 -6.2564802169799805 285.168 -6.2400331497192383 286.204 -6.2350063323974609
		 287.236 -6.2768340110778809 288.272 -6.3181157112121582 289.308 -6.3184981346130371
		 290.34 -6.2825441360473633 291.376 -6.2542400360107422 292.412 -6.2369184494018555
		 293.444 -6.2180399894714355 294.48 -6.1568140983581543 295.512 -6.0736770629882812
		 296.548 -6.0134892463684082 297.584 -6.0162487030029297 298.616 -6.064033031463623
		 299.652 -6.1159424781799316 300.688 -6.135230541229248;
createNode animCurveTA -n "Bip01_L_Thigh_rotateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 291 ".ktv";
	setAttr ".ktv[0:249]"  0.688 -175.45697021484375 1.72 -175.15185546875 2.756 -174.9541015625
		 3.788 -174.84992980957031 4.824 -174.82818603515625 5.86 -174.86958312988281 6.892 -174.93994140625
		 7.928 -175.08447265625 8.964 -175.28912353515625 9.996 -175.48182678222656 11.032 -175.65281677246094
		 12.064 -175.81822204589844 13.1 -175.98651123046875 14.136 -176.14785766601562 15.168 -176.30206298828125
		 16.204 -176.45008850097656 17.24 -176.59840393066406 18.272 -176.70623779296875 19.308 -176.7689208984375
		 20.34 -176.81114196777344 21.376 -176.89328002929687 22.412 -176.94815063476562 23.444 -176.92965698242187
		 24.48 -176.87863159179687 25.512 -176.87985229492187 26.548 -176.90000915527344 27.584 -176.91104125976562
		 28.616 -176.92141723632812 29.652 -176.95755004882813 30.688 -177.00837707519531
		 31.72 -177.05532836914062 32.756 -177.11257934570312 33.788 -177.19345092773437 34.824 -177.29518127441406
		 35.86 -177.37075805664062 36.892 -177.39630126953125 37.928 -177.37078857421875 38.964 -177.290283203125
		 39.996 -177.10414123535156 41.032 -176.83392333984375 42.064 -176.607421875 43.1 -176.48963928222656
		 44.136 -176.43553161621094 45.168 -176.31108093261719 46.204 -176.00141906738281
		 47.24 -175.51797485351562 48.272 -175.06398010253906 49.308 -174.77346801757812 50.34 -174.59489440917969
		 51.376 -174.38818359375 52.412 -174.11843872070312 53.444 -173.90510559082031 54.48 -173.88223266601562
		 55.512 -174.07131958007812 56.548 -174.42289733886719 57.584 -174.84817504882812
		 58.616 -175.21714782714844 59.652 -175.48895263671875 60.688 -175.76010131835937
		 61.72 -176.11956787109375 62.756 -176.5438232421875 63.788 -176.88311767578125 64.824 -177.01225280761719
		 65.86 -177.00578308105469 66.892 -176.99757385253906 67.928 -177.0096435546875 68.964 -177.02012634277344
		 69.996 -177.03787231445312 71.032 -177.08364868164062 72.064 -177.1405029296875 73.1 -177.15267944335937
		 74.136 -177.05899047851562 75.168 -176.93899536132812 76.204 -176.91914367675781
		 77.24 -176.991455078125 78.272 -177.16084289550781 79.308 -177.5281982421875 80.34 -178.01838684082031
		 81.376 -178.39582824707031 82.412 -178.6722412109375 83.444 -179.0570068359375 84.48 -179.57026672363281
		 85.512 -179.98171997070312 86.548 -180.13763427734375 87.584 -180.10116577148437
		 88.616 -180.04505920410156 89.652 -180.09043884277344 90.688 -180.23356628417969
		 91.72 -180.42570495605469 92.756 -180.63215637207031 93.788 -180.84098815917969 94.824 -181.07429504394531
		 95.86 -181.30155944824219 96.892 -181.4705810546875 97.928 -181.59872436523437 98.964 -181.76646423339844
		 99.996 -181.97746276855469 101.032 -182.12442016601562 102.064 -182.18141174316406
		 103.1 -182.25457763671875 104.136 -182.37873840332031 105.168 -182.49833679199219
		 106.204 -182.56840515136719 107.24 -182.62887573242187 108.272 -182.70065307617187
		 109.308 -182.77607727050781 110.34 -182.82472229003906 111.376 -182.84645080566406
		 112.412 -182.84873962402344 113.444 -182.82707214355469 114.48 -182.77729797363281
		 115.512 -182.70574951171875 116.548 -182.63591003417969 117.584 -182.55583190917969
		 118.616 -182.45501708984375 119.652 -182.32958984375 120.688 -182.18899536132812
		 121.72 -181.99261474609375 122.756 -181.75250244140625 123.788 -181.54971313476562
		 124.824 -181.43254089355469 125.86 -181.34719848632812 126.892 -181.22682189941406
		 127.928 -181.04922485351562 128.964 -180.78038024902344 129.996 -180.46266174316406
		 131.032 -180.19975280761719 132.064 -179.95832824707031 133.1 -179.65618896484375
		 134.136 -179.38388061523437 135.168 -179.21580505371094 136.204 -179.10057067871094
		 137.236 -179.02252197265625 138.272 -178.98298645019531 139.308 -178.97093200683594
		 140.34 -178.99577331542969 141.376 -179.08065795898437 142.412 -179.15992736816406
		 143.444 -179.15133666992187 144.48 -179.11149597167969 145.512 -179.09564208984375
		 146.548 -179.10136413574219 147.584 -179.09930419921875 148.616 -179.07261657714844
		 149.652 -179.05270385742187 150.688 -179.02981567382812 151.72 -178.98477172851563
		 152.756 -178.90449523925781 153.788 -178.80303955078125 154.824 -178.68049621582031
		 155.86 -178.5423583984375 156.892 -178.39314270019531 157.928 -178.22259521484375
		 158.964 -178.03988647460937 159.996 -177.80348205566406 161.032 -177.48176574707031
		 162.064 -177.14146423339844 163.1 -176.81613159179687 164.136 -176.48480224609375
		 165.168 -176.12651062011719 166.204 -175.76837158203125 167.236 -175.3831787109375
		 168.272 -175.02101135253906 169.308 -174.79740905761719 170.34 -174.68716430664062
		 171.376 -174.48219299316406 172.412 -174.12200927734375 173.444 -173.74156188964844
		 174.48 -173.41171264648437 175.512 -173.12037658691406 176.548 -172.85211181640625
		 177.584 -172.57710266113281 178.616 -172.26469421386719 179.652 -171.95787048339844
		 180.688 -171.80140686035156 181.72 -171.79734802246094 182.756 -171.81045532226562
		 183.788 -171.80482482910156 184.824 -171.810791015625 185.86 -171.81626892089844
		 186.892 -171.80903625488281 187.928 -171.87486267089844 188.964 -172.06396484375
		 189.996 -172.29034423828125 191.032 -172.50625610351562 192.064 -172.72872924804687
		 193.1 -172.96533203125 194.136 -173.18769836425781 195.168 -173.39488220214844 196.204 -173.61695861816406
		 197.236 -173.85418701171875 198.272 -174.10780334472656 199.308 -174.36972045898437
		 200.34 -174.6300048828125 201.376 -174.90959167480469 202.412 -175.1943359375 203.444 -175.4471435546875
		 204.48 -175.65345764160156 205.512 -175.84735107421875 206.548 -176.02655029296875
		 207.584 -176.19287109375 208.616 -176.33720397949219 209.652 -176.45939636230469
		 210.688 -176.56105041503906 211.72 -176.6307373046875 212.756 -176.66938781738281
		 213.788 -176.69557189941406 214.824 -176.71669006347656 215.86 -176.71745300292969
		 216.892 -176.69400024414062 217.928 -176.65080261230469 218.964 -176.58131408691406
		 219.996 -176.48234558105469 221.032 -176.36801147460937 222.064 -176.24879455566406
		 223.1 -176.12431335449219 224.136 -175.98178100585937 225.168 -175.81535339355469
		 226.204 -175.64683532714844 227.236 -175.50497436523437 228.272 -175.3739013671875
		 229.308 -175.23666381835937 230.34 -175.08720397949219 231.376 -174.92924499511719
		 232.412 -174.75227355957031 233.444 -174.57231140136719 234.48 -174.404296875 235.512 -174.24185180664062
		 236.548 -174.08110046386719 237.584 -173.92022705078125 238.616 -173.74905395507812
		 239.652 -173.54653930664062 240.688 -173.32194519042969 241.72 -173.08131408691406
		 242.756 -172.86595153808594 243.788 -172.70756530761719 244.824 -172.56564331054687
		 245.86 -172.39231872558594 246.892 -172.18899536132812 247.928 -172.009521484375
		 248.96 -171.87953186035156 249.996 -171.78254699707031 251.032 -171.69609069824219
		 252.064 -171.63134765625 253.1 -171.57669067382812 254.136 -171.54586791992187 255.168 -171.54833984375
		 256.204 -171.5609130859375 257.236 -171.58683776855469 258.272 -171.62800598144531;
	setAttr ".ktv[250:290]" 259.308 -171.67288208007813 260.34 -171.7354736328125
		 261.376 -171.83174133300781 262.412 -171.93701171875 263.444 -172.0347900390625 264.48 -172.12583923339844
		 265.512 -172.21131896972656 266.548 -172.30099487304688 267.584 -172.38847351074219
		 268.616 -172.46134948730469 269.652 -172.51072692871094 270.688 -172.55635070800781
		 271.72 -172.59416198730469 272.756 -172.6258544921875 273.788 -172.656494140625 274.824 -172.69534301757812
		 275.86 -172.76872253417969 276.892 -172.82827758789063 277.928 -172.87635803222656
		 278.96 -172.9383544921875 279.996 -173.01741027832031 281.032 -173.09286499023437
		 282.064 -173.14712524414062 283.1 -173.2078857421875 284.136 -173.279296875 285.168 -173.34219360351562
		 286.204 -173.42784118652344 287.236 -173.55325317382812 288.272 -173.69515991210937
		 289.308 -173.8095703125 290.34 -173.89300537109375 291.376 -173.97138977050781 292.412 -174.0810546875
		 293.444 -174.17315673828125 294.48 -174.22071838378906 295.512 -174.25607299804687
		 296.548 -174.30778503417969 297.584 -174.42097473144531 298.616 -174.5889892578125
		 299.652 -174.74844360351562 300.688 -174.87677001953125;
createNode animCurveTA -n "Bip01_L_Thigh_rotateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 291 ".ktv";
	setAttr ".ktv[0:249]"  0.688 -89.785560607910156 1.72 -91.063407897949219
		 2.756 -92.487174987792969 3.788 -93.557685852050781 4.824 -94.231277465820313 5.86 -94.757095336914062
		 6.892 -95.224197387695312 7.928 -95.643760681152344 8.964 -96.003791809082031 9.996 -96.292823791503906
		 11.032 -96.467620849609375 12.064 -96.548873901367188 13.1 -96.591629028320313 14.136 -96.643943786621094
		 15.168 -96.723037719726563 16.204 -96.835845947265625 17.24 -96.941200256347656 18.272 -97.023025512695312
		 19.308 -97.080039978027344 20.34 -97.16473388671875 21.376 -97.152198791503906 22.412 -97.092964172363281
		 23.444 -97.045722961425781 24.48 -97.017311096191406 25.512 -96.952369689941406 26.548 -96.858551025390625
		 27.584 -96.771400451660156 28.616 -96.685287475585938 29.652 -96.521522521972656
		 30.688 -96.233566284179687 31.72 -95.935600280761719 32.756 -95.674880981445313 33.788 -95.388938903808594
		 34.824 -95.06884765625 35.86 -94.708709716796875 36.892 -94.230018615722656 37.928 -93.56768798828125
		 38.964 -92.834426879882813 39.996 -92.074989318847656 41.032 -91.399894714355469
		 42.064 -90.901535034179688 43.1 -90.595352172851562 44.136 -90.486015319824219 45.168 -90.402412414550781
		 46.204 -90.073768615722656 47.24 -89.453536987304688 48.272 -88.797073364257813 49.308 -88.309532165527344
		 50.34 -87.982749938964844 51.376 -87.716590881347656 52.412 -87.451606750488281 53.444 -87.162933349609375
		 54.48 -86.868141174316406 55.512 -86.514015197753906 56.548 -86.071334838867188 57.584 -85.588577270507813
		 58.616 -85.030303955078125 59.652 -84.414688110351562 60.688 -83.834663391113281
		 61.72 -83.200363159179687 62.756 -82.497726440429687 63.788 -81.88934326171875 64.824 -81.430274963378906
		 65.86 -80.997566223144531 66.892 -80.611007690429688 67.928 -80.319740295410156 68.964 -80.099784851074219
		 69.996 -79.869468688964844 71.032 -79.589897155761719 72.064 -79.231094360351563
		 73.1 -78.8587646484375 74.136 -78.594245910644531 75.168 -78.4200439453125 76.204 -78.284507751464844
		 77.24 -78.186233520507812 78.272 -77.996414184570313 79.308 -77.594657897949219 80.34 -77.016853332519531
		 81.376 -76.501144409179688 82.412 -76.07537841796875 83.444 -75.572654724121094 84.48 -74.969108581542969
		 85.512 -74.504165649414063 86.548 -74.367996215820313 87.584 -74.455345153808594
		 88.616 -74.513999938964844 89.652 -74.516212463378906 90.688 -74.572578430175781
		 91.72 -74.568397521972656 92.756 -74.271934509277344 93.788 -73.814834594726563 94.824 -73.504440307617188
		 95.86 -73.363906860351563 96.892 -73.280250549316406 97.928 -73.189216613769531 98.964 -72.986083984375
		 99.996 -72.650802612304688 101.032 -72.419754028320312 102.064 -72.42926025390625
		 103.1 -72.519775390625 104.136 -72.549308776855469 105.168 -72.591468811035156 106.204 -72.650535583496094
		 107.24 -72.71221923828125 108.272 -72.783477783203125 109.308 -72.860954284667969
		 110.34 -72.948410034179688 111.376 -73.054115295410156 112.412 -73.093650817871094
		 113.444 -73.045890808105469 114.48 -73.085502624511719 115.512 -73.333168029785156
		 116.548 -73.721153259277344 117.584 -74.022445678710938 118.616 -74.166038513183594
		 119.652 -74.306282043457031 120.688 -74.588859558105469 121.72 -75.115219116210937
		 122.756 -75.777587890625 123.788 -76.123519897460938 124.824 -75.986862182617188
		 125.86 -75.661911010742188 126.892 -75.53631591796875 127.928 -75.736328125 128.964 -76.321815490722656
		 129.996 -77.180831909179688 131.032 -78.064224243164063 132.064 -78.878189086914063
		 133.1 -79.593498229980469 134.136 -80.120872497558594 135.168 -80.454948425292969
		 136.204 -80.679801940917969 137.236 -80.852851867675781 138.272 -81.078765869140625
		 139.308 -81.457183837890625 140.34 -82.007316589355469 141.376 -82.615501403808594
		 142.412 -83.074386596679688 143.444 -83.229782104492188 144.48 -83.134628295898438
		 145.512 -83.066459655761719 146.548 -83.353927612304688 147.584 -83.925453186035156
		 148.616 -84.446128845214844 149.652 -84.807151794433594 150.688 -85.050140380859375
		 151.72 -85.248817443847656 152.756 -85.548576354980469 153.788 -85.973251342773438
		 154.824 -86.329216003417969 155.86 -86.506607055664063 156.892 -86.596466064453125
		 157.928 -86.723426818847656 158.964 -86.864509582519531 159.996 -87.030677795410156
		 161.032 -87.225555419921875 162.064 -87.390380859375 163.1 -87.515617370605469 164.136 -87.622825622558594
		 165.168 -87.729896545410156 166.204 -87.811119079589844 167.236 -87.878822326660156
		 168.272 -87.93963623046875 169.308 -87.969062805175781 170.34 -87.957611083984375
		 171.376 -87.92633056640625 172.412 -87.885459899902344 173.444 -87.884994506835938
		 174.48 -87.9075927734375 175.512 -87.896247863769531 176.548 -87.851638793945313
		 177.584 -87.8472900390625 178.616 -87.842918395996094 179.652 -87.768447875976563
		 180.688 -87.756149291992188 181.72 -87.859283447265625 182.756 -87.985946655273438
		 183.788 -88.0570068359375 184.824 -88.148750305175781 185.86 -88.301612854003906
		 186.892 -88.472335815429688 187.928 -88.6341552734375 188.964 -88.766799926757813
		 189.996 -88.813926696777344 191.032 -88.772262573242188 192.064 -88.703445434570312
		 193.1 -88.602325439453125 194.136 -88.458023071289063 195.168 -88.300048828125 196.204 -88.173118591308594
		 197.236 -88.096183776855469 198.272 -88.033401489257813 199.308 -87.980674743652344
		 200.34 -87.955101013183594 201.376 -87.940261840820313 202.412 -87.894615173339844
		 203.444 -87.821205139160156 204.48 -87.787132263183594 205.512 -87.875076293945313
		 206.548 -88.079330444335938 207.584 -88.291938781738281 208.616 -88.4578857421875
		 209.652 -88.618911743164063 210.688 -88.812370300292969 211.72 -89.062980651855469
		 212.756 -89.333702087402344 213.788 -89.616806030273437 214.824 -89.8826904296875
		 215.86 -90.13751220703125 216.892 -90.47418212890625 217.928 -90.874649047851563
		 218.964 -91.19793701171875 219.996 -91.430328369140625 221.032 -91.63116455078125
		 222.064 -91.797706604003906 223.1 -91.9183349609375 224.136 -92.067420959472656 225.168 -92.308113098144531
		 226.204 -92.531494140625 227.236 -92.648750305175781 228.272 -92.735687255859375
		 229.308 -92.858139038085938 230.34 -93.004547119140625 231.376 -93.133476257324219
		 232.412 -93.249290466308594 233.444 -93.371109008789063 234.48 -93.48944091796875
		 235.512 -93.61956787109375 236.548 -93.770980834960938 237.584 -93.938209533691406
		 238.616 -94.107162475585937 239.652 -94.254638671875 240.688 -94.339363098144531
		 241.72 -94.377853393554688 242.756 -94.381927490234375 243.788 -94.327392578125 244.824 -94.258163452148438
		 245.86 -94.196418762207031 246.892 -94.109619140625 247.928 -93.977333068847656 248.96 -93.838409423828125
		 249.996 -93.714591979980469 251.032 -93.586700439453125 252.064 -93.4393310546875
		 253.1 -93.269584655761719 254.136 -93.041763305664063 255.168 -92.774375915527344
		 256.204 -92.494171142578125 257.236 -92.22003173828125 258.272 -91.926445007324219;
	setAttr ".ktv[250:290]" 259.308 -91.667991638183594 260.34 -91.471771240234375
		 261.376 -91.288017272949219 262.412 -91.100044250488281 263.444 -90.955848693847656
		 264.48 -90.797935485839844 265.512 -90.621879577636719 266.548 -90.434730529785156
		 267.584 -90.273048400878906 268.616 -90.146774291992188 269.652 -90.064643859863281
		 270.688 -90.018447875976563 271.72 -89.988090515136719 272.756 -89.9708251953125
		 273.788 -89.978500366210938 274.824 -90.008445739746094 275.86 -90.035003662109375
		 276.892 -90.040168762207031 277.928 -90.051368713378906 278.96 -90.037513732910156
		 279.996 -89.985855102539062 281.032 -89.981452941894531 282.064 -90.022132873535156
		 283.1 -90.035354614257813 284.136 -90.032951354980469 285.168 -90.019676208496094
		 286.204 -89.951950073242188 287.236 -89.817230224609375 288.272 -89.726387023925781
		 289.308 -89.684722900390625 290.34 -89.687973022460938 291.376 -89.675567626953125
		 292.412 -89.664505004882813 293.444 -89.646827697753906 294.48 -89.664451599121094
		 295.512 -89.69622802734375 296.548 -89.756767272949219 297.584 -89.828514099121094
		 298.616 -89.952873229980469 299.652 -90.084968566894531 300.688 -90.181526184082031;
createNode animCurveTA -n "Bip01_L_Calf_rotateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 291 ".ktv";
	setAttr ".ktv[0:249]"  0.688 0.93206977844238281 1.72 0.9321376085281371
		 2.756 0.93212556838989258 3.788 0.93222409486770641 4.824 0.9324209690093993 5.86 0.93261051177978527
		 6.892 0.93275642395019542 7.928 0.93285363912582397 8.964 0.93291562795639049 9.996 0.93302428722381592
		 11.032 0.93320113420486439 12.064 0.93345373868942261 13.1 0.93369740247726429 14.136 0.9339207410812379
		 15.168 0.93409961462020874 16.204 0.93428623676300038 17.24 0.93448668718338024 18.272 0.93466222286224365
		 19.308 0.93479716777801525 20.34 0.93493556976318359 21.376 0.93500310182571422 22.412 0.93507206439971924
		 23.444 0.93515765666961659 24.48 0.93521881103515636 25.512 0.93520152568817139 26.548 0.93519181013107289
		 27.584 0.93515580892562855 28.616 0.93514215946197521 29.652 0.9351610541343689 30.688 0.93519222736358654
		 31.72 0.93521922826766968 32.756 0.93524229526519786 33.788 0.93524289131164551 34.824 0.9352440834045409
		 35.86 0.93523907661437988 36.892 0.93524962663650502 37.928 0.9352480173110963 38.964 0.93524271249771118
		 39.996 0.93523883819580078 41.032 0.9352245330810548 42.064 0.93518793582916249 43.1 0.93513083457946777
		 44.136 0.93513023853302002 45.168 0.93511432409286499 46.204 0.93506503105163574
		 47.24 0.93504410982131969 48.272 0.93499863147735585 49.308 0.93492722511291504 50.34 0.93488496541976929
		 51.376 0.93485617637634277 52.412 0.93482679128646851 53.444 0.93478775024414074
		 54.48 0.9347251057624818 55.512 0.93462294340133656 56.548 0.93449103832244873 57.584 0.93433266878128052
		 58.616 0.93417364358901978 59.652 0.93404883146286011 60.688 0.93390303850173961
		 61.72 0.93371957540512074 62.756 0.93353825807571422 63.788 0.93337392807006836 64.824 0.93327164649963379
		 65.86 0.93322044610977173 66.892 0.93309640884399403 67.928 0.93286496400833119 68.964 0.93263721466064453
		 69.996 0.93244266510009766 71.032 0.93226367235183716 72.064 0.93207293748855591
		 73.1 0.93180346488952637 74.136 0.93164819478988659 75.168 0.93157851696014404 76.204 0.93144941329956055
		 77.24 0.93127244710922241 78.272 0.93103390932083141 79.308 0.93072241544723511 80.34 0.93032366037368774
		 81.376 0.92999917268753041 82.412 0.9297051429748534 83.444 0.92945837974548329 84.48 0.92919886112213124
		 85.512 0.92890244722366322 86.548 0.92865031957626332 87.584 0.9285539984703064 88.616 0.92835211753845215
		 89.652 0.9280799627304076 90.688 0.92783641815185547 91.72 0.92767739295959473 92.756 0.92764359712600708
		 93.788 0.92769962549209595 94.824 0.92778187990188588 95.86 0.92780697345733631 96.892 0.92781221866607666
		 97.928 0.92785423994064331 98.964 0.92796522378921509 99.996 0.92797571420669556
		 101.032 0.92781925201416016 102.064 0.92755776643753052 103.1 0.92725503444671631
		 104.136 0.92696505784988403 105.168 0.92680794000625599 106.204 0.92667233943939209
		 107.24 0.92655116319656372 108.272 0.92647969722747803 109.308 0.92651104927062988
		 110.34 0.9265141487121582 111.376 0.92649495601654053 112.412 0.92652964591979992
		 113.444 0.92655760049819946 114.48 0.92659962177276622 115.512 0.92664563655853283
		 116.548 0.92668873071670532 117.584 0.92673188447952282 118.616 0.92672961950302113
		 119.652 0.92674714326858521 120.688 0.92678993940353394 121.72 0.92673313617706288
		 122.756 0.92669194936752308 123.788 0.92671763896942128 124.824 0.92694479227066029
		 125.86 0.92721134424209606 126.892 0.92739057540893555 127.928 0.92751389741897594
		 128.964 0.92754441499710083 129.996 0.92748391628265381 131.032 0.92734730243682872
		 132.064 0.92722427845001232 133.1 0.92722910642623912 134.136 0.92723292112350453
		 135.168 0.92725759744644165 136.204 0.92729383707046509 137.236 0.92742031812667847
		 138.272 0.92758935689926147 139.308 0.92772883176803578 140.34 0.92764472961425781
		 141.376 0.92735916376113892 142.412 0.92707836627960216 143.444 0.92701989412307739
		 144.48 0.92725479602813721 145.512 0.92751049995422363 146.548 0.9274701476097108
		 147.584 0.92717635631561279 148.616 0.9267507791519165 149.652 0.92627829313278198
		 150.688 0.92586785554885875 151.72 0.92571789026260387 152.756 0.92577445507049561
		 153.788 0.92581844329833984 154.824 0.92600297927856445 155.86 0.92631572484970082
		 156.892 0.92669397592544545 157.928 0.92711949348449707 158.964 0.9275701642036438
		 159.996 0.92808347940444935 161.032 0.92858988046646118 162.064 0.92905294895172108
		 163.1 0.92950791120529186 164.136 0.92999255657196056 165.168 0.9304470419883728
		 166.204 0.93086934089660656 167.236 0.93131214380264282 168.272 0.93168842792510975
		 169.308 0.93195950984954834 170.34 0.93212056159973145 171.376 0.93235099315643322
		 172.412 0.9326203465461731 173.444 0.93285322189331055 174.48 0.93303930759429932
		 175.512 0.93316173553466797 176.548 0.93323856592178345 177.584 0.93333208560943592
		 178.616 0.93343251943588257 179.652 0.93346375226974476 180.688 0.93344104290008534
		 181.72 0.93338519334793091 182.756 0.93331426382064819 183.788 0.93321102857589722
		 184.824 0.93317145109176647 185.86 0.93316698074340831 186.892 0.9331929087638855
		 187.928 0.93317121267318714 188.964 0.9330580234527589 189.996 0.93293368816375721
		 191.032 0.93281060457229614 192.064 0.93266421556472767 193.1 0.93251854181289673
		 194.136 0.93235653638839733 195.168 0.93218183517456055 196.204 0.9319899082183839
		 197.236 0.93182730674743652 198.272 0.93171632289886464 199.308 0.93160873651504517
		 200.34 0.93152302503585815 201.376 0.93144917488098145 202.412 0.93137073516845692
		 203.444 0.93129622936248779 204.48 0.93126040697097778 205.512 0.93122828006744385
		 206.548 0.93111246824264526 207.584 0.93101578950881969 208.616 0.93091171979904175
		 209.652 0.9307897686958313 210.688 0.93071311712265015 211.72 0.93072742223739624
		 212.756 0.93078833818435669 213.788 0.93082237243652333 214.824 0.9308847188949585
		 215.86 0.93089401721954346 216.892 0.93087446689605702 217.928 0.93084728717803955
		 218.964 0.93089795112609874 219.996 0.93110358715057373 221.032 0.93128234148025524
		 222.064 0.93145173788070679 223.1 0.93156188726425171 224.136 0.93173122406005859
		 225.168 0.93201148509979237 226.204 0.93223494291305542 227.236 0.93236172199249279
		 228.272 0.9325106143951416 229.308 0.93270647525787354 230.34 0.93290334939956665
		 231.376 0.93308353424072277 232.412 0.9332672953605653 233.444 0.93344730138778687
		 234.48 0.93360781669616688 235.512 0.93377357721328735 236.548 0.9339207410812379
		 237.584 0.93406343460083008 238.616 0.93414157629013062 239.652 0.93420588970184315
		 240.688 0.93427014350891113 241.72 0.93432992696762085 242.756 0.93433010578155518
		 243.788 0.93429768085479725 244.824 0.93426692485809326 245.86 0.93425464630126942
		 246.892 0.93426674604415894 247.928 0.93430644273757935 248.96 0.93432384729385376
		 249.996 0.93433558940887462 251.032 0.93433129787445057 252.064 0.93433362245559692
		 253.1 0.93435704708099365 254.136 0.93439626693725586 255.168 0.93442916870117188
		 256.204 0.93445432186126698 257.236 0.93451410531997681 258.272 0.9345766305923463;
	setAttr ".ktv[250:290]" 259.308 0.93462616205215454 260.34 0.93462949991226196
		 261.376 0.93463367223739613 262.412 0.93463009595870972 263.444 0.9346320629119873
		 264.48 0.93460845947265625 265.512 0.93461400270462036 266.548 0.93456608057022106
		 267.584 0.93449008464813232 268.616 0.93444818258285522 269.652 0.93441879749298096
		 270.688 0.93438595533370961 271.72 0.93435126543045033 272.756 0.93434751033782959
		 273.788 0.93429738283157349 274.824 0.9342883825302124 275.86 0.93428069353103627
		 276.892 0.93426269292831421 277.928 0.93423706293106079 278.96 0.93421357870101929
		 279.996 0.93417805433273327 281.032 0.93414920568466175 282.064 0.93408477306365978
		 283.1 0.93399769067764282 284.136 0.93396234512329102 285.168 0.93392789363861084
		 286.204 0.93382918834686279 287.236 0.9336639642715453 288.272 0.93349194526672363
		 289.308 0.93336737155914296 290.34 0.93324261903762817 291.376 0.93311971426010143
		 292.412 0.9329906702041626 293.444 0.93286943435668956 294.48 0.93278962373733509
		 295.512 0.93266534805297863 296.548 0.93255096673965443 297.584 0.93243849277496327
		 298.616 0.93235182762145996 299.652 0.9322354793548584 300.688 0.93209993839263905;
createNode animCurveTA -n "Bip01_L_Calf_rotateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 291 ".ktv";
	setAttr ".ktv[0:249]"  0.688 -0.075612850487232208 1.72 -0.074034295976161957
		 2.756 -0.073381818830966949 3.788 -0.07138708233833313 4.824 -0.068740159273147583
		 5.86 -0.066282838582992554 6.892 -0.064069300889968872 7.928 -0.062621183693408966
		 8.964 -0.061638090759515762 9.996 -0.059848122298717499 11.032 -0.056740965694189072
		 12.064 -0.053132783621549606 13.1 -0.049717243760824203 14.136 -0.046095613390207291
		 15.168 -0.041892342269420624 16.204 -0.037034746259450912 17.24 -0.031950648874044418
		 18.272 -0.027122180908918381 19.308 -0.02236352302134037 20.34 -0.017879297956824303
		 21.376 -0.013789314776659012 22.412 -0.0095396507531404495 23.444 -0.0050037638284265995
		 24.48 -0.00071112473960965872 25.512 0.0025713483337312937 26.548 0.0046506584621965885
		 27.584 0.0061985962092876434 28.616 0.0074270972982048988 29.652 0.007783978246152401
		 30.688 0.0067380503751337528 31.72 0.0057240212336182594 32.756 0.0055322763510048389
		 33.788 0.0055797877721488476 34.824 0.0055799935944378376 35.86 0.0044738291762769222
		 36.892 0.0038938180077821016 37.928 0.0045193275436758995 38.964 0.0062505248934030533
		 39.996 0.0086304238066077232 41.032 0.010712672024965286 42.064 0.012188699096441269
		 43.1 0.011298953555524349 44.136 0.0086657628417015076 45.168 0.0062719797715544701
		 46.204 0.0059472667053341866 47.24 0.0068241753615438938 48.272 0.0070502897724509248
		 49.308 0.0057199951261281967 50.34 0.0033366442658007145 51.376 0.00088839069940149784
		 52.412 -0.0015026853652670979 53.444 -0.0051379501819610596 54.48 -0.009474058635532856
		 55.512 -0.015626884996891022 56.548 -0.024197565391659737 57.584 -0.032269835472106934
		 58.616 -0.037167567759752274 59.652 -0.040134299546480179 60.688 -0.04348541796207428
		 61.72 -0.047613445669412613 62.756 -0.051959346979856491 63.788 -0.055650588124990463
		 64.824 -0.057642724364995963 65.86 -0.058671895414590836 66.892 -0.06069623306393624
		 67.928 -0.064005613327026367 68.964 -0.067691713571548462 69.996 -0.070815056562423706
		 71.032 -0.073184691369533539 72.064 -0.076178468763828278 73.1 -0.079841367900371552
		 74.136 -0.081989169120788574 75.168 -0.082176975905895233 76.204 -0.082714423537254333
		 77.24 -0.084427647292613983 78.272 -0.087759494781494141 79.308 -0.092218197882175446
		 80.34 -0.096870049834251404 81.376 -0.10093648731708527 82.412 -0.10422824323177338
		 83.444 -0.10718570649623871 84.48 -0.11035390943288803 85.512 -0.11359819024801253
		 86.548 -0.11594608426094055 87.584 -0.11626989394426347 88.616 -0.1176006868481636
		 89.652 -0.12008297443389893 90.688 -0.12229260802268982 91.72 -0.12347156554460526
		 92.756 -0.12400666624307632 93.788 -0.12382941693067551 94.824 -0.12333473563194274
		 95.86 -0.12307620048522949 96.892 -0.12283558398485184 97.928 -0.12227055430412292
		 98.964 -0.12156745046377182 99.996 -0.12162518501281737 101.032 -0.12299917638301849
		 102.064 -0.12517085671424866 103.1 -0.12769137322902679 104.136 -0.12998726963996887
		 105.168 -0.13099135458469391 106.204 -0.13174492120742798 107.24 -0.13263480365276337
		 108.272 -0.13294361531734467 109.308 -0.1325249969959259 110.34 -0.13220557570457458
		 111.376 -0.1322048157453537 112.412 -0.13200049102306366 113.444 -0.1319669783115387
		 114.48 -0.13187108933925629 115.512 -0.13146887719631195 116.548 -0.13094329833984375
		 117.584 -0.130512535572052 118.616 -0.13071483373641968 119.652 -0.13064073026180267
		 120.688 -0.13023808598518372 121.72 -0.13044314086437225 122.756 -0.13055704534053802
		 123.788 -0.13022434711456299 124.824 -0.12858064472675323 125.86 -0.12665398418903351
		 126.892 -0.12540110945701599 127.928 -0.12450939416885376 128.964 -0.12402516603469847
		 129.996 -0.12438324838876724 131.032 -0.12534421682357788 132.064 -0.12621667981147766
		 133.1 -0.12620960175991058 134.136 -0.12629294395446777 135.168 -0.12638196349143982
		 136.204 -0.1262216717004776 137.236 -0.12526997923851013 138.272 -0.12358274310827254
		 139.308 -0.12196874618530272 140.34 -0.12250372022390366 141.376 -0.1246012896299362
		 142.412 -0.12671740353107452 143.444 -0.12716788053512573 144.48 -0.12548863887786865
		 145.512 -0.12367228418588638 146.548 -0.12425772845745085 147.584 -0.12693524360656738
		 148.616 -0.130323126912117 149.652 -0.13412609696388245 150.688 -0.13711924850940704
		 151.72 -0.13816972076892853 152.756 -0.13787123560905457 153.788 -0.13756032288074493
		 154.824 -0.13624361157417297 155.86 -0.13421785831451416 156.892 -0.13140293955802917
		 157.928 -0.12782859802246094 158.964 -0.12373065948486328 159.996 -0.11926142126321791
		 161.032 -0.11474765092134477 162.064 -0.1105789914727211 163.1 -0.10612539201974869
		 164.136 -0.10103443264961243 165.168 -0.095968149602413177 166.204 -0.090999379754066467
		 167.236 -0.085617132484912872 168.272 -0.080255672335624695 169.308 -0.076330535113811493
		 170.34 -0.074143253266811371 171.376 -0.071113906800746918 172.412 -0.066852279007434845
		 173.444 -0.062325958162546158 174.48 -0.058314058929681771 175.512 -0.055735453963279724
		 176.548 -0.054449211806058884 177.584 -0.052570357918739319 178.616 -0.04975518211722374
		 179.652 -0.04782409593462944 180.688 -0.04781578853726387 181.72 -0.048935346305370331
		 182.756 -0.050300378352403641 183.788 -0.052023861557245255 184.824 -0.052529215812683105
		 185.86 -0.051607515662908554 186.892 -0.050506845116615295 187.928 -0.050522588193416595
		 188.964 -0.051831841468811035 189.996 -0.054056491702795029 191.032 -0.056389603763818734
		 192.064 -0.058654200285673141 193.1 -0.061069604009389884 194.136 -0.064187362790107727
		 195.168 -0.067884616553783417 196.204 -0.071565307676792145 197.236 -0.074329890310764313
		 198.272 -0.076285228133201599 199.308 -0.078135184943675995 200.34 -0.079662971198558807
		 201.376 -0.080751992762088776 202.412 -0.081942096352577209 203.444 -0.083229102194309235
		 204.48 -0.084271818399429321 205.512 -0.085194230079650879 206.548 -0.087018519639968872
		 207.584 -0.088299579918384552 208.616 -0.089625179767608643 209.652 -0.091115258634090424
		 210.688 -0.092162132263183594 211.72 -0.091959245502948761 212.756 -0.090968422591686249
		 213.788 -0.090226203203201294 214.824 -0.089541241526603699 215.86 -0.089298173785209656
		 216.892 -0.089087538421154022 217.928 -0.088653258979320526 218.964 -0.08756440132856369
		 219.996 -0.085138440132141113 221.032 -0.082721807062625885 222.064 -0.080443710088729858
		 223.1 -0.078778333961963654 224.136 -0.076304242014884949 225.168 -0.072182714939117432
		 226.204 -0.068224921822547913 227.236 -0.06582237035036087 228.272 -0.063625887036323547
		 229.308 -0.060379497706890113 230.34 -0.056567184627056129 231.376 -0.052776560187339783
		 232.412 -0.048801720142364502 233.444 -0.044518042355775833 234.48 -0.04021613672375679
		 235.512 -0.035687945783138275 236.548 -0.030445653945207592 237.584 -0.024542002007365227
		 238.616 -0.018326878547668457 239.652 -0.01264906395226717 240.688 -0.0080910651013255119
		 241.72 -0.0042768982239067554 242.756 -0.001473519136197865 243.788 -1.3101152035233099e-005
		 244.824 0.0012574647553265095 245.86 0.0030713328160345554 246.892 0.0047025042586028576
		 247.928 0.0057319486513733864 248.96 0.006637276615947485 249.996 0.0076533309184014797
		 251.032 0.0090751806274056435 252.064 0.01001637801527977 253.1 0.010627374053001404
		 254.136 0.010119630955159664 255.168 0.0083572175353765488 256.204 0.0060073519125580788
		 257.236 0.003371003083884716 258.272 0.00037365328171290457;
	setAttr ".ktv[250:290]" 259.308 -0.0023932161275297403 260.34 -0.0043464605696499348
		 261.376 -0.0071767298504710197 262.412 -0.010108527727425098 263.444 -0.012516172602772713
		 264.48 -0.014947148039937019 265.512 -0.017841283231973648 266.548 -0.021133016794919968
		 267.584 -0.02457362599670887 268.616 -0.027455659583210945 269.652 -0.029618183150887486
		 270.688 -0.031416602432727814 271.72 -0.032959457486867905 272.756 -0.033721547573804855
		 273.788 -0.034545324742794037 274.824 -0.035300072282552719 275.86 -0.035681363195180893
		 276.892 -0.036048457026481628 277.928 -0.03661380335688591 278.96 -0.037962354719638824
		 279.996 -0.039766538888216019 281.032 -0.041124213486909866 282.064 -0.041649375110864639
		 283.1 -0.042873211205005646 284.136 -0.043771199882030487 285.168 -0.044772766530513763
		 286.204 -0.047361910343170166 287.236 -0.051177095621824265 288.272 -0.054460838437080383
		 289.308 -0.056417204439640045 290.34 -0.058203674852848053 291.376 -0.060186121612787247
		 292.412 -0.062607370316982269 293.444 -0.064563050866127014 294.48 -0.065954931080341339
		 295.512 -0.067009702324867249 296.548 -0.068259641528129578 297.584 -0.069973714649677277
		 298.616 -0.071827612817287445 299.652 -0.073778778314590454 300.688 -0.075771793723106384;
createNode animCurveTA -n "Bip01_L_Calf_rotateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 291 ".ktv";
	setAttr ".ktv[0:249]"  0.688 -83.561332702636719 1.72 -83.613258361816406
		 2.756 -83.601692199707031 3.788 -83.676872253417969 4.824 -83.832122802734375 5.86 -83.995223999023438
		 6.892 -84.131912231445312 7.928 -84.221633911132813 8.964 -84.269660949707031 9.996 -84.371307373046875
		 11.032 -84.54754638671875 12.064 -84.795158386230469 13.1 -85.049034118652344 14.136 -85.288063049316406
		 15.168 -85.524154663085938 16.204 -85.798332214355469 17.24 -86.099578857421875 18.272 -86.396133422851562
		 19.308 -86.680824279785156 20.34 -86.956954956054688 21.376 -87.195022583007812 22.412 -87.445075988769531
		 23.444 -87.730171203613281 24.48 -88.008453369140625 25.512 -88.1759033203125 26.548 -88.282707214355469
		 27.584 -88.350875854492187 28.616 -88.403144836425781 29.652 -88.436332702636719
		 30.688 -88.402488708496094 31.72 -88.368057250976563 32.756 -88.371116638183594 33.788 -88.378189086914063
		 34.824 -88.385002136230469 35.86 -88.311134338378906 36.892 -88.289649963378906 37.928 -88.327079772949219
		 38.964 -88.423797607421875 39.996 -88.560745239257813 41.032 -88.674087524414063
		 42.064 -88.732963562011719 43.1 -88.634056091308594 44.136 -88.467437744140625 45.168 -88.30859375
		 46.204 -88.252960205078125 47.24 -88.284011840820313 48.272 -88.261344909667969 49.308 -88.122993469238281
		 50.34 -87.942558288574219 51.376 -87.777061462402344 52.412 -87.627197265625 53.444 -87.411842346191406
		 54.48 -87.142822265625 55.512 -86.7860107421875 56.548 -86.344795227050781 57.584 -85.957427978515625
		 58.616 -85.699913024902344 59.652 -85.519599914550781 60.688 -85.326446533203125
		 61.72 -85.097457885742188 62.756 -84.887664794921875 63.788 -84.701446533203125 64.824 -84.6009521484375
		 65.86 -84.549629211425781 66.892 -84.427589416503906 67.928 -84.219680786132813 68.964 -84.017112731933594
		 69.996 -83.855155944824219 71.032 -83.718528747558594 72.064 -83.567390441894531
		 73.1 -83.383010864257813 74.136 -83.264739990234375 75.168 -83.207618713378906 76.204 -83.105239868164063
		 77.24 -82.977264404296875 78.272 -82.824935913085938 79.308 -82.635971069335938 80.34 -82.409507751464844
		 81.376 -82.231925964355469 82.412 -82.084724426269531 83.444 -81.960853576660156
		 84.48 -81.840568542480469 85.512 -81.705131530761719 86.548 -81.580215454101563 87.584 -81.516502380371094
		 88.616 -81.396812438964844 89.652 -81.268692016601563 90.688 -81.1558837890625 91.72 -81.077644348144531
		 92.756 -81.068801879882813 93.788 -81.116363525390625 94.824 -81.1571044921875 95.86 -81.166969299316406
		 96.892 -81.158515930175781 97.928 -81.181358337402344 98.964 -81.236801147460938
		 99.996 -81.250221252441406 101.032 -81.173500061035156 102.064 -81.050926208496094
		 103.1 -80.919334411621094 104.136 -80.782608032226562 105.168 -80.707107543945313
		 106.204 -80.62744140625 107.24 -80.566421508789063 108.272 -80.5281982421875 109.308 -80.526618957519531
		 110.34 -80.514747619628906 111.376 -80.502952575683594 112.412 -80.516166687011719
		 113.444 -80.536170959472656 114.48 -80.569801330566406 115.512 -80.582542419433594
		 116.548 -80.59478759765625 117.584 -80.609825134277344 118.616 -80.616722106933594
		 119.652 -80.627975463867188 120.688 -80.647216796875 121.72 -80.613067626953125 122.756 -80.576805114746094
		 123.788 -80.583419799804688 124.824 -80.696723937988281 125.86 -80.82769775390625
		 126.892 -80.925102233886719 127.928 -80.977302551269531 128.964 -80.994682312011719
		 129.996 -80.955184936523438 131.032 -80.879379272460938 132.064 -80.813285827636719
		 133.1 -80.813011169433594 134.136 -80.826850891113281 135.168 -80.8497314453125 136.204 -80.877265930175781
		 137.236 -80.938156127929687 138.272 -81.013877868652344 139.308 -81.053321838378906
		 140.34 -80.998512268066406 141.376 -80.855751037597656 142.412 -80.712196350097656
		 143.444 -80.68475341796875 144.48 -80.805931091308594 145.512 -80.938896179199219
		 146.548 -80.936164855957031 147.584 -80.805870056152344 148.616 -80.619895935058594
		 149.652 -80.412933349609375 150.688 -80.232192993164063 151.72 -80.16845703125 152.756 -80.192253112792969
		 153.788 -80.217491149902344 154.824 -80.299766540527344 155.86 -80.453445434570313
		 156.892 -80.622886657714844 157.928 -80.808364868164062 158.964 -80.994354248046875
		 159.996 -81.232635498046875 161.032 -81.486770629882813 162.064 -81.723655700683594
		 163.1 -81.965011596679688 164.136 -82.221855163574219 165.168 -82.491539001464844
		 166.204 -82.744613647460938 167.236 -83.025375366210937 168.272 -83.284011840820312
		 169.308 -83.476387023925781 170.34 -83.594329833984375 171.376 -83.78070068359375
		 172.412 -84.000404357910156 173.444 -84.217201232910156 174.48 -84.392265319824219
		 175.512 -84.515434265136719 176.548 -84.595573425292969 177.584 -84.69488525390625
		 178.616 -84.820693969726563 179.652 -84.877182006835938 180.688 -84.858955383300781
		 181.72 -84.79205322265625 182.756 -84.709075927734375 183.788 -84.597785949707031
		 184.824 -84.55792236328125 185.86 -84.572845458984375 186.892 -84.605621337890625
		 187.928 -84.584831237792969 188.964 -84.474746704101563 189.996 -84.337997436523438
		 191.032 -84.209342956542969 192.064 -84.072151184082031 193.1 -83.927375793457031
		 194.136 -83.784774780273437 195.168 -83.634201049804687 196.204 -83.474044799804688
		 197.236 -83.347030639648438 198.272 -83.266632080078125 199.308 -83.193191528320313
		 200.34 -83.129539489746094 201.376 -83.081199645996094 202.412 -83.021171569824219
		 203.444 -82.97894287109375 204.48 -82.970001220703125 205.512 -82.943466186523438
		 206.548 -82.871421813964844 207.584 -82.819786071777344 208.616 -82.746322631835937
		 209.652 -82.676979064941406 210.688 -82.629257202148438 211.72 -82.640518188476563
		 212.756 -82.672950744628906 213.788 -82.689605712890625 214.824 -82.722732543945313
		 215.86 -82.732452392578125 216.892 -82.711441040039063 217.928 -82.681617736816406
		 218.964 -82.70941162109375 219.996 -82.839324951171875 221.032 -82.966880798339844
		 222.064 -83.075096130371094 223.1 -83.152519226074219 224.136 -83.281784057617188
		 225.168 -83.488929748535156 226.204 -83.677078247070313 227.236 -83.78961181640625
		 228.272 -83.913894653320313 229.308 -84.092361450195313 230.34 -84.294143676757812
		 231.376 -84.484954833984375 232.412 -84.690399169921875 233.444 -84.914787292480469
		 234.48 -85.139617919921875 235.512 -85.387962341308594 236.548 -85.664436340332031
		 237.584 -85.962173461914063 238.616 -86.264083862304688 239.652 -86.553504943847656
		 240.688 -86.820693969726563 241.72 -87.057083129882813 242.756 -87.210281372070312
		 243.788 -87.2633056640625 244.824 -87.298736572265625 245.86 -87.385116577148438
		 246.892 -87.4959716796875 247.928 -87.587059020996094 248.96 -87.660980224609375
		 249.996 -87.727691650390625 251.032 -87.813507080078125 252.064 -87.873825073242188
		 253.1 -87.929283142089844 254.136 -87.924400329589844 255.168 -87.852867126464844
		 256.204 -87.733169555664063 257.236 -87.625999450683594 258.272 -87.514724731445313;
	setAttr ".ktv[250:290]" 259.308 -87.406265258789063 260.34 -87.321098327636719
		 261.376 -87.187309265136719 262.412 -87.032646179199219 263.444 -86.926994323730469
		 264.48 -86.810104370117188 265.512 -86.686355590820313 266.548 -86.517707824707031
		 267.584 -86.341804504394531 268.616 -86.198806762695313 269.652 -86.111503601074219
		 270.688 -86.026580810546875 271.72 -85.958534240722656 272.756 -85.927604675292969
		 273.788 -85.876358032226563 274.824 -85.836326599121094 275.86 -85.822471618652344
		 276.892 -85.805610656738281 277.928 -85.762557983398438 278.96 -85.713920593261719
		 279.996 -85.645118713378906 281.032 -85.581298828125 282.064 -85.5191650390625 283.1 -85.417945861816406
		 284.136 -85.370582580566406 285.168 -85.322471618652344 286.204 -85.200813293457031
		 287.236 -84.997047424316406 288.272 -84.81439208984375 289.308 -84.689071655273437
		 290.34 -84.572822570800781 291.376 -84.450584411621094 292.412 -84.331275939941406
		 293.444 -84.225135803222656 294.48 -84.149734497070313 295.512 -84.039649963378906
		 296.548 -83.946426391601562 297.584 -83.859565734863281 298.616 -83.788200378417969
		 299.652 -83.693885803222656 300.688 -83.587875366210937;
createNode animCurveTA -n "Bip01_L_Foot_rotateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 291 ".ktv";
	setAttr ".ktv[0:249]"  0.688 -8.9299993515014648 1.72 -8.9304914474487305
		 2.756 -8.8869895935058594 3.788 -8.8720254898071289 4.824 -8.8777017593383789 5.86 -8.8609380722045898
		 6.892 -8.8577995300292969 7.928 -9.0979347229003906 8.964 -9.4084577560424805 9.996 -9.5496835708618164
		 11.032 -9.519770622253418 12.064 -9.4085931777954102 13.1 -9.3983068466186523 14.136 -9.505859375
		 15.168 -9.7006368637084961 16.204 -9.9124727249145508 17.24 -10.065113067626953 18.272 -10.108163833618164
		 19.308 -9.9055976867675781 20.34 -9.763026237487793 21.376 -9.8653659820556641 22.412 -10.159399032592773
		 23.444 -10.126823425292969 24.48 -9.8708877563476563 25.512 -9.8536205291748047 26.548 -10.094182968139648
		 27.584 -10.171672821044922 28.616 -10.151015281677246 29.652 -10.173488616943359
		 30.688 -10.260736465454102 31.72 -10.344710350036621 32.756 -10.399692535400391 33.788 -10.494404792785645
		 34.824 -10.623593330383301 35.86 -10.649767875671387 36.892 -10.531035423278809 37.928 -10.484540939331055
		 38.964 -10.501778602600098 39.996 -10.377419471740723 41.032 -10.166384696960449
		 42.064 -10.015579223632812 43.1 -9.960749626159668 44.136 -10.012065887451172 45.168 -9.9947195053100586
		 46.204 -9.7926969528198242 47.24 -9.4623231887817383 48.272 -9.1851444244384766 49.308 -9.1445016860961914
		 50.34 -9.187530517578125 51.376 -9.1159124374389648 52.412 -8.8515186309814453 53.444 -8.4948711395263672
		 54.48 -8.3622274398803711 55.512 -8.5240592956542969 56.548 -8.6415424346923828 57.584 -8.5737686157226563
		 58.616 -8.4630680084228516 59.652 -8.4586858749389648 60.688 -8.5254354476928711
		 61.72 -8.6624851226806641 62.756 -8.7729511260986328 63.788 -8.8211688995361328 64.824 -8.7572813034057617
		 65.86 -8.5942821502685547 66.892 -8.4532709121704102 67.928 -8.3374223709106445 68.964 -8.1944074630737305
		 69.996 -8.1694746017456055 71.032 -8.2417659759521484 72.064 -8.2320451736450195
		 73.1 -8.0376291275024414 74.136 -7.8071894645690927 75.168 -7.8604760169982919 76.204 -8.2105331420898437
		 77.24 -8.4315385818481445 78.272 -8.293980598449707 79.308 -8.2442512512207031 80.34 -8.4566612243652344
		 81.376 -8.5730142593383789 82.412 -8.479069709777832 83.444 -8.4409646987915039 84.48 -8.479374885559082
		 85.512 -8.485133171081543 86.548 -8.5228233337402344 87.584 -8.5619783401489258 88.616 -8.5349607467651367
		 89.652 -8.4259767532348633 90.688 -8.3838386535644531 91.72 -8.475438117980957 92.756 -8.6080493927001953
		 93.788 -8.5958156585693359 94.824 -8.6352195739746094 95.86 -8.6649513244628906 96.892 -8.6518983840942383
		 97.928 -8.5080709457397461 98.964 -8.4470052719116211 99.996 -8.6200504302978516
		 101.032 -8.6799154281616211 102.064 -8.5420627593994141 103.1 -8.4024009704589844
		 104.136 -8.4011325836181641 105.168 -8.3780984878540039 106.204 -8.238499641418457
		 107.24 -8.0914030075073242 108.272 -8.0375251770019531 109.308 -8.1279811859130859
		 110.34 -8.2126474380493164 111.376 -8.1692771911621094 112.412 -8.0419588088989258
		 113.444 -7.9022092819213876 114.48 -7.7728667259216309 115.512 -7.5895786285400391
		 116.548 -7.5029335021972665 117.584 -7.5421276092529297 118.616 -7.5634188652038583
		 119.652 -7.5435423851013184 120.688 -7.5984382629394531 121.72 -7.7302088737487793
		 122.756 -7.8402729034423837 123.788 -7.8296928405761719 124.824 -7.7299003601074219
		 125.86 -7.6874098777771005 126.892 -7.7350916862487802 127.928 -7.7521624565124521
		 128.964 -7.7791419029235849 129.996 -7.8692421913146964 131.032 -7.9820547103881836
		 132.064 -7.9958715438842773 133.1 -7.782954216003418 134.136 -7.4616398811340332
		 135.168 -7.243797779083252 136.204 -7.154080867767334 137.236 -7.2833719253540039
		 138.272 -7.5419745445251474 139.308 -7.7127346992492676 140.34 -7.7291049957275391
		 141.376 -7.9811849594116202 142.412 -8.2033557891845703 143.444 -8.0875511169433594
		 144.48 -7.8774709701538077 145.512 -7.8214950561523437 146.548 -7.6587471961975107
		 147.584 -7.4347491264343262 148.616 -7.2488069534301758 149.652 -7.1227974891662598
		 150.688 -7.210207462310791 151.72 -7.5295610427856454 152.756 -7.6353621482849121
		 153.788 -7.556795597076416 154.824 -7.5012626647949219 155.86 -7.5472655296325675
		 156.892 -7.6134567260742188 157.928 -7.6740927696228027 158.964 -7.8151812553405762
		 159.996 -7.9574484825134277 161.032 -8.0054845809936523 162.064 -8.0368766784667969
		 163.1 -8.1518402099609375 164.136 -8.2822551727294922 165.168 -8.2859535217285156
		 166.204 -8.2890119552612305 167.236 -8.4855461120605469 168.272 -8.7345190048217773
		 169.308 -8.893031120300293 170.34 -8.9624910354614258 171.376 -8.9875545501708984
		 172.412 -8.9787273406982422 173.444 -8.9507904052734375 174.48 -9.0544004440307617
		 175.512 -9.1580219268798828 176.548 -8.9761762619018555 177.584 -8.6062707901000977
		 178.616 -8.3645620346069336 179.652 -8.2457523345947266 180.688 -8.0876493453979492
		 181.72 -7.8658981323242187 182.756 -7.6828603744506845 183.788 -7.6235051155090341
		 184.824 -7.6095767021179199 185.86 -7.5366010665893555 186.892 -7.290956974029541
		 187.928 -7.1251492500305176 188.964 -7.2483267784118652 189.996 -7.4486818313598624
		 191.032 -7.572659969329834 192.064 -7.7168378829956055 193.1 -7.8736801147460937
		 194.136 -7.846942424774169 195.168 -7.612511157989502 196.204 -7.4167971611022949
		 197.236 -7.3030600547790518 198.272 -7.2142333984375009 199.308 -7.2255783081054696
		 200.34 -7.3451566696166992 201.376 -7.602816104888916 202.412 -7.8913702964782715
		 203.444 -8.0165929794311523 204.48 -7.9645891189575204 205.512 -7.8716011047363281
		 206.548 -7.7470436096191397 207.584 -7.7254338264465323 208.616 -7.7811117172241211
		 209.652 -7.7989811897277823 210.688 -7.8522386550903329 211.72 -7.9307293891906729
		 212.756 -7.9857258796691903 213.788 -8.0867757797241211 214.824 -8.2151260375976562
		 215.86 -8.2308864593505859 216.892 -8.2046146392822266 217.928 -8.1274375915527344
		 218.964 -8.0194768905639648 219.996 -8.0093860626220703 221.032 -8.036829948425293
		 222.064 -8.0599164962768555 223.1 -8.0427093505859375 224.136 -7.933537483215332
		 225.168 -7.7094874382019043 226.204 -7.5324416160583496 227.236 -7.5637073516845703
		 228.272 -7.6580877304077148 229.308 -7.6312761306762695 230.34 -7.6672606468200684
		 231.376 -7.7275648117065439 232.412 -7.6940140724182129 233.444 -7.668602943420411
		 234.48 -7.6905398368835449 235.512 -7.7145342826843262 236.548 -7.7890071868896484
		 237.584 -7.9439024925231942 238.616 -8.0754613876342773 239.652 -8.113245964050293
		 240.688 -7.993267536163331 241.72 -7.7748141288757333 242.756 -7.7224998474121103
		 243.788 -8.0118989944458008 244.824 -8.3896541595458984 245.86 -8.5526905059814453
		 246.892 -8.4513044357299805 247.928 -8.3122663497924805 248.96 -8.2981281280517578
		 249.996 -8.3922519683837891 251.032 -8.5124521255493164 252.064 -8.6698751449584961
		 253.1 -8.7463550567626953 254.136 -8.8539628982543945 255.168 -9.1180753707885742
		 256.204 -9.3743696212768555 257.236 -9.4929819107055664 258.272 -9.4579706192016602;
	setAttr ".ktv[250:290]" 259.308 -9.4380207061767578 260.34 -9.4369096755981445
		 261.376 -9.4968090057373047 262.412 -9.5728607177734375 263.444 -9.5076932907104492
		 264.48 -9.4694595336914062 265.512 -9.4482040405273437 266.548 -9.5123672485351562
		 267.584 -9.6362295150756836 268.616 -9.7185134887695313 269.652 -9.6778583526611328
		 270.688 -9.6473093032836914 271.72 -9.6709108352661133 272.756 -9.6065797805786133
		 273.788 -9.5266141891479492 274.824 -9.503753662109375 275.86 -9.6112174987792969
		 276.892 -9.7986268997192383 277.928 -9.8523540496826172 278.96 -9.9758005142211914
		 279.996 -10.094944953918457 281.032 -10.123558044433594 282.064 -10.073805809020996
		 283.1 -9.984705924987793 284.136 -9.9218330383300781 285.168 -9.7528839111328125
		 286.204 -9.6114692687988281 287.236 -9.6214132308959961 288.272 -9.7580928802490234
		 289.308 -9.8537387847900391 290.34 -9.8156709671020508 291.376 -9.6577777862548828
		 292.412 -9.6080942153930664 293.444 -9.5123214721679687 294.48 -9.2956514358520508
		 295.512 -9.2035846710205078 296.548 -9.255274772644043 297.584 -9.1036615371704102
		 298.616 -8.8522119522094727 299.652 -8.7128400802612305 300.688 -8.6752252578735352;
createNode animCurveTA -n "Bip01_L_Foot_rotateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 291 ".ktv";
	setAttr ".ktv[0:249]"  0.688 0.26982581615447998 1.72 0.28517916798591614
		 2.756 0.2546660304069519 3.788 0.22394916415214539 4.824 0.21127115190029144 5.86 0.18927358090877533
		 6.892 0.17629985511302948 7.928 0.3213331401348114 8.964 0.48052164912223821 9.996 0.5244370698928833
		 11.032 0.51041465997695923 12.064 0.44240498542785645 13.1 0.4307200014591217 14.136 0.48618698120117188
		 15.168 0.61417800188064575 16.204 0.70869898796081543 17.24 0.79755187034606934 18.272 0.79763740301132202
		 19.308 0.73567014932632446 20.34 0.55970776081085205 21.376 0.64205306768417358 22.412 0.8228452205657959
		 23.444 0.82097554206848145 24.48 0.65271025896072388 25.512 0.66842561960220337 26.548 0.79576528072357178
		 27.584 0.85480445623397827 28.616 0.82248282432556152 29.652 0.85541975498199463
		 30.688 0.90695279836654674 31.72 0.99008035659790039 32.756 1.0317829847335815 33.788 1.0882337093353271
		 34.824 1.1584073305130005 35.86 1.2082761526107788 36.892 1.1519637107849121 37.928 1.1344178915023804
		 38.964 1.1835910081863403 39.996 1.1771994829177856 41.032 1.0883065462112427 42.064 1.0336284637451172
		 43.1 1.0185939073562622 44.136 1.0650186538696289 45.168 1.0790389776229858 46.204 1.0236276388168335
		 47.24 0.87199461460113525 48.272 0.74158412218093872 49.308 0.72852575778961182 50.34 0.7926819920539856
		 51.376 0.78187423944473267 52.412 0.65494292974472046 53.444 0.47132676839828497
		 54.48 0.40076211094856262 55.512 0.51652604341506958 56.548 0.55495351552963257 57.584 0.48662340641021729
		 58.616 0.42584830522537231 59.652 0.4051862359046936 60.688 0.42572224140167236 61.72 0.50319349765777588
		 62.756 0.52739673852920532 63.788 0.58216625452041626 64.824 0.55018836259841919
		 65.86 0.4607068002223968 66.892 0.40409627556800842 67.928 0.34352144598960876 68.964 0.2811589241027832
		 69.996 0.29155978560447693 71.032 0.33528339862823486 72.064 0.32716882228851318
		 73.1 0.24549883604049683 74.136 0.11625965684652328 75.168 0.1758800745010376 76.204 0.37591716647148132
		 77.24 0.5052105188369751 78.272 0.41691774129867554 79.308 0.36144909262657166 80.34 0.45350009202957153
		 81.376 0.48324897885322571 82.412 0.41445633769035339 83.444 0.36512511968612671
		 84.48 0.35253810882568359 85.512 0.3360745906829834 86.548 0.31461024284362793 87.584 0.37693583965301514
		 88.616 0.34598025679588318 89.652 0.30197203159332275 90.688 0.26851022243499756
		 91.72 0.32928138971328735 92.756 0.37464931607246399 93.788 0.37284889817237854 94.824 0.34111866354942322
		 95.86 0.38412287831306458 96.892 0.36735966801643372 97.928 0.27107951045036316 98.964 0.25023850798606873
		 99.996 0.34060323238372803 101.032 0.37741005420684814 102.064 0.29745283722877502
		 103.1 0.22584651410579684 104.136 0.24082244932651517 105.168 0.22748029232025146
		 106.204 0.17386089265346527 107.24 0.09166543185710907 108.272 0.05418822169303894
		 109.308 0.15246114134788513 110.34 0.17281799018383026 111.376 0.17062707245349884
		 112.412 0.10839106887578964 113.444 0.043251875787973404 114.48 -0.028979649767279628
		 115.512 -0.12104082852602005 116.548 -0.14798106253147125 117.584 -0.1426207423210144
		 118.616 -0.12286151945590974 119.652 -0.16296277940273285 120.688 -0.10532574355602264
		 121.72 -0.046989433467388153 122.756 0.0065293163061141968 123.788 -0.017914645373821259
		 124.824 -0.081393353641033173 125.86 -0.10581497102975845 126.892 -0.12782756984233856
		 127.928 -0.11052116006612778 128.964 -0.10290156304836273 129.996 -0.058037988841533654
		 131.032 -0.015045543201267719 132.064 0.017172668129205704 133.1 -0.11908397078514098
		 134.136 -0.27362096309661865 135.168 -0.3919198215007782 136.204 -0.41831612586975098
		 137.236 -0.38755768537521362 138.272 -0.26916918158531189 139.308 -0.18360385298728943
		 140.34 -0.18476772308349609 141.376 -0.11468961089849472 142.412 0.043601293116807938
		 143.444 -0.042155560106039047 144.48 -0.13861437141895294 145.512 -0.18600687384605408
		 146.548 -0.26572015881538391 147.584 -0.39076054096221924 148.616 -0.49029609560966492
		 149.652 -0.56592506170272827 150.688 -0.52357560396194458 151.72 -0.35307514667510986
		 152.756 -0.29429933428764343 153.788 -0.33638262748718262 154.824 -0.37749451398849487
		 155.86 -0.35643962025642395 156.892 -0.32559186220169067 157.928 -0.28819862008094788
		 158.964 -0.21851849555969238 159.996 -0.15979035198688507 161.032 -0.14264318346977234
		 162.064 -0.11213398724794386 163.1 -0.05298297107219696 164.136 0.034115597605705261
		 165.168 0.024341501295566559 166.204 0.035930559039115906 167.236 0.14610965549945831
		 168.272 0.29789489507675171 169.308 0.38026788830757141 170.34 0.40107113122940063
		 171.376 0.42678186297416687 172.412 0.437946617603302 173.444 0.44420900940895081
		 174.48 0.52090597152709961 175.512 0.60627442598342896 176.548 0.52828866243362427
		 177.584 0.34660908579826355 178.616 0.22583922743797302 179.652 0.19377842545509338
		 180.688 0.13170817494392395 181.72 0.010542761534452438 182.756 -0.092548951506614685
		 183.788 -0.10016829520463943 184.824 -0.086521916091442108 185.86 -0.11286081373691559
		 186.892 -0.23465950787067413 187.928 -0.32894307374954224 188.964 -0.26016148924827576
		 189.996 -0.11693216860294342 191.032 -0.058280121535062797 192.064 0.00068731291685253382
		 193.1 0.10638526827096939 194.136 0.098921336233615875 195.168 -0.03075895830988884
		 196.204 -0.13232964277267456 197.236 -0.20377789437770844 198.272 -0.25212326645851135
		 199.308 -0.25410354137420654 200.34 -0.1886303722858429 201.376 -0.032859943807125092
		 202.412 0.076423853635787964 203.444 0.15917143225669861 204.48 0.14127442240715027
		 205.512 0.05631304532289505 206.548 -0.01933923177421093 207.584 -0.04275837168097496
		 208.616 -0.014622421935200691 209.652 -0.02010997012257576 210.688 0.0056032100692391396
		 211.72 0.041265137493610382 212.756 0.077613212168216705 213.788 0.13859482109546661
		 214.824 0.19276568293571472 215.86 0.22213585674762726 216.892 0.19970043003559113
		 217.928 0.14688713848590851 218.964 0.11167292296886444 219.996 0.09876144677400589
		 221.032 0.11804837733507156 222.064 0.13222587108612061 223.1 0.12868383526802063
		 224.136 0.080408714711666107 225.168 -0.037017360329627991 226.204 -0.12176044285297394
		 227.236 -0.1032092347741127 228.272 -0.066230058670043945 229.308 -0.053526174277067184
		 230.34 -0.036690827459096909 231.376 0.0031175236217677593 232.412 -0.0090989759191870689
		 233.444 -0.029074549674987796 234.48 -0.011006657034158707 235.512 0.0076438440009951583
		 236.548 0.052509065717458725 237.584 0.11744421720504762 238.616 0.18973350524902344
		 239.652 0.2039816826581955 240.688 0.14141735434532166 241.72 0.01564730703830719
		 242.756 -0.026044834405183792 243.788 0.13938035070896149 244.824 0.34992924332618713
		 245.86 0.44004550576210022 246.892 0.37844762206077576 247.928 0.29150566458702087
		 248.96 0.30104774236679077 249.996 0.33003425598144531 251.032 0.40701740980148315
		 252.064 0.4562107622623443 253.1 0.4976834654808045 254.136 0.56773585081100464 255.168 0.73031032085418701
		 256.204 0.888619065284729 257.236 0.93157458305358887 258.272 0.92739975452423107;
	setAttr ".ktv[250:290]" 259.308 0.90344101190567017 260.34 0.89884471893310547
		 261.376 0.9605506658554076 262.412 0.95008790493011475 263.444 0.90114301443099964
		 264.48 0.8438485860824585 265.512 0.81646269559860229 266.548 0.86045819520950317
		 267.584 0.92073583602905273 268.616 0.98001909255981445 269.652 0.9456489086151123
		 270.688 0.89438730478286743 271.72 0.90139412879943848 272.756 0.85963523387908936
		 273.788 0.74444276094436646 274.824 0.7075502872467041 275.86 0.80005842447280884
		 276.892 0.87626200914382935 277.928 0.94805210828781139 278.96 1.0047105550765991
		 279.996 1.0731288194656372 281.032 1.1072180271148682 282.064 1.0232371091842651
		 283.1 0.9549592137336731 284.136 0.92300856113433838 285.168 0.81146913766860962
		 286.204 0.73456013202667236 287.236 0.72985774278640747 288.272 0.83309692144393921
		 289.308 0.86546093225479126 290.34 0.8431708812713623 291.376 0.74691414833068848
		 292.412 0.71568983793258667 293.444 0.67354726791381836 294.48 0.53328609466552734
		 295.512 0.48627251386642456 296.548 0.49092552065849304 297.584 0.41281238198280334
		 298.616 0.25317230820655823 299.652 0.17101144790649414 300.688 0.13423821330070496;
createNode animCurveTA -n "Bip01_L_Foot_rotateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 291 ".ktv";
	setAttr ".ktv[0:249]"  0.688 -0.92103397846221924 1.72 -1.0359026193618774
		 2.756 -1.3220010995864868 3.788 -1.4623056650161743 4.824 -1.3304281234741211 5.86 -1.1098467111587524
		 6.892 -0.91741806268692028 7.928 -0.73335129022598267 8.964 -0.64639741182327271
		 9.996 -0.74398857355117798 11.032 -0.74074161052703857 12.064 -0.38987109065055847
		 13.1 0.12472171336412428 14.136 0.50690668821334839 15.168 0.69347864389419556 16.204 0.85099881887435913
		 17.24 1.1363852024078369 18.272 1.4627528190612793 19.308 1.6382206678390503 20.34 1.7036938667297363
		 21.376 1.8982722759246826 22.412 2.3173696994781494 23.444 2.7547376155853271 24.48 2.9314076900482178
		 25.512 2.9597527980804443 26.548 2.9976494312286377 27.584 2.9387125968933105 28.616 2.8116946220397949
		 29.652 2.8678066730499268 30.688 3.1903116703033447 31.72 3.4936144351959229 32.756 3.6030275821685791
		 33.788 3.6682276725769039 34.824 3.7095186710357666 35.86 3.668140172958374 36.892 3.6976101398468018
		 37.928 3.85682225227356 38.964 4.0501646995544434 39.996 4.3953957557678223 41.032 4.6870064735412598
		 42.064 4.6020669937133789 43.1 4.3063368797302246 44.136 4.0879554748535156 45.168 3.9499285221099858
		 46.204 3.8987350463867192 47.24 3.9573366641998291 48.272 3.880511999130249 49.308 3.557589054107666
		 50.34 3.243239164352417 51.376 3.1066069602966309 52.412 3.0171568393707275 53.444 2.8161575794219971
		 54.48 2.4276647567749023 55.512 2.0836765766143799 56.548 2.0390255451202393 57.584 1.9779791831970213
		 58.616 1.6203254461288452 59.652 1.1896284818649292 60.688 0.88469827175140381 61.72 0.66895687580108643
		 62.756 0.51681238412857056 63.788 0.47080001235008245 64.824 0.51875787973403931
		 65.86 0.5051078200340271 66.892 0.30632194876670837 67.928 0.083452597260475159 68.964 0.041265547275543213
		 69.996 0.071976743638515472 71.032 0.060165233910083778 72.064 0.11766167730093004
		 73.1 0.19645082950592041 74.136 0.11809938400983809 75.168 -0.15005026757717133 76.204 -0.51853722333908081
		 77.24 -0.7515680193901062 78.272 -0.74825656414031982 79.308 -0.72257804870605469
		 80.34 -0.76275086402893066 81.376 -0.75590896606445313 82.412 -0.73340189456939697
		 83.444 -0.79139304161071777 84.48 -0.87771075963973999 85.512 -0.88090759515762329
		 86.548 -0.96605461835861206 87.584 -1.324886679649353 88.616 -1.6873871088027954
		 89.652 -1.7767353057861328 90.688 -1.8685165643692017 91.72 -2.033017635345459 92.756 -1.9393620491027832
		 93.788 -1.7156364917755127 94.824 -1.7259854078292847 95.86 -1.9769135713577273 96.892 -2.3113617897033691
		 97.928 -2.525437593460083 98.964 -2.462515115737915 99.996 -2.3005125522613525 101.032 -2.316826343536377
		 102.064 -2.4509761333465576 103.1 -2.5185577869415283 104.136 -2.602121114730835
		 105.168 -2.8781957626342773 106.204 -3.2237935066223145 107.24 -3.4474225044250488
		 108.272 -3.5990352630615234 109.308 -3.7518718242645264 110.34 -3.9172995090484624
		 111.376 -4.0423336029052734 112.412 -4.0346765518188477 113.444 -3.8599224090576172
		 114.48 -3.7361419200897217 115.512 -3.8725423812866215 116.548 -4.0933208465576172
		 117.584 -4.0908222198486328 118.616 -3.9057831764221196 119.652 -3.8328256607055664
		 120.688 -3.8421804904937744 121.72 -3.8569636344909668 122.756 -3.9998092651367187
		 123.788 -4.1164355278015137 124.824 -4.0272436141967773 125.86 -3.8088855743408203
		 126.892 -3.6036276817321777 127.928 -3.4974420070648193 128.964 -3.4640889167785645
		 129.996 -3.4271466732025146 131.032 -3.4205543994903564 132.064 -3.4917807579040527
		 133.1 -3.5077805519104004 134.136 -3.419917106628418 135.168 -3.2779908180236816
		 136.204 -3.0817372798919678 137.236 -2.920219898223877 138.272 -2.991530179977417
		 139.308 -3.2774350643157959 140.34 -3.5850880146026616 141.376 -3.8474726676940922
		 142.412 -4.0590152740478516 143.444 -4.1455831527709961 144.48 -4.0427956581115723
		 145.512 -3.7783088684082031 146.548 -3.5857863426208496 147.584 -3.6602785587310791
		 148.616 -3.8619005680084224 149.652 -4.0182671546936035 150.688 -4.0732178688049316
		 151.72 -3.9484295845031738 152.756 -3.7634613513946533 153.788 -3.6721565723419185
		 154.824 -3.5201447010040283 155.86 -3.0676984786987305 156.892 -2.6868183612823486
		 157.928 -2.7328698635101318 158.964 -2.829021692276001 159.996 -2.6161038875579834
		 161.032 -2.2331171035766602 162.064 -1.8569934368133545 163.1 -1.5787208080291748
		 164.136 -1.4118673801422119 165.168 -1.292972207069397 166.204 -1.1374655961990356
		 167.236 -0.85729175806045532 168.272 -0.64235818386077881 169.308 -0.61990392208099365
		 170.34 -0.56826704740524292 171.376 -0.29181808233261108 172.412 -0.018757283687591553
		 173.444 0.031204255297780033 174.48 0.018528113141655922 175.512 0.22448447346687317
		 176.548 0.51381921768188477 177.584 0.55645114183425903 178.616 0.41963464021682739
		 179.652 0.32654598355293274 180.688 0.23507855832576752 181.72 0.067978605628013611
		 182.756 -0.10291609913110733 183.788 -0.1980903148651123 184.824 -0.27803555130958557
		 185.86 -0.4523179829120636 186.892 -0.72770655155181885 187.928 -1.0702071189880371
		 188.964 -1.3901543617248535 189.996 -1.5109254121780396 191.032 -1.5506255626678467
		 192.064 -1.7157151699066162 193.1 -1.8520616292953491 194.136 -1.7957584857940676
		 195.168 -1.6330856084823608 196.204 -1.5395296812057495 197.236 -1.5691176652908325
		 198.272 -1.5720669031143188 199.308 -1.4907674789428711 200.34 -1.4319409132003784
		 201.376 -1.3882238864898682 202.412 -1.2860956192016602 203.444 -1.1067382097244263
		 204.48 -0.9235888123512267 205.512 -0.82725250720977783 206.548 -0.79724091291427612
		 207.584 -0.82317966222763062 208.616 -0.91191154718399037 209.652 -0.91405415534973145
		 210.688 -0.84843289852142334 211.72 -0.81655204296112061 212.756 -0.90281575918197632
		 213.788 -1.0859267711639404 214.824 -1.0935385227203369 215.86 -1.0248442888259888
		 216.892 -1.3676447868347168 217.928 -1.9268487691879272 218.964 -2.1234714984893799
		 219.996 -2.0524957180023193 221.032 -1.9836100339889529 222.064 -1.9564008712768557
		 223.1 -1.9303483963012695 224.136 -1.9488632678985598 225.168 -2.1015002727508545
		 226.204 -2.2840151786804199 227.236 -2.2766106128692627 228.272 -2.0806820392608643
		 229.308 -1.8827970027923586 230.34 -1.7625670433044434 231.376 -1.6931482553482056
		 232.412 -1.6529240608215332 233.444 -1.5989660024642944 234.48 -1.4664227962493896
		 235.512 -1.3145173788070679 236.548 -1.2161910533905029 237.584 -1.1980171203613281
		 238.616 -1.2944731712341309 239.652 -1.3185000419616699 240.688 -1.0695693492889404
		 241.72 -0.7857171893119812 242.756 -0.69625180959701538 243.788 -0.67222416400909424
		 244.824 -0.66723519563674927 245.86 -0.619182288646698 246.892 -0.38975816965103149
		 247.928 -0.069134354591369629 248.96 0.1811516135931015 249.996 0.38054046034812927
		 251.032 0.56588160991668701 252.064 0.72551721334457397 253.1 0.92368078231811512
		 254.136 1.2331156730651855 255.168 1.5648627281188965 256.204 1.8229833841323853
		 257.236 2.096942663192749 258.272 2.485363245010376;
	setAttr ".ktv[250:290]" 259.308 2.7904894351959229 260.34 2.8490350246429443
		 261.376 2.8218834400177002 262.412 2.8192665576934814 263.444 2.8236021995544434
		 264.48 2.8748748302459717 265.512 2.9097957611083984 266.548 2.9190349578857422 267.584 2.9523298740386963
		 268.616 3.0370466709136963 269.652 3.1106855869293213 270.688 3.1082417964935303
		 271.72 3.0907671451568604 272.756 3.0635182857513428 273.788 2.8779623508453369 274.824 2.6852121353149414
		 275.86 2.7353074550628662 276.892 2.7754952907562256 277.928 2.7088527679443359 278.96 2.8604447841644287
		 279.996 3.2240705490112305 281.032 3.2406117916107178 282.064 2.7543988227844238
		 283.1 2.3202123641967773 284.136 2.1770210266113281 285.168 2.0816099643707275 286.204 2.0090587139129639
		 287.236 1.9832044839859009 288.272 1.9107476472854614 289.308 1.7320865392684937
		 290.34 1.468788743019104 291.376 1.2116115093231201 292.412 1.055145263671875 293.444 0.9489646553993224
		 294.48 0.72004884481430054 295.512 0.28633099794387817 296.548 -0.074976243078708649
		 297.584 -0.20439045131206512 298.616 -0.30300799012184143 299.652 -0.41956111788749695
		 300.688 -0.49428567290306091;
createNode animCurveTA -n "Bip01_R_Thigh_rotateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 291 ".ktv";
	setAttr ".ktv[0:249]"  0.688 9.1471529006958008 1.72 9.2004013061523437 2.756 9.1766729354858398
		 3.788 9.1083984375 4.824 9.0208492279052734 5.86 8.9356355667114258 6.892 8.8582353591918945
		 7.928 8.7321233749389648 8.964 8.5748653411865234 9.996 8.4433021545410156 11.032 8.3892335891723633
		 12.064 8.3533620834350586 13.1 8.3182544708251953 14.136 8.3058376312255859 15.168 8.3241968154907227
		 16.204 8.326533317565918 17.24 8.3057279586791992 18.272 8.2903871536254883 19.308 8.3250436782836914
		 20.34 8.3269157409667969 21.376 8.3113155364990234 22.412 8.3459444046020508 23.444 8.4454870223999023
		 24.48 8.5270538330078125 25.512 8.5869407653808594 26.548 8.6484537124633789 27.584 8.7078619003295898
		 28.616 8.7671070098876953 29.652 8.8290843963623047 30.688 8.9514541625976563 31.72 9.0297555923461914
		 32.756 9.0740833282470703 33.788 9.1135072708129883 34.824 9.1715364456176758 35.86 9.2604522705078125
		 36.892 9.3819198608398437 37.928 9.5629606246948242 38.964 9.8226175308227539 39.996 10.221063613891602
		 41.032 10.693930625915527 42.064 11.070766448974609 43.1 11.297323226928711 44.136 11.435102462768555
		 45.168 11.64671516418457 46.204 12.066512107849121 47.24 12.653006553649902 48.272 13.211333274841309
		 49.308 13.615475654602051 50.34 13.911373138427734 51.376 14.225875854492188 52.412 14.598627090454102
		 53.444 14.927159309387207 54.48 15.098965644836426 55.512 15.114497184753418 56.548 14.994682312011719
		 57.584 14.823122024536133 58.616 14.75387668609619 59.652 14.806729316711426 60.688 14.886123657226562
		 61.72 14.897324562072754 62.756 14.823353767395018 63.788 14.816154479980467 64.824 14.981719017028807
		 65.86 15.260267257690431 66.892 15.53631591796875 67.928 15.743803977966307 68.964 15.919885635375977
		 69.996 16.063619613647461 71.032 16.189802169799805 72.064 16.281681060791016 73.1 16.392002105712891
		 74.136 16.577045440673828 75.168 16.777839660644531 76.204 16.887887954711914 77.24 16.903051376342773
		 78.272 16.798822402954102 79.308 16.51225471496582 80.34 16.149763107299805 81.376 15.88311195373535
		 82.412 15.714707374572752 83.444 15.45353412628174 84.48 15.086206436157228 85.512 14.81170177459717
		 86.548 14.734848022460937 87.584 14.795883178710937 88.616 14.900685310363768 89.652 14.950231552124022
		 90.688 14.904755592346191 91.72 14.783821105957031 92.756 14.706817626953125 93.788 14.661218643188477
		 94.824 14.599583625793455 95.86 14.526704788208008 96.892 14.487131118774414 97.928 14.479249000549318
		 98.964 14.505805015563965 99.996 14.606714248657225 101.032 14.796196937561035 102.064 15.012399673461914
		 103.1 15.173373222351076 104.136 15.288871765136719 105.168 15.420968055725099 106.204 15.573937416076662
		 107.24 15.721319198608398 108.272 15.838251113891602 109.308 15.970360755920408 110.34 16.113235473632812
		 111.376 16.225742340087891 112.412 16.275766372680664 113.444 16.310491561889648
		 114.48 16.321582794189453 115.512 16.263772964477539 116.548 16.119464874267578 117.584 15.944009780883791
		 118.616 15.769116401672363 119.652 15.596504211425781 120.688 15.399425506591797
		 121.72 15.13703727722168 122.756 14.861207008361818 123.788 14.616644859313965 124.824 14.40328311920166
		 125.86 14.18071460723877 126.892 13.957791328430176 127.928 13.713489532470703 128.964 13.463846206665039
		 129.996 13.210240364074707 131.032 12.947606086730957 132.064 12.760746002197266
		 133.1 12.699219703674316 134.136 12.648567199707031 135.168 12.553654670715332 136.204 12.452773094177246
		 137.236 12.372149467468262 138.272 12.285214424133301 139.308 12.216652870178223
		 140.34 12.113421440124512 141.376 11.997295379638672 142.412 11.927626609802246 143.444 11.930727958679199
		 144.48 11.975820541381836 145.512 12.020052909851074 146.548 12.010668754577637 147.584 11.975438117980957
		 148.616 11.972132682800293 149.652 11.990286827087402 150.688 12.026077270507813
		 151.72 12.052414894104004 152.756 12.045939445495605 153.788 12.016365051269531 154.824 12.013017654418945
		 155.86 12.012170791625977 156.892 11.984918594360352 157.928 11.98050594329834 158.964 11.942435264587402
		 159.996 11.898557662963867 161.032 11.885402679443359 162.064 11.896427154541016
		 163.1 11.882821083068848 164.136 11.857809066772461 165.168 11.836525917053223 166.204 11.826881408691406
		 167.236 11.857932090759277 168.272 11.85440731048584 169.308 11.664978981018066 170.34 11.383930206298828
		 171.376 11.269046783447266 172.412 11.341296195983887 173.444 11.431195259094238
		 174.48 11.470373153686523 175.512 11.529126167297363 176.548 11.607168197631836 177.584 11.702627182006836
		 178.616 11.863368988037109 179.652 12.029874801635742 180.688 12.088696479797363
		 181.72 12.057182312011719 182.756 12.052510261535645 183.788 12.096578598022461 184.824 12.153009414672852
		 185.86 12.209659576416016 186.892 12.295869827270508 187.928 12.367382049560547 188.964 12.374048233032227
		 189.996 12.358720779418945 191.032 12.380537033081055 192.064 12.434604644775391
		 193.1 12.50001049041748 194.136 12.582805633544922 195.168 12.667172431945801 196.204 12.765226364135742
		 197.236 12.851669311523437 198.272 12.913332939147949 199.308 12.981866836547852
		 200.34 13.089401245117188 201.376 13.185528755187988 202.412 13.250675201416016 203.444 13.31832218170166
		 204.48 13.394682884216309 205.512 13.454037666320801 206.548 13.509061813354492 207.584 13.560711860656738
		 208.616 13.633685111999512 209.652 13.713379859924316 210.688 13.790683746337891
		 211.72 13.856363296508789 212.756 13.898027420043945 213.788 13.936563491821289 214.824 13.960960388183594
		 215.86 13.970099449157715 216.892 13.977407455444336 217.928 14.002842903137207 218.964 14.030519485473633
		 219.996 14.081718444824219 221.032 14.153435707092285 222.064 14.239823341369629
		 223.1 14.304628372192383 224.136 14.375143051147463 225.168 14.46146297454834 226.204 14.553615570068358
		 227.236 14.61536121368408 228.272 14.638174057006836 229.308 14.689932823181151 230.34 14.787139892578125
		 231.376 14.909906387329102 232.412 15.016074180603029 233.444 15.124032020568848
		 234.48 15.23018741607666 235.512 15.32621955871582 236.548 15.400354385375975 237.584 15.481319427490234
		 238.616 15.60366153717041 239.652 15.73157787322998 240.688 15.877333641052246 241.72 16.027872085571289
		 242.756 16.164325714111328 243.788 16.262119293212891 244.824 16.355842590332031
		 245.86 16.494113922119141 246.892 16.623996734619141 247.928 16.687736511230469 248.96 16.688665390014648
		 249.996 16.681615829467773 251.032 16.672996520996094 252.064 16.65350341796875 253.1 16.606224060058594
		 254.136 16.538715362548828 255.168 16.493730545043945 256.204 16.48643684387207 257.236 16.427820205688477
		 258.272 16.305995941162109;
	setAttr ".ktv[250:290]" 259.308 16.145145416259766 260.34 15.996329307556152
		 261.376 15.835314750671385 262.412 15.656363487243654 263.444 15.46625232696533 264.48 15.292068481445314
		 265.512 15.136244773864746 266.548 14.971582412719727 267.584 14.823107719421387
		 268.616 14.684618949890135 269.652 14.533794403076172 270.688 14.391194343566895
		 271.72 14.250246047973633 272.756 14.105091094970703 273.788 13.966643333435059 274.824 13.823209762573242
		 275.86 13.662344932556152 276.892 13.527667045593262 277.928 13.438751220703125 278.96 13.363387107849121
		 279.996 13.27403450012207 281.032 13.1937255859375 282.064 13.136898040771484 283.1 13.101996421813965
		 284.136 13.101832389831543 285.168 13.105055809020996 286.204 13.088362693786621
		 287.236 13.058200836181641 288.272 13.050673484802246 289.308 13.081696510314941
		 290.34 13.13554573059082 291.376 13.190638542175293 292.412 13.207071304321289 293.444 13.231100082397461
		 294.48 13.299607276916504 295.512 13.41815185546875 296.548 13.512640953063965 297.584 13.503201484680176
		 298.616 13.436183929443359 299.652 13.395831108093262 300.688 13.402523994445801;
createNode animCurveTA -n "Bip01_R_Thigh_rotateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 291 ".ktv";
	setAttr ".ktv[0:249]"  0.688 168.89961242675781 1.72 169.38468933105469 2.756 169.78509521484375
		 3.788 170.01242065429687 4.824 170.10308837890625 5.86 170.13302612304688 6.892 170.11129760742187
		 7.928 170.00157165527344 8.964 169.8197021484375 9.996 169.61575317382812 11.032 169.42428588867187
		 12.064 169.22830200195312 13.1 169.00863647460937 14.136 168.79315185546875 15.168 168.60916137695312
		 16.204 168.42228698730469 17.24 168.22708129882812 18.272 168.04521179199219 19.308 167.90518188476562
		 20.34 167.75877380371094 21.376 167.59336853027344 22.412 167.49102783203125 23.444 167.43838500976562
		 24.48 167.38911437988281 25.512 167.32533264160156 26.548 167.24917602539062 27.584 167.19366455078125
		 28.616 167.12422180175781 29.652 167.04808044433594 30.688 166.95292663574219 31.72 166.87214660644531
		 32.756 166.76596069335938 33.788 166.64080810546875 34.824 166.51490783691406 35.86 166.40321350097656
		 36.892 166.29083251953125 37.928 166.19000244140625 38.964 166.13514709472656 39.996 166.19129943847656
		 41.032 166.34126281738281 42.064 166.48626708984375 43.1 166.564208984375 44.136 166.64248657226562
		 45.168 166.81015014648437 46.204 167.09962463378906 47.24 167.46247863769531 48.272 167.79371643066406
		 49.308 168.02728271484375 50.34 168.18553161621094 51.376 168.36302185058594 52.412 168.59844970703125
		 53.444 168.75666809082031 54.48 168.7406005859375 55.512 168.53915405273437 56.548 168.16545104980469
		 57.584 167.70164489746094 58.616 167.29190063476562 59.652 166.97273254394531 60.688 166.67829895019531
		 61.72 166.31417846679687 62.756 165.84017944335938 63.788 165.46470642089844 64.824 165.33039855957031
		 65.86 165.34600830078125 66.892 165.38998413085937 67.928 165.441162109375 68.964 165.49354553222656
		 69.996 165.55058288574219 71.032 165.58282470703125 72.064 165.57647705078125 73.1 165.61087036132812
		 74.136 165.75796508789063 75.168 165.95401000976562 76.204 166.06463623046875 77.24 166.06625366210937
		 78.272 165.92605590820312 79.308 165.529052734375 80.34 164.99012756347656 81.376 164.56297302246094
		 82.412 164.23760986328125 83.444 163.76210021972656 84.48 163.12113952636719 85.512 162.61430358886719
		 86.548 162.4327392578125 87.584 162.46580505371094 88.616 162.51713562011719 89.652 162.50019836425781
		 90.688 162.37132263183594 91.72 162.14521789550781 92.756 161.82073974609375 93.788 161.47990417480469
		 94.824 161.15048217773437 95.86 160.85693359375 96.892 160.61317443847656 97.928 160.38346862792969
		 98.964 160.11653137207031 99.996 159.85838317871094 101.032 159.72500610351562 102.064 159.70639038085937
		 103.1 159.67678833007812 104.136 159.5799560546875 105.168 159.47518920898437 106.204 159.41445922851562
		 107.24 159.37828063964844 108.272 159.34718322753906 109.308 159.33306884765625 110.34 159.38038635253906
		 111.376 159.44598388671875 112.412 159.49090576171875 113.444 159.53692626953125
		 114.48 159.63429260253906 115.512 159.78778076171875 116.548 159.96342468261719 117.584 160.12266540527344
		 118.616 160.27227783203125 119.652 160.47201538085937 120.688 160.71571350097656
		 121.72 161.03341674804687 122.756 161.41046142578125 123.788 161.70281982421875 124.824 161.8116455078125
		 125.86 161.83494567871094 126.892 161.91563415527344 127.928 162.14561462402344 128.964 162.51034545898437
		 129.996 162.93826293945312 131.032 163.3101806640625 132.064 163.67327880859375 133.1 164.05364990234375
		 134.136 164.35748291015625 135.168 164.5018310546875 136.204 164.55949401855469 137.236 164.57701110839844
		 138.272 164.56019592285156 139.308 164.53916931152344 140.34 164.51788330078125 141.376 164.47174072265625
		 142.412 164.41050720214844 143.444 164.33134460449219 144.48 164.21647644042969 145.512 164.11262512207031
		 146.548 164.06526184082031 147.584 164.06341552734375 148.616 164.07920837402344
		 149.652 164.10621643066406 150.688 164.1488037109375 151.72 164.19325256347656 152.756 164.25204467773437
		 153.788 164.34756469726562 154.824 164.48599243164062 155.86 164.61717224121094 156.892 164.74681091308594
		 157.928 164.90025329589844 158.964 165.081298828125 159.996 165.30154418945312 161.032 165.62730407714844
		 162.064 165.99923706054687 163.1 166.34562683105469 164.136 166.69401550292969 165.168 167.06330871582031
		 166.204 167.46835327148437 167.236 167.92610168457031 168.272 168.3759765625 169.308 168.64002990722656
		 170.34 168.80325317382812 171.376 169.12567138671875 172.412 169.61959838867187 173.444 170.09629821777344
		 174.48 170.52902221679687 175.512 170.97134399414062 176.548 171.42138671875 177.584 171.86151123046875
		 178.616 172.32098388671875 179.652 172.76052856445312 180.688 173.0677490234375 181.72 173.25860595703125
		 182.756 173.44004821777344 183.788 173.63414001464844 184.824 173.76174926757812
		 185.86 173.87313842773437 186.892 173.97451782226562 187.928 173.98820495605469 188.964 173.89126586914062
		 189.996 173.72537231445312 191.032 173.5494384765625 192.064 173.34840393066406 193.1 173.11488342285156
		 194.136 172.85867309570312 195.168 172.57499694824219 196.204 172.26509094238281
		 197.236 171.92755126953125 198.272 171.54896545410156 199.308 171.16839599609375
		 200.34 170.80567932128906 201.376 170.44052124023437 202.412 170.03579711914062 203.444 169.63134765625
		 204.48 169.2669677734375 205.512 168.92178344726562 206.548 168.611083984375 207.584 168.33261108398437
		 208.616 168.08399963378906 209.652 167.88798522949219 210.688 167.736083984375 211.72 167.61465454101562
		 212.756 167.5108642578125 213.788 167.42884826660156 214.824 167.37556457519531 215.86 167.34539794921875
		 216.892 167.36149597167969 217.928 167.42829895019531 218.964 167.51426696777344
		 219.996 167.61746215820313 221.032 167.75550842285156 222.064 167.90443420410156
		 223.1 168.03584289550781 224.136 168.17819213867187 225.168 168.34480285644531 226.204 168.51448059082031
		 227.236 168.6273193359375 228.272 168.70436096191406 229.308 168.80079650878906 230.34 168.93745422363281
		 231.376 169.08128356933594 232.412 169.19709777832031 233.444 169.29611206054687
		 234.48 169.39091491699219 235.512 169.46455383300781 236.548 169.5189208984375 237.584 169.574462890625
		 238.616 169.66143798828125 239.652 169.75227355957031 240.688 169.8553466796875 241.72 169.96214294433594
		 242.756 170.04818725585938 243.788 170.11727905273438 244.824 170.1907958984375 245.86 170.31158447265625
		 246.892 170.42951965332031 247.928 170.50578308105469 248.96 170.53677368164062 249.996 170.58157348632812
		 251.032 170.63642883300781 252.064 170.68934631347656 253.1 170.71878051757813 254.136 170.71946716308594
		 255.168 170.75575256347656 256.204 170.83953857421875 257.236 170.87199401855469
		 258.272 170.83892822265625;
	setAttr ".ktv[250:290]" 259.308 170.78231811523438 260.34 170.73513793945312
		 261.376 170.67752075195312 262.412 170.60438537597656 263.444 170.50445556640625
		 264.48 170.43142700195312 265.512 170.36543273925781 266.548 170.30941772460937 267.584 170.2454833984375
		 268.616 170.21247863769531 269.652 170.17286682128906 270.688 170.14529418945313
		 271.72 170.12413024902344 272.756 170.09675598144531 273.788 170.06150817871094 274.824 170.0296630859375
		 275.86 169.96875 276.892 169.91096496582031 277.928 169.887451171875 278.96 169.85816955566406
		 279.996 169.78868103027344 281.032 169.71366882324219 282.064 169.62783813476562
		 283.1 169.55686950683594 284.136 169.49235534667969 285.168 169.41581726074219 286.204 169.28907775878906
		 287.236 169.13047790527344 288.272 168.98895263671875 289.308 168.88677978515625
		 290.34 168.80287170410156 291.376 168.70722961425781 292.412 168.58599853515625 293.444 168.46920776367187
		 294.48 168.39097595214844 295.512 168.34912109375 296.548 168.29530334472656 297.584 168.14836120605469
		 298.616 167.94212341308594 299.652 167.76634216308594 300.688 167.62918090820312;
createNode animCurveTA -n "Bip01_R_Thigh_rotateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 291 ".ktv";
	setAttr ".ktv[0:249]"  0.688 -90.133872985839844 1.72 -91.304542541503906
		 2.756 -92.631675720214844 3.788 -93.646339416503906 4.824 -94.288215637207031 5.86 -94.792716979980469
		 6.892 -95.261871337890625 7.928 -95.712692260742187 8.964 -96.108131408691406 9.996 -96.358200073242188
		 11.032 -96.498931884765625 12.064 -96.589500427246094 13.1 -96.638267517089844 14.136 -96.640945434570313
		 15.168 -96.65411376953125 16.204 -96.712493896484375 17.24 -96.777191162109375 18.272 -96.799705505371094
		 19.308 -96.804924011230469 20.34 -96.792488098144531 21.376 -96.748641967773437 22.412 -96.655204772949219
		 23.444 -96.543571472167969 24.48 -96.441200256347656 25.512 -96.314376831054688 26.548 -96.153678894042969
		 27.584 -96.021553039550781 28.616 -95.872245788574219 29.652 -95.630157470703125
		 30.688 -95.244964599609375 31.72 -94.949295043945313 32.756 -94.733245849609375 33.788 -94.451126098632813
		 34.824 -94.118637084960938 35.86 -93.738304138183594 36.892 -93.212844848632813 37.928 -92.467727661132813
		 38.964 -91.579879760742188 39.996 -90.565353393554688 41.032 -89.557243347167969
		 42.064 -88.805023193359375 43.1 -88.37103271484375 44.136 -88.245437622070313 45.168 -88.14434814453125
		 46.204 -87.6636962890625 47.24 -86.7396240234375 48.272 -85.755043029785156 49.308 -85.025604248046875
		 50.34 -84.53216552734375 51.376 -84.127571105957031 52.412 -83.70928955078125 53.444 -83.270683288574219
		 54.48 -82.88726806640625 55.512 -82.531547546386719 56.548 -82.167716979980469 57.584 -81.764900207519531
		 58.616 -81.180427551269531 59.652 -80.414436340332031 60.688 -79.689422607421875
		 61.72 -78.969795227050781 62.756 -78.212379455566406 63.788 -77.512069702148438 64.824 -76.857574462890625
		 65.86 -76.140892028808594 66.892 -75.494758605957031 67.928 -75.041427612304688 68.964 -74.711174011230469
		 69.996 -74.39605712890625 71.032 -74.044082641601563 72.064 -73.641647338867188 73.1 -73.222953796386719
		 74.136 -72.802459716796875 75.168 -72.41754150390625 76.204 -72.127342224121094 77.24 -71.988578796386719
		 78.272 -71.938117980957031 79.308 -71.833152770996094 80.34 -71.630020141601562 81.376 -71.378120422363281
		 82.412 -71.113876342773437 83.444 -70.813728332519531 84.48 -70.480827331542969 85.512 -70.246253967285156
		 86.548 -70.216659545898437 87.584 -70.264968872070313 88.616 -70.243629455566406
		 89.652 -70.257095336914062 90.688 -70.424545288085938 91.72 -70.541893005371094 92.756 -70.303764343261719
		 93.788 -69.875099182128906 94.824 -69.6373291015625 95.86 -69.604705810546875 96.892 -69.610443115234375
		 97.928 -69.559516906738281 98.964 -69.322509765625 99.996 -68.893684387207031 101.032 -68.502883911132813
		 102.064 -68.346336364746094 103.1 -68.37237548828125 104.136 -68.397537231445312
		 105.168 -68.31787109375 106.204 -68.217300415039062 107.24 -68.179161071777344 108.272 -68.168258666992188
		 109.308 -68.089302062988281 110.34 -68.001113891601562 111.376 -67.969314575195313
		 112.412 -67.949531555175781 113.444 -67.841453552246094 114.48 -67.817001342773438
		 115.512 -68.055755615234375 116.548 -68.497146606445313 117.584 -68.885513305664063
		 118.616 -69.147407531738281 119.652 -69.371498107910156 120.688 -69.754859924316406
		 121.72 -70.512382507324219 122.756 -71.431564331054687 123.788 -71.925224304199219
		 124.824 -71.789817810058594 125.86 -71.43243408203125 126.892 -71.325393676757813
		 127.928 -71.634498596191406 128.964 -72.4334716796875 129.996 -73.606239318847656
		 131.032 -74.791908264160156 132.064 -75.7686767578125 133.1 -76.54827880859375 134.136 -77.103622436523437
		 135.168 -77.476661682128906 136.204 -77.734840393066406 137.236 -77.950431823730469
		 138.272 -78.187080383300781 139.308 -78.548866271972656 140.34 -79.125114440917969
		 141.376 -79.77703857421875 142.412 -80.250869750976563 143.444 -80.383888244628906
		 144.48 -80.293869018554687 145.512 -80.27130126953125 146.548 -80.611557006835938
		 147.584 -81.200508117675781 148.616 -81.7239990234375 149.652 -82.084197998046875
		 150.688 -82.359207153320313 151.72 -82.600448608398438 152.756 -82.913932800292969
		 153.788 -83.324508666992188 154.824 -83.676559448242188 155.86 -83.876686096191406
		 156.892 -83.999359130859375 157.928 -84.044677734375 158.964 -84.12554931640625 159.996 -84.322044372558594
		 161.032 -84.615303039550781 162.064 -84.850448608398438 163.1 -84.995719909667969
		 164.136 -85.100410461425781 165.168 -85.171958923339844 166.204 -85.217506408691406
		 167.236 -85.274330139160156 168.272 -85.350967407226563 169.308 -85.477653503417969
		 170.34 -85.590843200683594 171.376 -85.581825256347656 172.412 -85.425033569335938
		 173.444 -85.22125244140625 174.48 -85.068611145019531 175.512 -84.920997619628906
		 176.548 -84.789993286132813 177.584 -84.641311645507813 178.616 -84.411979675292969
		 179.652 -84.115875244140625 180.688 -83.969520568847656 181.72 -84.012359619140625
		 182.756 -84.081642150878906 183.788 -84.077682495117188 184.824 -84.072303771972656
		 185.86 -84.119705200195312 186.892 -84.162841796875 187.928 -84.1552734375 188.964 -84.186637878417969
		 189.996 -84.219207763671875 191.032 -84.18609619140625 192.064 -84.104949951171875
		 193.1 -84.020065307617188 194.136 -83.884963989257813 195.168 -83.711967468261719
		 196.204 -83.567741394042969 197.236 -83.488731384277344 198.272 -83.475997924804687
		 199.308 -83.498184204101563 200.34 -83.5194091796875 201.376 -83.531845092773438
		 202.412 -83.523483276367188 203.444 -83.516273498535156 204.48 -83.541572570800781
		 205.512 -83.705574035644531 206.548 -83.971977233886719 207.584 -84.21319580078125
		 208.616 -84.395263671875 209.652 -84.594810485839844 210.688 -84.8411865234375 211.72 -85.109947204589844
		 212.756 -85.357635498046875 213.788 -85.584175109863281 214.824 -85.826271057128906
		 215.86 -86.071441650390625 216.892 -86.38153076171875 217.928 -86.726211547851562
		 218.964 -87.010543823242188 219.996 -87.190498352050781 221.032 -87.311256408691406
		 222.064 -87.38958740234375 223.1 -87.469635009765625 224.136 -87.549766540527344
		 225.168 -87.650039672851562 226.204 -87.744071960449219 227.236 -87.776145935058594
		 228.272 -87.827980041503906 229.308 -87.920783996582031 230.34 -88.022613525390625
		 231.376 -88.07501220703125 232.412 -88.126808166503906 233.444 -88.160118103027344
		 234.48 -88.193557739257813 235.512 -88.273773193359375 236.548 -88.368736267089844
		 237.584 -88.460205078125 238.616 -88.530174255371094 239.652 -88.566925048828125
		 240.688 -88.564407348632812 241.72 -88.517333984375 242.756 -88.423652648925781 243.788 -88.308029174804688
		 244.824 -88.189071655273438 245.86 -88.053756713867188 246.892 -87.889862060546875
		 247.928 -87.731887817382813 248.96 -87.607795715332031 249.996 -87.516685485839844
		 251.032 -87.436500549316406 252.064 -87.318092346191406 253.1 -87.162689208984375
		 254.136 -86.988410949707031 255.168 -86.818855285644531 256.204 -86.608924865722656
		 257.236 -86.434127807617188 258.272 -86.297554016113281;
	setAttr ".ktv[250:290]" 259.308 -86.216300964355469 260.34 -86.166435241699219
		 261.376 -86.12255859375 262.412 -86.096687316894531 263.444 -86.087669372558594 264.48 -86.046089172363281
		 265.512 -85.992790222167969 266.548 -85.9522705078125 267.584 -85.90478515625 268.616 -85.8641357421875
		 269.652 -85.890281677246094 270.688 -85.952110290527344 271.72 -86.031173706054687
		 272.756 -86.12725830078125 273.788 -86.236709594726563 274.824 -86.373008728027344
		 275.86 -86.501060485839844 276.892 -86.593841552734375 277.928 -86.685096740722656
		 278.96 -86.781318664550781 279.996 -86.861175537109375 281.032 -86.94049072265625
		 282.064 -86.9573974609375 283.1 -86.95037841796875 284.136 -86.957321166992188 285.168 -86.95849609375
		 286.204 -86.902130126953125 287.236 -86.802490234375 288.272 -86.684196472167969
		 289.308 -86.621490478515625 290.34 -86.575157165527344 291.376 -86.524642944335938
		 292.412 -86.495048522949219 293.444 -86.464317321777344 294.48 -86.431365966796875
		 295.512 -86.387924194335937 296.548 -86.392326354980469 297.584 -86.471504211425781
		 298.616 -86.609878540039063 299.652 -86.748069763183594 300.688 -86.839263916015625;
createNode animCurveTA -n "Bip01_R_Calf_rotateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 291 ".ktv";
	setAttr ".ktv[0:249]"  0.688 -0.9332634210586549 1.72 -0.93313324451446533
		 2.756 -0.93299728631973267 3.788 -0.93304550647735596 4.824 -0.93316256999969494
		 5.86 -0.93328893184661865 6.892 -0.93342703580856334 7.928 -0.93354809284210205 8.964 -0.93367964029312134
		 9.996 -0.93381303548812855 11.032 -0.93396639823913574 12.064 -0.93408763408660889
		 13.1 -0.93421572446823131 14.136 -0.93431508541107189 15.168 -0.93435657024383545
		 16.204 -0.93442225456237793 17.24 -0.93448543548583984 18.272 -0.93452787399292003
		 19.308 -0.93456488847732544 20.34 -0.93455976247787464 21.376 -0.93456745147705078
		 22.412 -0.93455028533935547 23.444 -0.93453365564346313 24.48 -0.93452584743499767
		 25.512 -0.93452519178390503 26.548 -0.93451690673828114 27.584 -0.93452721834182739
		 28.616 -0.93458902835845947 29.652 -0.93471676111221302 30.688 -0.93480032682418823
		 31.72 -0.93482553958892811 32.756 -0.93480384349822998 33.788 -0.93475383520126343
		 34.824 -0.93474316596984852 35.86 -0.93474346399307251 36.892 -0.93477576971054077
		 37.928 -0.93478626012802135 38.964 -0.93483930826187134 39.996 -0.93489533662796021
		 41.032 -0.93497568368911743 42.064 -0.93509989976882935 43.1 -0.93516498804092407
		 44.136 -0.93516021966934193 45.168 -0.93509447574615467 46.204 -0.93505722284317028
		 47.24 -0.93508261442184448 48.272 -0.93508881330490123 49.308 -0.93499541282653809
		 50.34 -0.93481963872909535 51.376 -0.93463152647018433 52.412 -0.93437588214874268
		 53.444 -0.93406414985656738 54.48 -0.93376320600509655 55.512 -0.93346804380416881
		 56.548 -0.93320870399475087 57.584 -0.93303561210632324 58.616 -0.93296217918396007
		 59.652 -0.93284863233566284 60.688 -0.93270742893218983 61.72 -0.93262600898742687
		 62.756 -0.93261104822158825 63.788 -0.93255311250686646 64.824 -0.93243807554244995
		 65.86 -0.93222171068191539 66.892 -0.93192845582962036 67.928 -0.93156516551971436
		 68.964 -0.93116676807403564 69.996 -0.93080788850784302 71.032 -0.93054044246673584
		 72.064 -0.93032050132751465 73.1 -0.93000245094299316 74.136 -0.92953962087631226
		 75.168 -0.92917501926422119 76.204 -0.928877294063568 77.24 -0.92868006229400624
		 78.272 -0.92865884304046642 79.308 -0.92890769243240356 80.34 -0.92926818132400524
		 81.376 -0.92948132753372203 82.412 -0.92956519126892101 83.444 -0.92980748414993297
		 84.48 -0.93014878034591664 85.512 -0.93036627769470215 86.548 -0.93036359548568714
		 87.584 -0.93027704954147339 88.616 -0.93011099100112915 89.652 -0.9298967719078064
		 90.688 -0.92980855703353871 91.72 -0.92988967895507824 92.756 -0.93009763956069935
		 93.788 -0.9303392767906189 94.824 -0.93067193031311035 95.86 -0.93096750974655151
		 96.892 -0.93120497465133667 97.928 -0.93139111995697033 98.964 -0.9315442442893983
		 99.996 -0.93165022134780873 101.032 -0.93161088228225697 102.064 -0.93144136667251598
		 103.1 -0.93122303485870372 104.136 -0.93108302354812633 105.168 -0.93102461099624623
		 106.204 -0.93097096681594849 107.24 -0.93096059560775768 108.272 -0.93096542358398438
		 109.308 -0.93092262744903564 110.34 -0.93086379766464245 111.376 -0.93081724643707286
		 112.412 -0.93078249692916859 113.444 -0.93073159456253052 114.48 -0.93067574501037587
		 115.512 -0.93060994148254395 116.548 -0.9305437207221986 117.584 -0.93053263425827037
		 118.616 -0.93054789304733276 119.652 -0.93055379390716553 120.688 -0.93050873279571533
		 121.72 -0.93039798736572277 122.756 -0.93025910854339611 123.788 -0.93020075559616089
		 124.824 -0.93035835027694702 125.86 -0.93058526515960704 126.892 -0.93072080612182606
		 127.928 -0.9306970238685609 128.964 -0.93051874637603749 129.996 -0.93024516105651855
		 131.032 -0.92998999357223522 132.064 -0.92967492341995239 133.1 -0.92932617664337169
		 134.136 -0.92904675006866455 135.168 -0.92893874645233154 136.204 -0.92896664142608643
		 137.236 -0.92907613515853882 138.272 -0.92922133207321167 139.308 -0.92927962541580211
		 140.34 -0.92925763130187999 141.376 -0.92911529541015625 142.412 -0.9289821982383728
		 143.444 -0.92902565002441417 144.48 -0.92938709259033192 145.512 -0.92977911233901978
		 146.548 -0.92984151840209961 147.584 -0.92970705032348633 148.616 -0.92954397201538086
		 149.652 -0.92930120229721058 150.688 -0.92907565832138062 151.72 -0.92903983592987049
		 152.756 -0.92907524108886719 153.788 -0.92903202772140503 154.824 -0.92897516489028931
		 155.86 -0.92903554439544678 156.892 -0.92913675308227539 157.928 -0.92920440435409535
		 158.964 -0.92934560775756836 159.996 -0.92942798137664795 161.032 -0.92941772937774647
		 162.064 -0.92936158180236805 163.1 -0.92931485176086415 164.136 -0.92937034368515015
		 165.168 -0.92932415008544933 166.204 -0.92926275730133057 167.236 -0.9291960597038269
		 168.272 -0.92919337749481212 169.308 -0.92928999662399292 170.34 -0.92942339181900024
		 171.376 -0.92942476272582997 172.412 -0.92931050062179565 173.444 -0.92921322584152222
		 174.48 -0.92914849519729625 175.512 -0.92892038822174083 176.548 -0.92863994836807251
		 177.584 -0.92838209867477417 178.616 -0.92818808555603027 179.652 -0.92786109447479237
		 180.688 -0.92756426334381104 181.72 -0.92736655473709106 182.756 -0.92720985412597645
		 183.788 -0.92705130577087402 184.824 -0.92701405286788929 185.86 -0.9270784854888916
		 186.892 -0.92714893817901622 187.928 -0.92708373069763195 188.964 -0.92708808183670044
		 189.996 -0.92719835042953491 191.032 -0.92729735374450684 192.064 -0.92741692066192638
		 193.1 -0.92749202251434326 194.136 -0.92749899625778198 195.168 -0.92736780643463146
		 196.204 -0.92721372842788685 197.236 -0.92725479602813721 198.272 -0.92746472358703602
		 199.308 -0.92767643928527843 200.34 -0.92788380384445179 201.376 -0.92806440591812134
		 202.412 -0.92829853296279907 203.444 -0.92854088544845581 204.48 -0.9287375807762146
		 205.512 -0.92891508340835571 206.548 -0.9290386438369751 207.584 -0.92909818887710571
		 208.616 -0.92913854122161865 209.652 -0.92918688058853138 210.688 -0.92923641204833995
		 211.72 -0.92926383018493652 212.756 -0.9292987585067749 213.788 -0.92936652898788452
		 214.824 -0.92937612533569336 215.86 -0.92937761545181274 216.892 -0.92939466238021839
		 217.928 -0.9293876290321349 218.964 -0.92938387393951416 219.996 -0.9293828010559082
		 221.032 -0.92943620681762684 222.064 -0.92947256565093994 223.1 -0.92952948808670033
		 224.136 -0.92959392070770264 225.168 -0.92964279651641846 226.204 -0.92969584465026855
		 227.236 -0.92973244190216064 228.272 -0.92982286214828491 229.308 -0.92994558811187744
		 230.34 -0.93006700277328502 231.376 -0.93019229173660278 232.412 -0.93037277460098267
		 233.444 -0.93052059412002552 234.48 -0.93066138029098522 235.512 -0.93090647459030162
		 236.548 -0.9312090277671814 237.584 -0.93150967359542858 238.616 -0.93178361654281627
		 239.652 -0.93197464942932129 240.688 -0.93204861879348744 241.72 -0.93209058046340953
		 242.756 -0.93210911750793457 243.788 -0.93210029602050781 244.824 -0.93206709623336803
		 245.86 -0.93201661109924316 246.892 -0.93196505308151245 247.928 -0.93196493387222279
		 248.96 -0.93201333284378041 249.996 -0.93208467960357666 251.032 -0.9321972131729126
		 252.064 -0.93227159976959229 253.1 -0.93234747648239147 254.136 -0.93235075473785389
		 255.168 -0.93232119083404541 256.204 -0.93219226598739624 257.236 -0.93213719129562378
		 258.272 -0.93213099241256703;
	setAttr ".ktv[250:290]" 259.308 -0.9321618676185609 260.34 -0.93214124441146851
		 261.376 -0.93213474750518799 262.412 -0.93214982748031605 263.444 -0.93218779563903809
		 264.48 -0.93216633796691895 265.512 -0.93216913938522339 266.548 -0.9321179986000061
		 267.584 -0.93199229240417492 268.616 -0.93186283111572277 269.652 -0.93183195590972889
		 270.688 -0.93183815479278564 271.72 -0.93183755874633789 272.756 -0.93185192346572887
		 273.788 -0.93182677030563343 274.824 -0.93189078569412231 275.86 -0.93202078342437744
		 276.892 -0.93209344148635864 277.928 -0.93213880062103271 278.96 -0.93214726448059093
		 279.996 -0.93213796615600586 281.032 -0.93215554952621449 282.064 -0.93211823701858509
		 283.1 -0.93211370706558228 284.136 -0.932117760181427 285.168 -0.93214082717895519
		 286.204 -0.93203890323638916 287.236 -0.93195599317550659 288.272 -0.93184882402420044
		 289.308 -0.93181627988815319 290.34 -0.93175351619720459 291.376 -0.93167459964752186
		 292.412 -0.93161278963088989 293.444 -0.93155503273010254 294.48 -0.93151140213012695
		 295.512 -0.93141096830368042 296.548 -0.93134808540344249 297.584 -0.93135195970535289
		 298.616 -0.93140518665313721 299.652 -0.9314368963241576 300.688 -0.93141019344329834;
createNode animCurveTA -n "Bip01_R_Calf_rotateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 291 ".ktv";
	setAttr ".ktv[0:249]"  0.688 0.046452689915895462 1.72 0.049421191215515137
		 2.756 0.051169488579034805 3.788 0.050012398511171341 4.824 0.047387115657329559
		 5.86 0.044809889048337936 6.892 0.042019642889499664 7.928 0.038663126528263092 8.964 0.035533785820007324
		 9.996 0.032584782689809799 11.032 0.028469687327742573 12.064 0.023324396461248398
		 13.1 0.018636221066117287 14.136 0.014747251756489275 15.168 0.010822893120348454
		 16.204 0.0061403261497616768 17.24 0.0011451729806140065 18.272 -0.0029297338332980871
		 19.308 -0.0072803166694939128 20.34 -0.011227212846279144 21.376 -0.014543658122420309
		 22.412 -0.017533024773001671 23.444 -0.020595567300915718 24.48 -0.023283468559384346
		 25.512 -0.02497546374797821 26.548 -0.025823507457971573 27.584 -0.025928033515810966
		 28.616 -0.025432353839278221 29.652 -0.024024294689297676 30.688 -0.021928228437900543
		 31.72 -0.020623315125703812 32.756 -0.020803717896342278 33.788 -0.021416597068309784
		 34.824 -0.0214206762611866 35.86 -0.02074844017624855 36.892 -0.02010042779147625
		 37.928 -0.019978083670139313 38.964 -0.019813194870948792 39.996 -0.018571343272924423
		 41.032 -0.015901807695627213 42.064 -0.012786728329956532 43.1 -0.0095354942604899406
		 44.136 -0.0055792508646845818 45.168 -0.000713640998583287 46.204 0.0045999730937182903
		 47.24 0.010531068779528141 48.272 0.016826281324028969 49.308 0.023187324404716492
		 50.34 0.029587207362055779 51.376 0.036197837442159653 52.412 0.043305929750204086
		 53.444 0.0502190962433815 54.48 0.056129563599824905 55.512 0.061485745012760162
		 56.548 0.065896220505237579 57.584 0.068177834153175354 58.616 0.068963676691055298
		 59.652 0.070430949330329895 60.688 0.072540454566478729 61.72 0.073858469724655151
		 62.756 0.074302442371845245 63.788 0.075292982161045074 64.824 0.077698267996311188
		 65.86 0.081389419734477997 66.892 0.086023077368736267 67.928 0.091032080352306366
		 68.964 0.095780260860919952 69.996 0.09968528151512146 71.032 0.10242216289043427
		 72.064 0.10495390743017197 73.1 0.10845857113599777 74.136 0.11275488138198854 75.168 0.11642551422119141
		 76.204 0.11934617161750793 77.24 0.12101318687200548 78.272 0.1209592968225479 79.308 0.11819509416818619
		 80.34 0.11433010548353194 81.376 0.11194224655628206 82.412 0.11109873652458191 83.444 0.10866935551166534
		 84.48 0.1051296591758728 85.512 0.10277267545461655 86.548 0.10222028195858002 87.584 0.10284136235713959
		 88.616 0.10451380908489227 89.652 0.10619750618934631 90.688 0.10643203556537628
		 91.72 0.10526768118143082 92.756 0.10340398550033569 93.788 0.10110434889793396 94.824 0.097286120057106018
		 95.86 0.093503452837467194 96.892 0.090331576764583588 97.928 0.087838083505630493
		 98.964 0.086129523813724518 99.996 0.085166394710540771 101.032 0.085852384567260742
		 102.064 0.088347099721431732 103.1 0.09094422310590744 104.136 0.092323467135429382
		 105.168 0.09304819256067276 106.204 0.093522734940052032 107.24 0.093674533069133759
		 108.272 0.093660652637481689 109.308 0.094016268849372864 110.34 0.094630420207977295
		 111.376 0.095044441521167755 112.412 0.095542244613170624 113.444 0.096454687416553497
		 114.48 0.097503118216991425 115.512 0.098257295787334442 116.548 0.098605453968048096
		 117.584 0.098570659756660461 118.616 0.09820229560136795 119.652 0.098045207560062408
		 120.688 0.098427973687648773 121.72 0.099143609404563904 122.756 0.10018198937177658
		 123.788 0.10057223588228226 124.824 0.09904780238866806 125.86 0.096812941133975983
		 126.892 0.095759190618991852 127.928 0.096003569662570953 128.964 0.097614437341690063
		 129.996 0.10009288042783737 131.032 0.10245159268379211 132.064 0.10511111468076706
		 133.1 0.10847239196300507 134.136 0.11125572025775909 135.168 0.11255855113267899
		 136.204 0.11262989044189453 137.236 0.11177381128072739 138.272 0.11045796424150467
		 139.308 0.10955046862363815 140.34 0.10949747264385223 141.376 0.11036180704832077
		 142.412 0.11149527877569199 143.444 0.11130431294441223 144.48 0.1083792969584465
		 145.512 0.10459280759096146 146.548 0.1031481921672821 147.584 0.10420367121696472
		 148.616 0.10621081292629242 149.652 0.10866089910268784 150.688 0.1106913760304451
		 151.72 0.11098415404558182 152.756 0.11094099283218384 153.788 0.11142773181200027
		 154.824 0.11195753514766695 155.86 0.11153901368379593 156.892 0.11058961600065231
		 157.928 0.11006705462932587 158.964 0.10896409302949905 159.996 0.10799247026443481
		 161.032 0.10775661468505859 162.064 0.10858521610498428 163.1 0.1092103123664856
		 164.136 0.10905319452285767 165.168 0.10978668183088303 166.204 0.11056701838970184
		 167.236 0.11149162799119949 168.272 0.11179041862487793 169.308 0.11107945442199707
		 170.34 0.10991290956735611 171.376 0.10979987680912018 172.412 0.11090303212404251
		 173.444 0.11206588149070741 174.48 0.1134491041302681 175.512 0.11590497195720671
		 176.548 0.1186697781085968 177.584 0.1210346296429634 178.616 0.12320405244827271
		 179.652 0.12617455422878265 180.688 0.12879504263401031 181.72 0.13023707270622253
		 182.756 0.13146699965000153 183.788 0.13284496963024139 184.824 0.13322365283966064
		 185.86 0.13227923214435577 186.892 0.13154718279838562 187.928 0.13184206187725067
		 188.964 0.13215647637844086 189.996 0.13151906430721283 191.032 0.13094401359558105
		 192.064 0.12982824444770813 193.1 0.12908641993999481 194.136 0.12889711558818817
		 195.168 0.13000708818435669 196.204 0.13119550049304962 197.236 0.13081154227256775
		 198.272 0.12894436717033386 199.308 0.12693828344345093 200.34 0.12494320422410965
		 201.376 0.1232970356941223 202.412 0.12115152925252914 203.444 0.1190783753991127
		 204.48 0.11715270578861235 205.512 0.11526788026094438 206.548 0.11384153366088866
		 207.584 0.1130203977227211 208.616 0.11239718645811082 209.652 0.11170291155576706
		 210.688 0.11079082638025284 211.72 0.1099645271897316 212.756 0.10969609022140503
		 213.788 0.10886397212743759 214.824 0.10914556682109833 215.86 0.10894416272640228
		 216.892 0.10894755274057388 217.928 0.10896631330251694 218.964 0.10860376805067062
		 219.996 0.10870672017335892 221.032 0.10829874128103256 222.064 0.10791359841823578
		 223.1 0.10770270973443985 224.136 0.10740412771701813 225.168 0.10681712627410889
		 226.204 0.10649041831493378 227.236 0.10662399977445602 228.272 0.10636409372091293
		 229.308 0.10528410226106644 230.34 0.10385900735855103 231.376 0.10227297246456146
		 232.412 0.10076694935560226 233.444 0.099502287805080414 234.48 0.09804929792881012
		 235.512 0.095788702368736267 236.548 0.092699252068996429 237.584 0.089468911290168762
		 238.616 0.086498565971851349 239.652 0.084107644855976105 240.688 0.082926914095878601
		 241.72 0.08224526047706604 242.756 0.082223668694496155 243.788 0.083043128252029419
		 244.824 0.083901479840278625 245.86 0.084637641906738281 246.892 0.085378557443618774
		 247.928 0.085664033889770508 248.96 0.085590332746505737 249.996 0.084785796701908112
		 251.032 0.083910085260868073 252.064 0.083104319870471954 253.1 0.082600973546504974
		 254.136 0.082973949611186981 255.168 0.083324365317821503 256.204 0.084481000900268555
		 257.236 0.085517086088657379 258.272 0.085419110953807831;
	setAttr ".ktv[250:290]" 259.308 0.084756694734096527 260.34 0.084882736206054688
		 261.376 0.084553830325603485 262.412 0.084005780518054962 263.444 0.083948656916618347
		 264.48 0.084196142852306366 265.512 0.083756707608699799 266.548 0.084268927574157715
		 267.584 0.086046397686004639 268.616 0.087294101715087891 269.652 0.087884865701198578
		 270.688 0.087904199957847595 271.72 0.087568782269954681 272.756 0.086891472339630127
		 273.788 0.086364969611167908 274.824 0.085599057376384735 275.86 0.084358453750610352
		 276.892 0.083436347544193268 277.928 0.08285599946975708 278.96 0.082157380878925323
		 279.996 0.081525422632694244 281.032 0.081320807337760925 282.064 0.081470474600791931
		 283.1 0.081623755395412445 284.136 0.081303656101226807 285.168 0.080806337296962738
		 286.204 0.081844717264175415 287.236 0.083477288484573364 288.272 0.085044354200363159
		 289.308 0.085510030388832092 290.34 0.086087919771671295 291.376 0.086818590760231018
		 292.412 0.087613753974437714 293.444 0.088028892874717712 294.48 0.088476411998271942
		 295.512 0.089404314756393433 296.548 0.090161941945552826 297.584 0.090065009891986847
		 298.616 0.089255936443805695 299.652 0.088719435036182404 300.688 0.088896244764328003;
createNode animCurveTA -n "Bip01_R_Calf_rotateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 291 ".ktv";
	setAttr ".ktv[0:249]"  0.688 -84.668350219726563 1.72 -84.512657165527344
		 2.756 -84.373641967773438 3.788 -84.422119140625 4.824 -84.569503784179688 5.86 -84.717689514160156
		 6.892 -84.88677978515625 7.928 -85.069808959960938 8.964 -85.252983093261719 9.996 -85.449859619140625
		 11.032 -85.700576782226563 12.064 -85.972824096679688 13.1 -86.261878967285156 14.136 -86.500831604003906
		 15.168 -86.720230102539063 16.204 -86.986244201660156 17.24 -87.2928466796875 18.272 -87.561729431152344
		 19.308 -87.837715148925781 20.34 -88.076507568359375 21.376 -88.305038452148438 22.412 -88.498863220214844
		 23.444 -88.68707275390625 24.48 -88.873321533203125 25.512 -88.986740112304688 26.548 -89.055519104003906
		 27.584 -89.076362609863281 28.616 -89.085235595703125 29.652 -89.09332275390625 30.688 -89.013839721679688
		 31.72 -88.943199157714844 32.756 -88.934288024902344 33.788 -88.936317443847656 34.824 -88.921417236328125
		 35.86 -88.882064819335938 36.892 -88.860260009765625 37.928 -88.8641357421875 38.964 -88.891891479492187
		 39.996 -88.853485107421875 41.032 -88.741996765136719 42.064 -88.642425537109375
		 43.1 -88.488655090332031 44.136 -88.24676513671875 45.168 -87.926040649414063 46.204 -87.6162109375
		 47.24 -87.344390869140625 48.272 -87.076820373535156 49.308 -86.758346557617188 50.34 -86.401718139648438
		 51.376 -86.047836303710938 52.412 -85.678573608398438 53.444 -85.2928466796875 54.48 -84.966461181640625
		 55.512 -84.685798645019531 56.548 -84.455146789550781 57.584 -84.3116455078125 58.616 -84.243148803710938
		 59.652 -84.146881103515625 60.688 -84.0343017578125 61.72 -83.980033874511719 62.756 -83.968666076660156
		 63.788 -83.919281005859375 64.824 -83.834510803222656 65.86 -83.697418212890625 66.892 -83.508827209472656
		 67.928 -83.276954650878906 68.964 -83.042991638183594 69.996 -82.82867431640625 71.032 -82.67913818359375
		 72.064 -82.562522888183594 73.1 -82.40374755859375 74.136 -82.173263549804687 75.168 -81.989395141601562
		 76.204 -81.858161926269531 77.24 -81.769683837890625 78.272 -81.744819641113281 79.308 -81.838577270507813
		 80.34 -81.995155334472656 81.376 -82.08978271484375 82.412 -82.116950988769531 83.444 -82.2437744140625
		 84.48 -82.425285339355469 85.512 -82.533065795898438 86.548 -82.525482177734375 87.584 -82.467781066894531
		 88.616 -82.37384033203125 89.652 -82.243972778320313 90.688 -82.172004699707031 91.72 -82.202262878417969
		 92.756 -82.322189331054688 93.788 -82.465087890625 94.824 -82.650299072265625 95.86 -82.820632934570313
		 96.892 -82.961074829101563 97.928 -83.074134826660156 98.964 -83.17462158203125 99.996 -83.247871398925781
		 101.032 -83.227485656738281 102.064 -83.120468139648437 103.1 -82.983222961425781
		 104.136 -82.895286560058594 105.168 -82.854804992675781 106.204 -82.829025268554688
		 107.24 -82.813362121582031 108.272 -82.812019348144531 109.308 -82.793815612792969
		 110.34 -82.759880065917969 111.376 -82.716072082519531 112.412 -82.711624145507813
		 113.444 -82.690322875976563 114.48 -82.662055969238281 115.512 -82.617683410644531
		 116.548 -82.581077575683594 117.584 -82.564483642578125 118.616 -82.572959899902344
		 119.652 -82.57086181640625 120.688 -82.543006896972656 121.72 -82.468063354492187
		 122.756 -82.3720703125 123.788 -82.34027099609375 124.824 -82.433929443359375 125.86 -82.5689697265625
		 126.892 -82.659248352050781 127.928 -82.643203735351563 128.964 -82.530891418457031
		 129.996 -82.358329772949219 131.032 -82.205459594726563 132.064 -82.026390075683594
		 133.1 -81.827072143554687 134.136 -81.681243896484375 135.168 -81.642738342285156
		 136.204 -81.666786193847656 137.236 -81.731277465820313 138.272 -81.807716369628906
		 139.308 -81.826820373535156 140.34 -81.801841735839844 141.376 -81.713035583496094
		 142.412 -81.641014099121094 143.444 -81.671821594238281 144.48 -81.874282836914062
		 145.512 -82.088653564453125 146.548 -82.110313415527344 147.584 -82.023857116699219
		 148.616 -81.940155029296875 149.652 -81.810234069824219 150.688 -81.691322326660156
		 151.72 -81.676727294921875 152.756 -81.699562072753906 153.788 -81.680168151855469
		 154.824 -81.654205322265625 155.86 -81.683082580566406 156.892 -81.744499206542969
		 157.928 -81.780685424804688 158.964 -81.868667602539062 159.996 -81.897201538085938
		 161.032 -81.882919311523438 162.064 -81.862503051757813 163.1 -81.849609375 164.136 -81.883148193359375
		 165.168 -81.87249755859375 166.204 -81.84228515625 167.236 -81.824790954589844 168.272 -81.828193664550781
		 169.308 -81.8868408203125 170.34 -81.965179443359375 171.376 -81.961463928222656
		 172.412 -81.893203735351563 173.444 -81.857765197753906 174.48 -81.857505798339844
		 175.512 -81.759605407714844 176.548 -81.627044677734375 177.584 -81.508575439453125
		 178.616 -81.443603515625 179.652 -81.305717468261719 180.688 -81.17791748046875 181.72 -81.080497741699219
		 182.756 -81.010673522949219 183.788 -80.950370788574219 184.824 -80.934585571289063
		 185.86 -80.946487426757813 186.892 -80.963394165039063 187.928 -80.930015563964844
		 188.964 -80.949440002441406 189.996 -81.010696411132813 191.032 -81.056999206542969
		 192.064 -81.102577209472656 193.1 -81.1292724609375 194.136 -81.12396240234375 195.168 -81.064476013183594
		 196.204 -81.002349853515625 197.236 -81.014366149902344 198.272 -81.100379943847656
		 199.308 -81.18280029296875 200.34 -81.267288208007812 201.376 -81.3397216796875 202.412 -81.443428039550781
		 203.444 -81.564048767089844 204.48 -81.646766662597656 205.512 -81.727439880371094
		 206.548 -81.77386474609375 207.584 -81.803535461425781 208.616 -81.810714721679688
		 209.652 -81.821083068847656 210.688 -81.829833984375 211.72 -81.835334777832031 212.756 -81.841873168945313
		 213.788 -81.882453918457031 214.824 -81.887252807617187 215.86 -81.891952514648437
		 216.892 -81.90325927734375 217.928 -81.895408630371094 218.964 -81.885002136230469
		 219.996 -81.884613037109375 221.032 -81.916702270507813 222.064 -81.938652038574219
		 223.1 -81.976173400878906 224.136 -82.02325439453125 225.168 -82.048004150390625
		 226.204 -82.080680847167969 227.236 -82.116615295410156 228.272 -82.180000305175781
		 229.308 -82.250152587890625 230.34 -82.319999694824219 231.376 -82.381233215332031
		 232.412 -82.48712158203125 233.444 -82.586105346679688 234.48 -82.659385681152344
		 235.512 -82.82427978515625 236.548 -83.016403198242187 237.584 -83.208084106445313
		 238.616 -83.387680053710937 239.652 -83.517059326171875 240.688 -83.570640563964844
		 241.72 -83.587394714355469 242.756 -83.602073669433594 243.788 -83.60772705078125
		 244.824 -83.596672058105469 245.86 -83.561614990234375 246.892 -83.524024963378906
		 247.928 -83.531082153320313 248.96 -83.569206237792969 249.996 -83.625724792480469
		 251.032 -83.706832885742188 252.064 -83.751541137695313 253.1 -83.81298828125 254.136 -83.824638366699219
		 255.168 -83.807861328125 256.204 -83.709342956542969 257.236 -83.670677185058594
		 258.272 -83.659934997558594;
	setAttr ".ktv[250:290]" 259.308 -83.682281494140625 260.34 -83.676277160644531
		 261.376 -83.65362548828125 262.412 -83.665931701660156 263.444 -83.700119018554688
		 264.48 -83.684638977050781 265.512 -83.676292419433594 266.548 -83.639328002929688
		 267.584 -83.555923461914063 268.616 -83.471138000488281 269.652 -83.447257995605469
		 270.688 -83.453277587890625 271.72 -83.458297729492188 272.756 -83.451789855957031
		 273.788 -83.427558898925781 274.824 -83.465194702148438 275.86 -83.551895141601562
		 276.892 -83.607864379882813 277.928 -83.634925842285156 278.96 -83.639816284179687
		 279.996 -83.62420654296875 281.032 -83.631370544433594 282.064 -83.611473083496094
		 283.1 -83.602272033691406 284.136 -83.60443115234375 285.168 -83.6123046875 286.204 -83.547172546386719
		 287.236 -83.489387512207031 288.272 -83.419425964355469 289.308 -83.392280578613281
		 290.34 -83.353424072265625 291.376 -83.298294067382813 292.412 -83.252792358398438
		 293.444 -83.220596313476562 294.48 -83.193328857421875 295.512 -83.119117736816406
		 296.548 -83.076728820800781 297.584 -83.083442687988281 298.616 -83.112068176269531
		 299.652 -83.126327514648438 300.688 -83.100372314453125;
createNode animCurveTA -n "Bip01_R_Foot_rotateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 291 ".ktv";
	setAttr ".ktv[0:249]"  0.688 9.0067958831787109 1.72 9.136993408203125 2.756 9.2951498031616211
		 3.788 9.3695096969604492 4.824 9.3414163589477539 5.86 9.3752346038818359 6.892 9.3675270080566406
		 7.928 9.1578969955444336 8.964 8.8410425186157227 9.996 8.62872314453125 11.032 8.6834535598754883
		 12.064 8.9225044250488281 13.1 9.0269222259521484 14.136 8.9873018264770508 15.168 9.112767219543457
		 16.204 9.3456554412841797 17.24 9.4208774566650391 18.272 9.3192214965820312 19.308 9.3857212066650391
		 20.34 9.5214319229125977 21.376 9.4840164184570312 22.412 9.528538703918457 23.444 9.714263916015625
		 24.48 9.9045782089233398 25.512 9.9515466690063477 26.548 9.9211711883544922 27.584 9.8857698440551758
		 28.616 9.8051433563232422 29.652 9.7588205337524414 30.688 9.7495613098144531 31.72 9.8544301986694336
		 32.756 9.7873620986938477 33.788 9.6609973907470703 34.824 9.6344623565673828 35.86 9.607630729675293
		 36.892 9.4860038757324219 37.928 9.3240871429443359 38.964 9.1299991607666016 39.996 9.1780328750610352
		 41.032 9.253382682800293 42.064 9.2428226470947266 43.1 9.0467929840087891 44.136 8.9585943222045898
		 45.168 9.2089319229125977 46.204 9.467041015625 47.24 9.5266342163085938 48.272 9.4641942977905273
		 49.308 9.4811677932739258 50.34 9.4943161010742187 51.376 9.51397705078125 52.412 9.5130147933959961
		 53.444 9.4936904907226563 54.48 9.4221744537353516 55.512 9.2078342437744141 56.548 8.868525505065918
		 57.584 8.6353416442871094 58.616 8.6955080032348633 59.652 8.7928133010864258 60.688 8.8672046661376953
		 61.72 8.9433431625366211 62.756 8.908238410949707 63.788 8.7505035400390625 64.824 8.5978784561157227
		 65.86 8.6194801330566406 66.892 8.7182931900024414 67.928 8.7410316467285156 68.964 8.7721319198608398
		 69.996 8.8685379028320312 71.032 9.0000371932983398 72.064 9.0499000549316406 73.1 9.0496959686279297
		 74.136 9.0520029067993164 75.168 8.9921207427978516 76.204 8.9576883316040039 77.24 8.8907670974731445
		 78.272 8.912322998046875 79.308 8.7511711120605469 80.34 8.5006027221679687 81.376 8.3366947174072266
		 82.412 8.1787109375 83.444 7.8490924835205069 84.48 7.3440513610839844 85.512 7.0100393295288086
		 86.548 6.948798656463623 87.584 6.8959970474243164 88.616 6.8746886253356934 89.652 7.1247892379760742
		 90.688 7.3739495277404794 91.72 7.2759475708007812 92.756 6.9112672805786133 93.788 6.686800479888916
		 94.824 6.7660641670227051 95.86 6.9011392593383789 96.892 6.8789596557617187 97.928 6.6901397705078125
		 98.964 6.4342570304870605 99.996 6.4144325256347656 101.032 6.7228255271911621 102.064 6.9959745407104492
		 103.1 7.1716675758361816 104.136 7.1838293075561523 105.168 7.039459228515625 106.204 6.9621219635009766
		 107.24 6.988853931427002 108.272 7.0513172149658203 109.308 7.1422309875488281 110.34 7.4315366744995117
		 111.376 7.7077150344848642 112.412 7.6499590873718262 113.444 7.5012922286987305
		 114.48 7.5059046745300284 115.512 7.6902871131896973 116.548 7.8567013740539551 117.584 7.7775921821594238
		 118.616 7.6115899085998544 119.652 7.7215018272399902 120.688 7.9327163696289062
		 121.72 8.0588045120239258 122.756 8.1539506912231445 123.788 8.287165641784668 124.824 8.3618497848510742
		 125.86 8.3069553375244141 126.892 8.2163724899291992 127.928 8.2429275512695313 128.964 8.2907962799072266
		 129.996 8.1895666122436523 131.032 8.0899181365966797 132.064 8.1915884017944336
		 133.1 8.3987569808959961 134.136 8.4920902252197266 135.168 8.4339513778686523 136.204 8.2158832550048828
		 137.236 8.0340795516967773 138.272 7.8239016532897949 139.308 7.7049884796142569
		 140.34 7.7334094047546387 141.376 7.7995524406433105 142.412 7.8262572288513184 143.444 7.6484632492065439
		 144.48 7.4173545837402344 145.512 7.4939689636230478 146.548 7.7676005363464355 147.584 7.8466644287109375
		 148.616 7.8219270706176767 149.652 7.8887209892272949 150.688 8.0347270965576172
		 151.72 7.9977035522460938 152.756 7.9105734825134277 153.788 7.9093756675720224 154.824 8.0904207229614258
		 155.86 8.1278285980224609 156.892 8.2037773132324219 157.928 8.4354324340820312 158.964 8.5611715316772461
		 159.996 8.551854133605957 161.032 8.6352329254150391 162.064 8.7571029663085938 163.1 8.8466043472290039
		 164.136 8.8443460464477539 165.168 8.8308744430541992 166.204 8.9154796600341797
		 167.236 9.0858678817749023 168.272 9.0953292846679687 169.308 8.7202587127685547
		 170.34 8.3904819488525391 171.376 8.5486297607421875 172.412 8.9322319030761719 173.444 8.921137809753418
		 174.48 8.6396627426147461 175.512 8.5211505889892578 176.548 8.7807950973510742 177.584 9.0616607666015625
		 178.616 9.2498455047607422 179.652 9.294097900390625 180.688 9.3161840438842773 181.72 9.3993549346923828
		 182.756 9.6084718704223633 183.788 9.7376375198364258 184.824 9.6994266510009766
		 185.86 9.6621932983398437 186.892 9.7027568817138672 187.928 9.6793985366821289 188.964 9.5735664367675781
		 189.996 9.38592529296875 191.032 9.2478828430175781 192.064 9.2527675628662109 193.1 9.4544315338134766
		 194.136 9.5693416595458984 195.168 9.3913717269897461 196.204 9.2561483383178711
		 197.236 9.2314987182617188 198.272 9.0829639434814453 199.308 8.9707937240600586
		 200.34 9.0686311721801758 201.376 9.1644830703735352 202.412 9.1002769470214844 203.444 8.9479379653930664
		 204.48 8.9020595550537109 205.512 8.9410886764526367 206.548 8.9233722686767578 207.584 8.8829822540283203
		 208.616 8.871678352355957 209.652 9.1053886413574219 210.688 9.3881120681762695 211.72 9.4993829727172852
		 212.756 9.4040403366088867 213.788 9.2921152114868164 214.824 9.1822919845581055
		 215.86 9.1339311599731445 216.892 9.1324625015258789 217.928 9.1891164779663086 218.964 9.2107229232788086
		 219.996 9.2526712417602539 221.032 9.3254327774047852 222.064 9.449554443359375 223.1 9.5047664642333984
		 224.136 9.533203125 225.168 9.6455926895141602 226.204 9.7197723388671875 227.236 9.4354610443115234
		 228.272 9.0262842178344727 229.308 8.9760532379150391 230.34 9.2759838104248047 231.376 9.5017795562744141
		 232.412 9.4999771118164062 233.444 9.4413652420043945 234.48 9.5210409164428711 235.512 9.577672004699707
		 236.548 9.5397872924804687 237.584 9.5761308670043945 238.616 9.8331670761108398
		 239.652 10.045345306396484 240.688 10.253109931945801 241.72 10.548691749572754 242.756 10.725691795349121
		 243.788 10.707306861877441 244.824 10.716089248657227 245.86 10.995058059692383 246.892 11.268318176269531
		 247.928 11.284153938293457 248.96 11.193925857543945 249.996 11.21992015838623 251.032 11.344123840332031
		 252.064 11.450296401977539 253.1 11.39522647857666 254.136 11.325211524963379 255.168 11.539435386657715
		 256.204 12.050749778747559 257.236 12.34209156036377 258.272 12.206533432006836;
	setAttr ".ktv[250:290]" 259.308 12.02922534942627 260.34 11.987319946289063
		 261.376 12.039876937866211 262.412 11.983670234680176 263.444 11.827134132385254
		 264.48 11.717741966247559 265.512 11.691043853759766 266.548 11.71939754486084 267.584 11.666976928710938
		 268.616 11.628218650817871 269.652 11.585588455200195 270.688 11.530697822570801
		 271.72 11.591135025024414 272.756 11.630823135375977 273.788 11.604545593261719 274.824 11.552314758300781
		 275.86 11.28043270111084 276.892 11.050790786743164 277.928 11.050409317016602 278.96 11.105134010314941
		 279.996 11.099828720092773 281.032 11.008995056152344 282.064 10.814277648925781
		 283.1 10.695690155029297 284.136 10.752168655395508 285.168 10.767048835754395 286.204 10.528087615966797
		 287.236 10.141097068786621 288.272 9.8971710205078125 289.308 9.9278116226196289
		 290.34 10.071681976318359 291.376 10.194297790527344 292.412 10.197717666625977 293.444 10.180570602416992
		 294.48 10.302157402038574 295.512 10.524459838867187 296.548 10.550716400146484 297.584 10.43829345703125
		 298.616 10.448093414306641 299.652 10.581498146057129 300.688 10.632383346557617;
createNode animCurveTA -n "Bip01_R_Foot_rotateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 291 ".ktv";
	setAttr ".ktv[0:249]"  0.688 0.082345038652420044 1.72 0.041610252112150192
		 2.756 -0.024287335574626923 3.788 -0.0065008639357984066 4.824 0.030595403164625168
		 5.86 0.0078349439427256584 6.892 0.011666100472211838 7.928 0.14241234958171844 8.964 0.28553509712219238
		 9.996 0.43061763048171997 11.032 0.3878515362739563 12.064 0.24559104442596436 13.1 0.17701558768749237
		 14.136 0.18098510801792145 15.168 0.12296072393655778 16.204 -0.0020342876669019461
		 17.24 -0.087714016437530518 18.272 -0.035497039556503296 19.308 -0.066801480948925018
		 20.34 -0.15273422002792358 21.376 -0.13463304936885834 22.412 -0.16195464134216309
		 23.444 -0.28172174096107483 24.48 -0.39356085658073425 25.512 -0.43350949883460999
		 26.548 -0.45495972037315374 27.584 -0.41997608542442322 28.616 -0.37854877114295959
		 29.652 -0.33054384589195251 30.688 -0.42094850540161133 31.72 -0.44497105479240417
		 32.756 -0.4009145200252533 33.788 -0.32052537798881531 34.824 -0.29081696271896362
		 35.86 -0.28669705986976624 36.892 -0.21668551862239838 37.928 -0.10920744389295578
		 38.964 -0.0069485721178352833 39.996 -0.019749978557229042 41.032 -0.069447383284568787
		 42.064 -0.018455235287547112 43.1 0.12915921211242676 44.136 0.16366878151893616
		 45.168 0.071708627045154572 46.204 -0.072612136602401733 47.24 -0.077619120478630066
		 48.272 -0.032337658107280731 49.308 0.017159132286906242 50.34 0.035484109073877335
		 51.376 0.05596151202917099 52.412 0.070547915995121002 53.444 0.082863368093967438
		 54.48 0.12988176941871643 55.512 0.25089901685714722 56.548 0.4253617525100708 57.584 0.49787077307701111
		 58.616 0.44582119584083557 59.652 0.32730484008789063 60.688 0.28870642185211182
		 61.72 0.18582554161548615 62.756 0.15175133943557739 63.788 0.21832415461540222 64.824 0.28520312905311584
		 65.86 0.24506536126136777 66.892 0.2168743908405304 67.928 0.16716982424259186 68.964 0.13887907564640045
		 69.996 0.073461972177028656 71.032 -0.011338938027620316 72.064 -0.060736566781997688
		 73.1 -0.085305266082286835 74.136 -0.042082119733095169 75.168 -0.026540657505393028
		 76.204 0.0095494510605931282 77.24 0.035875618457794189 78.272 0.013239373452961445
		 79.308 0.074575707316398621 80.34 0.17722614109516144 81.376 0.29486337304115295
		 82.412 0.3141162097454071 83.444 0.48056098818778986 84.48 0.73650342226028442 85.512 0.88971412181854248
		 86.548 0.91744512319564819 87.584 0.9564307928085326 88.616 0.99513292312622081 89.652 0.84767109155654907
		 90.688 0.68275994062423706 91.72 0.7217441201210022 92.756 0.91924256086349487 93.788 1.030686616897583
		 94.824 0.95663636922836315 95.86 0.86154991388320923 96.892 0.8695833683013916 97.928 0.97602951526641846
		 98.964 1.1234239339828491 99.996 1.117993950843811 101.032 0.94424903392791748 102.064 0.76730865240097046
		 103.1 0.66054224967956543 104.136 0.66580379009246826 105.168 0.75595235824584961
		 106.204 0.81532669067382813 107.24 0.81120544672012329 108.272 0.78506326675415039
		 109.308 0.72716355323791504 110.34 0.57276099920272827 111.376 0.42863133549690247
		 112.412 0.43047943711280823 113.444 0.55848121643066406 114.48 0.53846889734268188
		 115.512 0.42054939270019531 116.548 0.33089515566825867 117.584 0.38132157921791077
		 118.616 0.48900672793388361 119.652 0.44253367185592651 120.688 0.30383330583572388
		 121.72 0.23921279609203341 122.756 0.21220147609710693 123.788 0.13269734382629395
		 124.824 0.067126795649528503 125.86 0.069993056356906891 126.892 0.090821720659732819
		 127.928 0.07542487233877182 128.964 0.090101443231105804 129.996 0.1503264456987381
		 131.032 0.26128610968589783 132.064 0.24150803685188293 133.1 0.13145378232002258
		 134.136 0.086858369410037994 135.168 0.13427332043647766 136.204 0.23682035505771637
		 137.236 0.36525073647499084 138.272 0.51938426494598389 139.308 0.57793498039245605
		 140.34 0.569488525390625 141.376 0.54778820276260376 142.412 0.52723854780197144
		 143.444 0.642067551612854 144.48 0.79758369922637939 145.512 0.73624438047409058
		 146.548 0.61481958627700806 147.584 0.58573716878890991 148.616 0.60506796836853027
		 149.652 0.53794652223587036 150.688 0.48457837104797358 151.72 0.51299691200256348
		 152.756 0.59011614322662354 153.788 0.60840076208114624 154.824 0.51504188776016235
		 155.86 0.47926020622253418 156.892 0.46291631460189819 157.928 0.30300244688987732
		 158.964 0.25594055652618408 159.996 0.23749105632305145 161.032 0.22184188663959503
		 162.064 0.16634511947631836 163.1 0.1125231981277466 164.136 0.11065196245908737
		 165.168 0.14529597759246826 166.204 0.0600687488913536 167.236 -0.02271563932299614
		 168.272 -0.0054270164109766483 169.308 0.20262961089611053 170.34 0.37909668684005737
		 171.376 0.26991903781890869 172.412 0.08276454359292984 173.444 0.065911971032619476
		 174.48 0.21687865257263184 175.512 0.30027124285697937 176.548 0.19155687093734741
		 177.584 0.0086184777319431305 178.616 -0.085971489548683167 179.652 -0.11805942654609682
		 180.688 -0.11092598736286163 181.72 -0.15302321314811707 182.756 -0.2927837073802948
		 183.788 -0.35436061024665833 184.824 -0.33162128925323486 185.86 -0.28342905640602112
		 186.892 -0.25498801469802856 187.928 -0.24259392917156219 188.964 -0.17242363095283508
		 189.996 -0.057749178260564797 191.032 -0.01381178293377161 192.064 -0.011875214986503124
		 193.1 -0.13805747032165527 194.136 -0.20645095407962799 195.168 -0.1072944775223732
		 196.204 -0.039913170039653778 197.236 -0.011723902076482773 198.272 0.082978673279285431
		 199.308 0.18376742303371429 200.34 0.11977134644985199 201.376 0.047669589519500732
		 202.412 0.10328349471092224 203.444 0.18819931149482727 204.48 0.21846146881580353
		 205.512 0.22197961807250977 206.548 0.2338811457157135 207.584 0.2886340320110321
		 208.616 0.26293742656707764 209.652 0.17368008196353912 210.688 0.022630149498581886
		 211.72 -0.028267417103052139 212.756 0.046945057809352875 213.788 0.1468222439289093
		 214.824 0.17395202815532684 215.86 0.24920661747455597 216.892 0.26718401908874512
		 217.928 0.2340650409460068 218.964 0.25733408331871033 219.996 0.23339213430881503
		 221.032 0.2062934935092926 222.064 0.17924147844314575 223.1 0.14866907894611359
		 224.136 0.11392814666032791 225.168 0.074357196688652039 226.204 0.059303347021341324
		 227.236 0.21122637391090393 228.272 0.47406452894210815 229.308 0.51668602228164673
		 230.34 0.33271834254264832 231.376 0.22027201950550079 232.412 0.26163259148597717
		 233.444 0.29954737424850464 234.48 0.2835477888584137 235.512 0.24181517958641052
		 236.548 0.25736916065216064 237.584 0.26615545153617859 238.616 0.14797158539295197
		 239.652 0.017492607235908508 240.688 -0.10640711337327957 241.72 -0.23189656436443326
		 242.756 -0.34875413775444031 243.788 -0.33540955185890198 244.824 -0.33204656839370728
		 245.86 -0.50733000040054321 246.892 -0.66918516159057617 247.928 -0.70536643266677856
		 248.96 -0.66202735900878906 249.996 -0.68592506647109985 251.032 -0.78764200210571289
		 252.064 -0.85908657312393188 253.1 -0.84165126085281372 254.136 -0.84768664836883545
		 255.168 -0.98256158828735352 256.204 -1.3449356555938721 257.236 -1.5606338977813721
		 258.272 -1.4990202188491821;
	setAttr ".ktv[250:290]" 259.308 -1.3818511962890625 260.34 -1.3687266111373901
		 261.376 -1.4125136137008667 262.412 -1.4141198396682739 263.444 -1.3299405574798584
		 264.48 -1.2843437194824219 265.512 -1.3203611373901367 266.548 -1.3303427696228027
		 267.584 -1.334632396697998 268.616 -1.3345437049865723 269.652 -1.3178929090499878
		 270.688 -1.3144708871841431 271.72 -1.3562654256820679 272.756 -1.3932106494903564
		 273.788 -1.3821003437042236 274.824 -1.3461737632751465 275.86 -1.1904919147491455
		 276.892 -1.0402889251708984 277.928 -1.0519814491271973 278.96 -1.0913361310958862
		 279.996 -1.0522514581680298 281.032 -0.99240052700042725 282.064 -0.87764549255371094
		 283.1 -0.83047270774841309 284.136 -0.83598911762237549 285.168 -0.82657647132873535
		 286.204 -0.70488208532333374 287.236 -0.49673119187355042 288.272 -0.33246839046478271
		 289.308 -0.32904809713363647 290.34 -0.44666466116905212 291.376 -0.52573108673095703
		 292.412 -0.51919490098953247 293.444 -0.49142792820930481 294.48 -0.57179385423660278
		 295.512 -0.66878175735473633 296.548 -0.70757699012756348 297.584 -0.6282198429107666
		 298.616 -0.66116011142730713 299.652 -0.73548853397369385 300.688 -0.77792912721633911;
createNode animCurveTA -n "Bip01_R_Foot_rotateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 291 ".ktv";
	setAttr ".ktv[0:249]"  0.688 -3.9230217933654785 1.72 -4.1785445213317871
		 2.756 -4.5679745674133301 3.788 -4.790036678314209 4.824 -4.7498197555541992 5.86 -4.5784611701965332
		 6.892 -4.4132227897644043 7.928 -4.3516826629638672 8.964 -4.2102341651916504 9.996 -3.8531813621521001
		 11.032 -3.4734680652618408 12.064 -3.1243140697479248 13.1 -2.6391463279724121 14.136 -2.095416784286499
		 15.168 -1.6665803194046021 16.204 -1.2830841541290283 17.24 -0.79226607084274292
		 18.272 -0.22679658234119415 19.308 0.28491583466529846 20.34 0.7626308798789978 21.376 1.2447292804718018
		 22.412 1.7506884336471558 23.444 2.2427773475646973 24.48 2.6526672840118408 25.512 3.0021703243255615
		 26.548 3.2803661823272705 27.584 3.4744346141815186 28.616 3.7298138141632076 29.652 4.3723540306091309
		 30.688 5.0347590446472168 31.72 5.0743775367736816 32.756 4.6916942596435547 33.788 4.4073314666748047
		 34.824 4.2669677734375 35.86 4.1971149444580078 36.892 4.137732982635498 37.928 4.0270247459411621
		 38.964 3.9901998043060303 39.996 4.1221890449523926 41.032 4.3322372436523437 42.064 4.4495902061462402
		 43.1 4.29644775390625 44.136 3.7957599163055424 45.168 3.0914325714111328 46.204 2.5800743103027344
		 47.24 2.5175220966339111 48.272 2.5741689205169678 49.308 2.4283421039581299 50.34 2.10611891746521
		 51.376 1.7378391027450562 52.412 1.3758629560470581 53.444 1.0338960886001587 54.48 0.77867084741592407
		 55.512 0.67758196592330933 56.548 0.73102396726608276 57.584 0.81794208288192749
		 58.616 0.87470471858978271 59.652 1.0358812808990479 60.688 1.3402489423751831 61.72 1.6827964782714844
		 62.756 1.9672002792358398 63.788 2.1587374210357666 64.824 2.4762523174285889 65.86 2.9989159107208252
		 66.892 3.5071208477020264 67.928 3.8038516044616699 68.964 3.8940284252166748 69.996 3.8885459899902339
		 71.032 3.9508438110351567 72.064 4.1647238731384277 73.1 4.3434944152832031 74.136 4.273531436920166
		 75.168 4.1110391616821289 76.204 4.1015372276306152 77.24 4.1097307205200195 78.272 3.9446525573730464
		 79.308 3.6841461658477783 80.34 3.468233585357666 81.376 3.3475005626678467 82.412 3.3333380222320557
		 83.444 3.4388294219970703 84.48 3.590865850448608 85.512 3.5928888320922847 86.548 3.2582459449768066
		 87.584 2.8202219009399414 88.616 2.6287624835968018 89.652 2.527989387512207 90.688 2.227773904800415
		 91.72 1.9398285150527956 92.756 2.0492467880249023 93.788 2.3459124565124512 94.824 2.4567427635192871
		 95.86 2.4034814834594727 96.892 2.2161533832550049 97.928 1.9884212017059326 98.964 2.0061321258544922
		 99.996 2.2746903896331787 101.032 2.7317919731140137 102.064 3.2433719635009766 103.1 3.3613455295562744
		 104.136 3.1042923927307129 105.168 2.926577091217041 106.204 2.8478951454162598 107.24 2.7435569763183594
		 108.272 2.7145621776580811 109.308 2.7864091396331787 110.34 2.8289890289306641 111.376 2.8253345489501953
		 112.412 2.9049248695373535 113.444 3.0734732151031494 114.48 3.2563440799713135 115.512 3.3762052059173584
		 116.548 3.3408622741699219 117.584 3.0759766101837158 118.616 2.8108315467834473
		 119.652 2.7753536701202393 120.688 2.7535803318023682 121.72 2.4760372638702393 122.756 2.1455583572387695
		 123.788 2.0688800811767578 124.824 2.2617380619049072 125.86 2.5974130630493164 126.892 2.9433953762054443
		 127.928 3.047196626663208 128.964 2.7286844253540039 129.996 2.126230001449585 131.032 1.612385630607605
		 132.064 1.2965860366821289 133.1 1.1170197725296021 134.136 1.1466548442840576 135.168 1.3439114093780518
		 136.204 1.5323162078857422 137.236 1.5405213832855225 138.272 1.3587350845336914
		 139.308 1.135344386100769 140.34 0.88843142986297607 141.376 0.63663339614868164
		 142.412 0.57893663644790649 143.444 0.76507407426834106 144.48 1.0516341924667358
		 145.512 1.2043600082397461 146.548 1.0042195320129395 147.584 0.75792551040649414
		 148.616 0.8333669900894165 149.652 1.0143979787826538 150.688 0.98616528511047363
		 151.72 0.93588227033615123 152.756 1.0666975975036621 153.788 1.2153432369232178
		 154.824 1.2660598754882813 155.86 1.2300770282745361 156.892 1.2409476041793823 157.928 1.5456891059875488
		 158.964 1.82075035572052 159.996 1.5013322830200195 161.032 1.0504980087280273 162.064 1.1135793924331665
		 163.1 1.3351448774337769 164.136 1.3598729372024536 165.168 1.3875255584716797 166.204 1.5175340175628662
		 167.236 1.6042021512985229 168.272 1.5535413026809692 169.308 1.4343163967132568
		 170.34 1.3436357975006104 171.376 1.2474690675735474 172.412 1.148398756980896 173.444 1.1574018001556396
		 174.48 1.2309861183166504 175.512 1.2759033441543579 176.548 1.2876347303390503 177.584 1.3295103311538696
		 178.616 1.4079993963241577 179.652 1.4293861389160156 180.688 1.3399087190628052
		 181.72 1.2544766664505005 182.756 1.2388931512832642 183.788 1.2726846933364868 184.824 1.2047195434570313
		 185.86 0.74905848503112793 186.892 0.22970490157604218 187.928 0.1991066038608551
		 188.964 0.49106171727180487 189.996 0.66740608215332031 191.032 0.73219585418701172
		 192.064 0.77540409564971924 193.1 0.77080881595611572 194.136 0.78215301036834717
		 195.168 0.83930569887161255 196.204 0.92402100563049305 197.236 0.98346000909805298
		 198.272 0.90877550840377808 199.308 0.74893432855606079 200.34 0.69170522689819336
		 201.376 0.77359217405319214 202.412 0.86122453212738037 203.444 0.88794159889221191
		 204.48 0.8293796181678772 205.512 0.69636768102645874 206.548 0.5942305326461792
		 207.584 0.55056285858154297 208.616 0.52185982465744019 209.652 0.46308830380439758
		 210.688 0.28019875288009644 211.72 0.014112557284533978 212.756 -0.10380544513463974
		 213.788 -0.071782290935516357 214.824 -0.047709506005048752 215.86 -0.059162668883800507
		 216.892 -0.088689059019088745 217.928 -0.2162596583366394 218.964 -0.42174825072288513
		 219.996 -0.49595302343368536 221.032 -0.38465043902397156 222.064 -0.25723940134048462
		 223.1 -0.20425254106521606 224.136 -0.17478747665882111 225.168 -0.14791429042816162
		 226.204 -0.10457158088684082 227.236 -0.00084353215061128139 228.272 0.069101385772228241
		 229.308 0.011979944072663784 230.34 -0.068884819746017456 231.376 -0.069962687790393829
		 232.412 -0.036219585686922073 233.444 0.032477430999279022 234.48 0.13873493671417236
		 235.512 0.25792628526687622 236.548 0.43273693323135376 237.584 0.64619994163513184
		 238.616 0.83527863025665283 239.652 0.90209865570068348 240.688 0.8393779993057251
		 241.72 0.82534152269363403 242.756 1.0484961271286011 243.788 1.3615999221801758
		 244.824 1.5705039501190186 245.86 1.701326847076416 246.892 1.9018532037734988 247.928 2.1748137474060059
		 248.96 2.4218106269836426 249.996 2.6301631927490234 251.032 2.8187284469604492 252.064 3.0119798183441162
		 253.1 3.3182497024536133 254.136 3.6517083644866948 255.168 3.8761932849884029 256.204 4.1507201194763184
		 257.236 4.4158420562744141 258.272 4.3971700668334961;
	setAttr ".ktv[250:290]" 259.308 4.2266378402709961 260.34 4.0542178153991699
		 261.376 3.9431798458099365 262.412 4.0177650451660156 263.444 4.1793513298034668
		 264.48 4.2955155372619629 265.512 4.345921516418457 266.548 4.3702688217163086 267.584 4.41064453125
		 268.616 4.4589142799377441 269.652 4.4928345680236816 270.688 4.5319328308105469
		 271.72 4.5234923362731934 272.756 4.3695306777954102 273.788 4.0839219093322754 274.824 3.8991186618804932
		 275.86 3.9728877544403076 276.892 4.058382511138916 277.928 3.9486665725708012 278.96 3.7258706092834473
		 279.996 3.4430928230285645 281.032 3.1539978981018066 282.064 3.0170323848724365
		 283.1 3.0110583305358887 284.136 2.9581189155578613 285.168 2.7855052947998047 286.204 2.5981264114379883
		 287.236 2.5612633228302002 288.272 2.6514027118682861 289.308 2.7037594318389893
		 290.34 2.7428247928619385 291.376 2.7791028022766113 292.412 2.7688136100769043 293.444 2.7597131729125977
		 294.48 2.778264045715332 295.512 2.7289624214172363 296.548 2.6163897514343262 297.584 2.5946614742279053
		 298.616 2.6863746643066406 299.652 2.8185951709747314 300.688 2.9485533237457275;
select -ne :time1;
	setAttr ".o" 12;
	setAttr ".unw" 12;
select -ne :renderPartition;
	setAttr -s 130 ".st";
select -ne :initialShadingGroup;
	setAttr ".ro" yes;
select -ne :initialParticleSE;
	setAttr ".ro" yes;
select -ne :defaultShaderList1;
	setAttr -s 66 ".s";
select -ne :defaultTextureList1;
	setAttr -s 32 ".tx";
select -ne :postProcessList1;
	setAttr -s 2 ".p";
select -ne :defaultRenderUtilityList1;
	setAttr -s 32 ".u";
select -ne :defaultRenderingList1;
select -ne :renderGlobalsList1;
select -ne :defaultResolution;
	setAttr ".pa" 1;
select -ne :hardwareRenderGlobals;
	setAttr ".ctrs" 256;
	setAttr ".btrs" 512;
select -ne :hardwareRenderingGlobals;
	setAttr ".otfna" -type "stringArray" 18 "NURBS Curves" "NURBS Surfaces" "Polygons" "Subdiv Surfaces" "Particles" "Fluids" "Image Planes" "UI:" "Lights" "Cameras" "Locators" "Joints" "IK Handles" "Deformers" "Motion Trails" "Components" "Misc. UI" "Ornaments"  ;
	setAttr ".otfva" -type "Int32Array" 18 0 1 1 1 1 1
		 1 0 0 0 0 0 0 0 0 0 0 0 ;
select -ne :defaultHardwareRenderGlobals;
	setAttr ".fn" -type "string" "im";
	setAttr ".res" -type "string" "ntsc_4d 646 485 1.333";
select -ne :hyperGraphLayout;
	setAttr -s 4 ".hyp";
connectAttr "Bip01_Spine_translateX.o" "Bip01_Spine.tx";
connectAttr "Bip01_Spine_translateY.o" "Bip01_Spine.ty";
connectAttr "Bip01_Spine_translateZ.o" "Bip01_Spine.tz";
connectAttr "Bip01_Spine_rotateX.o" "Bip01_Spine.rx";
connectAttr "Bip01_Spine_rotateY.o" "Bip01_Spine.ry";
connectAttr "Bip01_Spine_rotateZ.o" "Bip01_Spine.rz";
connectAttr "Bip01_Spine1_rotateX.o" "Bip01_Spine1.rx";
connectAttr "Bip01_Spine1_rotateY.o" "Bip01_Spine1.ry";
connectAttr "Bip01_Spine1_rotateZ.o" "Bip01_Spine1.rz";
connectAttr "Bip01_Spine.s" "Bip01_Spine1.is";
connectAttr "Bip01_Spine2_rotateX.o" "Bip01_Spine2.rx";
connectAttr "Bip01_Spine2_rotateY.o" "Bip01_Spine2.ry";
connectAttr "Bip01_Spine2_rotateZ.o" "Bip01_Spine2.rz";
connectAttr "Bip01_Spine1.s" "Bip01_Spine2.is";
connectAttr "Bip01_Neck_rotateX.o" "Bip01_Neck.rx";
connectAttr "Bip01_Neck_rotateY.o" "Bip01_Neck.ry";
connectAttr "Bip01_Neck_rotateZ.o" "Bip01_Neck.rz";
connectAttr "Bip01_Spine2.s" "Bip01_Neck.is";
connectAttr "Bip01_Head_rotateX.o" "Bip01_Head.rx";
connectAttr "Bip01_Head_rotateY.o" "Bip01_Head.ry";
connectAttr "Bip01_Head_rotateZ.o" "Bip01_Head.rz";
connectAttr "Bip01_Neck.s" "Bip01_Head.is";
connectAttr "Bip01_Head.s" "_notused_Bip01_HeadNub.is";
connectAttr "Bip01_R_Clavicle_rotateX.o" "Bip01_R_Clavicle.rx";
connectAttr "Bip01_R_Clavicle_rotateY.o" "Bip01_R_Clavicle.ry";
connectAttr "Bip01_R_Clavicle_rotateZ.o" "Bip01_R_Clavicle.rz";
connectAttr "Bip01_Neck.s" "Bip01_R_Clavicle.is";
connectAttr "Bip01_R_UpperArm_rotateX.o" "Bip01_R_UpperArm.rx";
connectAttr "Bip01_R_UpperArm_rotateY.o" "Bip01_R_UpperArm.ry";
connectAttr "Bip01_R_UpperArm_rotateZ.o" "Bip01_R_UpperArm.rz";
connectAttr "Bip01_R_Clavicle.s" "Bip01_R_UpperArm.is";
connectAttr "Bip01_R_Forearm_rotateY.o" "Bip01_R_Forearm.ry";
connectAttr "Bip01_R_Forearm_rotateX.o" "Bip01_R_Forearm.rx";
connectAttr "Bip01_R_Forearm_rotateZ.o" "Bip01_R_Forearm.rz";
connectAttr "Bip01_R_UpperArm.s" "Bip01_R_Forearm.is";
connectAttr "Bip01_R_Forearm.s" "Bip01_R_Hand.is";
connectAttr "Bip01_R_Hand.s" "Bip01_R_Finger0.is";
connectAttr "Bip01_R_Finger0.s" "Bip01_R_Finger01.is";
connectAttr "Bip01_R_Finger01.s" "_notused_Bip01_R_Finger0Nub.is";
connectAttr "Bip01_R_Hand.s" "Bip01_R_Finger2.is";
connectAttr "Bip01_R_Finger2.s" "Bip01_R_Finger21.is";
connectAttr "Bip01_R_Finger21.s" "_notused_Bip01_R_Finger2Nub.is";
connectAttr "Bip01_R_Hand.s" "Bip01_R_Finger1.is";
connectAttr "Bip01_R_Finger1.s" "Bip01_R_Finger11.is";
connectAttr "Bip01_R_Finger11.s" "_notused_Bip01_R_Finger1Nub.is";
connectAttr "Bip01_R_Hand.s" "Bip01_R_Finger3.is";
connectAttr "Bip01_R_Finger3.s" "Bip01_R_Finger31.is";
connectAttr "Bip01_R_Finger31.s" "_notused_Bip01_R_Finger3Nub.is";
connectAttr "Bip01_R_Hand.s" "Bip01_R_Finger4.is";
connectAttr "Bip01_R_Finger4.s" "Bip01_R_Finger41.is";
connectAttr "Bip01_R_Finger41.s" "_notused_Bip01_R_Finger4Nub.is";
connectAttr "Bip01_L_Clavicle_rotateX.o" "Bip01_L_Clavicle.rx";
connectAttr "Bip01_L_Clavicle_rotateY.o" "Bip01_L_Clavicle.ry";
connectAttr "Bip01_L_Clavicle_rotateZ.o" "Bip01_L_Clavicle.rz";
connectAttr "Bip01_Neck.s" "Bip01_L_Clavicle.is";
connectAttr "Bip01_L_UpperArm_rotateX.o" "Bip01_L_UpperArm.rx";
connectAttr "Bip01_L_UpperArm_rotateY.o" "Bip01_L_UpperArm.ry";
connectAttr "Bip01_L_UpperArm_rotateZ.o" "Bip01_L_UpperArm.rz";
connectAttr "Bip01_L_Clavicle.s" "Bip01_L_UpperArm.is";
connectAttr "Bip01_L_Forearm_rotateY.o" "Bip01_L_Forearm.ry";
connectAttr "Bip01_L_Forearm_rotateZ.o" "Bip01_L_Forearm.rz";
connectAttr "Bip01_L_Forearm_rotateX.o" "Bip01_L_Forearm.rx";
connectAttr "Bip01_L_UpperArm.s" "Bip01_L_Forearm.is";
connectAttr "Bip01_L_Forearm.s" "Bip01_L_Hand.is";
connectAttr "Bip01_L_Hand.s" "Bip01_L_Finger0.is";
connectAttr "Bip01_L_Finger0.s" "Bip01_L_Finger01.is";
connectAttr "Bip01_L_Finger01.s" "_notused_Bip01_L_Finger0Nub.is";
connectAttr "Bip01_L_Hand.s" "Bip01_L_Finger2.is";
connectAttr "Bip01_L_Finger2.s" "Bip01_L_Finger21.is";
connectAttr "Bip01_L_Finger21.s" "_notused_Bip01_L_Finger2Nub.is";
connectAttr "Bip01_L_Hand.s" "Bip01_L_Finger1.is";
connectAttr "Bip01_L_Finger1.s" "Bip01_L_Finger11.is";
connectAttr "Bip01_L_Finger11.s" "_notused_Bip01_L_Finger1Nub.is";
connectAttr "Bip01_L_Hand.s" "Bip01_L_Finger3.is";
connectAttr "Bip01_L_Finger3.s" "Bip01_L_Finger31.is";
connectAttr "Bip01_L_Finger31.s" "_notused_Bip01_L_Finger3Nub.is";
connectAttr "Bip01_L_Hand.s" "Bip01_L_Finger4.is";
connectAttr "Bip01_L_Finger4.s" "Bip01_L_Finger41.is";
connectAttr "Bip01_L_Finger41.s" "_notused_Bip01_L_Finger4Nub.is";
connectAttr "Bip01_L_Thigh_rotateX.o" "Bip01_L_Thigh.rx";
connectAttr "Bip01_L_Thigh_rotateY.o" "Bip01_L_Thigh.ry";
connectAttr "Bip01_L_Thigh_rotateZ.o" "Bip01_L_Thigh.rz";
connectAttr "Bip01_Spine.s" "Bip01_L_Thigh.is";
connectAttr "Bip01_L_Calf_rotateX.o" "Bip01_L_Calf.rx";
connectAttr "Bip01_L_Calf_rotateY.o" "Bip01_L_Calf.ry";
connectAttr "Bip01_L_Calf_rotateZ.o" "Bip01_L_Calf.rz";
connectAttr "Bip01_L_Thigh.s" "Bip01_L_Calf.is";
connectAttr "Bip01_L_Foot_rotateX.o" "Bip01_L_Foot.rx";
connectAttr "Bip01_L_Foot_rotateY.o" "Bip01_L_Foot.ry";
connectAttr "Bip01_L_Foot_rotateZ.o" "Bip01_L_Foot.rz";
connectAttr "Bip01_L_Calf.s" "Bip01_L_Foot.is";
connectAttr "Bip01_L_Foot.s" "Bip01_L_Toe0.is";
connectAttr "Bip01_L_Toe0.s" "_notused_Bip01_L_Toe0Nub.is";
connectAttr "Bip01_R_Thigh_rotateX.o" "Bip01_R_Thigh.rx";
connectAttr "Bip01_R_Thigh_rotateY.o" "Bip01_R_Thigh.ry";
connectAttr "Bip01_R_Thigh_rotateZ.o" "Bip01_R_Thigh.rz";
connectAttr "Bip01_Spine.s" "Bip01_R_Thigh.is";
connectAttr "Bip01_R_Calf_rotateX.o" "Bip01_R_Calf.rx";
connectAttr "Bip01_R_Calf_rotateY.o" "Bip01_R_Calf.ry";
connectAttr "Bip01_R_Calf_rotateZ.o" "Bip01_R_Calf.rz";
connectAttr "Bip01_R_Thigh.s" "Bip01_R_Calf.is";
connectAttr "Bip01_R_Foot_rotateX.o" "Bip01_R_Foot.rx";
connectAttr "Bip01_R_Foot_rotateY.o" "Bip01_R_Foot.ry";
connectAttr "Bip01_R_Foot_rotateZ.o" "Bip01_R_Foot.rz";
connectAttr "Bip01_R_Calf.s" "Bip01_R_Foot.is";
connectAttr "Bip01_R_Foot.s" "Bip01_R_Toe0.is";
connectAttr "Bip01_R_Toe0.s" "_notused_Bip01_R_Toe0Nub.is";
relationship "link" ":lightLinker1" ":initialShadingGroup.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" ":initialParticleSE.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" "business05_f_highpolySG.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" "business05_f_highpolySG1.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" "business05_f_highpoly_hairSG.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" "business05_f_highpoly_hairSG1.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" "MoCap_Result_Animations_v1_business05_f_highpolySG.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" "MoCap_Result_Animations_v1_business05_f_highpolySG1.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" "MoCap_Result_Animations_v1_business05_f_highpoly_hairSG.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" "MoCap_Result_Animations_v1_business05_f_highpoly_hairSG1.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" "idle_v1:business05_f_highpolySG.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" "idle_v1:business05_f_highpolySG1.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" "idle_v1:business05_f_highpoly_hairSG.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" "idle_v1:business05_f_highpoly_hairSG1.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" "idle_v1_business05_f_highpolySG.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" "idle_v1_business05_f_highpolySG1.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" "idle_v1_business05_f_highpoly_hairSG.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" "idle_v1_business05_f_highpoly_hairSG1.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" "idle_v1_business05_f_highpolySG2.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" "idle_v1_business05_f_highpolySG3.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" "idle_v1_business05_f_highpoly_hairSG2.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" "idle_v1_business05_f_highpoly_hairSG3.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" "stand_v1_business05_f_highpolySG.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" "stand_v1_business05_f_highpolySG1.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" "stand_v1_business05_f_highpoly_hairSG.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" "stand_v1_business05_f_highpoly_hairSG1.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" "sit_idle_v1_business05_f_highpolySG.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" "sit_idle_v1_business05_f_highpolySG1.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" "sit_idle_v1_business05_f_highpoly_hairSG.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" "sit_idle_v1_business05_f_highpoly_hairSG1.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" "sit_talk_v1_business05_f_highpolySG.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" "sit_talk_v1_business05_f_highpolySG1.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" "sit_talk_v1_business05_f_highpoly_hairSG.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" "sit_talk_v1_business05_f_highpoly_hairSG1.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" "Animations_v3_business05_f_highpolySG.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" "Animations_v3_business05_f_highpolySG1.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" "Animations_v3_business05_f_highpoly_hairSG.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" "Animations_v3_business05_f_highpoly_hairSG1.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" "Jessica_Take_2_2_business05_f_highpolySG.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" "Jessica_Take_2_2_business05_f_highpolySG1.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" "Jessica_Take_2_2_business05_f_highpoly_hairSG.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" "Jessica_Take_2_2_business05_f_highpoly_hairSG1.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" "Jessica_Take_2_2_business05_f_highpolySG2.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" "Jessica_Take_2_2_business05_f_highpolySG3.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" "Jessica_Take_2_2_business05_f_highpoly_hairSG2.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" "Jessica_Take_2_2_business05_f_highpoly_hairSG3.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" "Jessica_Take_2_2_business05_f_highpolySG4.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" "Jessica_Take_2_2_business05_f_highpolySG5.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" "Jessica_Take_2_2_business05_f_highpoly_hairSG4.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" "Jessica_Take_2_2_business05_f_highpoly_hairSG5.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" "Jessica_Take_2_2_business05_f_highpolySG6.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" "Jessica_Take_2_2_business05_f_highpolySG7.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" "Jessica_Take_2_2_business05_f_highpoly_hairSG6.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" "Jessica_Take_2_2_business05_f_highpoly_hairSG7.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" "Jessica_Take_2_2_business05_f_highpolySG8.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" "Jessica_Take_2_2_business05_f_highpolySG9.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" "Jessica_Take_2_2_business05_f_highpoly_hairSG8.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" "Jessica_Take_2_2_business05_f_highpoly_hairSG9.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" "Jessica_Take_2_2_business05_f_highpolySG10.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" "Jessica_Take_2_2_business05_f_highpolySG11.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" "Jessica_Take_2_2_business05_f_highpoly_hairSG10.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" "Jessica_Take_2_2_business05_f_highpoly_hairSG11.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" "Candice_Take_1_business05_f_highpolySG.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" "Candice_Take_1_business05_f_highpolySG1.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" "Candice_Take_1_business05_f_highpoly_hairSG.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" "Candice_Take_1_business05_f_highpoly_hairSG1.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" "Candice_Take_1_business05_f_highpolySG2.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" "Candice_Take_1_business05_f_highpolySG3.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" "Candice_Take_1_business05_f_highpoly_hairSG2.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" "Candice_Take_1_business05_f_highpoly_hairSG3.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" "Candice_Take_1_business05_f_highpolySG4.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" "Candice_Take_1_business05_f_highpolySG5.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" "Candice_Take_1_business05_f_highpoly_hairSG4.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" "Candice_Take_1_business05_f_highpoly_hairSG5.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" "Candice_Take_1_business05_f_highpolySG6.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" "Candice_Take_1_business05_f_highpolySG7.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" "Candice_Take_1_business05_f_highpoly_hairSG6.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" "Candice_Take_1_business05_f_highpoly_hairSG7.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" "Candice_Take_1_business05_f_highpolySG8.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" "Candice_Take_1_business05_f_highpolySG9.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" "Candice_Take_1_business05_f_highpoly_hairSG8.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" "Candice_Take_1_business05_f_highpoly_hairSG9.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" "Candice_Take_1_business05_f_highpolySG10.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" "Candice_Take_1_business05_f_highpolySG11.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" "Candice_Take_1_business05_f_highpoly_hairSG10.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" "Candice_Take_1_business05_f_highpoly_hairSG11.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" "Candice_Take_1_business05_f_highpolySG12.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" "Candice_Take_1_business05_f_highpolySG13.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" "Candice_Take_1_business05_f_highpoly_hairSG12.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" "Candice_Take_1_business05_f_highpoly_hairSG13.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" "Candice_Take_1_business05_f_highpolySG14.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" "Candice_Take_1_business05_f_highpolySG15.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" "Candice_Take_1_business05_f_highpoly_hairSG14.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" "Candice_Take_1_business05_f_highpoly_hairSG15.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" "Animations_v3_business05_f_highpolySG2.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" "Animations_v3_business05_f_highpolySG3.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" "Animations_v3_business05_f_highpoly_hairSG2.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" "Animations_v3_business05_f_highpoly_hairSG3.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" "Kuan_take_1_business05_f_highpolySG.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" "Kuan_take_1_business05_f_highpolySG1.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" "Kuan_take_1_business05_f_highpoly_hairSG.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" "Kuan_take_1_business05_f_highpoly_hairSG1.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" "Kuan_take_1_business05_f_highpolySG2.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" "Kuan_take_1_business05_f_highpolySG3.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" "Kuan_take_1_business05_f_highpoly_hairSG2.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" "Kuan_take_1_business05_f_highpoly_hairSG3.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" "Kuan_take_1_business05_f_highpolySG4.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" "Kuan_take_1_business05_f_highpolySG5.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" "Kuan_take_1_business05_f_highpoly_hairSG4.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" "Kuan_take_1_business05_f_highpoly_hairSG5.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" "Kuan_take_1_business05_f_highpolySG6.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" "Kuan_take_1_business05_f_highpolySG7.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" "Kuan_take_1_business05_f_highpoly_hairSG6.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" "Kuan_take_1_business05_f_highpoly_hairSG7.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" "Kuan_take_1_business05_f_highpolySG8.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" "Kuan_take_1_business05_f_highpolySG9.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" "Kuan_take_1_business05_f_highpoly_hairSG8.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" "Kuan_take_1_business05_f_highpoly_hairSG9.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" "Kuan_take_1_business05_f_highpolySG10.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" "Kuan_take_1_business05_f_highpolySG11.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" "Kuan_take_1_business05_f_highpoly_hairSG10.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" "Kuan_take_1_business05_f_highpoly_hairSG11.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" "Kuan_take_1_business05_f_highpolySG12.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" "Kuan_take_1_business05_f_highpolySG13.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" "Kuan_take_1_business05_f_highpoly_hairSG12.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" "Kuan_take_1_business05_f_highpoly_hairSG13.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" "Candice_take_2_business05_f_highpolySG.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" "Candice_take_2_business05_f_highpolySG1.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" "Candice_take_2_business05_f_highpoly_hairSG.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" "Candice_take_2_business05_f_highpoly_hairSG1.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" ":initialShadingGroup.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" ":initialParticleSE.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" "business05_f_highpolySG.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" "business05_f_highpolySG1.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" "business05_f_highpoly_hairSG.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" "business05_f_highpoly_hairSG1.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" "MoCap_Result_Animations_v1_business05_f_highpolySG.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" "MoCap_Result_Animations_v1_business05_f_highpolySG1.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" "MoCap_Result_Animations_v1_business05_f_highpoly_hairSG.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" "MoCap_Result_Animations_v1_business05_f_highpoly_hairSG1.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" "idle_v1:business05_f_highpolySG.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" "idle_v1:business05_f_highpolySG1.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" "idle_v1:business05_f_highpoly_hairSG.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" "idle_v1:business05_f_highpoly_hairSG1.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" "idle_v1_business05_f_highpolySG.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" "idle_v1_business05_f_highpolySG1.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" "idle_v1_business05_f_highpoly_hairSG.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" "idle_v1_business05_f_highpoly_hairSG1.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" "idle_v1_business05_f_highpolySG2.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" "idle_v1_business05_f_highpolySG3.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" "idle_v1_business05_f_highpoly_hairSG2.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" "idle_v1_business05_f_highpoly_hairSG3.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" "stand_v1_business05_f_highpolySG.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" "stand_v1_business05_f_highpolySG1.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" "stand_v1_business05_f_highpoly_hairSG.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" "stand_v1_business05_f_highpoly_hairSG1.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" "sit_idle_v1_business05_f_highpolySG.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" "sit_idle_v1_business05_f_highpolySG1.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" "sit_idle_v1_business05_f_highpoly_hairSG.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" "sit_idle_v1_business05_f_highpoly_hairSG1.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" "sit_talk_v1_business05_f_highpolySG.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" "sit_talk_v1_business05_f_highpolySG1.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" "sit_talk_v1_business05_f_highpoly_hairSG.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" "sit_talk_v1_business05_f_highpoly_hairSG1.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" "Animations_v3_business05_f_highpolySG.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" "Animations_v3_business05_f_highpolySG1.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" "Animations_v3_business05_f_highpoly_hairSG.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" "Animations_v3_business05_f_highpoly_hairSG1.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" "Jessica_Take_2_2_business05_f_highpolySG.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" "Jessica_Take_2_2_business05_f_highpolySG1.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" "Jessica_Take_2_2_business05_f_highpoly_hairSG.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" "Jessica_Take_2_2_business05_f_highpoly_hairSG1.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" "Jessica_Take_2_2_business05_f_highpolySG2.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" "Jessica_Take_2_2_business05_f_highpolySG3.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" "Jessica_Take_2_2_business05_f_highpoly_hairSG2.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" "Jessica_Take_2_2_business05_f_highpoly_hairSG3.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" "Jessica_Take_2_2_business05_f_highpolySG4.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" "Jessica_Take_2_2_business05_f_highpolySG5.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" "Jessica_Take_2_2_business05_f_highpoly_hairSG4.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" "Jessica_Take_2_2_business05_f_highpoly_hairSG5.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" "Jessica_Take_2_2_business05_f_highpolySG6.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" "Jessica_Take_2_2_business05_f_highpolySG7.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" "Jessica_Take_2_2_business05_f_highpoly_hairSG6.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" "Jessica_Take_2_2_business05_f_highpoly_hairSG7.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" "Jessica_Take_2_2_business05_f_highpolySG8.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" "Jessica_Take_2_2_business05_f_highpolySG9.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" "Jessica_Take_2_2_business05_f_highpoly_hairSG8.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" "Jessica_Take_2_2_business05_f_highpoly_hairSG9.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" "Jessica_Take_2_2_business05_f_highpolySG10.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" "Jessica_Take_2_2_business05_f_highpolySG11.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" "Jessica_Take_2_2_business05_f_highpoly_hairSG10.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" "Jessica_Take_2_2_business05_f_highpoly_hairSG11.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" "Candice_Take_1_business05_f_highpolySG.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" "Candice_Take_1_business05_f_highpolySG1.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" "Candice_Take_1_business05_f_highpoly_hairSG.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" "Candice_Take_1_business05_f_highpoly_hairSG1.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" "Candice_Take_1_business05_f_highpolySG2.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" "Candice_Take_1_business05_f_highpolySG3.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" "Candice_Take_1_business05_f_highpoly_hairSG2.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" "Candice_Take_1_business05_f_highpoly_hairSG3.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" "Candice_Take_1_business05_f_highpolySG4.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" "Candice_Take_1_business05_f_highpolySG5.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" "Candice_Take_1_business05_f_highpoly_hairSG4.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" "Candice_Take_1_business05_f_highpoly_hairSG5.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" "Candice_Take_1_business05_f_highpolySG6.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" "Candice_Take_1_business05_f_highpolySG7.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" "Candice_Take_1_business05_f_highpoly_hairSG6.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" "Candice_Take_1_business05_f_highpoly_hairSG7.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" "Candice_Take_1_business05_f_highpolySG8.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" "Candice_Take_1_business05_f_highpolySG9.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" "Candice_Take_1_business05_f_highpoly_hairSG8.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" "Candice_Take_1_business05_f_highpoly_hairSG9.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" "Candice_Take_1_business05_f_highpolySG10.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" "Candice_Take_1_business05_f_highpolySG11.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" "Candice_Take_1_business05_f_highpoly_hairSG10.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" "Candice_Take_1_business05_f_highpoly_hairSG11.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" "Candice_Take_1_business05_f_highpolySG12.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" "Candice_Take_1_business05_f_highpolySG13.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" "Candice_Take_1_business05_f_highpoly_hairSG12.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" "Candice_Take_1_business05_f_highpoly_hairSG13.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" "Candice_Take_1_business05_f_highpolySG14.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" "Candice_Take_1_business05_f_highpolySG15.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" "Candice_Take_1_business05_f_highpoly_hairSG14.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" "Candice_Take_1_business05_f_highpoly_hairSG15.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" "Animations_v3_business05_f_highpolySG2.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" "Animations_v3_business05_f_highpolySG3.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" "Animations_v3_business05_f_highpoly_hairSG2.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" "Animations_v3_business05_f_highpoly_hairSG3.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" "Kuan_take_1_business05_f_highpolySG.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" "Kuan_take_1_business05_f_highpolySG1.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" "Kuan_take_1_business05_f_highpoly_hairSG.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" "Kuan_take_1_business05_f_highpoly_hairSG1.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" "Kuan_take_1_business05_f_highpolySG2.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" "Kuan_take_1_business05_f_highpolySG3.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" "Kuan_take_1_business05_f_highpoly_hairSG2.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" "Kuan_take_1_business05_f_highpoly_hairSG3.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" "Kuan_take_1_business05_f_highpolySG4.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" "Kuan_take_1_business05_f_highpolySG5.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" "Kuan_take_1_business05_f_highpoly_hairSG4.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" "Kuan_take_1_business05_f_highpoly_hairSG5.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" "Kuan_take_1_business05_f_highpolySG6.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" "Kuan_take_1_business05_f_highpolySG7.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" "Kuan_take_1_business05_f_highpoly_hairSG6.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" "Kuan_take_1_business05_f_highpoly_hairSG7.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" "Kuan_take_1_business05_f_highpolySG8.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" "Kuan_take_1_business05_f_highpolySG9.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" "Kuan_take_1_business05_f_highpoly_hairSG8.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" "Kuan_take_1_business05_f_highpoly_hairSG9.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" "Kuan_take_1_business05_f_highpolySG10.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" "Kuan_take_1_business05_f_highpolySG11.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" "Kuan_take_1_business05_f_highpoly_hairSG10.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" "Kuan_take_1_business05_f_highpoly_hairSG11.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" "Kuan_take_1_business05_f_highpolySG12.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" "Kuan_take_1_business05_f_highpolySG13.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" "Kuan_take_1_business05_f_highpoly_hairSG12.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" "Kuan_take_1_business05_f_highpoly_hairSG13.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" "Candice_take_2_business05_f_highpolySG.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" "Candice_take_2_business05_f_highpolySG1.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" "Candice_take_2_business05_f_highpoly_hairSG.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" "Candice_take_2_business05_f_highpoly_hairSG1.message" ":defaultLightSet.message";
connectAttr "layerManager.dli[0]" "defaultLayer.id";
connectAttr "renderLayerManager.rlmi[0]" "defaultRenderLayer.rlid";
connectAttr "Map1.oc" "business05_f_highpolyPhong.c";
connectAttr "business05_f_highpolyPhong.oc" "business05_f_highpolySG.ss";
connectAttr "business05_f_highpolySG.msg" "materialInfo1.sg";
connectAttr "business05_f_highpolyPhong.msg" "materialInfo1.m";
connectAttr "Map1.msg" "materialInfo1.t" -na;
connectAttr "business05_f_highpolyPhong.oc" "business05_f_highpolySG1.ss";
connectAttr "business05_f_highpolySG1.msg" "materialInfo2.sg";
connectAttr "business05_f_highpolyPhong.msg" "materialInfo2.m";
connectAttr "Map1.msg" "materialInfo2.t" -na;
connectAttr "place2dTexture1.o" "Map1.uv";
connectAttr "place2dTexture1.ofu" "Map1.ofu";
connectAttr "place2dTexture1.ofv" "Map1.ofv";
connectAttr "place2dTexture1.rf" "Map1.rf";
connectAttr "place2dTexture1.reu" "Map1.reu";
connectAttr "place2dTexture1.rev" "Map1.rev";
connectAttr "place2dTexture1.vt1" "Map1.vt1";
connectAttr "place2dTexture1.vt2" "Map1.vt2";
connectAttr "place2dTexture1.vt3" "Map1.vt3";
connectAttr "place2dTexture1.vc1" "Map1.vc1";
connectAttr "place2dTexture1.ofs" "Map1.fs";
connectAttr "business05_f_highpoly_hairPhong.oc" "business05_f_highpoly_hairSG.ss"
		;
connectAttr "business05_f_highpoly_hairSG.msg" "materialInfo3.sg";
connectAttr "business05_f_highpoly_hairPhong.msg" "materialInfo3.m";
connectAttr "business05_f_highpoly_hairPhong.oc" "business05_f_highpoly_hairSG1.ss"
		;
connectAttr "business05_f_highpoly_hairSG1.msg" "materialInfo4.sg";
connectAttr "business05_f_highpoly_hairPhong.msg" "materialInfo4.m";
connectAttr "MoCap_Result_Animations_v1_Map1.oc" "MoCap_Result_Animations_v1_business05_f_highpolyPhong.c"
		;
connectAttr "MoCap_Result_Animations_v1_business05_f_highpolyPhong.oc" "MoCap_Result_Animations_v1_business05_f_highpolySG.ss"
		;
connectAttr "MoCap_Result_Animations_v1_business05_f_highpolySG.msg" "MoCap_Result_Animations_v1_materialInfo1.sg"
		;
connectAttr "MoCap_Result_Animations_v1_business05_f_highpolyPhong.msg" "MoCap_Result_Animations_v1_materialInfo1.m"
		;
connectAttr "MoCap_Result_Animations_v1_Map1.msg" "MoCap_Result_Animations_v1_materialInfo1.t"
		 -na;
connectAttr "MoCap_Result_Animations_v1_business05_f_highpolyPhong.oc" "MoCap_Result_Animations_v1_business05_f_highpolySG1.ss"
		;
connectAttr "MoCap_Result_Animations_v1_business05_f_highpolySG1.msg" "MoCap_Result_Animations_v1_materialInfo2.sg"
		;
connectAttr "MoCap_Result_Animations_v1_business05_f_highpolyPhong.msg" "MoCap_Result_Animations_v1_materialInfo2.m"
		;
connectAttr "MoCap_Result_Animations_v1_Map1.msg" "MoCap_Result_Animations_v1_materialInfo2.t"
		 -na;
connectAttr "MoCap_Result_Animations_v1_place2dTexture1.o" "MoCap_Result_Animations_v1_Map1.uv"
		;
connectAttr "MoCap_Result_Animations_v1_place2dTexture1.ofu" "MoCap_Result_Animations_v1_Map1.ofu"
		;
connectAttr "MoCap_Result_Animations_v1_place2dTexture1.ofv" "MoCap_Result_Animations_v1_Map1.ofv"
		;
connectAttr "MoCap_Result_Animations_v1_place2dTexture1.rf" "MoCap_Result_Animations_v1_Map1.rf"
		;
connectAttr "MoCap_Result_Animations_v1_place2dTexture1.reu" "MoCap_Result_Animations_v1_Map1.reu"
		;
connectAttr "MoCap_Result_Animations_v1_place2dTexture1.rev" "MoCap_Result_Animations_v1_Map1.rev"
		;
connectAttr "MoCap_Result_Animations_v1_place2dTexture1.vt1" "MoCap_Result_Animations_v1_Map1.vt1"
		;
connectAttr "MoCap_Result_Animations_v1_place2dTexture1.vt2" "MoCap_Result_Animations_v1_Map1.vt2"
		;
connectAttr "MoCap_Result_Animations_v1_place2dTexture1.vt3" "MoCap_Result_Animations_v1_Map1.vt3"
		;
connectAttr "MoCap_Result_Animations_v1_place2dTexture1.vc1" "MoCap_Result_Animations_v1_Map1.vc1"
		;
connectAttr "MoCap_Result_Animations_v1_place2dTexture1.ofs" "MoCap_Result_Animations_v1_Map1.fs"
		;
connectAttr "MoCap_Result_Animations_v1_business05_f_highpoly_hairPhong.oc" "MoCap_Result_Animations_v1_business05_f_highpoly_hairSG.ss"
		;
connectAttr "MoCap_Result_Animations_v1_business05_f_highpoly_hairSG.msg" "MoCap_Result_Animations_v1_materialInfo3.sg"
		;
connectAttr "MoCap_Result_Animations_v1_business05_f_highpoly_hairPhong.msg" "MoCap_Result_Animations_v1_materialInfo3.m"
		;
connectAttr "MoCap_Result_Animations_v1_business05_f_highpoly_hairPhong.oc" "MoCap_Result_Animations_v1_business05_f_highpoly_hairSG1.ss"
		;
connectAttr "MoCap_Result_Animations_v1_business05_f_highpoly_hairSG1.msg" "MoCap_Result_Animations_v1_materialInfo4.sg"
		;
connectAttr "MoCap_Result_Animations_v1_business05_f_highpoly_hairPhong.msg" "MoCap_Result_Animations_v1_materialInfo4.m"
		;
connectAttr "idle_v1:Map1.oc" "idle_v1:business05_f_highpolyPhong.c";
connectAttr "idle_v1:business05_f_highpolyPhong.oc" "idle_v1:business05_f_highpolySG.ss"
		;
connectAttr "idle_v1:business05_f_highpolySG.msg" "idle_v1:materialInfo1.sg";
connectAttr "idle_v1:business05_f_highpolyPhong.msg" "idle_v1:materialInfo1.m";
connectAttr "idle_v1:Map1.msg" "idle_v1:materialInfo1.t" -na;
connectAttr "idle_v1:business05_f_highpolyPhong.oc" "idle_v1:business05_f_highpolySG1.ss"
		;
connectAttr "idle_v1:business05_f_highpolySG1.msg" "idle_v1:materialInfo2.sg";
connectAttr "idle_v1:business05_f_highpolyPhong.msg" "idle_v1:materialInfo2.m";
connectAttr "idle_v1:Map1.msg" "idle_v1:materialInfo2.t" -na;
connectAttr "idle_v1:place2dTexture1.o" "idle_v1:Map1.uv";
connectAttr "idle_v1:place2dTexture1.ofu" "idle_v1:Map1.ofu";
connectAttr "idle_v1:place2dTexture1.ofv" "idle_v1:Map1.ofv";
connectAttr "idle_v1:place2dTexture1.rf" "idle_v1:Map1.rf";
connectAttr "idle_v1:place2dTexture1.reu" "idle_v1:Map1.reu";
connectAttr "idle_v1:place2dTexture1.rev" "idle_v1:Map1.rev";
connectAttr "idle_v1:place2dTexture1.vt1" "idle_v1:Map1.vt1";
connectAttr "idle_v1:place2dTexture1.vt2" "idle_v1:Map1.vt2";
connectAttr "idle_v1:place2dTexture1.vt3" "idle_v1:Map1.vt3";
connectAttr "idle_v1:place2dTexture1.vc1" "idle_v1:Map1.vc1";
connectAttr "idle_v1:place2dTexture1.ofs" "idle_v1:Map1.fs";
connectAttr "idle_v1:business05_f_highpoly_hairPhong.oc" "idle_v1:business05_f_highpoly_hairSG.ss"
		;
connectAttr "idle_v1:business05_f_highpoly_hairSG.msg" "idle_v1:materialInfo3.sg"
		;
connectAttr "idle_v1:business05_f_highpoly_hairPhong.msg" "idle_v1:materialInfo3.m"
		;
connectAttr "idle_v1:business05_f_highpoly_hairPhong.oc" "idle_v1:business05_f_highpoly_hairSG1.ss"
		;
connectAttr "idle_v1:business05_f_highpoly_hairSG1.msg" "idle_v1:materialInfo4.sg"
		;
connectAttr "idle_v1:business05_f_highpoly_hairPhong.msg" "idle_v1:materialInfo4.m"
		;
connectAttr "idle_v1_Map1.oc" "idle_v1_business05_f_highpolyPhong.c";
connectAttr "idle_v1_business05_f_highpolyPhong.oc" "idle_v1_business05_f_highpolySG.ss"
		;
connectAttr "idle_v1_business05_f_highpolySG.msg" "idle_v1_materialInfo1.sg";
connectAttr "idle_v1_business05_f_highpolyPhong.msg" "idle_v1_materialInfo1.m";
connectAttr "idle_v1_Map1.msg" "idle_v1_materialInfo1.t" -na;
connectAttr "idle_v1_business05_f_highpolyPhong.oc" "idle_v1_business05_f_highpolySG1.ss"
		;
connectAttr "idle_v1_business05_f_highpolySG1.msg" "idle_v1_materialInfo2.sg";
connectAttr "idle_v1_business05_f_highpolyPhong.msg" "idle_v1_materialInfo2.m";
connectAttr "idle_v1_Map1.msg" "idle_v1_materialInfo2.t" -na;
connectAttr "idle_v1_place2dTexture1.o" "idle_v1_Map1.uv";
connectAttr "idle_v1_place2dTexture1.ofu" "idle_v1_Map1.ofu";
connectAttr "idle_v1_place2dTexture1.ofv" "idle_v1_Map1.ofv";
connectAttr "idle_v1_place2dTexture1.rf" "idle_v1_Map1.rf";
connectAttr "idle_v1_place2dTexture1.reu" "idle_v1_Map1.reu";
connectAttr "idle_v1_place2dTexture1.rev" "idle_v1_Map1.rev";
connectAttr "idle_v1_place2dTexture1.vt1" "idle_v1_Map1.vt1";
connectAttr "idle_v1_place2dTexture1.vt2" "idle_v1_Map1.vt2";
connectAttr "idle_v1_place2dTexture1.vt3" "idle_v1_Map1.vt3";
connectAttr "idle_v1_place2dTexture1.vc1" "idle_v1_Map1.vc1";
connectAttr "idle_v1_place2dTexture1.ofs" "idle_v1_Map1.fs";
connectAttr "idle_v1_business05_f_highpoly_hairPhong.oc" "idle_v1_business05_f_highpoly_hairSG.ss"
		;
connectAttr "idle_v1_business05_f_highpoly_hairSG.msg" "idle_v1_materialInfo3.sg"
		;
connectAttr "idle_v1_business05_f_highpoly_hairPhong.msg" "idle_v1_materialInfo3.m"
		;
connectAttr "idle_v1_business05_f_highpoly_hairPhong.oc" "idle_v1_business05_f_highpoly_hairSG1.ss"
		;
connectAttr "idle_v1_business05_f_highpoly_hairSG1.msg" "idle_v1_materialInfo4.sg"
		;
connectAttr "idle_v1_business05_f_highpoly_hairPhong.msg" "idle_v1_materialInfo4.m"
		;
connectAttr "idle_v1_Map2.oc" "idle_v1_business05_f_highpolyPhong1.c";
connectAttr "idle_v1_business05_f_highpolyPhong1.oc" "idle_v1_business05_f_highpolySG2.ss"
		;
connectAttr "idle_v1_business05_f_highpolySG2.msg" "idle_v1_materialInfo5.sg";
connectAttr "idle_v1_business05_f_highpolyPhong1.msg" "idle_v1_materialInfo5.m";
connectAttr "idle_v1_Map2.msg" "idle_v1_materialInfo5.t" -na;
connectAttr "idle_v1_business05_f_highpolyPhong1.oc" "idle_v1_business05_f_highpolySG3.ss"
		;
connectAttr "idle_v1_business05_f_highpolySG3.msg" "idle_v1_materialInfo6.sg";
connectAttr "idle_v1_business05_f_highpolyPhong1.msg" "idle_v1_materialInfo6.m";
connectAttr "idle_v1_Map2.msg" "idle_v1_materialInfo6.t" -na;
connectAttr "idle_v1_place2dTexture2.o" "idle_v1_Map2.uv";
connectAttr "idle_v1_place2dTexture2.ofu" "idle_v1_Map2.ofu";
connectAttr "idle_v1_place2dTexture2.ofv" "idle_v1_Map2.ofv";
connectAttr "idle_v1_place2dTexture2.rf" "idle_v1_Map2.rf";
connectAttr "idle_v1_place2dTexture2.reu" "idle_v1_Map2.reu";
connectAttr "idle_v1_place2dTexture2.rev" "idle_v1_Map2.rev";
connectAttr "idle_v1_place2dTexture2.vt1" "idle_v1_Map2.vt1";
connectAttr "idle_v1_place2dTexture2.vt2" "idle_v1_Map2.vt2";
connectAttr "idle_v1_place2dTexture2.vt3" "idle_v1_Map2.vt3";
connectAttr "idle_v1_place2dTexture2.vc1" "idle_v1_Map2.vc1";
connectAttr "idle_v1_place2dTexture2.ofs" "idle_v1_Map2.fs";
connectAttr "idle_v1_business05_f_highpoly_hairPhong1.oc" "idle_v1_business05_f_highpoly_hairSG2.ss"
		;
connectAttr "idle_v1_business05_f_highpoly_hairSG2.msg" "idle_v1_materialInfo7.sg"
		;
connectAttr "idle_v1_business05_f_highpoly_hairPhong1.msg" "idle_v1_materialInfo7.m"
		;
connectAttr "idle_v1_business05_f_highpoly_hairPhong1.oc" "idle_v1_business05_f_highpoly_hairSG3.ss"
		;
connectAttr "idle_v1_business05_f_highpoly_hairSG3.msg" "idle_v1_materialInfo8.sg"
		;
connectAttr "idle_v1_business05_f_highpoly_hairPhong1.msg" "idle_v1_materialInfo8.m"
		;
connectAttr "stand_v1_Map1.oc" "stand_v1_business05_f_highpolyPhong.c";
connectAttr "stand_v1_business05_f_highpolyPhong.oc" "stand_v1_business05_f_highpolySG.ss"
		;
connectAttr "stand_v1_business05_f_highpolySG.msg" "stand_v1_materialInfo1.sg";
connectAttr "stand_v1_business05_f_highpolyPhong.msg" "stand_v1_materialInfo1.m"
		;
connectAttr "stand_v1_Map1.msg" "stand_v1_materialInfo1.t" -na;
connectAttr "stand_v1_business05_f_highpolyPhong.oc" "stand_v1_business05_f_highpolySG1.ss"
		;
connectAttr "stand_v1_business05_f_highpolySG1.msg" "stand_v1_materialInfo2.sg";
connectAttr "stand_v1_business05_f_highpolyPhong.msg" "stand_v1_materialInfo2.m"
		;
connectAttr "stand_v1_Map1.msg" "stand_v1_materialInfo2.t" -na;
connectAttr "stand_v1_place2dTexture1.o" "stand_v1_Map1.uv";
connectAttr "stand_v1_place2dTexture1.ofu" "stand_v1_Map1.ofu";
connectAttr "stand_v1_place2dTexture1.ofv" "stand_v1_Map1.ofv";
connectAttr "stand_v1_place2dTexture1.rf" "stand_v1_Map1.rf";
connectAttr "stand_v1_place2dTexture1.reu" "stand_v1_Map1.reu";
connectAttr "stand_v1_place2dTexture1.rev" "stand_v1_Map1.rev";
connectAttr "stand_v1_place2dTexture1.vt1" "stand_v1_Map1.vt1";
connectAttr "stand_v1_place2dTexture1.vt2" "stand_v1_Map1.vt2";
connectAttr "stand_v1_place2dTexture1.vt3" "stand_v1_Map1.vt3";
connectAttr "stand_v1_place2dTexture1.vc1" "stand_v1_Map1.vc1";
connectAttr "stand_v1_place2dTexture1.ofs" "stand_v1_Map1.fs";
connectAttr "stand_v1_business05_f_highpoly_hairPhong.oc" "stand_v1_business05_f_highpoly_hairSG.ss"
		;
connectAttr "stand_v1_business05_f_highpoly_hairSG.msg" "stand_v1_materialInfo3.sg"
		;
connectAttr "stand_v1_business05_f_highpoly_hairPhong.msg" "stand_v1_materialInfo3.m"
		;
connectAttr "stand_v1_business05_f_highpoly_hairPhong.oc" "stand_v1_business05_f_highpoly_hairSG1.ss"
		;
connectAttr "stand_v1_business05_f_highpoly_hairSG1.msg" "stand_v1_materialInfo4.sg"
		;
connectAttr "stand_v1_business05_f_highpoly_hairPhong.msg" "stand_v1_materialInfo4.m"
		;
connectAttr "sit_idle_v1_Map1.oc" "sit_idle_v1_business05_f_highpolyPhong.c";
connectAttr "sit_idle_v1_business05_f_highpolyPhong.oc" "sit_idle_v1_business05_f_highpolySG.ss"
		;
connectAttr "sit_idle_v1_business05_f_highpolySG.msg" "sit_idle_v1_materialInfo1.sg"
		;
connectAttr "sit_idle_v1_business05_f_highpolyPhong.msg" "sit_idle_v1_materialInfo1.m"
		;
connectAttr "sit_idle_v1_Map1.msg" "sit_idle_v1_materialInfo1.t" -na;
connectAttr "sit_idle_v1_business05_f_highpolyPhong.oc" "sit_idle_v1_business05_f_highpolySG1.ss"
		;
connectAttr "sit_idle_v1_business05_f_highpolySG1.msg" "sit_idle_v1_materialInfo2.sg"
		;
connectAttr "sit_idle_v1_business05_f_highpolyPhong.msg" "sit_idle_v1_materialInfo2.m"
		;
connectAttr "sit_idle_v1_Map1.msg" "sit_idle_v1_materialInfo2.t" -na;
connectAttr "sit_idle_v1_place2dTexture1.o" "sit_idle_v1_Map1.uv";
connectAttr "sit_idle_v1_place2dTexture1.ofu" "sit_idle_v1_Map1.ofu";
connectAttr "sit_idle_v1_place2dTexture1.ofv" "sit_idle_v1_Map1.ofv";
connectAttr "sit_idle_v1_place2dTexture1.rf" "sit_idle_v1_Map1.rf";
connectAttr "sit_idle_v1_place2dTexture1.reu" "sit_idle_v1_Map1.reu";
connectAttr "sit_idle_v1_place2dTexture1.rev" "sit_idle_v1_Map1.rev";
connectAttr "sit_idle_v1_place2dTexture1.vt1" "sit_idle_v1_Map1.vt1";
connectAttr "sit_idle_v1_place2dTexture1.vt2" "sit_idle_v1_Map1.vt2";
connectAttr "sit_idle_v1_place2dTexture1.vt3" "sit_idle_v1_Map1.vt3";
connectAttr "sit_idle_v1_place2dTexture1.vc1" "sit_idle_v1_Map1.vc1";
connectAttr "sit_idle_v1_place2dTexture1.ofs" "sit_idle_v1_Map1.fs";
connectAttr "sit_idle_v1_business05_f_highpoly_hairPhong.oc" "sit_idle_v1_business05_f_highpoly_hairSG.ss"
		;
connectAttr "sit_idle_v1_business05_f_highpoly_hairSG.msg" "sit_idle_v1_materialInfo3.sg"
		;
connectAttr "sit_idle_v1_business05_f_highpoly_hairPhong.msg" "sit_idle_v1_materialInfo3.m"
		;
connectAttr "sit_idle_v1_business05_f_highpoly_hairPhong.oc" "sit_idle_v1_business05_f_highpoly_hairSG1.ss"
		;
connectAttr "sit_idle_v1_business05_f_highpoly_hairSG1.msg" "sit_idle_v1_materialInfo4.sg"
		;
connectAttr "sit_idle_v1_business05_f_highpoly_hairPhong.msg" "sit_idle_v1_materialInfo4.m"
		;
connectAttr "sit_talk_v1_Map1.oc" "sit_talk_v1_business05_f_highpolyPhong.c";
connectAttr "sit_talk_v1_business05_f_highpolyPhong.oc" "sit_talk_v1_business05_f_highpolySG.ss"
		;
connectAttr "sit_talk_v1_business05_f_highpolySG.msg" "sit_talk_v1_materialInfo1.sg"
		;
connectAttr "sit_talk_v1_business05_f_highpolyPhong.msg" "sit_talk_v1_materialInfo1.m"
		;
connectAttr "sit_talk_v1_Map1.msg" "sit_talk_v1_materialInfo1.t" -na;
connectAttr "sit_talk_v1_business05_f_highpolyPhong.oc" "sit_talk_v1_business05_f_highpolySG1.ss"
		;
connectAttr "sit_talk_v1_business05_f_highpolySG1.msg" "sit_talk_v1_materialInfo2.sg"
		;
connectAttr "sit_talk_v1_business05_f_highpolyPhong.msg" "sit_talk_v1_materialInfo2.m"
		;
connectAttr "sit_talk_v1_Map1.msg" "sit_talk_v1_materialInfo2.t" -na;
connectAttr "sit_talk_v1_place2dTexture1.o" "sit_talk_v1_Map1.uv";
connectAttr "sit_talk_v1_place2dTexture1.ofu" "sit_talk_v1_Map1.ofu";
connectAttr "sit_talk_v1_place2dTexture1.ofv" "sit_talk_v1_Map1.ofv";
connectAttr "sit_talk_v1_place2dTexture1.rf" "sit_talk_v1_Map1.rf";
connectAttr "sit_talk_v1_place2dTexture1.reu" "sit_talk_v1_Map1.reu";
connectAttr "sit_talk_v1_place2dTexture1.rev" "sit_talk_v1_Map1.rev";
connectAttr "sit_talk_v1_place2dTexture1.vt1" "sit_talk_v1_Map1.vt1";
connectAttr "sit_talk_v1_place2dTexture1.vt2" "sit_talk_v1_Map1.vt2";
connectAttr "sit_talk_v1_place2dTexture1.vt3" "sit_talk_v1_Map1.vt3";
connectAttr "sit_talk_v1_place2dTexture1.vc1" "sit_talk_v1_Map1.vc1";
connectAttr "sit_talk_v1_place2dTexture1.ofs" "sit_talk_v1_Map1.fs";
connectAttr "sit_talk_v1_business05_f_highpoly_hairPhong.oc" "sit_talk_v1_business05_f_highpoly_hairSG.ss"
		;
connectAttr "sit_talk_v1_business05_f_highpoly_hairSG.msg" "sit_talk_v1_materialInfo3.sg"
		;
connectAttr "sit_talk_v1_business05_f_highpoly_hairPhong.msg" "sit_talk_v1_materialInfo3.m"
		;
connectAttr "sit_talk_v1_business05_f_highpoly_hairPhong.oc" "sit_talk_v1_business05_f_highpoly_hairSG1.ss"
		;
connectAttr "sit_talk_v1_business05_f_highpoly_hairSG1.msg" "sit_talk_v1_materialInfo4.sg"
		;
connectAttr "sit_talk_v1_business05_f_highpoly_hairPhong.msg" "sit_talk_v1_materialInfo4.m"
		;
connectAttr ":mentalrayGlobals.msg" ":mentalrayItemsList.glb";
connectAttr ":miDefaultOptions.msg" ":mentalrayItemsList.opt" -na;
connectAttr ":miDefaultFramebuffer.msg" ":mentalrayItemsList.fb" -na;
connectAttr ":miDefaultOptions.msg" ":mentalrayGlobals.opt";
connectAttr ":miDefaultFramebuffer.msg" ":mentalrayGlobals.fb";
connectAttr "Animations_v3_Map1.oc" "Animations_v3_business05_f_highpolyPhong.c"
		;
connectAttr "Animations_v3_business05_f_highpolyPhong.oc" "Animations_v3_business05_f_highpolySG.ss"
		;
connectAttr "Animations_v3_business05_f_highpolySG.msg" "Animations_v3_materialInfo1.sg"
		;
connectAttr "Animations_v3_business05_f_highpolyPhong.msg" "Animations_v3_materialInfo1.m"
		;
connectAttr "Animations_v3_Map1.msg" "Animations_v3_materialInfo1.t" -na;
connectAttr "Animations_v3_business05_f_highpolyPhong.oc" "Animations_v3_business05_f_highpolySG1.ss"
		;
connectAttr "Animations_v3_business05_f_highpolySG1.msg" "Animations_v3_materialInfo2.sg"
		;
connectAttr "Animations_v3_business05_f_highpolyPhong.msg" "Animations_v3_materialInfo2.m"
		;
connectAttr "Animations_v3_Map1.msg" "Animations_v3_materialInfo2.t" -na;
connectAttr "Animations_v3_place2dTexture1.o" "Animations_v3_Map1.uv";
connectAttr "Animations_v3_place2dTexture1.ofu" "Animations_v3_Map1.ofu";
connectAttr "Animations_v3_place2dTexture1.ofv" "Animations_v3_Map1.ofv";
connectAttr "Animations_v3_place2dTexture1.rf" "Animations_v3_Map1.rf";
connectAttr "Animations_v3_place2dTexture1.reu" "Animations_v3_Map1.reu";
connectAttr "Animations_v3_place2dTexture1.rev" "Animations_v3_Map1.rev";
connectAttr "Animations_v3_place2dTexture1.vt1" "Animations_v3_Map1.vt1";
connectAttr "Animations_v3_place2dTexture1.vt2" "Animations_v3_Map1.vt2";
connectAttr "Animations_v3_place2dTexture1.vt3" "Animations_v3_Map1.vt3";
connectAttr "Animations_v3_place2dTexture1.vc1" "Animations_v3_Map1.vc1";
connectAttr "Animations_v3_place2dTexture1.ofs" "Animations_v3_Map1.fs";
connectAttr "Animations_v3_business05_f_highpoly_hairPhong.oc" "Animations_v3_business05_f_highpoly_hairSG.ss"
		;
connectAttr "Animations_v3_business05_f_highpoly_hairSG.msg" "Animations_v3_materialInfo3.sg"
		;
connectAttr "Animations_v3_business05_f_highpoly_hairPhong.msg" "Animations_v3_materialInfo3.m"
		;
connectAttr "Animations_v3_business05_f_highpoly_hairPhong.oc" "Animations_v3_business05_f_highpoly_hairSG1.ss"
		;
connectAttr "Animations_v3_business05_f_highpoly_hairSG1.msg" "Animations_v3_materialInfo4.sg"
		;
connectAttr "Animations_v3_business05_f_highpoly_hairPhong.msg" "Animations_v3_materialInfo4.m"
		;
connectAttr "Jessica_Take_2_2_Map1.oc" "Jessica_Take_2_2_business05_f_highpolyPhong.c"
		;
connectAttr "Jessica_Take_2_2_business05_f_highpolyPhong.oc" "Jessica_Take_2_2_business05_f_highpolySG.ss"
		;
connectAttr "Jessica_Take_2_2_business05_f_highpolySG.msg" "Jessica_Take_2_2_materialInfo1.sg"
		;
connectAttr "Jessica_Take_2_2_business05_f_highpolyPhong.msg" "Jessica_Take_2_2_materialInfo1.m"
		;
connectAttr "Jessica_Take_2_2_Map1.msg" "Jessica_Take_2_2_materialInfo1.t" -na;
connectAttr "Jessica_Take_2_2_business05_f_highpolyPhong.oc" "Jessica_Take_2_2_business05_f_highpolySG1.ss"
		;
connectAttr "Jessica_Take_2_2_business05_f_highpolySG1.msg" "Jessica_Take_2_2_materialInfo2.sg"
		;
connectAttr "Jessica_Take_2_2_business05_f_highpolyPhong.msg" "Jessica_Take_2_2_materialInfo2.m"
		;
connectAttr "Jessica_Take_2_2_Map1.msg" "Jessica_Take_2_2_materialInfo2.t" -na;
connectAttr "Jessica_Take_2_2_place2dTexture1.o" "Jessica_Take_2_2_Map1.uv";
connectAttr "Jessica_Take_2_2_place2dTexture1.ofu" "Jessica_Take_2_2_Map1.ofu";
connectAttr "Jessica_Take_2_2_place2dTexture1.ofv" "Jessica_Take_2_2_Map1.ofv";
connectAttr "Jessica_Take_2_2_place2dTexture1.rf" "Jessica_Take_2_2_Map1.rf";
connectAttr "Jessica_Take_2_2_place2dTexture1.reu" "Jessica_Take_2_2_Map1.reu";
connectAttr "Jessica_Take_2_2_place2dTexture1.rev" "Jessica_Take_2_2_Map1.rev";
connectAttr "Jessica_Take_2_2_place2dTexture1.vt1" "Jessica_Take_2_2_Map1.vt1";
connectAttr "Jessica_Take_2_2_place2dTexture1.vt2" "Jessica_Take_2_2_Map1.vt2";
connectAttr "Jessica_Take_2_2_place2dTexture1.vt3" "Jessica_Take_2_2_Map1.vt3";
connectAttr "Jessica_Take_2_2_place2dTexture1.vc1" "Jessica_Take_2_2_Map1.vc1";
connectAttr "Jessica_Take_2_2_place2dTexture1.ofs" "Jessica_Take_2_2_Map1.fs";
connectAttr "Jessica_Take_2_2_business05_f_highpoly_hairPhong.oc" "Jessica_Take_2_2_business05_f_highpoly_hairSG.ss"
		;
connectAttr "Jessica_Take_2_2_business05_f_highpoly_hairSG.msg" "Jessica_Take_2_2_materialInfo3.sg"
		;
connectAttr "Jessica_Take_2_2_business05_f_highpoly_hairPhong.msg" "Jessica_Take_2_2_materialInfo3.m"
		;
connectAttr "Jessica_Take_2_2_business05_f_highpoly_hairPhong.oc" "Jessica_Take_2_2_business05_f_highpoly_hairSG1.ss"
		;
connectAttr "Jessica_Take_2_2_business05_f_highpoly_hairSG1.msg" "Jessica_Take_2_2_materialInfo4.sg"
		;
connectAttr "Jessica_Take_2_2_business05_f_highpoly_hairPhong.msg" "Jessica_Take_2_2_materialInfo4.m"
		;
connectAttr "Jessica_Take_2_2_Map2.oc" "Jessica_Take_2_2_business05_f_highpolyPhong1.c"
		;
connectAttr "Jessica_Take_2_2_business05_f_highpolyPhong1.oc" "Jessica_Take_2_2_business05_f_highpolySG2.ss"
		;
connectAttr "Jessica_Take_2_2_business05_f_highpolySG2.msg" "Jessica_Take_2_2_materialInfo5.sg"
		;
connectAttr "Jessica_Take_2_2_business05_f_highpolyPhong1.msg" "Jessica_Take_2_2_materialInfo5.m"
		;
connectAttr "Jessica_Take_2_2_Map2.msg" "Jessica_Take_2_2_materialInfo5.t" -na;
connectAttr "Jessica_Take_2_2_business05_f_highpolyPhong1.oc" "Jessica_Take_2_2_business05_f_highpolySG3.ss"
		;
connectAttr "Jessica_Take_2_2_business05_f_highpolySG3.msg" "Jessica_Take_2_2_materialInfo6.sg"
		;
connectAttr "Jessica_Take_2_2_business05_f_highpolyPhong1.msg" "Jessica_Take_2_2_materialInfo6.m"
		;
connectAttr "Jessica_Take_2_2_Map2.msg" "Jessica_Take_2_2_materialInfo6.t" -na;
connectAttr "Jessica_Take_2_2_place2dTexture2.o" "Jessica_Take_2_2_Map2.uv";
connectAttr "Jessica_Take_2_2_place2dTexture2.ofu" "Jessica_Take_2_2_Map2.ofu";
connectAttr "Jessica_Take_2_2_place2dTexture2.ofv" "Jessica_Take_2_2_Map2.ofv";
connectAttr "Jessica_Take_2_2_place2dTexture2.rf" "Jessica_Take_2_2_Map2.rf";
connectAttr "Jessica_Take_2_2_place2dTexture2.reu" "Jessica_Take_2_2_Map2.reu";
connectAttr "Jessica_Take_2_2_place2dTexture2.rev" "Jessica_Take_2_2_Map2.rev";
connectAttr "Jessica_Take_2_2_place2dTexture2.vt1" "Jessica_Take_2_2_Map2.vt1";
connectAttr "Jessica_Take_2_2_place2dTexture2.vt2" "Jessica_Take_2_2_Map2.vt2";
connectAttr "Jessica_Take_2_2_place2dTexture2.vt3" "Jessica_Take_2_2_Map2.vt3";
connectAttr "Jessica_Take_2_2_place2dTexture2.vc1" "Jessica_Take_2_2_Map2.vc1";
connectAttr "Jessica_Take_2_2_place2dTexture2.ofs" "Jessica_Take_2_2_Map2.fs";
connectAttr "Jessica_Take_2_2_business05_f_highpoly_hairPhong1.oc" "Jessica_Take_2_2_business05_f_highpoly_hairSG2.ss"
		;
connectAttr "Jessica_Take_2_2_business05_f_highpoly_hairSG2.msg" "Jessica_Take_2_2_materialInfo7.sg"
		;
connectAttr "Jessica_Take_2_2_business05_f_highpoly_hairPhong1.msg" "Jessica_Take_2_2_materialInfo7.m"
		;
connectAttr "Jessica_Take_2_2_business05_f_highpoly_hairPhong1.oc" "Jessica_Take_2_2_business05_f_highpoly_hairSG3.ss"
		;
connectAttr "Jessica_Take_2_2_business05_f_highpoly_hairSG3.msg" "Jessica_Take_2_2_materialInfo8.sg"
		;
connectAttr "Jessica_Take_2_2_business05_f_highpoly_hairPhong1.msg" "Jessica_Take_2_2_materialInfo8.m"
		;
connectAttr "Jessica_Take_2_2_Map3.oc" "Jessica_Take_2_2_business05_f_highpolyPhong2.c"
		;
connectAttr "Jessica_Take_2_2_business05_f_highpolyPhong2.oc" "Jessica_Take_2_2_business05_f_highpolySG4.ss"
		;
connectAttr "Jessica_Take_2_2_business05_f_highpolySG4.msg" "Jessica_Take_2_2_materialInfo9.sg"
		;
connectAttr "Jessica_Take_2_2_business05_f_highpolyPhong2.msg" "Jessica_Take_2_2_materialInfo9.m"
		;
connectAttr "Jessica_Take_2_2_Map3.msg" "Jessica_Take_2_2_materialInfo9.t" -na;
connectAttr "Jessica_Take_2_2_business05_f_highpolyPhong2.oc" "Jessica_Take_2_2_business05_f_highpolySG5.ss"
		;
connectAttr "Jessica_Take_2_2_business05_f_highpolySG5.msg" "Jessica_Take_2_2_materialInfo10.sg"
		;
connectAttr "Jessica_Take_2_2_business05_f_highpolyPhong2.msg" "Jessica_Take_2_2_materialInfo10.m"
		;
connectAttr "Jessica_Take_2_2_Map3.msg" "Jessica_Take_2_2_materialInfo10.t" -na;
connectAttr "Jessica_Take_2_2_place2dTexture3.o" "Jessica_Take_2_2_Map3.uv";
connectAttr "Jessica_Take_2_2_place2dTexture3.ofu" "Jessica_Take_2_2_Map3.ofu";
connectAttr "Jessica_Take_2_2_place2dTexture3.ofv" "Jessica_Take_2_2_Map3.ofv";
connectAttr "Jessica_Take_2_2_place2dTexture3.rf" "Jessica_Take_2_2_Map3.rf";
connectAttr "Jessica_Take_2_2_place2dTexture3.reu" "Jessica_Take_2_2_Map3.reu";
connectAttr "Jessica_Take_2_2_place2dTexture3.rev" "Jessica_Take_2_2_Map3.rev";
connectAttr "Jessica_Take_2_2_place2dTexture3.vt1" "Jessica_Take_2_2_Map3.vt1";
connectAttr "Jessica_Take_2_2_place2dTexture3.vt2" "Jessica_Take_2_2_Map3.vt2";
connectAttr "Jessica_Take_2_2_place2dTexture3.vt3" "Jessica_Take_2_2_Map3.vt3";
connectAttr "Jessica_Take_2_2_place2dTexture3.vc1" "Jessica_Take_2_2_Map3.vc1";
connectAttr "Jessica_Take_2_2_place2dTexture3.ofs" "Jessica_Take_2_2_Map3.fs";
connectAttr "Jessica_Take_2_2_business05_f_highpoly_hairPhong2.oc" "Jessica_Take_2_2_business05_f_highpoly_hairSG4.ss"
		;
connectAttr "Jessica_Take_2_2_business05_f_highpoly_hairSG4.msg" "Jessica_Take_2_2_materialInfo11.sg"
		;
connectAttr "Jessica_Take_2_2_business05_f_highpoly_hairPhong2.msg" "Jessica_Take_2_2_materialInfo11.m"
		;
connectAttr "Jessica_Take_2_2_business05_f_highpoly_hairPhong2.oc" "Jessica_Take_2_2_business05_f_highpoly_hairSG5.ss"
		;
connectAttr "Jessica_Take_2_2_business05_f_highpoly_hairSG5.msg" "Jessica_Take_2_2_materialInfo12.sg"
		;
connectAttr "Jessica_Take_2_2_business05_f_highpoly_hairPhong2.msg" "Jessica_Take_2_2_materialInfo12.m"
		;
connectAttr "Jessica_Take_2_2_Map4.oc" "Jessica_Take_2_2_business05_f_highpolyPhong3.c"
		;
connectAttr "Jessica_Take_2_2_business05_f_highpolyPhong3.oc" "Jessica_Take_2_2_business05_f_highpolySG6.ss"
		;
connectAttr "Jessica_Take_2_2_business05_f_highpolySG6.msg" "Jessica_Take_2_2_materialInfo13.sg"
		;
connectAttr "Jessica_Take_2_2_business05_f_highpolyPhong3.msg" "Jessica_Take_2_2_materialInfo13.m"
		;
connectAttr "Jessica_Take_2_2_Map4.msg" "Jessica_Take_2_2_materialInfo13.t" -na;
connectAttr "Jessica_Take_2_2_business05_f_highpolyPhong3.oc" "Jessica_Take_2_2_business05_f_highpolySG7.ss"
		;
connectAttr "Jessica_Take_2_2_business05_f_highpolySG7.msg" "Jessica_Take_2_2_materialInfo14.sg"
		;
connectAttr "Jessica_Take_2_2_business05_f_highpolyPhong3.msg" "Jessica_Take_2_2_materialInfo14.m"
		;
connectAttr "Jessica_Take_2_2_Map4.msg" "Jessica_Take_2_2_materialInfo14.t" -na;
connectAttr "Jessica_Take_2_2_place2dTexture4.o" "Jessica_Take_2_2_Map4.uv";
connectAttr "Jessica_Take_2_2_place2dTexture4.ofu" "Jessica_Take_2_2_Map4.ofu";
connectAttr "Jessica_Take_2_2_place2dTexture4.ofv" "Jessica_Take_2_2_Map4.ofv";
connectAttr "Jessica_Take_2_2_place2dTexture4.rf" "Jessica_Take_2_2_Map4.rf";
connectAttr "Jessica_Take_2_2_place2dTexture4.reu" "Jessica_Take_2_2_Map4.reu";
connectAttr "Jessica_Take_2_2_place2dTexture4.rev" "Jessica_Take_2_2_Map4.rev";
connectAttr "Jessica_Take_2_2_place2dTexture4.vt1" "Jessica_Take_2_2_Map4.vt1";
connectAttr "Jessica_Take_2_2_place2dTexture4.vt2" "Jessica_Take_2_2_Map4.vt2";
connectAttr "Jessica_Take_2_2_place2dTexture4.vt3" "Jessica_Take_2_2_Map4.vt3";
connectAttr "Jessica_Take_2_2_place2dTexture4.vc1" "Jessica_Take_2_2_Map4.vc1";
connectAttr "Jessica_Take_2_2_place2dTexture4.ofs" "Jessica_Take_2_2_Map4.fs";
connectAttr "Jessica_Take_2_2_business05_f_highpoly_hairPhong3.oc" "Jessica_Take_2_2_business05_f_highpoly_hairSG6.ss"
		;
connectAttr "Jessica_Take_2_2_business05_f_highpoly_hairSG6.msg" "Jessica_Take_2_2_materialInfo15.sg"
		;
connectAttr "Jessica_Take_2_2_business05_f_highpoly_hairPhong3.msg" "Jessica_Take_2_2_materialInfo15.m"
		;
connectAttr "Jessica_Take_2_2_business05_f_highpoly_hairPhong3.oc" "Jessica_Take_2_2_business05_f_highpoly_hairSG7.ss"
		;
connectAttr "Jessica_Take_2_2_business05_f_highpoly_hairSG7.msg" "Jessica_Take_2_2_materialInfo16.sg"
		;
connectAttr "Jessica_Take_2_2_business05_f_highpoly_hairPhong3.msg" "Jessica_Take_2_2_materialInfo16.m"
		;
connectAttr "Jessica_Take_2_2_Map5.oc" "Jessica_Take_2_2_business05_f_highpolyPhong4.c"
		;
connectAttr "Jessica_Take_2_2_business05_f_highpolyPhong4.oc" "Jessica_Take_2_2_business05_f_highpolySG8.ss"
		;
connectAttr "Jessica_Take_2_2_business05_f_highpolySG8.msg" "Jessica_Take_2_2_materialInfo17.sg"
		;
connectAttr "Jessica_Take_2_2_business05_f_highpolyPhong4.msg" "Jessica_Take_2_2_materialInfo17.m"
		;
connectAttr "Jessica_Take_2_2_Map5.msg" "Jessica_Take_2_2_materialInfo17.t" -na;
connectAttr "Jessica_Take_2_2_business05_f_highpolyPhong4.oc" "Jessica_Take_2_2_business05_f_highpolySG9.ss"
		;
connectAttr "Jessica_Take_2_2_business05_f_highpolySG9.msg" "Jessica_Take_2_2_materialInfo18.sg"
		;
connectAttr "Jessica_Take_2_2_business05_f_highpolyPhong4.msg" "Jessica_Take_2_2_materialInfo18.m"
		;
connectAttr "Jessica_Take_2_2_Map5.msg" "Jessica_Take_2_2_materialInfo18.t" -na;
connectAttr "Jessica_Take_2_2_place2dTexture5.o" "Jessica_Take_2_2_Map5.uv";
connectAttr "Jessica_Take_2_2_place2dTexture5.ofu" "Jessica_Take_2_2_Map5.ofu";
connectAttr "Jessica_Take_2_2_place2dTexture5.ofv" "Jessica_Take_2_2_Map5.ofv";
connectAttr "Jessica_Take_2_2_place2dTexture5.rf" "Jessica_Take_2_2_Map5.rf";
connectAttr "Jessica_Take_2_2_place2dTexture5.reu" "Jessica_Take_2_2_Map5.reu";
connectAttr "Jessica_Take_2_2_place2dTexture5.rev" "Jessica_Take_2_2_Map5.rev";
connectAttr "Jessica_Take_2_2_place2dTexture5.vt1" "Jessica_Take_2_2_Map5.vt1";
connectAttr "Jessica_Take_2_2_place2dTexture5.vt2" "Jessica_Take_2_2_Map5.vt2";
connectAttr "Jessica_Take_2_2_place2dTexture5.vt3" "Jessica_Take_2_2_Map5.vt3";
connectAttr "Jessica_Take_2_2_place2dTexture5.vc1" "Jessica_Take_2_2_Map5.vc1";
connectAttr "Jessica_Take_2_2_place2dTexture5.ofs" "Jessica_Take_2_2_Map5.fs";
connectAttr "Jessica_Take_2_2_business05_f_highpoly_hairPhong4.oc" "Jessica_Take_2_2_business05_f_highpoly_hairSG8.ss"
		;
connectAttr "Jessica_Take_2_2_business05_f_highpoly_hairSG8.msg" "Jessica_Take_2_2_materialInfo19.sg"
		;
connectAttr "Jessica_Take_2_2_business05_f_highpoly_hairPhong4.msg" "Jessica_Take_2_2_materialInfo19.m"
		;
connectAttr "Jessica_Take_2_2_business05_f_highpoly_hairPhong4.oc" "Jessica_Take_2_2_business05_f_highpoly_hairSG9.ss"
		;
connectAttr "Jessica_Take_2_2_business05_f_highpoly_hairSG9.msg" "Jessica_Take_2_2_materialInfo20.sg"
		;
connectAttr "Jessica_Take_2_2_business05_f_highpoly_hairPhong4.msg" "Jessica_Take_2_2_materialInfo20.m"
		;
connectAttr "Jessica_Take_2_2_Map6.oc" "Jessica_Take_2_2_business05_f_highpolyPhong5.c"
		;
connectAttr "Jessica_Take_2_2_business05_f_highpolyPhong5.oc" "Jessica_Take_2_2_business05_f_highpolySG10.ss"
		;
connectAttr "Jessica_Take_2_2_business05_f_highpolySG10.msg" "Jessica_Take_2_2_materialInfo21.sg"
		;
connectAttr "Jessica_Take_2_2_business05_f_highpolyPhong5.msg" "Jessica_Take_2_2_materialInfo21.m"
		;
connectAttr "Jessica_Take_2_2_Map6.msg" "Jessica_Take_2_2_materialInfo21.t" -na;
connectAttr "Jessica_Take_2_2_business05_f_highpolyPhong5.oc" "Jessica_Take_2_2_business05_f_highpolySG11.ss"
		;
connectAttr "Jessica_Take_2_2_business05_f_highpolySG11.msg" "Jessica_Take_2_2_materialInfo22.sg"
		;
connectAttr "Jessica_Take_2_2_business05_f_highpolyPhong5.msg" "Jessica_Take_2_2_materialInfo22.m"
		;
connectAttr "Jessica_Take_2_2_Map6.msg" "Jessica_Take_2_2_materialInfo22.t" -na;
connectAttr "Jessica_Take_2_2_place2dTexture6.o" "Jessica_Take_2_2_Map6.uv";
connectAttr "Jessica_Take_2_2_place2dTexture6.ofu" "Jessica_Take_2_2_Map6.ofu";
connectAttr "Jessica_Take_2_2_place2dTexture6.ofv" "Jessica_Take_2_2_Map6.ofv";
connectAttr "Jessica_Take_2_2_place2dTexture6.rf" "Jessica_Take_2_2_Map6.rf";
connectAttr "Jessica_Take_2_2_place2dTexture6.reu" "Jessica_Take_2_2_Map6.reu";
connectAttr "Jessica_Take_2_2_place2dTexture6.rev" "Jessica_Take_2_2_Map6.rev";
connectAttr "Jessica_Take_2_2_place2dTexture6.vt1" "Jessica_Take_2_2_Map6.vt1";
connectAttr "Jessica_Take_2_2_place2dTexture6.vt2" "Jessica_Take_2_2_Map6.vt2";
connectAttr "Jessica_Take_2_2_place2dTexture6.vt3" "Jessica_Take_2_2_Map6.vt3";
connectAttr "Jessica_Take_2_2_place2dTexture6.vc1" "Jessica_Take_2_2_Map6.vc1";
connectAttr "Jessica_Take_2_2_place2dTexture6.ofs" "Jessica_Take_2_2_Map6.fs";
connectAttr "Jessica_Take_2_2_business05_f_highpoly_hairPhong5.oc" "Jessica_Take_2_2_business05_f_highpoly_hairSG10.ss"
		;
connectAttr "Jessica_Take_2_2_business05_f_highpoly_hairSG10.msg" "Jessica_Take_2_2_materialInfo23.sg"
		;
connectAttr "Jessica_Take_2_2_business05_f_highpoly_hairPhong5.msg" "Jessica_Take_2_2_materialInfo23.m"
		;
connectAttr "Jessica_Take_2_2_business05_f_highpoly_hairPhong5.oc" "Jessica_Take_2_2_business05_f_highpoly_hairSG11.ss"
		;
connectAttr "Jessica_Take_2_2_business05_f_highpoly_hairSG11.msg" "Jessica_Take_2_2_materialInfo24.sg"
		;
connectAttr "Jessica_Take_2_2_business05_f_highpoly_hairPhong5.msg" "Jessica_Take_2_2_materialInfo24.m"
		;
connectAttr "Candice_Take_1_Map1.oc" "Candice_Take_1_business05_f_highpolyPhong.c"
		;
connectAttr "Candice_Take_1_business05_f_highpolyPhong.oc" "Candice_Take_1_business05_f_highpolySG.ss"
		;
connectAttr "Candice_Take_1_business05_f_highpolySG.msg" "Candice_Take_1_materialInfo1.sg"
		;
connectAttr "Candice_Take_1_business05_f_highpolyPhong.msg" "Candice_Take_1_materialInfo1.m"
		;
connectAttr "Candice_Take_1_Map1.msg" "Candice_Take_1_materialInfo1.t" -na;
connectAttr "Candice_Take_1_business05_f_highpolyPhong.oc" "Candice_Take_1_business05_f_highpolySG1.ss"
		;
connectAttr "Candice_Take_1_business05_f_highpolySG1.msg" "Candice_Take_1_materialInfo2.sg"
		;
connectAttr "Candice_Take_1_business05_f_highpolyPhong.msg" "Candice_Take_1_materialInfo2.m"
		;
connectAttr "Candice_Take_1_Map1.msg" "Candice_Take_1_materialInfo2.t" -na;
connectAttr "Candice_Take_1_place2dTexture1.o" "Candice_Take_1_Map1.uv";
connectAttr "Candice_Take_1_place2dTexture1.ofu" "Candice_Take_1_Map1.ofu";
connectAttr "Candice_Take_1_place2dTexture1.ofv" "Candice_Take_1_Map1.ofv";
connectAttr "Candice_Take_1_place2dTexture1.rf" "Candice_Take_1_Map1.rf";
connectAttr "Candice_Take_1_place2dTexture1.reu" "Candice_Take_1_Map1.reu";
connectAttr "Candice_Take_1_place2dTexture1.rev" "Candice_Take_1_Map1.rev";
connectAttr "Candice_Take_1_place2dTexture1.vt1" "Candice_Take_1_Map1.vt1";
connectAttr "Candice_Take_1_place2dTexture1.vt2" "Candice_Take_1_Map1.vt2";
connectAttr "Candice_Take_1_place2dTexture1.vt3" "Candice_Take_1_Map1.vt3";
connectAttr "Candice_Take_1_place2dTexture1.vc1" "Candice_Take_1_Map1.vc1";
connectAttr "Candice_Take_1_place2dTexture1.ofs" "Candice_Take_1_Map1.fs";
connectAttr "Candice_Take_1_business05_f_highpoly_hairPhong.oc" "Candice_Take_1_business05_f_highpoly_hairSG.ss"
		;
connectAttr "Candice_Take_1_business05_f_highpoly_hairSG.msg" "Candice_Take_1_materialInfo3.sg"
		;
connectAttr "Candice_Take_1_business05_f_highpoly_hairPhong.msg" "Candice_Take_1_materialInfo3.m"
		;
connectAttr "Candice_Take_1_business05_f_highpoly_hairPhong.oc" "Candice_Take_1_business05_f_highpoly_hairSG1.ss"
		;
connectAttr "Candice_Take_1_business05_f_highpoly_hairSG1.msg" "Candice_Take_1_materialInfo4.sg"
		;
connectAttr "Candice_Take_1_business05_f_highpoly_hairPhong.msg" "Candice_Take_1_materialInfo4.m"
		;
connectAttr "Candice_Take_1_Map2.oc" "Candice_Take_1_business05_f_highpolyPhong1.c"
		;
connectAttr "Candice_Take_1_business05_f_highpolyPhong1.oc" "Candice_Take_1_business05_f_highpolySG2.ss"
		;
connectAttr "Candice_Take_1_business05_f_highpolySG2.msg" "Candice_Take_1_materialInfo5.sg"
		;
connectAttr "Candice_Take_1_business05_f_highpolyPhong1.msg" "Candice_Take_1_materialInfo5.m"
		;
connectAttr "Candice_Take_1_Map2.msg" "Candice_Take_1_materialInfo5.t" -na;
connectAttr "Candice_Take_1_business05_f_highpolyPhong1.oc" "Candice_Take_1_business05_f_highpolySG3.ss"
		;
connectAttr "Candice_Take_1_business05_f_highpolySG3.msg" "Candice_Take_1_materialInfo6.sg"
		;
connectAttr "Candice_Take_1_business05_f_highpolyPhong1.msg" "Candice_Take_1_materialInfo6.m"
		;
connectAttr "Candice_Take_1_Map2.msg" "Candice_Take_1_materialInfo6.t" -na;
connectAttr "Candice_Take_1_place2dTexture2.o" "Candice_Take_1_Map2.uv";
connectAttr "Candice_Take_1_place2dTexture2.ofu" "Candice_Take_1_Map2.ofu";
connectAttr "Candice_Take_1_place2dTexture2.ofv" "Candice_Take_1_Map2.ofv";
connectAttr "Candice_Take_1_place2dTexture2.rf" "Candice_Take_1_Map2.rf";
connectAttr "Candice_Take_1_place2dTexture2.reu" "Candice_Take_1_Map2.reu";
connectAttr "Candice_Take_1_place2dTexture2.rev" "Candice_Take_1_Map2.rev";
connectAttr "Candice_Take_1_place2dTexture2.vt1" "Candice_Take_1_Map2.vt1";
connectAttr "Candice_Take_1_place2dTexture2.vt2" "Candice_Take_1_Map2.vt2";
connectAttr "Candice_Take_1_place2dTexture2.vt3" "Candice_Take_1_Map2.vt3";
connectAttr "Candice_Take_1_place2dTexture2.vc1" "Candice_Take_1_Map2.vc1";
connectAttr "Candice_Take_1_place2dTexture2.ofs" "Candice_Take_1_Map2.fs";
connectAttr "Candice_Take_1_business05_f_highpoly_hairPhong1.oc" "Candice_Take_1_business05_f_highpoly_hairSG2.ss"
		;
connectAttr "Candice_Take_1_business05_f_highpoly_hairSG2.msg" "Candice_Take_1_materialInfo7.sg"
		;
connectAttr "Candice_Take_1_business05_f_highpoly_hairPhong1.msg" "Candice_Take_1_materialInfo7.m"
		;
connectAttr "Candice_Take_1_business05_f_highpoly_hairPhong1.oc" "Candice_Take_1_business05_f_highpoly_hairSG3.ss"
		;
connectAttr "Candice_Take_1_business05_f_highpoly_hairSG3.msg" "Candice_Take_1_materialInfo8.sg"
		;
connectAttr "Candice_Take_1_business05_f_highpoly_hairPhong1.msg" "Candice_Take_1_materialInfo8.m"
		;
connectAttr "Candice_Take_1_Map3.oc" "Candice_Take_1_business05_f_highpolyPhong2.c"
		;
connectAttr "Candice_Take_1_business05_f_highpolyPhong2.oc" "Candice_Take_1_business05_f_highpolySG4.ss"
		;
connectAttr "Candice_Take_1_business05_f_highpolySG4.msg" "Candice_Take_1_materialInfo9.sg"
		;
connectAttr "Candice_Take_1_business05_f_highpolyPhong2.msg" "Candice_Take_1_materialInfo9.m"
		;
connectAttr "Candice_Take_1_Map3.msg" "Candice_Take_1_materialInfo9.t" -na;
connectAttr "Candice_Take_1_business05_f_highpolyPhong2.oc" "Candice_Take_1_business05_f_highpolySG5.ss"
		;
connectAttr "Candice_Take_1_business05_f_highpolySG5.msg" "Candice_Take_1_materialInfo10.sg"
		;
connectAttr "Candice_Take_1_business05_f_highpolyPhong2.msg" "Candice_Take_1_materialInfo10.m"
		;
connectAttr "Candice_Take_1_Map3.msg" "Candice_Take_1_materialInfo10.t" -na;
connectAttr "Candice_Take_1_place2dTexture3.o" "Candice_Take_1_Map3.uv";
connectAttr "Candice_Take_1_place2dTexture3.ofu" "Candice_Take_1_Map3.ofu";
connectAttr "Candice_Take_1_place2dTexture3.ofv" "Candice_Take_1_Map3.ofv";
connectAttr "Candice_Take_1_place2dTexture3.rf" "Candice_Take_1_Map3.rf";
connectAttr "Candice_Take_1_place2dTexture3.reu" "Candice_Take_1_Map3.reu";
connectAttr "Candice_Take_1_place2dTexture3.rev" "Candice_Take_1_Map3.rev";
connectAttr "Candice_Take_1_place2dTexture3.vt1" "Candice_Take_1_Map3.vt1";
connectAttr "Candice_Take_1_place2dTexture3.vt2" "Candice_Take_1_Map3.vt2";
connectAttr "Candice_Take_1_place2dTexture3.vt3" "Candice_Take_1_Map3.vt3";
connectAttr "Candice_Take_1_place2dTexture3.vc1" "Candice_Take_1_Map3.vc1";
connectAttr "Candice_Take_1_place2dTexture3.ofs" "Candice_Take_1_Map3.fs";
connectAttr "Candice_Take_1_business05_f_highpoly_hairPhong2.oc" "Candice_Take_1_business05_f_highpoly_hairSG4.ss"
		;
connectAttr "Candice_Take_1_business05_f_highpoly_hairSG4.msg" "Candice_Take_1_materialInfo11.sg"
		;
connectAttr "Candice_Take_1_business05_f_highpoly_hairPhong2.msg" "Candice_Take_1_materialInfo11.m"
		;
connectAttr "Candice_Take_1_business05_f_highpoly_hairPhong2.oc" "Candice_Take_1_business05_f_highpoly_hairSG5.ss"
		;
connectAttr "Candice_Take_1_business05_f_highpoly_hairSG5.msg" "Candice_Take_1_materialInfo12.sg"
		;
connectAttr "Candice_Take_1_business05_f_highpoly_hairPhong2.msg" "Candice_Take_1_materialInfo12.m"
		;
connectAttr "Candice_Take_1_Map4.oc" "Candice_Take_1_business05_f_highpolyPhong3.c"
		;
connectAttr "Candice_Take_1_business05_f_highpolyPhong3.oc" "Candice_Take_1_business05_f_highpolySG6.ss"
		;
connectAttr "Candice_Take_1_business05_f_highpolySG6.msg" "Candice_Take_1_materialInfo13.sg"
		;
connectAttr "Candice_Take_1_business05_f_highpolyPhong3.msg" "Candice_Take_1_materialInfo13.m"
		;
connectAttr "Candice_Take_1_Map4.msg" "Candice_Take_1_materialInfo13.t" -na;
connectAttr "Candice_Take_1_business05_f_highpolyPhong3.oc" "Candice_Take_1_business05_f_highpolySG7.ss"
		;
connectAttr "Candice_Take_1_business05_f_highpolySG7.msg" "Candice_Take_1_materialInfo14.sg"
		;
connectAttr "Candice_Take_1_business05_f_highpolyPhong3.msg" "Candice_Take_1_materialInfo14.m"
		;
connectAttr "Candice_Take_1_Map4.msg" "Candice_Take_1_materialInfo14.t" -na;
connectAttr "Candice_Take_1_place2dTexture4.o" "Candice_Take_1_Map4.uv";
connectAttr "Candice_Take_1_place2dTexture4.ofu" "Candice_Take_1_Map4.ofu";
connectAttr "Candice_Take_1_place2dTexture4.ofv" "Candice_Take_1_Map4.ofv";
connectAttr "Candice_Take_1_place2dTexture4.rf" "Candice_Take_1_Map4.rf";
connectAttr "Candice_Take_1_place2dTexture4.reu" "Candice_Take_1_Map4.reu";
connectAttr "Candice_Take_1_place2dTexture4.rev" "Candice_Take_1_Map4.rev";
connectAttr "Candice_Take_1_place2dTexture4.vt1" "Candice_Take_1_Map4.vt1";
connectAttr "Candice_Take_1_place2dTexture4.vt2" "Candice_Take_1_Map4.vt2";
connectAttr "Candice_Take_1_place2dTexture4.vt3" "Candice_Take_1_Map4.vt3";
connectAttr "Candice_Take_1_place2dTexture4.vc1" "Candice_Take_1_Map4.vc1";
connectAttr "Candice_Take_1_place2dTexture4.ofs" "Candice_Take_1_Map4.fs";
connectAttr "Candice_Take_1_business05_f_highpoly_hairPhong3.oc" "Candice_Take_1_business05_f_highpoly_hairSG6.ss"
		;
connectAttr "Candice_Take_1_business05_f_highpoly_hairSG6.msg" "Candice_Take_1_materialInfo15.sg"
		;
connectAttr "Candice_Take_1_business05_f_highpoly_hairPhong3.msg" "Candice_Take_1_materialInfo15.m"
		;
connectAttr "Candice_Take_1_business05_f_highpoly_hairPhong3.oc" "Candice_Take_1_business05_f_highpoly_hairSG7.ss"
		;
connectAttr "Candice_Take_1_business05_f_highpoly_hairSG7.msg" "Candice_Take_1_materialInfo16.sg"
		;
connectAttr "Candice_Take_1_business05_f_highpoly_hairPhong3.msg" "Candice_Take_1_materialInfo16.m"
		;
connectAttr "Candice_Take_1_Map5.oc" "Candice_Take_1_business05_f_highpolyPhong4.c"
		;
connectAttr "Candice_Take_1_business05_f_highpolyPhong4.oc" "Candice_Take_1_business05_f_highpolySG8.ss"
		;
connectAttr "Candice_Take_1_business05_f_highpolySG8.msg" "Candice_Take_1_materialInfo17.sg"
		;
connectAttr "Candice_Take_1_business05_f_highpolyPhong4.msg" "Candice_Take_1_materialInfo17.m"
		;
connectAttr "Candice_Take_1_Map5.msg" "Candice_Take_1_materialInfo17.t" -na;
connectAttr "Candice_Take_1_business05_f_highpolyPhong4.oc" "Candice_Take_1_business05_f_highpolySG9.ss"
		;
connectAttr "Candice_Take_1_business05_f_highpolySG9.msg" "Candice_Take_1_materialInfo18.sg"
		;
connectAttr "Candice_Take_1_business05_f_highpolyPhong4.msg" "Candice_Take_1_materialInfo18.m"
		;
connectAttr "Candice_Take_1_Map5.msg" "Candice_Take_1_materialInfo18.t" -na;
connectAttr "Candice_Take_1_place2dTexture5.o" "Candice_Take_1_Map5.uv";
connectAttr "Candice_Take_1_place2dTexture5.ofu" "Candice_Take_1_Map5.ofu";
connectAttr "Candice_Take_1_place2dTexture5.ofv" "Candice_Take_1_Map5.ofv";
connectAttr "Candice_Take_1_place2dTexture5.rf" "Candice_Take_1_Map5.rf";
connectAttr "Candice_Take_1_place2dTexture5.reu" "Candice_Take_1_Map5.reu";
connectAttr "Candice_Take_1_place2dTexture5.rev" "Candice_Take_1_Map5.rev";
connectAttr "Candice_Take_1_place2dTexture5.vt1" "Candice_Take_1_Map5.vt1";
connectAttr "Candice_Take_1_place2dTexture5.vt2" "Candice_Take_1_Map5.vt2";
connectAttr "Candice_Take_1_place2dTexture5.vt3" "Candice_Take_1_Map5.vt3";
connectAttr "Candice_Take_1_place2dTexture5.vc1" "Candice_Take_1_Map5.vc1";
connectAttr "Candice_Take_1_place2dTexture5.ofs" "Candice_Take_1_Map5.fs";
connectAttr "Candice_Take_1_business05_f_highpoly_hairPhong4.oc" "Candice_Take_1_business05_f_highpoly_hairSG8.ss"
		;
connectAttr "Candice_Take_1_business05_f_highpoly_hairSG8.msg" "Candice_Take_1_materialInfo19.sg"
		;
connectAttr "Candice_Take_1_business05_f_highpoly_hairPhong4.msg" "Candice_Take_1_materialInfo19.m"
		;
connectAttr "Candice_Take_1_business05_f_highpoly_hairPhong4.oc" "Candice_Take_1_business05_f_highpoly_hairSG9.ss"
		;
connectAttr "Candice_Take_1_business05_f_highpoly_hairSG9.msg" "Candice_Take_1_materialInfo20.sg"
		;
connectAttr "Candice_Take_1_business05_f_highpoly_hairPhong4.msg" "Candice_Take_1_materialInfo20.m"
		;
connectAttr "Candice_Take_1_Map6.oc" "Candice_Take_1_business05_f_highpolyPhong5.c"
		;
connectAttr "Candice_Take_1_business05_f_highpolyPhong5.oc" "Candice_Take_1_business05_f_highpolySG10.ss"
		;
connectAttr "Candice_Take_1_business05_f_highpolySG10.msg" "Candice_Take_1_materialInfo21.sg"
		;
connectAttr "Candice_Take_1_business05_f_highpolyPhong5.msg" "Candice_Take_1_materialInfo21.m"
		;
connectAttr "Candice_Take_1_Map6.msg" "Candice_Take_1_materialInfo21.t" -na;
connectAttr "Candice_Take_1_business05_f_highpolyPhong5.oc" "Candice_Take_1_business05_f_highpolySG11.ss"
		;
connectAttr "Candice_Take_1_business05_f_highpolySG11.msg" "Candice_Take_1_materialInfo22.sg"
		;
connectAttr "Candice_Take_1_business05_f_highpolyPhong5.msg" "Candice_Take_1_materialInfo22.m"
		;
connectAttr "Candice_Take_1_Map6.msg" "Candice_Take_1_materialInfo22.t" -na;
connectAttr "Candice_Take_1_place2dTexture6.o" "Candice_Take_1_Map6.uv";
connectAttr "Candice_Take_1_place2dTexture6.ofu" "Candice_Take_1_Map6.ofu";
connectAttr "Candice_Take_1_place2dTexture6.ofv" "Candice_Take_1_Map6.ofv";
connectAttr "Candice_Take_1_place2dTexture6.rf" "Candice_Take_1_Map6.rf";
connectAttr "Candice_Take_1_place2dTexture6.reu" "Candice_Take_1_Map6.reu";
connectAttr "Candice_Take_1_place2dTexture6.rev" "Candice_Take_1_Map6.rev";
connectAttr "Candice_Take_1_place2dTexture6.vt1" "Candice_Take_1_Map6.vt1";
connectAttr "Candice_Take_1_place2dTexture6.vt2" "Candice_Take_1_Map6.vt2";
connectAttr "Candice_Take_1_place2dTexture6.vt3" "Candice_Take_1_Map6.vt3";
connectAttr "Candice_Take_1_place2dTexture6.vc1" "Candice_Take_1_Map6.vc1";
connectAttr "Candice_Take_1_place2dTexture6.ofs" "Candice_Take_1_Map6.fs";
connectAttr "Candice_Take_1_business05_f_highpoly_hairPhong5.oc" "Candice_Take_1_business05_f_highpoly_hairSG10.ss"
		;
connectAttr "Candice_Take_1_business05_f_highpoly_hairSG10.msg" "Candice_Take_1_materialInfo23.sg"
		;
connectAttr "Candice_Take_1_business05_f_highpoly_hairPhong5.msg" "Candice_Take_1_materialInfo23.m"
		;
connectAttr "Candice_Take_1_business05_f_highpoly_hairPhong5.oc" "Candice_Take_1_business05_f_highpoly_hairSG11.ss"
		;
connectAttr "Candice_Take_1_business05_f_highpoly_hairSG11.msg" "Candice_Take_1_materialInfo24.sg"
		;
connectAttr "Candice_Take_1_business05_f_highpoly_hairPhong5.msg" "Candice_Take_1_materialInfo24.m"
		;
connectAttr "Candice_Take_1_Map7.oc" "Candice_Take_1_business05_f_highpolyPhong6.c"
		;
connectAttr "Candice_Take_1_business05_f_highpolyPhong6.oc" "Candice_Take_1_business05_f_highpolySG12.ss"
		;
connectAttr "Candice_Take_1_business05_f_highpolySG12.msg" "Candice_Take_1_materialInfo25.sg"
		;
connectAttr "Candice_Take_1_business05_f_highpolyPhong6.msg" "Candice_Take_1_materialInfo25.m"
		;
connectAttr "Candice_Take_1_Map7.msg" "Candice_Take_1_materialInfo25.t" -na;
connectAttr "Candice_Take_1_business05_f_highpolyPhong6.oc" "Candice_Take_1_business05_f_highpolySG13.ss"
		;
connectAttr "Candice_Take_1_business05_f_highpolySG13.msg" "Candice_Take_1_materialInfo26.sg"
		;
connectAttr "Candice_Take_1_business05_f_highpolyPhong6.msg" "Candice_Take_1_materialInfo26.m"
		;
connectAttr "Candice_Take_1_Map7.msg" "Candice_Take_1_materialInfo26.t" -na;
connectAttr "Candice_Take_1_place2dTexture7.o" "Candice_Take_1_Map7.uv";
connectAttr "Candice_Take_1_place2dTexture7.ofu" "Candice_Take_1_Map7.ofu";
connectAttr "Candice_Take_1_place2dTexture7.ofv" "Candice_Take_1_Map7.ofv";
connectAttr "Candice_Take_1_place2dTexture7.rf" "Candice_Take_1_Map7.rf";
connectAttr "Candice_Take_1_place2dTexture7.reu" "Candice_Take_1_Map7.reu";
connectAttr "Candice_Take_1_place2dTexture7.rev" "Candice_Take_1_Map7.rev";
connectAttr "Candice_Take_1_place2dTexture7.vt1" "Candice_Take_1_Map7.vt1";
connectAttr "Candice_Take_1_place2dTexture7.vt2" "Candice_Take_1_Map7.vt2";
connectAttr "Candice_Take_1_place2dTexture7.vt3" "Candice_Take_1_Map7.vt3";
connectAttr "Candice_Take_1_place2dTexture7.vc1" "Candice_Take_1_Map7.vc1";
connectAttr "Candice_Take_1_place2dTexture7.ofs" "Candice_Take_1_Map7.fs";
connectAttr "Candice_Take_1_business05_f_highpoly_hairPhong6.oc" "Candice_Take_1_business05_f_highpoly_hairSG12.ss"
		;
connectAttr "Candice_Take_1_business05_f_highpoly_hairSG12.msg" "Candice_Take_1_materialInfo27.sg"
		;
connectAttr "Candice_Take_1_business05_f_highpoly_hairPhong6.msg" "Candice_Take_1_materialInfo27.m"
		;
connectAttr "Candice_Take_1_business05_f_highpoly_hairPhong6.oc" "Candice_Take_1_business05_f_highpoly_hairSG13.ss"
		;
connectAttr "Candice_Take_1_business05_f_highpoly_hairSG13.msg" "Candice_Take_1_materialInfo28.sg"
		;
connectAttr "Candice_Take_1_business05_f_highpoly_hairPhong6.msg" "Candice_Take_1_materialInfo28.m"
		;
connectAttr "Candice_Take_1_Map8.oc" "Candice_Take_1_business05_f_highpolyPhong7.c"
		;
connectAttr "Candice_Take_1_business05_f_highpolyPhong7.oc" "Candice_Take_1_business05_f_highpolySG14.ss"
		;
connectAttr "Candice_Take_1_business05_f_highpolySG14.msg" "Candice_Take_1_materialInfo29.sg"
		;
connectAttr "Candice_Take_1_business05_f_highpolyPhong7.msg" "Candice_Take_1_materialInfo29.m"
		;
connectAttr "Candice_Take_1_Map8.msg" "Candice_Take_1_materialInfo29.t" -na;
connectAttr "Candice_Take_1_business05_f_highpolyPhong7.oc" "Candice_Take_1_business05_f_highpolySG15.ss"
		;
connectAttr "Candice_Take_1_business05_f_highpolySG15.msg" "Candice_Take_1_materialInfo30.sg"
		;
connectAttr "Candice_Take_1_business05_f_highpolyPhong7.msg" "Candice_Take_1_materialInfo30.m"
		;
connectAttr "Candice_Take_1_Map8.msg" "Candice_Take_1_materialInfo30.t" -na;
connectAttr "Candice_Take_1_place2dTexture8.o" "Candice_Take_1_Map8.uv";
connectAttr "Candice_Take_1_place2dTexture8.ofu" "Candice_Take_1_Map8.ofu";
connectAttr "Candice_Take_1_place2dTexture8.ofv" "Candice_Take_1_Map8.ofv";
connectAttr "Candice_Take_1_place2dTexture8.rf" "Candice_Take_1_Map8.rf";
connectAttr "Candice_Take_1_place2dTexture8.reu" "Candice_Take_1_Map8.reu";
connectAttr "Candice_Take_1_place2dTexture8.rev" "Candice_Take_1_Map8.rev";
connectAttr "Candice_Take_1_place2dTexture8.vt1" "Candice_Take_1_Map8.vt1";
connectAttr "Candice_Take_1_place2dTexture8.vt2" "Candice_Take_1_Map8.vt2";
connectAttr "Candice_Take_1_place2dTexture8.vt3" "Candice_Take_1_Map8.vt3";
connectAttr "Candice_Take_1_place2dTexture8.vc1" "Candice_Take_1_Map8.vc1";
connectAttr "Candice_Take_1_place2dTexture8.ofs" "Candice_Take_1_Map8.fs";
connectAttr "Candice_Take_1_business05_f_highpoly_hairPhong7.oc" "Candice_Take_1_business05_f_highpoly_hairSG14.ss"
		;
connectAttr "Candice_Take_1_business05_f_highpoly_hairSG14.msg" "Candice_Take_1_materialInfo31.sg"
		;
connectAttr "Candice_Take_1_business05_f_highpoly_hairPhong7.msg" "Candice_Take_1_materialInfo31.m"
		;
connectAttr "Candice_Take_1_business05_f_highpoly_hairPhong7.oc" "Candice_Take_1_business05_f_highpoly_hairSG15.ss"
		;
connectAttr "Candice_Take_1_business05_f_highpoly_hairSG15.msg" "Candice_Take_1_materialInfo32.sg"
		;
connectAttr "Candice_Take_1_business05_f_highpoly_hairPhong7.msg" "Candice_Take_1_materialInfo32.m"
		;
connectAttr "Animations_v3_Map2.oc" "Animations_v3_business05_f_highpolyPhong1.c"
		;
connectAttr "Animations_v3_business05_f_highpolyPhong1.oc" "Animations_v3_business05_f_highpolySG2.ss"
		;
connectAttr "Animations_v3_business05_f_highpolySG2.msg" "Animations_v3_materialInfo5.sg"
		;
connectAttr "Animations_v3_business05_f_highpolyPhong1.msg" "Animations_v3_materialInfo5.m"
		;
connectAttr "Animations_v3_Map2.msg" "Animations_v3_materialInfo5.t" -na;
connectAttr "Animations_v3_business05_f_highpolyPhong1.oc" "Animations_v3_business05_f_highpolySG3.ss"
		;
connectAttr "Animations_v3_business05_f_highpolySG3.msg" "Animations_v3_materialInfo6.sg"
		;
connectAttr "Animations_v3_business05_f_highpolyPhong1.msg" "Animations_v3_materialInfo6.m"
		;
connectAttr "Animations_v3_Map2.msg" "Animations_v3_materialInfo6.t" -na;
connectAttr "Animations_v3_place2dTexture2.o" "Animations_v3_Map2.uv";
connectAttr "Animations_v3_place2dTexture2.ofu" "Animations_v3_Map2.ofu";
connectAttr "Animations_v3_place2dTexture2.ofv" "Animations_v3_Map2.ofv";
connectAttr "Animations_v3_place2dTexture2.rf" "Animations_v3_Map2.rf";
connectAttr "Animations_v3_place2dTexture2.reu" "Animations_v3_Map2.reu";
connectAttr "Animations_v3_place2dTexture2.rev" "Animations_v3_Map2.rev";
connectAttr "Animations_v3_place2dTexture2.vt1" "Animations_v3_Map2.vt1";
connectAttr "Animations_v3_place2dTexture2.vt2" "Animations_v3_Map2.vt2";
connectAttr "Animations_v3_place2dTexture2.vt3" "Animations_v3_Map2.vt3";
connectAttr "Animations_v3_place2dTexture2.vc1" "Animations_v3_Map2.vc1";
connectAttr "Animations_v3_place2dTexture2.ofs" "Animations_v3_Map2.fs";
connectAttr "Animations_v3_business05_f_highpoly_hairPhong1.oc" "Animations_v3_business05_f_highpoly_hairSG2.ss"
		;
connectAttr "Animations_v3_business05_f_highpoly_hairSG2.msg" "Animations_v3_materialInfo7.sg"
		;
connectAttr "Animations_v3_business05_f_highpoly_hairPhong1.msg" "Animations_v3_materialInfo7.m"
		;
connectAttr "Animations_v3_business05_f_highpoly_hairPhong1.oc" "Animations_v3_business05_f_highpoly_hairSG3.ss"
		;
connectAttr "Animations_v3_business05_f_highpoly_hairSG3.msg" "Animations_v3_materialInfo8.sg"
		;
connectAttr "Animations_v3_business05_f_highpoly_hairPhong1.msg" "Animations_v3_materialInfo8.m"
		;
connectAttr "Kuan_take_1_Map1.oc" "Kuan_take_1_business05_f_highpolyPhong.c";
connectAttr "Kuan_take_1_business05_f_highpolyPhong.oc" "Kuan_take_1_business05_f_highpolySG.ss"
		;
connectAttr "Kuan_take_1_business05_f_highpolySG.msg" "Kuan_take_1_materialInfo1.sg"
		;
connectAttr "Kuan_take_1_business05_f_highpolyPhong.msg" "Kuan_take_1_materialInfo1.m"
		;
connectAttr "Kuan_take_1_Map1.msg" "Kuan_take_1_materialInfo1.t" -na;
connectAttr "Kuan_take_1_business05_f_highpolyPhong.oc" "Kuan_take_1_business05_f_highpolySG1.ss"
		;
connectAttr "Kuan_take_1_business05_f_highpolySG1.msg" "Kuan_take_1_materialInfo2.sg"
		;
connectAttr "Kuan_take_1_business05_f_highpolyPhong.msg" "Kuan_take_1_materialInfo2.m"
		;
connectAttr "Kuan_take_1_Map1.msg" "Kuan_take_1_materialInfo2.t" -na;
connectAttr "Kuan_take_1_place2dTexture1.o" "Kuan_take_1_Map1.uv";
connectAttr "Kuan_take_1_place2dTexture1.ofu" "Kuan_take_1_Map1.ofu";
connectAttr "Kuan_take_1_place2dTexture1.ofv" "Kuan_take_1_Map1.ofv";
connectAttr "Kuan_take_1_place2dTexture1.rf" "Kuan_take_1_Map1.rf";
connectAttr "Kuan_take_1_place2dTexture1.reu" "Kuan_take_1_Map1.reu";
connectAttr "Kuan_take_1_place2dTexture1.rev" "Kuan_take_1_Map1.rev";
connectAttr "Kuan_take_1_place2dTexture1.vt1" "Kuan_take_1_Map1.vt1";
connectAttr "Kuan_take_1_place2dTexture1.vt2" "Kuan_take_1_Map1.vt2";
connectAttr "Kuan_take_1_place2dTexture1.vt3" "Kuan_take_1_Map1.vt3";
connectAttr "Kuan_take_1_place2dTexture1.vc1" "Kuan_take_1_Map1.vc1";
connectAttr "Kuan_take_1_place2dTexture1.ofs" "Kuan_take_1_Map1.fs";
connectAttr "Kuan_take_1_business05_f_highpoly_hairPhong.oc" "Kuan_take_1_business05_f_highpoly_hairSG.ss"
		;
connectAttr "Kuan_take_1_business05_f_highpoly_hairSG.msg" "Kuan_take_1_materialInfo3.sg"
		;
connectAttr "Kuan_take_1_business05_f_highpoly_hairPhong.msg" "Kuan_take_1_materialInfo3.m"
		;
connectAttr "Kuan_take_1_business05_f_highpoly_hairPhong.oc" "Kuan_take_1_business05_f_highpoly_hairSG1.ss"
		;
connectAttr "Kuan_take_1_business05_f_highpoly_hairSG1.msg" "Kuan_take_1_materialInfo4.sg"
		;
connectAttr "Kuan_take_1_business05_f_highpoly_hairPhong.msg" "Kuan_take_1_materialInfo4.m"
		;
connectAttr "Kuan_take_1_Map2.oc" "Kuan_take_1_business05_f_highpolyPhong1.c";
connectAttr "Kuan_take_1_business05_f_highpolyPhong1.oc" "Kuan_take_1_business05_f_highpolySG2.ss"
		;
connectAttr "Kuan_take_1_business05_f_highpolySG2.msg" "Kuan_take_1_materialInfo5.sg"
		;
connectAttr "Kuan_take_1_business05_f_highpolyPhong1.msg" "Kuan_take_1_materialInfo5.m"
		;
connectAttr "Kuan_take_1_Map2.msg" "Kuan_take_1_materialInfo5.t" -na;
connectAttr "Kuan_take_1_business05_f_highpolyPhong1.oc" "Kuan_take_1_business05_f_highpolySG3.ss"
		;
connectAttr "Kuan_take_1_business05_f_highpolySG3.msg" "Kuan_take_1_materialInfo6.sg"
		;
connectAttr "Kuan_take_1_business05_f_highpolyPhong1.msg" "Kuan_take_1_materialInfo6.m"
		;
connectAttr "Kuan_take_1_Map2.msg" "Kuan_take_1_materialInfo6.t" -na;
connectAttr "Kuan_take_1_place2dTexture2.o" "Kuan_take_1_Map2.uv";
connectAttr "Kuan_take_1_place2dTexture2.ofu" "Kuan_take_1_Map2.ofu";
connectAttr "Kuan_take_1_place2dTexture2.ofv" "Kuan_take_1_Map2.ofv";
connectAttr "Kuan_take_1_place2dTexture2.rf" "Kuan_take_1_Map2.rf";
connectAttr "Kuan_take_1_place2dTexture2.reu" "Kuan_take_1_Map2.reu";
connectAttr "Kuan_take_1_place2dTexture2.rev" "Kuan_take_1_Map2.rev";
connectAttr "Kuan_take_1_place2dTexture2.vt1" "Kuan_take_1_Map2.vt1";
connectAttr "Kuan_take_1_place2dTexture2.vt2" "Kuan_take_1_Map2.vt2";
connectAttr "Kuan_take_1_place2dTexture2.vt3" "Kuan_take_1_Map2.vt3";
connectAttr "Kuan_take_1_place2dTexture2.vc1" "Kuan_take_1_Map2.vc1";
connectAttr "Kuan_take_1_place2dTexture2.ofs" "Kuan_take_1_Map2.fs";
connectAttr "Kuan_take_1_business05_f_highpoly_hairPhong1.oc" "Kuan_take_1_business05_f_highpoly_hairSG2.ss"
		;
connectAttr "Kuan_take_1_business05_f_highpoly_hairSG2.msg" "Kuan_take_1_materialInfo7.sg"
		;
connectAttr "Kuan_take_1_business05_f_highpoly_hairPhong1.msg" "Kuan_take_1_materialInfo7.m"
		;
connectAttr "Kuan_take_1_business05_f_highpoly_hairPhong1.oc" "Kuan_take_1_business05_f_highpoly_hairSG3.ss"
		;
connectAttr "Kuan_take_1_business05_f_highpoly_hairSG3.msg" "Kuan_take_1_materialInfo8.sg"
		;
connectAttr "Kuan_take_1_business05_f_highpoly_hairPhong1.msg" "Kuan_take_1_materialInfo8.m"
		;
connectAttr "Kuan_take_1_Map3.oc" "Kuan_take_1_business05_f_highpolyPhong2.c";
connectAttr "Kuan_take_1_business05_f_highpolyPhong2.oc" "Kuan_take_1_business05_f_highpolySG4.ss"
		;
connectAttr "Kuan_take_1_business05_f_highpolySG4.msg" "Kuan_take_1_materialInfo9.sg"
		;
connectAttr "Kuan_take_1_business05_f_highpolyPhong2.msg" "Kuan_take_1_materialInfo9.m"
		;
connectAttr "Kuan_take_1_Map3.msg" "Kuan_take_1_materialInfo9.t" -na;
connectAttr "Kuan_take_1_business05_f_highpolyPhong2.oc" "Kuan_take_1_business05_f_highpolySG5.ss"
		;
connectAttr "Kuan_take_1_business05_f_highpolySG5.msg" "Kuan_take_1_materialInfo10.sg"
		;
connectAttr "Kuan_take_1_business05_f_highpolyPhong2.msg" "Kuan_take_1_materialInfo10.m"
		;
connectAttr "Kuan_take_1_Map3.msg" "Kuan_take_1_materialInfo10.t" -na;
connectAttr "Kuan_take_1_place2dTexture3.o" "Kuan_take_1_Map3.uv";
connectAttr "Kuan_take_1_place2dTexture3.ofu" "Kuan_take_1_Map3.ofu";
connectAttr "Kuan_take_1_place2dTexture3.ofv" "Kuan_take_1_Map3.ofv";
connectAttr "Kuan_take_1_place2dTexture3.rf" "Kuan_take_1_Map3.rf";
connectAttr "Kuan_take_1_place2dTexture3.reu" "Kuan_take_1_Map3.reu";
connectAttr "Kuan_take_1_place2dTexture3.rev" "Kuan_take_1_Map3.rev";
connectAttr "Kuan_take_1_place2dTexture3.vt1" "Kuan_take_1_Map3.vt1";
connectAttr "Kuan_take_1_place2dTexture3.vt2" "Kuan_take_1_Map3.vt2";
connectAttr "Kuan_take_1_place2dTexture3.vt3" "Kuan_take_1_Map3.vt3";
connectAttr "Kuan_take_1_place2dTexture3.vc1" "Kuan_take_1_Map3.vc1";
connectAttr "Kuan_take_1_place2dTexture3.ofs" "Kuan_take_1_Map3.fs";
connectAttr "Kuan_take_1_business05_f_highpoly_hairPhong2.oc" "Kuan_take_1_business05_f_highpoly_hairSG4.ss"
		;
connectAttr "Kuan_take_1_business05_f_highpoly_hairSG4.msg" "Kuan_take_1_materialInfo11.sg"
		;
connectAttr "Kuan_take_1_business05_f_highpoly_hairPhong2.msg" "Kuan_take_1_materialInfo11.m"
		;
connectAttr "Kuan_take_1_business05_f_highpoly_hairPhong2.oc" "Kuan_take_1_business05_f_highpoly_hairSG5.ss"
		;
connectAttr "Kuan_take_1_business05_f_highpoly_hairSG5.msg" "Kuan_take_1_materialInfo12.sg"
		;
connectAttr "Kuan_take_1_business05_f_highpoly_hairPhong2.msg" "Kuan_take_1_materialInfo12.m"
		;
connectAttr "Kuan_take_1_Map4.oc" "Kuan_take_1_business05_f_highpolyPhong3.c";
connectAttr "Kuan_take_1_business05_f_highpolyPhong3.oc" "Kuan_take_1_business05_f_highpolySG6.ss"
		;
connectAttr "Kuan_take_1_business05_f_highpolySG6.msg" "Kuan_take_1_materialInfo13.sg"
		;
connectAttr "Kuan_take_1_business05_f_highpolyPhong3.msg" "Kuan_take_1_materialInfo13.m"
		;
connectAttr "Kuan_take_1_Map4.msg" "Kuan_take_1_materialInfo13.t" -na;
connectAttr "Kuan_take_1_business05_f_highpolyPhong3.oc" "Kuan_take_1_business05_f_highpolySG7.ss"
		;
connectAttr "Kuan_take_1_business05_f_highpolySG7.msg" "Kuan_take_1_materialInfo14.sg"
		;
connectAttr "Kuan_take_1_business05_f_highpolyPhong3.msg" "Kuan_take_1_materialInfo14.m"
		;
connectAttr "Kuan_take_1_Map4.msg" "Kuan_take_1_materialInfo14.t" -na;
connectAttr "Kuan_take_1_place2dTexture4.o" "Kuan_take_1_Map4.uv";
connectAttr "Kuan_take_1_place2dTexture4.ofu" "Kuan_take_1_Map4.ofu";
connectAttr "Kuan_take_1_place2dTexture4.ofv" "Kuan_take_1_Map4.ofv";
connectAttr "Kuan_take_1_place2dTexture4.rf" "Kuan_take_1_Map4.rf";
connectAttr "Kuan_take_1_place2dTexture4.reu" "Kuan_take_1_Map4.reu";
connectAttr "Kuan_take_1_place2dTexture4.rev" "Kuan_take_1_Map4.rev";
connectAttr "Kuan_take_1_place2dTexture4.vt1" "Kuan_take_1_Map4.vt1";
connectAttr "Kuan_take_1_place2dTexture4.vt2" "Kuan_take_1_Map4.vt2";
connectAttr "Kuan_take_1_place2dTexture4.vt3" "Kuan_take_1_Map4.vt3";
connectAttr "Kuan_take_1_place2dTexture4.vc1" "Kuan_take_1_Map4.vc1";
connectAttr "Kuan_take_1_place2dTexture4.ofs" "Kuan_take_1_Map4.fs";
connectAttr "Kuan_take_1_business05_f_highpoly_hairPhong3.oc" "Kuan_take_1_business05_f_highpoly_hairSG6.ss"
		;
connectAttr "Kuan_take_1_business05_f_highpoly_hairSG6.msg" "Kuan_take_1_materialInfo15.sg"
		;
connectAttr "Kuan_take_1_business05_f_highpoly_hairPhong3.msg" "Kuan_take_1_materialInfo15.m"
		;
connectAttr "Kuan_take_1_business05_f_highpoly_hairPhong3.oc" "Kuan_take_1_business05_f_highpoly_hairSG7.ss"
		;
connectAttr "Kuan_take_1_business05_f_highpoly_hairSG7.msg" "Kuan_take_1_materialInfo16.sg"
		;
connectAttr "Kuan_take_1_business05_f_highpoly_hairPhong3.msg" "Kuan_take_1_materialInfo16.m"
		;
connectAttr "Kuan_take_1_Map5.oc" "Kuan_take_1_business05_f_highpolyPhong4.c";
connectAttr "Kuan_take_1_business05_f_highpolyPhong4.oc" "Kuan_take_1_business05_f_highpolySG8.ss"
		;
connectAttr "Kuan_take_1_business05_f_highpolySG8.msg" "Kuan_take_1_materialInfo17.sg"
		;
connectAttr "Kuan_take_1_business05_f_highpolyPhong4.msg" "Kuan_take_1_materialInfo17.m"
		;
connectAttr "Kuan_take_1_Map5.msg" "Kuan_take_1_materialInfo17.t" -na;
connectAttr "Kuan_take_1_business05_f_highpolyPhong4.oc" "Kuan_take_1_business05_f_highpolySG9.ss"
		;
connectAttr "Kuan_take_1_business05_f_highpolySG9.msg" "Kuan_take_1_materialInfo18.sg"
		;
connectAttr "Kuan_take_1_business05_f_highpolyPhong4.msg" "Kuan_take_1_materialInfo18.m"
		;
connectAttr "Kuan_take_1_Map5.msg" "Kuan_take_1_materialInfo18.t" -na;
connectAttr "Kuan_take_1_place2dTexture5.o" "Kuan_take_1_Map5.uv";
connectAttr "Kuan_take_1_place2dTexture5.ofu" "Kuan_take_1_Map5.ofu";
connectAttr "Kuan_take_1_place2dTexture5.ofv" "Kuan_take_1_Map5.ofv";
connectAttr "Kuan_take_1_place2dTexture5.rf" "Kuan_take_1_Map5.rf";
connectAttr "Kuan_take_1_place2dTexture5.reu" "Kuan_take_1_Map5.reu";
connectAttr "Kuan_take_1_place2dTexture5.rev" "Kuan_take_1_Map5.rev";
connectAttr "Kuan_take_1_place2dTexture5.vt1" "Kuan_take_1_Map5.vt1";
connectAttr "Kuan_take_1_place2dTexture5.vt2" "Kuan_take_1_Map5.vt2";
connectAttr "Kuan_take_1_place2dTexture5.vt3" "Kuan_take_1_Map5.vt3";
connectAttr "Kuan_take_1_place2dTexture5.vc1" "Kuan_take_1_Map5.vc1";
connectAttr "Kuan_take_1_place2dTexture5.ofs" "Kuan_take_1_Map5.fs";
connectAttr "Kuan_take_1_business05_f_highpoly_hairPhong4.oc" "Kuan_take_1_business05_f_highpoly_hairSG8.ss"
		;
connectAttr "Kuan_take_1_business05_f_highpoly_hairSG8.msg" "Kuan_take_1_materialInfo19.sg"
		;
connectAttr "Kuan_take_1_business05_f_highpoly_hairPhong4.msg" "Kuan_take_1_materialInfo19.m"
		;
connectAttr "Kuan_take_1_business05_f_highpoly_hairPhong4.oc" "Kuan_take_1_business05_f_highpoly_hairSG9.ss"
		;
connectAttr "Kuan_take_1_business05_f_highpoly_hairSG9.msg" "Kuan_take_1_materialInfo20.sg"
		;
connectAttr "Kuan_take_1_business05_f_highpoly_hairPhong4.msg" "Kuan_take_1_materialInfo20.m"
		;
connectAttr "Kuan_take_1_Map6.oc" "Kuan_take_1_business05_f_highpolyPhong5.c";
connectAttr "Kuan_take_1_business05_f_highpolyPhong5.oc" "Kuan_take_1_business05_f_highpolySG10.ss"
		;
connectAttr "Kuan_take_1_business05_f_highpolySG10.msg" "Kuan_take_1_materialInfo21.sg"
		;
connectAttr "Kuan_take_1_business05_f_highpolyPhong5.msg" "Kuan_take_1_materialInfo21.m"
		;
connectAttr "Kuan_take_1_Map6.msg" "Kuan_take_1_materialInfo21.t" -na;
connectAttr "Kuan_take_1_business05_f_highpolyPhong5.oc" "Kuan_take_1_business05_f_highpolySG11.ss"
		;
connectAttr "Kuan_take_1_business05_f_highpolySG11.msg" "Kuan_take_1_materialInfo22.sg"
		;
connectAttr "Kuan_take_1_business05_f_highpolyPhong5.msg" "Kuan_take_1_materialInfo22.m"
		;
connectAttr "Kuan_take_1_Map6.msg" "Kuan_take_1_materialInfo22.t" -na;
connectAttr "Kuan_take_1_place2dTexture6.o" "Kuan_take_1_Map6.uv";
connectAttr "Kuan_take_1_place2dTexture6.ofu" "Kuan_take_1_Map6.ofu";
connectAttr "Kuan_take_1_place2dTexture6.ofv" "Kuan_take_1_Map6.ofv";
connectAttr "Kuan_take_1_place2dTexture6.rf" "Kuan_take_1_Map6.rf";
connectAttr "Kuan_take_1_place2dTexture6.reu" "Kuan_take_1_Map6.reu";
connectAttr "Kuan_take_1_place2dTexture6.rev" "Kuan_take_1_Map6.rev";
connectAttr "Kuan_take_1_place2dTexture6.vt1" "Kuan_take_1_Map6.vt1";
connectAttr "Kuan_take_1_place2dTexture6.vt2" "Kuan_take_1_Map6.vt2";
connectAttr "Kuan_take_1_place2dTexture6.vt3" "Kuan_take_1_Map6.vt3";
connectAttr "Kuan_take_1_place2dTexture6.vc1" "Kuan_take_1_Map6.vc1";
connectAttr "Kuan_take_1_place2dTexture6.ofs" "Kuan_take_1_Map6.fs";
connectAttr "Kuan_take_1_business05_f_highpoly_hairPhong5.oc" "Kuan_take_1_business05_f_highpoly_hairSG10.ss"
		;
connectAttr "Kuan_take_1_business05_f_highpoly_hairSG10.msg" "Kuan_take_1_materialInfo23.sg"
		;
connectAttr "Kuan_take_1_business05_f_highpoly_hairPhong5.msg" "Kuan_take_1_materialInfo23.m"
		;
connectAttr "Kuan_take_1_business05_f_highpoly_hairPhong5.oc" "Kuan_take_1_business05_f_highpoly_hairSG11.ss"
		;
connectAttr "Kuan_take_1_business05_f_highpoly_hairSG11.msg" "Kuan_take_1_materialInfo24.sg"
		;
connectAttr "Kuan_take_1_business05_f_highpoly_hairPhong5.msg" "Kuan_take_1_materialInfo24.m"
		;
connectAttr "Kuan_take_1_Map7.oc" "Kuan_take_1_business05_f_highpolyPhong6.c";
connectAttr "Kuan_take_1_business05_f_highpolyPhong6.oc" "Kuan_take_1_business05_f_highpolySG12.ss"
		;
connectAttr "Kuan_take_1_business05_f_highpolySG12.msg" "Kuan_take_1_materialInfo25.sg"
		;
connectAttr "Kuan_take_1_business05_f_highpolyPhong6.msg" "Kuan_take_1_materialInfo25.m"
		;
connectAttr "Kuan_take_1_Map7.msg" "Kuan_take_1_materialInfo25.t" -na;
connectAttr "Kuan_take_1_business05_f_highpolyPhong6.oc" "Kuan_take_1_business05_f_highpolySG13.ss"
		;
connectAttr "Kuan_take_1_business05_f_highpolySG13.msg" "Kuan_take_1_materialInfo26.sg"
		;
connectAttr "Kuan_take_1_business05_f_highpolyPhong6.msg" "Kuan_take_1_materialInfo26.m"
		;
connectAttr "Kuan_take_1_Map7.msg" "Kuan_take_1_materialInfo26.t" -na;
connectAttr "Kuan_take_1_place2dTexture7.o" "Kuan_take_1_Map7.uv";
connectAttr "Kuan_take_1_place2dTexture7.ofu" "Kuan_take_1_Map7.ofu";
connectAttr "Kuan_take_1_place2dTexture7.ofv" "Kuan_take_1_Map7.ofv";
connectAttr "Kuan_take_1_place2dTexture7.rf" "Kuan_take_1_Map7.rf";
connectAttr "Kuan_take_1_place2dTexture7.reu" "Kuan_take_1_Map7.reu";
connectAttr "Kuan_take_1_place2dTexture7.rev" "Kuan_take_1_Map7.rev";
connectAttr "Kuan_take_1_place2dTexture7.vt1" "Kuan_take_1_Map7.vt1";
connectAttr "Kuan_take_1_place2dTexture7.vt2" "Kuan_take_1_Map7.vt2";
connectAttr "Kuan_take_1_place2dTexture7.vt3" "Kuan_take_1_Map7.vt3";
connectAttr "Kuan_take_1_place2dTexture7.vc1" "Kuan_take_1_Map7.vc1";
connectAttr "Kuan_take_1_place2dTexture7.ofs" "Kuan_take_1_Map7.fs";
connectAttr "Kuan_take_1_business05_f_highpoly_hairPhong6.oc" "Kuan_take_1_business05_f_highpoly_hairSG12.ss"
		;
connectAttr "Kuan_take_1_business05_f_highpoly_hairSG12.msg" "Kuan_take_1_materialInfo27.sg"
		;
connectAttr "Kuan_take_1_business05_f_highpoly_hairPhong6.msg" "Kuan_take_1_materialInfo27.m"
		;
connectAttr "Kuan_take_1_business05_f_highpoly_hairPhong6.oc" "Kuan_take_1_business05_f_highpoly_hairSG13.ss"
		;
connectAttr "Kuan_take_1_business05_f_highpoly_hairSG13.msg" "Kuan_take_1_materialInfo28.sg"
		;
connectAttr "Kuan_take_1_business05_f_highpoly_hairPhong6.msg" "Kuan_take_1_materialInfo28.m"
		;
connectAttr "Candice_take_2_Map1.oc" "Candice_take_2_business05_f_highpolyPhong.c"
		;
connectAttr "Candice_take_2_business05_f_highpolyPhong.oc" "Candice_take_2_business05_f_highpolySG.ss"
		;
connectAttr "Candice_take_2_business05_f_highpolySG.msg" "Candice_take_2_materialInfo1.sg"
		;
connectAttr "Candice_take_2_business05_f_highpolyPhong.msg" "Candice_take_2_materialInfo1.m"
		;
connectAttr "Candice_take_2_Map1.msg" "Candice_take_2_materialInfo1.t" -na;
connectAttr "Candice_take_2_business05_f_highpolyPhong.oc" "Candice_take_2_business05_f_highpolySG1.ss"
		;
connectAttr "Candice_take_2_business05_f_highpolySG1.msg" "Candice_take_2_materialInfo2.sg"
		;
connectAttr "Candice_take_2_business05_f_highpolyPhong.msg" "Candice_take_2_materialInfo2.m"
		;
connectAttr "Candice_take_2_Map1.msg" "Candice_take_2_materialInfo2.t" -na;
connectAttr "Candice_take_2_place2dTexture1.o" "Candice_take_2_Map1.uv";
connectAttr "Candice_take_2_place2dTexture1.ofu" "Candice_take_2_Map1.ofu";
connectAttr "Candice_take_2_place2dTexture1.ofv" "Candice_take_2_Map1.ofv";
connectAttr "Candice_take_2_place2dTexture1.rf" "Candice_take_2_Map1.rf";
connectAttr "Candice_take_2_place2dTexture1.reu" "Candice_take_2_Map1.reu";
connectAttr "Candice_take_2_place2dTexture1.rev" "Candice_take_2_Map1.rev";
connectAttr "Candice_take_2_place2dTexture1.vt1" "Candice_take_2_Map1.vt1";
connectAttr "Candice_take_2_place2dTexture1.vt2" "Candice_take_2_Map1.vt2";
connectAttr "Candice_take_2_place2dTexture1.vt3" "Candice_take_2_Map1.vt3";
connectAttr "Candice_take_2_place2dTexture1.vc1" "Candice_take_2_Map1.vc1";
connectAttr "Candice_take_2_place2dTexture1.ofs" "Candice_take_2_Map1.fs";
connectAttr "Candice_take_2_business05_f_highpoly_hairPhong.oc" "Candice_take_2_business05_f_highpoly_hairSG.ss"
		;
connectAttr "Candice_take_2_business05_f_highpoly_hairSG.msg" "Candice_take_2_materialInfo3.sg"
		;
connectAttr "Candice_take_2_business05_f_highpoly_hairPhong.msg" "Candice_take_2_materialInfo3.m"
		;
connectAttr "Candice_take_2_business05_f_highpoly_hairPhong.oc" "Candice_take_2_business05_f_highpoly_hairSG1.ss"
		;
connectAttr "Candice_take_2_business05_f_highpoly_hairSG1.msg" "Candice_take_2_materialInfo4.sg"
		;
connectAttr "Candice_take_2_business05_f_highpoly_hairPhong.msg" "Candice_take_2_materialInfo4.m"
		;
connectAttr "Bip01_Spine.msg" "Candice_take_2_bindPose1.m[0]";
connectAttr "Bip01_R_Thigh.msg" "Candice_take_2_bindPose1.m[1]";
connectAttr "Bip01_Spine1.msg" "Candice_take_2_bindPose1.m[2]";
connectAttr "Bip01_Spine2.msg" "Candice_take_2_bindPose1.m[3]";
connectAttr "Bip01_Neck.msg" "Candice_take_2_bindPose1.m[4]";
connectAttr "Bip01_R_Clavicle.msg" "Candice_take_2_bindPose1.m[5]";
connectAttr "Bip01_L_Clavicle.msg" "Candice_take_2_bindPose1.m[6]";
connectAttr "Bip01_R_UpperArm.msg" "Candice_take_2_bindPose1.m[7]";
connectAttr "Bip01_R_Forearm.msg" "Candice_take_2_bindPose1.m[8]";
connectAttr "Bip01_R_Hand.msg" "Candice_take_2_bindPose1.m[9]";
connectAttr "Bip01_R_Finger0.msg" "Candice_take_2_bindPose1.m[10]";
connectAttr "Bip01_L_Thigh.msg" "Candice_take_2_bindPose1.m[11]";
connectAttr "Bip01_Head.msg" "Candice_take_2_bindPose1.m[12]";
connectAttr "Bip01_L_Calf.msg" "Candice_take_2_bindPose1.m[13]";
connectAttr "Bip01_R_Finger01.msg" "Candice_take_2_bindPose1.m[14]";
connectAttr "Bip01_R_Finger3.msg" "Candice_take_2_bindPose1.m[15]";
connectAttr "Bip01_R_Finger4.msg" "Candice_take_2_bindPose1.m[16]";
connectAttr "Bip01_R_Finger1.msg" "Candice_take_2_bindPose1.m[17]";
connectAttr "Bip01_R_Finger11.msg" "Candice_take_2_bindPose1.m[18]";
connectAttr "Bip01_R_Finger2.msg" "Candice_take_2_bindPose1.m[19]";
connectAttr "Bip01_R_Finger21.msg" "Candice_take_2_bindPose1.m[20]";
connectAttr "Bip01_R_Finger31.msg" "Candice_take_2_bindPose1.m[21]";
connectAttr "Bip01_R_Finger41.msg" "Candice_take_2_bindPose1.m[22]";
connectAttr "Bip01_R_Calf.msg" "Candice_take_2_bindPose1.m[23]";
connectAttr "Bip01_R_Foot.msg" "Candice_take_2_bindPose1.m[24]";
connectAttr "Bip01_R_Toe0.msg" "Candice_take_2_bindPose1.m[25]";
connectAttr "Bip01_L_Foot.msg" "Candice_take_2_bindPose1.m[26]";
connectAttr "Bip01_L_Toe0.msg" "Candice_take_2_bindPose1.m[27]";
connectAttr "Bip01_L_UpperArm.msg" "Candice_take_2_bindPose1.m[28]";
connectAttr "Bip01_L_Forearm.msg" "Candice_take_2_bindPose1.m[29]";
connectAttr "Bip01_L_Hand.msg" "Candice_take_2_bindPose1.m[30]";
connectAttr "Bip01_L_Finger0.msg" "Candice_take_2_bindPose1.m[31]";
connectAttr "Bip01_L_Finger01.msg" "Candice_take_2_bindPose1.m[32]";
connectAttr "Bip01_L_Finger3.msg" "Candice_take_2_bindPose1.m[33]";
connectAttr "Bip01_L_Finger4.msg" "Candice_take_2_bindPose1.m[34]";
connectAttr "Bip01_L_Finger1.msg" "Candice_take_2_bindPose1.m[35]";
connectAttr "Bip01_L_Finger11.msg" "Candice_take_2_bindPose1.m[36]";
connectAttr "Bip01_L_Finger2.msg" "Candice_take_2_bindPose1.m[37]";
connectAttr "Bip01_L_Finger21.msg" "Candice_take_2_bindPose1.m[38]";
connectAttr "Bip01_L_Finger31.msg" "Candice_take_2_bindPose1.m[39]";
connectAttr "Bip01_L_Finger41.msg" "Candice_take_2_bindPose1.m[40]";
connectAttr "Candice_take_2_bindPose1.w" "Candice_take_2_bindPose1.p[0]";
connectAttr "Candice_take_2_bindPose1.m[0]" "Candice_take_2_bindPose1.p[1]";
connectAttr "Candice_take_2_bindPose1.m[0]" "Candice_take_2_bindPose1.p[2]";
connectAttr "Candice_take_2_bindPose1.m[2]" "Candice_take_2_bindPose1.p[3]";
connectAttr "Candice_take_2_bindPose1.m[3]" "Candice_take_2_bindPose1.p[4]";
connectAttr "Candice_take_2_bindPose1.m[4]" "Candice_take_2_bindPose1.p[5]";
connectAttr "Candice_take_2_bindPose1.m[4]" "Candice_take_2_bindPose1.p[6]";
connectAttr "Candice_take_2_bindPose1.m[5]" "Candice_take_2_bindPose1.p[7]";
connectAttr "Candice_take_2_bindPose1.m[7]" "Candice_take_2_bindPose1.p[8]";
connectAttr "Candice_take_2_bindPose1.m[8]" "Candice_take_2_bindPose1.p[9]";
connectAttr "Candice_take_2_bindPose1.m[9]" "Candice_take_2_bindPose1.p[10]";
connectAttr "Candice_take_2_bindPose1.m[0]" "Candice_take_2_bindPose1.p[11]";
connectAttr "Candice_take_2_bindPose1.m[4]" "Candice_take_2_bindPose1.p[12]";
connectAttr "Candice_take_2_bindPose1.m[11]" "Candice_take_2_bindPose1.p[13]";
connectAttr "Candice_take_2_bindPose1.m[10]" "Candice_take_2_bindPose1.p[14]";
connectAttr "Candice_take_2_bindPose1.m[9]" "Candice_take_2_bindPose1.p[15]";
connectAttr "Candice_take_2_bindPose1.m[9]" "Candice_take_2_bindPose1.p[16]";
connectAttr "Candice_take_2_bindPose1.m[9]" "Candice_take_2_bindPose1.p[17]";
connectAttr "Candice_take_2_bindPose1.m[17]" "Candice_take_2_bindPose1.p[18]";
connectAttr "Candice_take_2_bindPose1.m[9]" "Candice_take_2_bindPose1.p[19]";
connectAttr "Candice_take_2_bindPose1.m[19]" "Candice_take_2_bindPose1.p[20]";
connectAttr "Candice_take_2_bindPose1.m[15]" "Candice_take_2_bindPose1.p[21]";
connectAttr "Candice_take_2_bindPose1.m[16]" "Candice_take_2_bindPose1.p[22]";
connectAttr "Candice_take_2_bindPose1.m[1]" "Candice_take_2_bindPose1.p[23]";
connectAttr "Candice_take_2_bindPose1.m[23]" "Candice_take_2_bindPose1.p[24]";
connectAttr "Candice_take_2_bindPose1.m[24]" "Candice_take_2_bindPose1.p[25]";
connectAttr "Candice_take_2_bindPose1.m[13]" "Candice_take_2_bindPose1.p[26]";
connectAttr "Candice_take_2_bindPose1.m[26]" "Candice_take_2_bindPose1.p[27]";
connectAttr "Candice_take_2_bindPose1.m[6]" "Candice_take_2_bindPose1.p[28]";
connectAttr "Candice_take_2_bindPose1.m[28]" "Candice_take_2_bindPose1.p[29]";
connectAttr "Candice_take_2_bindPose1.m[29]" "Candice_take_2_bindPose1.p[30]";
connectAttr "Candice_take_2_bindPose1.m[30]" "Candice_take_2_bindPose1.p[31]";
connectAttr "Candice_take_2_bindPose1.m[31]" "Candice_take_2_bindPose1.p[32]";
connectAttr "Candice_take_2_bindPose1.m[30]" "Candice_take_2_bindPose1.p[33]";
connectAttr "Candice_take_2_bindPose1.m[30]" "Candice_take_2_bindPose1.p[34]";
connectAttr "Candice_take_2_bindPose1.m[30]" "Candice_take_2_bindPose1.p[35]";
connectAttr "Candice_take_2_bindPose1.m[35]" "Candice_take_2_bindPose1.p[36]";
connectAttr "Candice_take_2_bindPose1.m[30]" "Candice_take_2_bindPose1.p[37]";
connectAttr "Candice_take_2_bindPose1.m[37]" "Candice_take_2_bindPose1.p[38]";
connectAttr "Candice_take_2_bindPose1.m[33]" "Candice_take_2_bindPose1.p[39]";
connectAttr "Candice_take_2_bindPose1.m[34]" "Candice_take_2_bindPose1.p[40]";
connectAttr "Bip01_Spine.bps" "Candice_take_2_bindPose1.wm[0]";
connectAttr "Bip01_R_Thigh.bps" "Candice_take_2_bindPose1.wm[1]";
connectAttr "Bip01_Spine1.bps" "Candice_take_2_bindPose1.wm[2]";
connectAttr "Bip01_Spine2.bps" "Candice_take_2_bindPose1.wm[3]";
connectAttr "Bip01_Neck.bps" "Candice_take_2_bindPose1.wm[4]";
connectAttr "Bip01_R_Clavicle.bps" "Candice_take_2_bindPose1.wm[5]";
connectAttr "Bip01_L_Clavicle.bps" "Candice_take_2_bindPose1.wm[6]";
connectAttr "Bip01_R_UpperArm.bps" "Candice_take_2_bindPose1.wm[7]";
connectAttr "Bip01_R_Forearm.bps" "Candice_take_2_bindPose1.wm[8]";
connectAttr "Bip01_R_Hand.bps" "Candice_take_2_bindPose1.wm[9]";
connectAttr "Bip01_R_Finger0.bps" "Candice_take_2_bindPose1.wm[10]";
connectAttr "Bip01_L_Thigh.bps" "Candice_take_2_bindPose1.wm[11]";
connectAttr "Bip01_Head.bps" "Candice_take_2_bindPose1.wm[12]";
connectAttr "Bip01_L_Calf.bps" "Candice_take_2_bindPose1.wm[13]";
connectAttr "Bip01_R_Finger01.bps" "Candice_take_2_bindPose1.wm[14]";
connectAttr "Bip01_R_Finger3.bps" "Candice_take_2_bindPose1.wm[15]";
connectAttr "Bip01_R_Finger4.bps" "Candice_take_2_bindPose1.wm[16]";
connectAttr "Bip01_R_Finger1.bps" "Candice_take_2_bindPose1.wm[17]";
connectAttr "Bip01_R_Finger11.bps" "Candice_take_2_bindPose1.wm[18]";
connectAttr "Bip01_R_Finger2.bps" "Candice_take_2_bindPose1.wm[19]";
connectAttr "Bip01_R_Finger21.bps" "Candice_take_2_bindPose1.wm[20]";
connectAttr "Bip01_R_Finger31.bps" "Candice_take_2_bindPose1.wm[21]";
connectAttr "Bip01_R_Finger41.bps" "Candice_take_2_bindPose1.wm[22]";
connectAttr "Bip01_R_Calf.bps" "Candice_take_2_bindPose1.wm[23]";
connectAttr "Bip01_R_Foot.bps" "Candice_take_2_bindPose1.wm[24]";
connectAttr "Bip01_R_Toe0.bps" "Candice_take_2_bindPose1.wm[25]";
connectAttr "Bip01_L_Foot.bps" "Candice_take_2_bindPose1.wm[26]";
connectAttr "Bip01_L_Toe0.bps" "Candice_take_2_bindPose1.wm[27]";
connectAttr "Bip01_L_UpperArm.bps" "Candice_take_2_bindPose1.wm[28]";
connectAttr "Bip01_L_Forearm.bps" "Candice_take_2_bindPose1.wm[29]";
connectAttr "Bip01_L_Hand.bps" "Candice_take_2_bindPose1.wm[30]";
connectAttr "Bip01_L_Finger0.bps" "Candice_take_2_bindPose1.wm[31]";
connectAttr "Bip01_L_Finger01.bps" "Candice_take_2_bindPose1.wm[32]";
connectAttr "Bip01_L_Finger3.bps" "Candice_take_2_bindPose1.wm[33]";
connectAttr "Bip01_L_Finger4.bps" "Candice_take_2_bindPose1.wm[34]";
connectAttr "Bip01_L_Finger1.bps" "Candice_take_2_bindPose1.wm[35]";
connectAttr "Bip01_L_Finger11.bps" "Candice_take_2_bindPose1.wm[36]";
connectAttr "Bip01_L_Finger2.bps" "Candice_take_2_bindPose1.wm[37]";
connectAttr "Bip01_L_Finger21.bps" "Candice_take_2_bindPose1.wm[38]";
connectAttr "Bip01_L_Finger31.bps" "Candice_take_2_bindPose1.wm[39]";
connectAttr "Bip01_L_Finger41.bps" "Candice_take_2_bindPose1.wm[40]";
connectAttr "business05_f_highpolySG.pa" ":renderPartition.st" -na;
connectAttr "business05_f_highpolySG1.pa" ":renderPartition.st" -na;
connectAttr "business05_f_highpoly_hairSG.pa" ":renderPartition.st" -na;
connectAttr "business05_f_highpoly_hairSG1.pa" ":renderPartition.st" -na;
connectAttr "MoCap_Result_Animations_v1_business05_f_highpolySG.pa" ":renderPartition.st"
		 -na;
connectAttr "MoCap_Result_Animations_v1_business05_f_highpolySG1.pa" ":renderPartition.st"
		 -na;
connectAttr "MoCap_Result_Animations_v1_business05_f_highpoly_hairSG.pa" ":renderPartition.st"
		 -na;
connectAttr "MoCap_Result_Animations_v1_business05_f_highpoly_hairSG1.pa" ":renderPartition.st"
		 -na;
connectAttr "idle_v1:business05_f_highpolySG.pa" ":renderPartition.st" -na;
connectAttr "idle_v1:business05_f_highpolySG1.pa" ":renderPartition.st" -na;
connectAttr "idle_v1:business05_f_highpoly_hairSG.pa" ":renderPartition.st" -na;
connectAttr "idle_v1:business05_f_highpoly_hairSG1.pa" ":renderPartition.st" -na
		;
connectAttr "idle_v1_business05_f_highpolySG.pa" ":renderPartition.st" -na;
connectAttr "idle_v1_business05_f_highpolySG1.pa" ":renderPartition.st" -na;
connectAttr "idle_v1_business05_f_highpoly_hairSG.pa" ":renderPartition.st" -na;
connectAttr "idle_v1_business05_f_highpoly_hairSG1.pa" ":renderPartition.st" -na
		;
connectAttr "idle_v1_business05_f_highpolySG2.pa" ":renderPartition.st" -na;
connectAttr "idle_v1_business05_f_highpolySG3.pa" ":renderPartition.st" -na;
connectAttr "idle_v1_business05_f_highpoly_hairSG2.pa" ":renderPartition.st" -na
		;
connectAttr "idle_v1_business05_f_highpoly_hairSG3.pa" ":renderPartition.st" -na
		;
connectAttr "stand_v1_business05_f_highpolySG.pa" ":renderPartition.st" -na;
connectAttr "stand_v1_business05_f_highpolySG1.pa" ":renderPartition.st" -na;
connectAttr "stand_v1_business05_f_highpoly_hairSG.pa" ":renderPartition.st" -na
		;
connectAttr "stand_v1_business05_f_highpoly_hairSG1.pa" ":renderPartition.st" -na
		;
connectAttr "sit_idle_v1_business05_f_highpolySG.pa" ":renderPartition.st" -na;
connectAttr "sit_idle_v1_business05_f_highpolySG1.pa" ":renderPartition.st" -na;
connectAttr "sit_idle_v1_business05_f_highpoly_hairSG.pa" ":renderPartition.st" 
		-na;
connectAttr "sit_idle_v1_business05_f_highpoly_hairSG1.pa" ":renderPartition.st"
		 -na;
connectAttr "sit_talk_v1_business05_f_highpolySG.pa" ":renderPartition.st" -na;
connectAttr "sit_talk_v1_business05_f_highpolySG1.pa" ":renderPartition.st" -na;
connectAttr "sit_talk_v1_business05_f_highpoly_hairSG.pa" ":renderPartition.st" 
		-na;
connectAttr "sit_talk_v1_business05_f_highpoly_hairSG1.pa" ":renderPartition.st"
		 -na;
connectAttr "Animations_v3_business05_f_highpolySG.pa" ":renderPartition.st" -na
		;
connectAttr "Animations_v3_business05_f_highpolySG1.pa" ":renderPartition.st" -na
		;
connectAttr "Animations_v3_business05_f_highpoly_hairSG.pa" ":renderPartition.st"
		 -na;
connectAttr "Animations_v3_business05_f_highpoly_hairSG1.pa" ":renderPartition.st"
		 -na;
connectAttr "Jessica_Take_2_2_business05_f_highpolySG.pa" ":renderPartition.st" 
		-na;
connectAttr "Jessica_Take_2_2_business05_f_highpolySG1.pa" ":renderPartition.st"
		 -na;
connectAttr "Jessica_Take_2_2_business05_f_highpoly_hairSG.pa" ":renderPartition.st"
		 -na;
connectAttr "Jessica_Take_2_2_business05_f_highpoly_hairSG1.pa" ":renderPartition.st"
		 -na;
connectAttr "Jessica_Take_2_2_business05_f_highpolySG2.pa" ":renderPartition.st"
		 -na;
connectAttr "Jessica_Take_2_2_business05_f_highpolySG3.pa" ":renderPartition.st"
		 -na;
connectAttr "Jessica_Take_2_2_business05_f_highpoly_hairSG2.pa" ":renderPartition.st"
		 -na;
connectAttr "Jessica_Take_2_2_business05_f_highpoly_hairSG3.pa" ":renderPartition.st"
		 -na;
connectAttr "Jessica_Take_2_2_business05_f_highpolySG4.pa" ":renderPartition.st"
		 -na;
connectAttr "Jessica_Take_2_2_business05_f_highpolySG5.pa" ":renderPartition.st"
		 -na;
connectAttr "Jessica_Take_2_2_business05_f_highpoly_hairSG4.pa" ":renderPartition.st"
		 -na;
connectAttr "Jessica_Take_2_2_business05_f_highpoly_hairSG5.pa" ":renderPartition.st"
		 -na;
connectAttr "Jessica_Take_2_2_business05_f_highpolySG6.pa" ":renderPartition.st"
		 -na;
connectAttr "Jessica_Take_2_2_business05_f_highpolySG7.pa" ":renderPartition.st"
		 -na;
connectAttr "Jessica_Take_2_2_business05_f_highpoly_hairSG6.pa" ":renderPartition.st"
		 -na;
connectAttr "Jessica_Take_2_2_business05_f_highpoly_hairSG7.pa" ":renderPartition.st"
		 -na;
connectAttr "Jessica_Take_2_2_business05_f_highpolySG8.pa" ":renderPartition.st"
		 -na;
connectAttr "Jessica_Take_2_2_business05_f_highpolySG9.pa" ":renderPartition.st"
		 -na;
connectAttr "Jessica_Take_2_2_business05_f_highpoly_hairSG8.pa" ":renderPartition.st"
		 -na;
connectAttr "Jessica_Take_2_2_business05_f_highpoly_hairSG9.pa" ":renderPartition.st"
		 -na;
connectAttr "Jessica_Take_2_2_business05_f_highpolySG10.pa" ":renderPartition.st"
		 -na;
connectAttr "Jessica_Take_2_2_business05_f_highpolySG11.pa" ":renderPartition.st"
		 -na;
connectAttr "Jessica_Take_2_2_business05_f_highpoly_hairSG10.pa" ":renderPartition.st"
		 -na;
connectAttr "Jessica_Take_2_2_business05_f_highpoly_hairSG11.pa" ":renderPartition.st"
		 -na;
connectAttr "Candice_Take_1_business05_f_highpolySG.pa" ":renderPartition.st" -na
		;
connectAttr "Candice_Take_1_business05_f_highpolySG1.pa" ":renderPartition.st" -na
		;
connectAttr "Candice_Take_1_business05_f_highpoly_hairSG.pa" ":renderPartition.st"
		 -na;
connectAttr "Candice_Take_1_business05_f_highpoly_hairSG1.pa" ":renderPartition.st"
		 -na;
connectAttr "Candice_Take_1_business05_f_highpolySG2.pa" ":renderPartition.st" -na
		;
connectAttr "Candice_Take_1_business05_f_highpolySG3.pa" ":renderPartition.st" -na
		;
connectAttr "Candice_Take_1_business05_f_highpoly_hairSG2.pa" ":renderPartition.st"
		 -na;
connectAttr "Candice_Take_1_business05_f_highpoly_hairSG3.pa" ":renderPartition.st"
		 -na;
connectAttr "Candice_Take_1_business05_f_highpolySG4.pa" ":renderPartition.st" -na
		;
connectAttr "Candice_Take_1_business05_f_highpolySG5.pa" ":renderPartition.st" -na
		;
connectAttr "Candice_Take_1_business05_f_highpoly_hairSG4.pa" ":renderPartition.st"
		 -na;
connectAttr "Candice_Take_1_business05_f_highpoly_hairSG5.pa" ":renderPartition.st"
		 -na;
connectAttr "Candice_Take_1_business05_f_highpolySG6.pa" ":renderPartition.st" -na
		;
connectAttr "Candice_Take_1_business05_f_highpolySG7.pa" ":renderPartition.st" -na
		;
connectAttr "Candice_Take_1_business05_f_highpoly_hairSG6.pa" ":renderPartition.st"
		 -na;
connectAttr "Candice_Take_1_business05_f_highpoly_hairSG7.pa" ":renderPartition.st"
		 -na;
connectAttr "Candice_Take_1_business05_f_highpolySG8.pa" ":renderPartition.st" -na
		;
connectAttr "Candice_Take_1_business05_f_highpolySG9.pa" ":renderPartition.st" -na
		;
connectAttr "Candice_Take_1_business05_f_highpoly_hairSG8.pa" ":renderPartition.st"
		 -na;
connectAttr "Candice_Take_1_business05_f_highpoly_hairSG9.pa" ":renderPartition.st"
		 -na;
connectAttr "Candice_Take_1_business05_f_highpolySG10.pa" ":renderPartition.st" 
		-na;
connectAttr "Candice_Take_1_business05_f_highpolySG11.pa" ":renderPartition.st" 
		-na;
connectAttr "Candice_Take_1_business05_f_highpoly_hairSG10.pa" ":renderPartition.st"
		 -na;
connectAttr "Candice_Take_1_business05_f_highpoly_hairSG11.pa" ":renderPartition.st"
		 -na;
connectAttr "Candice_Take_1_business05_f_highpolySG12.pa" ":renderPartition.st" 
		-na;
connectAttr "Candice_Take_1_business05_f_highpolySG13.pa" ":renderPartition.st" 
		-na;
connectAttr "Candice_Take_1_business05_f_highpoly_hairSG12.pa" ":renderPartition.st"
		 -na;
connectAttr "Candice_Take_1_business05_f_highpoly_hairSG13.pa" ":renderPartition.st"
		 -na;
connectAttr "Candice_Take_1_business05_f_highpolySG14.pa" ":renderPartition.st" 
		-na;
connectAttr "Candice_Take_1_business05_f_highpolySG15.pa" ":renderPartition.st" 
		-na;
connectAttr "Candice_Take_1_business05_f_highpoly_hairSG14.pa" ":renderPartition.st"
		 -na;
connectAttr "Candice_Take_1_business05_f_highpoly_hairSG15.pa" ":renderPartition.st"
		 -na;
connectAttr "Animations_v3_business05_f_highpolySG2.pa" ":renderPartition.st" -na
		;
connectAttr "Animations_v3_business05_f_highpolySG3.pa" ":renderPartition.st" -na
		;
connectAttr "Animations_v3_business05_f_highpoly_hairSG2.pa" ":renderPartition.st"
		 -na;
connectAttr "Animations_v3_business05_f_highpoly_hairSG3.pa" ":renderPartition.st"
		 -na;
connectAttr "Kuan_take_1_business05_f_highpolySG.pa" ":renderPartition.st" -na;
connectAttr "Kuan_take_1_business05_f_highpolySG1.pa" ":renderPartition.st" -na;
connectAttr "Kuan_take_1_business05_f_highpoly_hairSG.pa" ":renderPartition.st" 
		-na;
connectAttr "Kuan_take_1_business05_f_highpoly_hairSG1.pa" ":renderPartition.st"
		 -na;
connectAttr "Kuan_take_1_business05_f_highpolySG2.pa" ":renderPartition.st" -na;
connectAttr "Kuan_take_1_business05_f_highpolySG3.pa" ":renderPartition.st" -na;
connectAttr "Kuan_take_1_business05_f_highpoly_hairSG2.pa" ":renderPartition.st"
		 -na;
connectAttr "Kuan_take_1_business05_f_highpoly_hairSG3.pa" ":renderPartition.st"
		 -na;
connectAttr "Kuan_take_1_business05_f_highpolySG4.pa" ":renderPartition.st" -na;
connectAttr "Kuan_take_1_business05_f_highpolySG5.pa" ":renderPartition.st" -na;
connectAttr "Kuan_take_1_business05_f_highpoly_hairSG4.pa" ":renderPartition.st"
		 -na;
connectAttr "Kuan_take_1_business05_f_highpoly_hairSG5.pa" ":renderPartition.st"
		 -na;
connectAttr "Kuan_take_1_business05_f_highpolySG6.pa" ":renderPartition.st" -na;
connectAttr "Kuan_take_1_business05_f_highpolySG7.pa" ":renderPartition.st" -na;
connectAttr "Kuan_take_1_business05_f_highpoly_hairSG6.pa" ":renderPartition.st"
		 -na;
connectAttr "Kuan_take_1_business05_f_highpoly_hairSG7.pa" ":renderPartition.st"
		 -na;
connectAttr "Kuan_take_1_business05_f_highpolySG8.pa" ":renderPartition.st" -na;
connectAttr "Kuan_take_1_business05_f_highpolySG9.pa" ":renderPartition.st" -na;
connectAttr "Kuan_take_1_business05_f_highpoly_hairSG8.pa" ":renderPartition.st"
		 -na;
connectAttr "Kuan_take_1_business05_f_highpoly_hairSG9.pa" ":renderPartition.st"
		 -na;
connectAttr "Kuan_take_1_business05_f_highpolySG10.pa" ":renderPartition.st" -na
		;
connectAttr "Kuan_take_1_business05_f_highpolySG11.pa" ":renderPartition.st" -na
		;
connectAttr "Kuan_take_1_business05_f_highpoly_hairSG10.pa" ":renderPartition.st"
		 -na;
connectAttr "Kuan_take_1_business05_f_highpoly_hairSG11.pa" ":renderPartition.st"
		 -na;
connectAttr "Kuan_take_1_business05_f_highpolySG12.pa" ":renderPartition.st" -na
		;
connectAttr "Kuan_take_1_business05_f_highpolySG13.pa" ":renderPartition.st" -na
		;
connectAttr "Kuan_take_1_business05_f_highpoly_hairSG12.pa" ":renderPartition.st"
		 -na;
connectAttr "Kuan_take_1_business05_f_highpoly_hairSG13.pa" ":renderPartition.st"
		 -na;
connectAttr "Candice_take_2_business05_f_highpolySG.pa" ":renderPartition.st" -na
		;
connectAttr "Candice_take_2_business05_f_highpolySG1.pa" ":renderPartition.st" -na
		;
connectAttr "Candice_take_2_business05_f_highpoly_hairSG.pa" ":renderPartition.st"
		 -na;
connectAttr "Candice_take_2_business05_f_highpoly_hairSG1.pa" ":renderPartition.st"
		 -na;
connectAttr "business05_f_highpolyPhong.msg" ":defaultShaderList1.s" -na;
connectAttr "business05_f_highpoly_hairPhong.msg" ":defaultShaderList1.s" -na;
connectAttr "MoCap_Result_Animations_v1_business05_f_highpolyPhong.msg" ":defaultShaderList1.s"
		 -na;
connectAttr "MoCap_Result_Animations_v1_business05_f_highpoly_hairPhong.msg" ":defaultShaderList1.s"
		 -na;
connectAttr "idle_v1:business05_f_highpolyPhong.msg" ":defaultShaderList1.s" -na
		;
connectAttr "idle_v1:business05_f_highpoly_hairPhong.msg" ":defaultShaderList1.s"
		 -na;
connectAttr "idle_v1_business05_f_highpolyPhong.msg" ":defaultShaderList1.s" -na
		;
connectAttr "idle_v1_business05_f_highpoly_hairPhong.msg" ":defaultShaderList1.s"
		 -na;
connectAttr "idle_v1_business05_f_highpolyPhong1.msg" ":defaultShaderList1.s" -na
		;
connectAttr "idle_v1_business05_f_highpoly_hairPhong1.msg" ":defaultShaderList1.s"
		 -na;
connectAttr "stand_v1_business05_f_highpolyPhong.msg" ":defaultShaderList1.s" -na
		;
connectAttr "stand_v1_business05_f_highpoly_hairPhong.msg" ":defaultShaderList1.s"
		 -na;
connectAttr "sit_idle_v1_business05_f_highpolyPhong.msg" ":defaultShaderList1.s"
		 -na;
connectAttr "sit_idle_v1_business05_f_highpoly_hairPhong.msg" ":defaultShaderList1.s"
		 -na;
connectAttr "sit_talk_v1_business05_f_highpolyPhong.msg" ":defaultShaderList1.s"
		 -na;
connectAttr "sit_talk_v1_business05_f_highpoly_hairPhong.msg" ":defaultShaderList1.s"
		 -na;
connectAttr "Animations_v3_business05_f_highpolyPhong.msg" ":defaultShaderList1.s"
		 -na;
connectAttr "Animations_v3_business05_f_highpoly_hairPhong.msg" ":defaultShaderList1.s"
		 -na;
connectAttr "Jessica_Take_2_2_business05_f_highpolyPhong.msg" ":defaultShaderList1.s"
		 -na;
connectAttr "Jessica_Take_2_2_business05_f_highpoly_hairPhong.msg" ":defaultShaderList1.s"
		 -na;
connectAttr "Jessica_Take_2_2_business05_f_highpolyPhong1.msg" ":defaultShaderList1.s"
		 -na;
connectAttr "Jessica_Take_2_2_business05_f_highpoly_hairPhong1.msg" ":defaultShaderList1.s"
		 -na;
connectAttr "Jessica_Take_2_2_business05_f_highpolyPhong2.msg" ":defaultShaderList1.s"
		 -na;
connectAttr "Jessica_Take_2_2_business05_f_highpoly_hairPhong2.msg" ":defaultShaderList1.s"
		 -na;
connectAttr "Jessica_Take_2_2_business05_f_highpolyPhong3.msg" ":defaultShaderList1.s"
		 -na;
connectAttr "Jessica_Take_2_2_business05_f_highpoly_hairPhong3.msg" ":defaultShaderList1.s"
		 -na;
connectAttr "Jessica_Take_2_2_business05_f_highpolyPhong4.msg" ":defaultShaderList1.s"
		 -na;
connectAttr "Jessica_Take_2_2_business05_f_highpoly_hairPhong4.msg" ":defaultShaderList1.s"
		 -na;
connectAttr "Jessica_Take_2_2_business05_f_highpolyPhong5.msg" ":defaultShaderList1.s"
		 -na;
connectAttr "Jessica_Take_2_2_business05_f_highpoly_hairPhong5.msg" ":defaultShaderList1.s"
		 -na;
connectAttr "Candice_Take_1_business05_f_highpolyPhong.msg" ":defaultShaderList1.s"
		 -na;
connectAttr "Candice_Take_1_business05_f_highpoly_hairPhong.msg" ":defaultShaderList1.s"
		 -na;
connectAttr "Candice_Take_1_business05_f_highpolyPhong1.msg" ":defaultShaderList1.s"
		 -na;
connectAttr "Candice_Take_1_business05_f_highpoly_hairPhong1.msg" ":defaultShaderList1.s"
		 -na;
connectAttr "Candice_Take_1_business05_f_highpolyPhong2.msg" ":defaultShaderList1.s"
		 -na;
connectAttr "Candice_Take_1_business05_f_highpoly_hairPhong2.msg" ":defaultShaderList1.s"
		 -na;
connectAttr "Candice_Take_1_business05_f_highpolyPhong3.msg" ":defaultShaderList1.s"
		 -na;
connectAttr "Candice_Take_1_business05_f_highpoly_hairPhong3.msg" ":defaultShaderList1.s"
		 -na;
connectAttr "Candice_Take_1_business05_f_highpolyPhong4.msg" ":defaultShaderList1.s"
		 -na;
connectAttr "Candice_Take_1_business05_f_highpoly_hairPhong4.msg" ":defaultShaderList1.s"
		 -na;
connectAttr "Candice_Take_1_business05_f_highpolyPhong5.msg" ":defaultShaderList1.s"
		 -na;
connectAttr "Candice_Take_1_business05_f_highpoly_hairPhong5.msg" ":defaultShaderList1.s"
		 -na;
connectAttr "Candice_Take_1_business05_f_highpolyPhong6.msg" ":defaultShaderList1.s"
		 -na;
connectAttr "Candice_Take_1_business05_f_highpoly_hairPhong6.msg" ":defaultShaderList1.s"
		 -na;
connectAttr "Candice_Take_1_business05_f_highpolyPhong7.msg" ":defaultShaderList1.s"
		 -na;
connectAttr "Candice_Take_1_business05_f_highpoly_hairPhong7.msg" ":defaultShaderList1.s"
		 -na;
connectAttr "Animations_v3_business05_f_highpolyPhong1.msg" ":defaultShaderList1.s"
		 -na;
connectAttr "Animations_v3_business05_f_highpoly_hairPhong1.msg" ":defaultShaderList1.s"
		 -na;
connectAttr "Kuan_take_1_business05_f_highpolyPhong.msg" ":defaultShaderList1.s"
		 -na;
connectAttr "Kuan_take_1_business05_f_highpoly_hairPhong.msg" ":defaultShaderList1.s"
		 -na;
connectAttr "Kuan_take_1_business05_f_highpolyPhong1.msg" ":defaultShaderList1.s"
		 -na;
connectAttr "Kuan_take_1_business05_f_highpoly_hairPhong1.msg" ":defaultShaderList1.s"
		 -na;
connectAttr "Kuan_take_1_business05_f_highpolyPhong2.msg" ":defaultShaderList1.s"
		 -na;
connectAttr "Kuan_take_1_business05_f_highpoly_hairPhong2.msg" ":defaultShaderList1.s"
		 -na;
connectAttr "Kuan_take_1_business05_f_highpolyPhong3.msg" ":defaultShaderList1.s"
		 -na;
connectAttr "Kuan_take_1_business05_f_highpoly_hairPhong3.msg" ":defaultShaderList1.s"
		 -na;
connectAttr "Kuan_take_1_business05_f_highpolyPhong4.msg" ":defaultShaderList1.s"
		 -na;
connectAttr "Kuan_take_1_business05_f_highpoly_hairPhong4.msg" ":defaultShaderList1.s"
		 -na;
connectAttr "Kuan_take_1_business05_f_highpolyPhong5.msg" ":defaultShaderList1.s"
		 -na;
connectAttr "Kuan_take_1_business05_f_highpoly_hairPhong5.msg" ":defaultShaderList1.s"
		 -na;
connectAttr "Kuan_take_1_business05_f_highpolyPhong6.msg" ":defaultShaderList1.s"
		 -na;
connectAttr "Kuan_take_1_business05_f_highpoly_hairPhong6.msg" ":defaultShaderList1.s"
		 -na;
connectAttr "Candice_take_2_business05_f_highpolyPhong.msg" ":defaultShaderList1.s"
		 -na;
connectAttr "Candice_take_2_business05_f_highpoly_hairPhong.msg" ":defaultShaderList1.s"
		 -na;
connectAttr "Map1.msg" ":defaultTextureList1.tx" -na;
connectAttr "MoCap_Result_Animations_v1_Map1.msg" ":defaultTextureList1.tx" -na;
connectAttr "idle_v1:Map1.msg" ":defaultTextureList1.tx" -na;
connectAttr "idle_v1_Map1.msg" ":defaultTextureList1.tx" -na;
connectAttr "idle_v1_Map2.msg" ":defaultTextureList1.tx" -na;
connectAttr "stand_v1_Map1.msg" ":defaultTextureList1.tx" -na;
connectAttr "sit_idle_v1_Map1.msg" ":defaultTextureList1.tx" -na;
connectAttr "sit_talk_v1_Map1.msg" ":defaultTextureList1.tx" -na;
connectAttr "Animations_v3_Map1.msg" ":defaultTextureList1.tx" -na;
connectAttr "Jessica_Take_2_2_Map1.msg" ":defaultTextureList1.tx" -na;
connectAttr "Jessica_Take_2_2_Map2.msg" ":defaultTextureList1.tx" -na;
connectAttr "Jessica_Take_2_2_Map3.msg" ":defaultTextureList1.tx" -na;
connectAttr "Jessica_Take_2_2_Map4.msg" ":defaultTextureList1.tx" -na;
connectAttr "Jessica_Take_2_2_Map5.msg" ":defaultTextureList1.tx" -na;
connectAttr "Jessica_Take_2_2_Map6.msg" ":defaultTextureList1.tx" -na;
connectAttr "Candice_Take_1_Map1.msg" ":defaultTextureList1.tx" -na;
connectAttr "Candice_Take_1_Map2.msg" ":defaultTextureList1.tx" -na;
connectAttr "Candice_Take_1_Map3.msg" ":defaultTextureList1.tx" -na;
connectAttr "Candice_Take_1_Map4.msg" ":defaultTextureList1.tx" -na;
connectAttr "Candice_Take_1_Map5.msg" ":defaultTextureList1.tx" -na;
connectAttr "Candice_Take_1_Map6.msg" ":defaultTextureList1.tx" -na;
connectAttr "Candice_Take_1_Map7.msg" ":defaultTextureList1.tx" -na;
connectAttr "Candice_Take_1_Map8.msg" ":defaultTextureList1.tx" -na;
connectAttr "Animations_v3_Map2.msg" ":defaultTextureList1.tx" -na;
connectAttr "Kuan_take_1_Map1.msg" ":defaultTextureList1.tx" -na;
connectAttr "Kuan_take_1_Map2.msg" ":defaultTextureList1.tx" -na;
connectAttr "Kuan_take_1_Map3.msg" ":defaultTextureList1.tx" -na;
connectAttr "Kuan_take_1_Map4.msg" ":defaultTextureList1.tx" -na;
connectAttr "Kuan_take_1_Map5.msg" ":defaultTextureList1.tx" -na;
connectAttr "Kuan_take_1_Map6.msg" ":defaultTextureList1.tx" -na;
connectAttr "Kuan_take_1_Map7.msg" ":defaultTextureList1.tx" -na;
connectAttr "Candice_take_2_Map1.msg" ":defaultTextureList1.tx" -na;
connectAttr "place2dTexture1.msg" ":defaultRenderUtilityList1.u" -na;
connectAttr "MoCap_Result_Animations_v1_place2dTexture1.msg" ":defaultRenderUtilityList1.u"
		 -na;
connectAttr "idle_v1:place2dTexture1.msg" ":defaultRenderUtilityList1.u" -na;
connectAttr "idle_v1_place2dTexture1.msg" ":defaultRenderUtilityList1.u" -na;
connectAttr "idle_v1_place2dTexture2.msg" ":defaultRenderUtilityList1.u" -na;
connectAttr "stand_v1_place2dTexture1.msg" ":defaultRenderUtilityList1.u" -na;
connectAttr "sit_idle_v1_place2dTexture1.msg" ":defaultRenderUtilityList1.u" -na
		;
connectAttr "sit_talk_v1_place2dTexture1.msg" ":defaultRenderUtilityList1.u" -na
		;
connectAttr "Animations_v3_place2dTexture1.msg" ":defaultRenderUtilityList1.u" -na
		;
connectAttr "Jessica_Take_2_2_place2dTexture1.msg" ":defaultRenderUtilityList1.u"
		 -na;
connectAttr "Jessica_Take_2_2_place2dTexture2.msg" ":defaultRenderUtilityList1.u"
		 -na;
connectAttr "Jessica_Take_2_2_place2dTexture3.msg" ":defaultRenderUtilityList1.u"
		 -na;
connectAttr "Jessica_Take_2_2_place2dTexture4.msg" ":defaultRenderUtilityList1.u"
		 -na;
connectAttr "Jessica_Take_2_2_place2dTexture5.msg" ":defaultRenderUtilityList1.u"
		 -na;
connectAttr "Jessica_Take_2_2_place2dTexture6.msg" ":defaultRenderUtilityList1.u"
		 -na;
connectAttr "Candice_Take_1_place2dTexture1.msg" ":defaultRenderUtilityList1.u" 
		-na;
connectAttr "Candice_Take_1_place2dTexture2.msg" ":defaultRenderUtilityList1.u" 
		-na;
connectAttr "Candice_Take_1_place2dTexture3.msg" ":defaultRenderUtilityList1.u" 
		-na;
connectAttr "Candice_Take_1_place2dTexture4.msg" ":defaultRenderUtilityList1.u" 
		-na;
connectAttr "Candice_Take_1_place2dTexture5.msg" ":defaultRenderUtilityList1.u" 
		-na;
connectAttr "Candice_Take_1_place2dTexture6.msg" ":defaultRenderUtilityList1.u" 
		-na;
connectAttr "Candice_Take_1_place2dTexture7.msg" ":defaultRenderUtilityList1.u" 
		-na;
connectAttr "Candice_Take_1_place2dTexture8.msg" ":defaultRenderUtilityList1.u" 
		-na;
connectAttr "Animations_v3_place2dTexture2.msg" ":defaultRenderUtilityList1.u" -na
		;
connectAttr "Kuan_take_1_place2dTexture1.msg" ":defaultRenderUtilityList1.u" -na
		;
connectAttr "Kuan_take_1_place2dTexture2.msg" ":defaultRenderUtilityList1.u" -na
		;
connectAttr "Kuan_take_1_place2dTexture3.msg" ":defaultRenderUtilityList1.u" -na
		;
connectAttr "Kuan_take_1_place2dTexture4.msg" ":defaultRenderUtilityList1.u" -na
		;
connectAttr "Kuan_take_1_place2dTexture5.msg" ":defaultRenderUtilityList1.u" -na
		;
connectAttr "Kuan_take_1_place2dTexture6.msg" ":defaultRenderUtilityList1.u" -na
		;
connectAttr "Kuan_take_1_place2dTexture7.msg" ":defaultRenderUtilityList1.u" -na
		;
connectAttr "Candice_take_2_place2dTexture1.msg" ":defaultRenderUtilityList1.u" 
		-na;
connectAttr "defaultRenderLayer.msg" ":defaultRenderingList1.r" -na;
// End of Animation v6 - Sit Talk 3.ma
