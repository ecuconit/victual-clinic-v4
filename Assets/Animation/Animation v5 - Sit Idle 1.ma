//Maya ASCII 2014 scene
//Name: Animation v5 - Sit Idle 1.ma
//Last modified: Tue, Apr 14, 2015 07:15:20 AM
//Codeset: 1252
requires maya "2014";
currentUnit -l centimeter -a degree -t film;
fileInfo "application" "maya";
fileInfo "product" "Maya 2014";
fileInfo "version" "2014 x64";
fileInfo "cutIdentifier" "201303010241-864206";
fileInfo "osv" "Microsoft Windows 7 Enterprise Edition, 64-bit Windows 7 Service Pack 1 (Build 7601)\n";
fileInfo "license" "education";
createNode transform -n "Sit_Idle_1";
	setAttr ".v" no;
createNode joint -n "Bip01_Spine" -p "Sit_Idle_1";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".s" -type "double3" 0.99999999999999989 1 1 ;
	setAttr ".jo" -type "double3" 90.000171000000023 0 89.999999999999986 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" 0.013812997692949969 0.99941897817840208 -0.031159383042241019 0
		 0.093875120227494585 0.02972852543556026 0.99514002862798212 0 0.99548815306713123 -0.016670957748905309 -0.093409936680880934 0
		 -6.7139604917915256e-006 99.443700000000007 -2.2495999999899592 1;
	setAttr -k on ".liw";
	setAttr ".fbxID" 5;
createNode joint -n "Bip01_Spine1" -p "|Sit_Idle_1|Bip01_Spine";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".v" no;
	setAttr ".t" -type "double3" 13.3984 -0.0107 0 ;
	setAttr ".s" -type "double3" 1 0.99999999999999989 1 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" -0.0073797755917797293 -0.022484114022877283 0.99971996255392492 0
		 -0.014444622269562606 -0.99964048138672645 -0.022588954389337175 0 0.99986843724547314 -0.014607278648678978 0.0070523478909664643 0
		 9.9999999999999995e-007 2.2603360000000001 112.84212599999999 1;
	setAttr -k on ".liw";
	setAttr ".fbxID" 5;
createNode joint -n "Bip01_Spine2" -p "|Sit_Idle_1|Bip01_Spine|Bip01_Spine1";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".v" no;
	setAttr ".t" -type "double3" 13.3983 -0.0107 0 ;
	setAttr ".s" -type "double3" 1 0.99999999999999989 0.99999999999999989 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" 0 0 1 0 0 -1 0 0 1 0 0 0 -3.9999999999999998e-006 -9.6239790000000003 -19.074804 1;
	setAttr -k on ".liw";
	setAttr ".fbxID" 5;
createNode joint -n "Bip01_Neck" -p "|Sit_Idle_1|Bip01_Spine|Bip01_Spine1|Bip01_Spine2";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".v" no;
	setAttr ".t" -type "double3" 13.3985 -0.007899999999999999 0 ;
	setAttr ".r" -type "double3" 1.1848489498583726e-023 -5.9999999999999985e-006 1.1479959999999994 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" 0 -0.020830999999999999 0.99978299999999998 0 0 -0.99978299999999998 -0.020830999999999999 0
		 1 0 0 0 -1.0000000000000001e-005 -9.6266999999999996 -5.6762839999999999 1;
	setAttr -k on ".liw";
	setAttr ".fbxID" 5;
createNode joint -n "Bip01_Head" -p "|Sit_Idle_1|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".v" no;
	setAttr ".t" -type "double3" 9.9829 0 0 ;
	setAttr ".r" -type "double3" 9.4787915988671744e-023 -1.3000000000000004e-005 4.622390999999995 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" 0 -0.10133399999999999 0.99485299999999999 0 0 -0.99485199999999996 -0.10133399999999999 0
		 1 0 0 0 -4.4000000000000012e-005 -9.8346470000000004 4.3044190000000002 1;
	setAttr -k on ".liw";
	setAttr ".fbxID" 5;
createNode joint -n "_notused_Bip01_HeadNub" -p "|Sit_Idle_1|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_Head";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".v" no;
	setAttr ".t" -type "double3" 18.3537 0 0 ;
	setAttr ".ssc" no;
	setAttr ".fbxID" 5;
createNode joint -n "Bip01_R_Clavicle" -p "|Sit_Idle_1|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".v" no;
	setAttr ".t" -type "double3" -0.8268 -0.6612 -3.9165 ;
	setAttr ".s" -type "double3" 1 1.0000000000000002 1 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" -0.99454799999999999 -9.9454800000000002e-007 -0.104286 0
		 -9.8906500000000008e-007 1 -1.0428599999999999e-007 0 0.104286 -5.7179000000000015e-010 -0.99454799999999999 0
		 -3.9165049999999999 2.9251839999999998 138.82619800000001 1;
	setAttr -k on ".liw";
	setAttr ".fbxID" 5;
createNode joint -n "Bip01_R_UpperArm" -p "|Sit_Idle_1|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_R_Clavicle";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".v" no;
	setAttr ".t" -type "double3" 10.6745 0 0 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" -0.75332600000000005 0.10000199999999999 -0.65000000000000002 0
		 0.071782399999999996 0.99496899999999999 0.0698825 0 0.65371900000000005 0.0059857399999999998 -0.75671500000000003 0
		 -14.532831 2.9251239999999998 137.712976 1;
	setAttr -k on ".liw";
	setAttr ".fbxID" 5;
createNode joint -n "Bip01_R_Forearm" -p "|Sit_Idle_1|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_R_Clavicle|Bip01_R_UpperArm";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".v" no;
	setAttr ".t" -type "double3" 25.3375 0 0 ;
	setAttr ".s" -type "double3" 1 0.99999999999999989 1 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" -0.752861 -0.095945600000000006 -0.65114899999999998 0
		 -0.076500899999999997 0.99536800000000003 -0.058215000000000003 0 0.65371900000000005 0.0059856600000000003 -0.756714 0
		 -33.620182 5.4589869999999996 121.243574 1;
	setAttr -k on ".liw";
	setAttr ".fbxID" 5;
createNode joint -n "Bip01_R_Hand" -p "|Sit_Idle_1|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_R_Clavicle|Bip01_R_UpperArm|Bip01_R_Forearm";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".v" no;
	setAttr ".t" -type "double3" 24.9881 0 0 ;
	setAttr ".r" -type "double3" 90.133325000000013 -6.3004020000000009 2.5653330000000008 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" -0.67922700000000003 -0.050331500000000001 -0.73219999999999996 0
		 0.73278100000000002 0.0092545500000000003 -0.68040299999999998 0 0.0410219 -0.99868900000000005 0.030596000000000002 0
		 -52.432737000000003 3.0614050000000002 104.972679 1;
	setAttr -k on ".liw";
	setAttr ".fbxID" 5;
createNode joint -n "Bip01_R_Finger0" -p "|Sit_Idle_1|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_R_Clavicle|Bip01_R_UpperArm|Bip01_R_Forearm|Bip01_R_Hand";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".v" no;
	setAttr ".t" -type "double3" 2.3036000000000003 1.3941 2.9222 ;
	setAttr ".r" -type "double3" -81.93018 -23.314181 18.295908 ;
	setAttr ".s" -type "double3" 0.99999999999999989 1 1 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" -0.36474400000000001 -0.436471 -0.82246900000000001 0
		 -0.072261699999999998 0.89392799999999994 -0.44234699999999999 0 0.92829899999999999 -0.10191 -0.357595 0
		 -52.855969000000002 0.0400224 102.426821 1;
	setAttr -k on ".liw";
	setAttr ".fbxID" 5;
createNode joint -n "Bip01_R_Finger01" -p "|Sit_Idle_1|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_R_Clavicle|Bip01_R_UpperArm|Bip01_R_Forearm|Bip01_R_Hand|Bip01_R_Finger0";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".v" no;
	setAttr ".t" -type "double3" 4.0806 0 0 ;
	setAttr ".r" -type "double3" 9.4787915988669464e-023 3.0000000000000005e-006 24.064549 ;
	setAttr ".s" -type "double3" 1 1 0.99999999999999989 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" -0.36250900000000003 -0.034022900000000002 -0.93135900000000005 0
		 0.082749500000000004 0.99421099999999996 -0.068527199999999996 0 0.92830000000000001 -0.101911 -0.357595 0
		 -54.344414999999998 -1.7410159999999999 99.070615000000004 1;
	setAttr -k on ".liw";
	setAttr ".fbxID" 5;
createNode joint -n "_notused_Bip01_R_Finger0Nub" -p "|Sit_Idle_1|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_R_Clavicle|Bip01_R_UpperArm|Bip01_R_Forearm|Bip01_R_Hand|Bip01_R_Finger0|Bip01_R_Finger01";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".v" no;
	setAttr ".t" -type "double3" 3.3413 0 0 ;
	setAttr ".r" -type "double3" 1e-006 0 -180 ;
	setAttr ".ssc" no;
	setAttr ".fbxID" 5;
createNode joint -n "Bip01_R_Finger2" -p "|Sit_Idle_1|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_R_Clavicle|Bip01_R_UpperArm|Bip01_R_Forearm|Bip01_R_Hand";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".v" no;
	setAttr ".t" -type "double3" 7.3709 -0.4816 0.6341 ;
	setAttr ".r" -type "double3" 0.043615000000000029 -0.013407000000000007 17.086613999999997 ;
	setAttr ".s" -type "double3" 0.99999999999999989 0.99999999999999989 0.99999999999999989 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" -0.43393399999999999 -0.045623999999999998 -0.89978899999999995 0
		 0.90003599999999995 0.022875199999999998 -0.43521399999999999 0 0.040439000000000003 -0.99869600000000003 0.031137000000000001 0
		 -57.766043000000003 2.0527600000000001 99.922762000000006 1;
	setAttr -k on ".liw";
	setAttr ".fbxID" 5;
createNode joint -n "Bip01_R_Finger21" -p "|Sit_Idle_1|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_R_Clavicle|Bip01_R_UpperArm|Bip01_R_Forearm|Bip01_R_Hand|Bip01_R_Finger2";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".v" no;
	setAttr ".t" -type "double3" 3.8208 0 0 ;
	setAttr ".r" -type "double3" 0 0 29.946389999999987 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" 0.0732873 -0.028113900000000001 -0.99691399999999997 0
		 0.99648999999999999 0.042598999999999998 0.072054900000000005 0 0.0404418 -0.99869600000000003 0.0311372 0
		 -59.424045999999997 1.8784400000000001 96.484806000000006 1;
	setAttr -k on ".liw";
	setAttr ".fbxID" 5;
createNode joint -n "_notused_Bip01_R_Finger2Nub" -p "|Sit_Idle_1|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_R_Clavicle|Bip01_R_UpperArm|Bip01_R_Forearm|Bip01_R_Hand|Bip01_R_Finger2|Bip01_R_Finger21";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".v" no;
	setAttr ".t" -type "double3" 4.7174000000000005 0 0 ;
	setAttr ".r" -type "double3" 0 0 180 ;
	setAttr ".ssc" no;
	setAttr ".fbxID" 5;
createNode joint -n "Bip01_R_Finger1" -p "|Sit_Idle_1|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_R_Clavicle|Bip01_R_UpperArm|Bip01_R_Forearm|Bip01_R_Hand";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".v" no;
	setAttr ".t" -type "double3" 7.3263 -0.211 2.2784 ;
	setAttr ".r" -type "double3" -0.73310399999999953 -5.3145149999999983 22.646732999999994 ;
	setAttr ".s" -type "double3" 1 1 0.99999999999999989 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" -0.33941900000000003 -0.13520499999999999 -0.93086800000000003 0
		 0.93680799999999997 0.040593299999999999 -0.34748099999999998 0 0.084767999999999996 -0.98998600000000003 0.112883 0
		 -57.470132999999997 0.41539700000000002 99.821579 1;
	setAttr -k on ".liw";
	setAttr ".fbxID" 5;
createNode joint -n "Bip01_R_Finger11" -p "|Sit_Idle_1|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_R_Clavicle|Bip01_R_UpperArm|Bip01_R_Forearm|Bip01_R_Hand|Bip01_R_Finger1";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".v" no;
	setAttr ".t" -type "double3" 3.5597999999999996 0 0 ;
	setAttr ".r" -type "double3" -2e-006 3.999999999999999e-006 22.627677999999996 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" 0.047135799999999999 -0.10918 -0.99290400000000001 0
		 0.995286 0.089482699999999998 0.037409400000000002 0 0.084763400000000003 -0.98998600000000003 0.112883 0
		 -58.678344000000003 -0.065903900000000001 96.507863999999998 1;
	setAttr -k on ".liw";
	setAttr ".fbxID" 5;
createNode joint -n "_notused_Bip01_R_Finger1Nub" -p "|Sit_Idle_1|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_R_Clavicle|Bip01_R_UpperArm|Bip01_R_Forearm|Bip01_R_Hand|Bip01_R_Finger1|Bip01_R_Finger11";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".v" no;
	setAttr ".t" -type "double3" 4.1404000000000005 0 0 ;
	setAttr ".r" -type "double3" 0 0 180 ;
	setAttr ".ssc" no;
	setAttr ".fbxID" 5;
createNode joint -n "Bip01_R_Finger3" -p "|Sit_Idle_1|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_R_Clavicle|Bip01_R_UpperArm|Bip01_R_Forearm|Bip01_R_Hand";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".v" no;
	setAttr ".t" -type "double3" 7.2345 -0.5997 -0.9332 ;
	setAttr ".r" -type "double3" 0.043363999999999805 3.826455 18.509659999999997 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" -0.41327900000000001 0.0219578 -0.91034000000000004 0
		 0.91051400000000005 0.0239971 -0.41277999999999998 0 0.0127817 -0.999471 -0.0299104 0
		 -57.824306 3.6237469999999998 100.05504999999999 1;
	setAttr -k on ".liw";
	setAttr ".fbxID" 5;
createNode joint -n "Bip01_R_Finger31" -p "|Sit_Idle_1|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_R_Clavicle|Bip01_R_UpperArm|Bip01_R_Forearm|Bip01_R_Hand|Bip01_R_Finger3";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".v" no;
	setAttr ".t" -type "double3" 3.7241 0 0 ;
	setAttr ".r" -type "double3" 3.0000000000000013e-006 3.0000000000000005e-006 27.984287000000009 ;
	setAttr ".s" -type "double3" 1 0.99999999999999989 0.99999999999999989 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" 0.0622821 0.0306512 -0.99758800000000003 0 0.99797599999999997 0.0108983 0.062641199999999994 0
		 0.012792100000000001 -0.999471 -0.0299104 0 -59.363360999999998 3.705527 96.664748000000003 1;
	setAttr -k on ".liw";
	setAttr ".fbxID" 5;
createNode joint -n "_notused_Bip01_R_Finger3Nub" -p "|Sit_Idle_1|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_R_Clavicle|Bip01_R_UpperArm|Bip01_R_Forearm|Bip01_R_Hand|Bip01_R_Finger3|Bip01_R_Finger31";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".v" no;
	setAttr ".t" -type "double3" 4.2397 0 0 ;
	setAttr ".r" -type "double3" 3e-006 -5e-006 180 ;
	setAttr ".ssc" no;
	setAttr ".fbxID" 5;
createNode joint -n "Bip01_R_Finger4" -p "|Sit_Idle_1|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_R_Clavicle|Bip01_R_UpperArm|Bip01_R_Forearm|Bip01_R_Hand";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".v" no;
	setAttr ".t" -type "double3" 7.2292 -0.4187 -2.3639 ;
	setAttr ".r" -type "double3" -0.17713400000000007 5.5619560000000012 16.615208 ;
	setAttr ".s" -type "double3" 0.99999999999999989 0.99999999999999989 1 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" -0.44323200000000001 0.051426199999999998 -0.89493100000000003 0
		 0.89640699999999995 0.026346700000000001 -0.44244899999999998 0 0.00082491599999999997 -0.99832900000000002 -0.057776500000000001 0
		 -57.746822000000002 5.0544450000000003 99.892002000000005 1;
	setAttr -k on ".liw";
	setAttr ".fbxID" 5;
createNode joint -n "Bip01_R_Finger41" -p "|Sit_Idle_1|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_R_Clavicle|Bip01_R_UpperArm|Bip01_R_Forearm|Bip01_R_Hand|Bip01_R_Finger4";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".v" no;
	setAttr ".t" -type "double3" 3.4791000000000003 0 0 ;
	setAttr ".r" -type "double3" -1.9999999999999995e-006 -3.0000000000000005e-006 29.444368 ;
	setAttr ".s" -type "double3" 1 0.99999999999999989 1 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" 0.054672699999999998 0.057735700000000001 -0.996834 0
		 0.99850399999999995 -0.0023359700000000001 0.054628999999999997 0 0.00082546799999999999 -0.99832900000000002 -0.057777000000000002 0
		 -59.288860999999997 5.2333790000000002 96.778379000000001 1;
	setAttr -k on ".liw";
	setAttr ".fbxID" 5;
createNode joint -n "_notused_Bip01_R_Finger4Nub" -p "|Sit_Idle_1|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_R_Clavicle|Bip01_R_UpperArm|Bip01_R_Forearm|Bip01_R_Hand|Bip01_R_Finger4|Bip01_R_Finger41";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".v" no;
	setAttr ".t" -type "double3" 3.3974 0 0 ;
	setAttr ".r" -type "double3" 0 0 -180 ;
	setAttr ".ssc" no;
	setAttr ".fbxID" 5;
createNode joint -n "Bip01_L_Clavicle" -p "|Sit_Idle_1|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".v" no;
	setAttr ".t" -type "double3" -0.8268 -0.6612 3.9165 ;
	setAttr ".s" -type "double3" 1.0000000000000002 1.0000000000000002 1 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" 0.99454799999999999 9.9454800000000002e-007 -0.104286 0
		 -9.8906500000000008e-007 1 1.0428599999999999e-007 0 0.104286 -5.7179000000000015e-010 0.99454799999999999 0
		 3.9165070000000002 2.9252039999999999 138.82619700000001 1;
	setAttr -k on ".liw";
	setAttr ".fbxID" 5;
createNode joint -n "Bip01_L_UpperArm" -p "|Sit_Idle_1|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_L_Clavicle";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".v" no;
	setAttr ".t" -type "double3" 10.6745 0 0 ;
	setAttr ".s" -type "double3" 0.99999999999999989 0.99999999999999989 0.99999999999999989 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" 0.75332500000000002 0.100004 -0.65000000000000002 0
		 -0.071785600000000005 0.99496899999999999 0.069882299999999994 0 0.65371900000000005 -0.0059834299999999997 0.756714 0
		 14.532825000000001 2.925173 137.712974 1;
	setAttr -k on ".liw";
	setAttr ".fbxID" 5;
createNode joint -n "Bip01_L_Forearm" -p "|Sit_Idle_1|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_L_Clavicle|Bip01_L_UpperArm";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".v" no;
	setAttr ".t" -type "double3" 25.3374 0 0 ;
	setAttr ".s" -type "double3" 0.99999999999999989 0.99999999999999989 1 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" 0.752861 -0.095943299999999995 -0.65114899999999998 0
		 0.076497700000000002 0.99536899999999995 -0.058215299999999998 0 0.65371900000000005 -0.0059834199999999997 0.756714 0
		 33.620189000000003 5.4590990000000001 121.243576 1;
	setAttr -k on ".liw";
	setAttr ".fbxID" 5;
createNode joint -n "Bip01_L_Hand" -p "|Sit_Idle_1|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_L_Clavicle|Bip01_L_UpperArm|Bip01_L_Forearm";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".v" no;
	setAttr ".t" -type "double3" 24.9881 0 0 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" 0.67922800000000005 -0.050329499999999999 -0.73219999999999996 0
		 -0.73278100000000002 0.0092525000000000003 -0.68040299999999998 0 0.041019 0.99868999999999997 -0.030595899999999999 0
		 52.432744999999997 3.0615610000000002 104.972662 1;
	setAttr -k on ".liw";
	setAttr ".fbxID" 5;
createNode joint -n "Bip01_L_Finger0" -p "|Sit_Idle_1|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_L_Clavicle|Bip01_L_UpperArm|Bip01_L_Forearm|Bip01_L_Hand";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".v" no;
	setAttr ".t" -type "double3" 2.3036000000000003 1.3941 -2.9222 ;
	setAttr ".r" -type "double3" 81.930180000000021 23.314181000000019 18.295909 ;
	setAttr ".s" -type "double3" 0.99999999999999989 1 1.0000000000000002 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" 0.36474600000000001 -0.43647000000000002 -0.82246900000000001 0
		 0.072258799999999998 0.89392799999999994 -0.44234699999999999 0 0.92830000000000001 0.101913 0.357595 0
		 52.856034999999999 0.0402022 102.42681 1;
	setAttr -k on ".liw";
	setAttr ".fbxID" 5;
createNode joint -n "Bip01_L_Finger01" -p "|Sit_Idle_1|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_L_Clavicle|Bip01_L_UpperArm|Bip01_L_Forearm|Bip01_L_Hand|Bip01_L_Finger0";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".v" no;
	setAttr ".t" -type "double3" 4.0806 0 0 ;
	setAttr ".r" -type "double3" 0 0 24.06455200000001 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" 0.36250900000000003 -0.034021900000000001 -0.93135900000000005 0
		 -0.082751900000000003 0.99421199999999998 -0.068527000000000005 0 0.92829899999999999 0.101913 0.357595 0
		 54.344436000000002 -1.7408459999999999 99.070606999999995 1;
	setAttr -k on ".liw";
	setAttr ".fbxID" 5;
createNode joint -n "_notused_Bip01_L_Finger0Nub" -p "|Sit_Idle_1|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_L_Clavicle|Bip01_L_UpperArm|Bip01_L_Forearm|Bip01_L_Hand|Bip01_L_Finger0|Bip01_L_Finger01";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".v" no;
	setAttr ".t" -type "double3" 3.3413 0 0 ;
	setAttr ".ssc" no;
	setAttr ".fbxID" 5;
createNode joint -n "Bip01_L_Finger2" -p "|Sit_Idle_1|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_L_Clavicle|Bip01_L_UpperArm|Bip01_L_Forearm|Bip01_L_Hand";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".v" no;
	setAttr ".t" -type "double3" 7.3709 -0.4816 -0.6341 ;
	setAttr ".r" -type "double3" -0.043610000000000031 0.01341 17.086619000000006 ;
	setAttr ".s" -type "double3" 1 0.99999999999999989 0.99999999999999989 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" 0.43393500000000002 -0.045622700000000002 -0.89978899999999995 0
		 -0.90003599999999995 0.022871599999999999 -0.43521399999999999 0 0.040435199999999998 0.99869699999999995 -0.031137399999999999 0
		 57.766060000000003 2.0529269999999999 99.922754999999995 1;
	setAttr -k on ".liw";
	setAttr ".fbxID" 5;
createNode joint -n "Bip01_L_Finger21" -p "|Sit_Idle_1|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_L_Clavicle|Bip01_L_UpperArm|Bip01_L_Forearm|Bip01_L_Hand|Bip01_L_Finger2";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".v" no;
	setAttr ".t" -type "double3" 3.8208 0 0 ;
	setAttr ".r" -type "double3" -4.0000000000000024e-006 -4.0000000000000015e-006 29.946390000000019 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" -0.0732873 -0.0281141 -0.99691399999999997 0 -0.99649100000000002 0.042592999999999999 0.072054999999999994 0
		 0.040435899999999997 0.99869699999999995 -0.031137000000000001 0 59.424058000000002 1.878614 96.484800000000007 1;
	setAttr -k on ".liw";
	setAttr ".fbxID" 5;
createNode joint -n "_notused_Bip01_L_Finger2Nub" -p "|Sit_Idle_1|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_L_Clavicle|Bip01_L_UpperArm|Bip01_L_Forearm|Bip01_L_Hand|Bip01_L_Finger2|Bip01_L_Finger21";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".v" no;
	setAttr ".t" -type "double3" 4.7174000000000005 0 0 ;
	setAttr ".ssc" no;
	setAttr ".fbxID" 5;
createNode joint -n "Bip01_L_Finger1" -p "|Sit_Idle_1|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_L_Clavicle|Bip01_L_UpperArm|Bip01_L_Forearm|Bip01_L_Hand";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".v" no;
	setAttr ".t" -type "double3" 7.3263 -0.211 -2.2784 ;
	setAttr ".r" -type "double3" 0.73310799999999965 5.3145129999999989 22.646742999999997 ;
	setAttr ".s" -type "double3" 1 0.99999999999999989 0.99999999999999989 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" 0.33942 -0.13520399999999999 -0.93086800000000003 0
		 -0.93680799999999997 0.040590800000000003 -0.34748099999999998 0 0.084765400000000005 0.98998600000000003 -0.112883 0
		 57.470143999999998 0.415574 99.821611000000004 1;
	setAttr -k on ".liw";
	setAttr ".fbxID" 5;
createNode joint -n "Bip01_L_Finger11" -p "|Sit_Idle_1|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_L_Clavicle|Bip01_L_UpperArm|Bip01_L_Forearm|Bip01_L_Hand|Bip01_L_Finger1";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".v" no;
	setAttr ".t" -type "double3" 3.5597999999999996 0 0 ;
	setAttr ".r" -type "double3" -2.0000000000000012e-006 3.0000000000000005e-006 22.627677000000009 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" -0.047135499999999997 -0.10918 -0.99290400000000001 0
		 -0.99528499999999998 0.089479600000000006 0.037409299999999999 0 0.0847604 0.98998699999999995 -0.112883 0
		 58.678319000000002 -0.065726099999999996 96.507845000000003 1;
	setAttr -k on ".liw";
	setAttr ".fbxID" 5;
createNode joint -n "_notused_Bip01_L_Finger1Nub" -p "|Sit_Idle_1|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_L_Clavicle|Bip01_L_UpperArm|Bip01_L_Forearm|Bip01_L_Hand|Bip01_L_Finger1|Bip01_L_Finger11";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".v" no;
	setAttr ".t" -type "double3" 4.1404000000000005 0 0 ;
	setAttr ".ssc" no;
	setAttr ".fbxID" 5;
createNode joint -n "Bip01_L_Finger3" -p "|Sit_Idle_1|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_L_Clavicle|Bip01_L_UpperArm|Bip01_L_Forearm|Bip01_L_Hand";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".v" no;
	setAttr ".t" -type "double3" 7.2345 -0.5997 0.9332 ;
	setAttr ".r" -type "double3" -0.043362000000000123 -3.826458 18.509666999999997 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" 0.41327900000000001 0.021958700000000001 -0.91034000000000004 0
		 -0.91051400000000005 0.0239966 -0.41277999999999998 0 0.012781000000000001 0.999471 0.029911 0
		 57.824314000000001 3.6238670000000002 100.055049 1;
	setAttr -k on ".liw";
	setAttr ".fbxID" 5;
createNode joint -n "Bip01_L_Finger31" -p "|Sit_Idle_1|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_L_Clavicle|Bip01_L_UpperArm|Bip01_L_Forearm|Bip01_L_Hand|Bip01_L_Finger3";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".v" no;
	setAttr ".t" -type "double3" 3.7241 0 0 ;
	setAttr ".r" -type "double3" 2e-006 -9.4787915988669346e-023 27.984288 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" -0.062282200000000003 0.030651100000000001 -0.99758800000000003 0
		 -0.99797599999999997 0.0108933 0.062641199999999994 0 0.012787099999999999 0.999471 0.029910599999999999 0
		 59.363363 3.7056490000000002 96.664751999999993 1;
	setAttr -k on ".liw";
	setAttr ".fbxID" 5;
createNode joint -n "_notused_Bip01_L_Finger3Nub" -p "|Sit_Idle_1|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_L_Clavicle|Bip01_L_UpperArm|Bip01_L_Forearm|Bip01_L_Hand|Bip01_L_Finger3|Bip01_L_Finger31";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".v" no;
	setAttr ".t" -type "double3" 4.2397 0 0 ;
	setAttr ".ssc" no;
	setAttr ".fbxID" 5;
createNode joint -n "Bip01_L_Finger4" -p "|Sit_Idle_1|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_L_Clavicle|Bip01_L_UpperArm|Bip01_L_Forearm|Bip01_L_Hand";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".v" no;
	setAttr ".t" -type "double3" 7.2292 -0.4187 2.3639 ;
	setAttr ".r" -type "double3" 0.17713199999999998 -5.5619539999999983 16.615218000000002 ;
	setAttr ".s" -type "double3" 1 0.99999999999999989 1 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" 0.44323200000000001 0.051427599999999997 -0.89493100000000003 0
		 -0.89640600000000004 0.026343800000000001 -0.44244899999999998 0 0.00082181400000000003 0.99832900000000002 0.057776399999999999 0
		 57.746775 5.054627 99.891990000000007 1;
	setAttr -k on ".liw";
	setAttr ".fbxID" 5;
createNode joint -n "Bip01_L_Finger41" -p "|Sit_Idle_1|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_L_Clavicle|Bip01_L_UpperArm|Bip01_L_Forearm|Bip01_L_Hand|Bip01_L_Finger4";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".v" no;
	setAttr ".t" -type "double3" 3.4791000000000003 0 0 ;
	setAttr ".r" -type "double3" 2e-006 -3.0000000000000009e-006 29.444363999999997 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" -0.054672800000000001 0.057735500000000002 -0.996834 0
		 -0.99850399999999995 -0.00234096 0.054628799999999998 0 0.00082047800000000003 0.99832900000000002 0.057777200000000001 0
		 59.288862000000002 5.2335589999999996 96.778389000000004 1;
	setAttr -k on ".liw";
	setAttr ".fbxID" 5;
createNode joint -n "_notused_Bip01_L_Finger4Nub" -p "|Sit_Idle_1|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_L_Clavicle|Bip01_L_UpperArm|Bip01_L_Forearm|Bip01_L_Hand|Bip01_L_Finger4|Bip01_L_Finger41";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".v" no;
	setAttr ".t" -type "double3" 3.3974 0 0 ;
	setAttr ".ssc" no;
	setAttr ".fbxID" 5;
createNode joint -n "Bip01_L_Thigh" -p "|Sit_Idle_1|Bip01_Spine";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".v" no;
	setAttr ".t" -type "double3" -11.0456 2.2584 8.5201999999999991 ;
	setAttr ".s" -type "double3" 0.99999999999999989 0.99999999999999989 0.99999999999999989 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" 0.059694200000000003 0.0067868399999999997 -0.998193 0
		 0.0013515700000000001 -0.99997599999999998 -0.00671814 0 -0.99821599999999999 -0.00094809699999999996 -0.059701999999999998 0
		 8.5202019999999994 4.3064900000000002e-005 88.396292000000003 1;
	setAttr -k on ".liw";
	setAttr ".fbxID" 5;
createNode joint -n "Bip01_L_Calf" -p "|Sit_Idle_1|Bip01_Spine|Bip01_L_Thigh";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".v" no;
	setAttr ".t" -type "double3" 36.1527 0 0 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" 0.059157700000000001 0.119882 -0.99102400000000002 0
		 0.0080950799999999993 -0.99278699999999998 -0.119612 0 -0.99821599999999999 -0.00094644599999999998 -0.059701400000000002 0
		 10.678299000000001 0.245397 52.308965999999998 1;
	setAttr -k on ".liw";
	setAttr ".fbxID" 5;
createNode joint -n "Bip01_L_Foot" -p "|Sit_Idle_1|Bip01_Spine|Bip01_L_Thigh|Bip01_L_Calf";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".v" no;
	setAttr ".t" -type "double3" 43.2543 0 0 ;
	setAttr ".s" -type "double3" 1 1 0.99999999999999989 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" 0 0 -1 0 0.123471 -0.99234800000000001 0 0 -0.99234800000000001 -0.123471 0 0
		 13.237131 5.4308170000000002 9.4429110000000005 1;
	setAttr -k on ".liw";
	setAttr ".fbxID" 5;
createNode joint -n "Bip01_L_Toe0" -p "|Sit_Idle_1|Bip01_Spine|Bip01_L_Thigh|Bip01_L_Calf|Bip01_L_Foot";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".v" no;
	setAttr ".t" -type "double3" 9.4714999999999989 11.2377 0 ;
	setAttr ".r" -type "double3" -5.000000000000003e-006 1.9999999999999995e-006 89.999979999999979 ;
	setAttr ".s" -type "double3" 0.99999999999999989 1 1 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" 0.123471 -0.99234800000000001 0 0 0 0 1 0 -0.99234800000000001 -0.123471 0 0
		 14.624665999999999 -5.7209120000000002 -0.028559000000000001 1;
	setAttr -k on ".liw";
	setAttr ".fbxID" 5;
createNode joint -n "_notused_Bip01_L_Toe0Nub" -p "|Sit_Idle_1|Bip01_Spine|Bip01_L_Thigh|Bip01_L_Calf|Bip01_L_Foot|Bip01_L_Toe0";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".v" no;
	setAttr ".t" -type "double3" 7.2685 0 0 ;
	setAttr ".r" -type "double3" 0 0 180 ;
	setAttr ".ssc" no;
	setAttr ".fbxID" 5;
createNode joint -n "Bip01_R_Thigh" -p "|Sit_Idle_1|Bip01_Spine";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".v" no;
	setAttr ".t" -type "double3" -11.0456 2.2585 -8.5201999999999991 ;
	setAttr ".s" -type "double3" 0.99999999999999989 0.99999999999999989 0.99999999999999989 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" -0.059694200000000003 0.0067826700000000002 -0.998193 0
		 -0.00134545 -0.99997599999999998 -0.0067143300000000001 0 -0.99821599999999999 0.00094221699999999997 0.059701900000000002 0
		 -8.5201879999999992 1.40303e-005 88.396298000000002 1;
	setAttr -k on ".liw";
	setAttr ".fbxID" 5;
createNode joint -n "Bip01_R_Calf" -p "|Sit_Idle_1|Bip01_Spine|Bip01_R_Thigh";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".v" no;
	setAttr ".t" -type "double3" 36.1527 0 0 ;
	setAttr ".s" -type "double3" 0.99999999999999989 0.99999999999999989 1 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" -0.059158000000000002 0.11988699999999999 -0.99102400000000002 0
		 -0.0080972100000000005 -0.99278699999999998 -0.119617 0 -0.99821599999999999 0.00094822899999999998 0.059701999999999998 0
		 -10.678288 0.24518599999999999 52.308996999999998 1;
	setAttr -k on ".liw";
	setAttr ".fbxID" 5;
createNode joint -n "Bip01_R_Foot" -p "|Sit_Idle_1|Bip01_Spine|Bip01_R_Thigh|Bip01_R_Calf";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".v" no;
	setAttr ".t" -type "double3" 43.2543 0 0 ;
	setAttr ".s" -type "double3" 1 0.99999999999999989 1 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" 0 0 -1 0 -0.123469 -0.99234800000000001 0 0 -0.99234800000000001 0.123469 0 0
		 -13.237124 5.4308209999999999 9.442933 1;
	setAttr -k on ".liw";
	setAttr ".fbxID" 5;
createNode joint -n "Bip01_R_Toe0" -p "|Sit_Idle_1|Bip01_Spine|Bip01_R_Thigh|Bip01_R_Calf|Bip01_R_Foot";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".v" no;
	setAttr ".t" -type "double3" 9.4714999999999989 11.2377 0 ;
	setAttr ".r" -type "double3" 1.9999999999999991e-006 -1.9999999999999995e-006 89.999986 ;
	setAttr ".s" -type "double3" 0.99999999999999989 0.99999999999999989 1 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" -0.12346799999999999 -0.99234800000000001 0 0 0 0 1 0
		 -0.99234800000000001 0.12346799999999999 0 0 -14.624625999999999 -5.7209019999999997 -0.028538999999999998 1;
	setAttr -k on ".liw";
	setAttr ".fbxID" 5;
createNode joint -n "_notused_Bip01_R_Toe0Nub" -p "|Sit_Idle_1|Bip01_Spine|Bip01_R_Thigh|Bip01_R_Calf|Bip01_R_Foot|Bip01_R_Toe0";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".v" no;
	setAttr ".t" -type "double3" 7.2685 0 0 ;
	setAttr ".ssc" no;
	setAttr ".fbxID" 5;
createNode animCurveTL -n "Bip01_Spine_translateX1";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 67 ".ktv[0:66]"  1.052 -1.6584634780883789 2.128 -1.6983990669250488
		 3.204 -1.7332481145858765 4.28 -1.7617931365966797 5.356 -1.7856494188308716 6.432 -1.8064171075820925
		 7.508 -1.8215579986572263 8.584 -1.830882787704468 9.66 -1.8380799293518064 10.736 -1.849793553352356
		 11.812 -1.8726433515548704 12.888 -1.8993597030639648 13.968 -1.9186168909072876
		 15.044 -1.9331148862838743 16.12 -1.9463307857513428 17.196 -1.9570440053939819 18.272 -1.9666367769241333
		 19.348 -1.9717299938201904 20.424 -1.9643265008926392 21.5 -1.9484264850616455 22.576 -1.9378517866134644
		 23.652 -1.9356561899185181 24.728 -1.9311521053314209 25.804 -1.912702798843384 26.88 -1.8832156658172607
		 27.956 -1.8502771854400637 29.032 -1.8095369338989256 30.108 -1.7675340175628662
		 31.184 -1.7348998785018921 32.26 -1.7025973796844482 33.34 -1.6664917469024658 34.416 -1.6327511072158811
		 35.492 -1.6032445430755615 36.568 -1.5792183876037598 37.644 -1.5595171451568604
		 38.72 -1.5394725799560549 39.796 -1.5172320604324341 40.872 -1.4911402463912964 41.948 -1.4662997722625732
		 43.024 -1.4362655878067017 44.1 -1.4087494611740112 45.176 -1.3880347013473513 46.252 -1.3639745712280271
		 47.328 -1.3308076858520508 48.404 -1.2948219776153564 49.48 -1.2663805484771729 50.556 -1.2489063739776611
		 51.636 -1.2345830202102659 52.712 -1.2165117263793943 53.788 -1.1899330615997314
		 54.864 -1.1523127555847168 55.94 -1.1170979738235474 57.016 -1.0952796936035156 58.092 -1.0776379108428955
		 59.168 -1.0631921291351318 60.244 -1.0580097436904907 61.32 -1.0502815246582031 62.396 -1.0321768522262571
		 63.472 -1.0160094499588013 64.548 -1.0221242904663086 65.624 -1.0261783599853516
		 66.7 -1.037258505821228 67.776 -1.0558402538299561 68.852 -1.0741645097732544 69.932 -1.09833824634552
		 71.008 -1.1568280458450315 72.084 -1.2526799440383911;
createNode animCurveTL -n "Bip01_Spine_translateY1";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 67 ".ktv[0:66]"  1.052 68.799858093261719 2.128 68.713005065917969
		 3.204 68.617134094238281 4.28 68.510635375976562 5.356 68.387718200683594 6.432 68.253273010253906
		 7.508 68.121063232421875 8.584 67.996170043945313 9.66 67.8817138671875 10.736 67.780868530273438
		 11.812 67.692520141601563 12.888 67.613601684570313 13.968 67.536781311035156 15.044 67.462867736816406
		 16.12 67.404624938964844 17.196 67.366546630859375 18.272 67.338226318359375 19.348 67.311454772949219
		 20.424 67.2930908203125 21.5 67.286453247070313 22.576 67.28497314453125 23.652 67.292411804199219
		 24.728 67.309577941894531 25.804 67.323822021484375 26.88 67.335365295410156 27.956 67.354217529296875
		 29.032 67.373527526855469 30.108 67.383026123046875 31.184 67.389472961425781 32.26 67.39947509765625
		 33.34 67.40966796875 34.416 67.415863037109375 35.492 67.416954040527344 36.568 67.414459228515625
		 37.644 67.408500671386719 38.72 67.398002624511719 39.796 67.382919311523437 40.872 67.364105224609375
		 41.948 67.345962524414063 43.024 67.328567504882812 44.1 67.314407348632813 45.176 67.305328369140625
		 46.252 67.298027038574219 47.328 67.287681579589844 48.404 67.273635864257813 49.48 67.267791748046875
		 50.556 67.28253173828125 51.636 67.3087158203125 52.712 67.330978393554688 53.788 67.351806640625
		 54.864 67.378494262695313 55.94 67.409896850585938 57.016 67.45184326171875 58.092 67.511428833007813
		 59.168 67.580360412597656 60.244 67.649932861328125 61.32 67.721214294433594 62.396 67.800071716308594
		 63.472 67.888145446777344 64.548 67.980072021484375 65.624 68.070266723632812 66.7 68.161766052246094
		 67.776 68.249359130859375 68.852 68.328544616699219 69.932 68.403671264648438 71.008 68.478492736816406
		 72.084 68.551078796386719;
createNode animCurveTL -n "Bip01_Spine_translateZ1";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 67 ".ktv[0:66]"  1.052 -63.892299652099609 2.128 -63.894916534423821
		 3.204 -63.900039672851563 4.28 -63.912548065185547 5.356 -63.922882080078125 6.432 -63.926673889160163
		 7.508 -63.931236267089837 8.584 -63.939533233642578 9.66 -63.95233154296875 10.736 -63.970142364501953
		 11.812 -63.987987518310547 12.888 -64.000617980957031 13.968 -64.009025573730469
		 15.044 -64.018051147460938 16.12 -64.02825927734375 17.196 -64.035591125488281 18.272 -64.034805297851563
		 19.348 -64.024932861328125 20.424 -64.014122009277344 21.5 -64.00384521484375 22.576 -63.984073638916016
		 23.652 -63.955451965332031 24.728 -63.927150726318359 25.804 -63.900051116943359
		 26.88 -63.869724273681641 27.956 -63.831314086914063 29.032 -63.785903930664063 30.108 -63.741775512695313
		 31.184 -63.69989013671875 32.26 -63.655364990234375 33.34 -63.608669281005859 34.416 -63.563152313232422
		 35.492 -63.523403167724609 36.568 -63.490161895751953 37.644 -63.456577301025391
		 38.72 -63.420886993408203 39.796 -63.392940521240234 40.872 -63.382293701171875 41.948 -63.375820159912109
		 43.024 -63.370090484619141 44.1 -63.368808746337891 45.176 -63.375167846679688 46.252 -63.386844635009766
		 47.328 -63.402408599853523 48.404 -63.421749114990234 49.48 -63.443042755126953 50.556 -63.463787078857422
		 51.636 -63.480728149414063 52.712 -63.490390777587891 53.788 -63.491813659667969
		 54.864 -63.489616394042969 55.94 -63.493827819824219 57.016 -63.504287719726563 58.092 -63.508174896240234
		 59.168 -63.510597229003906 60.244 -63.530704498291016 61.32 -63.562088012695313 62.396 -63.583847045898438
		 63.472 -63.596195220947266 64.548 -63.602893829345703 65.624 -63.60565185546875 66.7 -63.601837158203125
		 67.776 -63.603832244873047 68.852 -63.614284515380859 69.932 -63.618446350097656
		 71.008 -63.602973937988281 72.084 -63.575168609619141;
createNode animCurveTA -n "Bip01_Spine_rotateX1";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 67 ".ktv[0:66]"  1.052 -0.38823649287223816 2.128 -0.46319016814231873
		 3.204 -0.53943628072738647 4.28 -0.60678559541702271 5.356 -0.66257768869400024 6.432 -0.71628546714782715
		 7.508 -0.77266824245452881 8.584 -0.82981556653976452 9.66 -0.8859894871711731 10.736 -0.93862628936767567
		 11.812 -0.98052012920379639 12.888 -1.0104316473007202 13.968 -1.0355302095413208
		 15.044 -1.0550949573516846 16.12 -1.0632773637771606 17.196 -1.0618990659713743 18.272 -1.0564392805099487
		 19.348 -1.0512617826461792 20.424 -1.0456826686859133 21.5 -1.034096360206604 22.576 -1.0169880390167236
		 23.652 -0.99805182218551636 24.728 -0.97351217269897428 25.804 -0.94149714708328258
		 26.88 -0.90769058465957653 27.956 -0.8712199330329895 29.032 -0.82679480314254772
		 30.108 -0.78070449829101563 31.184 -0.74052327871322621 32.26 -0.70502370595932007
		 33.34 -0.67077982425689697 34.416 -0.63514226675033569 35.492 -0.60382539033889771
		 36.568 -0.58193337917327881 37.644 -0.56113708019256592 38.72 -0.53958457708358765
		 39.796 -0.52786058187484741 40.872 -0.54095560312271118 41.948 -0.54491424560546886
		 43.024 -0.54800081253051758 44.1 -0.55553996562957764 45.176 -0.57536786794662476
		 46.252 -0.60404479503631592 47.328 -0.6321912407875061 48.404 -0.65597027540206909
		 49.48 -0.67621445655822754 50.556 -0.69889438152313232 51.636 -0.72418677806854237
		 52.712 -0.73957103490829468 53.788 -0.74435192346572876 54.864 -0.7472081184387207
		 55.94 -0.7397417426109314 57.016 -0.71741735935211193 58.092 -0.69439172744750977
		 59.168 -0.67239671945571899 60.244 -0.6388547420501709 61.32 -0.59275394678115845
		 62.396 -0.54578053951263428 63.472 -0.50762540102005005 64.548 -0.45560458302497858
		 65.624 -0.38280218839645386 66.7 -0.30510380864143372 67.776 -0.27579119801521301
		 68.852 -0.28828680515289307 69.932 -0.30150792002677917 71.008 -0.3086375892162323
		 72.084 -0.32215660810470587;
createNode animCurveTA -n "Bip01_Spine_rotateY1";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 67 ".ktv[0:66]"  1.052 -4.0541882514953622 2.128 -4.0485372543334961
		 3.204 -4.0398454666137695 4.28 -4.0335555076599121 5.356 -4.024566650390625 6.432 -4.0116872787475586
		 7.508 -4.0071148872375488 8.584 -4.009547233581543 9.66 -4.0053806304931641 10.736 -4.0021252632141113
		 11.812 -4.0147347450256348 12.888 -4.0388040542602539 13.968 -4.0645442008972168
		 15.044 -4.0890254974365234 16.12 -4.112647533416748 17.196 -4.1432657241821289 18.272 -4.1865487098693848
		 19.348 -4.231900691986084 20.424 -4.2646889686584473 21.5 -4.2833700180053711 22.576 -4.2976570129394531
		 23.652 -4.3133649826049805 24.728 -4.3212070465087891 25.804 -4.3090105056762695
		 26.88 -4.2797665596008301 27.956 -4.243072509765625 29.032 -4.2029552459716797 30.108 -4.1635246276855469
		 31.184 -4.122713565826416 32.26 -4.0717835426330566 33.34 -4.0128498077392578 34.416 -3.954099178314209
		 35.492 -3.899709939956665 36.568 -3.8570230007171622 37.644 -3.8290238380432129 38.72 -3.8031964302062984
		 39.796 -3.7781338691711426 40.872 -3.7813341617584233 41.948 -3.7863688468933101
		 43.024 -3.788732528686523 44.1 -3.8018712997436519 45.176 -3.8359441757202148 46.252 -3.8746192455291744
		 47.328 -3.9079587459564222 48.404 -3.944378137588501 49.48 -3.9915246963500977 50.556 -4.0454988479614258
		 51.636 -4.0964107513427734 52.712 -4.1362380981445313 53.788 -4.1644306182861328
		 54.864 -4.187375545501709 55.94 -4.2086515426635742 57.016 -4.2243423461914062 58.092 -4.2289676666259766
		 59.168 -4.2253599166870117 60.244 -4.2211518287658691 61.32 -4.2142715454101563 62.396 -4.2007818222045898
		 63.472 -4.188758373260498 64.548 -4.1412701606750488 65.624 -4.1239104270935059 66.7 -4.1086139678955078
		 67.776 -4.1190876960754395 68.852 -4.147089958190918 69.932 -4.1769752502441406 71.008 -4.2127728462219238
		 72.084 -4.2549962997436523;
createNode animCurveTA -n "Bip01_Spine_rotateZ1";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 67 ".ktv[0:66]"  1.052 -0.48852220177650457 2.128 -0.41347682476043707
		 3.204 -0.33770862221717834 4.28 -0.25918054580688477 5.356 -0.1894146203994751 6.432 -0.13693845272064209
		 7.508 -0.092130400240421295 8.584 -0.03979971632361412 9.66 0.012884372845292091
		 10.736 0.055328242480754845 11.812 0.096751332283020033 12.888 0.14382250607013702
		 13.968 0.19071231782436376 15.044 0.23767322301864621 16.12 0.28934738039970404 17.196 0.33986249566078186
		 18.272 0.38616091012954712 19.348 0.4391949474811554 20.424 0.5032004714012146 21.5 0.56933671236038208
		 22.576 0.63178437948226929 23.652 0.68770569562911987 24.728 0.73540741205215454
		 25.804 0.78431272506713867 26.88 0.83988505601882923 27.956 0.88645052909851074 29.032 0.91840201616287231
		 30.108 0.95131582021713279 31.184 0.98687380552291892 32.26 1.0125055313110352 33.34 1.0243003368377686
		 34.416 1.0268375873565674 35.492 1.0260170698165894 36.568 1.0223346948623655 37.644 1.0077559947967527
		 38.72 0.97964352369308494 39.796 0.94744795560836803 40.872 0.90250015258789063 41.948 0.86234641075134277
		 43.024 0.81311428546905518 44.1 0.75715291500091553 45.176 0.7002187967300415 46.252 0.64319103956222534
		 47.328 0.58061587810516357 48.404 0.51290035247802734 49.48 0.4450002908706665 50.556 0.37660437822341919
		 51.636 0.30420580506324768 52.712 0.23455210030078888 53.788 0.17555619776248932
		 54.864 0.11956319957971571 55.94 0.061883293092250817 57.016 0.0069431262090802193
		 58.092 -0.043180383741855621 59.168 -0.08211158961057663 60.244 -0.10701079666614532
		 61.32 -0.12510250508785248 62.396 -0.14139102399349213 63.472 -0.15472181141376495
		 64.548 -0.15633200109004974 65.624 -0.18401019275188449 66.7 -0.19800290465354919
		 67.776 -0.14223213493824005 68.852 -0.026132155209779739 69.932 0.088051021099090576
		 71.008 0.18474040925502777 72.084 0.29495212435722351;
createNode animCurveTA -n "Bip01_Spine1_rotateX1";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 67 ".ktv[0:66]"  1.052 -0.79856425523757935 2.128 -0.82194525003433228
		 3.204 -0.88015097379684459 4.28 -0.90963053703308094 5.356 -0.94791054725646973 6.432 -0.98965710401535023
		 7.508 -1.0317109823226929 8.584 -1.0731687545776367 9.66 -1.1131293773651123 10.736 -1.1507805585861206
		 11.812 -1.1854095458984375 12.888 -1.215046763420105 13.968 -1.236419677734375 15.044 -1.2728663682937622
		 16.12 -1.2816087007522583 17.196 -1.3015484809875488 18.272 -1.3158416748046875 19.348 -1.3188186883926392
		 20.424 -1.326259970664978 21.5 -1.3305556774139404 22.576 -1.3313133716583252 23.652 -1.3330708742141724
		 24.728 -1.333599328994751 25.804 -1.3340047597885132 26.88 -1.3351951837539673 27.956 -1.3368353843688965
		 29.032 -1.3375366926193235 30.108 -1.3402076959609983 31.184 -1.3410936594009399
		 32.26 -1.343599796295166 33.34 -1.3442126512527466 34.416 -1.3452632427215576 35.492 -1.3451176881790159
		 36.568 -1.3447713851928711 37.644 -1.342446565628052 38.72 -1.3387085199356079 39.796 -1.3373445272445681
		 40.872 -1.3318287134170532 41.948 -1.3255722522735596 43.024 -1.323675274848938 44.1 -1.3172794580459597
		 45.176 -1.311917781829834 46.252 -1.3070873022079468 47.328 -1.3025293350219729 48.404 -1.2981852293014526
		 49.48 -1.2936934232711792 50.556 -1.2883408069610596 51.636 -1.2863005399703979 52.712 -1.2787269353866577
		 53.788 -1.2757545709609983 54.864 -1.2653108835220337 55.94 -1.2610831260681157 57.016 -1.245996356010437
		 58.092 -1.2399669885635378 59.168 -1.2187634706497192 60.244 -1.2105216979980469
		 61.32 -1.1807123422622681 62.396 -1.1640146970748899 63.472 -1.1412301063537598 64.548 -1.1166919469833374
		 65.624 -1.0955461263656616 66.7 -1.0443778038024902 67.776 -1.0208355188369751 68.852 -0.99633800983428955
		 69.932 -0.93446791172027588 71.008 -0.90416258573532082 72.084 -0.86514025926589988;
createNode animCurveTA -n "Bip01_Spine1_rotateY1";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 67 ".ktv[0:66]"  1.052 0.94354438781738303 2.128 0.92794060707092285
		 3.204 0.89382153749465931 4.28 0.87843459844589233 5.356 0.85970437526702892 6.432 0.84065973758697521
		 7.508 0.82293200492858876 8.584 0.80685693025588989 9.66 0.79271394014358532 10.736 0.78071504831314076
		 11.812 0.7709866166114806 12.888 0.76376813650131248 13.968 0.75935757160186768 15.044 0.75448614358901989
		 16.12 0.75380623340606689 17.196 0.7544524073600769 18.272 0.7570871114730835 19.348 0.75816613435745239
		 20.424 0.76245874166488636 21.5 0.76689225435256958 22.576 0.76819527149200428 23.652 0.77205532789230347
		 24.728 0.77306848764419556 25.804 0.77372896671295166 26.88 0.77331185340881348 27.956 0.77089822292327892
		 29.032 0.7695167064666748 30.108 0.76366513967514027 31.184 0.76133513450622559 32.26 0.7533409595489502
		 33.34 0.75089704990386963 34.416 0.74346464872360229 35.492 0.73653674125671387 36.568 0.73475998640060436
		 37.644 0.72971349954605103 38.72 0.72654706239700317 39.796 0.72611075639724731 40.872 0.72646498680114757
		 41.948 0.72936689853668213 43.024 0.73075824975967407 44.1 0.73698651790618896 45.176 0.74395424127578735
		 46.252 0.751928150653839 47.328 0.76084911823272705 48.404 0.77045553922653198 49.48 0.78097832202911377
		 50.556 0.79330676794052124 51.636 0.7973969578742981 52.712 0.81070739030838024 53.788 0.81467372179031383
		 54.864 0.8249726891517638 55.94 0.82778000831604004 57.016 0.83426088094711293 58.092 0.83578985929489147
		 59.168 0.83850586414337147 60.244 0.83888417482376099 61.32 0.83844882249832164 62.396 0.83760625123977661
		 63.472 0.83617764711380005 64.548 0.83448469638824463 65.624 0.83295392990112305
		 66.7 0.82918655872344971 67.776 0.82737332582473755 68.852 0.82539528608322144 69.932 0.81966465711593628
		 71.008 0.81606191396713279 72.084 0.81054258346557617;
createNode animCurveTA -n "Bip01_Spine1_rotateZ1";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 67 ".ktv[0:66]"  1.052 1.0823348760604858 2.128 1.0782616138458252
		 3.204 1.0763645172119141 4.28 1.0783933401107788 5.356 1.0829586982727053 6.432 1.0899876356124878
		 7.508 1.0993293523788452 8.584 1.1107437610626221 9.66 1.123990535736084 10.736 1.1388071775436399
		 11.812 1.1549550294876101 12.888 1.1709821224212646 13.968 1.1843233108520508 15.044 1.2134418487548828
		 16.12 1.2217416763305664 17.196 1.2469936609268188 18.272 1.2722790241241455 19.348 1.2794653177261353
		 20.424 1.3042700290679932 21.5 1.3284595012664795 22.576 1.336241602897644 23.652 1.3632365465164185
		 24.728 1.3745499849319458 25.804 1.3846278190612793 26.88 1.4052749872207642 27.956 1.4233018159866333
		 29.032 1.4287585020065308 30.108 1.446303129196167 31.184 1.4516478776931767 32.26 1.4662139415740969
		 33.34 1.4702091217041016 34.416 1.4812412261962893 35.492 1.4917916059494021 36.568 1.4947940111160278
		 37.644 1.5057936906814575 38.72 1.5179272890090942 39.796 1.5217288732528689 40.872 1.5367068052291872
		 41.948 1.5541563034057615 43.024 1.5597342252731323 44.1 1.5801340341567991 45.176 1.599547266960144
		 46.252 1.6197382211685181 47.328 1.6414848566055298 48.404 1.6645541191101076 49.48 1.6902955770492554
		 50.556 1.7216370105743408 51.636 1.7328263521194458 52.712 1.7715615034103394 53.788 1.7844938039779663
		 54.864 1.823235869407654 55.94 1.8359457254409792 57.016 1.8734217882156372 58.092 1.8854774236679075
		 59.168 1.9201745986938483 60.244 1.9309874773025513 61.32 1.9619536399841315 62.396 1.974694609642029
		 63.472 1.9887025356292731 64.548 2.0007383823394775 65.624 2.0085046291351318 66.7 2.0179882049560547
		 67.776 2.0190510749816895 68.852 2.0185227394104004 69.932 2.0102782249450684 71.008 2.0030868053436279
		 72.084 1.991790890693665;
createNode animCurveTA -n "Bip01_Spine2_rotateX1";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 67 ".ktv[0:66]"  1.052 -2.6008009910583496 2.128 -2.7104134559631348
		 3.204 -2.8309628963470459 4.28 -2.9605386257171631 5.356 -3.0967378616333008 6.432 -3.2367937564849858
		 7.508 -3.3777065277099609 8.584 -3.5164210796356201 9.66 -3.6499345302581792 10.736 -3.7754731178283687
		 11.812 -3.890738964080811 12.888 -3.9939277172088623 13.968 -4.0837969779968262 15.044 -4.1597127914428711
		 16.12 -4.2217206954956055 17.196 -4.2704877853393555 18.272 -4.3071846961975098 19.348 -4.3333797454833984
		 20.424 -4.3508782386779785 21.5 -4.3616747856140137 22.576 -4.3677282333374023 23.652 -4.3708586692810059
		 24.728 -4.3726229667663574 25.804 -4.3742609024047852 26.88 -4.3765840530395508 27.956 -4.380000114440918
		 29.032 -4.3845510482788086 30.108 -4.3899664878845215 31.184 -4.3953804969787598
		 32.26 -4.3995308876037598 33.34 -4.4064702987670898 34.416 -4.4078226089477539 35.492 -4.4081206321716309
		 36.568 -4.4031062126159677 37.644 -4.3975915908813477 38.72 -4.3893318176269531 39.796 -4.3808407783508301
		 40.872 -4.3568544387817383 41.948 -4.3441262245178223 43.024 -4.3273940086364746
		 44.1 -4.3091487884521484 45.176 -4.2908682823181152 46.252 -4.2729558944702157 47.328 -4.2555556297302246
		 48.404 -4.2386331558227539 49.48 -4.2219071388244629 50.556 -4.204914093017579 51.636 -4.1869707107543945
		 52.712 -4.1672797203063974 53.788 -4.1449575424194336 54.864 -4.1190357208251953
		 55.94 -4.0885729789733887 57.016 -4.0530767440795898 58.092 -4.012115478515625 59.168 -3.9638881683349609
		 60.244 -3.9067237377166748 61.32 -3.8413181304931641 62.396 -3.7685580253601079 63.472 -3.6878864765167241
		 64.548 -3.6058709621429439 65.624 -3.5294942855834961 66.7 -3.4383170604705811 67.776 -3.3120553493499756
		 68.852 -3.1718831062316895 69.932 -3.0390279293060303 71.008 -2.9073514938354492
		 72.084 -2.7708477973937988;
createNode animCurveTA -n "Bip01_Spine2_rotateY1";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 67 ".ktv[0:66]"  1.052 3.1979498863220215 2.128 3.130561351776123
		 3.204 3.0633482933044434 4.28 2.9973776340484619 5.356 2.9338135719299316 6.432 2.8737680912017822
		 7.508 2.8183660507202148 8.584 2.7685966491699219 9.66 2.725322961807251 10.736 2.689272403717041
		 11.812 2.6607799530029297 12.888 2.6398439407348633 13.968 2.6262643337249756 15.044 2.6195518970489502
		 16.12 2.6189703941345215 17.196 2.6235110759735107 18.272 2.632030725479126 19.348 2.6432681083679199
		 20.424 2.655935525894165 21.5 2.6687629222869873 22.576 2.680591344833374 23.652 2.6903800964355469
		 24.728 2.6973161697387695 25.804 2.7007682323455811 26.88 2.7003700733184814 27.956 2.6960198879241943
		 29.032 2.687870979309082 30.108 2.6763134002685547 31.184 2.663069486618042 32.26 2.6511631011962891
		 33.34 2.6223232746124268 34.416 2.6101372241973877 35.492 2.5983812808990479 36.568 2.5742623805999756
		 37.644 2.5664546489715576 38.72 2.5595769882202148 39.796 2.5560028553009033 40.872 2.5593130588531494
		 41.948 2.5663602352142334 43.024 2.579279899597168 44.1 2.5975940227508545 45.176 2.6207365989685059
		 46.252 2.648035049438477 47.328 2.6786339282989502 48.404 2.71152663230896 49.48 2.7456097602844243
		 50.556 2.7797179222106938 51.636 2.8127477169036865 52.712 2.8436205387115479 53.788 2.8714230060577393
		 54.864 2.8954763412475586 55.94 2.9152536392211914 57.016 2.9306244850158691 58.092 2.941676139831543
		 59.168 2.9481971263885498 60.244 2.9501125812530522 61.32 2.9483127593994141 62.396 2.9437634944915771
		 63.472 2.9369428157806396 64.548 2.9308249950408936 65.624 2.9283711910247803 66.7 2.9219632148742676
		 67.776 2.9037177562713623 68.852 2.8810355663299561 69.932 2.8610661029815674 71.008 2.8402698040008545
		 72.084 2.8149714469909668;
createNode animCurveTA -n "Bip01_Spine2_rotateZ1";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 67 ".ktv[0:66]"  1.052 3.5582599639892578 2.128 3.5427274703979492
		 3.204 3.5375380516052246 4.28 3.5421082973480225 5.356 3.5557842254638672 6.432 3.5778675079345703
		 7.508 3.6076304912567139 8.584 3.6443030834197998 9.66 3.6871185302734375 10.736 3.7353000640869145
		 11.812 3.7879374027252202 12.888 3.8441450595855726 13.968 3.9031484127044673 15.044 3.9641966819763175
		 16.12 4.0266075134277344 17.196 4.0897469520568848 18.272 4.1530570983886719 19.348 4.2160358428955078
		 20.424 4.2782268524169922 21.5 4.339198112487793 22.576 4.3985738754272461 23.652 4.4559745788574228
		 24.728 4.5110378265380859 25.804 4.5634317398071289 26.88 4.6128191947937012 27.956 4.6589517593383789
		 29.032 4.7016644477844238 30.108 4.7409191131591797 31.184 4.7749514579772949 32.26 4.8003330230712891
		 33.34 4.8494901657104492 34.416 4.8688464164733887 35.492 4.8878059387207031 36.568 4.9324173927307129
		 37.644 4.9545326232910156 38.72 4.9816274642944336 39.796 5.0065979957580566 40.872 5.0696063041687012
		 41.948 5.1039838790893555 43.024 5.1511664390563965 44.1 5.206573486328125 45.176 5.2677440643310547
		 46.252 5.3344125747680664 47.328 5.4060592651367187 48.404 5.482017993927002 49.48 5.5615291595458984
		 50.556 5.6437845230102539 51.636 5.7280116081237802 52.712 5.8134560585021973 53.788 5.8993968963623047
		 54.864 5.9851627349853525 55.94 6.0700440406799316 57.016 6.1533880233764648 58.092 6.2343869209289551
		 59.168 6.3119874000549316 60.244 6.3849577903747559 61.32 6.4522509574890137 62.396 6.5127887725830078
		 63.472 6.5652356147766113 64.548 6.6109614372253427 65.624 6.6514749526977548 66.7 6.6781215667724609
		 67.776 6.6825013160705566 68.852 6.6722187995910645 69.932 6.6552848815917978 71.008 6.6296558380126953
		 72.084 6.5936203002929687;
createNode animCurveTA -n "Bip01_R_Clavicle_rotateX1";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 67 ".ktv[0:66]"  1.052 -0.20389369130134588 2.128 -0.17737889289855957
		 3.204 -0.1669117659330368 4.28 -0.15691666305065155 5.356 -0.13620868325233462 6.432 -0.13087204098701477
		 7.508 -0.11751016974449156 8.584 -0.10753993690013884 9.66 -0.098817020654678372
		 10.736 -0.096450373530387878 11.812 -0.090256229043006897 12.888 -0.088907927274703979
		 13.968 -0.087230250239372253 15.044 -0.087477691471576691 16.12 -0.089492544531822205
		 17.196 -0.093584448099136353 18.272 -0.095171630382537842 19.348 -0.10144892334938048
		 20.424 -0.10777538269758224 21.5 -0.11451179534196854 22.576 -0.12172510474920271
		 23.652 -0.12971815466880798 24.728 -0.13913437724113464 25.804 -0.14199115335941315
		 26.88 -0.15137641131877902 27.956 -0.16004268825054169 29.032 -0.16232304275035858
		 30.108 -0.16900819540023804 31.184 -0.17364434897899628 32.26 -0.17682898044586182
		 33.34 -0.17898869514465332 34.416 -0.1791064590215683 35.492 -0.17803490161895752
		 36.568 -0.17725358903408053 37.644 -0.17318844795227054 38.72 -0.1675676554441452
		 39.796 -0.15911808609962463 40.872 -0.15619587898254397 41.948 -0.14500212669372561
		 43.024 -0.13400653004646301 44.1 -0.12237651646137236 45.176 -0.10970120877027512
		 46.252 -0.095163680613040924 47.328 -0.077065721154212966 48.404 -0.070424832403659821
		 49.48 -0.047284506261348717 50.556 -0.039392087608575821 51.636 -0.015425147488713264
		 52.712 -0.0074081877246499053 53.788 0.01658562570810318 54.864 0.024491805583238602
		 55.94 0.048812579363584518 57.016 0.059256929904222488 58.092 0.069588407874107361
		 59.168 0.092302247881889343 60.244 0.099409274756908417 61.32 0.12030335515737532
		 62.396 0.12716458737850189 63.472 0.14761871099472046 64.548 0.1544610857963562 65.624 0.17623879015445709
		 66.7 0.18707036972045898 67.776 0.20096737146377563 68.852 0.21527823805809021 69.932 0.22711621224880221
		 71.008 0.25421622395515442 72.084 0.26734107732772827;
createNode animCurveTA -n "Bip01_R_Clavicle_rotateY1";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 67 ".ktv[0:66]"  1.052 79.017219543457017 2.128 78.968109130859375
		 3.204 78.948623657226562 4.28 78.930023193359389 5.356 78.891548156738281 6.432 78.881629943847656
		 7.508 78.856781005859375 8.584 78.838203430175781 9.66 78.821914672851563 10.736 78.817604064941406
		 11.812 78.806007385253906 12.888 78.803504943847642 13.968 78.800361633300781 15.044 78.800811767578125
		 16.12 78.804595947265625 17.196 78.812263488769531 18.272 78.815193176269531 19.348 78.826873779296875
		 20.424 78.838684082031264 21.5 78.851188659667969 22.576 78.864593505859375 23.652 78.879417419433594
		 24.728 78.897041320800781 25.804 78.902267456054687 26.88 78.919769287109375 27.956 78.93582916259767
		 29.032 78.940048217773438 30.108 78.952529907226577 31.184 78.961105346679673 32.26 78.966987609863281
		 33.34 78.971000671386719 34.416 78.971206665039077 35.492 78.969245910644531 36.568 78.967849731445327
		 37.644 78.960296630859375 38.72 78.949806213378906 39.796 78.934104919433594 40.872 78.928680419921875
		 41.948 78.907875061035156 43.024 78.887451171875 44.1 78.865837097167969 45.176 78.842208862304688
		 46.252 78.81514739990233 47.328 78.781402587890625 48.404 78.769073486328125 49.48 78.725845336914062
		 50.556 78.711112976074219 51.636 78.666351318359375 52.712 78.651420593261719 53.788 78.606475830078125
		 54.864 78.591667175292983 55.94 78.546134948730469 57.016 78.526535034179673 58.092 78.507255554199219
		 59.168 78.464530944824219 60.244 78.451202392578125 61.32 78.411941528320327 62.396 78.399002075195312
		 63.472 78.360565185546875 64.548 78.347625732421889 65.624 78.30670166015625 66.7 78.286308288574219
		 67.776 78.260124206542983 68.852 78.233131408691406 69.932 78.210807800292983 71.008 78.15960693359375
		 72.084 78.134757995605469;
createNode animCurveTA -n "Bip01_R_Clavicle_rotateZ1";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 67 ".ktv[0:66]"  1.052 173.22410583496094 2.128 173.2296142578125
		 3.204 173.23175048828125 4.28 173.23379516601562 5.356 173.23802185058594 6.432 173.23912048339844
		 7.508 173.24183654785156 8.584 173.24386596679687 9.66 173.24559020996094 10.736 173.24612426757812
		 11.812 173.24736022949219 12.888 173.24763488769531 13.968 173.24795532226562 15.044 173.24789428710935
		 16.12 173.24751281738281 17.196 173.24668884277344 18.272 173.24636840820312 19.348 173.24508666992187
		 20.424 173.24382019042969 21.5 173.24243164062503 22.576 173.24098205566406 23.652 173.23931884765625
		 24.728 173.23745727539062 25.804 173.23683166503906 26.88 173.23493957519531 27.956 173.23313903808594
		 29.032 173.232666015625 30.108 173.2313232421875 31.184 173.2303466796875 32.26 173.22967529296875
		 33.34 173.229248046875 34.416 173.22921752929687 35.492 173.22946166992187 36.568 173.22962951660156
		 37.644 173.23045349121094 38.72 173.23159790039062 39.796 173.23332214355469 40.872 173.23393249511719
		 41.948 173.23622131347656 43.024 173.23847961425781 44.1 173.2408447265625 45.176 173.24342346191406
		 46.252 173.24635314941406 47.328 173.25 48.404 173.25135803222656 49.48 173.25599670410156
		 50.556 173.257568359375 51.636 173.26235961914068 52.712 173.26394653320312 53.788 173.2686767578125
		 54.864 173.27023315429687 55.94 173.2750244140625 57.016 173.27705383300781 58.092 173.27911376953125
		 59.168 173.28349304199219 60.244 173.28486633300781 61.32 173.28890991210935 62.396 173.29022216796875
		 63.472 173.2941589355469 64.548 173.29545593261719 65.624 173.29962158203125 66.7 173.30171203613281
		 67.776 173.30433654785156 68.852 173.30705261230469 69.932 173.30929565429687 71.008 173.31437683105469
		 72.084 173.31680297851562;
createNode animCurveTA -n "Bip01_R_UpperArm_rotateX1";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 67 ".ktv[0:66]"  1.052 -1.0002373456954956 2.128 -1.6318544149398804
		 3.204 -2.4944682121276855 4.28 -3.5651888847351074 5.356 -4.8040199279785156 6.432 -6.2109870910644531
		 7.508 -7.7273273468017578 8.584 -9.2858648300170898 9.66 -10.928200721740726 10.736 -12.989842414855955
		 11.812 -15.96011257171631 12.888 -19.61994743347168 13.968 -22.877334594726563 15.044 -24.999670028686523
		 16.12 -26.195472717285156 17.196 -26.885740280151367 18.272 -27.413015365600582 19.348 -28.030763626098633
		 20.424 -28.633445739746097 21.5 -29.206211090087891 22.576 -29.846210479736328 23.652 -30.342458724975586
		 24.728 -30.582633972167965 25.804 -30.79652214050293 26.88 -31.102924346923825 27.956 -31.430688858032227
		 29.032 -31.757892608642571 30.108 -32.127567291259766 31.184 -32.522136688232422
		 32.26 -32.836025238037109 33.34 -32.966365814208984 34.416 -32.880996704101563 35.492 -32.606132507324219
		 36.568 -32.256721496582031 37.644 -31.94576263427734 38.72 -31.584920883178711 39.796 -31.155237197875973
		 40.872 -30.842281341552734 41.948 -30.624950408935547 43.024 -30.330289840698239
		 44.1 -29.877878189086914 45.176 -29.281852722167965 46.252 -28.71531867980957 47.328 -28.289495468139648
		 48.404 -27.900104522705082 49.48 -27.432216644287109 50.556 -26.964334487915039 51.636 -26.505760192871097
		 52.712 -25.649629592895508 53.788 -24.489799499511722 54.864 -23.985969543457031
		 55.94 -24.19285774230957 57.016 -24.505872726440433 58.092 -24.966676712036133 59.168 -25.341451644897461
		 60.244 -25.081943511962891 61.32 -24.600471496582031 62.396 -24.391433715820313 63.472 -24.145563125610352
		 64.548 -23.787727355957031 65.624 -23.592144012451172 66.7 -23.42024040222168 67.776 -22.987668991088867
		 68.852 -22.397909164428711 69.932 -21.961463928222656 71.008 -21.859273910522461
		 72.084 -22.038925170898441;
createNode animCurveTA -n "Bip01_R_UpperArm_rotateY1";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 67 ".ktv[0:66]"  1.052 -68.870872497558594 2.128 -68.89959716796875
		 3.204 -68.904060363769531 4.28 -68.888343811035156 5.356 -68.8404541015625 6.432 -68.78192138671875
		 7.508 -68.708526611328125 8.584 -68.627235412597656 9.66 -68.729927062988281 10.736 -69.295982360839844
		 11.812 -70.349845886230469 12.888 -71.388427734375 13.968 -71.866287231445313 15.044 -71.88153076171875
		 16.12 -71.81707763671875 17.196 -71.781425476074219 18.272 -71.686302185058594 19.348 -71.456398010253906
		 20.424 -71.122398376464844 21.5 -70.782554626464844 22.576 -70.495262145996094 23.652 -70.26629638671875
		 24.728 -70.068168640136719 25.804 -69.853523254394531 26.88 -69.636199951171875 27.956 -69.475418090820313
		 29.032 -69.360298156738281 30.108 -69.2601318359375 31.184 -69.176406860351562 32.26 -69.094276428222656
		 33.34 -69.018112182617188 34.416 -68.985054016113281 35.492 -69.008934020996094 36.568 -69.058296203613281
		 37.644 -69.087356567382812 38.72 -69.098892211914062 39.796 -69.154090881347656 40.872 -69.283462524414077
		 41.948 -69.452682495117187 43.024 -69.635452270507813 44.1 -69.830589294433594 45.176 -70.030303955078125
		 46.252 -70.253349304199219 47.328 -70.54827880859375 48.404 -70.872535705566406 49.48 -71.087554931640625
		 50.556 -71.138427734375 51.636 -71.075393676757813 52.712 -70.782394409179687 53.788 -70.030799865722656
		 54.864 -69.067329406738281 55.94 -68.497398376464844 57.016 -68.412979125976562 58.092 -68.415657043457031
		 59.168 -68.411155700683594 60.244 -68.55377197265625 61.32 -68.739501953125 62.396 -68.854881286621094
		 63.472 -68.997390747070312 64.548 -69.178062438964844 65.624 -69.286788940429688
		 66.7 -69.370445251464844 67.776 -69.547721862792969 68.852 -69.777900695800781 69.932 -69.973869323730469
		 71.008 -70.134750366210937 72.084 -70.289291381835938;
createNode animCurveTA -n "Bip01_R_UpperArm_rotateZ1";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 67 ".ktv[0:66]"  1.052 -16.130624771118164 2.128 -16.010181427001953
		 3.204 -15.723978996276855 4.28 -15.314222335815428 5.356 -14.829862594604492 6.432 -14.275152206420898
		 7.508 -13.717523574829102 8.584 -13.23469352722168 9.66 -12.848278999328613 10.736 -12.263312339782717
		 11.812 -11.140440940856934 12.888 -9.8953819274902344 13.968 -9.089472770690918 15.044 -8.6283149719238281
		 16.12 -8.294184684753418 17.196 -8.1454582214355487 18.272 -8.1864728927612305 19.348 -8.2731409072875977
		 20.424 -8.4655876159667969 21.5 -8.6911048889160156 22.576 -8.7839269638061523 23.652 -8.9879322052001953
		 24.728 -9.4256601333618164 25.804 -9.76495361328125 26.88 -9.8606948852539062 27.956 -9.8506460189819336
		 29.032 -9.7634792327880859 30.108 -9.5302619934082031 31.184 -9.1824111938476563
		 32.26 -8.8572196960449219 33.34 -8.6982212066650391 34.416 -8.772273063659668 35.492 -9.0489215850830078
		 36.568 -9.3427867889404297 37.644 -9.4619913101196289 38.72 -9.5256214141845703 39.796 -9.5913534164428711
		 40.872 -9.4153175354003906 41.948 -9.0030965805053711 43.024 -8.5730180740356445
		 44.1 -8.2320766448974609 45.176 -7.9838819503784189 46.252 -7.644688606262207 47.328 -7.0355625152587891
		 48.404 -6.2374591827392578 49.48 -5.4252934455871582 50.556 -4.4362645149230957 51.636 -3.1723251342773442
		 52.712 -2.2017166614532471 53.788 -1.3734902143478394 54.864 0.45567619800567616
		 55.94 2.9436380863189697 57.016 5.1517424583435059 58.092 7.4162273406982422 59.168 9.4908609390258789
		 60.244 10.524150848388674 61.32 11.144365310668944 62.396 12.08621311187744 63.472 12.870413780212402
		 64.548 13.39180850982666 65.624 14.09625244140625 66.7 14.772006034851074 67.776 14.961600303649901
		 68.852 14.83637523651123 69.932 14.865672111511229 71.008 15.276256561279297 72.084 15.980197906494141;
createNode animCurveTA -n "Bip01_R_Forearm_rotateX1";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 67 ".ktv[0:66]"  1.052 -1.139295220375061 2.128 -1.1621814966201782
		 3.204 -1.1912919282913208 4.28 -1.2232309579849243 5.356 -1.2584496736526487 6.432 -1.2998917102813721
		 7.508 -1.3448041677474976 8.584 -1.3907554149627686 9.66 -1.4432255029678345 10.736 -1.5074547529220581
		 11.812 -1.5760033130645752 12.888 -1.6342184543609619 13.968 -1.6749663352966309
		 15.044 -1.6999292373657229 16.12 -1.7132700681686399 17.196 -1.7178915739059448 18.272 -1.7199610471725464
		 19.348 -1.7265231609344482 20.424 -1.733222484588623 21.5 -1.7359573841094973 22.576 -1.738872766494751
		 23.652 -1.7387946844100952 24.728 -1.733098030090332 25.804 -1.7301211357116701 26.88 -1.7327021360397341
		 27.956 -1.7341717481613159 29.032 -1.7334893941879272 30.108 -1.7359083890914917
		 31.184 -1.7418410778045654 32.26 -1.7470130920410156 33.34 -1.7484111785888674 34.416 -1.746131420135498
		 35.492 -1.7396005392074585 36.568 -1.7329753637313845 37.644 -1.730809211730957 38.72 -1.7305266857147217
		 39.796 -1.728588342666626 40.872 -1.7289108037948608 41.948 -1.7325323820114138 43.024 -1.7359428405761723
		 44.1 -1.7377852201461792 45.176 -1.7386629581451416 46.252 -1.7410653829574585 47.328 -1.7469145059585571
		 48.404 -1.7559841871261597 49.48 -1.765857458114624 50.556 -1.7763763666152954 51.636 -1.7903580665588379
		 52.712 -1.8021416664123537 53.788 -1.8094277381896977 54.864 -1.8256386518478394
		 55.94 -1.8501361608505251 57.016 -1.8670665025711066 58.092 -1.8800957202911377 59.168 -1.8894063234329226
		 60.244 -1.8859037160873413 61.32 -1.8766748905181887 62.396 -1.8713397979736328 63.472 -1.8628735542297363
		 64.548 -1.8493540287017829 65.624 -1.838159441947937 66.7 -1.8295161724090576 67.776 -1.819274306297302
		 68.852 -1.8074971437454219 69.932 -1.796149730682373 71.008 -1.7879952192306521 72.084 -1.7852784395217896;
createNode animCurveTA -n "Bip01_R_Forearm_rotateY1";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 67 ".ktv[0:66]"  1.052 1.9554533958435059 2.128 1.9647936820983891
		 3.204 1.9769204854965208 4.28 1.990064263343811 5.356 2.0043025016784668 6.432 2.020594596862793
		 7.508 2.0368421077728271 8.584 2.0514976978302002 9.66 2.0669040679931641 10.736 2.084958553314209
		 11.812 2.1025521755218506 12.888 2.1151971817016602 13.968 2.1223809719085693 15.044 2.12638258934021
		 16.12 2.129331111907959 17.196 2.1316072940826416 18.272 2.1337239742279053 19.348 2.1365542411804199
		 20.424 2.1387436389923096 21.5 2.1397674083709717 22.576 2.1414148807525635 23.652 2.1431126594543457
		 24.728 2.1434783935546875 25.804 2.1435024738311768 26.88 2.1435275077819824 27.956 2.1426291465759277
		 29.032 2.1416816711425786 30.108 2.1423237323760986 31.184 2.1442174911499023 32.26 2.1458845138549805
		 33.34 2.1464905738830566 34.416 2.1464996337890625 35.492 2.1447117328643799 36.568 2.142730712890625
		 37.644 2.1419312953948975 38.72 2.1418724060058594 39.796 2.1416621208190918 40.872 2.1422901153564453
		 41.948 2.1440768241882324 43.024 2.1460802555084229 44.1 2.1475703716278076 45.176 2.1480605602264404
		 46.252 2.1473839282989502 47.328 2.1458077430725098 48.404 2.1440794467926025 49.48 2.1430912017822266
		 50.556 2.1438007354736328 51.636 2.1466898918151855 52.712 2.1494712829589844 53.788 2.1508016586303711
		 54.864 2.1533637046813965 55.94 2.1572818756103516 57.016 2.1576311588287354 58.092 2.1595213413238525
		 59.168 2.1614775657653809 60.244 2.1621377468109131 61.32 2.1620090007781982 62.396 2.1617312431335449
		 63.472 2.1596405506134033 64.548 2.1562108993530273 65.624 2.1542811393737793 66.7 2.1548292636871338
		 67.776 2.1567862033843994 68.852 2.1589620113372803 69.932 2.1600172519683838 71.008 2.1592388153076172
		 72.084 2.1570901870727539;
createNode animCurveTA -n "Bip01_R_Forearm_rotateZ1";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 67 ".ktv[0:66]"  1.052 -53.896064758300781 2.128 -54.540534973144531
		 3.204 -55.365585327148437 4.28 -56.271987915039063 5.356 -57.270011901855469 6.432 -58.441295623779297
		 7.508 -59.698146820068366 8.584 -60.967147827148438 9.66 -62.407310485839837 10.736 -64.167640686035156
		 11.812 -66.036933898925781 12.888 -67.611434936523438 13.968 -68.704864501953125
		 15.044 -69.373641967773438 16.12 -69.734848022460938 17.196 -69.865806579589844 18.272 -69.930160522460938
		 19.348 -70.11456298828125 20.424 -70.299209594726563 21.5 -70.375587463378906 22.576 -70.459785461425781
		 23.652 -70.467437744140625 24.728 -70.322708129882812 25.804 -70.246231079101562
		 26.88 -70.312911987304688 27.956 -70.345405578613281 29.032 -70.322303771972656 30.108 -70.388206481933594
		 31.184 -70.551864624023438 32.26 -70.694587707519531 33.34 -70.734169006347656 34.416 -70.675697326660156
		 35.492 -70.496994018554688 36.568 -70.314956665039063 37.644 -70.254837036132813
		 38.72 -70.247215270996094 39.796 -70.196022033691406 40.872 -70.208023071289063 41.948 -70.311836242675781
		 43.024 -70.411651611328125 44.1 -70.468086242675781 45.176 -70.493858337402344 46.252 -70.551239013671875
		 47.328 -70.692642211914062 48.404 -70.917289733886719 49.48 -71.167755126953125 50.556 -71.444313049316406
		 51.636 -71.821342468261719 52.712 -72.140327453613281 53.788 -72.33587646484375 54.864 -72.7691650390625
		 55.94 -73.42425537109375 57.016 -73.867843627929702 58.092 -74.215354919433594 59.168 -74.465934753417983
		 60.244 -74.376846313476563 61.32 -74.135612487792983 62.396 -73.995079040527344 63.472 -73.766265869140625
		 64.548 -73.39971923828125 65.624 -73.100059509277344 66.7 -72.877182006835938 67.776 -72.620887756347642
		 68.852 -72.328010559082031 69.932 -72.041778564453125 71.008 -71.827690124511719
		 72.084 -71.745063781738281;
createNode animCurveTA -n "Bip01_L_Clavicle_rotateX1";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 67 ".ktv[0:66]"  1.052 0.60234177112579346 2.128 0.60727280378341675
		 3.204 0.61237674951553345 4.28 0.61805325746536255 5.356 0.62541234493255615 6.432 0.62907111644744873
		 7.508 0.63277477025985718 8.584 0.6412007212638855 9.66 0.64953345060348511 10.736 0.65200614929199219
		 11.812 0.65992242097854614 12.888 0.66643136739730835 13.968 0.67223775386810303
		 15.044 0.67805087566375732 16.12 0.67951422929763794 17.196 0.68302476406097412 18.272 0.6842876672744751
		 19.348 0.68332338333129883 20.424 0.67966544628143311 21.5 0.67800164222717285 22.576 0.67058336734771729
		 23.652 0.66223609447479248 24.728 0.65272212028503429 25.804 0.64107829332351685
		 26.88 0.62632399797439575 27.956 0.62095516920089722 29.032 0.60239803791046143 30.108 0.59638243913650513
		 31.184 0.57884097099304199 32.26 0.57392185926437378 33.34 0.56058216094970703 34.416 0.54870808124542236
		 35.492 0.54522967338562012 36.568 0.53378736972808838 37.644 0.52916532754898071
		 38.72 0.52472323179244995 39.796 0.51503574848175049 40.872 0.5118025541305542 41.948 0.50169289112091064
		 43.024 0.49856540560722368 44.1 0.48789280652999872 45.176 0.47573241591453552 46.252 0.47118782997131348
		 47.328 0.45462745428085327 48.404 0.44876694679260254 49.48 0.42933377623558056 50.556 0.42063337564468389
		 51.636 0.412605881690979 52.712 0.39606046676635742 53.788 0.38328111171722418 54.864 0.37160885334014898
		 55.94 0.35943859815597534 57.016 0.35615774989128113 58.092 0.34672406315803528 59.168 0.34033414721488953
		 60.244 0.33602273464202886 61.32 0.33318093419075012 62.396 0.33160293102264404 63.472 0.33152133226394659
		 64.548 0.33229869604110718 65.624 0.33440765738487244 66.7 0.33800387382507324 67.776 0.33931219577789307
		 68.852 0.34474289417266846 69.932 0.35123586654663086 71.008 0.35331463813781738
		 72.084 0.36090540885925299;
createNode animCurveTA -n "Bip01_L_Clavicle_rotateY1";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 67 ".ktv[0:66]"  1.052 -79.751724243164062 2.128 -79.760650634765625
		 3.204 -79.769943237304688 4.28 -79.780258178710938 5.356 -79.793617248535156 6.432 -79.800209045410156
		 7.508 -79.806907653808594 8.584 -79.822212219238281 9.66 -79.83740234375 10.736 -79.841743469238281
		 11.812 -79.856185913085938 12.888 -79.867904663085938 13.968 -79.878471374511719
		 15.044 -79.888931274414063 16.12 -79.891578674316406 17.196 -79.897880554199219 18.272 -79.90020751953125
		 19.348 -79.898468017578125 20.424 -79.891838073730469 21.5 -79.888832092285156 22.576 -79.875434875488281
		 23.652 -79.860359191894531 24.728 -79.843032836914063 25.804 -79.822059631347642
		 26.88 -79.795257568359375 27.956 -79.785514831542983 29.032 -79.751869201660156 30.108 -79.740913391113281
		 31.184 -79.708984375 32.26 -79.700088500976563 33.34 -79.675811767578125 34.416 -79.654167175292983
		 35.492 -79.647781372070327 36.568 -79.626907348632813 37.644 -79.6185302734375 38.72 -79.610443115234375
		 39.796 -79.592758178710938 40.872 -79.586898803710937 41.948 -79.568443298339844
		 43.024 -79.562637329101563 44.1 -79.543159484863281 45.176 -79.520942687988281 46.252 -79.51263427734375
		 47.328 -79.48236083984375 48.404 -79.471565246582031 49.48 -79.436012268066406 50.556 -79.420028686523438
		 51.636 -79.4052734375 52.712 -79.374870300292983 53.788 -79.351425170898438 54.864 -79.329978942871108
		 55.94 -79.307540893554688 57.016 -79.301544189453125 58.092 -79.284103393554687 59.168 -79.272422790527344
		 60.244 -79.264488220214844 61.32 -79.259193420410156 62.396 -79.256378173828125 63.472 -79.256202697753906
		 64.548 -79.257652282714844 65.624 -79.261459350585938 66.7 -79.268104553222656 67.776 -79.270538330078125
		 68.852 -79.280509948730469 69.932 -79.292457580566406 71.008 -79.296295166015625
		 72.084 -79.310333251953125;
createNode animCurveTA -n "Bip01_L_Clavicle_rotateZ1";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 67 ".ktv[0:66]"  1.052 173.13894653320312 2.128 173.13780212402344
		 3.204 173.13667297363281 4.28 173.1353759765625 5.356 173.13369750976562 6.432 173.13285827636719
		 7.508 173.13200378417969 8.584 173.13008117675781 9.66 173.12820434570312 10.736 173.12759399414062
		 11.812 173.12580871582034 12.888 173.12428283691406 13.968 173.12297058105472 15.044 173.12161254882813
		 16.12 173.12127685546875 17.196 173.12043762207031 18.272 173.12016296386719 19.348 173.12037658691406
		 20.424 173.12123107910156 21.5 173.12161254882813 22.576 173.12333679199219 23.652 173.12525939941406
		 24.728 173.12741088867187 25.804 173.13014221191406 26.88 173.13348388671875 27.956 173.13471984863281
		 29.032 173.13896179199219 30.108 173.14028930664062 31.184 173.14424133300781 32.26 173.1453857421875
		 33.34 173.14839172363281 34.416 173.15104675292969 35.492 173.15180969238281 36.568 173.15435791015625
		 37.644 173.15541076660156 38.72 173.15640258789065 39.796 173.15855407714844 40.872 173.15928649902344
		 41.948 173.16154479980469 43.024 173.16220092773435 44.1 173.16458129882812 45.176 173.16726684570312
		 46.252 173.16825866699219 47.328 173.17192077636719 48.404 173.17318725585935 49.48 173.17744445800781
		 50.556 173.1793212890625 51.636 173.18106079101562 52.712 173.18463134765625 53.788 173.18740844726562
		 54.864 173.18992614746094 55.94 173.19252014160156 57.016 173.1932373046875 58.092 173.19522094726562
		 59.168 173.19664001464844 60.244 173.19754028320312 61.32 173.19813537597656 62.396 173.19850158691406
		 63.472 173.19851684570312 64.548 173.19836425781253 65.624 173.1978759765625 66.7 173.19712829589844
		 67.776 173.19683837890625 68.852 173.1956787109375 69.932 173.19429016113281 71.008 173.19383239746094
		 72.084 173.19223022460935;
createNode animCurveTA -n "Bip01_L_UpperArm_rotateX1";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 67 ".ktv[0:66]"  1.052 -8.3503808975219727 2.128 -7.4221405982971191
		 3.204 -6.3193702697753906 4.28 -4.5285806655883789 5.356 -1.2690926790237429 6.432 3.0584788322448735
		 7.508 7.2635521888732892 8.584 11.25689697265625 9.66 15.491022109985352 10.736 19.670791625976563
		 11.812 23.118064880371097 12.888 25.638595581054687 13.968 27.467897415161133 15.044 28.834686279296879
		 16.12 29.936983108520504 17.196 30.780061721801765 18.272 31.306367874145511 19.348 31.656135559082031
		 20.424 31.924661636352539 21.5 32.177234649658203 22.576 32.576190948486328 23.652 33.06005859375
		 24.728 33.418163299560547 25.804 33.6466064453125 26.88 33.845375061035156 27.956 34.026611328125
		 29.032 34.172340393066406 30.108 34.277973175048828 31.184 34.366653442382813 32.26 34.486495971679688
		 33.34 34.607334136962891 34.416 34.660198211669922 35.492 34.672084808349609 36.568 34.686347961425781
		 37.644 34.681159973144531 38.72 34.661014556884766 39.796 34.611637115478516 40.872 34.443439483642578
		 41.948 34.158084869384766 43.024 33.824172973632812 44.1 33.412502288818359 45.176 32.911327362060547
		 46.252 32.386543273925781 47.328 31.842111587524418 48.404 31.202167510986332 49.48 30.39376258850098
		 50.556 29.428688049316406 51.636 28.387493133544918 52.712 27.400703430175781 53.788 26.561197280883789
		 54.864 25.679527282714844 55.94 24.477773666381836 57.016 23.129463195800781 58.092 22.121513366699219
		 59.168 21.370450973510746 60.244 20.306974411010746 61.32 18.995101928710937 62.396 17.930150985717773
		 63.472 17.171712875366211 64.548 16.437717437744141 65.624 15.520079612731934 66.7 14.466577529907227
		 67.776 13.503146171569824 68.852 12.769504547119141 69.932 12.079699516296388 71.008 11.130175590515137
		 72.084 9.9212999343872088;
createNode animCurveTA -n "Bip01_L_UpperArm_rotateY1";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 67 ".ktv[0:66]"  1.052 67.387413024902344 2.128 67.284080505371094
		 3.204 67.140480041503906 4.28 66.877609252929688 5.356 66.359649658203125 6.432 65.651611328125
		 7.508 65.06689453125 8.584 64.666641235351563 9.66 64.276580810546875 10.736 64.083915710449219
		 11.812 64.248046875 12.888 64.435050964355469 13.968 64.434104919433594 15.044 64.369422912597656
		 16.12 64.277557373046875 17.196 64.174476623535156 18.272 64.132858276367188 19.348 64.117340087890625
		 20.424 64.082321166992188 21.5 64.031723022460938 22.576 63.954780578613274 23.652 63.872703552246094
		 24.728 63.831508636474609 25.804 63.839725494384766 26.88 63.878528594970703 27.956 63.951385498046875
		 29.032 64.065811157226563 30.108 64.177757263183594 31.184 64.25 32.26 64.300262451171875
		 33.34 64.355628967285156 34.416 64.432296752929688 35.492 64.516990661621094 36.568 64.581535339355469
		 37.644 64.631492614746094 38.72 64.682533264160156 39.796 64.742485046386719 40.872 64.822288513183594
		 41.948 64.923240661621094 43.024 65.050338745117187 44.1 65.207527160644531 45.176 65.379539489746094
		 46.252 65.560417175292969 47.328 65.749130249023438 48.404 65.923828125 49.48 66.066978454589844
		 50.556 66.155525207519531 51.636 66.178108215332031 52.712 66.1705322265625 53.788 66.167701721191406
		 54.864 66.1585693359375 55.94 66.029808044433594 57.016 65.788734436035156 58.092 65.717536926269531
		 59.168 65.79254150390625 60.244 65.735557556152344 61.32 65.63922119140625 62.396 65.70343017578125
		 63.472 65.901771545410156 64.548 66.144577026367188 65.624 66.397750854492188 66.7 66.646629333496094
		 67.776 66.854331970214844 68.852 67.002891540527344 69.932 67.123527526855469 71.008 67.237434387207031
		 72.084 67.331687927246094;
createNode animCurveTA -n "Bip01_L_UpperArm_rotateZ1";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 67 ".ktv[0:66]"  1.052 -25.739702224731445 2.128 -24.772493362426761
		 3.204 -23.802894592285156 4.28 -22.730413436889648 5.356 -21.191154479980469 6.432 -19.081779479980469
		 7.508 -16.517303466796875 8.584 -13.76077938079834 9.66 -11.395556449890137 10.736 -9.5965662002563477
		 11.812 -8.063262939453125 12.888 -6.7388916015625 13.968 -5.8085498809814453 15.044 -5.3630666732788086
		 16.12 -5.3112554550170898 17.196 -5.3866925239562988 18.272 -5.3187408447265625 19.348 -5.1899042129516602
		 20.424 -5.2521142959594727 21.5 -5.4338674545288086 22.576 -5.4128456115722665 23.652 -5.2102518081665039
		 24.728 -5.1202149391174316 25.804 -5.1599931716918945 26.88 -5.1796116828918457 27.956 -5.1705269813537598
		 29.032 -5.1727867126464844 30.108 -5.1770219802856445 31.184 -5.1347184181213379
		 32.26 -4.991663932800293 33.34 -4.8119139671325684 34.416 -4.7062687873840332 35.492 -4.6207551956176758
		 36.568 -4.4635238647460938 37.644 -4.2585248947143555 38.72 -3.9990553855895992 39.796 -3.6985557079315186
		 40.872 -3.4795393943786621 41.948 -3.3280119895935059 43.024 -3.1418232917785645
		 44.1 -2.9634277820587158 45.176 -2.7967493534088135 46.252 -2.5461912155151372 47.328 -2.2204821109771729
		 48.404 -1.9288918972015381 49.48 -1.7622115612030027 50.556 -1.6970272064208984 51.636 -1.6503614187240601
		 52.712 -1.5005019903182983 53.788 -1.1670564413070681 54.864 -0.89854604005813599
		 55.94 -0.9550095796585083 57.016 -1.146602988243103 58.092 -1.1207559108734133 59.168 -1.012669563293457
		 60.244 -1.3762819766998291 61.32 -2.1777312755584717 62.396 -2.8672130107879639 63.472 -3.3258137702941895
		 64.548 -3.878677129745483 65.624 -4.7853779792785645 66.7 -5.9572286605834961 67.776 -7.0393481254577637
		 68.852 -7.812708854675293 69.932 -8.5280475616455078 71.008 -9.579401969909668 72.084 -10.98960018157959;
createNode animCurveTA -n "Bip01_L_Forearm_rotateX1";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 67 ".ktv[0:66]"  1.052 0.79985392093658447 2.128 0.82412487268447876
		 3.204 0.85499697923660267 4.28 0.90201073884963978 5.356 0.97258406877517711 6.432 1.0676469802856443
		 7.508 1.1921461820602417 8.584 1.3496004343032837 9.66 1.5131016969680786 10.736 1.6414008140563965
		 11.812 1.7268437147140503 12.888 1.7872209548950195 13.968 1.829525351524353 15.044 1.8528144359588623
		 16.12 1.8632082939147949 17.196 1.8684152364730835 18.272 1.8721152544021613 19.348 1.8745580911636353
		 20.424 1.8732930421829217 21.5 1.8706777095794676 22.576 1.872215151786804 23.652 1.8751566410064695
		 24.728 1.8749145269393921 25.804 1.8737096786499019 26.88 1.8719162940978995 27.956 1.866537809371948
		 29.032 1.8597780466079712 30.108 1.855876088142395 31.184 1.8548195362091064 32.26 1.8553047180175781
		 33.34 1.855968713760376 34.416 1.8552103042602541 35.492 1.8544577360153196 36.568 1.8558559417724607
		 37.644 1.8585916757583616 38.72 1.8628121614456179 39.796 1.8690282106399536 40.872 1.874269962310791
		 41.948 1.8779879808425903 43.024 1.8824907541275031 44.1 1.8859982490539553 45.176 1.887536287307739
		 46.252 1.8903651237487791 47.328 1.8949332237243659 48.404 1.9007341861724851 49.48 1.9033108949661255
		 50.556 1.9008530378341679 51.636 1.8938175439834593 52.712 1.8842710256576534 53.788 1.8739837408065796
		 54.864 1.8633848428726196 55.94 1.844387412071228 57.016 1.8123822212219238 58.092 1.7720564603805542
		 59.168 1.7052433490753174 60.244 1.5973477363586426 61.32 1.485553503036499 62.396 1.4026937484741211
		 63.472 1.3381952047348022 64.548 1.2797137498855593 65.624 1.2297194004058838 66.7 1.1893775463104248
		 67.776 1.1588099002838137 68.852 1.1383799314498899 69.932 1.1188783645629885 71.008 1.089873194694519
		 72.084 1.0544036626815796;
createNode animCurveTA -n "Bip01_L_Forearm_rotateY1";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 67 ".ktv[0:66]"  1.052 -1.7530491352081301 2.128 -1.7729772329330444
		 3.204 -1.7952126264572144 4.28 -1.8238890171051023 5.356 -1.8624353408813483 6.432 -1.910275936126709
		 7.508 -1.968326210975647 8.584 -2.0330822467803955 9.66 -2.0869317054748535 10.736 -2.1177809238433838
		 11.812 -2.1317324638366699 12.888 -2.138648509979248 13.968 -2.1420881748199463 15.044 -2.1432130336761475
		 16.12 -2.1443133354187012 17.196 -2.1469738483428955 18.272 -2.1509671211242676 19.348 -2.1551363468170166
		 20.424 -2.1582319736480713 21.5 -2.1604251861572266 22.576 -2.1626434326171875 23.652 -2.1643631458282471
		 24.728 -2.1648752689361572 25.804 -2.1648111343383789 26.88 -2.1645510196685791 27.956 -2.163707971572876
		 29.032 -2.1628358364105225 30.108 -2.1626918315887451 31.184 -2.1631247997283936
		 32.26 -2.1636335849761963 33.34 -2.1637299060821533 34.416 -2.1630439758300781 35.492 -2.1620907783508301
		 36.568 -2.1617574691772461 37.644 -2.1621973514556889 38.72 -2.1632788181304932 39.796 -2.1645262241363525
		 40.872 -2.1648666858673096 41.948 -2.1642787456512451 43.024 -2.163686990737915 44.1 -2.163172721862793
		 45.176 -2.162595272064209 46.252 -2.1624510288238525 47.328 -2.1614284515380859 48.404 -2.1617414951324463
		 49.48 -2.1614370346069336 50.556 -2.1604676246643062 51.636 -2.158484935760498 52.712 -2.1570639610290527
		 53.788 -2.1560077667236328 54.864 -2.1554279327392578 55.94 -2.153656005859375 57.016 -2.1494176387786865
		 58.092 -2.1433758735656734 59.168 -2.1306030750274658 60.244 -2.1040313243865967
		 61.32 -2.071460485458374 62.396 -2.0463879108428955 63.472 -2.0270957946777344 64.548 -2.0080184936523442
		 65.624 -1.9898562431335447 66.7 -1.974093556404114 67.776 -1.9626106023788452 68.852 -1.9571214914321893
		 69.932 -1.9523974657058716 71.008 -1.9411571025848389 72.084 -1.9233183860778809;
createNode animCurveTA -n "Bip01_L_Forearm_rotateZ1";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 67 ".ktv[0:66]"  1.052 -43.527687072753906 2.128 -44.354900360107422
		 3.204 -45.352687835693359 4.28 -46.787570953369141 5.356 -48.883220672607422 6.432 -51.668437957763672
		 7.508 -55.283576965332031 8.584 -59.777889251708977 9.66 -64.32611083984375 10.736 -67.813751220703125
		 11.812 -70.10235595703125 12.888 -71.710632324218764 13.968 -72.835739135742187 15.044 -73.454338073730469
		 16.12 -73.732315063476562 17.196 -73.875457763671875 18.272 -73.982566833496094 19.348 -74.057647705078125
		 20.424 -74.034530639648437 21.5 -73.974029541015625 22.576 -74.022811889648438 23.652 -74.106765747070327
		 24.728 -74.102439880371094 25.804 -74.071090698242188 26.88 -74.023139953613281 27.956 -73.879737854003906
		 29.032 -73.700141906738281 30.108 -73.59808349609375 31.184 -73.572792053222656 32.26 -73.587654113769531
		 33.34 -73.605484008789063 34.416 -73.582664489746094 35.492 -73.558624267578125 36.568 -73.593124389648438
		 37.644 -73.666305541992188 38.72 -73.780807495117188 39.796 -73.947868347167969 40.872 -74.085861206054688
		 41.948 -74.180061340332045 43.024 -74.294807434082031 44.1 -74.38421630859375 45.176 -74.422027587890625
		 46.252 -74.495384216308594 47.328 -74.611274719238281 48.404 -74.764213562011719
		 49.48 -74.830612182617188 50.556 -74.763359069824219 51.636 -74.572647094726563 52.712 -74.317642211914063
		 53.788 -74.044502258300781 54.864 -73.764785766601591 55.94 -73.260765075683594 57.016 -72.408103942871094
		 58.092 -71.330238342285156 59.168 -69.532127380371094 60.244 -66.593048095703125
		 61.32 -63.511657714843757 62.396 -61.220073699951165 63.472 -59.44182968139647 64.548 -57.821987152099609
		 65.624 -56.4227294921875 66.7 -55.286876678466797 67.776 -54.436309814453125 68.852 -53.900993347167969
		 69.932 -53.401775360107422 71.008 -52.603485107421882 72.084 -51.568840026855469;
createNode animCurveTA -n "Bip01_L_Hand_rotateX1";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 67 ".ktv[0:66]"  1.052 -90.133308410644531 2.128 -90.133316040039063
		 3.204 -90.133316040039063 4.28 -90.133316040039063 5.356 -90.133316040039063 6.432 -90.133308410644531
		 7.508 -90.133316040039063 8.584 -90.133316040039063 9.66 -90.133316040039063 10.736 -90.133316040039063
		 11.812 -90.133316040039063 12.888 -90.133316040039063 13.968 -90.133316040039063
		 15.044 -90.133308410644531 16.12 -90.133308410644531 17.196 -90.133316040039063 18.272 -90.133316040039063
		 19.348 -90.133316040039063 20.424 -90.133316040039063 21.5 -90.133308410644531 22.576 -90.133331298828125
		 23.652 -90.133316040039063 24.728 -90.133316040039063 25.804 -90.133316040039063
		 26.88 -90.133316040039063 27.956 -90.133308410644531 29.032 -90.133316040039063 30.108 -90.133308410644531
		 31.184 -90.133308410644531 32.26 -90.133316040039063 33.34 -90.133316040039063 34.416 -90.133331298828125
		 35.492 -90.133331298828125 36.568 -90.133316040039063 37.644 -90.133316040039063
		 38.72 -90.133316040039063 39.796 -90.133308410644531 40.872 -90.133316040039063 41.948 -90.133316040039063
		 43.024 -90.133308410644531 44.1 -90.133308410644531 45.176 -90.133316040039063 46.252 -90.133316040039063
		 47.328 -90.133308410644531 48.404 -90.133316040039063 49.48 -90.133308410644531 50.556 -90.133316040039063
		 51.636 -90.133316040039063 52.712 -90.133316040039063 53.788 -90.133308410644531
		 54.864 -90.133316040039063 55.94 -90.133316040039063 57.016 -90.133316040039063 58.092 -90.133308410644531
		 59.168 -90.133316040039063 60.244 -90.133316040039063 61.32 -90.133308410644531 62.396 -90.133316040039063
		 63.472 -90.133316040039063 64.548 -90.133316040039063 65.624 -90.133316040039063
		 66.7 -90.133316040039063 67.776 -90.133316040039063 68.852 -90.133316040039063 69.932 -90.133308410644531
		 71.008 -90.133316040039063 72.084 -90.133316040039063;
createNode animCurveTA -n "Bip01_L_Hand_rotateY1";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 67 ".ktv[0:66]"  1.052 6.3003983497619629 2.128 6.3003926277160645
		 3.204 6.3003926277160645 4.28 6.3003983497619629 5.356 6.3003931045532227 6.432 6.3003978729248047
		 7.508 6.3003935813903809 8.584 6.3004012107849121 9.66 6.3003926277160645 10.736 6.3003983497619629
		 11.812 6.3003964424133301 12.888 6.3003969192504883 13.968 6.3004007339477539 15.044 6.3003926277160645
		 16.12 6.3003940582275391 17.196 6.3003921508789063 18.272 6.3003964424133301 19.348 6.3003983497619629
		 20.424 6.3003945350646973 21.5 6.3003921508789063 22.576 6.3003926277160645 23.652 6.3003969192504883
		 24.728 6.3004021644592285 25.804 6.3003940582275391 26.88 6.3004026412963876 27.956 6.3003959655761728
		 29.032 6.3003954887390137 30.108 6.3003950119018555 31.184 6.3003954887390137 32.26 6.3004007339477539
		 33.34 6.3004007339477539 34.416 6.3003945350646973 35.492 6.3003954887390137 36.568 6.3004002571105957
		 37.644 6.3003964424133301 38.72 6.3003931045532227 39.796 6.3003988265991211 40.872 6.3004007339477539
		 41.948 6.3003959655761728 43.024 6.3003926277160645 44.1 6.3003945350646973 45.176 6.3004012107849121
		 46.252 6.3004007339477539 47.328 6.3003950119018555 48.404 6.3003940582275391 49.48 6.3003926277160645
		 50.556 6.3003954887390137 51.636 6.3004007339477539 52.712 6.3003973960876465 53.788 6.3003969192504883
		 54.864 6.3003931045532227 55.94 6.3003954887390137 57.016 6.3003969192504883 58.092 6.3003983497619629
		 59.168 6.3003959655761728 60.244 6.3003897666931152 61.32 6.3003997802734375 62.396 6.3003964424133301
		 63.472 6.3004007339477539 64.548 6.3003954887390137 65.624 6.3003964424133301 66.7 6.3003983497619629
		 67.776 6.3003983497619629 68.852 6.3003964424133301 69.932 6.3003969192504883 71.008 6.3004012107849121
		 72.084 6.3003911972045898;
createNode animCurveTA -n "Bip01_L_Hand_rotateZ1";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 67 ".ktv[0:66]"  1.052 2.5653319358825684 2.128 2.5653386116027832
		 3.204 2.565338134765625 4.28 2.5653290748596191 5.356 2.5653347969055176 6.432 2.5653293132781982
		 7.508 2.5653347969055176 8.584 2.5653300285339355 9.66 2.5653316974639893 10.736 2.5653312206268311
		 11.812 2.5653305053710938 12.888 2.5653302669525142 13.968 2.5653326511383057 15.044 2.5653328895568848
		 16.12 2.5653345584869385 17.196 2.5653336048126221 18.272 2.5653350353240967 19.348 2.5653347969055176
		 20.424 2.5653388500213623 21.5 2.5653331279754639 22.576 2.5653345584869385 23.652 2.5653324127197266
		 24.728 2.5653336048126221 25.804 2.5653319358825684 26.88 2.5653276443481445 27.956 2.5653338432312016
		 29.032 2.5653367042541504 30.108 2.565338134765625 31.184 2.5653338432312016 32.26 2.5653321743011475
		 33.34 2.5653257369995117 34.416 2.5653316974639893 35.492 2.5653345584869385 36.568 2.5653343200683594
		 37.644 2.5653269290924072 38.72 2.5653376579284668 39.796 2.5653319358825684 40.872 2.5653269290924072
		 41.948 2.5653307437896729 43.024 2.5653324127197266 44.1 2.5653278827667236 45.176 2.5653321743011475
		 46.252 2.5653347969055176 47.328 2.5653274059295654 48.404 2.5653326511383057 49.48 2.5653324127197266
		 50.556 2.5653331279754639 51.636 2.5653297901153564 52.712 2.5653285980224609 53.788 2.5653328895568848
		 54.864 2.5653338432312016 55.94 2.5653350353240967 57.016 2.5653338432312016 58.092 2.5653305053710938
		 59.168 2.5653297901153564 60.244 2.5653314590454106 61.32 2.5653326511383057 62.396 2.565338134765625
		 63.472 2.565333366394043 64.548 2.5653343200683594 65.624 2.5653324127197266 66.7 2.5653362274169922
		 67.776 2.5653300285339355 68.852 2.5653359889984131 69.932 2.5653386116027832 71.008 2.5653324127197266
		 72.084 2.5653343200683594;
createNode animCurveTA -n "Bip01_L_Thigh_rotateX1";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 67 ".ktv[0:66]"  1.052 0.20752845704555511 2.128 0.26383653283119202
		 3.204 0.34894067049026489 4.28 0.4198107123374939 5.356 0.4398095309734345 6.432 0.46303215622901922
		 7.508 0.52252310514450073 8.584 0.57777935266494751 9.66 0.59008735418319702 10.736 0.61714851856231689
		 11.812 0.68845570087432861 12.888 0.76091033220291138 13.968 0.80775177478790283
		 15.044 0.83178037405014027 16.12 0.85004431009292614 17.196 0.87396365404129028 18.272 0.91464424133300803
		 19.348 0.94487464427947998 20.424 0.93822205066680919 21.5 0.89920800924301147 22.576 0.86556249856948853
		 23.652 0.84810453653335571 24.728 0.81142640113830555 25.804 0.72770196199417125
		 26.88 0.62731188535690308 27.956 0.52714031934738159 29.032 0.41196966171264648 30.108 0.30096542835235596
		 31.184 0.21939931809902191 32.26 0.13433615863323212 33.34 0.02934249117970467 34.416 -0.087235167622566223
		 35.492 -0.19826671481132507 36.568 -0.28520137071609497 37.644 -0.40716120600700378
		 38.72 -0.49051684141159063 39.796 -0.53371095657348633 40.872 -0.53496772050857555
		 41.948 -0.54452997446060181 43.024 -0.58644002676010132 44.1 -0.60917085409164429
		 45.176 -0.59048348665237427 46.252 -0.56857222318649292 47.328 -0.57488334178924561
		 48.404 -0.57411837577819824 49.48 -0.53469449281692505 50.556 -0.48281237483024592
		 51.636 -0.42866265773773193 52.712 -0.35639923810958862 53.788 -0.30828741192817688
		 54.864 -0.29126656055450439 55.94 -0.28787881135940552 57.016 -0.28749629855155945
		 58.092 -0.30607441067695623 59.168 -0.30757707357406616 60.244 -0.28837057948112488
		 61.32 -0.29574716091156006 62.396 -0.33743864297866827 63.472 -0.36907607316970825
		 64.548 -0.38628816604614258 65.624 -0.41120469570159912 66.7 -0.44683095812797546
		 67.776 -0.44751396775245667 68.852 -0.44278746843338024 69.932 -0.44819697737693787
		 71.008 -0.43841615319252014 72.084 -0.39415654540061951;
createNode animCurveTA -n "Bip01_L_Thigh_rotateY1";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 67 ".ktv[0:66]"  1.052 -171.77694702148435 2.128 -171.703125
		 3.204 -171.61404418945312 4.28 -171.55319213867187 5.356 -171.55119323730469 6.432 -171.55415344238281
		 7.508 -171.50437927246094 8.584 -171.45932006835937 9.66 -171.45356750488281 10.736 -171.44172668457034
		 11.812 -171.38723754882813 12.888 -171.32023620605469 13.968 -171.28182983398435
		 15.044 -171.25105285644531 16.12 -171.23065185546875 17.196 -171.19398498535156 18.272 -171.14021301269531
		 19.348 -171.09751892089844 20.424 -171.08401489257812 21.5 -171.09516906738281 22.576 -171.1065673828125
		 23.652 -171.09661865234375 24.728 -171.10823059082031 25.804 -171.14524841308594
		 26.88 -171.21551513671875 27.956 -171.2762451171875 29.032 -171.35868835449219 30.108 -171.45014953613281
		 31.184 -171.49674987792969 32.26 -171.53622436523435 33.34 -171.59866333007813 34.416 -171.67156982421878
		 35.492 -171.74546813964844 36.568 -171.80271911621094 37.644 -171.85910034179687
		 38.72 -171.90509033203125 39.796 -171.91970825195312 40.872 -171.89309692382812 41.948 -171.88792419433594
		 43.024 -171.90887451171875 44.1 -171.91534423828125 45.176 -171.882568359375 46.252 -171.85249328613281
		 47.328 -171.84999084472656 48.404 -171.86503601074219 49.48 -171.84100341796875 50.556 -171.79377746582031
		 51.636 -171.74778747558594 52.712 -171.69479370117187 53.788 -171.67767333984375
		 54.864 -171.69174194335935 55.94 -171.71040344238281 57.016 -171.73497009277344 58.092 -171.77278137207031
		 59.168 -171.80455017089844 60.244 -171.82075500488281 61.32 -171.8551940917969 62.396 -171.91673278808597
		 63.472 -171.95716857910156 64.548 -172.01069641113281 65.624 -172.02885437011719
		 66.7 -172.04966735839844 67.776 -172.043701171875 68.852 -172.01390075683594 69.932 -171.97238159179687
		 71.008 -171.89613342285156 72.084 -171.76431274414062;
createNode animCurveTA -n "Bip01_L_Thigh_rotateZ1";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 67 ".ktv[0:66]"  1.052 -87.1494140625 2.128 -87.3851318359375
		 3.204 -87.648780822753906 4.28 -87.9193115234375 5.356 -88.175605773925781 6.432 -88.407966613769531
		 7.508 -88.592819213867187 8.584 -88.757453918457031 9.66 -88.915809631347642 10.736 -89.064651489257813
		 11.812 -89.207427978515625 12.888 -89.337310791015625 13.968 -89.452110290527344
		 15.044 -89.562515258789063 16.12 -89.657402038574219 17.196 -89.734939575195312 18.272 -89.814247131347642
		 19.348 -89.889686584472656 20.424 -89.942848205566406 21.5 -89.97530364990233 22.576 -90.0123291015625
		 23.652 -90.029617309570327 24.728 -90.001152038574219 25.804 -89.990058898925781
		 26.88 -89.981452941894531 27.956 -89.9462890625 29.032 -89.890281677246094 30.108 -89.875778198242188
		 31.184 -89.880943298339844 32.26 -89.857284545898438 33.34 -89.803291320800781 34.416 -89.7454833984375
		 35.492 -89.703575134277358 36.568 -89.675491333007813 37.644 -89.60675048828125 38.72 -89.52850341796875
		 39.796 -89.490005493164063 40.872 -89.502273559570327 41.948 -89.528068542480469
		 43.024 -89.505386352539063 44.1 -89.455253601074219 45.176 -89.415885925292983 46.252 -89.369415283203125
		 47.328 -89.321464538574219 48.404 -89.275650024414062 49.48 -89.255210876464844 50.556 -89.179450988769531
		 51.636 -89.084129333496094 52.712 -89.033233642578125 53.788 -89.014762878417969
		 54.864 -88.972801208496094 55.94 -88.894416809082031 57.016 -88.81390380859375 58.092 -88.718193054199219
		 59.168 -88.638008117675781 60.244 -88.565361022949219 61.32 -88.467910766601563 62.396 -88.332618713378906
		 63.472 -88.206535339355469 64.548 -88.122245788574219 65.624 -87.993675231933594
		 66.7 -87.869972229003906 67.776 -87.825271606445313 68.852 -87.824096679687514 69.932 -87.823989868164062
		 71.008 -87.838493347167969 72.084 -87.904281616210937;
createNode animCurveTA -n "Bip01_L_Calf_rotateX1";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 67 ".ktv[0:66]"  1.052 0.93406194448471069 2.128 0.93398350477218617
		 3.204 0.93390065431594849 4.28 0.93378478288650479 5.356 0.9336342215538026 6.432 0.93346709012985218
		 7.508 0.9333961009979248 8.584 0.93337976932525646 9.66 0.93330699205398571 10.736 0.93320560455322277
		 11.812 0.93312895298004161 12.888 0.93310141563415538 13.968 0.93304115533828735
		 15.044 0.93300515413284291 16.12 0.93298459053039551 17.196 0.93298321962356578 18.272 0.93298929929733287
		 19.348 0.9329870343208313 20.424 0.93295276165008556 21.5 0.93298530578613281 22.576 0.93298864364624001
		 23.652 0.93301659822463978 24.728 0.93308264017105103 25.804 0.93310344219207775
		 26.88 0.93310528993606578 27.956 0.93313908576965343 29.032 0.93315768241882324 30.108 0.93314069509506226
		 31.184 0.93308520317077637 32.26 0.93306225538253795 33.34 0.93302309513092052 34.416 0.93297380208969127
		 35.492 0.93290221691131592 36.568 0.93281304836273204 37.644 0.93271821737289429
		 38.72 0.93263369798660278 39.796 0.93254244327545166 40.872 0.93238908052444458 41.948 0.93222373723983776
		 43.024 0.93207341432571411 44.1 0.93196284770965576 45.176 0.93187147378921498 46.252 0.93179374933242831
		 47.328 0.93173050880432129 48.404 0.93162769079208363 49.48 0.93158525228500366 50.556 0.93162322044372547
		 51.636 0.93171417713165283 52.712 0.93173307180404663 53.788 0.93169921636581421
		 54.864 0.93168842792510975 55.94 0.9317316412925718 57.016 0.93185216188430786 58.092 0.93198192119598378
		 59.168 0.93207764625549328 60.244 0.93227016925811812 61.32 0.93249535560607921 62.396 0.93272727727890015
		 63.472 0.9329320788383485 64.548 0.93310368061065674 65.624 0.93328309059143089 66.7 0.93346416950225841
		 67.776 0.93360316753387451 68.852 0.93373864889144909 69.932 0.93383961915969838
		 71.008 0.93387120962142933 72.084 0.93387752771377575;
createNode animCurveTA -n "Bip01_L_Calf_rotateY1";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 67 ".ktv[0:66]"  1.052 0.018862409517168999 2.128 0.022073255851864815
		 3.204 0.025671970099210739 4.28 0.028895050287246701 5.356 0.031771834939718246 6.432 0.035403549671173096
		 7.508 0.038378097116947174 8.584 0.040667310357093811 9.66 0.042833913117647171 10.736 0.044958420097827918
		 11.812 0.04682265967130661 12.888 0.04815606027841568 13.968 0.04954741895198822
		 15.044 0.050901602953672409 16.12 0.051679756492376328 17.196 0.052077669650316238
		 18.272 0.052406847476959235 19.348 0.052712082862853997 20.424 0.053166609257459641
		 21.5 0.052442234009504318 22.576 0.052145585417747498 23.652 0.052024766802787781
		 24.728 0.051544051617383957 25.804 0.0515633225440979 26.88 0.051680836826562881
		 27.956 0.05183308944106102 29.032 0.051884464919567101 30.108 0.05237250030040741
		 31.184 0.05364903062582016 32.26 0.054814178496599197 33.34 0.056130491197109222
		 34.416 0.057509440928697593 35.492 0.059089697897434235 36.568 0.060899823904037476
		 37.644 0.062669694423675537 38.72 0.064353108406066895 39.796 0.066436268389225006
		 40.872 0.069089882075786591 41.948 0.071699157357215881 43.024 0.073789671063423157
		 44.1 0.075461991131305695 45.176 0.076907500624656677 46.252 0.078061044216156006
		 47.328 0.079128310084342957 48.404 0.080219589173793793 49.48 0.080857463181018829
		 50.556 0.080219097435474396 51.636 0.079189315438270583 52.712 0.079133480787277222
		 53.788 0.07954397052526474 54.864 0.079417213797569275 55.94 0.078368395566940308
		 57.016 0.076497927308082581 58.092 0.074287384748458862 59.168 0.072850167751312256
		 60.244 0.069810628890991211 61.32 0.066247187554836273 62.396 0.062367148697376237
		 63.472 0.058296870440244682 64.548 0.054243467748165131 65.624 0.05024009570479393
		 66.7 0.046380605548620224 67.776 0.042573172599077225 68.852 0.038442343473434448
		 69.932 0.034527603536844254 71.008 0.031825456768274307 72.084 0.030119702219963074;
createNode animCurveTA -n "Bip01_L_Calf_rotateZ1";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 67 ".ktv[0:66]"  1.052 -88.22235107421875 2.128 -88.379508972167969
		 3.204 -88.571220397949219 4.28 -88.725486755371094 5.356 -88.81591796875 6.432 -88.965583801269531
		 7.508 -89.168563842773438 8.584 -89.350776672363281 9.66 -89.473892211914063 10.736 -89.57574462890625
		 11.812 -89.685951232910156 12.888 -89.781768798828125 13.968 -89.868156433105469
		 15.044 -89.959190368652358 16.12 -90.021347045898437 17.196 -90.056854248046875 18.272 -90.090408325195313
		 19.348 -90.115890502929702 20.424 -90.140220642089844 21.5 -90.089828491210937 22.576 -90.065200805664062
		 23.652 -90.082283020019531 24.728 -90.088569641113281 25.804 -90.103424072265639
		 26.88 -90.120574951171875 27.956 -90.157035827636719 29.032 -90.185142517089844 30.108 -90.217155456542983
		 31.184 -90.295654296875 32.26 -90.392837524414063 33.34 -90.490928649902315 34.416 -90.5843505859375
		 35.492 -90.683074951171875 36.568 -90.791038513183622 37.644 -90.895980834960938
		 38.72 -91.001724243164062 39.796 -91.146842956542983 40.872 -91.303443908691406 41.948 -91.452461242675781
		 43.024 -91.558128356933594 44.1 -91.656692504882812 45.176 -91.75146484375 46.252 -91.824493408203125
		 47.328 -91.889091491699219 48.404 -91.935012817382813 49.48 -91.975517272949219 50.556 -91.934783935546875
		 51.636 -91.883453369140625 52.712 -91.890800476074219 53.788 -91.913871765136719
		 54.864 -91.888992309570327 55.94 -91.805595397949219 57.016 -91.682296752929702 58.092 -91.5458984375
		 59.168 -91.4522705078125 60.244 -91.281150817871108 61.32 -91.08154296875 62.396 -90.867492675781236
		 63.472 -90.625762939453125 64.548 -90.374473571777344 65.624 -90.143516540527344
		 66.7 -89.931976318359389 67.776 -89.705841064453125 68.852 -89.46026611328125 69.932 -89.225471496582031
		 71.008 -89.034568786621094 72.084 -88.897705078125;
createNode animCurveTA -n "Bip01_L_Foot_rotateX1";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 67 ".ktv[0:66]"  1.052 -10.48517894744873 2.128 -10.489912986755373
		 3.204 -10.277693748474119 4.28 -10.168293952941898 5.356 -10.506497383117676 6.432 -10.800180435180664
		 7.508 -10.595342636108398 8.584 -10.388769149780272 9.66 -10.56421947479248 10.736 -10.730945587158203
		 11.812 -10.637126922607422 12.888 -10.508214950561523 13.968 -10.519858360290527
		 15.044 -10.5933837890625 16.12 -10.659440994262697 17.196 -10.535943984985352 18.272 -10.368762016296388
		 19.348 -10.321826934814451 20.424 -10.417797088623049 21.5 -10.586258888244627 22.576 -10.702866554260254
		 23.652 -10.639822959899904 24.728 -10.642086029052734 25.804 -10.819362640380861
		 26.88 -10.985996246337892 27.956 -11.043968200683594 29.032 -11.18730640411377 30.108 -11.324974060058594
		 31.184 -11.338686943054199 32.26 -11.283706665039064 33.34 -11.346689224243164 34.416 -11.498403549194336
		 35.492 -11.675868988037108 36.568 -11.847195625305176 37.644 -12.107359886169434
		 38.72 -12.350997924804688 39.796 -12.334693908691406 40.872 -12.096714019775392 41.948 -11.996082305908203
		 43.024 -12.101204872131348 44.1 -12.201493263244627 45.176 -12.18263053894043 46.252 -12.136631965637209
		 47.328 -12.242867469787598 48.404 -12.346250534057615 49.48 -12.251128196716309 50.556 -12.151250839233398
		 51.636 -12.065643310546877 52.712 -11.776307106018066 53.788 -11.536633491516112
		 54.864 -11.480532646179199 55.94 -11.531101226806641 57.016 -11.567792892456056 58.092 -11.551609039306641
		 59.168 -11.502484321594238 60.244 -11.38322639465332 61.32 -11.335360527038574 62.396 -11.397568702697754
		 63.472 -11.399866104125977 64.548 -11.276120185852053 65.624 -11.110681533813477
		 66.7 -10.982105255126953 67.776 -10.961658477783203 68.852 -11.068967819213867 69.932 -11.198123931884766
		 71.008 -11.432745933532717 72.084 -11.574933052062988;
createNode animCurveTA -n "Bip01_L_Foot_rotateY1";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 67 ".ktv[0:66]"  1.052 2.0180692672729492 2.128 2.0341320037841797
		 3.204 1.8920508623123169 4.28 1.8292733430862429 5.356 2.0276060104370117 6.432 2.1921393871307373
		 7.508 2.0961019992828369 8.584 1.9938865900039671 9.66 2.116985559463501 10.736 2.2273368835449219
		 11.812 2.1736686229705811 12.888 2.1045234203338623 13.968 2.1201443672180176 15.044 2.1789836883544922
		 16.12 2.2221269607543945 17.196 2.1742229461669922 18.272 2.0630943775177002 19.348 2.0369985103607178
		 20.424 2.1007194519042969 21.5 2.2119266986846924 22.576 2.2843339443206787 23.652 2.2527894973754883
		 24.728 2.2527463436126709 25.804 2.3697521686553955 26.88 2.4812228679656982 27.956 2.5537147521972656
		 29.032 2.6183557510375977 30.108 2.7192752361297607 31.184 2.709647655487061 32.26 2.6923942565917969
		 33.34 2.7335579395294189 34.416 2.8199548721313477 35.492 2.9441971778869629 36.568 3.0662417411804204
		 37.644 3.2456421852111821 38.72 3.4271595478057861 39.796 3.4247312545776367 40.872 3.2660162448883057
		 41.948 3.1564311981201172 43.024 3.2649199962615971 44.1 3.3499238491058354 45.176 3.3263075351715088
		 46.252 3.3318564891815186 47.328 3.3921856880187993 48.404 3.4802086353302006 49.48 3.4278254508972168
		 50.556 3.3805370330810547 51.636 3.3137209415435791 52.712 3.1111512184143062 53.788 2.9345450401306152
		 54.864 2.9080531597137451 55.94 2.8607332706451416 57.016 2.8915457725524902 58.092 2.899494886398315
		 59.168 2.8538055419921875 60.244 2.7331357002258301 61.32 2.69921875 62.396 2.7367041110992432
		 63.472 2.7140250205993652 64.548 2.60542893409729 65.624 2.4783880710601807 66.7 2.3761651515960693
		 67.776 2.3470795154571533 68.852 2.4039945602416992 69.932 2.492509126663208 71.008 2.6300230026245117
		 72.084 2.7185041904449463;
createNode animCurveTA -n "Bip01_L_Foot_rotateZ1";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 67 ".ktv[0:66]"  1.052 2.8483991622924805 2.128 2.8074593544006348
		 3.204 2.6935017108917236 4.28 2.4696848392486572 5.356 2.1020677089691162 6.432 1.9460318088531496
		 7.508 2.331584215164185 8.584 2.8267061710357666 9.66 3.0383286476135254 10.736 3.1314735412597656
		 11.812 3.2706561088562012 12.888 3.4523913860321045 13.968 3.6862225532531734 15.044 3.9707784652709961
		 16.12 4.242218017578125 17.196 4.4113459587097168 18.272 4.4899330139160156 19.348 4.5761003494262695
		 20.424 4.6785750389099121 21.5 4.6890759468078613 22.576 4.6678800582885742 23.652 4.8811769485473633
		 24.728 5.2358436584472656 25.804 5.453521728515625 26.88 5.6367011070251465 27.956 5.9676389694213867
		 29.032 6.2425990104675293 30.108 6.2958707809448242 31.184 6.3563942909240723 32.26 6.6036505699157715
		 33.34 6.885664939880372 34.416 7.1294407844543457 35.492 7.3213543891906765 36.568 7.5038700103759757
		 37.644 7.8065128326416033 38.72 8.2148246765136719 39.796 8.4904594421386719 40.872 8.4741325378417969
		 41.948 8.3436403274536133 43.024 8.3879947662353516 44.1 8.5798616409301758 45.176 8.7624006271362305
		 46.252 8.9552316665649414 47.328 9.1018428802490234 48.404 9.1026983261108398 49.48 9.0715541839599609
		 50.556 9.0570850372314453 51.636 9.0429210662841797 52.712 8.9346456527709961 53.788 8.6992063522338867
		 54.864 8.4389123916625977 55.94 8.171320915222168 57.016 7.8773770332336426 58.092 7.6235876083374015
		 59.168 7.3642268180847132 60.244 7.0999665260314941 61.32 6.9857544898986816 62.396 6.9298229217529306
		 63.472 6.5675067901611328 64.548 5.9868588447570801 65.624 5.5949091911315918 66.7 5.3465633392333984
		 67.776 5.0933804512023926 68.852 4.9500732421875 69.932 4.8642911911010742 71.008 4.6229925155639648
		 72.084 4.3228363990783691;
createNode animCurveTA -n "Bip01_R_Thigh_rotateX1";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 67 ".ktv[0:66]"  1.052 10.348911285400392 2.128 10.260227203369142
		 3.204 10.14690113067627 4.28 10.007237434387209 5.356 9.8790073394775391 6.432 9.7701206207275391
		 7.508 9.6820659637451172 8.584 9.6073026657104492 9.66 9.5373888015747088 10.736 9.4704256057739258
		 11.812 9.4266033172607422 12.888 9.4087629318237305 13.968 9.4020967483520508 15.044 9.4035854339599609
		 16.12 9.4232969284057617 17.196 9.4352502822875977 18.272 9.4754390716552734 19.348 9.5316791534423828
		 20.424 9.570857048034668 21.5 9.5733432769775391 22.576 9.5938882827758789 23.652 9.6497592926025391
		 24.728 9.7105064392089844 25.804 9.7273225784301758 26.88 9.7093868255615234 27.956 9.6866416931152344
		 29.032 9.6553325653076172 30.108 9.6335172653198242 31.184 9.6131763458251953 32.26 9.5818119049072266
		 33.34 9.5309133529663086 34.416 9.4936075210571289 35.492 9.4722013473510742 36.568 9.4533224105834961
		 37.644 9.4464378356933594 38.72 9.4365205764770508 39.796 9.4303598403930664 40.872 9.4524621963500977
		 41.948 9.4680624008178711 43.024 9.4664640426635742 44.1 9.472966194152832 45.176 9.5153818130493164
		 46.252 9.5729465484619141 47.328 9.6198701858520508 48.404 9.6671485900878906 49.48 9.7455730438232422
		 50.556 9.8496513366699219 51.636 9.9805583953857422 52.712 10.095319747924805 53.788 10.193278312683104
		 54.864 10.275390625 55.94 10.369524002075195 57.016 10.467209815979004 58.092 10.543571472167969
		 59.168 10.615861892700195 60.244 10.716634750366213 61.32 10.80729866027832 62.396 10.863073348999023
		 63.472 10.900243759155272 64.548 10.945268630981444 65.624 10.972807884216309 66.7 11.010169029235842
		 67.776 11.08261013031006 68.852 11.148111343383787 69.932 11.209596633911133 71.008 11.25855541229248
		 72.084 11.315956115722656;
createNode animCurveTA -n "Bip01_R_Thigh_rotateY1";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 67 ".ktv[0:66]"  1.052 175.13616943359375 2.128 175.32867431640625
		 3.204 175.48518371582031 4.28 175.61892700195312 5.356 175.74044799804687 6.432 175.87289428710935
		 7.508 176.01750183105469 8.584 176.164794921875 9.66 176.27423095703125 10.736 176.37701416015625
		 11.812 176.47152709960935 12.888 176.56376647949219 13.968 176.64524841308594 15.044 176.71844482421875
		 16.12 176.76884460449219 17.196 176.79751586914062 18.272 176.82752990722656 19.348 176.85881042480469
		 20.424 176.84442138671875 21.5 176.7890625 22.576 176.71952819824219 23.652 176.67990112304688
		 24.728 176.62953186035156 25.804 176.54055786132812 26.88 176.41615295410156 27.956 176.27899169921875
		 29.032 176.13551330566406 30.108 175.99790954589844 31.184 175.86965942382812 32.26 175.73884582519531
		 33.34 175.59951782226562 34.416 175.47004699707031 35.492 175.3671875 36.568 175.28688049316409
		 37.644 175.21922302246094 38.72 175.15834045410156 39.796 175.11100769042969 40.872 175.08308410644531
		 41.948 175.0626220703125 43.024 175.01768493652344 44.1 174.97265625 45.176 174.95704650878906
		 46.252 174.95254516601562 47.328 174.92997741699219 48.404 174.886962890625 49.48 174.87203979492187
		 50.556 174.86746215820313 51.636 174.86105346679687 52.712 174.84309387207031 53.788 174.78337097167969
		 54.864 174.70231628417969 55.94 174.62034606933594 57.016 174.52447509765625 58.092 174.41316223144531
		 59.168 174.28781127929687 60.244 174.18931579589844 61.32 174.09820556640625 62.396 173.96768188476562
		 63.472 173.83430480957031 64.548 173.70480346679687 65.624 173.59930419921875 66.7 173.52249145507812
		 67.776 173.52072143554687 68.852 173.56614685058594 69.932 173.62490844726562 71.008 173.71623229980472
		 72.084 173.86094665527344;
createNode animCurveTA -n "Bip01_R_Thigh_rotateZ1";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 67 ".ktv[0:66]"  1.052 -84.190628051757813 2.128 -84.43766021728517
		 3.204 -84.694366455078125 4.28 -84.969459533691406 5.356 -85.233268737792983 6.432 -85.481040954589844
		 7.508 -85.7454833984375 8.584 -86.024833679199219 9.66 -86.257553100585938 10.736 -86.435844421386719
		 11.812 -86.591903686523438 12.888 -86.729652404785156 13.968 -86.843635559082045
		 15.044 -86.942649841308594 16.12 -87.040130615234375 17.196 -87.1214599609375 18.272 -87.174629211425781
		 19.348 -87.223098754882813 20.424 -87.285224914550781 21.5 -87.364974975585937 22.576 -87.3887939453125
		 23.652 -87.381935119628906 24.728 -87.377975463867188 25.804 -87.406387329101563
		 26.88 -87.452812194824219 27.956 -87.479499816894531 29.032 -87.458625793457017 30.108 -87.455513000488281
		 31.184 -87.476387023925781 32.26 -87.489936828613281 33.34 -87.484855651855469 34.416 -87.488243103027344
		 35.492 -87.47698974609375 36.568 -87.464309692382813 37.644 -87.438682556152358 38.72 -87.398956298828125
		 39.796 -87.381912231445313 40.872 -87.35882568359375 41.948 -87.333114624023437 43.024 -87.288253784179673
		 44.1 -87.232627868652358 45.176 -87.170722961425781 46.252 -87.109413146972656 47.328 -87.062370300292983
		 48.404 -87.016220092773438 49.48 -86.931388854980469 50.556 -86.801292419433594 51.636 -86.653350830078125
		 52.712 -86.541305541992188 53.788 -86.478164672851562 54.864 -86.380035400390625
		 55.94 -86.226951599121094 57.016 -86.054420471191406 58.092 -85.8721923828125 59.168 -85.677886962890625
		 60.244 -85.503501892089844 61.32 -85.35870361328125 62.396 -85.235809326171875 63.472 -85.075927734375
		 64.548 -84.887908935546875 65.624 -84.683982849121094 66.7 -84.497360229492187 67.776 -84.406272888183594
		 68.852 -84.376655578613281 69.932 -84.339065551757812 71.008 -84.312393188476563
		 72.084 -84.309638977050781;
createNode animCurveTA -n "Bip01_R_Calf_rotateX1";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 67 ".ktv[0:66]"  1.052 -0.92169159650802623 2.128 -0.92201656103134155
		 3.204 -0.922435462474823 4.28 -0.92288422584533703 5.356 -0.92333352565765381 6.432 -0.92384856939315796
		 7.508 -0.92434555292129561 8.584 -0.92479914426803578 9.66 -0.92515569925308228 10.736 -0.92541259527206432
		 11.812 -0.92562568187713612 12.888 -0.92578864097595237 13.968 -0.92598074674606323
		 15.044 -0.92605495452880893 16.12 -0.92613786458969105 17.196 -0.92616909742355358
		 18.272 -0.92620146274566639 19.348 -0.92622864246368453 20.424 -0.92623573541641213
		 21.5 -0.92626631259918213 22.576 -0.92628663778305054 23.652 -0.92623871564865101
		 24.728 -0.9262317419052124 25.804 -0.92624062299728394 26.88 -0.92627233266830455
		 27.956 -0.92633461952209473 29.032 -0.92633634805679343 30.108 -0.92647892236709595
		 31.184 -0.92659890651702892 32.26 -0.92672097682952892 33.34 -0.92681574821472179
		 34.416 -0.92696845531463612 35.492 -0.92705792188644409 36.568 -0.92712098360061646
		 37.644 -0.92720419168472279 38.72 -0.92734849452972412 39.796 -0.92746955156326283
		 40.872 -0.9275052547454834 41.948 -0.92753303050994884 43.024 -0.92763805389404297
		 44.1 -0.92768210172653187 45.176 -0.92769140005111717 46.252 -0.92763292789459229
		 47.328 -0.92762851715087891 48.404 -0.9276554584503176 49.48 -0.92764782905578624
		 50.556 -0.92750012874603271 51.636 -0.92734229564666759 52.712 -0.92723858356475852
		 53.788 -0.92719852924346924 54.864 -0.9271271824836731 55.94 -0.92704242467880271
		 57.016 -0.92690551280975364 58.092 -0.9266563057899474 59.168 -0.92632216215133656
		 60.244 -0.92595613002777111 61.32 -0.92558932304382335 62.396 -0.92524492740631104
		 63.472 -0.92480766773223888 64.548 -0.92428833246231068 65.624 -0.9238108992576598
		 66.7 -0.92333394289016735 67.776 -0.92278093099594105 68.852 -0.92211729288101185
		 69.932 -0.92149853706359863 71.008 -0.92104774713516246 72.084 -0.92058253288269043;
createNode animCurveTA -n "Bip01_R_Calf_rotateY1";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 67 ".ktv[0:66]"  1.052 0.16913038492202759 2.128 0.16698427498340607
		 3.204 0.16432382166385651 4.28 0.16154159605503082 5.356 0.15862266719341278 6.432 0.15525296330451965
		 7.508 0.15177448093891144 8.584 0.14857657253742218 9.66 0.14601929485797882 10.736 0.14417487382888794
		 11.812 0.14265727996826175 12.888 0.14156079292297363 13.968 0.14037090539932251
		 15.044 0.13996602594852448 16.12 0.13932454586029053 17.196 0.13911451399326327 18.272 0.13884711265563965
		 19.348 0.13865426182746887 20.424 0.13854683935642242 21.5 0.13857088983058927 22.576 0.13853326439857486
		 23.652 0.13899894058704376 24.728 0.13908395171165466 25.804 0.13888789713382721
		 26.88 0.13844488561153415 27.956 0.13807442784309387 29.032 0.13841880857944489 30.108 0.13745671510696411
		 31.184 0.13659627735614777 32.26 0.13579879701137543 33.34 0.13491600751876831 34.416 0.1339518129825592
		 35.492 0.13314171135425568 36.568 0.13261635601520538 37.644 0.1321384608745575 38.72 0.13112743198871613
		 39.796 0.13015861809253693 40.872 0.12946650385856628 41.948 0.12927611172199249
		 43.024 0.12858648598194122 44.1 0.12840047478675842 45.176 0.12846718728542328 46.252 0.12867850065231323
		 47.328 0.12871702015399933 48.404 0.1285783052444458 49.48 0.1285146027803421 50.556 0.12984217703342438
		 51.636 0.13135670125484469 52.712 0.1319119781255722 53.788 0.13171631097793579 54.864 0.13202407956123352
		 55.94 0.1331009715795517 57.016 0.13470995426177981 58.092 0.13691006600856781 59.168 0.13968890905380249
		 60.244 0.14254516363143921 61.32 0.14516247808933258 62.396 0.14759869873523712 63.472 0.15082019567489624
		 64.548 0.15458524227142334 65.624 0.15791819989681244 66.7 0.16095961630344391 67.776 0.16448971629142761
		 68.852 0.16863895952701569 69.932 0.17258380353450775 71.008 0.1755010932683945 72.084 0.17814554274082184;
createNode animCurveTA -n "Bip01_R_Calf_rotateZ1";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 67 ".ktv[0:66]"  1.052 -78.855506896972656 2.128 -78.95926666259767
		 3.204 -79.098274230957017 4.28 -79.263114929199219 5.356 -79.422866821289062 6.432 -79.617362976074219
		 7.508 -79.799331665039063 8.584 -79.969200134277344 9.66 -80.102157592773438 10.736 -80.206840515136719
		 11.812 -80.29168701171875 12.888 -80.365852355957017 13.968 -80.457229614257813 15.044 -80.496635437011719
		 16.12 -80.529411315917969 17.196 -80.54520416259767 18.272 -80.56201171875 19.348 -80.574539184570327
		 20.424 -80.570335388183594 21.5 -80.599845886230469 22.576 -80.614402770996094 23.652 -80.601760864257813
		 24.728 -80.59637451171875 25.804 -80.592483520507827 26.88 -80.596878051757813 27.956 -80.623634338378906
		 29.032 -80.646629333496094 30.108 -80.717567443847642 31.184 -80.772392272949219
		 32.26 -80.823883056640625 33.34 -80.862045288085938 34.416 -80.934776306152372 35.492 -80.974288940429702
		 36.568 -80.994857788085938 37.644 -81.034210205078125 38.72 -81.103805541992187 39.796 -81.162063598632813
		 40.872 -81.161529541015625 41.948 -81.169288635253906 43.024 -81.227912902832031
		 44.1 -81.258949279785156 45.176 -81.269142150878906 46.252 -81.234786987304688 47.328 -81.230270385742187
		 48.404 -81.240974426269531 49.48 -81.234405517578125 50.556 -81.171905517578125 51.636 -81.115898132324219
		 52.712 -81.057769775390625 53.788 -81.015296936035156 54.864 -80.970489501953125
		 55.94 -80.956253051757813 57.016 -80.9248046875 58.092 -80.836822509765625 59.168 -80.709419250488281
		 60.244 -80.566825866699219 61.32 -80.419158935546875 62.396 -80.286819458007812 63.472 -80.12530517578125
		 64.548 -79.947212219238281 65.624 -79.780708312988281 66.7 -79.606582641601563 67.776 -79.408836364746094
		 68.852 -79.176223754882813 69.932 -78.981178283691406 71.008 -78.8525390625 72.084 -78.693618774414063;
createNode animCurveTA -n "Bip01_R_Foot_rotateX1";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 67 ".ktv[0:66]"  1.052 6.6238470077514648 2.128 6.8217463493347168
		 3.204 6.8222680091857919 4.28 6.4928526878356934 5.356 6.2777915000915527 6.432 6.2565813064575195
		 7.508 6.2846255302429199 8.584 6.41414499282837 9.66 6.4367351531982422 10.736 6.3868861198425293
		 11.812 6.3386430740356445 12.888 6.271756649017334 13.968 6.2049417495727539 15.044 6.2437601089477539
		 16.12 6.3094367980957031 17.196 6.3431382179260254 18.272 6.4107818603515625 19.348 6.6521759033203125
		 20.424 6.6478481292724609 21.5 6.4080619812011719 22.576 6.2405014038085938 23.652 6.352259635925293
		 24.728 6.4977550506591797 25.804 6.5724577903747559 26.88 6.4251689910888672 27.956 6.2083282470703125
		 29.032 6.0694537162780762 30.108 5.9587869644165039 31.184 5.9596047401428223 32.26 5.8326926231384286
		 33.34 5.6962594985961914 34.416 5.6303434371948242 35.492 5.7094182968139657 36.568 5.7838335037231445
		 37.644 5.8462424278259277 38.72 5.8908209800720215 39.796 5.9843077659606934 40.872 6.1188373565673828
		 41.948 6.2370448112487802 43.024 6.1226425170898437 44.1 6.011195182800293 45.176 6.0110478401184082
		 46.252 6.028141975402832 47.328 6.0288405418395996 48.404 6.0035829544067383 49.48 5.9647259712219238
		 50.556 5.9917140007019043 51.636 6.1301031112670898 52.712 6.351531982421875 53.788 6.5238933563232422
		 54.864 6.5601630210876465 55.94 6.6208534240722656 57.016 6.6208000183105478 58.092 6.421380043029786
		 59.168 6.1520628929138184 60.244 6.1866793632507324 61.32 6.3234810829162598 62.396 6.2656106948852539
		 63.472 6.046931266784668 64.548 5.7853412628173828 65.624 5.7827434539794922 66.7 5.8750052452087402
		 67.776 5.9726276397705078 68.852 6.0200090408325195 69.932 5.9976649284362793 71.008 5.9700074195861816
		 72.084 6.0796360969543457;
createNode animCurveTA -n "Bip01_R_Foot_rotateY1";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 67 ".ktv[0:66]"  1.052 1.2662909030914309 2.128 1.1657055616378784
		 3.204 1.2010409832000732 4.28 1.3263076543807983 5.356 1.4330192804336548 6.432 1.4797872304916384
		 7.508 1.4403368234634399 8.584 1.4271489381790159 9.66 1.3739006519317627 10.736 1.4148659706115725
		 11.812 1.460371255874634 12.888 1.4872670173645022 13.968 1.5107210874557495 15.044 1.4864227771759033
		 16.12 1.4399800300598145 17.196 1.4746814966201782 18.272 1.422656774520874 19.348 1.3037246465682983
		 20.424 1.30003821849823 21.5 1.4375736713409424 22.576 1.5199635028839111 23.652 1.4807744026184082
		 24.728 1.3891019821166992 25.804 1.3724789619445801 26.88 1.4663962125778198 27.956 1.5572774410247805
		 29.032 1.6469807624816897 30.108 1.6875234842300415 31.184 1.6967142820358276 32.26 1.7469578981399536
		 33.34 1.8366819620132449 34.416 1.8803417682647705 35.492 1.8104623556137087 36.568 1.7625967264175415
		 37.644 1.7655342817306523 38.72 1.7540074586868286 39.796 1.6728023290634155 40.872 1.5705615282058716
		 41.948 1.5225762128829956 43.024 1.5862798690795898 44.1 1.655786395072937 45.176 1.6888395547866819
		 46.252 1.645620584487915 47.328 1.6497297286987305 48.404 1.6825014352798462 49.48 1.6910367012023926
		 50.556 1.6940147876739504 51.636 1.6267201900482178 52.712 1.4954096078872681 53.788 1.4061517715454102
		 54.864 1.3935524225234983 55.94 1.3712515830993652 57.016 1.3990319967269895 58.092 1.5134371519088743
		 59.168 1.6614638566970823 60.244 1.6332353353500366 61.32 1.5505915880203249 62.396 1.5791045427322388
		 63.472 1.7482724189758301 64.548 1.8691338300704956 65.624 1.8625077009201048 66.7 1.8023848533630371
		 67.776 1.7470701932907104 68.852 1.7539190053939819 69.932 1.7233325242996216 71.008 1.742158055305481
		 72.084 1.6928030252456665;
createNode animCurveTA -n "Bip01_R_Foot_rotateZ1";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 67 ".ktv[0:66]"  1.052 -2.4999122619628902 2.128 -2.5638983249664307
		 3.204 -2.6524012088775635 4.28 -2.7659764289855957 5.356 -2.7767362594604492 6.432 -2.7159969806671143
		 7.508 -2.7297050952911377 8.584 -2.8357841968536377 9.66 -2.9143099784851074 10.736 -2.929768323898315
		 11.812 -2.9558022022247314 12.888 -2.8946611881256104 13.968 -2.701484203338623 15.044 -2.5323476791381836
		 16.12 -2.5129458904266357 17.196 -2.5536842346191406 18.272 -2.4845874309539795 19.348 -2.3487720489501953
		 20.424 -2.3764033317565918 21.5 -2.398369312286377 22.576 -2.2353541851043701 23.652 -2.0284056663513184
		 24.728 -1.9576940536499017 25.804 -2.0334124565124512 26.88 -2.1874089241027832 27.956 -2.187455415725708
		 29.032 -1.9080934524536135 30.108 -1.622958779335022 31.184 -1.474341869354248 32.26 -1.3926193714141846
		 33.34 -1.3352639675140383 34.416 -1.2563431262969973 35.492 -1.1641751527786257 36.568 -1.0702139139175415
		 37.644 -0.87911140918731701 38.72 -0.60235047340393066 39.796 -0.49058535695075983
		 40.872 -0.58764058351516724 41.948 -0.58599609136581421 43.024 -0.42630597949028015
		 44.1 -0.2896786630153656 45.176 -0.29802313446998596 46.252 -0.40821203589439392
		 47.328 -0.49129226803779602 48.404 -0.54422014951705933 49.48 -0.57898759841918945
		 50.556 -0.51987391710281372 51.636 -0.41414064168930054 52.712 -0.51525038480758667
		 53.788 -0.94688880443572998 54.864 -1.1046195030212402 55.94 -0.64036136865615845
		 57.016 -0.14793774485588074 58.092 -0.024392895400524139 59.168 0.04066167026758194
		 60.244 0.20703426003456116 61.32 0.28429019451141357 62.396 0.25723406672477722 63.472 0.27797490358352661
		 64.548 0.2913987934589386 65.624 0.38175854086875921 66.7 0.48196294903755182 67.776 0.40304803848266602
		 68.852 0.30869394540786743 69.932 0.38782855868339539 71.008 0.46598035097122176
		 72.084 0.39340084791183472;
select -ne :time1;
	setAttr ".o" 24.2;
	setAttr ".unw" 24.2;
select -ne :renderPartition;
	setAttr -s 126 ".st";
select -ne :initialShadingGroup;
	setAttr ".ro" yes;
select -ne :initialParticleSE;
	setAttr ".ro" yes;
select -ne :defaultShaderList1;
	setAttr -s 64 ".s";
select -ne :defaultTextureList1;
	setAttr -s 31 ".tx";
select -ne :postProcessList1;
	setAttr -s 2 ".p";
select -ne :defaultRenderUtilityList1;
	setAttr -s 31 ".u";
select -ne :defaultRenderingList1;
select -ne :renderGlobalsList1;
select -ne :defaultResolution;
	setAttr ".pa" 1;
select -ne :hardwareRenderGlobals;
	setAttr ".ctrs" 256;
	setAttr ".btrs" 512;
select -ne :hardwareRenderingGlobals;
	setAttr ".otfna" -type "stringArray" 18 "NURBS Curves" "NURBS Surfaces" "Polygons" "Subdiv Surfaces" "Particles" "Fluids" "Image Planes" "UI:" "Lights" "Cameras" "Locators" "Joints" "IK Handles" "Deformers" "Motion Trails" "Components" "Misc. UI" "Ornaments"  ;
	setAttr ".otfva" -type "Int32Array" 18 0 1 1 1 1 1
		 1 0 0 0 0 0 0 0 0 0 0 0 ;
select -ne :defaultHardwareRenderGlobals;
	setAttr ".fn" -type "string" "im";
	setAttr ".res" -type "string" "ntsc_4d 646 485 1.333";
select -ne :hyperGraphLayout;
	setAttr -s 4 ".hyp";
	setAttr ".hyp[1].isc" yes;
	setAttr ".hyp[2].isc" yes;
	setAttr ".hyp[3].isc" yes;
	setAttr ".hyp[4].isc" yes;
connectAttr "Bip01_Spine_translateX1.o" "|Sit_Idle_1|Bip01_Spine.tx";
connectAttr "Bip01_Spine_translateY1.o" "|Sit_Idle_1|Bip01_Spine.ty";
connectAttr "Bip01_Spine_translateZ1.o" "|Sit_Idle_1|Bip01_Spine.tz";
connectAttr "Bip01_Spine_rotateX1.o" "|Sit_Idle_1|Bip01_Spine.rx";
connectAttr "Bip01_Spine_rotateY1.o" "|Sit_Idle_1|Bip01_Spine.ry";
connectAttr "Bip01_Spine_rotateZ1.o" "|Sit_Idle_1|Bip01_Spine.rz";
connectAttr "|Sit_Idle_1|Bip01_Spine.s" "|Sit_Idle_1|Bip01_Spine|Bip01_Spine1.is"
		;
connectAttr "Bip01_Spine1_rotateX1.o" "|Sit_Idle_1|Bip01_Spine|Bip01_Spine1.rx";
connectAttr "Bip01_Spine1_rotateY1.o" "|Sit_Idle_1|Bip01_Spine|Bip01_Spine1.ry";
connectAttr "Bip01_Spine1_rotateZ1.o" "|Sit_Idle_1|Bip01_Spine|Bip01_Spine1.rz";
connectAttr "|Sit_Idle_1|Bip01_Spine|Bip01_Spine1.s" "|Sit_Idle_1|Bip01_Spine|Bip01_Spine1|Bip01_Spine2.is"
		;
connectAttr "Bip01_Spine2_rotateX1.o" "|Sit_Idle_1|Bip01_Spine|Bip01_Spine1|Bip01_Spine2.rx"
		;
connectAttr "Bip01_Spine2_rotateY1.o" "|Sit_Idle_1|Bip01_Spine|Bip01_Spine1|Bip01_Spine2.ry"
		;
connectAttr "Bip01_Spine2_rotateZ1.o" "|Sit_Idle_1|Bip01_Spine|Bip01_Spine1|Bip01_Spine2.rz"
		;
connectAttr "|Sit_Idle_1|Bip01_Spine|Bip01_Spine1|Bip01_Spine2.s" "|Sit_Idle_1|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck.is"
		;
connectAttr "|Sit_Idle_1|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck.s" "|Sit_Idle_1|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_Head.is"
		;
connectAttr "|Sit_Idle_1|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_Head.s" "|Sit_Idle_1|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_Head|_notused_Bip01_HeadNub.is"
		;
connectAttr "|Sit_Idle_1|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck.s" "|Sit_Idle_1|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_R_Clavicle.is"
		;
connectAttr "Bip01_R_Clavicle_rotateX1.o" "|Sit_Idle_1|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_R_Clavicle.rx"
		;
connectAttr "Bip01_R_Clavicle_rotateY1.o" "|Sit_Idle_1|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_R_Clavicle.ry"
		;
connectAttr "Bip01_R_Clavicle_rotateZ1.o" "|Sit_Idle_1|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_R_Clavicle.rz"
		;
connectAttr "|Sit_Idle_1|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_R_Clavicle.s" "|Sit_Idle_1|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_R_Clavicle|Bip01_R_UpperArm.is"
		;
connectAttr "Bip01_R_UpperArm_rotateX1.o" "|Sit_Idle_1|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_R_Clavicle|Bip01_R_UpperArm.rx"
		;
connectAttr "Bip01_R_UpperArm_rotateY1.o" "|Sit_Idle_1|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_R_Clavicle|Bip01_R_UpperArm.ry"
		;
connectAttr "Bip01_R_UpperArm_rotateZ1.o" "|Sit_Idle_1|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_R_Clavicle|Bip01_R_UpperArm.rz"
		;
connectAttr "|Sit_Idle_1|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_R_Clavicle|Bip01_R_UpperArm.s" "|Sit_Idle_1|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_R_Clavicle|Bip01_R_UpperArm|Bip01_R_Forearm.is"
		;
connectAttr "Bip01_R_Forearm_rotateX1.o" "|Sit_Idle_1|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_R_Clavicle|Bip01_R_UpperArm|Bip01_R_Forearm.rx"
		;
connectAttr "Bip01_R_Forearm_rotateY1.o" "|Sit_Idle_1|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_R_Clavicle|Bip01_R_UpperArm|Bip01_R_Forearm.ry"
		;
connectAttr "Bip01_R_Forearm_rotateZ1.o" "|Sit_Idle_1|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_R_Clavicle|Bip01_R_UpperArm|Bip01_R_Forearm.rz"
		;
connectAttr "|Sit_Idle_1|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_R_Clavicle|Bip01_R_UpperArm|Bip01_R_Forearm.s" "|Sit_Idle_1|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_R_Clavicle|Bip01_R_UpperArm|Bip01_R_Forearm|Bip01_R_Hand.is"
		;
connectAttr "|Sit_Idle_1|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_R_Clavicle|Bip01_R_UpperArm|Bip01_R_Forearm|Bip01_R_Hand.s" "|Sit_Idle_1|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_R_Clavicle|Bip01_R_UpperArm|Bip01_R_Forearm|Bip01_R_Hand|Bip01_R_Finger0.is"
		;
connectAttr "|Sit_Idle_1|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_R_Clavicle|Bip01_R_UpperArm|Bip01_R_Forearm|Bip01_R_Hand|Bip01_R_Finger0.s" "|Sit_Idle_1|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_R_Clavicle|Bip01_R_UpperArm|Bip01_R_Forearm|Bip01_R_Hand|Bip01_R_Finger0|Bip01_R_Finger01.is"
		;
connectAttr "|Sit_Idle_1|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_R_Clavicle|Bip01_R_UpperArm|Bip01_R_Forearm|Bip01_R_Hand|Bip01_R_Finger0|Bip01_R_Finger01.s" "|Sit_Idle_1|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_R_Clavicle|Bip01_R_UpperArm|Bip01_R_Forearm|Bip01_R_Hand|Bip01_R_Finger0|Bip01_R_Finger01|_notused_Bip01_R_Finger0Nub.is"
		;
connectAttr "|Sit_Idle_1|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_R_Clavicle|Bip01_R_UpperArm|Bip01_R_Forearm|Bip01_R_Hand.s" "|Sit_Idle_1|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_R_Clavicle|Bip01_R_UpperArm|Bip01_R_Forearm|Bip01_R_Hand|Bip01_R_Finger2.is"
		;
connectAttr "|Sit_Idle_1|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_R_Clavicle|Bip01_R_UpperArm|Bip01_R_Forearm|Bip01_R_Hand|Bip01_R_Finger2.s" "|Sit_Idle_1|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_R_Clavicle|Bip01_R_UpperArm|Bip01_R_Forearm|Bip01_R_Hand|Bip01_R_Finger2|Bip01_R_Finger21.is"
		;
connectAttr "|Sit_Idle_1|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_R_Clavicle|Bip01_R_UpperArm|Bip01_R_Forearm|Bip01_R_Hand|Bip01_R_Finger2|Bip01_R_Finger21.s" "|Sit_Idle_1|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_R_Clavicle|Bip01_R_UpperArm|Bip01_R_Forearm|Bip01_R_Hand|Bip01_R_Finger2|Bip01_R_Finger21|_notused_Bip01_R_Finger2Nub.is"
		;
connectAttr "|Sit_Idle_1|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_R_Clavicle|Bip01_R_UpperArm|Bip01_R_Forearm|Bip01_R_Hand.s" "|Sit_Idle_1|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_R_Clavicle|Bip01_R_UpperArm|Bip01_R_Forearm|Bip01_R_Hand|Bip01_R_Finger1.is"
		;
connectAttr "|Sit_Idle_1|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_R_Clavicle|Bip01_R_UpperArm|Bip01_R_Forearm|Bip01_R_Hand|Bip01_R_Finger1.s" "|Sit_Idle_1|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_R_Clavicle|Bip01_R_UpperArm|Bip01_R_Forearm|Bip01_R_Hand|Bip01_R_Finger1|Bip01_R_Finger11.is"
		;
connectAttr "|Sit_Idle_1|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_R_Clavicle|Bip01_R_UpperArm|Bip01_R_Forearm|Bip01_R_Hand|Bip01_R_Finger1|Bip01_R_Finger11.s" "|Sit_Idle_1|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_R_Clavicle|Bip01_R_UpperArm|Bip01_R_Forearm|Bip01_R_Hand|Bip01_R_Finger1|Bip01_R_Finger11|_notused_Bip01_R_Finger1Nub.is"
		;
connectAttr "|Sit_Idle_1|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_R_Clavicle|Bip01_R_UpperArm|Bip01_R_Forearm|Bip01_R_Hand.s" "|Sit_Idle_1|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_R_Clavicle|Bip01_R_UpperArm|Bip01_R_Forearm|Bip01_R_Hand|Bip01_R_Finger3.is"
		;
connectAttr "|Sit_Idle_1|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_R_Clavicle|Bip01_R_UpperArm|Bip01_R_Forearm|Bip01_R_Hand|Bip01_R_Finger3.s" "|Sit_Idle_1|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_R_Clavicle|Bip01_R_UpperArm|Bip01_R_Forearm|Bip01_R_Hand|Bip01_R_Finger3|Bip01_R_Finger31.is"
		;
connectAttr "|Sit_Idle_1|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_R_Clavicle|Bip01_R_UpperArm|Bip01_R_Forearm|Bip01_R_Hand|Bip01_R_Finger3|Bip01_R_Finger31.s" "|Sit_Idle_1|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_R_Clavicle|Bip01_R_UpperArm|Bip01_R_Forearm|Bip01_R_Hand|Bip01_R_Finger3|Bip01_R_Finger31|_notused_Bip01_R_Finger3Nub.is"
		;
connectAttr "|Sit_Idle_1|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_R_Clavicle|Bip01_R_UpperArm|Bip01_R_Forearm|Bip01_R_Hand.s" "|Sit_Idle_1|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_R_Clavicle|Bip01_R_UpperArm|Bip01_R_Forearm|Bip01_R_Hand|Bip01_R_Finger4.is"
		;
connectAttr "|Sit_Idle_1|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_R_Clavicle|Bip01_R_UpperArm|Bip01_R_Forearm|Bip01_R_Hand|Bip01_R_Finger4.s" "|Sit_Idle_1|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_R_Clavicle|Bip01_R_UpperArm|Bip01_R_Forearm|Bip01_R_Hand|Bip01_R_Finger4|Bip01_R_Finger41.is"
		;
connectAttr "|Sit_Idle_1|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_R_Clavicle|Bip01_R_UpperArm|Bip01_R_Forearm|Bip01_R_Hand|Bip01_R_Finger4|Bip01_R_Finger41.s" "|Sit_Idle_1|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_R_Clavicle|Bip01_R_UpperArm|Bip01_R_Forearm|Bip01_R_Hand|Bip01_R_Finger4|Bip01_R_Finger41|_notused_Bip01_R_Finger4Nub.is"
		;
connectAttr "|Sit_Idle_1|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck.s" "|Sit_Idle_1|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_L_Clavicle.is"
		;
connectAttr "Bip01_L_Clavicle_rotateX1.o" "|Sit_Idle_1|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_L_Clavicle.rx"
		;
connectAttr "Bip01_L_Clavicle_rotateY1.o" "|Sit_Idle_1|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_L_Clavicle.ry"
		;
connectAttr "Bip01_L_Clavicle_rotateZ1.o" "|Sit_Idle_1|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_L_Clavicle.rz"
		;
connectAttr "|Sit_Idle_1|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_L_Clavicle.s" "|Sit_Idle_1|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_L_Clavicle|Bip01_L_UpperArm.is"
		;
connectAttr "Bip01_L_UpperArm_rotateX1.o" "|Sit_Idle_1|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_L_Clavicle|Bip01_L_UpperArm.rx"
		;
connectAttr "Bip01_L_UpperArm_rotateY1.o" "|Sit_Idle_1|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_L_Clavicle|Bip01_L_UpperArm.ry"
		;
connectAttr "Bip01_L_UpperArm_rotateZ1.o" "|Sit_Idle_1|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_L_Clavicle|Bip01_L_UpperArm.rz"
		;
connectAttr "|Sit_Idle_1|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_L_Clavicle|Bip01_L_UpperArm.s" "|Sit_Idle_1|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_L_Clavicle|Bip01_L_UpperArm|Bip01_L_Forearm.is"
		;
connectAttr "Bip01_L_Forearm_rotateX1.o" "|Sit_Idle_1|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_L_Clavicle|Bip01_L_UpperArm|Bip01_L_Forearm.rx"
		;
connectAttr "Bip01_L_Forearm_rotateY1.o" "|Sit_Idle_1|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_L_Clavicle|Bip01_L_UpperArm|Bip01_L_Forearm.ry"
		;
connectAttr "Bip01_L_Forearm_rotateZ1.o" "|Sit_Idle_1|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_L_Clavicle|Bip01_L_UpperArm|Bip01_L_Forearm.rz"
		;
connectAttr "|Sit_Idle_1|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_L_Clavicle|Bip01_L_UpperArm|Bip01_L_Forearm.s" "|Sit_Idle_1|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_L_Clavicle|Bip01_L_UpperArm|Bip01_L_Forearm|Bip01_L_Hand.is"
		;
connectAttr "Bip01_L_Hand_rotateX1.o" "|Sit_Idle_1|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_L_Clavicle|Bip01_L_UpperArm|Bip01_L_Forearm|Bip01_L_Hand.rx"
		;
connectAttr "Bip01_L_Hand_rotateY1.o" "|Sit_Idle_1|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_L_Clavicle|Bip01_L_UpperArm|Bip01_L_Forearm|Bip01_L_Hand.ry"
		;
connectAttr "Bip01_L_Hand_rotateZ1.o" "|Sit_Idle_1|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_L_Clavicle|Bip01_L_UpperArm|Bip01_L_Forearm|Bip01_L_Hand.rz"
		;
connectAttr "|Sit_Idle_1|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_L_Clavicle|Bip01_L_UpperArm|Bip01_L_Forearm|Bip01_L_Hand.s" "|Sit_Idle_1|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_L_Clavicle|Bip01_L_UpperArm|Bip01_L_Forearm|Bip01_L_Hand|Bip01_L_Finger0.is"
		;
connectAttr "|Sit_Idle_1|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_L_Clavicle|Bip01_L_UpperArm|Bip01_L_Forearm|Bip01_L_Hand|Bip01_L_Finger0.s" "|Sit_Idle_1|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_L_Clavicle|Bip01_L_UpperArm|Bip01_L_Forearm|Bip01_L_Hand|Bip01_L_Finger0|Bip01_L_Finger01.is"
		;
connectAttr "|Sit_Idle_1|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_L_Clavicle|Bip01_L_UpperArm|Bip01_L_Forearm|Bip01_L_Hand|Bip01_L_Finger0|Bip01_L_Finger01.s" "|Sit_Idle_1|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_L_Clavicle|Bip01_L_UpperArm|Bip01_L_Forearm|Bip01_L_Hand|Bip01_L_Finger0|Bip01_L_Finger01|_notused_Bip01_L_Finger0Nub.is"
		;
connectAttr "|Sit_Idle_1|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_L_Clavicle|Bip01_L_UpperArm|Bip01_L_Forearm|Bip01_L_Hand.s" "|Sit_Idle_1|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_L_Clavicle|Bip01_L_UpperArm|Bip01_L_Forearm|Bip01_L_Hand|Bip01_L_Finger2.is"
		;
connectAttr "|Sit_Idle_1|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_L_Clavicle|Bip01_L_UpperArm|Bip01_L_Forearm|Bip01_L_Hand|Bip01_L_Finger2.s" "|Sit_Idle_1|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_L_Clavicle|Bip01_L_UpperArm|Bip01_L_Forearm|Bip01_L_Hand|Bip01_L_Finger2|Bip01_L_Finger21.is"
		;
connectAttr "|Sit_Idle_1|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_L_Clavicle|Bip01_L_UpperArm|Bip01_L_Forearm|Bip01_L_Hand|Bip01_L_Finger2|Bip01_L_Finger21.s" "|Sit_Idle_1|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_L_Clavicle|Bip01_L_UpperArm|Bip01_L_Forearm|Bip01_L_Hand|Bip01_L_Finger2|Bip01_L_Finger21|_notused_Bip01_L_Finger2Nub.is"
		;
connectAttr "|Sit_Idle_1|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_L_Clavicle|Bip01_L_UpperArm|Bip01_L_Forearm|Bip01_L_Hand.s" "|Sit_Idle_1|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_L_Clavicle|Bip01_L_UpperArm|Bip01_L_Forearm|Bip01_L_Hand|Bip01_L_Finger1.is"
		;
connectAttr "|Sit_Idle_1|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_L_Clavicle|Bip01_L_UpperArm|Bip01_L_Forearm|Bip01_L_Hand|Bip01_L_Finger1.s" "|Sit_Idle_1|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_L_Clavicle|Bip01_L_UpperArm|Bip01_L_Forearm|Bip01_L_Hand|Bip01_L_Finger1|Bip01_L_Finger11.is"
		;
connectAttr "|Sit_Idle_1|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_L_Clavicle|Bip01_L_UpperArm|Bip01_L_Forearm|Bip01_L_Hand|Bip01_L_Finger1|Bip01_L_Finger11.s" "|Sit_Idle_1|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_L_Clavicle|Bip01_L_UpperArm|Bip01_L_Forearm|Bip01_L_Hand|Bip01_L_Finger1|Bip01_L_Finger11|_notused_Bip01_L_Finger1Nub.is"
		;
connectAttr "|Sit_Idle_1|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_L_Clavicle|Bip01_L_UpperArm|Bip01_L_Forearm|Bip01_L_Hand.s" "|Sit_Idle_1|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_L_Clavicle|Bip01_L_UpperArm|Bip01_L_Forearm|Bip01_L_Hand|Bip01_L_Finger3.is"
		;
connectAttr "|Sit_Idle_1|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_L_Clavicle|Bip01_L_UpperArm|Bip01_L_Forearm|Bip01_L_Hand|Bip01_L_Finger3.s" "|Sit_Idle_1|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_L_Clavicle|Bip01_L_UpperArm|Bip01_L_Forearm|Bip01_L_Hand|Bip01_L_Finger3|Bip01_L_Finger31.is"
		;
connectAttr "|Sit_Idle_1|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_L_Clavicle|Bip01_L_UpperArm|Bip01_L_Forearm|Bip01_L_Hand|Bip01_L_Finger3|Bip01_L_Finger31.s" "|Sit_Idle_1|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_L_Clavicle|Bip01_L_UpperArm|Bip01_L_Forearm|Bip01_L_Hand|Bip01_L_Finger3|Bip01_L_Finger31|_notused_Bip01_L_Finger3Nub.is"
		;
connectAttr "|Sit_Idle_1|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_L_Clavicle|Bip01_L_UpperArm|Bip01_L_Forearm|Bip01_L_Hand.s" "|Sit_Idle_1|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_L_Clavicle|Bip01_L_UpperArm|Bip01_L_Forearm|Bip01_L_Hand|Bip01_L_Finger4.is"
		;
connectAttr "|Sit_Idle_1|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_L_Clavicle|Bip01_L_UpperArm|Bip01_L_Forearm|Bip01_L_Hand|Bip01_L_Finger4.s" "|Sit_Idle_1|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_L_Clavicle|Bip01_L_UpperArm|Bip01_L_Forearm|Bip01_L_Hand|Bip01_L_Finger4|Bip01_L_Finger41.is"
		;
connectAttr "|Sit_Idle_1|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_L_Clavicle|Bip01_L_UpperArm|Bip01_L_Forearm|Bip01_L_Hand|Bip01_L_Finger4|Bip01_L_Finger41.s" "|Sit_Idle_1|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_L_Clavicle|Bip01_L_UpperArm|Bip01_L_Forearm|Bip01_L_Hand|Bip01_L_Finger4|Bip01_L_Finger41|_notused_Bip01_L_Finger4Nub.is"
		;
connectAttr "|Sit_Idle_1|Bip01_Spine.s" "|Sit_Idle_1|Bip01_Spine|Bip01_L_Thigh.is"
		;
connectAttr "Bip01_L_Thigh_rotateX1.o" "|Sit_Idle_1|Bip01_Spine|Bip01_L_Thigh.rx"
		;
connectAttr "Bip01_L_Thigh_rotateY1.o" "|Sit_Idle_1|Bip01_Spine|Bip01_L_Thigh.ry"
		;
connectAttr "Bip01_L_Thigh_rotateZ1.o" "|Sit_Idle_1|Bip01_Spine|Bip01_L_Thigh.rz"
		;
connectAttr "|Sit_Idle_1|Bip01_Spine|Bip01_L_Thigh.s" "|Sit_Idle_1|Bip01_Spine|Bip01_L_Thigh|Bip01_L_Calf.is"
		;
connectAttr "Bip01_L_Calf_rotateX1.o" "|Sit_Idle_1|Bip01_Spine|Bip01_L_Thigh|Bip01_L_Calf.rx"
		;
connectAttr "Bip01_L_Calf_rotateY1.o" "|Sit_Idle_1|Bip01_Spine|Bip01_L_Thigh|Bip01_L_Calf.ry"
		;
connectAttr "Bip01_L_Calf_rotateZ1.o" "|Sit_Idle_1|Bip01_Spine|Bip01_L_Thigh|Bip01_L_Calf.rz"
		;
connectAttr "|Sit_Idle_1|Bip01_Spine|Bip01_L_Thigh|Bip01_L_Calf.s" "|Sit_Idle_1|Bip01_Spine|Bip01_L_Thigh|Bip01_L_Calf|Bip01_L_Foot.is"
		;
connectAttr "Bip01_L_Foot_rotateX1.o" "|Sit_Idle_1|Bip01_Spine|Bip01_L_Thigh|Bip01_L_Calf|Bip01_L_Foot.rx"
		;
connectAttr "Bip01_L_Foot_rotateY1.o" "|Sit_Idle_1|Bip01_Spine|Bip01_L_Thigh|Bip01_L_Calf|Bip01_L_Foot.ry"
		;
connectAttr "Bip01_L_Foot_rotateZ1.o" "|Sit_Idle_1|Bip01_Spine|Bip01_L_Thigh|Bip01_L_Calf|Bip01_L_Foot.rz"
		;
connectAttr "|Sit_Idle_1|Bip01_Spine|Bip01_L_Thigh|Bip01_L_Calf|Bip01_L_Foot.s" "|Sit_Idle_1|Bip01_Spine|Bip01_L_Thigh|Bip01_L_Calf|Bip01_L_Foot|Bip01_L_Toe0.is"
		;
connectAttr "|Sit_Idle_1|Bip01_Spine|Bip01_L_Thigh|Bip01_L_Calf|Bip01_L_Foot|Bip01_L_Toe0.s" "|Sit_Idle_1|Bip01_Spine|Bip01_L_Thigh|Bip01_L_Calf|Bip01_L_Foot|Bip01_L_Toe0|_notused_Bip01_L_Toe0Nub.is"
		;
connectAttr "|Sit_Idle_1|Bip01_Spine.s" "|Sit_Idle_1|Bip01_Spine|Bip01_R_Thigh.is"
		;
connectAttr "Bip01_R_Thigh_rotateX1.o" "|Sit_Idle_1|Bip01_Spine|Bip01_R_Thigh.rx"
		;
connectAttr "Bip01_R_Thigh_rotateY1.o" "|Sit_Idle_1|Bip01_Spine|Bip01_R_Thigh.ry"
		;
connectAttr "Bip01_R_Thigh_rotateZ1.o" "|Sit_Idle_1|Bip01_Spine|Bip01_R_Thigh.rz"
		;
connectAttr "|Sit_Idle_1|Bip01_Spine|Bip01_R_Thigh.s" "|Sit_Idle_1|Bip01_Spine|Bip01_R_Thigh|Bip01_R_Calf.is"
		;
connectAttr "Bip01_R_Calf_rotateX1.o" "|Sit_Idle_1|Bip01_Spine|Bip01_R_Thigh|Bip01_R_Calf.rx"
		;
connectAttr "Bip01_R_Calf_rotateY1.o" "|Sit_Idle_1|Bip01_Spine|Bip01_R_Thigh|Bip01_R_Calf.ry"
		;
connectAttr "Bip01_R_Calf_rotateZ1.o" "|Sit_Idle_1|Bip01_Spine|Bip01_R_Thigh|Bip01_R_Calf.rz"
		;
connectAttr "|Sit_Idle_1|Bip01_Spine|Bip01_R_Thigh|Bip01_R_Calf.s" "|Sit_Idle_1|Bip01_Spine|Bip01_R_Thigh|Bip01_R_Calf|Bip01_R_Foot.is"
		;
connectAttr "Bip01_R_Foot_rotateX1.o" "|Sit_Idle_1|Bip01_Spine|Bip01_R_Thigh|Bip01_R_Calf|Bip01_R_Foot.rx"
		;
connectAttr "Bip01_R_Foot_rotateY1.o" "|Sit_Idle_1|Bip01_Spine|Bip01_R_Thigh|Bip01_R_Calf|Bip01_R_Foot.ry"
		;
connectAttr "Bip01_R_Foot_rotateZ1.o" "|Sit_Idle_1|Bip01_Spine|Bip01_R_Thigh|Bip01_R_Calf|Bip01_R_Foot.rz"
		;
connectAttr "|Sit_Idle_1|Bip01_Spine|Bip01_R_Thigh|Bip01_R_Calf|Bip01_R_Foot.s" "|Sit_Idle_1|Bip01_Spine|Bip01_R_Thigh|Bip01_R_Calf|Bip01_R_Foot|Bip01_R_Toe0.is"
		;
connectAttr "|Sit_Idle_1|Bip01_Spine|Bip01_R_Thigh|Bip01_R_Calf|Bip01_R_Foot|Bip01_R_Toe0.s" "|Sit_Idle_1|Bip01_Spine|Bip01_R_Thigh|Bip01_R_Calf|Bip01_R_Foot|Bip01_R_Toe0|_notused_Bip01_R_Toe0Nub.is"
		;
// End of Animation v5 - Sit Idle 1.ma
