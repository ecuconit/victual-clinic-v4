//Maya ASCII 2014 scene
//Name: Animation v6 - Stand Idle 1.ma
//Last modified: Fri, Apr 17, 2015 12:53:09 PM
//Codeset: 1252
requires maya "2014";
requires -nodeType "mentalrayFramebuffer" -nodeType "mentalrayOutputPass" -nodeType "mentalrayRenderPass"
		 -nodeType "mentalrayUserBuffer" -nodeType "mentalraySubdivApprox" -nodeType "mentalrayCurveApprox"
		 -nodeType "mentalraySurfaceApprox" -nodeType "mentalrayDisplaceApprox" -nodeType "mentalrayOptions"
		 -nodeType "mentalrayGlobals" -nodeType "mentalrayItemsList" -nodeType "mentalrayShader"
		 -nodeType "mentalrayUserData" -nodeType "mentalrayText" -nodeType "mentalrayTessellation"
		 -nodeType "mentalrayPhenomenon" -nodeType "mentalrayLightProfile" -nodeType "mentalrayVertexColors"
		 -nodeType "mentalrayIblShape" -nodeType "mapVizShape" -nodeType "mentalrayCCMeshProxy"
		 -nodeType "cylindricalLightLocator" -nodeType "discLightLocator" -nodeType "rectangularLightLocator"
		 -nodeType "sphericalLightLocator" -nodeType "abcimport" -nodeType "mia_physicalsun"
		 -nodeType "mia_physicalsky" -nodeType "mia_material" -nodeType "mia_material_x" -nodeType "mia_roundcorners"
		 -nodeType "mia_exposure_simple" -nodeType "mia_portal_light" -nodeType "mia_light_surface"
		 -nodeType "mia_exposure_photographic" -nodeType "mia_exposure_photographic_rev" -nodeType "mia_lens_bokeh"
		 -nodeType "mia_envblur" -nodeType "mia_ciesky" -nodeType "mia_photometric_light"
		 -nodeType "mib_texture_vector" -nodeType "mib_texture_remap" -nodeType "mib_texture_rotate"
		 -nodeType "mib_bump_basis" -nodeType "mib_bump_map" -nodeType "mib_passthrough_bump_map"
		 -nodeType "mib_bump_map2" -nodeType "mib_lookup_spherical" -nodeType "mib_lookup_cube1"
		 -nodeType "mib_lookup_cube6" -nodeType "mib_lookup_background" -nodeType "mib_lookup_cylindrical"
		 -nodeType "mib_texture_lookup" -nodeType "mib_texture_lookup2" -nodeType "mib_texture_filter_lookup"
		 -nodeType "mib_texture_checkerboard" -nodeType "mib_texture_polkadot" -nodeType "mib_texture_polkasphere"
		 -nodeType "mib_texture_turbulence" -nodeType "mib_texture_wave" -nodeType "mib_reflect"
		 -nodeType "mib_refract" -nodeType "mib_transparency" -nodeType "mib_continue" -nodeType "mib_opacity"
		 -nodeType "mib_twosided" -nodeType "mib_refraction_index" -nodeType "mib_dielectric"
		 -nodeType "mib_ray_marcher" -nodeType "mib_illum_lambert" -nodeType "mib_illum_phong"
		 -nodeType "mib_illum_ward" -nodeType "mib_illum_ward_deriv" -nodeType "mib_illum_blinn"
		 -nodeType "mib_illum_cooktorr" -nodeType "mib_illum_hair" -nodeType "mib_volume"
		 -nodeType "mib_color_alpha" -nodeType "mib_color_average" -nodeType "mib_color_intensity"
		 -nodeType "mib_color_interpolate" -nodeType "mib_color_mix" -nodeType "mib_color_spread"
		 -nodeType "mib_geo_cube" -nodeType "mib_geo_torus" -nodeType "mib_geo_sphere" -nodeType "mib_geo_cone"
		 -nodeType "mib_geo_cylinder" -nodeType "mib_geo_square" -nodeType "mib_geo_instance"
		 -nodeType "mib_geo_instance_mlist" -nodeType "mib_geo_add_uv_texsurf" -nodeType "mib_photon_basic"
		 -nodeType "mib_light_infinite" -nodeType "mib_light_point" -nodeType "mib_light_spot"
		 -nodeType "mib_light_photometric" -nodeType "mib_cie_d" -nodeType "mib_blackbody"
		 -nodeType "mib_shadow_transparency" -nodeType "mib_lens_stencil" -nodeType "mib_lens_clamp"
		 -nodeType "mib_lightmap_write" -nodeType "mib_lightmap_sample" -nodeType "mib_amb_occlusion"
		 -nodeType "mib_fast_occlusion" -nodeType "mib_map_get_scalar" -nodeType "mib_map_get_integer"
		 -nodeType "mib_map_get_vector" -nodeType "mib_map_get_color" -nodeType "mib_map_get_transform"
		 -nodeType "mib_map_get_scalar_array" -nodeType "mib_map_get_integer_array" -nodeType "mib_fg_occlusion"
		 -nodeType "mib_bent_normal_env" -nodeType "mib_glossy_reflection" -nodeType "mib_glossy_refraction"
		 -nodeType "builtin_bsdf_architectural" -nodeType "builtin_bsdf_architectural_comp"
		 -nodeType "builtin_bsdf_carpaint" -nodeType "builtin_bsdf_ashikhmin" -nodeType "builtin_bsdf_lambert"
		 -nodeType "builtin_bsdf_mirror" -nodeType "builtin_bsdf_phong" -nodeType "contour_store_function"
		 -nodeType "contour_store_function_simple" -nodeType "contour_contrast_function_levels"
		 -nodeType "contour_contrast_function_simple" -nodeType "contour_shader_simple" -nodeType "contour_shader_silhouette"
		 -nodeType "contour_shader_maxcolor" -nodeType "contour_shader_curvature" -nodeType "contour_shader_factorcolor"
		 -nodeType "contour_shader_depthfade" -nodeType "contour_shader_framefade" -nodeType "contour_shader_layerthinner"
		 -nodeType "contour_shader_widthfromcolor" -nodeType "contour_shader_widthfromlightdir"
		 -nodeType "contour_shader_widthfromlight" -nodeType "contour_shader_combi" -nodeType "contour_only"
		 -nodeType "contour_composite" -nodeType "contour_ps" -nodeType "mi_metallic_paint"
		 -nodeType "mi_metallic_paint_x" -nodeType "mi_bump_flakes" -nodeType "mi_car_paint_phen"
		 -nodeType "mi_metallic_paint_output_mixer" -nodeType "mi_car_paint_phen_x" -nodeType "physical_lens_dof"
		 -nodeType "physical_light" -nodeType "dgs_material" -nodeType "dgs_material_photon"
		 -nodeType "dielectric_material" -nodeType "dielectric_material_photon" -nodeType "oversampling_lens"
		 -nodeType "path_material" -nodeType "parti_volume" -nodeType "parti_volume_photon"
		 -nodeType "transmat" -nodeType "transmat_photon" -nodeType "mip_rayswitch" -nodeType "mip_rayswitch_advanced"
		 -nodeType "mip_rayswitch_environment" -nodeType "mip_card_opacity" -nodeType "mip_motionblur"
		 -nodeType "mip_motion_vector" -nodeType "mip_matteshadow" -nodeType "mip_cameramap"
		 -nodeType "mip_mirrorball" -nodeType "mip_grayball" -nodeType "mip_gamma_gain" -nodeType "mip_render_subset"
		 -nodeType "mip_matteshadow_mtl" -nodeType "mip_binaryproxy" -nodeType "mip_rayswitch_stage"
		 -nodeType "mip_fgshooter" -nodeType "mib_ptex_lookup" -nodeType "misss_physical"
		 -nodeType "misss_physical_phen" -nodeType "misss_fast_shader" -nodeType "misss_fast_shader_x"
		 -nodeType "misss_fast_shader2" -nodeType "misss_fast_shader2_x" -nodeType "misss_skin_specular"
		 -nodeType "misss_lightmap_write" -nodeType "misss_lambert_gamma" -nodeType "misss_call_shader"
		 -nodeType "misss_set_normal" -nodeType "misss_fast_lmap_maya" -nodeType "misss_fast_simple_maya"
		 -nodeType "misss_fast_skin_maya" -nodeType "misss_fast_skin_phen" -nodeType "misss_fast_skin_phen_d"
		 -nodeType "misss_mia_skin2_phen" -nodeType "misss_mia_skin2_phen_d" -nodeType "misss_lightmap_phen"
		 -nodeType "misss_mia_skin2_surface_phen" -nodeType "surfaceSampler" -nodeType "mib_data_bool"
		 -nodeType "mib_data_int" -nodeType "mib_data_scalar" -nodeType "mib_data_vector"
		 -nodeType "mib_data_color" -nodeType "mib_data_string" -nodeType "mib_data_texture"
		 -nodeType "mib_data_shader" -nodeType "mib_data_bool_array" -nodeType "mib_data_int_array"
		 -nodeType "mib_data_scalar_array" -nodeType "mib_data_vector_array" -nodeType "mib_data_color_array"
		 -nodeType "mib_data_string_array" -nodeType "mib_data_texture_array" -nodeType "mib_data_shader_array"
		 -nodeType "mib_data_get_bool" -nodeType "mib_data_get_int" -nodeType "mib_data_get_scalar"
		 -nodeType "mib_data_get_vector" -nodeType "mib_data_get_color" -nodeType "mib_data_get_string"
		 -nodeType "mib_data_get_texture" -nodeType "mib_data_get_shader" -nodeType "mib_data_get_shader_bool"
		 -nodeType "mib_data_get_shader_int" -nodeType "mib_data_get_shader_scalar" -nodeType "mib_data_get_shader_vector"
		 -nodeType "mib_data_get_shader_color" -nodeType "user_ibl_env" -nodeType "user_ibl_rect"
		 -nodeType "mia_material_x_passes" -nodeType "mi_metallic_paint_x_passes" -nodeType "mi_car_paint_phen_x_passes"
		 -nodeType "misss_fast_shader_x_passes" -dataType "byteArray" "Mayatomr" "2014.0 - 3.11.1.4 ";
currentUnit -l centimeter -a degree -t film;
fileInfo "application" "maya";
fileInfo "product" "Maya 2014";
fileInfo "version" "2014 x64";
fileInfo "cutIdentifier" "201303010241-864206";
fileInfo "osv" "Microsoft Windows 7 Enterprise Edition, 64-bit Windows 7 Service Pack 1 (Build 7601)\n";
fileInfo "license" "education";
createNode transform -s -n "persp";
	setAttr ".v" no;
	setAttr ".t" -type "double3" 15.899223588864835 109.48614186602931 274.34710588064115 ;
	setAttr ".r" -type "double3" -3.3383527296095981 1.3999999999996657 5.5924881908836784e-017 ;
createNode camera -s -n "perspShape" -p "persp";
	setAttr -k off ".v" no;
	setAttr ".fl" 34.999999999999986;
	setAttr ".coi" 261.49125146802692;
	setAttr ".imn" -type "string" "persp";
	setAttr ".den" -type "string" "persp_depth";
	setAttr ".man" -type "string" "persp_mask";
	setAttr ".hc" -type "string" "viewSet -p %camera";
createNode transform -s -n "top";
	setAttr ".v" no;
	setAttr ".t" -type "double3" 0 100.1 0 ;
	setAttr ".r" -type "double3" -89.999999999999986 0 0 ;
createNode camera -s -n "topShape" -p "top";
	setAttr -k off ".v" no;
	setAttr ".rnd" no;
	setAttr ".coi" 100.1;
	setAttr ".ow" 30;
	setAttr ".imn" -type "string" "top";
	setAttr ".den" -type "string" "top_depth";
	setAttr ".man" -type "string" "top_mask";
	setAttr ".hc" -type "string" "viewSet -t %camera";
	setAttr ".o" yes;
createNode transform -s -n "front";
	setAttr ".v" no;
	setAttr ".t" -type "double3" 0 0 100.1 ;
createNode camera -s -n "frontShape" -p "front";
	setAttr -k off ".v" no;
	setAttr ".rnd" no;
	setAttr ".coi" 100.1;
	setAttr ".ow" 30;
	setAttr ".imn" -type "string" "front";
	setAttr ".den" -type "string" "front_depth";
	setAttr ".man" -type "string" "front_mask";
	setAttr ".hc" -type "string" "viewSet -f %camera";
	setAttr ".o" yes;
createNode transform -s -n "side";
	setAttr ".v" no;
	setAttr ".t" -type "double3" 100.1 0 0 ;
	setAttr ".r" -type "double3" 0 89.999999999999986 0 ;
createNode camera -s -n "sideShape" -p "side";
	setAttr -k off ".v" no;
	setAttr ".rnd" no;
	setAttr ".coi" 100.1;
	setAttr ".ow" 30;
	setAttr ".imn" -type "string" "side";
	setAttr ".den" -type "string" "side_depth";
	setAttr ".man" -type "string" "side_mask";
	setAttr ".hc" -type "string" "viewSet -s %camera";
	setAttr ".o" yes;
createNode transform -n "Stand_Idle_1";
createNode joint -n "Bip01_Spine" -p "Stand_Idle_1";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".jo" -type "double3" 90.000171000000023 0 89.999999999999986 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" 0.0098161168805748194 0.99935285933343521 0.034605005295103568 0
		 0.012550008731826712 -0.034727071419246469 0.99931803135512043 0 0.99987306250854557 -0.0093751294780294314 -0.012882772102781992 0
		 -6.7139604917915256e-006 99.443700000000007 -2.2495999999899592 1;
	setAttr -k on ".liw";
	setAttr ".fbxID" 5;
createNode joint -n "Bip01_Spine1" -p "Bip01_Spine";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".t" -type "double3" 13.3984 -0.0107 0 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" -0.0019100987456287133 0.013677511412441756 0.99990463405484054 0
		 -0.0042303051545588893 -0.99989762166674445 0.013669334420047589 0 0.99998922796252221 -0.0042037919489802001 0.001967763239932373 0
		 9.9999999999999995e-007 2.2603360000000001 112.84212599999999 1;
	setAttr -k on ".liw";
	setAttr ".fbxID" 5;
createNode joint -n "Bip01_Spine2" -p "Bip01_Spine1";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".t" -type "double3" 13.3983 -0.0107 0 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" 0 0 1 0 0 -1 0 0 1 0 0 0 -3.9999999999999998e-006 -9.6239790000000003 -19.074804 1;
	setAttr -k on ".liw";
	setAttr ".fbxID" 5;
createNode joint -n "Bip01_Neck" -p "Bip01_Spine2";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".t" -type "double3" 13.3985 -0.007899999999999999 0 ;
	setAttr ".r" -type "double3" 0 -6e-006 1.147996 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" 0 -0.020830999999999999 0.99978299999999998 0 0 -0.99978299999999998 -0.020830999999999999 0
		 1 0 0 0 -1.0000000000000001e-005 -9.6266999999999996 -5.6762839999999999 1;
	setAttr -k on ".liw";
	setAttr ".fbxID" 5;
createNode joint -n "Bip01_Head" -p "Bip01_Neck";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".t" -type "double3" 9.9829 0 0 ;
	setAttr ".r" -type "double3" 0 -1.3000000000000001e-005 4.622391 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" 0 -0.10133399999999999 0.99485299999999999 0 0 -0.99485199999999996 -0.10133399999999999 0
		 1 0 0 0 -4.4000000000000012e-005 -9.8346470000000004 4.3044190000000002 1;
	setAttr -k on ".liw";
	setAttr ".fbxID" 5;
createNode joint -n "_notused_Bip01_HeadNub" -p "Bip01_Head";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".t" -type "double3" 18.3537 0 0 ;
	setAttr ".ssc" no;
	setAttr ".fbxID" 5;
createNode joint -n "Bip01_R_Clavicle" -p "Bip01_Neck";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".t" -type "double3" -0.8268 -0.6612 -3.9165 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" -0.99454799999999999 -9.9454800000000002e-007 -0.104286 0
		 -9.8906500000000008e-007 1 -1.0428599999999999e-007 0 0.104286 -5.7179000000000015e-010 -0.99454799999999999 0
		 -3.9165049999999999 2.9251839999999998 138.82619800000001 1;
	setAttr -k on ".liw";
	setAttr ".fbxID" 5;
createNode joint -n "Bip01_R_UpperArm" -p "Bip01_R_Clavicle";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".t" -type "double3" 10.6745 0 0 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" -0.75332600000000005 0.10000199999999999 -0.65000000000000002 0
		 0.071782399999999996 0.99496899999999999 0.0698825 0 0.65371900000000005 0.0059857399999999998 -0.75671500000000003 0
		 -14.532831 2.9251239999999998 137.712976 1;
	setAttr -k on ".liw";
	setAttr ".fbxID" 5;
createNode joint -n "Bip01_R_Forearm" -p "Bip01_R_UpperArm";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".t" -type "double3" 25.3375 0 0 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" -0.752861 -0.095945600000000006 -0.65114899999999998 0
		 -0.076500899999999997 0.99536800000000003 -0.058215000000000003 0 0.65371900000000005 0.0059856600000000003 -0.756714 0
		 -33.620182 5.4589869999999996 121.243574 1;
	setAttr -k on ".liw";
	setAttr ".fbxID" 5;
createNode joint -n "Bip01_R_Hand" -p "Bip01_R_Forearm";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".t" -type "double3" 24.9881 0 0 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" -0.67922700000000003 -0.050331500000000001 -0.73219999999999996 0
		 0.73278100000000002 0.0092545500000000003 -0.68040299999999998 0 0.0410219 -0.99868900000000005 0.030596000000000002 0
		 -52.432737000000003 3.0614050000000002 104.972679 1;
	setAttr -k on ".liw";
	setAttr ".fbxID" 5;
createNode joint -n "Bip01_R_Finger0" -p "Bip01_R_Hand";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".t" -type "double3" 2.3036000000000003 1.3941 2.9222 ;
	setAttr ".r" -type "double3" -81.93018 -23.314181 18.295908 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" -0.36474400000000001 -0.436471 -0.82246900000000001 0
		 -0.072261699999999998 0.89392799999999994 -0.44234699999999999 0 0.92829899999999999 -0.10191 -0.357595 0
		 -52.855969000000002 0.0400224 102.426821 1;
	setAttr -k on ".liw";
	setAttr ".fbxID" 5;
createNode joint -n "Bip01_R_Finger01" -p "Bip01_R_Finger0";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".t" -type "double3" 4.0806 0 0 ;
	setAttr ".r" -type "double3" 1e-006 3e-006 24.064549 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" -0.36250900000000003 -0.034022900000000002 -0.93135900000000005 0
		 0.082749500000000004 0.99421099999999996 -0.068527199999999996 0 0.92830000000000001 -0.101911 -0.357595 0
		 -54.344414999999998 -1.7410159999999999 99.070615000000004 1;
	setAttr -k on ".liw";
	setAttr ".fbxID" 5;
createNode joint -n "_notused_Bip01_R_Finger0Nub" -p "Bip01_R_Finger01";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".t" -type "double3" 3.3413 0 0 ;
	setAttr ".r" -type "double3" 1e-006 0 -180 ;
	setAttr ".ssc" no;
	setAttr ".fbxID" 5;
createNode joint -n "Bip01_R_Finger2" -p "Bip01_R_Hand";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".t" -type "double3" 7.3709 -0.4816 0.6341 ;
	setAttr ".r" -type "double3" 0.043615 -0.013407 17.086614 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" -0.43393399999999999 -0.045623999999999998 -0.89978899999999995 0
		 0.90003599999999995 0.022875199999999998 -0.43521399999999999 0 0.040439000000000003 -0.99869600000000003 0.031137000000000001 0
		 -57.766043000000003 2.0527600000000001 99.922762000000006 1;
	setAttr -k on ".liw";
	setAttr ".fbxID" 5;
createNode joint -n "Bip01_R_Finger21" -p "Bip01_R_Finger2";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".t" -type "double3" 3.8208 0 0 ;
	setAttr ".r" -type "double3" 0 1e-006 29.94639 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" 0.0732873 -0.028113900000000001 -0.99691399999999997 0
		 0.99648999999999999 0.042598999999999998 0.072054900000000005 0 0.0404418 -0.99869600000000003 0.0311372 0
		 -59.424045999999997 1.8784400000000001 96.484806000000006 1;
	setAttr -k on ".liw";
	setAttr ".fbxID" 5;
createNode joint -n "_notused_Bip01_R_Finger2Nub" -p "Bip01_R_Finger21";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".t" -type "double3" 4.7174000000000005 0 0 ;
	setAttr ".r" -type "double3" 0 0 180 ;
	setAttr ".ssc" no;
	setAttr ".fbxID" 5;
createNode joint -n "Bip01_R_Finger1" -p "Bip01_R_Hand";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".t" -type "double3" 7.3263 -0.211 2.2784 ;
	setAttr ".r" -type "double3" -0.733104 -5.314515 22.646733 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" -0.33941900000000003 -0.13520499999999999 -0.93086800000000003 0
		 0.93680799999999997 0.040593299999999999 -0.34748099999999998 0 0.084767999999999996 -0.98998600000000003 0.112883 0
		 -57.470132999999997 0.41539700000000002 99.821579 1;
	setAttr -k on ".liw";
	setAttr ".fbxID" 5;
createNode joint -n "Bip01_R_Finger11" -p "Bip01_R_Finger1";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".t" -type "double3" 3.5597999999999996 0 0 ;
	setAttr ".r" -type "double3" -2e-006 4e-006 22.627678 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" 0.047135799999999999 -0.10918 -0.99290400000000001 0
		 0.995286 0.089482699999999998 0.037409400000000002 0 0.084763400000000003 -0.98998600000000003 0.112883 0
		 -58.678344000000003 -0.065903900000000001 96.507863999999998 1;
	setAttr -k on ".liw";
	setAttr ".fbxID" 5;
createNode joint -n "_notused_Bip01_R_Finger1Nub" -p "Bip01_R_Finger11";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".t" -type "double3" 4.1404000000000005 0 0 ;
	setAttr ".r" -type "double3" 0 0 180 ;
	setAttr ".ssc" no;
	setAttr ".fbxID" 5;
createNode joint -n "Bip01_R_Finger3" -p "Bip01_R_Hand";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".t" -type "double3" 7.2345 -0.5997 -0.9332 ;
	setAttr ".r" -type "double3" 0.043364 3.826455 18.50966 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" -0.41327900000000001 0.0219578 -0.91034000000000004 0
		 0.91051400000000005 0.0239971 -0.41277999999999998 0 0.0127817 -0.999471 -0.0299104 0
		 -57.824306 3.6237469999999998 100.05504999999999 1;
	setAttr -k on ".liw";
	setAttr ".fbxID" 5;
createNode joint -n "Bip01_R_Finger31" -p "Bip01_R_Finger3";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".t" -type "double3" 3.7241 0 0 ;
	setAttr ".r" -type "double3" 3e-006 3e-006 27.984287 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" 0.0622821 0.0306512 -0.99758800000000003 0 0.99797599999999997 0.0108983 0.062641199999999994 0
		 0.012792100000000001 -0.999471 -0.0299104 0 -59.363360999999998 3.705527 96.664748000000003 1;
	setAttr -k on ".liw";
	setAttr ".fbxID" 5;
createNode joint -n "_notused_Bip01_R_Finger3Nub" -p "Bip01_R_Finger31";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".t" -type "double3" 4.2397 0 0 ;
	setAttr ".r" -type "double3" 3e-006 -5e-006 180 ;
	setAttr ".ssc" no;
	setAttr ".fbxID" 5;
createNode joint -n "Bip01_R_Finger4" -p "Bip01_R_Hand";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".t" -type "double3" 7.2292 -0.4187 -2.3639 ;
	setAttr ".r" -type "double3" -0.17713400000000004 5.561956 16.615208 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" -0.44323200000000001 0.051426199999999998 -0.89493100000000003 0
		 0.89640699999999995 0.026346700000000001 -0.44244899999999998 0 0.00082491599999999997 -0.99832900000000002 -0.057776500000000001 0
		 -57.746822000000002 5.0544450000000003 99.892002000000005 1;
	setAttr -k on ".liw";
	setAttr ".fbxID" 5;
createNode joint -n "Bip01_R_Finger41" -p "Bip01_R_Finger4";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".t" -type "double3" 3.4791000000000003 0 0 ;
	setAttr ".r" -type "double3" -2e-006 -3e-006 29.444368 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" 0.054672699999999998 0.057735700000000001 -0.996834 0
		 0.99850399999999995 -0.0023359700000000001 0.054628999999999997 0 0.00082546799999999999 -0.99832900000000002 -0.057777000000000002 0
		 -59.288860999999997 5.2333790000000002 96.778379000000001 1;
	setAttr -k on ".liw";
	setAttr ".fbxID" 5;
createNode joint -n "_notused_Bip01_R_Finger4Nub" -p "Bip01_R_Finger41";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".t" -type "double3" 3.3974 0 0 ;
	setAttr ".r" -type "double3" 0 0 -180 ;
	setAttr ".ssc" no;
	setAttr ".fbxID" 5;
createNode joint -n "Bip01_L_Clavicle" -p "Bip01_Neck";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".t" -type "double3" -0.8268 -0.6612 3.9165 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" 0.99454799999999999 9.9454800000000002e-007 -0.104286 0
		 -9.8906500000000008e-007 1 1.0428599999999999e-007 0 0.104286 -5.7179000000000015e-010 0.99454799999999999 0
		 3.9165070000000002 2.9252039999999999 138.82619700000001 1;
	setAttr -k on ".liw";
	setAttr ".fbxID" 5;
createNode joint -n "Bip01_L_UpperArm" -p "Bip01_L_Clavicle";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".t" -type "double3" 10.6745 0 0 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" 0.75332500000000002 0.100004 -0.65000000000000002 0
		 -0.071785600000000005 0.99496899999999999 0.069882299999999994 0 0.65371900000000005 -0.0059834299999999997 0.756714 0
		 14.532825000000001 2.925173 137.712974 1;
	setAttr -k on ".liw";
	setAttr ".fbxID" 5;
createNode joint -n "Bip01_L_Forearm" -p "Bip01_L_UpperArm";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".t" -type "double3" 25.3374 0 0 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" 0.752861 -0.095943299999999995 -0.65114899999999998 0
		 0.076497700000000002 0.99536899999999995 -0.058215299999999998 0 0.65371900000000005 -0.0059834199999999997 0.756714 0
		 33.620189000000003 5.4590990000000001 121.243576 1;
	setAttr -k on ".liw";
	setAttr ".fbxID" 5;
createNode joint -n "Bip01_L_Hand" -p "Bip01_L_Forearm";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".t" -type "double3" 24.9881 0 0 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" 0.67922800000000005 -0.050329499999999999 -0.73219999999999996 0
		 -0.73278100000000002 0.0092525000000000003 -0.68040299999999998 0 0.041019 0.99868999999999997 -0.030595899999999999 0
		 52.432744999999997 3.0615610000000002 104.972662 1;
	setAttr -k on ".liw";
	setAttr ".fbxID" 5;
createNode joint -n "Bip01_L_Finger0" -p "Bip01_L_Hand";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".t" -type "double3" 2.3036000000000003 1.3941 -2.9222 ;
	setAttr ".r" -type "double3" 81.93018 23.314181 18.295909 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" 0.36474600000000001 -0.43647000000000002 -0.82246900000000001 0
		 0.072258799999999998 0.89392799999999994 -0.44234699999999999 0 0.92830000000000001 0.101913 0.357595 0
		 52.856034999999999 0.0402022 102.42681 1;
	setAttr -k on ".liw";
	setAttr ".fbxID" 5;
createNode joint -n "Bip01_L_Finger01" -p "Bip01_L_Finger0";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".t" -type "double3" 4.0806 0 0 ;
	setAttr ".r" -type "double3" 1e-006 -1e-006 24.064552 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" 0.36250900000000003 -0.034021900000000001 -0.93135900000000005 0
		 -0.082751900000000003 0.99421199999999998 -0.068527000000000005 0 0.92829899999999999 0.101913 0.357595 0
		 54.344436000000002 -1.7408459999999999 99.070606999999995 1;
	setAttr -k on ".liw";
	setAttr ".fbxID" 5;
createNode joint -n "_notused_Bip01_L_Finger0Nub" -p "Bip01_L_Finger01";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".t" -type "double3" 3.3413 0 0 ;
	setAttr ".ssc" no;
	setAttr ".fbxID" 5;
createNode joint -n "Bip01_L_Finger2" -p "Bip01_L_Hand";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".t" -type "double3" 7.3709 -0.4816 -0.6341 ;
	setAttr ".r" -type "double3" -0.04361 0.01341 17.086619 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" 0.43393500000000002 -0.045622700000000002 -0.89978899999999995 0
		 -0.90003599999999995 0.022871599999999999 -0.43521399999999999 0 0.040435199999999998 0.99869699999999995 -0.031137399999999999 0
		 57.766060000000003 2.0529269999999999 99.922754999999995 1;
	setAttr -k on ".liw";
	setAttr ".fbxID" 5;
createNode joint -n "Bip01_L_Finger21" -p "Bip01_L_Finger2";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".t" -type "double3" 3.8208 0 0 ;
	setAttr ".r" -type "double3" -4e-006 -4e-006 29.94639 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" -0.0732873 -0.0281141 -0.99691399999999997 0 -0.99649100000000002 0.042592999999999999 0.072054999999999994 0
		 0.040435899999999997 0.99869699999999995 -0.031137000000000001 0 59.424058000000002 1.878614 96.484800000000007 1;
	setAttr -k on ".liw";
	setAttr ".fbxID" 5;
createNode joint -n "_notused_Bip01_L_Finger2Nub" -p "Bip01_L_Finger21";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".t" -type "double3" 4.7174000000000005 0 0 ;
	setAttr ".ssc" no;
	setAttr ".fbxID" 5;
createNode joint -n "Bip01_L_Finger1" -p "Bip01_L_Hand";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".t" -type "double3" 7.3263 -0.211 -2.2784 ;
	setAttr ".r" -type "double3" 0.733108 5.314513 22.646743 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" 0.33942 -0.13520399999999999 -0.93086800000000003 0
		 -0.93680799999999997 0.040590800000000003 -0.34748099999999998 0 0.084765400000000005 0.98998600000000003 -0.112883 0
		 57.470143999999998 0.415574 99.821611000000004 1;
	setAttr -k on ".liw";
	setAttr ".fbxID" 5;
createNode joint -n "Bip01_L_Finger11" -p "Bip01_L_Finger1";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".t" -type "double3" 3.5597999999999996 0 0 ;
	setAttr ".r" -type "double3" -2e-006 3e-006 22.627677 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" -0.047135499999999997 -0.10918 -0.99290400000000001 0
		 -0.99528499999999998 0.089479600000000006 0.037409299999999999 0 0.0847604 0.98998699999999995 -0.112883 0
		 58.678319000000002 -0.065726099999999996 96.507845000000003 1;
	setAttr -k on ".liw";
	setAttr ".fbxID" 5;
createNode joint -n "_notused_Bip01_L_Finger1Nub" -p "Bip01_L_Finger11";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".t" -type "double3" 4.1404000000000005 0 0 ;
	setAttr ".ssc" no;
	setAttr ".fbxID" 5;
createNode joint -n "Bip01_L_Finger3" -p "Bip01_L_Hand";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".t" -type "double3" 7.2345 -0.5997 0.9332 ;
	setAttr ".r" -type "double3" -0.043362 -3.826458 18.509667 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" 0.41327900000000001 0.021958700000000001 -0.91034000000000004 0
		 -0.91051400000000005 0.0239966 -0.41277999999999998 0 0.012781000000000001 0.999471 0.029911 0
		 57.824314000000001 3.6238670000000002 100.055049 1;
	setAttr -k on ".liw";
	setAttr ".fbxID" 5;
createNode joint -n "Bip01_L_Finger31" -p "Bip01_L_Finger3";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".t" -type "double3" 3.7241 0 0 ;
	setAttr ".r" -type "double3" 2e-006 0 27.984288 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" -0.062282200000000003 0.030651100000000001 -0.99758800000000003 0
		 -0.99797599999999997 0.0108933 0.062641199999999994 0 0.012787099999999999 0.999471 0.029910599999999999 0
		 59.363363 3.7056490000000002 96.664751999999993 1;
	setAttr -k on ".liw";
	setAttr ".fbxID" 5;
createNode joint -n "_notused_Bip01_L_Finger3Nub" -p "Bip01_L_Finger31";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".t" -type "double3" 4.2397 0 0 ;
	setAttr ".ssc" no;
	setAttr ".fbxID" 5;
createNode joint -n "Bip01_L_Finger4" -p "Bip01_L_Hand";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".t" -type "double3" 7.2292 -0.4187 2.3639 ;
	setAttr ".r" -type "double3" 0.177132 -5.561954 16.615218 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" 0.44323200000000001 0.051427599999999997 -0.89493100000000003 0
		 -0.89640600000000004 0.026343800000000001 -0.44244899999999998 0 0.00082181400000000003 0.99832900000000002 0.057776399999999999 0
		 57.746775 5.054627 99.891990000000007 1;
	setAttr -k on ".liw";
	setAttr ".fbxID" 5;
createNode joint -n "Bip01_L_Finger41" -p "Bip01_L_Finger4";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".t" -type "double3" 3.4791000000000003 0 0 ;
	setAttr ".r" -type "double3" 2e-006 -3e-006 29.444363999999997 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" -0.054672800000000001 0.057735500000000002 -0.996834 0
		 -0.99850399999999995 -0.00234096 0.054628799999999998 0 0.00082047800000000003 0.99832900000000002 0.057777200000000001 0
		 59.288862000000002 5.2335589999999996 96.778389000000004 1;
	setAttr -k on ".liw";
	setAttr ".fbxID" 5;
createNode joint -n "_notused_Bip01_L_Finger4Nub" -p "Bip01_L_Finger41";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".t" -type "double3" 3.3974 0 0 ;
	setAttr ".ssc" no;
	setAttr ".fbxID" 5;
createNode joint -n "Bip01_L_Thigh" -p "Bip01_Spine";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".t" -type "double3" -11.0456 2.2584 8.5201999999999991 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" 0.059694200000000003 0.0067868399999999997 -0.998193 0
		 0.0013515700000000001 -0.99997599999999998 -0.00671814 0 -0.99821599999999999 -0.00094809699999999996 -0.059701999999999998 0
		 8.5202019999999994 4.3064900000000002e-005 88.396292000000003 1;
	setAttr -k on ".liw";
	setAttr ".fbxID" 5;
createNode joint -n "Bip01_L_Calf" -p "Bip01_L_Thigh";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".t" -type "double3" 36.1527 0 0 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" 0.059157700000000001 0.119882 -0.99102400000000002 0
		 0.0080950799999999993 -0.99278699999999998 -0.119612 0 -0.99821599999999999 -0.00094644599999999998 -0.059701400000000002 0
		 10.678299000000001 0.245397 52.308965999999998 1;
	setAttr -k on ".liw";
	setAttr ".fbxID" 5;
createNode joint -n "Bip01_L_Foot" -p "Bip01_L_Calf";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".t" -type "double3" 43.2543 0 0 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" 0 0 -1 0 0.123471 -0.99234800000000001 0 0 -0.99234800000000001 -0.123471 0 0
		 13.237131 5.4308170000000002 9.4429110000000005 1;
	setAttr -k on ".liw";
	setAttr ".fbxID" 5;
createNode joint -n "Bip01_L_Toe0" -p "Bip01_L_Foot";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".t" -type "double3" 9.4714999999999989 11.2377 0 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" 0.123471 -0.99234800000000001 0 0 0 0 1 0 -0.99234800000000001 -0.123471 0 0
		 14.624665999999999 -5.7209120000000002 -0.028559000000000001 1;
	setAttr -k on ".liw";
	setAttr ".fbxID" 5;
createNode joint -n "_notused_Bip01_L_Toe0Nub" -p "Bip01_L_Toe0";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".t" -type "double3" 7.2685 0 0 ;
	setAttr ".r" -type "double3" 0 0 180 ;
	setAttr ".ssc" no;
	setAttr ".fbxID" 5;
createNode joint -n "Bip01_R_Thigh" -p "Bip01_Spine";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".t" -type "double3" -11.0456 2.2585 -8.5201999999999991 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" -0.059694200000000003 0.0067826700000000002 -0.998193 0
		 -0.00134545 -0.99997599999999998 -0.0067143300000000001 0 -0.99821599999999999 0.00094221699999999997 0.059701900000000002 0
		 -8.5201879999999992 1.40303e-005 88.396298000000002 1;
	setAttr -k on ".liw";
	setAttr ".fbxID" 5;
createNode joint -n "Bip01_R_Calf" -p "Bip01_R_Thigh";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".t" -type "double3" 36.1527 0 0 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" -0.059158000000000002 0.11988699999999999 -0.99102400000000002 0
		 -0.0080972100000000005 -0.99278699999999998 -0.119617 0 -0.99821599999999999 0.00094822899999999998 0.059701999999999998 0
		 -10.678288 0.24518599999999999 52.308996999999998 1;
	setAttr -k on ".liw";
	setAttr ".fbxID" 5;
createNode joint -n "Bip01_R_Foot" -p "Bip01_R_Calf";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".t" -type "double3" 43.2543 0 0 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" 0 0 -1 0 -0.123469 -0.99234800000000001 0 0 -0.99234800000000001 0.123469 0 0
		 -13.237124 5.4308209999999999 9.442933 1;
	setAttr -k on ".liw";
	setAttr ".fbxID" 5;
createNode joint -n "Bip01_R_Toe0" -p "Bip01_R_Foot";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".t" -type "double3" 9.4714999999999989 11.2377 0 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" -0.12346799999999999 -0.99234800000000001 0 0 0 0 1 0
		 -0.99234800000000001 0.12346799999999999 0 0 -14.624625999999999 -5.7209019999999997 -0.028538999999999998 1;
	setAttr -k on ".liw";
	setAttr ".fbxID" 5;
createNode joint -n "_notused_Bip01_R_Toe0Nub" -p "Bip01_R_Toe0";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".t" -type "double3" 7.2685 0 0 ;
	setAttr ".ssc" no;
	setAttr ".fbxID" 5;
createNode lightLinker -s -n "lightLinker1";
	setAttr -s 126 ".lnk";
	setAttr -s 126 ".slnk";
createNode displayLayerManager -n "layerManager";
createNode displayLayer -n "defaultLayer";
createNode renderLayerManager -n "renderLayerManager";
createNode renderLayer -n "defaultRenderLayer";
	setAttr ".g" yes;
createNode phong -n "business05_f_highpolyPhong";
	setAttr ".sc" -type "float3" 0 0 0 ;
createNode shadingEngine -n "business05_f_highpolySG";
	setAttr ".ihi" 0;
	setAttr ".ro" yes;
createNode materialInfo -n "materialInfo1";
createNode shadingEngine -n "business05_f_highpolySG1";
	setAttr ".ihi" 0;
	setAttr ".ro" yes;
createNode materialInfo -n "materialInfo2";
createNode file -n "Map1";
	setAttr ".ftn" -type "string" "C:\\Users\\Kuan\\Desktop\\iPi Record 20150227\\business05_f\\business05_f_35.tga";
createNode place2dTexture -n "place2dTexture1";
createNode phong -n "business05_f_highpoly_hairPhong";
	setAttr ".c" -type "float3" 0 0 0 ;
	setAttr ".sc" -type "float3" 0 0 0 ;
createNode shadingEngine -n "business05_f_highpoly_hairSG";
	setAttr ".ihi" 0;
	setAttr ".ro" yes;
createNode materialInfo -n "materialInfo3";
createNode shadingEngine -n "business05_f_highpoly_hairSG1";
	setAttr ".ihi" 0;
	setAttr ".ro" yes;
createNode materialInfo -n "materialInfo4";
createNode phong -n "MoCap_Result_Animations_v1_business05_f_highpolyPhong";
	setAttr ".sc" -type "float3" 0 0 0 ;
createNode shadingEngine -n "MoCap_Result_Animations_v1_business05_f_highpolySG";
	setAttr ".ihi" 0;
	setAttr ".ro" yes;
createNode materialInfo -n "MoCap_Result_Animations_v1_materialInfo1";
createNode shadingEngine -n "MoCap_Result_Animations_v1_business05_f_highpolySG1";
	setAttr ".ihi" 0;
	setAttr ".ro" yes;
createNode materialInfo -n "MoCap_Result_Animations_v1_materialInfo2";
createNode file -n "MoCap_Result_Animations_v1_Map1";
	setAttr ".ftn" -type "string" "C:\\Users\\Kuan\\Desktop\\iPi Record 20150227\\business05_f\\business05_f_35.tga";
createNode place2dTexture -n "MoCap_Result_Animations_v1_place2dTexture1";
createNode phong -n "MoCap_Result_Animations_v1_business05_f_highpoly_hairPhong";
	setAttr ".c" -type "float3" 0 0 0 ;
	setAttr ".sc" -type "float3" 0 0 0 ;
createNode shadingEngine -n "MoCap_Result_Animations_v1_business05_f_highpoly_hairSG";
	setAttr ".ihi" 0;
	setAttr ".ro" yes;
createNode materialInfo -n "MoCap_Result_Animations_v1_materialInfo3";
createNode shadingEngine -n "MoCap_Result_Animations_v1_business05_f_highpoly_hairSG1";
	setAttr ".ihi" 0;
	setAttr ".ro" yes;
createNode materialInfo -n "MoCap_Result_Animations_v1_materialInfo4";
createNode phong -n "idle_v1:business05_f_highpolyPhong";
	setAttr ".sc" -type "float3" 0 0 0 ;
createNode shadingEngine -n "idle_v1:business05_f_highpolySG";
	setAttr ".ihi" 0;
	setAttr ".ro" yes;
createNode materialInfo -n "idle_v1:materialInfo1";
createNode shadingEngine -n "idle_v1:business05_f_highpolySG1";
	setAttr ".ihi" 0;
	setAttr ".ro" yes;
createNode materialInfo -n "idle_v1:materialInfo2";
createNode file -n "idle_v1:Map1";
	setAttr ".ftn" -type "string" "C:\\Users\\Kuan\\Desktop\\iPi Record 20150227\\business05_f\\business05_f_35.tga";
createNode place2dTexture -n "idle_v1:place2dTexture1";
createNode phong -n "idle_v1:business05_f_highpoly_hairPhong";
	setAttr ".c" -type "float3" 0 0 0 ;
	setAttr ".sc" -type "float3" 0 0 0 ;
createNode shadingEngine -n "idle_v1:business05_f_highpoly_hairSG";
	setAttr ".ihi" 0;
	setAttr ".ro" yes;
createNode materialInfo -n "idle_v1:materialInfo3";
createNode shadingEngine -n "idle_v1:business05_f_highpoly_hairSG1";
	setAttr ".ihi" 0;
	setAttr ".ro" yes;
createNode materialInfo -n "idle_v1:materialInfo4";
createNode script -n "idle_v1:uiConfigurationScriptNode";
	setAttr ".b" -type "string" (
		"// Maya Mel UI Configuration File.\n//\n//  This script is machine generated.  Edit at your own risk.\n//\n//\n\nglobal string $gMainPane;\nif (`paneLayout -exists $gMainPane`) {\n\n\tglobal int $gUseScenePanelConfig;\n\tint    $useSceneConfig = $gUseScenePanelConfig;\n\tint    $menusOkayInPanels = `optionVar -q allowMenusInPanels`;\tint    $nVisPanes = `paneLayout -q -nvp $gMainPane`;\n\tint    $nPanes = 0;\n\tstring $editorName;\n\tstring $panelName;\n\tstring $itemFilterName;\n\tstring $panelConfig;\n\n\t//\n\t//  get current state of the UI\n\t//\n\tsceneUIReplacement -update $gMainPane;\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"modelPanel\" (localizedPanelLabel(\"Top View\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `modelPanel -unParent -l (localizedPanelLabel(\"Top View\")) -mbv $menusOkayInPanels `;\n\t\t\t$editorName = $panelName;\n            modelEditor -e \n                -camera \"top\" \n                -useInteractiveMode 0\n                -displayLights \"default\" \n                -displayAppearance \"wireframe\" \n"
		+ "                -activeOnly 0\n                -ignorePanZoom 0\n                -wireframeOnShaded 0\n                -headsUpDisplay 1\n                -selectionHiliteDisplay 1\n                -useDefaultMaterial 0\n                -bufferMode \"double\" \n                -twoSidedLighting 1\n                -backfaceCulling 0\n                -xray 0\n                -jointXray 0\n                -activeComponentsXray 0\n                -displayTextures 0\n                -smoothWireframe 0\n                -lineWidth 1\n                -textureAnisotropic 0\n                -textureHilight 1\n                -textureSampling 2\n                -textureDisplay \"modulate\" \n                -textureMaxSize 16384\n                -fogging 0\n                -fogSource \"fragment\" \n                -fogMode \"linear\" \n                -fogStart 0\n                -fogEnd 100\n                -fogDensity 0.1\n                -fogColor 0.5 0.5 0.5 1 \n                -maxConstantTransparency 1\n                -rendererName \"base_OpenGL_Renderer\" \n"
		+ "                -objectFilterShowInHUD 1\n                -isFiltered 0\n                -colorResolution 4 4 \n                -bumpResolution 4 4 \n                -textureCompression 0\n                -transparencyAlgorithm \"frontAndBackCull\" \n                -transpInShadows 0\n                -cullingOverride \"none\" \n                -lowQualityLighting 0\n                -maximumNumHardwareLights 0\n                -occlusionCulling 0\n                -shadingModel 0\n                -useBaseRenderer 0\n                -useReducedRenderer 0\n                -smallObjectCulling 0\n                -smallObjectThreshold -1 \n                -interactiveDisableShadows 0\n                -interactiveBackFaceCull 0\n                -sortTransparent 1\n                -nurbsCurves 1\n                -nurbsSurfaces 1\n                -polymeshes 1\n                -subdivSurfaces 1\n                -planes 1\n                -lights 1\n                -cameras 1\n                -controlVertices 1\n                -hulls 1\n                -grid 1\n"
		+ "                -imagePlane 1\n                -joints 1\n                -ikHandles 1\n                -deformers 1\n                -dynamics 1\n                -fluids 1\n                -hairSystems 1\n                -follicles 1\n                -nCloths 1\n                -nParticles 1\n                -nRigids 1\n                -dynamicConstraints 1\n                -locators 1\n                -manipulators 1\n                -pluginShapes 1\n                -dimensions 1\n                -handles 1\n                -pivots 1\n                -textures 1\n                -strokes 1\n                -motionTrails 1\n                -clipGhosts 1\n                -greasePencils 1\n                -shadows 0\n                $editorName;\n            modelEditor -e -viewSelected 0 $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tmodelPanel -edit -l (localizedPanelLabel(\"Top View\")) -mbv $menusOkayInPanels  $panelName;\n\t\t$editorName = $panelName;\n        modelEditor -e \n            -camera \"top\" \n            -useInteractiveMode 0\n"
		+ "            -displayLights \"default\" \n            -displayAppearance \"wireframe\" \n            -activeOnly 0\n            -ignorePanZoom 0\n            -wireframeOnShaded 0\n            -headsUpDisplay 1\n            -selectionHiliteDisplay 1\n            -useDefaultMaterial 0\n            -bufferMode \"double\" \n            -twoSidedLighting 1\n            -backfaceCulling 0\n            -xray 0\n            -jointXray 0\n            -activeComponentsXray 0\n            -displayTextures 0\n            -smoothWireframe 0\n            -lineWidth 1\n            -textureAnisotropic 0\n            -textureHilight 1\n            -textureSampling 2\n            -textureDisplay \"modulate\" \n            -textureMaxSize 16384\n            -fogging 0\n            -fogSource \"fragment\" \n            -fogMode \"linear\" \n            -fogStart 0\n            -fogEnd 100\n            -fogDensity 0.1\n            -fogColor 0.5 0.5 0.5 1 \n            -maxConstantTransparency 1\n            -rendererName \"base_OpenGL_Renderer\" \n            -objectFilterShowInHUD 1\n"
		+ "            -isFiltered 0\n            -colorResolution 4 4 \n            -bumpResolution 4 4 \n            -textureCompression 0\n            -transparencyAlgorithm \"frontAndBackCull\" \n            -transpInShadows 0\n            -cullingOverride \"none\" \n            -lowQualityLighting 0\n            -maximumNumHardwareLights 0\n            -occlusionCulling 0\n            -shadingModel 0\n            -useBaseRenderer 0\n            -useReducedRenderer 0\n            -smallObjectCulling 0\n            -smallObjectThreshold -1 \n            -interactiveDisableShadows 0\n            -interactiveBackFaceCull 0\n            -sortTransparent 1\n            -nurbsCurves 1\n            -nurbsSurfaces 1\n            -polymeshes 1\n            -subdivSurfaces 1\n            -planes 1\n            -lights 1\n            -cameras 1\n            -controlVertices 1\n            -hulls 1\n            -grid 1\n            -imagePlane 1\n            -joints 1\n            -ikHandles 1\n            -deformers 1\n            -dynamics 1\n            -fluids 1\n"
		+ "            -hairSystems 1\n            -follicles 1\n            -nCloths 1\n            -nParticles 1\n            -nRigids 1\n            -dynamicConstraints 1\n            -locators 1\n            -manipulators 1\n            -pluginShapes 1\n            -dimensions 1\n            -handles 1\n            -pivots 1\n            -textures 1\n            -strokes 1\n            -motionTrails 1\n            -clipGhosts 1\n            -greasePencils 1\n            -shadows 0\n            $editorName;\n        modelEditor -e -viewSelected 0 $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"modelPanel\" (localizedPanelLabel(\"Side View\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `modelPanel -unParent -l (localizedPanelLabel(\"Side View\")) -mbv $menusOkayInPanels `;\n\t\t\t$editorName = $panelName;\n            modelEditor -e \n                -camera \"side\" \n                -useInteractiveMode 0\n                -displayLights \"default\" \n"
		+ "                -displayAppearance \"wireframe\" \n                -activeOnly 0\n                -ignorePanZoom 0\n                -wireframeOnShaded 0\n                -headsUpDisplay 1\n                -selectionHiliteDisplay 1\n                -useDefaultMaterial 0\n                -bufferMode \"double\" \n                -twoSidedLighting 1\n                -backfaceCulling 0\n                -xray 0\n                -jointXray 0\n                -activeComponentsXray 0\n                -displayTextures 0\n                -smoothWireframe 0\n                -lineWidth 1\n                -textureAnisotropic 0\n                -textureHilight 1\n                -textureSampling 2\n                -textureDisplay \"modulate\" \n                -textureMaxSize 16384\n                -fogging 0\n                -fogSource \"fragment\" \n                -fogMode \"linear\" \n                -fogStart 0\n                -fogEnd 100\n                -fogDensity 0.1\n                -fogColor 0.5 0.5 0.5 1 \n                -maxConstantTransparency 1\n"
		+ "                -rendererName \"base_OpenGL_Renderer\" \n                -objectFilterShowInHUD 1\n                -isFiltered 0\n                -colorResolution 4 4 \n                -bumpResolution 4 4 \n                -textureCompression 0\n                -transparencyAlgorithm \"frontAndBackCull\" \n                -transpInShadows 0\n                -cullingOverride \"none\" \n                -lowQualityLighting 0\n                -maximumNumHardwareLights 0\n                -occlusionCulling 0\n                -shadingModel 0\n                -useBaseRenderer 0\n                -useReducedRenderer 0\n                -smallObjectCulling 0\n                -smallObjectThreshold -1 \n                -interactiveDisableShadows 0\n                -interactiveBackFaceCull 0\n                -sortTransparent 1\n                -nurbsCurves 1\n                -nurbsSurfaces 1\n                -polymeshes 1\n                -subdivSurfaces 1\n                -planes 1\n                -lights 1\n                -cameras 1\n                -controlVertices 1\n"
		+ "                -hulls 1\n                -grid 1\n                -imagePlane 1\n                -joints 1\n                -ikHandles 1\n                -deformers 1\n                -dynamics 1\n                -fluids 1\n                -hairSystems 1\n                -follicles 1\n                -nCloths 1\n                -nParticles 1\n                -nRigids 1\n                -dynamicConstraints 1\n                -locators 1\n                -manipulators 1\n                -pluginShapes 1\n                -dimensions 1\n                -handles 1\n                -pivots 1\n                -textures 1\n                -strokes 1\n                -motionTrails 1\n                -clipGhosts 1\n                -greasePencils 1\n                -shadows 0\n                $editorName;\n            modelEditor -e -viewSelected 0 $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tmodelPanel -edit -l (localizedPanelLabel(\"Side View\")) -mbv $menusOkayInPanels  $panelName;\n\t\t$editorName = $panelName;\n        modelEditor -e \n"
		+ "            -camera \"side\" \n            -useInteractiveMode 0\n            -displayLights \"default\" \n            -displayAppearance \"wireframe\" \n            -activeOnly 0\n            -ignorePanZoom 0\n            -wireframeOnShaded 0\n            -headsUpDisplay 1\n            -selectionHiliteDisplay 1\n            -useDefaultMaterial 0\n            -bufferMode \"double\" \n            -twoSidedLighting 1\n            -backfaceCulling 0\n            -xray 0\n            -jointXray 0\n            -activeComponentsXray 0\n            -displayTextures 0\n            -smoothWireframe 0\n            -lineWidth 1\n            -textureAnisotropic 0\n            -textureHilight 1\n            -textureSampling 2\n            -textureDisplay \"modulate\" \n            -textureMaxSize 16384\n            -fogging 0\n            -fogSource \"fragment\" \n            -fogMode \"linear\" \n            -fogStart 0\n            -fogEnd 100\n            -fogDensity 0.1\n            -fogColor 0.5 0.5 0.5 1 \n            -maxConstantTransparency 1\n            -rendererName \"base_OpenGL_Renderer\" \n"
		+ "            -objectFilterShowInHUD 1\n            -isFiltered 0\n            -colorResolution 4 4 \n            -bumpResolution 4 4 \n            -textureCompression 0\n            -transparencyAlgorithm \"frontAndBackCull\" \n            -transpInShadows 0\n            -cullingOverride \"none\" \n            -lowQualityLighting 0\n            -maximumNumHardwareLights 0\n            -occlusionCulling 0\n            -shadingModel 0\n            -useBaseRenderer 0\n            -useReducedRenderer 0\n            -smallObjectCulling 0\n            -smallObjectThreshold -1 \n            -interactiveDisableShadows 0\n            -interactiveBackFaceCull 0\n            -sortTransparent 1\n            -nurbsCurves 1\n            -nurbsSurfaces 1\n            -polymeshes 1\n            -subdivSurfaces 1\n            -planes 1\n            -lights 1\n            -cameras 1\n            -controlVertices 1\n            -hulls 1\n            -grid 1\n            -imagePlane 1\n            -joints 1\n            -ikHandles 1\n            -deformers 1\n            -dynamics 1\n"
		+ "            -fluids 1\n            -hairSystems 1\n            -follicles 1\n            -nCloths 1\n            -nParticles 1\n            -nRigids 1\n            -dynamicConstraints 1\n            -locators 1\n            -manipulators 1\n            -pluginShapes 1\n            -dimensions 1\n            -handles 1\n            -pivots 1\n            -textures 1\n            -strokes 1\n            -motionTrails 1\n            -clipGhosts 1\n            -greasePencils 1\n            -shadows 0\n            $editorName;\n        modelEditor -e -viewSelected 0 $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"modelPanel\" (localizedPanelLabel(\"Front View\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `modelPanel -unParent -l (localizedPanelLabel(\"Front View\")) -mbv $menusOkayInPanels `;\n\t\t\t$editorName = $panelName;\n            modelEditor -e \n                -camera \"front\" \n                -useInteractiveMode 0\n                -displayLights \"default\" \n"
		+ "                -displayAppearance \"wireframe\" \n                -activeOnly 0\n                -ignorePanZoom 0\n                -wireframeOnShaded 0\n                -headsUpDisplay 1\n                -selectionHiliteDisplay 1\n                -useDefaultMaterial 0\n                -bufferMode \"double\" \n                -twoSidedLighting 1\n                -backfaceCulling 0\n                -xray 0\n                -jointXray 0\n                -activeComponentsXray 0\n                -displayTextures 0\n                -smoothWireframe 0\n                -lineWidth 1\n                -textureAnisotropic 0\n                -textureHilight 1\n                -textureSampling 2\n                -textureDisplay \"modulate\" \n                -textureMaxSize 16384\n                -fogging 0\n                -fogSource \"fragment\" \n                -fogMode \"linear\" \n                -fogStart 0\n                -fogEnd 100\n                -fogDensity 0.1\n                -fogColor 0.5 0.5 0.5 1 \n                -maxConstantTransparency 1\n"
		+ "                -rendererName \"base_OpenGL_Renderer\" \n                -objectFilterShowInHUD 1\n                -isFiltered 0\n                -colorResolution 4 4 \n                -bumpResolution 4 4 \n                -textureCompression 0\n                -transparencyAlgorithm \"frontAndBackCull\" \n                -transpInShadows 0\n                -cullingOverride \"none\" \n                -lowQualityLighting 0\n                -maximumNumHardwareLights 0\n                -occlusionCulling 0\n                -shadingModel 0\n                -useBaseRenderer 0\n                -useReducedRenderer 0\n                -smallObjectCulling 0\n                -smallObjectThreshold -1 \n                -interactiveDisableShadows 0\n                -interactiveBackFaceCull 0\n                -sortTransparent 1\n                -nurbsCurves 1\n                -nurbsSurfaces 1\n                -polymeshes 1\n                -subdivSurfaces 1\n                -planes 1\n                -lights 1\n                -cameras 1\n                -controlVertices 1\n"
		+ "                -hulls 1\n                -grid 1\n                -imagePlane 1\n                -joints 1\n                -ikHandles 1\n                -deformers 1\n                -dynamics 1\n                -fluids 1\n                -hairSystems 1\n                -follicles 1\n                -nCloths 1\n                -nParticles 1\n                -nRigids 1\n                -dynamicConstraints 1\n                -locators 1\n                -manipulators 1\n                -pluginShapes 1\n                -dimensions 1\n                -handles 1\n                -pivots 1\n                -textures 1\n                -strokes 1\n                -motionTrails 1\n                -clipGhosts 1\n                -greasePencils 1\n                -shadows 0\n                $editorName;\n            modelEditor -e -viewSelected 0 $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tmodelPanel -edit -l (localizedPanelLabel(\"Front View\")) -mbv $menusOkayInPanels  $panelName;\n\t\t$editorName = $panelName;\n        modelEditor -e \n"
		+ "            -camera \"front\" \n            -useInteractiveMode 0\n            -displayLights \"default\" \n            -displayAppearance \"wireframe\" \n            -activeOnly 0\n            -ignorePanZoom 0\n            -wireframeOnShaded 0\n            -headsUpDisplay 1\n            -selectionHiliteDisplay 1\n            -useDefaultMaterial 0\n            -bufferMode \"double\" \n            -twoSidedLighting 1\n            -backfaceCulling 0\n            -xray 0\n            -jointXray 0\n            -activeComponentsXray 0\n            -displayTextures 0\n            -smoothWireframe 0\n            -lineWidth 1\n            -textureAnisotropic 0\n            -textureHilight 1\n            -textureSampling 2\n            -textureDisplay \"modulate\" \n            -textureMaxSize 16384\n            -fogging 0\n            -fogSource \"fragment\" \n            -fogMode \"linear\" \n            -fogStart 0\n            -fogEnd 100\n            -fogDensity 0.1\n            -fogColor 0.5 0.5 0.5 1 \n            -maxConstantTransparency 1\n            -rendererName \"base_OpenGL_Renderer\" \n"
		+ "            -objectFilterShowInHUD 1\n            -isFiltered 0\n            -colorResolution 4 4 \n            -bumpResolution 4 4 \n            -textureCompression 0\n            -transparencyAlgorithm \"frontAndBackCull\" \n            -transpInShadows 0\n            -cullingOverride \"none\" \n            -lowQualityLighting 0\n            -maximumNumHardwareLights 0\n            -occlusionCulling 0\n            -shadingModel 0\n            -useBaseRenderer 0\n            -useReducedRenderer 0\n            -smallObjectCulling 0\n            -smallObjectThreshold -1 \n            -interactiveDisableShadows 0\n            -interactiveBackFaceCull 0\n            -sortTransparent 1\n            -nurbsCurves 1\n            -nurbsSurfaces 1\n            -polymeshes 1\n            -subdivSurfaces 1\n            -planes 1\n            -lights 1\n            -cameras 1\n            -controlVertices 1\n            -hulls 1\n            -grid 1\n            -imagePlane 1\n            -joints 1\n            -ikHandles 1\n            -deformers 1\n            -dynamics 1\n"
		+ "            -fluids 1\n            -hairSystems 1\n            -follicles 1\n            -nCloths 1\n            -nParticles 1\n            -nRigids 1\n            -dynamicConstraints 1\n            -locators 1\n            -manipulators 1\n            -pluginShapes 1\n            -dimensions 1\n            -handles 1\n            -pivots 1\n            -textures 1\n            -strokes 1\n            -motionTrails 1\n            -clipGhosts 1\n            -greasePencils 1\n            -shadows 0\n            $editorName;\n        modelEditor -e -viewSelected 0 $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"modelPanel\" (localizedPanelLabel(\"Persp View\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `modelPanel -unParent -l (localizedPanelLabel(\"Persp View\")) -mbv $menusOkayInPanels `;\n\t\t\t$editorName = $panelName;\n            modelEditor -e \n                -camera \"persp\" \n                -useInteractiveMode 0\n                -displayLights \"default\" \n"
		+ "                -displayAppearance \"smoothShaded\" \n                -activeOnly 0\n                -ignorePanZoom 0\n                -wireframeOnShaded 0\n                -headsUpDisplay 1\n                -selectionHiliteDisplay 1\n                -useDefaultMaterial 0\n                -bufferMode \"double\" \n                -twoSidedLighting 1\n                -backfaceCulling 0\n                -xray 0\n                -jointXray 1\n                -activeComponentsXray 0\n                -displayTextures 1\n                -smoothWireframe 0\n                -lineWidth 1\n                -textureAnisotropic 0\n                -textureHilight 1\n                -textureSampling 2\n                -textureDisplay \"modulate\" \n                -textureMaxSize 16384\n                -fogging 0\n                -fogSource \"fragment\" \n                -fogMode \"linear\" \n                -fogStart 0\n                -fogEnd 100\n                -fogDensity 0.1\n                -fogColor 0.5 0.5 0.5 1 \n                -maxConstantTransparency 1\n"
		+ "                -rendererName \"base_OpenGL_Renderer\" \n                -objectFilterShowInHUD 1\n                -isFiltered 0\n                -colorResolution 4 4 \n                -bumpResolution 4 4 \n                -textureCompression 0\n                -transparencyAlgorithm \"frontAndBackCull\" \n                -transpInShadows 0\n                -cullingOverride \"none\" \n                -lowQualityLighting 0\n                -maximumNumHardwareLights 1\n                -occlusionCulling 0\n                -shadingModel 0\n                -useBaseRenderer 0\n                -useReducedRenderer 0\n                -smallObjectCulling 0\n                -smallObjectThreshold -1 \n                -interactiveDisableShadows 0\n                -interactiveBackFaceCull 0\n                -sortTransparent 1\n                -nurbsCurves 1\n                -nurbsSurfaces 1\n                -polymeshes 1\n                -subdivSurfaces 1\n                -planes 1\n                -lights 1\n                -cameras 1\n                -controlVertices 1\n"
		+ "                -hulls 1\n                -grid 1\n                -imagePlane 1\n                -joints 1\n                -ikHandles 1\n                -deformers 1\n                -dynamics 1\n                -fluids 1\n                -hairSystems 1\n                -follicles 1\n                -nCloths 1\n                -nParticles 1\n                -nRigids 1\n                -dynamicConstraints 1\n                -locators 1\n                -manipulators 1\n                -pluginShapes 1\n                -dimensions 1\n                -handles 1\n                -pivots 1\n                -textures 1\n                -strokes 1\n                -motionTrails 1\n                -clipGhosts 1\n                -greasePencils 1\n                -shadows 0\n                $editorName;\n            modelEditor -e -viewSelected 0 $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tmodelPanel -edit -l (localizedPanelLabel(\"Persp View\")) -mbv $menusOkayInPanels  $panelName;\n\t\t$editorName = $panelName;\n        modelEditor -e \n"
		+ "            -camera \"persp\" \n            -useInteractiveMode 0\n            -displayLights \"default\" \n            -displayAppearance \"smoothShaded\" \n            -activeOnly 0\n            -ignorePanZoom 0\n            -wireframeOnShaded 0\n            -headsUpDisplay 1\n            -selectionHiliteDisplay 1\n            -useDefaultMaterial 0\n            -bufferMode \"double\" \n            -twoSidedLighting 1\n            -backfaceCulling 0\n            -xray 0\n            -jointXray 1\n            -activeComponentsXray 0\n            -displayTextures 1\n            -smoothWireframe 0\n            -lineWidth 1\n            -textureAnisotropic 0\n            -textureHilight 1\n            -textureSampling 2\n            -textureDisplay \"modulate\" \n            -textureMaxSize 16384\n            -fogging 0\n            -fogSource \"fragment\" \n            -fogMode \"linear\" \n            -fogStart 0\n            -fogEnd 100\n            -fogDensity 0.1\n            -fogColor 0.5 0.5 0.5 1 \n            -maxConstantTransparency 1\n            -rendererName \"base_OpenGL_Renderer\" \n"
		+ "            -objectFilterShowInHUD 1\n            -isFiltered 0\n            -colorResolution 4 4 \n            -bumpResolution 4 4 \n            -textureCompression 0\n            -transparencyAlgorithm \"frontAndBackCull\" \n            -transpInShadows 0\n            -cullingOverride \"none\" \n            -lowQualityLighting 0\n            -maximumNumHardwareLights 1\n            -occlusionCulling 0\n            -shadingModel 0\n            -useBaseRenderer 0\n            -useReducedRenderer 0\n            -smallObjectCulling 0\n            -smallObjectThreshold -1 \n            -interactiveDisableShadows 0\n            -interactiveBackFaceCull 0\n            -sortTransparent 1\n            -nurbsCurves 1\n            -nurbsSurfaces 1\n            -polymeshes 1\n            -subdivSurfaces 1\n            -planes 1\n            -lights 1\n            -cameras 1\n            -controlVertices 1\n            -hulls 1\n            -grid 1\n            -imagePlane 1\n            -joints 1\n            -ikHandles 1\n            -deformers 1\n            -dynamics 1\n"
		+ "            -fluids 1\n            -hairSystems 1\n            -follicles 1\n            -nCloths 1\n            -nParticles 1\n            -nRigids 1\n            -dynamicConstraints 1\n            -locators 1\n            -manipulators 1\n            -pluginShapes 1\n            -dimensions 1\n            -handles 1\n            -pivots 1\n            -textures 1\n            -strokes 1\n            -motionTrails 1\n            -clipGhosts 1\n            -greasePencils 1\n            -shadows 0\n            $editorName;\n        modelEditor -e -viewSelected 0 $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"outlinerPanel\" (localizedPanelLabel(\"Outliner\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `outlinerPanel -unParent -l (localizedPanelLabel(\"Outliner\")) -mbv $menusOkayInPanels `;\n\t\t\t$editorName = $panelName;\n            outlinerEditor -e \n                -docTag \"isolOutln_fromSeln\" \n                -showShapes 0\n                -showReferenceNodes 0\n"
		+ "                -showReferenceMembers 0\n                -showAttributes 0\n                -showConnected 0\n                -showAnimCurvesOnly 0\n                -showMuteInfo 0\n                -organizeByLayer 1\n                -showAnimLayerWeight 1\n                -autoExpandLayers 1\n                -autoExpand 0\n                -showDagOnly 1\n                -showAssets 1\n                -showContainedOnly 1\n                -showPublishedAsConnected 0\n                -showContainerContents 1\n                -ignoreDagHierarchy 0\n                -expandConnections 0\n                -showUpstreamCurves 1\n                -showUnitlessCurves 1\n                -showCompounds 1\n                -showLeafs 1\n                -showNumericAttrsOnly 0\n                -highlightActive 1\n                -autoSelectNewObjects 0\n                -doNotSelectNewObjects 0\n                -dropIsParent 1\n                -transmitFilters 0\n                -setFilter \"defaultSetFilter\" \n                -showSetMembers 1\n                -allowMultiSelection 1\n"
		+ "                -alwaysToggleSelect 0\n                -directSelect 0\n                -displayMode \"DAG\" \n                -expandObjects 0\n                -setsIgnoreFilters 1\n                -containersIgnoreFilters 0\n                -editAttrName 0\n                -showAttrValues 0\n                -highlightSecondary 0\n                -showUVAttrsOnly 0\n                -showTextureNodesOnly 0\n                -attrAlphaOrder \"default\" \n                -animLayerFilterOptions \"allAffecting\" \n                -sortOrder \"none\" \n                -longNames 0\n                -niceNames 1\n                -showNamespace 1\n                -showPinIcons 0\n                -mapMotionTrails 0\n                $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\toutlinerPanel -edit -l (localizedPanelLabel(\"Outliner\")) -mbv $menusOkayInPanels  $panelName;\n\t\t$editorName = $panelName;\n        outlinerEditor -e \n            -docTag \"isolOutln_fromSeln\" \n            -showShapes 0\n            -showReferenceNodes 0\n"
		+ "            -showReferenceMembers 0\n            -showAttributes 0\n            -showConnected 0\n            -showAnimCurvesOnly 0\n            -showMuteInfo 0\n            -organizeByLayer 1\n            -showAnimLayerWeight 1\n            -autoExpandLayers 1\n            -autoExpand 0\n            -showDagOnly 1\n            -showAssets 1\n            -showContainedOnly 1\n            -showPublishedAsConnected 0\n            -showContainerContents 1\n            -ignoreDagHierarchy 0\n            -expandConnections 0\n            -showUpstreamCurves 1\n            -showUnitlessCurves 1\n            -showCompounds 1\n            -showLeafs 1\n            -showNumericAttrsOnly 0\n            -highlightActive 1\n            -autoSelectNewObjects 0\n            -doNotSelectNewObjects 0\n            -dropIsParent 1\n            -transmitFilters 0\n            -setFilter \"defaultSetFilter\" \n            -showSetMembers 1\n            -allowMultiSelection 1\n            -alwaysToggleSelect 0\n            -directSelect 0\n            -displayMode \"DAG\" \n"
		+ "            -expandObjects 0\n            -setsIgnoreFilters 1\n            -containersIgnoreFilters 0\n            -editAttrName 0\n            -showAttrValues 0\n            -highlightSecondary 0\n            -showUVAttrsOnly 0\n            -showTextureNodesOnly 0\n            -attrAlphaOrder \"default\" \n            -animLayerFilterOptions \"allAffecting\" \n            -sortOrder \"none\" \n            -longNames 0\n            -niceNames 1\n            -showNamespace 1\n            -showPinIcons 0\n            -mapMotionTrails 0\n            $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"graphEditor\" (localizedPanelLabel(\"Graph Editor\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"graphEditor\" -l (localizedPanelLabel(\"Graph Editor\")) -mbv $menusOkayInPanels `;\n\n\t\t\t$editorName = ($panelName+\"OutlineEd\");\n            outlinerEditor -e \n                -showShapes 1\n                -showReferenceNodes 0\n"
		+ "                -showReferenceMembers 0\n                -showAttributes 1\n                -showConnected 1\n                -showAnimCurvesOnly 1\n                -showMuteInfo 0\n                -organizeByLayer 1\n                -showAnimLayerWeight 1\n                -autoExpandLayers 1\n                -autoExpand 1\n                -showDagOnly 0\n                -showAssets 1\n                -showContainedOnly 0\n                -showPublishedAsConnected 0\n                -showContainerContents 0\n                -ignoreDagHierarchy 0\n                -expandConnections 1\n                -showUpstreamCurves 1\n                -showUnitlessCurves 1\n                -showCompounds 0\n                -showLeafs 1\n                -showNumericAttrsOnly 1\n                -highlightActive 0\n                -autoSelectNewObjects 1\n                -doNotSelectNewObjects 0\n                -dropIsParent 1\n                -transmitFilters 1\n                -setFilter \"0\" \n                -showSetMembers 0\n                -allowMultiSelection 1\n"
		+ "                -alwaysToggleSelect 0\n                -directSelect 0\n                -displayMode \"DAG\" \n                -expandObjects 0\n                -setsIgnoreFilters 1\n                -containersIgnoreFilters 0\n                -editAttrName 0\n                -showAttrValues 0\n                -highlightSecondary 0\n                -showUVAttrsOnly 0\n                -showTextureNodesOnly 0\n                -attrAlphaOrder \"default\" \n                -animLayerFilterOptions \"allAffecting\" \n                -sortOrder \"none\" \n                -longNames 0\n                -niceNames 1\n                -showNamespace 1\n                -showPinIcons 1\n                -mapMotionTrails 1\n                $editorName;\n\n\t\t\t$editorName = ($panelName+\"GraphEd\");\n            animCurveEditor -e \n                -displayKeys 1\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 1\n                -displayInfinities 0\n                -autoFit 0\n                -snapTime \"integer\" \n"
		+ "                -snapValue \"none\" \n                -showResults \"off\" \n                -showBufferCurves \"off\" \n                -smoothness \"fine\" \n                -resultSamples 1\n                -resultScreenSamples 0\n                -resultUpdate \"delayed\" \n                -showUpstreamCurves 1\n                -stackedCurves 0\n                -stackedCurvesMin -1\n                -stackedCurvesMax 1\n                -stackedCurvesSpace 0.2\n                -displayNormalized 0\n                -preSelectionHighlight 0\n                -constrainDrag 0\n                -classicMode 1\n                $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Graph Editor\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = ($panelName+\"OutlineEd\");\n            outlinerEditor -e \n                -showShapes 1\n                -showReferenceNodes 0\n                -showReferenceMembers 0\n                -showAttributes 1\n                -showConnected 1\n                -showAnimCurvesOnly 1\n"
		+ "                -showMuteInfo 0\n                -organizeByLayer 1\n                -showAnimLayerWeight 1\n                -autoExpandLayers 1\n                -autoExpand 1\n                -showDagOnly 0\n                -showAssets 1\n                -showContainedOnly 0\n                -showPublishedAsConnected 0\n                -showContainerContents 0\n                -ignoreDagHierarchy 0\n                -expandConnections 1\n                -showUpstreamCurves 1\n                -showUnitlessCurves 1\n                -showCompounds 0\n                -showLeafs 1\n                -showNumericAttrsOnly 1\n                -highlightActive 0\n                -autoSelectNewObjects 1\n                -doNotSelectNewObjects 0\n                -dropIsParent 1\n                -transmitFilters 1\n                -setFilter \"0\" \n                -showSetMembers 0\n                -allowMultiSelection 1\n                -alwaysToggleSelect 0\n                -directSelect 0\n                -displayMode \"DAG\" \n                -expandObjects 0\n"
		+ "                -setsIgnoreFilters 1\n                -containersIgnoreFilters 0\n                -editAttrName 0\n                -showAttrValues 0\n                -highlightSecondary 0\n                -showUVAttrsOnly 0\n                -showTextureNodesOnly 0\n                -attrAlphaOrder \"default\" \n                -animLayerFilterOptions \"allAffecting\" \n                -sortOrder \"none\" \n                -longNames 0\n                -niceNames 1\n                -showNamespace 1\n                -showPinIcons 1\n                -mapMotionTrails 1\n                $editorName;\n\n\t\t\t$editorName = ($panelName+\"GraphEd\");\n            animCurveEditor -e \n                -displayKeys 1\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 1\n                -displayInfinities 0\n                -autoFit 0\n                -snapTime \"integer\" \n                -snapValue \"none\" \n                -showResults \"off\" \n                -showBufferCurves \"off\" \n                -smoothness \"fine\" \n"
		+ "                -resultSamples 1\n                -resultScreenSamples 0\n                -resultUpdate \"delayed\" \n                -showUpstreamCurves 1\n                -stackedCurves 0\n                -stackedCurvesMin -1\n                -stackedCurvesMax 1\n                -stackedCurvesSpace 0.2\n                -displayNormalized 0\n                -preSelectionHighlight 0\n                -constrainDrag 0\n                -classicMode 1\n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"dopeSheetPanel\" (localizedPanelLabel(\"Dope Sheet\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"dopeSheetPanel\" -l (localizedPanelLabel(\"Dope Sheet\")) -mbv $menusOkayInPanels `;\n\n\t\t\t$editorName = ($panelName+\"OutlineEd\");\n            outlinerEditor -e \n                -showShapes 1\n                -showReferenceNodes 0\n                -showReferenceMembers 0\n                -showAttributes 1\n"
		+ "                -showConnected 1\n                -showAnimCurvesOnly 1\n                -showMuteInfo 0\n                -organizeByLayer 1\n                -showAnimLayerWeight 1\n                -autoExpandLayers 1\n                -autoExpand 0\n                -showDagOnly 0\n                -showAssets 1\n                -showContainedOnly 0\n                -showPublishedAsConnected 0\n                -showContainerContents 0\n                -ignoreDagHierarchy 0\n                -expandConnections 1\n                -showUpstreamCurves 1\n                -showUnitlessCurves 0\n                -showCompounds 1\n                -showLeafs 1\n                -showNumericAttrsOnly 1\n                -highlightActive 0\n                -autoSelectNewObjects 0\n                -doNotSelectNewObjects 1\n                -dropIsParent 1\n                -transmitFilters 0\n                -setFilter \"0\" \n                -showSetMembers 0\n                -allowMultiSelection 1\n                -alwaysToggleSelect 0\n                -directSelect 0\n"
		+ "                -displayMode \"DAG\" \n                -expandObjects 0\n                -setsIgnoreFilters 1\n                -containersIgnoreFilters 0\n                -editAttrName 0\n                -showAttrValues 0\n                -highlightSecondary 0\n                -showUVAttrsOnly 0\n                -showTextureNodesOnly 0\n                -attrAlphaOrder \"default\" \n                -animLayerFilterOptions \"allAffecting\" \n                -sortOrder \"none\" \n                -longNames 0\n                -niceNames 1\n                -showNamespace 1\n                -showPinIcons 0\n                -mapMotionTrails 1\n                $editorName;\n\n\t\t\t$editorName = ($panelName+\"DopeSheetEd\");\n            dopeSheetEditor -e \n                -displayKeys 1\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 0\n                -displayInfinities 0\n                -autoFit 0\n                -snapTime \"integer\" \n                -snapValue \"none\" \n                -outliner \"dopeSheetPanel1OutlineEd\" \n"
		+ "                -showSummary 1\n                -showScene 0\n                -hierarchyBelow 0\n                -showTicks 1\n                -selectionWindow 0 0 0 0 \n                $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Dope Sheet\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = ($panelName+\"OutlineEd\");\n            outlinerEditor -e \n                -showShapes 1\n                -showReferenceNodes 0\n                -showReferenceMembers 0\n                -showAttributes 1\n                -showConnected 1\n                -showAnimCurvesOnly 1\n                -showMuteInfo 0\n                -organizeByLayer 1\n                -showAnimLayerWeight 1\n                -autoExpandLayers 1\n                -autoExpand 0\n                -showDagOnly 0\n                -showAssets 1\n                -showContainedOnly 0\n                -showPublishedAsConnected 0\n                -showContainerContents 0\n                -ignoreDagHierarchy 0\n"
		+ "                -expandConnections 1\n                -showUpstreamCurves 1\n                -showUnitlessCurves 0\n                -showCompounds 1\n                -showLeafs 1\n                -showNumericAttrsOnly 1\n                -highlightActive 0\n                -autoSelectNewObjects 0\n                -doNotSelectNewObjects 1\n                -dropIsParent 1\n                -transmitFilters 0\n                -setFilter \"0\" \n                -showSetMembers 0\n                -allowMultiSelection 1\n                -alwaysToggleSelect 0\n                -directSelect 0\n                -displayMode \"DAG\" \n                -expandObjects 0\n                -setsIgnoreFilters 1\n                -containersIgnoreFilters 0\n                -editAttrName 0\n                -showAttrValues 0\n                -highlightSecondary 0\n                -showUVAttrsOnly 0\n                -showTextureNodesOnly 0\n                -attrAlphaOrder \"default\" \n                -animLayerFilterOptions \"allAffecting\" \n                -sortOrder \"none\" \n"
		+ "                -longNames 0\n                -niceNames 1\n                -showNamespace 1\n                -showPinIcons 0\n                -mapMotionTrails 1\n                $editorName;\n\n\t\t\t$editorName = ($panelName+\"DopeSheetEd\");\n            dopeSheetEditor -e \n                -displayKeys 1\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 0\n                -displayInfinities 0\n                -autoFit 0\n                -snapTime \"integer\" \n                -snapValue \"none\" \n                -outliner \"dopeSheetPanel1OutlineEd\" \n                -showSummary 1\n                -showScene 0\n                -hierarchyBelow 0\n                -showTicks 1\n                -selectionWindow 0 0 0 0 \n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"clipEditorPanel\" (localizedPanelLabel(\"Trax Editor\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n"
		+ "\t\t\t$panelName = `scriptedPanel -unParent  -type \"clipEditorPanel\" -l (localizedPanelLabel(\"Trax Editor\")) -mbv $menusOkayInPanels `;\n\n\t\t\t$editorName = clipEditorNameFromPanel($panelName);\n            clipEditor -e \n                -displayKeys 0\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 0\n                -displayInfinities 0\n                -autoFit 0\n                -snapTime \"none\" \n                -snapValue \"none\" \n                -manageSequencer 0 \n                $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Trax Editor\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = clipEditorNameFromPanel($panelName);\n            clipEditor -e \n                -displayKeys 0\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 0\n                -displayInfinities 0\n                -autoFit 0\n                -snapTime \"none\" \n"
		+ "                -snapValue \"none\" \n                -manageSequencer 0 \n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"sequenceEditorPanel\" (localizedPanelLabel(\"Camera Sequencer\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"sequenceEditorPanel\" -l (localizedPanelLabel(\"Camera Sequencer\")) -mbv $menusOkayInPanels `;\n\n\t\t\t$editorName = sequenceEditorNameFromPanel($panelName);\n            clipEditor -e \n                -displayKeys 0\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 0\n                -displayInfinities 0\n                -autoFit 0\n                -snapTime \"none\" \n                -snapValue \"none\" \n                -manageSequencer 1 \n                $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Camera Sequencer\")) -mbv $menusOkayInPanels  $panelName;\n"
		+ "\t\t\t$editorName = sequenceEditorNameFromPanel($panelName);\n            clipEditor -e \n                -displayKeys 0\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 0\n                -displayInfinities 0\n                -autoFit 0\n                -snapTime \"none\" \n                -snapValue \"none\" \n                -manageSequencer 1 \n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"hyperGraphPanel\" (localizedPanelLabel(\"Hypergraph Hierarchy\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"hyperGraphPanel\" -l (localizedPanelLabel(\"Hypergraph Hierarchy\")) -mbv $menusOkayInPanels `;\n\n\t\t\t$editorName = ($panelName+\"HyperGraphEd\");\n            hyperGraph -e \n                -graphLayoutStyle \"hierarchicalLayout\" \n                -orientation \"horiz\" \n                -mergeConnections 1\n                -zoom 1\n"
		+ "                -animateTransition 0\n                -showRelationships 1\n                -showShapes 0\n                -showDeformers 0\n                -showExpressions 0\n                -showConstraints 0\n                -showConnectionFromSelected 0\n                -showConnectionToSelected 0\n                -showUnderworld 0\n                -showInvisible 0\n                -transitionFrames 1\n                -opaqueContainers 0\n                -freeform 0\n                -imagePosition 0 0 \n                -imageScale 1\n                -imageEnabled 0\n                -graphType \"DAG\" \n                -heatMapDisplay 0\n                -updateSelection 1\n                -updateNodeAdded 1\n                -useDrawOverrideColor 0\n                -limitGraphTraversal -1\n                -range 0 0 \n                -iconSize \"smallIcons\" \n                -showCachedConnections 0\n                $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Hypergraph Hierarchy\")) -mbv $menusOkayInPanels  $panelName;\n"
		+ "\t\t\t$editorName = ($panelName+\"HyperGraphEd\");\n            hyperGraph -e \n                -graphLayoutStyle \"hierarchicalLayout\" \n                -orientation \"horiz\" \n                -mergeConnections 1\n                -zoom 1\n                -animateTransition 0\n                -showRelationships 1\n                -showShapes 0\n                -showDeformers 0\n                -showExpressions 0\n                -showConstraints 0\n                -showConnectionFromSelected 0\n                -showConnectionToSelected 0\n                -showUnderworld 0\n                -showInvisible 0\n                -transitionFrames 1\n                -opaqueContainers 0\n                -freeform 0\n                -imagePosition 0 0 \n                -imageScale 1\n                -imageEnabled 0\n                -graphType \"DAG\" \n                -heatMapDisplay 0\n                -updateSelection 1\n                -updateNodeAdded 1\n                -useDrawOverrideColor 0\n                -limitGraphTraversal -1\n                -range 0 0 \n"
		+ "                -iconSize \"smallIcons\" \n                -showCachedConnections 0\n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"hyperShadePanel\" (localizedPanelLabel(\"Hypershade\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"hyperShadePanel\" -l (localizedPanelLabel(\"Hypershade\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Hypershade\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"visorPanel\" (localizedPanelLabel(\"Visor\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"visorPanel\" -l (localizedPanelLabel(\"Visor\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n"
		+ "\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Visor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"nodeEditorPanel\" (localizedPanelLabel(\"Node Editor\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"nodeEditorPanel\" -l (localizedPanelLabel(\"Node Editor\")) -mbv $menusOkayInPanels `;\n\n\t\t\t$editorName = ($panelName+\"NodeEditorEd\");\n            nodeEditor -e \n                -allAttributes 0\n                -allNodes 0\n                -autoSizeNodes 1\n                -createNodeCommand \"nodeEdCreateNodeCommand\" \n                -defaultPinnedState 0\n                -ignoreAssets 1\n                -additiveGraphingMode 0\n                -settingsChangedCallback \"nodeEdSyncControls\" \n                -traversalDepthLimit -1\n                -keyPressCommand \"nodeEdKeyPressCommand\" \n                -keyReleaseCommand \"nodeEdKeyReleaseCommand\" \n                -nodeTitleMode \"name\" \n"
		+ "                -gridSnap 0\n                -gridVisibility 1\n                -popupMenuScript \"nodeEdBuildPanelMenus\" \n                -island 0\n                -showNamespace 1\n                -showShapes 1\n                -showSGShapes 0\n                -showTransforms 1\n                -syncedSelection 1\n                -extendToShapes 1\n                $editorName;;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Node Editor\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = ($panelName+\"NodeEditorEd\");\n            nodeEditor -e \n                -allAttributes 0\n                -allNodes 0\n                -autoSizeNodes 1\n                -createNodeCommand \"nodeEdCreateNodeCommand\" \n                -defaultPinnedState 0\n                -ignoreAssets 1\n                -additiveGraphingMode 0\n                -settingsChangedCallback \"nodeEdSyncControls\" \n                -traversalDepthLimit -1\n                -keyPressCommand \"nodeEdKeyPressCommand\" \n"
		+ "                -keyReleaseCommand \"nodeEdKeyReleaseCommand\" \n                -nodeTitleMode \"name\" \n                -gridSnap 0\n                -gridVisibility 1\n                -popupMenuScript \"nodeEdBuildPanelMenus\" \n                -island 0\n                -showNamespace 1\n                -showShapes 1\n                -showSGShapes 0\n                -showTransforms 1\n                -syncedSelection 1\n                -extendToShapes 1\n                $editorName;;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"createNodePanel\" (localizedPanelLabel(\"Create Node\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"createNodePanel\" -l (localizedPanelLabel(\"Create Node\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Create Node\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n"
		+ "\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"polyTexturePlacementPanel\" (localizedPanelLabel(\"UV Texture Editor\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"polyTexturePlacementPanel\" -l (localizedPanelLabel(\"UV Texture Editor\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"UV Texture Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"renderWindowPanel\" (localizedPanelLabel(\"Render View\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"renderWindowPanel\" -l (localizedPanelLabel(\"Render View\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Render View\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n"
		+ "\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"blendShapePanel\" (localizedPanelLabel(\"Blend Shape\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\tblendShapePanel -unParent -l (localizedPanelLabel(\"Blend Shape\")) -mbv $menusOkayInPanels ;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tblendShapePanel -edit -l (localizedPanelLabel(\"Blend Shape\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"dynRelEdPanel\" (localizedPanelLabel(\"Dynamic Relationships\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"dynRelEdPanel\" -l (localizedPanelLabel(\"Dynamic Relationships\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Dynamic Relationships\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n"
		+ "\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"relationshipPanel\" (localizedPanelLabel(\"Relationship Editor\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"relationshipPanel\" -l (localizedPanelLabel(\"Relationship Editor\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Relationship Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"referenceEditorPanel\" (localizedPanelLabel(\"Reference Editor\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"referenceEditorPanel\" -l (localizedPanelLabel(\"Reference Editor\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Reference Editor\")) -mbv $menusOkayInPanels  $panelName;\n"
		+ "\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"componentEditorPanel\" (localizedPanelLabel(\"Component Editor\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"componentEditorPanel\" -l (localizedPanelLabel(\"Component Editor\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Component Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"dynPaintScriptedPanelType\" (localizedPanelLabel(\"Paint Effects\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"dynPaintScriptedPanelType\" -l (localizedPanelLabel(\"Paint Effects\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Paint Effects\")) -mbv $menusOkayInPanels  $panelName;\n"
		+ "\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"scriptEditorPanel\" (localizedPanelLabel(\"Script Editor\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"scriptEditorPanel\" -l (localizedPanelLabel(\"Script Editor\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Script Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\tif ($useSceneConfig) {\n        string $configName = `getPanel -cwl (localizedPanelLabel(\"Current Layout\"))`;\n        if (\"\" != $configName) {\n\t\t\tpanelConfiguration -edit -label (localizedPanelLabel(\"Current Layout\")) \n\t\t\t\t-defaultImage \"\"\n\t\t\t\t-image \"\"\n\t\t\t\t-sc false\n\t\t\t\t-configString \"global string $gMainPane; paneLayout -e -cn \\\"vertical2\\\" -ps 1 29 100 -ps 2 71 100 $gMainPane;\"\n\t\t\t\t-removeAllPanels\n\t\t\t\t-ap false\n\t\t\t\t\t(localizedPanelLabel(\"Outliner\")) \n"
		+ "\t\t\t\t\t\"outlinerPanel\"\n\t\t\t\t\t\"$panelName = `outlinerPanel -unParent -l (localizedPanelLabel(\\\"Outliner\\\")) -mbv $menusOkayInPanels `;\\n$editorName = $panelName;\\noutlinerEditor -e \\n    -docTag \\\"isolOutln_fromSeln\\\" \\n    -showShapes 0\\n    -showReferenceNodes 0\\n    -showReferenceMembers 0\\n    -showAttributes 0\\n    -showConnected 0\\n    -showAnimCurvesOnly 0\\n    -showMuteInfo 0\\n    -organizeByLayer 1\\n    -showAnimLayerWeight 1\\n    -autoExpandLayers 1\\n    -autoExpand 0\\n    -showDagOnly 1\\n    -showAssets 1\\n    -showContainedOnly 1\\n    -showPublishedAsConnected 0\\n    -showContainerContents 1\\n    -ignoreDagHierarchy 0\\n    -expandConnections 0\\n    -showUpstreamCurves 1\\n    -showUnitlessCurves 1\\n    -showCompounds 1\\n    -showLeafs 1\\n    -showNumericAttrsOnly 0\\n    -highlightActive 1\\n    -autoSelectNewObjects 0\\n    -doNotSelectNewObjects 0\\n    -dropIsParent 1\\n    -transmitFilters 0\\n    -setFilter \\\"defaultSetFilter\\\" \\n    -showSetMembers 1\\n    -allowMultiSelection 1\\n    -alwaysToggleSelect 0\\n    -directSelect 0\\n    -displayMode \\\"DAG\\\" \\n    -expandObjects 0\\n    -setsIgnoreFilters 1\\n    -containersIgnoreFilters 0\\n    -editAttrName 0\\n    -showAttrValues 0\\n    -highlightSecondary 0\\n    -showUVAttrsOnly 0\\n    -showTextureNodesOnly 0\\n    -attrAlphaOrder \\\"default\\\" \\n    -animLayerFilterOptions \\\"allAffecting\\\" \\n    -sortOrder \\\"none\\\" \\n    -longNames 0\\n    -niceNames 1\\n    -showNamespace 1\\n    -showPinIcons 0\\n    -mapMotionTrails 0\\n    $editorName\"\n"
		+ "\t\t\t\t\t\"outlinerPanel -edit -l (localizedPanelLabel(\\\"Outliner\\\")) -mbv $menusOkayInPanels  $panelName;\\n$editorName = $panelName;\\noutlinerEditor -e \\n    -docTag \\\"isolOutln_fromSeln\\\" \\n    -showShapes 0\\n    -showReferenceNodes 0\\n    -showReferenceMembers 0\\n    -showAttributes 0\\n    -showConnected 0\\n    -showAnimCurvesOnly 0\\n    -showMuteInfo 0\\n    -organizeByLayer 1\\n    -showAnimLayerWeight 1\\n    -autoExpandLayers 1\\n    -autoExpand 0\\n    -showDagOnly 1\\n    -showAssets 1\\n    -showContainedOnly 1\\n    -showPublishedAsConnected 0\\n    -showContainerContents 1\\n    -ignoreDagHierarchy 0\\n    -expandConnections 0\\n    -showUpstreamCurves 1\\n    -showUnitlessCurves 1\\n    -showCompounds 1\\n    -showLeafs 1\\n    -showNumericAttrsOnly 0\\n    -highlightActive 1\\n    -autoSelectNewObjects 0\\n    -doNotSelectNewObjects 0\\n    -dropIsParent 1\\n    -transmitFilters 0\\n    -setFilter \\\"defaultSetFilter\\\" \\n    -showSetMembers 1\\n    -allowMultiSelection 1\\n    -alwaysToggleSelect 0\\n    -directSelect 0\\n    -displayMode \\\"DAG\\\" \\n    -expandObjects 0\\n    -setsIgnoreFilters 1\\n    -containersIgnoreFilters 0\\n    -editAttrName 0\\n    -showAttrValues 0\\n    -highlightSecondary 0\\n    -showUVAttrsOnly 0\\n    -showTextureNodesOnly 0\\n    -attrAlphaOrder \\\"default\\\" \\n    -animLayerFilterOptions \\\"allAffecting\\\" \\n    -sortOrder \\\"none\\\" \\n    -longNames 0\\n    -niceNames 1\\n    -showNamespace 1\\n    -showPinIcons 0\\n    -mapMotionTrails 0\\n    $editorName\"\n"
		+ "\t\t\t\t-ap false\n\t\t\t\t\t(localizedPanelLabel(\"Persp View\")) \n\t\t\t\t\t\"modelPanel\"\n"
		+ "\t\t\t\t\t\"$panelName = `modelPanel -unParent -l (localizedPanelLabel(\\\"Persp View\\\")) -mbv $menusOkayInPanels `;\\n$editorName = $panelName;\\nmodelEditor -e \\n    -cam `findStartUpCamera persp` \\n    -useInteractiveMode 0\\n    -displayLights \\\"default\\\" \\n    -displayAppearance \\\"smoothShaded\\\" \\n    -activeOnly 0\\n    -ignorePanZoom 0\\n    -wireframeOnShaded 0\\n    -headsUpDisplay 1\\n    -selectionHiliteDisplay 1\\n    -useDefaultMaterial 0\\n    -bufferMode \\\"double\\\" \\n    -twoSidedLighting 1\\n    -backfaceCulling 0\\n    -xray 0\\n    -jointXray 1\\n    -activeComponentsXray 0\\n    -displayTextures 1\\n    -smoothWireframe 0\\n    -lineWidth 1\\n    -textureAnisotropic 0\\n    -textureHilight 1\\n    -textureSampling 2\\n    -textureDisplay \\\"modulate\\\" \\n    -textureMaxSize 16384\\n    -fogging 0\\n    -fogSource \\\"fragment\\\" \\n    -fogMode \\\"linear\\\" \\n    -fogStart 0\\n    -fogEnd 100\\n    -fogDensity 0.1\\n    -fogColor 0.5 0.5 0.5 1 \\n    -maxConstantTransparency 1\\n    -rendererName \\\"base_OpenGL_Renderer\\\" \\n    -objectFilterShowInHUD 1\\n    -isFiltered 0\\n    -colorResolution 4 4 \\n    -bumpResolution 4 4 \\n    -textureCompression 0\\n    -transparencyAlgorithm \\\"frontAndBackCull\\\" \\n    -transpInShadows 0\\n    -cullingOverride \\\"none\\\" \\n    -lowQualityLighting 0\\n    -maximumNumHardwareLights 1\\n    -occlusionCulling 0\\n    -shadingModel 0\\n    -useBaseRenderer 0\\n    -useReducedRenderer 0\\n    -smallObjectCulling 0\\n    -smallObjectThreshold -1 \\n    -interactiveDisableShadows 0\\n    -interactiveBackFaceCull 0\\n    -sortTransparent 1\\n    -nurbsCurves 1\\n    -nurbsSurfaces 1\\n    -polymeshes 1\\n    -subdivSurfaces 1\\n    -planes 1\\n    -lights 1\\n    -cameras 1\\n    -controlVertices 1\\n    -hulls 1\\n    -grid 1\\n    -imagePlane 1\\n    -joints 1\\n    -ikHandles 1\\n    -deformers 1\\n    -dynamics 1\\n    -fluids 1\\n    -hairSystems 1\\n    -follicles 1\\n    -nCloths 1\\n    -nParticles 1\\n    -nRigids 1\\n    -dynamicConstraints 1\\n    -locators 1\\n    -manipulators 1\\n    -pluginShapes 1\\n    -dimensions 1\\n    -handles 1\\n    -pivots 1\\n    -textures 1\\n    -strokes 1\\n    -motionTrails 1\\n    -clipGhosts 1\\n    -greasePencils 1\\n    -shadows 0\\n    $editorName;\\nmodelEditor -e -viewSelected 0 $editorName\"\n"
		+ "\t\t\t\t\t\"modelPanel -edit -l (localizedPanelLabel(\\\"Persp View\\\")) -mbv $menusOkayInPanels  $panelName;\\n$editorName = $panelName;\\nmodelEditor -e \\n    -cam `findStartUpCamera persp` \\n    -useInteractiveMode 0\\n    -displayLights \\\"default\\\" \\n    -displayAppearance \\\"smoothShaded\\\" \\n    -activeOnly 0\\n    -ignorePanZoom 0\\n    -wireframeOnShaded 0\\n    -headsUpDisplay 1\\n    -selectionHiliteDisplay 1\\n    -useDefaultMaterial 0\\n    -bufferMode \\\"double\\\" \\n    -twoSidedLighting 1\\n    -backfaceCulling 0\\n    -xray 0\\n    -jointXray 1\\n    -activeComponentsXray 0\\n    -displayTextures 1\\n    -smoothWireframe 0\\n    -lineWidth 1\\n    -textureAnisotropic 0\\n    -textureHilight 1\\n    -textureSampling 2\\n    -textureDisplay \\\"modulate\\\" \\n    -textureMaxSize 16384\\n    -fogging 0\\n    -fogSource \\\"fragment\\\" \\n    -fogMode \\\"linear\\\" \\n    -fogStart 0\\n    -fogEnd 100\\n    -fogDensity 0.1\\n    -fogColor 0.5 0.5 0.5 1 \\n    -maxConstantTransparency 1\\n    -rendererName \\\"base_OpenGL_Renderer\\\" \\n    -objectFilterShowInHUD 1\\n    -isFiltered 0\\n    -colorResolution 4 4 \\n    -bumpResolution 4 4 \\n    -textureCompression 0\\n    -transparencyAlgorithm \\\"frontAndBackCull\\\" \\n    -transpInShadows 0\\n    -cullingOverride \\\"none\\\" \\n    -lowQualityLighting 0\\n    -maximumNumHardwareLights 1\\n    -occlusionCulling 0\\n    -shadingModel 0\\n    -useBaseRenderer 0\\n    -useReducedRenderer 0\\n    -smallObjectCulling 0\\n    -smallObjectThreshold -1 \\n    -interactiveDisableShadows 0\\n    -interactiveBackFaceCull 0\\n    -sortTransparent 1\\n    -nurbsCurves 1\\n    -nurbsSurfaces 1\\n    -polymeshes 1\\n    -subdivSurfaces 1\\n    -planes 1\\n    -lights 1\\n    -cameras 1\\n    -controlVertices 1\\n    -hulls 1\\n    -grid 1\\n    -imagePlane 1\\n    -joints 1\\n    -ikHandles 1\\n    -deformers 1\\n    -dynamics 1\\n    -fluids 1\\n    -hairSystems 1\\n    -follicles 1\\n    -nCloths 1\\n    -nParticles 1\\n    -nRigids 1\\n    -dynamicConstraints 1\\n    -locators 1\\n    -manipulators 1\\n    -pluginShapes 1\\n    -dimensions 1\\n    -handles 1\\n    -pivots 1\\n    -textures 1\\n    -strokes 1\\n    -motionTrails 1\\n    -clipGhosts 1\\n    -greasePencils 1\\n    -shadows 0\\n    $editorName;\\nmodelEditor -e -viewSelected 0 $editorName\"\n"
		+ "\t\t\t\t$configName;\n\n            setNamedPanelLayout (localizedPanelLabel(\"Current Layout\"));\n        }\n\n        panelHistory -e -clear mainPanelHistory;\n        setFocus `paneLayout -q -p1 $gMainPane`;\n        sceneUIReplacement -deleteRemaining;\n        sceneUIReplacement -clear;\n\t}\n\n\ngrid -spacing 5 -size 12 -divisions 5 -displayAxes yes -displayGridLines yes -displayDivisionLines yes -displayPerspectiveLabels no -displayOrthographicLabels no -displayAxesBold yes -perspectiveLabelPosition axis -orthographicLabelPosition edge;\nviewManip -drawCompass 0 -compassAngle 0 -frontParameters \"\" -homeParameters \"\" -selectionLockParameters \"\";\n}\n");
	setAttr ".st" 3;
createNode script -n "idle_v1:sceneConfigurationScriptNode";
	setAttr ".b" -type "string" "playbackOptions -min 0 -max 440 -ast 0 -aet 6000 ";
	setAttr ".st" 6;
createNode phong -n "idle_v1_business05_f_highpolyPhong";
	setAttr ".sc" -type "float3" 0 0 0 ;
createNode shadingEngine -n "idle_v1_business05_f_highpolySG";
	setAttr ".ihi" 0;
	setAttr ".ro" yes;
createNode materialInfo -n "idle_v1_materialInfo1";
createNode shadingEngine -n "idle_v1_business05_f_highpolySG1";
	setAttr ".ihi" 0;
	setAttr ".ro" yes;
createNode materialInfo -n "idle_v1_materialInfo2";
createNode file -n "idle_v1_Map1";
	setAttr ".ftn" -type "string" "C:\\Users\\Kuan\\Desktop\\iPi Record 20150227\\business05_f\\business05_f_35.tga";
createNode place2dTexture -n "idle_v1_place2dTexture1";
createNode phong -n "idle_v1_business05_f_highpoly_hairPhong";
	setAttr ".c" -type "float3" 0 0 0 ;
	setAttr ".sc" -type "float3" 0 0 0 ;
createNode shadingEngine -n "idle_v1_business05_f_highpoly_hairSG";
	setAttr ".ihi" 0;
	setAttr ".ro" yes;
createNode materialInfo -n "idle_v1_materialInfo3";
createNode shadingEngine -n "idle_v1_business05_f_highpoly_hairSG1";
	setAttr ".ihi" 0;
	setAttr ".ro" yes;
createNode materialInfo -n "idle_v1_materialInfo4";
createNode phong -n "idle_v1_business05_f_highpolyPhong1";
	setAttr ".sc" -type "float3" 0 0 0 ;
createNode shadingEngine -n "idle_v1_business05_f_highpolySG2";
	setAttr ".ihi" 0;
	setAttr ".ro" yes;
createNode materialInfo -n "idle_v1_materialInfo5";
createNode shadingEngine -n "idle_v1_business05_f_highpolySG3";
	setAttr ".ihi" 0;
	setAttr ".ro" yes;
createNode materialInfo -n "idle_v1_materialInfo6";
createNode file -n "idle_v1_Map2";
	setAttr ".ftn" -type "string" "C:\\Users\\Kuan\\Desktop\\iPi Record 20150227\\business05_f\\business05_f_35.tga";
createNode place2dTexture -n "idle_v1_place2dTexture2";
createNode phong -n "idle_v1_business05_f_highpoly_hairPhong1";
	setAttr ".c" -type "float3" 0 0 0 ;
	setAttr ".sc" -type "float3" 0 0 0 ;
createNode shadingEngine -n "idle_v1_business05_f_highpoly_hairSG2";
	setAttr ".ihi" 0;
	setAttr ".ro" yes;
createNode materialInfo -n "idle_v1_materialInfo7";
createNode shadingEngine -n "idle_v1_business05_f_highpoly_hairSG3";
	setAttr ".ihi" 0;
	setAttr ".ro" yes;
createNode materialInfo -n "idle_v1_materialInfo8";
createNode dagPose -n "idle_v1_bindPose1";
	setAttr -s 41 ".wm";
	setAttr -s 41 ".xm";
	setAttr ".xm[0]" -type "matrix" "xform" 0.99999999999999989 1 1 0.012547960056150888
		 -0.009816171243840946 0.034613613241094902 0 -6.7139604917915256e-006 99.443700000000007
		 -2.2495999999899592 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1 0.50000074612769874 0.50000074612769851 0.49999925387118799 0.49999925387118815 1
		 1 1 no;
	setAttr ".xm[1]" -type "matrix" "xform" 1 1 1 0.016054937566026255 3.1243109519712209
		 0.0041203081482766527 0 -11.0456 2.2585000000000002 -8.5201999999999991 0 0
		 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1 0 0 0 1 1 1 1 no;
	setAttr ".xm[2]" -type "matrix" "xform" 1 1 1 -0.0042303254890614025 0.0019100999071225793
		 -0.01367796285364703 0 13.398400000000024 -0.01070000000000082 -3.3306690738754696e-016 0
		 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1 0 0 0 1 1 1 1 no;
	setAttr ".xm[3]" -type "matrix" "xform" 1 1 1 -0.014223988819492692 0.0061006035969784706
		 -0.045628514516559943 0 13.398299999999992 -0.010699999999999932 1.1102230246251563e-016 0
		 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1 0 0 0 1 1 1 1 no;
	setAttr ".xm[4]" -type "matrix" "xform" 1 1 1 0 -1.0471975511965976e-007 0.020036309999724822 0 13.398500000000013
		 -0.0079000000000002402 -2.7755575615628914e-017 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1 
		0 0 0 1 1 1 1 no;
	setAttr ".xm[5]" -type "matrix" "xform" 1 1 1 0.00066562508040709438 1.5943567208302265
		 3.1200414191805415 0 -0.82679999999999998 -0.66120000000000001 -3.9165000000000001 0
		 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1 0 0 0 1 1 1 1 no;
	setAttr ".xm[6]" -type "matrix" "xform" 1 1 1 -0.0023077941604372825 -1.5863924425080176
		 3.1185051649588265 0 -0.82679999999999998 -0.66120000000000001 3.9165000000000001 0
		 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1 0 0 0 1 1 1 1 no;
	setAttr ".xm[7]" -type "matrix" "xform" 1 1 1 0.041260689098729222 -0.087319702749340533
		 -0.083702822993913684 0 10.6745 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1 0 0 0 1 1
		 1 1 no;
	setAttr ".xm[8]" -type "matrix" "xform" 1 1 1 0.00049236404888397067 0.0066219535815420895
		 -0.030625446218014385 0 25.337499999999999 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 
		0 0 0 1 0 0 0 1 1 1 1 no;
	setAttr ".xm[9]" -type "matrix" "xform" 1 1 1 1.5731232870201179 -0.10996275909923578
		 0.04477350726006369 0 24.988099999999999 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 
		0 0 0 1 0 0 0 1 1 1 1 no;
	setAttr ".xm[10]" -type "matrix" "xform" 1 1 1 -1.4299513977516076 -0.40690922085590414
		 0.31932383424197069 0 2.3036000000000003 1.3940999999999999 2.9222000000000001 0
		 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1 0 0 0 1 1 1 1 no;
	setAttr ".xm[11]" -type "matrix" "xform" 1 1 1 -0.010673629745774932 -3.1224140517030023
		 0.033537593534992381 0 -11.0456 2.2584 8.5201999999999991 0 0 0 0 0 0 0 0 0 0
		 0 0 0 0 0 0 0 0 1 0 0 0 1 1 1 1 no;
	setAttr ".xm[12]" -type "matrix" "xform" 1 1 1 0 -2.2689280275926285e-007 0.080675942264553213 0 9.9828999999999724
		 -2.2204460492503131e-016 -1.3877787807814457e-017 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 
		0 0 0 1 0 0 0 1 1 1 1 no;
	setAttr ".xm[13]" -type "matrix" "xform" 1 1 1 -0.00097187946529535946 -0.017096550311845326
		 -0.0076016872222077758 0 36.152700000000003 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0
		 0 0 0 0 1 0 0 0 1 1 1 1 no;
	setAttr ".xm[14]" -type "matrix" "xform" 1 1 1 1.7453292519943295e-008 5.2359877559829887e-008
		 0.42000561305750889 0 4.0805999999999996 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 
		0 0 0 1 0 0 0 1 1 1 1 no;
	setAttr ".xm[15]" -type "matrix" "xform" 1 1 1 0.00075684457683482102 0.066784238429399623
		 0.32305451042469363 0 7.2344999999999997 -0.59970000000000001 -0.93320000000000003 0
		 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1 0 0 0 1 1 1 1 no;
	setAttr ".xm[16]" -type "matrix" "xform" 1 1 1 -0.0030915715172276366 0.09707444505105374
		 0.28999008550370198 0 7.2291999999999996 -0.41870000000000002 -2.3639000000000001 0
		 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1 0 0 0 1 1 1 1 no;
	setAttr ".xm[17]" -type "matrix" "xform" 1 1 1 -0.012795078559540507 -0.092755784896626448
		 0.39526005567005307 0 7.3262999999999998 -0.21099999999999999 2.2784 0 0 0 0
		 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1 0 0 0 1 1 1 1 no;
	setAttr ".xm[18]" -type "matrix" "xform" 1 1 1 -3.4906585039886589e-008 6.9813170079773178e-008
		 0.39492748318108545 0 3.5597999999999996 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 
		0 0 0 1 0 0 0 1 1 1 1 no;
	setAttr ".xm[19]" -type "matrix" "xform" 1 1 1 0.00076122535325732685 -0.00023399629281487978
		 0.29821767231735841 0 7.3708999999999998 -0.48159999999999997 0.6341 0 0 0 0
		 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1 0 0 0 1 1 1 1 no;
	setAttr ".xm[20]" -type "matrix" "xform" 1 1 1 0 1.7453292519943295e-008 0.52266310458630472 0 3.8208000000000002
		 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1 0 0 0 1 1 1 1 no;
	setAttr ".xm[21]" -type "matrix" "xform" 1 1 1 5.2359877559829887e-008 5.2359877559829887e-008
		 0.48841794697304641 0 3.7241 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1 0 0 0 1 1
		 1 1 no;
	setAttr ".xm[22]" -type "matrix" "xform" 1 1 1 -3.4906585039886589e-008 -5.2359877559829887e-008
		 0.51390116776885775 0 3.4791000000000003 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 
		0 0 0 1 0 0 0 1 1 1 1 no;
	setAttr ".xm[23]" -type "matrix" "xform" 1 1 1 0.00069162493647426863 0.017079730970413178
		 -0.025504458682417824 0 36.152700000000003 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 
		0 0 0 1 0 0 0 1 1 1 1 no;
	setAttr ".xm[24]" -type "matrix" "xform" 1 1 1 0.12767313933182581 0.001246596194693958
		 -0.001316447039224955 0 43.254300000000001 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 
		0 0 0 1 0 0 0 1 1 1 1 no;
	setAttr ".xm[25]" -type "matrix" "xform" 1 1 1 3.4906585039886589e-008 -3.4906585039886589e-008
		 1.5707960824488014 0 9.4714999999999989 11.2377 0 0 0 0 0 0 0 0 0 0 0 0 0 0
		 0 0 0 0 0 1 0 0 0 1 1 1 1 no;
	setAttr ".xm[26]" -type "matrix" "xform" 1 1 1 -0.13786208561377394 0.0039075463545194533
		 0.0070821192354181411 0 43.254300000000001 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 
		0 0 0 1 0 0 0 1 1 1 1 no;
	setAttr ".xm[27]" -type "matrix" "xform" 1 1 1 -8.7266462599716483e-008 3.4906585039886589e-008
		 1.5707959777290459 0 9.4714999999999989 11.2377 0 0 0 0 0 0 0 0 0 0 0 0 0 0
		 0 0 0 0 0 1 0 0 0 1 1 1 1 no;
	setAttr ".xm[28]" -type "matrix" "xform" 1 1 1 -0.13193210097764352 0.13670878337828657
		 0.074681007886865422 0 10.6745 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1 0 0 0 1 1
		 1 1 no;
	setAttr ".xm[29]" -type "matrix" "xform" 1 1 1 0.00082246315912159443 -0.011533588033988412
		 -0.1645484574746412 0 25.337399999999999 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 
		0 0 0 1 0 0 0 1 1 1 1 no;
	setAttr ".xm[30]" -type "matrix" "xform" 1 1 1 -1.573123393157245 0.1099626782710364
		 0.044773484763906843 0 24.988099999999999 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 
		0 0 0 1 0 0 0 1 1 1 1 no;
	setAttr ".xm[31]" -type "matrix" "xform" 1 1 1 1.4299513977516076 0.40690922085590414
		 0.31932385169526323 0 2.3036000000000003 1.3940999999999999 -2.9222000000000001 0
		 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1 0 0 0 1 1 1 1 no;
	setAttr ".xm[32]" -type "matrix" "xform" 1 1 1 1.7453292519943295e-008 -1.7453292519943295e-008
		 0.42000566541738638 0 4.0805999999999996 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 
		0 0 0 1 0 0 0 1 1 1 1 no;
	setAttr ".xm[33]" -type "matrix" "xform" 1 1 1 -0.00075680967024978113 -0.066784290789277181
		 0.32305463259774125 0 7.2344999999999997 -0.59970000000000001 0.93320000000000003 0
		 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1 0 0 0 1 1 1 1 no;
	setAttr ".xm[34]" -type "matrix" "xform" 1 1 1 0.0030915366106425961 -0.097074410144468687
		 0.2899902600366272 0 7.2291999999999996 -0.41870000000000002 2.3639000000000001 0
		 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1 0 0 0 1 1 1 1 no;
	setAttr ".xm[35]" -type "matrix" "xform" 1 1 1 0.012795148372710587 0.092755749990041395
		 0.39526023020297818 0 7.3262999999999998 -0.21099999999999999 -2.2784 0 0 0 0
		 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1 0 0 0 1 1 1 1 no;
	setAttr ".xm[36]" -type "matrix" "xform" 1 1 1 -3.4906585039886589e-008 5.2359877559829887e-008
		 0.39492746572779286 0 3.5597999999999996 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 
		0 0 0 1 0 0 0 1 1 1 1 no;
	setAttr ".xm[37]" -type "matrix" "xform" 1 1 1 -0.00076113808679472718 0.00023404865269243958
		 0.29821775958382096 0 7.3708999999999998 -0.48159999999999997 -0.6341 0 0 0 0
		 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1 0 0 0 1 1 1 1 no;
	setAttr ".xm[38]" -type "matrix" "xform" 1 1 1 -6.9813170079773178e-008 -6.9813170079773178e-008
		 0.52266310458630472 0 3.8208000000000002 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 
		0 0 0 1 0 0 0 1 1 1 1 no;
	setAttr ".xm[39]" -type "matrix" "xform" 1 1 1 3.4906585039886589e-008 0 0.48841796442633889 0 3.7241
		 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1 0 0 0 1 1 1 1 no;
	setAttr ".xm[40]" -type "matrix" "xform" 1 1 1 3.4906585039886589e-008 -5.2359877559829887e-008
		 0.51390109795568761 0 3.4791000000000003 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 
		0 0 0 1 0 0 0 1 1 1 1 no;
	setAttr -s 41 ".m";
	setAttr -s 41 ".p";
	setAttr ".bp" yes;
createNode phong -n "stand_v1_business05_f_highpolyPhong";
	setAttr ".sc" -type "float3" 0 0 0 ;
createNode shadingEngine -n "stand_v1_business05_f_highpolySG";
	setAttr ".ihi" 0;
	setAttr ".ro" yes;
createNode materialInfo -n "stand_v1_materialInfo1";
createNode shadingEngine -n "stand_v1_business05_f_highpolySG1";
	setAttr ".ihi" 0;
	setAttr ".ro" yes;
createNode materialInfo -n "stand_v1_materialInfo2";
createNode file -n "stand_v1_Map1";
	setAttr ".ftn" -type "string" "C:\\Users\\Kuan\\Desktop\\iPi Record 20150227\\business05_f\\business05_f_35.tga";
createNode place2dTexture -n "stand_v1_place2dTexture1";
createNode phong -n "stand_v1_business05_f_highpoly_hairPhong";
	setAttr ".c" -type "float3" 0 0 0 ;
	setAttr ".sc" -type "float3" 0 0 0 ;
createNode shadingEngine -n "stand_v1_business05_f_highpoly_hairSG";
	setAttr ".ihi" 0;
	setAttr ".ro" yes;
createNode materialInfo -n "stand_v1_materialInfo3";
createNode shadingEngine -n "stand_v1_business05_f_highpoly_hairSG1";
	setAttr ".ihi" 0;
	setAttr ".ro" yes;
createNode materialInfo -n "stand_v1_materialInfo4";
createNode phong -n "sit_idle_v1_business05_f_highpolyPhong";
	setAttr ".sc" -type "float3" 0 0 0 ;
createNode shadingEngine -n "sit_idle_v1_business05_f_highpolySG";
	setAttr ".ihi" 0;
	setAttr ".ro" yes;
createNode materialInfo -n "sit_idle_v1_materialInfo1";
createNode shadingEngine -n "sit_idle_v1_business05_f_highpolySG1";
	setAttr ".ihi" 0;
	setAttr ".ro" yes;
createNode materialInfo -n "sit_idle_v1_materialInfo2";
createNode file -n "sit_idle_v1_Map1";
	setAttr ".ftn" -type "string" "C:\\Users\\Kuan\\Desktop\\iPi Record 20150227\\business05_f\\business05_f_35.tga";
createNode place2dTexture -n "sit_idle_v1_place2dTexture1";
createNode phong -n "sit_idle_v1_business05_f_highpoly_hairPhong";
	setAttr ".c" -type "float3" 0 0 0 ;
	setAttr ".sc" -type "float3" 0 0 0 ;
createNode shadingEngine -n "sit_idle_v1_business05_f_highpoly_hairSG";
	setAttr ".ihi" 0;
	setAttr ".ro" yes;
createNode materialInfo -n "sit_idle_v1_materialInfo3";
createNode shadingEngine -n "sit_idle_v1_business05_f_highpoly_hairSG1";
	setAttr ".ihi" 0;
	setAttr ".ro" yes;
createNode materialInfo -n "sit_idle_v1_materialInfo4";
createNode phong -n "sit_talk_v1_business05_f_highpolyPhong";
	setAttr ".sc" -type "float3" 0 0 0 ;
createNode shadingEngine -n "sit_talk_v1_business05_f_highpolySG";
	setAttr ".ihi" 0;
	setAttr ".ro" yes;
createNode materialInfo -n "sit_talk_v1_materialInfo1";
createNode shadingEngine -n "sit_talk_v1_business05_f_highpolySG1";
	setAttr ".ihi" 0;
	setAttr ".ro" yes;
createNode materialInfo -n "sit_talk_v1_materialInfo2";
createNode file -n "sit_talk_v1_Map1";
	setAttr ".ftn" -type "string" "C:\\Users\\Kuan\\Desktop\\iPi Record 20150227\\business05_f\\business05_f_35.tga";
createNode place2dTexture -n "sit_talk_v1_place2dTexture1";
createNode phong -n "sit_talk_v1_business05_f_highpoly_hairPhong";
	setAttr ".c" -type "float3" 0 0 0 ;
	setAttr ".sc" -type "float3" 0 0 0 ;
createNode shadingEngine -n "sit_talk_v1_business05_f_highpoly_hairSG";
	setAttr ".ihi" 0;
	setAttr ".ro" yes;
createNode materialInfo -n "sit_talk_v1_materialInfo3";
createNode shadingEngine -n "sit_talk_v1_business05_f_highpoly_hairSG1";
	setAttr ".ihi" 0;
	setAttr ".ro" yes;
createNode materialInfo -n "sit_talk_v1_materialInfo4";
createNode mentalrayItemsList -s -n "mentalrayItemsList";
createNode mentalrayGlobals -s -n "mentalrayGlobals";
createNode mentalrayOptions -s -n "miDefaultOptions";
	addAttr -ci true -m -sn "stringOptions" -ln "stringOptions" -at "compound" -nc 
		3;
	addAttr -ci true -sn "name" -ln "name" -dt "string" -p "stringOptions";
	addAttr -ci true -sn "value" -ln "value" -dt "string" -p "stringOptions";
	addAttr -ci true -sn "type" -ln "type" -dt "string" -p "stringOptions";
	addAttr -ci true -sn "miSamplesQualityR" -ln "miSamplesQualityR" -dv 0.25 -min 0.01 
		-max 9999999.9000000004 -smn 0.1 -smx 2 -at "double";
	addAttr -ci true -sn "miSamplesQualityG" -ln "miSamplesQualityG" -dv 0.25 -min 0.01 
		-max 9999999.9000000004 -smn 0.1 -smx 2 -at "double";
	addAttr -ci true -sn "miSamplesQualityB" -ln "miSamplesQualityB" -dv 0.25 -min 0.01 
		-max 9999999.9000000004 -smn 0.1 -smx 2 -at "double";
	addAttr -ci true -sn "miSamplesQualityA" -ln "miSamplesQualityA" -dv 0.25 -min 0.01 
		-max 9999999.9000000004 -smn 0.1 -smx 2 -at "double";
	addAttr -ci true -sn "miSamplesMin" -ln "miSamplesMin" -dv 1 -min 0.1 -at "double";
	addAttr -ci true -sn "miSamplesMax" -ln "miSamplesMax" -dv 100 -min 0.1 -at "double";
	addAttr -ci true -sn "miSamplesErrorCutoffR" -ln "miSamplesErrorCutoffR" -min 0 
		-max 1 -at "double";
	addAttr -ci true -sn "miSamplesErrorCutoffG" -ln "miSamplesErrorCutoffG" -min 0 
		-max 1 -at "double";
	addAttr -ci true -sn "miSamplesErrorCutoffB" -ln "miSamplesErrorCutoffB" -min 0 
		-max 1 -at "double";
	addAttr -ci true -sn "miSamplesErrorCutoffA" -ln "miSamplesErrorCutoffA" -min 0 
		-max 1 -at "double";
	addAttr -ci true -sn "miSamplesPerObject" -ln "miSamplesPerObject" -min 0 -max 1 
		-at "bool";
	addAttr -ci true -sn "miRastShadingSamples" -ln "miRastShadingSamples" -dv 1 -min 
		0.25 -at "double";
	addAttr -ci true -sn "miRastSamples" -ln "miRastSamples" -dv 3 -min 1 -at "long";
	addAttr -ci true -sn "miContrastAsColor" -ln "miContrastAsColor" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "miProgMaxTime" -ln "miProgMaxTime" -min 0 -at "long";
	addAttr -ci true -sn "miProgSubsampleSize" -ln "miProgSubsampleSize" -dv 4 -min 
		1 -at "long";
	addAttr -ci true -sn "miTraceCameraMotionVectors" -ln "miTraceCameraMotionVectors" 
		-min 0 -max 1 -at "bool";
	addAttr -ci true -sn "miTraceCameraClip" -ln "miTraceCameraClip" -min 0 -max 1 -at "bool";
	setAttr -s 45 ".stringOptions";
	setAttr ".stringOptions[0].name" -type "string" "rast motion factor";
	setAttr ".stringOptions[0].value" -type "string" "1.0";
	setAttr ".stringOptions[0].type" -type "string" "scalar";
	setAttr ".stringOptions[1].name" -type "string" "rast transparency depth";
	setAttr ".stringOptions[1].value" -type "string" "8";
	setAttr ".stringOptions[1].type" -type "string" "integer";
	setAttr ".stringOptions[2].name" -type "string" "rast useopacity";
	setAttr ".stringOptions[2].value" -type "string" "true";
	setAttr ".stringOptions[2].type" -type "string" "boolean";
	setAttr ".stringOptions[3].name" -type "string" "importon";
	setAttr ".stringOptions[3].value" -type "string" "false";
	setAttr ".stringOptions[3].type" -type "string" "boolean";
	setAttr ".stringOptions[4].name" -type "string" "importon density";
	setAttr ".stringOptions[4].value" -type "string" "1.0";
	setAttr ".stringOptions[4].type" -type "string" "scalar";
	setAttr ".stringOptions[5].name" -type "string" "importon merge";
	setAttr ".stringOptions[5].value" -type "string" "0.0";
	setAttr ".stringOptions[5].type" -type "string" "scalar";
	setAttr ".stringOptions[6].name" -type "string" "importon trace depth";
	setAttr ".stringOptions[6].value" -type "string" "0";
	setAttr ".stringOptions[6].type" -type "string" "integer";
	setAttr ".stringOptions[7].name" -type "string" "importon traverse";
	setAttr ".stringOptions[7].value" -type "string" "true";
	setAttr ".stringOptions[7].type" -type "string" "boolean";
	setAttr ".stringOptions[8].name" -type "string" "shadowmap pixel samples";
	setAttr ".stringOptions[8].value" -type "string" "3";
	setAttr ".stringOptions[8].type" -type "string" "integer";
	setAttr ".stringOptions[9].name" -type "string" "ambient occlusion";
	setAttr ".stringOptions[9].value" -type "string" "false";
	setAttr ".stringOptions[9].type" -type "string" "boolean";
	setAttr ".stringOptions[10].name" -type "string" "ambient occlusion rays";
	setAttr ".stringOptions[10].value" -type "string" "256";
	setAttr ".stringOptions[10].type" -type "string" "integer";
	setAttr ".stringOptions[11].name" -type "string" "ambient occlusion cache";
	setAttr ".stringOptions[11].value" -type "string" "false";
	setAttr ".stringOptions[11].type" -type "string" "boolean";
	setAttr ".stringOptions[12].name" -type "string" "ambient occlusion cache density";
	setAttr ".stringOptions[12].value" -type "string" "1.0";
	setAttr ".stringOptions[12].type" -type "string" "scalar";
	setAttr ".stringOptions[13].name" -type "string" "ambient occlusion cache points";
	setAttr ".stringOptions[13].value" -type "string" "64";
	setAttr ".stringOptions[13].type" -type "string" "integer";
	setAttr ".stringOptions[14].name" -type "string" "irradiance particles";
	setAttr ".stringOptions[14].value" -type "string" "false";
	setAttr ".stringOptions[14].type" -type "string" "boolean";
	setAttr ".stringOptions[15].name" -type "string" "irradiance particles rays";
	setAttr ".stringOptions[15].value" -type "string" "256";
	setAttr ".stringOptions[15].type" -type "string" "integer";
	setAttr ".stringOptions[16].name" -type "string" "irradiance particles interpolate";
	setAttr ".stringOptions[16].value" -type "string" "1";
	setAttr ".stringOptions[16].type" -type "string" "integer";
	setAttr ".stringOptions[17].name" -type "string" "irradiance particles interppoints";
	setAttr ".stringOptions[17].value" -type "string" "64";
	setAttr ".stringOptions[17].type" -type "string" "integer";
	setAttr ".stringOptions[18].name" -type "string" "irradiance particles indirect passes";
	setAttr ".stringOptions[18].value" -type "string" "0";
	setAttr ".stringOptions[18].type" -type "string" "integer";
	setAttr ".stringOptions[19].name" -type "string" "irradiance particles scale";
	setAttr ".stringOptions[19].value" -type "string" "1.0";
	setAttr ".stringOptions[19].type" -type "string" "scalar";
	setAttr ".stringOptions[20].name" -type "string" "irradiance particles env";
	setAttr ".stringOptions[20].value" -type "string" "true";
	setAttr ".stringOptions[20].type" -type "string" "boolean";
	setAttr ".stringOptions[21].name" -type "string" "irradiance particles env rays";
	setAttr ".stringOptions[21].value" -type "string" "256";
	setAttr ".stringOptions[21].type" -type "string" "integer";
	setAttr ".stringOptions[22].name" -type "string" "irradiance particles env scale";
	setAttr ".stringOptions[22].value" -type "string" "1";
	setAttr ".stringOptions[22].type" -type "string" "integer";
	setAttr ".stringOptions[23].name" -type "string" "irradiance particles rebuild";
	setAttr ".stringOptions[23].value" -type "string" "true";
	setAttr ".stringOptions[23].type" -type "string" "boolean";
	setAttr ".stringOptions[24].name" -type "string" "irradiance particles file";
	setAttr ".stringOptions[24].value" -type "string" "";
	setAttr ".stringOptions[24].type" -type "string" "string";
	setAttr ".stringOptions[25].name" -type "string" "geom displace motion factor";
	setAttr ".stringOptions[25].value" -type "string" "1.0";
	setAttr ".stringOptions[25].type" -type "string" "scalar";
	setAttr ".stringOptions[26].name" -type "string" "contrast all buffers";
	setAttr ".stringOptions[26].value" -type "string" "true";
	setAttr ".stringOptions[26].type" -type "string" "boolean";
	setAttr ".stringOptions[27].name" -type "string" "finalgather normal tolerance";
	setAttr ".stringOptions[27].value" -type "string" "25.842";
	setAttr ".stringOptions[27].type" -type "string" "scalar";
	setAttr ".stringOptions[28].name" -type "string" "trace camera clip";
	setAttr ".stringOptions[28].value" -type "string" "false";
	setAttr ".stringOptions[28].type" -type "string" "boolean";
	setAttr ".stringOptions[29].name" -type "string" "unified sampling";
	setAttr ".stringOptions[29].value" -type "string" "true";
	setAttr ".stringOptions[29].type" -type "string" "boolean";
	setAttr ".stringOptions[30].name" -type "string" "samples quality";
	setAttr ".stringOptions[30].value" -type "string" "0.5 0.5 0.5 0.5";
	setAttr ".stringOptions[30].type" -type "string" "color";
	setAttr ".stringOptions[31].name" -type "string" "samples min";
	setAttr ".stringOptions[31].value" -type "string" "1.0";
	setAttr ".stringOptions[31].type" -type "string" "scalar";
	setAttr ".stringOptions[32].name" -type "string" "samples max";
	setAttr ".stringOptions[32].value" -type "string" "100.0";
	setAttr ".stringOptions[32].type" -type "string" "scalar";
	setAttr ".stringOptions[33].name" -type "string" "samples error cutoff";
	setAttr ".stringOptions[33].value" -type "string" "0.0 0.0 0.0 0.0";
	setAttr ".stringOptions[33].type" -type "string" "color";
	setAttr ".stringOptions[34].name" -type "string" "samples per object";
	setAttr ".stringOptions[34].value" -type "string" "false";
	setAttr ".stringOptions[34].type" -type "string" "boolean";
	setAttr ".stringOptions[35].name" -type "string" "progressive";
	setAttr ".stringOptions[35].value" -type "string" "false";
	setAttr ".stringOptions[35].type" -type "string" "boolean";
	setAttr ".stringOptions[36].name" -type "string" "progressive max time";
	setAttr ".stringOptions[36].value" -type "string" "0";
	setAttr ".stringOptions[36].type" -type "string" "integer";
	setAttr ".stringOptions[37].name" -type "string" "progressive subsampling size";
	setAttr ".stringOptions[37].value" -type "string" "1";
	setAttr ".stringOptions[37].type" -type "string" "integer";
	setAttr ".stringOptions[38].name" -type "string" "iray";
	setAttr ".stringOptions[38].value" -type "string" "false";
	setAttr ".stringOptions[38].type" -type "string" "boolean";
	setAttr ".stringOptions[39].name" -type "string" "light relative scale";
	setAttr ".stringOptions[39].value" -type "string" "0.31831";
	setAttr ".stringOptions[39].type" -type "string" "scalar";
	setAttr ".stringOptions[40].name" -type "string" "trace camera motion vectors";
	setAttr ".stringOptions[40].value" -type "string" "false";
	setAttr ".stringOptions[40].type" -type "string" "boolean";
	setAttr ".stringOptions[41].name" -type "string" "ray differentials";
	setAttr ".stringOptions[41].value" -type "string" "true";
	setAttr ".stringOptions[41].type" -type "string" "boolean";
	setAttr ".stringOptions[42].name" -type "string" "environment lighting mode";
	setAttr ".stringOptions[42].value" -type "string" "off";
	setAttr ".stringOptions[42].type" -type "string" "string";
	setAttr ".stringOptions[43].name" -type "string" "environment lighting quality";
	setAttr ".stringOptions[43].value" -type "string" "0.167";
	setAttr ".stringOptions[43].type" -type "string" "scalar";
	setAttr ".stringOptions[44].name" -type "string" "environment lighting shadow";
	setAttr ".stringOptions[44].value" -type "string" "transparent";
	setAttr ".stringOptions[44].type" -type "string" "string";
createNode mentalrayFramebuffer -s -n "miDefaultFramebuffer";
createNode phong -n "Animations_v3_business05_f_highpolyPhong";
	setAttr ".sc" -type "float3" 0 0 0 ;
createNode shadingEngine -n "Animations_v3_business05_f_highpolySG";
	setAttr ".ihi" 0;
	setAttr ".ro" yes;
createNode materialInfo -n "Animations_v3_materialInfo1";
createNode shadingEngine -n "Animations_v3_business05_f_highpolySG1";
	setAttr ".ihi" 0;
	setAttr ".ro" yes;
createNode materialInfo -n "Animations_v3_materialInfo2";
createNode file -n "Animations_v3_Map1";
	setAttr ".ftn" -type "string" "C:\\Users\\Kuan\\Desktop\\iPi Record 20150227\\business05_f\\business05_f_35.tga";
createNode place2dTexture -n "Animations_v3_place2dTexture1";
createNode phong -n "Animations_v3_business05_f_highpoly_hairPhong";
	setAttr ".c" -type "float3" 0 0 0 ;
	setAttr ".sc" -type "float3" 0 0 0 ;
createNode shadingEngine -n "Animations_v3_business05_f_highpoly_hairSG";
	setAttr ".ihi" 0;
	setAttr ".ro" yes;
createNode materialInfo -n "Animations_v3_materialInfo3";
createNode shadingEngine -n "Animations_v3_business05_f_highpoly_hairSG1";
	setAttr ".ihi" 0;
	setAttr ".ro" yes;
createNode materialInfo -n "Animations_v3_materialInfo4";
createNode phong -n "Jessica_Take_2_2_business05_f_highpolyPhong";
	setAttr ".sc" -type "float3" 0 0 0 ;
createNode shadingEngine -n "Jessica_Take_2_2_business05_f_highpolySG";
	setAttr ".ihi" 0;
	setAttr ".ro" yes;
createNode materialInfo -n "Jessica_Take_2_2_materialInfo1";
createNode shadingEngine -n "Jessica_Take_2_2_business05_f_highpolySG1";
	setAttr ".ihi" 0;
	setAttr ".ro" yes;
createNode materialInfo -n "Jessica_Take_2_2_materialInfo2";
createNode file -n "Jessica_Take_2_2_Map1";
	setAttr ".ftn" -type "string" "C:\\Users\\Kuan\\Desktop\\iPi Record 20150227\\business05_f\\business05_f_35.tga";
createNode place2dTexture -n "Jessica_Take_2_2_place2dTexture1";
createNode phong -n "Jessica_Take_2_2_business05_f_highpoly_hairPhong";
	setAttr ".c" -type "float3" 0 0 0 ;
	setAttr ".sc" -type "float3" 0 0 0 ;
createNode shadingEngine -n "Jessica_Take_2_2_business05_f_highpoly_hairSG";
	setAttr ".ihi" 0;
	setAttr ".ro" yes;
createNode materialInfo -n "Jessica_Take_2_2_materialInfo3";
createNode shadingEngine -n "Jessica_Take_2_2_business05_f_highpoly_hairSG1";
	setAttr ".ihi" 0;
	setAttr ".ro" yes;
createNode materialInfo -n "Jessica_Take_2_2_materialInfo4";
createNode phong -n "Jessica_Take_2_2_business05_f_highpolyPhong1";
	setAttr ".sc" -type "float3" 0 0 0 ;
createNode shadingEngine -n "Jessica_Take_2_2_business05_f_highpolySG2";
	setAttr ".ihi" 0;
	setAttr ".ro" yes;
createNode materialInfo -n "Jessica_Take_2_2_materialInfo5";
createNode shadingEngine -n "Jessica_Take_2_2_business05_f_highpolySG3";
	setAttr ".ihi" 0;
	setAttr ".ro" yes;
createNode materialInfo -n "Jessica_Take_2_2_materialInfo6";
createNode file -n "Jessica_Take_2_2_Map2";
	setAttr ".ftn" -type "string" "C:\\Users\\Kuan\\Desktop\\iPi Record 20150227\\business05_f\\business05_f_35.tga";
createNode place2dTexture -n "Jessica_Take_2_2_place2dTexture2";
createNode phong -n "Jessica_Take_2_2_business05_f_highpoly_hairPhong1";
	setAttr ".c" -type "float3" 0 0 0 ;
	setAttr ".sc" -type "float3" 0 0 0 ;
createNode shadingEngine -n "Jessica_Take_2_2_business05_f_highpoly_hairSG2";
	setAttr ".ihi" 0;
	setAttr ".ro" yes;
createNode materialInfo -n "Jessica_Take_2_2_materialInfo7";
createNode shadingEngine -n "Jessica_Take_2_2_business05_f_highpoly_hairSG3";
	setAttr ".ihi" 0;
	setAttr ".ro" yes;
createNode materialInfo -n "Jessica_Take_2_2_materialInfo8";
createNode phong -n "Jessica_Take_2_2_business05_f_highpolyPhong2";
	setAttr ".sc" -type "float3" 0 0 0 ;
createNode shadingEngine -n "Jessica_Take_2_2_business05_f_highpolySG4";
	setAttr ".ihi" 0;
	setAttr ".ro" yes;
createNode materialInfo -n "Jessica_Take_2_2_materialInfo9";
createNode shadingEngine -n "Jessica_Take_2_2_business05_f_highpolySG5";
	setAttr ".ihi" 0;
	setAttr ".ro" yes;
createNode materialInfo -n "Jessica_Take_2_2_materialInfo10";
createNode file -n "Jessica_Take_2_2_Map3";
	setAttr ".ftn" -type "string" "C:\\Users\\Kuan\\Desktop\\iPi Record 20150227\\business05_f\\business05_f_35.tga";
createNode place2dTexture -n "Jessica_Take_2_2_place2dTexture3";
createNode phong -n "Jessica_Take_2_2_business05_f_highpoly_hairPhong2";
	setAttr ".c" -type "float3" 0 0 0 ;
	setAttr ".sc" -type "float3" 0 0 0 ;
createNode shadingEngine -n "Jessica_Take_2_2_business05_f_highpoly_hairSG4";
	setAttr ".ihi" 0;
	setAttr ".ro" yes;
createNode materialInfo -n "Jessica_Take_2_2_materialInfo11";
createNode shadingEngine -n "Jessica_Take_2_2_business05_f_highpoly_hairSG5";
	setAttr ".ihi" 0;
	setAttr ".ro" yes;
createNode materialInfo -n "Jessica_Take_2_2_materialInfo12";
createNode phong -n "Jessica_Take_2_2_business05_f_highpolyPhong3";
	setAttr ".sc" -type "float3" 0 0 0 ;
createNode shadingEngine -n "Jessica_Take_2_2_business05_f_highpolySG6";
	setAttr ".ihi" 0;
	setAttr ".ro" yes;
createNode materialInfo -n "Jessica_Take_2_2_materialInfo13";
createNode shadingEngine -n "Jessica_Take_2_2_business05_f_highpolySG7";
	setAttr ".ihi" 0;
	setAttr ".ro" yes;
createNode materialInfo -n "Jessica_Take_2_2_materialInfo14";
createNode file -n "Jessica_Take_2_2_Map4";
	setAttr ".ftn" -type "string" "C:\\Users\\Kuan\\Desktop\\iPi Record 20150227\\business05_f\\business05_f_35.tga";
createNode place2dTexture -n "Jessica_Take_2_2_place2dTexture4";
createNode phong -n "Jessica_Take_2_2_business05_f_highpoly_hairPhong3";
	setAttr ".c" -type "float3" 0 0 0 ;
	setAttr ".sc" -type "float3" 0 0 0 ;
createNode shadingEngine -n "Jessica_Take_2_2_business05_f_highpoly_hairSG6";
	setAttr ".ihi" 0;
	setAttr ".ro" yes;
createNode materialInfo -n "Jessica_Take_2_2_materialInfo15";
createNode shadingEngine -n "Jessica_Take_2_2_business05_f_highpoly_hairSG7";
	setAttr ".ihi" 0;
	setAttr ".ro" yes;
createNode materialInfo -n "Jessica_Take_2_2_materialInfo16";
createNode phong -n "Jessica_Take_2_2_business05_f_highpolyPhong4";
	setAttr ".sc" -type "float3" 0 0 0 ;
createNode shadingEngine -n "Jessica_Take_2_2_business05_f_highpolySG8";
	setAttr ".ihi" 0;
	setAttr ".ro" yes;
createNode materialInfo -n "Jessica_Take_2_2_materialInfo17";
createNode shadingEngine -n "Jessica_Take_2_2_business05_f_highpolySG9";
	setAttr ".ihi" 0;
	setAttr ".ro" yes;
createNode materialInfo -n "Jessica_Take_2_2_materialInfo18";
createNode file -n "Jessica_Take_2_2_Map5";
	setAttr ".ftn" -type "string" "C:\\Users\\Kuan\\Desktop\\iPi Record 20150227\\business05_f\\business05_f_35.tga";
createNode place2dTexture -n "Jessica_Take_2_2_place2dTexture5";
createNode phong -n "Jessica_Take_2_2_business05_f_highpoly_hairPhong4";
	setAttr ".c" -type "float3" 0 0 0 ;
	setAttr ".sc" -type "float3" 0 0 0 ;
createNode shadingEngine -n "Jessica_Take_2_2_business05_f_highpoly_hairSG8";
	setAttr ".ihi" 0;
	setAttr ".ro" yes;
createNode materialInfo -n "Jessica_Take_2_2_materialInfo19";
createNode shadingEngine -n "Jessica_Take_2_2_business05_f_highpoly_hairSG9";
	setAttr ".ihi" 0;
	setAttr ".ro" yes;
createNode materialInfo -n "Jessica_Take_2_2_materialInfo20";
createNode phong -n "Jessica_Take_2_2_business05_f_highpolyPhong5";
	setAttr ".sc" -type "float3" 0 0 0 ;
createNode shadingEngine -n "Jessica_Take_2_2_business05_f_highpolySG10";
	setAttr ".ihi" 0;
	setAttr ".ro" yes;
createNode materialInfo -n "Jessica_Take_2_2_materialInfo21";
createNode shadingEngine -n "Jessica_Take_2_2_business05_f_highpolySG11";
	setAttr ".ihi" 0;
	setAttr ".ro" yes;
createNode materialInfo -n "Jessica_Take_2_2_materialInfo22";
createNode file -n "Jessica_Take_2_2_Map6";
	setAttr ".ftn" -type "string" "C:\\Users\\Kuan\\Desktop\\iPi Record 20150227\\business05_f\\business05_f_35.tga";
createNode place2dTexture -n "Jessica_Take_2_2_place2dTexture6";
createNode phong -n "Jessica_Take_2_2_business05_f_highpoly_hairPhong5";
	setAttr ".c" -type "float3" 0 0 0 ;
	setAttr ".sc" -type "float3" 0 0 0 ;
createNode shadingEngine -n "Jessica_Take_2_2_business05_f_highpoly_hairSG10";
	setAttr ".ihi" 0;
	setAttr ".ro" yes;
createNode materialInfo -n "Jessica_Take_2_2_materialInfo23";
createNode shadingEngine -n "Jessica_Take_2_2_business05_f_highpoly_hairSG11";
	setAttr ".ihi" 0;
	setAttr ".ro" yes;
createNode materialInfo -n "Jessica_Take_2_2_materialInfo24";
createNode phong -n "Candice_Take_1_business05_f_highpolyPhong";
	setAttr ".sc" -type "float3" 0 0 0 ;
createNode shadingEngine -n "Candice_Take_1_business05_f_highpolySG";
	setAttr ".ihi" 0;
	setAttr ".ro" yes;
createNode materialInfo -n "Candice_Take_1_materialInfo1";
createNode shadingEngine -n "Candice_Take_1_business05_f_highpolySG1";
	setAttr ".ihi" 0;
	setAttr ".ro" yes;
createNode materialInfo -n "Candice_Take_1_materialInfo2";
createNode file -n "Candice_Take_1_Map1";
	setAttr ".ftn" -type "string" "C:\\Users\\Kuan\\Desktop\\iPi Record 20150227\\business05_f\\business05_f_35.tga";
createNode place2dTexture -n "Candice_Take_1_place2dTexture1";
createNode phong -n "Candice_Take_1_business05_f_highpoly_hairPhong";
	setAttr ".c" -type "float3" 0 0 0 ;
	setAttr ".sc" -type "float3" 0 0 0 ;
createNode shadingEngine -n "Candice_Take_1_business05_f_highpoly_hairSG";
	setAttr ".ihi" 0;
	setAttr ".ro" yes;
createNode materialInfo -n "Candice_Take_1_materialInfo3";
createNode shadingEngine -n "Candice_Take_1_business05_f_highpoly_hairSG1";
	setAttr ".ihi" 0;
	setAttr ".ro" yes;
createNode materialInfo -n "Candice_Take_1_materialInfo4";
createNode phong -n "Candice_Take_1_business05_f_highpolyPhong1";
	setAttr ".sc" -type "float3" 0 0 0 ;
createNode shadingEngine -n "Candice_Take_1_business05_f_highpolySG2";
	setAttr ".ihi" 0;
	setAttr ".ro" yes;
createNode materialInfo -n "Candice_Take_1_materialInfo5";
createNode shadingEngine -n "Candice_Take_1_business05_f_highpolySG3";
	setAttr ".ihi" 0;
	setAttr ".ro" yes;
createNode materialInfo -n "Candice_Take_1_materialInfo6";
createNode file -n "Candice_Take_1_Map2";
	setAttr ".ftn" -type "string" "C:\\Users\\Kuan\\Desktop\\iPi Record 20150227\\business05_f\\business05_f_35.tga";
createNode place2dTexture -n "Candice_Take_1_place2dTexture2";
createNode phong -n "Candice_Take_1_business05_f_highpoly_hairPhong1";
	setAttr ".c" -type "float3" 0 0 0 ;
	setAttr ".sc" -type "float3" 0 0 0 ;
createNode shadingEngine -n "Candice_Take_1_business05_f_highpoly_hairSG2";
	setAttr ".ihi" 0;
	setAttr ".ro" yes;
createNode materialInfo -n "Candice_Take_1_materialInfo7";
createNode shadingEngine -n "Candice_Take_1_business05_f_highpoly_hairSG3";
	setAttr ".ihi" 0;
	setAttr ".ro" yes;
createNode materialInfo -n "Candice_Take_1_materialInfo8";
createNode phong -n "Candice_Take_1_business05_f_highpolyPhong2";
	setAttr ".sc" -type "float3" 0 0 0 ;
createNode shadingEngine -n "Candice_Take_1_business05_f_highpolySG4";
	setAttr ".ihi" 0;
	setAttr ".ro" yes;
createNode materialInfo -n "Candice_Take_1_materialInfo9";
createNode shadingEngine -n "Candice_Take_1_business05_f_highpolySG5";
	setAttr ".ihi" 0;
	setAttr ".ro" yes;
createNode materialInfo -n "Candice_Take_1_materialInfo10";
createNode file -n "Candice_Take_1_Map3";
	setAttr ".ftn" -type "string" "C:\\Users\\Kuan\\Desktop\\iPi Record 20150227\\business05_f\\business05_f_35.tga";
createNode place2dTexture -n "Candice_Take_1_place2dTexture3";
createNode phong -n "Candice_Take_1_business05_f_highpoly_hairPhong2";
	setAttr ".c" -type "float3" 0 0 0 ;
	setAttr ".sc" -type "float3" 0 0 0 ;
createNode shadingEngine -n "Candice_Take_1_business05_f_highpoly_hairSG4";
	setAttr ".ihi" 0;
	setAttr ".ro" yes;
createNode materialInfo -n "Candice_Take_1_materialInfo11";
createNode shadingEngine -n "Candice_Take_1_business05_f_highpoly_hairSG5";
	setAttr ".ihi" 0;
	setAttr ".ro" yes;
createNode materialInfo -n "Candice_Take_1_materialInfo12";
createNode phong -n "Candice_Take_1_business05_f_highpolyPhong3";
	setAttr ".sc" -type "float3" 0 0 0 ;
createNode shadingEngine -n "Candice_Take_1_business05_f_highpolySG6";
	setAttr ".ihi" 0;
	setAttr ".ro" yes;
createNode materialInfo -n "Candice_Take_1_materialInfo13";
createNode shadingEngine -n "Candice_Take_1_business05_f_highpolySG7";
	setAttr ".ihi" 0;
	setAttr ".ro" yes;
createNode materialInfo -n "Candice_Take_1_materialInfo14";
createNode file -n "Candice_Take_1_Map4";
	setAttr ".ftn" -type "string" "C:\\Users\\Kuan\\Desktop\\iPi Record 20150227\\business05_f\\business05_f_35.tga";
createNode place2dTexture -n "Candice_Take_1_place2dTexture4";
createNode phong -n "Candice_Take_1_business05_f_highpoly_hairPhong3";
	setAttr ".c" -type "float3" 0 0 0 ;
	setAttr ".sc" -type "float3" 0 0 0 ;
createNode shadingEngine -n "Candice_Take_1_business05_f_highpoly_hairSG6";
	setAttr ".ihi" 0;
	setAttr ".ro" yes;
createNode materialInfo -n "Candice_Take_1_materialInfo15";
createNode shadingEngine -n "Candice_Take_1_business05_f_highpoly_hairSG7";
	setAttr ".ihi" 0;
	setAttr ".ro" yes;
createNode materialInfo -n "Candice_Take_1_materialInfo16";
createNode phong -n "Candice_Take_1_business05_f_highpolyPhong4";
	setAttr ".sc" -type "float3" 0 0 0 ;
createNode shadingEngine -n "Candice_Take_1_business05_f_highpolySG8";
	setAttr ".ihi" 0;
	setAttr ".ro" yes;
createNode materialInfo -n "Candice_Take_1_materialInfo17";
createNode shadingEngine -n "Candice_Take_1_business05_f_highpolySG9";
	setAttr ".ihi" 0;
	setAttr ".ro" yes;
createNode materialInfo -n "Candice_Take_1_materialInfo18";
createNode file -n "Candice_Take_1_Map5";
	setAttr ".ftn" -type "string" "C:\\Users\\Kuan\\Desktop\\iPi Record 20150227\\business05_f\\business05_f_35.tga";
createNode place2dTexture -n "Candice_Take_1_place2dTexture5";
createNode phong -n "Candice_Take_1_business05_f_highpoly_hairPhong4";
	setAttr ".c" -type "float3" 0 0 0 ;
	setAttr ".sc" -type "float3" 0 0 0 ;
createNode shadingEngine -n "Candice_Take_1_business05_f_highpoly_hairSG8";
	setAttr ".ihi" 0;
	setAttr ".ro" yes;
createNode materialInfo -n "Candice_Take_1_materialInfo19";
createNode shadingEngine -n "Candice_Take_1_business05_f_highpoly_hairSG9";
	setAttr ".ihi" 0;
	setAttr ".ro" yes;
createNode materialInfo -n "Candice_Take_1_materialInfo20";
createNode phong -n "Candice_Take_1_business05_f_highpolyPhong5";
	setAttr ".sc" -type "float3" 0 0 0 ;
createNode shadingEngine -n "Candice_Take_1_business05_f_highpolySG10";
	setAttr ".ihi" 0;
	setAttr ".ro" yes;
createNode materialInfo -n "Candice_Take_1_materialInfo21";
createNode shadingEngine -n "Candice_Take_1_business05_f_highpolySG11";
	setAttr ".ihi" 0;
	setAttr ".ro" yes;
createNode materialInfo -n "Candice_Take_1_materialInfo22";
createNode file -n "Candice_Take_1_Map6";
	setAttr ".ftn" -type "string" "C:\\Users\\Kuan\\Desktop\\iPi Record 20150227\\business05_f\\business05_f_35.tga";
createNode place2dTexture -n "Candice_Take_1_place2dTexture6";
createNode phong -n "Candice_Take_1_business05_f_highpoly_hairPhong5";
	setAttr ".c" -type "float3" 0 0 0 ;
	setAttr ".sc" -type "float3" 0 0 0 ;
createNode shadingEngine -n "Candice_Take_1_business05_f_highpoly_hairSG10";
	setAttr ".ihi" 0;
	setAttr ".ro" yes;
createNode materialInfo -n "Candice_Take_1_materialInfo23";
createNode shadingEngine -n "Candice_Take_1_business05_f_highpoly_hairSG11";
	setAttr ".ihi" 0;
	setAttr ".ro" yes;
createNode materialInfo -n "Candice_Take_1_materialInfo24";
createNode phong -n "Candice_Take_1_business05_f_highpolyPhong6";
	setAttr ".sc" -type "float3" 0 0 0 ;
createNode shadingEngine -n "Candice_Take_1_business05_f_highpolySG12";
	setAttr ".ihi" 0;
	setAttr ".ro" yes;
createNode materialInfo -n "Candice_Take_1_materialInfo25";
createNode shadingEngine -n "Candice_Take_1_business05_f_highpolySG13";
	setAttr ".ihi" 0;
	setAttr ".ro" yes;
createNode materialInfo -n "Candice_Take_1_materialInfo26";
createNode file -n "Candice_Take_1_Map7";
	setAttr ".ftn" -type "string" "C:\\Users\\Kuan\\Desktop\\iPi Record 20150227\\business05_f\\business05_f_35.tga";
createNode place2dTexture -n "Candice_Take_1_place2dTexture7";
createNode phong -n "Candice_Take_1_business05_f_highpoly_hairPhong6";
	setAttr ".c" -type "float3" 0 0 0 ;
	setAttr ".sc" -type "float3" 0 0 0 ;
createNode shadingEngine -n "Candice_Take_1_business05_f_highpoly_hairSG12";
	setAttr ".ihi" 0;
	setAttr ".ro" yes;
createNode materialInfo -n "Candice_Take_1_materialInfo27";
createNode shadingEngine -n "Candice_Take_1_business05_f_highpoly_hairSG13";
	setAttr ".ihi" 0;
	setAttr ".ro" yes;
createNode materialInfo -n "Candice_Take_1_materialInfo28";
createNode phong -n "Candice_Take_1_business05_f_highpolyPhong7";
	setAttr ".sc" -type "float3" 0 0 0 ;
createNode shadingEngine -n "Candice_Take_1_business05_f_highpolySG14";
	setAttr ".ihi" 0;
	setAttr ".ro" yes;
createNode materialInfo -n "Candice_Take_1_materialInfo29";
createNode shadingEngine -n "Candice_Take_1_business05_f_highpolySG15";
	setAttr ".ihi" 0;
	setAttr ".ro" yes;
createNode materialInfo -n "Candice_Take_1_materialInfo30";
createNode file -n "Candice_Take_1_Map8";
	setAttr ".ftn" -type "string" "C:\\Users\\Kuan\\Desktop\\iPi Record 20150227\\business05_f\\business05_f_35.tga";
createNode place2dTexture -n "Candice_Take_1_place2dTexture8";
createNode phong -n "Candice_Take_1_business05_f_highpoly_hairPhong7";
	setAttr ".c" -type "float3" 0 0 0 ;
	setAttr ".sc" -type "float3" 0 0 0 ;
createNode shadingEngine -n "Candice_Take_1_business05_f_highpoly_hairSG14";
	setAttr ".ihi" 0;
	setAttr ".ro" yes;
createNode materialInfo -n "Candice_Take_1_materialInfo31";
createNode shadingEngine -n "Candice_Take_1_business05_f_highpoly_hairSG15";
	setAttr ".ihi" 0;
	setAttr ".ro" yes;
createNode materialInfo -n "Candice_Take_1_materialInfo32";
createNode phong -n "Animations_v3_business05_f_highpolyPhong1";
	setAttr ".sc" -type "float3" 0 0 0 ;
createNode shadingEngine -n "Animations_v3_business05_f_highpolySG2";
	setAttr ".ihi" 0;
	setAttr ".ro" yes;
createNode materialInfo -n "Animations_v3_materialInfo5";
createNode shadingEngine -n "Animations_v3_business05_f_highpolySG3";
	setAttr ".ihi" 0;
	setAttr ".ro" yes;
createNode materialInfo -n "Animations_v3_materialInfo6";
createNode file -n "Animations_v3_Map2";
	setAttr ".ftn" -type "string" "C:\\Users\\Kuan\\Desktop\\iPi Record 20150227\\business05_f\\business05_f_35.tga";
createNode place2dTexture -n "Animations_v3_place2dTexture2";
createNode phong -n "Animations_v3_business05_f_highpoly_hairPhong1";
	setAttr ".c" -type "float3" 0 0 0 ;
	setAttr ".sc" -type "float3" 0 0 0 ;
createNode shadingEngine -n "Animations_v3_business05_f_highpoly_hairSG2";
	setAttr ".ihi" 0;
	setAttr ".ro" yes;
createNode materialInfo -n "Animations_v3_materialInfo7";
createNode shadingEngine -n "Animations_v3_business05_f_highpoly_hairSG3";
	setAttr ".ihi" 0;
	setAttr ".ro" yes;
createNode materialInfo -n "Animations_v3_materialInfo8";
createNode phong -n "Kuan_take_1_business05_f_highpolyPhong";
	setAttr ".sc" -type "float3" 0 0 0 ;
createNode shadingEngine -n "Kuan_take_1_business05_f_highpolySG";
	setAttr ".ihi" 0;
	setAttr ".ro" yes;
createNode materialInfo -n "Kuan_take_1_materialInfo1";
createNode shadingEngine -n "Kuan_take_1_business05_f_highpolySG1";
	setAttr ".ihi" 0;
	setAttr ".ro" yes;
createNode materialInfo -n "Kuan_take_1_materialInfo2";
createNode file -n "Kuan_take_1_Map1";
	setAttr ".ftn" -type "string" "C:\\Users\\Kuan\\Desktop\\iPi Record 20150227\\business05_f\\business05_f_35.tga";
createNode place2dTexture -n "Kuan_take_1_place2dTexture1";
createNode phong -n "Kuan_take_1_business05_f_highpoly_hairPhong";
	setAttr ".c" -type "float3" 0 0 0 ;
	setAttr ".sc" -type "float3" 0 0 0 ;
createNode shadingEngine -n "Kuan_take_1_business05_f_highpoly_hairSG";
	setAttr ".ihi" 0;
	setAttr ".ro" yes;
createNode materialInfo -n "Kuan_take_1_materialInfo3";
createNode shadingEngine -n "Kuan_take_1_business05_f_highpoly_hairSG1";
	setAttr ".ihi" 0;
	setAttr ".ro" yes;
createNode materialInfo -n "Kuan_take_1_materialInfo4";
createNode phong -n "Kuan_take_1_business05_f_highpolyPhong1";
	setAttr ".sc" -type "float3" 0 0 0 ;
createNode shadingEngine -n "Kuan_take_1_business05_f_highpolySG2";
	setAttr ".ihi" 0;
	setAttr ".ro" yes;
createNode materialInfo -n "Kuan_take_1_materialInfo5";
createNode shadingEngine -n "Kuan_take_1_business05_f_highpolySG3";
	setAttr ".ihi" 0;
	setAttr ".ro" yes;
createNode materialInfo -n "Kuan_take_1_materialInfo6";
createNode file -n "Kuan_take_1_Map2";
	setAttr ".ftn" -type "string" "C:\\Users\\Kuan\\Desktop\\iPi Record 20150227\\business05_f\\business05_f_35.tga";
createNode place2dTexture -n "Kuan_take_1_place2dTexture2";
createNode phong -n "Kuan_take_1_business05_f_highpoly_hairPhong1";
	setAttr ".c" -type "float3" 0 0 0 ;
	setAttr ".sc" -type "float3" 0 0 0 ;
createNode shadingEngine -n "Kuan_take_1_business05_f_highpoly_hairSG2";
	setAttr ".ihi" 0;
	setAttr ".ro" yes;
createNode materialInfo -n "Kuan_take_1_materialInfo7";
createNode shadingEngine -n "Kuan_take_1_business05_f_highpoly_hairSG3";
	setAttr ".ihi" 0;
	setAttr ".ro" yes;
createNode materialInfo -n "Kuan_take_1_materialInfo8";
createNode phong -n "Kuan_take_1_business05_f_highpolyPhong2";
	setAttr ".sc" -type "float3" 0 0 0 ;
createNode shadingEngine -n "Kuan_take_1_business05_f_highpolySG4";
	setAttr ".ihi" 0;
	setAttr ".ro" yes;
createNode materialInfo -n "Kuan_take_1_materialInfo9";
createNode shadingEngine -n "Kuan_take_1_business05_f_highpolySG5";
	setAttr ".ihi" 0;
	setAttr ".ro" yes;
createNode materialInfo -n "Kuan_take_1_materialInfo10";
createNode file -n "Kuan_take_1_Map3";
	setAttr ".ftn" -type "string" "C:\\Users\\Kuan\\Desktop\\iPi Record 20150227\\business05_f\\business05_f_35.tga";
createNode place2dTexture -n "Kuan_take_1_place2dTexture3";
createNode phong -n "Kuan_take_1_business05_f_highpoly_hairPhong2";
	setAttr ".c" -type "float3" 0 0 0 ;
	setAttr ".sc" -type "float3" 0 0 0 ;
createNode shadingEngine -n "Kuan_take_1_business05_f_highpoly_hairSG4";
	setAttr ".ihi" 0;
	setAttr ".ro" yes;
createNode materialInfo -n "Kuan_take_1_materialInfo11";
createNode shadingEngine -n "Kuan_take_1_business05_f_highpoly_hairSG5";
	setAttr ".ihi" 0;
	setAttr ".ro" yes;
createNode materialInfo -n "Kuan_take_1_materialInfo12";
createNode phong -n "Kuan_take_1_business05_f_highpolyPhong3";
	setAttr ".sc" -type "float3" 0 0 0 ;
createNode shadingEngine -n "Kuan_take_1_business05_f_highpolySG6";
	setAttr ".ihi" 0;
	setAttr ".ro" yes;
createNode materialInfo -n "Kuan_take_1_materialInfo13";
createNode shadingEngine -n "Kuan_take_1_business05_f_highpolySG7";
	setAttr ".ihi" 0;
	setAttr ".ro" yes;
createNode materialInfo -n "Kuan_take_1_materialInfo14";
createNode file -n "Kuan_take_1_Map4";
	setAttr ".ftn" -type "string" "C:\\Users\\Kuan\\Desktop\\iPi Record 20150227\\business05_f\\business05_f_35.tga";
createNode place2dTexture -n "Kuan_take_1_place2dTexture4";
createNode phong -n "Kuan_take_1_business05_f_highpoly_hairPhong3";
	setAttr ".c" -type "float3" 0 0 0 ;
	setAttr ".sc" -type "float3" 0 0 0 ;
createNode shadingEngine -n "Kuan_take_1_business05_f_highpoly_hairSG6";
	setAttr ".ihi" 0;
	setAttr ".ro" yes;
createNode materialInfo -n "Kuan_take_1_materialInfo15";
createNode shadingEngine -n "Kuan_take_1_business05_f_highpoly_hairSG7";
	setAttr ".ihi" 0;
	setAttr ".ro" yes;
createNode materialInfo -n "Kuan_take_1_materialInfo16";
createNode phong -n "Kuan_take_1_business05_f_highpolyPhong4";
	setAttr ".sc" -type "float3" 0 0 0 ;
createNode shadingEngine -n "Kuan_take_1_business05_f_highpolySG8";
	setAttr ".ihi" 0;
	setAttr ".ro" yes;
createNode materialInfo -n "Kuan_take_1_materialInfo17";
createNode shadingEngine -n "Kuan_take_1_business05_f_highpolySG9";
	setAttr ".ihi" 0;
	setAttr ".ro" yes;
createNode materialInfo -n "Kuan_take_1_materialInfo18";
createNode file -n "Kuan_take_1_Map5";
	setAttr ".ftn" -type "string" "C:\\Users\\Kuan\\Desktop\\iPi Record 20150227\\business05_f\\business05_f_35.tga";
createNode place2dTexture -n "Kuan_take_1_place2dTexture5";
createNode phong -n "Kuan_take_1_business05_f_highpoly_hairPhong4";
	setAttr ".c" -type "float3" 0 0 0 ;
	setAttr ".sc" -type "float3" 0 0 0 ;
createNode shadingEngine -n "Kuan_take_1_business05_f_highpoly_hairSG8";
	setAttr ".ihi" 0;
	setAttr ".ro" yes;
createNode materialInfo -n "Kuan_take_1_materialInfo19";
createNode shadingEngine -n "Kuan_take_1_business05_f_highpoly_hairSG9";
	setAttr ".ihi" 0;
	setAttr ".ro" yes;
createNode materialInfo -n "Kuan_take_1_materialInfo20";
createNode phong -n "Kuan_take_1_business05_f_highpolyPhong5";
	setAttr ".sc" -type "float3" 0 0 0 ;
createNode shadingEngine -n "Kuan_take_1_business05_f_highpolySG10";
	setAttr ".ihi" 0;
	setAttr ".ro" yes;
createNode materialInfo -n "Kuan_take_1_materialInfo21";
createNode shadingEngine -n "Kuan_take_1_business05_f_highpolySG11";
	setAttr ".ihi" 0;
	setAttr ".ro" yes;
createNode materialInfo -n "Kuan_take_1_materialInfo22";
createNode file -n "Kuan_take_1_Map6";
	setAttr ".ftn" -type "string" "C:\\Users\\Kuan\\Desktop\\iPi Record 20150227\\business05_f\\business05_f_35.tga";
createNode place2dTexture -n "Kuan_take_1_place2dTexture6";
createNode phong -n "Kuan_take_1_business05_f_highpoly_hairPhong5";
	setAttr ".c" -type "float3" 0 0 0 ;
	setAttr ".sc" -type "float3" 0 0 0 ;
createNode shadingEngine -n "Kuan_take_1_business05_f_highpoly_hairSG10";
	setAttr ".ihi" 0;
	setAttr ".ro" yes;
createNode materialInfo -n "Kuan_take_1_materialInfo23";
createNode shadingEngine -n "Kuan_take_1_business05_f_highpoly_hairSG11";
	setAttr ".ihi" 0;
	setAttr ".ro" yes;
createNode materialInfo -n "Kuan_take_1_materialInfo24";
createNode phong -n "Kuan_take_1_business05_f_highpolyPhong6";
	setAttr ".sc" -type "float3" 0 0 0 ;
createNode shadingEngine -n "Kuan_take_1_business05_f_highpolySG12";
	setAttr ".ihi" 0;
	setAttr ".ro" yes;
createNode materialInfo -n "Kuan_take_1_materialInfo25";
createNode shadingEngine -n "Kuan_take_1_business05_f_highpolySG13";
	setAttr ".ihi" 0;
	setAttr ".ro" yes;
createNode materialInfo -n "Kuan_take_1_materialInfo26";
createNode file -n "Kuan_take_1_Map7";
	setAttr ".ftn" -type "string" "C:\\Users\\Kuan\\Desktop\\iPi Record 20150227\\business05_f\\business05_f_35.tga";
createNode place2dTexture -n "Kuan_take_1_place2dTexture7";
createNode phong -n "Kuan_take_1_business05_f_highpoly_hairPhong6";
	setAttr ".c" -type "float3" 0 0 0 ;
	setAttr ".sc" -type "float3" 0 0 0 ;
createNode shadingEngine -n "Kuan_take_1_business05_f_highpoly_hairSG12";
	setAttr ".ihi" 0;
	setAttr ".ro" yes;
createNode materialInfo -n "Kuan_take_1_materialInfo27";
createNode shadingEngine -n "Kuan_take_1_business05_f_highpoly_hairSG13";
	setAttr ".ihi" 0;
	setAttr ".ro" yes;
createNode materialInfo -n "Kuan_take_1_materialInfo28";
createNode animCurveTL -n "Bip01_Spine_translateX17";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 227 ".ktv[0:226]"  0 -3.3542411327362061 0.8 -3.3660569190979004
		 1.6 -3.3877711296081543 2.4 -3.4096126556396484 3.2 -3.4147436618804932 4 -3.4041707515716553
		 4.8 -3.398033618927002 5.6 -3.4025790691375732 6.4 -3.4135351181030273 7.2 -3.4190673828125
		 8 -3.4268856048583984 8.8 -3.4367406368255615 9.6 -3.4428892135620117 10.4 -3.4367401599884033
		 11.2 -3.4353642463684082 12 -3.4326729774475098 12.8 -3.4302699565887451 13.6 -3.4295041561126709
		 14.4 -3.428692102432251 15.2 -3.4281480312347412 16 -3.4218027591705318 16.8 -3.4092493057250977
		 17.6 -3.4013099670410156 18.4 -3.4013862609863281 19.2 -3.4013233184814453 20 -3.3960995674133301
		 20.8 -3.3923075199127197 21.6 -3.3985776901245117 22.4 -3.4126651287078857 23.2 -3.4271116256713867
		 24 -3.4370386600494385 24.8 -3.4444420337677002 25.6 -3.4577851295471191 26.4 -3.477802038192749
		 27.2 -3.4994182586669922 28 -3.5257637500762939 28.8 -3.5497980117797852 29.6 -3.5898568630218506
		 30.4 -3.6342763900756831 31.2 -3.677956342697144 32 -3.715778112411499 32.8 -3.7530550956726074
		 33.6 -3.7967607975006104 34.4 -3.857309103012085 35.2 -3.9222040176391602 36 -3.9947080612182617
		 36.8 -4.0628294944763184 37.6 -4.1372766494750977 38.4 -4.2387800216674805 39.2 -4.3531813621520996
		 40 -4.458867073059082 40.8 -4.5688557624816895 41.6 -4.6926765441894531 42.4 -4.8140397071838379
		 43.2 -4.9290566444396973 44 -5.0594244003295898 44.8 -5.2213387489318848 45.6 -5.4341645240783691
		 46.4 -5.6784672737121582 47.2 -5.9559202194213867 48 -6.2871823310852051 48.8 -6.624107837677002
		 49.6 -6.9188284873962402 50.4 -7.2083334922790527 51.2 -7.5568318367004403 52 -7.9914789199829102
		 52.8 -8.4520301818847656 53.6 -8.8230552673339844 54.4 -9.1232156753540039 55.2 -9.4863948822021484
		 56 -10.016180038452148 56.8 -10.644852638244627 57.6 -11.252026557922363 58.4 -11.840644836425779
		 59.2 -12.402107238769531 60 -12.909997940063477 60.8 -13.393240928649902 61.6 -13.896806716918944
		 62.4 -14.370970726013184 63.2 -14.833234786987305 64 -15.374340057373049 64.8 -15.99112606048584
		 65.6 -16.59565544128418 66.4 -17.143072128295898 67.2 -17.661602020263672 68 -18.140768051147461
		 68.8 -18.501081466674805 69.6 -18.717704772949219 70.4 -18.845743179321289 71.2 -18.944408416748047
		 72 -19.046367645263672 72.8 -19.142570495605469 73.6 -19.205202102661133 74.4 -19.231527328491211
		 75.2 -19.231904983520508 76 -19.215839385986328 76.8 -19.188774108886719 77.6 -19.14808464050293
		 78.4 -19.097705841064453 79.2 -19.050039291381836 80 -19.005548477172852 80.8 -18.949193954467773
		 81.6 -18.867610931396484 82.4 -18.753101348876953 83.2 -18.62109375 84 -18.497373580932617
		 84.8 -18.371124267578125 85.6 -18.233451843261719 86.4 -18.087890625 87.2 -17.900737762451172
		 88 -17.664875030517578 88.8 -17.43549919128418 89.6 -17.237777709960938 90.4 -17.048614501953125
		 91.2 -16.867328643798828 92 -16.717430114746094 92.8 -16.593011856079102 93.6 -16.470630645751953
		 94.4 -16.340404510498047 95.2 -16.198385238647461 96 -16.061779022216797 96.8 -15.946562767028809
		 97.6 -15.843036651611328 98.4 -15.740511894226074 99.2 -15.65060329437256 100 -15.571406364440918
		 100.8 -15.499135971069336 101.6 -15.441893577575684 102.4 -15.391709327697754 103.2 -15.334810256958008
		 104 -15.272308349609377 104.8 -15.212807655334473 105.6 -15.162100791931152 106.4 -15.116257667541504
		 107.2 -15.068819046020508 108 -15.02335262298584 108.8 -14.98810863494873 109.6 -14.963047981262209
		 110.4 -14.942678451538086 111.2 -14.92415714263916 112 -14.907191276550291 112.8 -14.894782066345217
		 113.6 -14.890121459960938 114.4 -14.887957572937012 115.2 -14.882426261901855 116 -14.878989219665527
		 116.8 -14.88319206237793 117.6 -14.893435478210447 118.4 -14.908621788024902 119.2 -14.929117202758787
		 120 -14.943623542785645 120.8 -14.967108726501465 121.6 -14.986303329467772 122.4 -14.997230529785156
		 123.2 -15.012852668762209 124 -15.031404495239258 124.8 -15.046971321105955 125.6 -15.062737464904783
		 126.4 -15.083033561706545 127.2 -15.100749015808104 128 -15.117768287658691 128.8 -15.134965896606444
		 129.6 -15.159761428833008 130.4 -15.183968544006348 131.2 -15.197022438049316 132 -15.200695037841797
		 132.8 -15.195413589477541 133.6 -15.195524215698242 134.4 -15.205418586730955 135.2 -15.213358879089355
		 136 -15.217185974121094 136.8 -15.215343475341797 137.6 -15.216408729553224 138.4 -15.218119621276855
		 139.2 -15.214542388916016 140 -15.210033416748049 140.8 -15.205856323242188 141.6 -15.19548511505127
		 142.4 -15.181437492370604 143.2 -15.166384696960447 144 -15.148287773132324 144.8 -15.129679679870604
		 145.6 -15.112372398376465 146.4 -15.098200798034668 147.2 -15.088976860046388 148 -15.07862377166748
		 148.8 -15.066399574279783 149.6 -15.058010101318359 150.4 -15.050506591796877 151.2 -15.044358253479004
		 152 -15.044615745544434 152.8 -15.046860694885254 153.6 -15.048827171325684 154.4 -15.051579475402832
		 155.2 -15.049187660217283 156 -15.044485092163086 156.8 -15.046321868896484 157.6 -15.051837921142578
		 158.4 -15.060005187988279 159.2 -15.071120262145996 160 -15.077484130859377 160.8 -15.079119682312012
		 161.6 -15.082655906677246 162.4 -15.091743469238279 163.2 -15.10714054107666 164 -15.121054649353027
		 164.8 -15.126729965209959 165.6 -15.129484176635742 166.4 -15.134593963623049 167.2 -15.138934135437012
		 168 -15.140023231506348 168.8 -15.141534805297852 169.6 -15.140829086303713 170.4 -15.133614540100098
		 171.2 -15.128419876098633 172 -15.126312255859377 172.8 -15.115453720092772 173.6 -15.095004081726074
		 174.4 -15.074848175048828 175.2 -15.059123992919922 176 -15.041157722473145 176.8 -15.017784118652344
		 177.6 -14.997093200683594 178.4 -14.979278564453123 179.2 -14.956013679504396 180 -14.92527961730957
		 180.8 -14.887216567993164;
createNode animCurveTL -n "Bip01_Spine_translateY17";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 227 ".ktv[0:226]"  0 103.06089782714844 0.8 103.0610809326172
		 1.6 103.05841064453124 2.4 103.05557250976562 3.2 103.05332946777344 4 103.04808807373048
		 4.8 103.04317474365234 5.6 103.04454040527344 6.4 103.04794311523436 7.2 103.0483856201172
		 8 103.04686737060548 8.8 103.04540252685548 9.6 103.04311370849608 10.4 103.04051971435548
		 11.2 103.0395050048828 12 103.0364990234375 12.8 103.03218841552734 13.6 103.03424835205078
		 14.4 103.0366668701172 15.2 103.03236389160156 16 103.02651977539062 16.8 103.02220916748048
		 17.6 103.01761627197266 18.4 103.01568603515624 19.2 103.01628875732422 20 103.01429748535156
		 20.8 103.00918579101562 21.6 103.00373840332033 22.4 103.00060272216795 23.2 102.9994659423828
		 24 102.99546051025392 24.8 102.98735809326172 25.6 102.98231506347656 26.4 102.98625946044922
		 27.2 102.99188232421876 28 102.98912048339844 28.8 102.9839324951172 29.6 102.98023223876952
		 30.4 102.97555541992187 31.2 102.97154235839844 32 102.97238159179687 32.8 102.97720336914062
		 33.6 102.98175048828124 34.4 102.98155212402344 35.2 102.97702789306641 36 102.97613525390624
		 36.8 102.97924041748048 37.6 102.98143005371094 38.4 102.98296356201172 39.2 102.98529815673828
		 40 102.98685455322266 40.8 102.9890594482422 41.6 102.99509429931641 42.4 103.00163269042967
		 43.2 103.00177001953124 44 102.99674224853516 44.8 102.99108123779295 45.6 102.98740386962892
		 46.4 102.98733520507812 47.2 102.98973083496094 48 102.99131774902344 48.8 102.99330902099608
		 49.6 102.99822998046876 50.4 103.00450134277344 51.2 103.01083374023436 52 103.01949310302734
		 52.8 103.03097534179687 53.6 103.03963470458984 54.4 103.04258728027344 55.2 103.0429229736328
		 56 103.0451202392578 56.8 103.04647064208984 57.6 103.03665161132812 58.4 103.02056121826172
		 59.2 103.00031280517578 60 102.97526550292967 60.8 102.9446258544922 61.6 102.90221405029295
		 62.4 102.84955596923828 63.2 102.78809356689452 64 102.70659637451172 64.8 102.60420989990234
		 65.6 102.5015869140625 66.4 102.41492462158205 67.2 102.34180450439452 68 102.27403259277344
		 68.8 102.21530151367187 69.6 102.1765594482422 70.4 102.15524291992187 71.2 102.13816833496094
		 72 102.12181854248048 72.8 102.11083984375 73.6 102.10578155517578 74.4 102.10658264160156
		 75.2 102.11440277099608 76 102.12638092041016 76.8 102.13833618164062 77.6 102.14871215820312
		 78.4 102.1581573486328 79.2 102.16558837890624 80 102.17081451416016 80.8 102.179931640625
		 81.6 102.19600677490234 82.4 102.21641540527344 83.2 102.23987579345705 84 102.26356506347656
		 84.8 102.28392028808594 85.6 102.3009033203125 86.4 102.3203582763672 87.2 102.34832763671876
		 88 102.37884521484376 88.8 102.40201568603516 89.6 102.419189453125 90.4 102.43720245361328
		 91.2 102.45530700683594 92 102.46592712402344 92.8 102.47322082519533 93.6 102.4879150390625
		 94.4 102.50543975830078 95.2 102.51731872558594 96 102.5242156982422 96.8 102.52999114990234
		 97.6 102.53562927246094 98.4 102.54051208496094 99.2 102.54399871826172 100 102.54806518554687
		 100.8 102.55204010009766 101.6 102.555419921875 102.4 102.56088256835936 103.2 102.56789398193359
		 104 102.5747528076172 104.8 102.57802581787108 105.6 102.57624053955078 106.4 102.57655334472656
		 107.2 102.58113861083984 108 102.58298492431641 108.8 102.58081817626952 109.6 102.57859039306641
		 110.4 102.57643127441406 111.2 102.5753173828125 112 102.57504272460936 112.8 102.57325744628906
		 113.6 102.57575225830078 114.4 102.58206939697266 115.2 102.57904052734376 116 102.5692138671875
		 116.8 102.56658172607422 117.6 102.57016754150392 118.4 102.57308197021484 119.2 102.57372283935548
		 120 102.57205963134766 120.8 102.56557464599608 121.6 102.55924224853516 122.4 102.55342864990234
		 123.2 102.55315399169922 124 102.55601501464844 124.8 102.55461883544922 125.6 102.54444885253906
		 126.4 102.53752899169922 127.2 102.54483795166016 128 102.55467987060548 128.8 102.55789947509766
		 129.6 102.55690002441406 130.4 102.55632019042967 131.2 102.55773162841795 132 102.55834197998048
		 132.8 102.55575561523436 133.6 102.55296325683594 134.4 102.55657196044922 135.2 102.56509399414062
		 136 102.5714874267578 136.8 102.57306671142578 137.6 102.5698699951172 138.4 102.5690460205078
		 139.2 102.57003784179687 140 102.56919097900392 140.8 102.57117462158205 141.6 102.57678985595705
		 142.4 102.57987213134766 143.2 102.58197784423828 144 102.58664703369141 144.8 102.5881805419922
		 145.6 102.58441925048828 146.4 102.58322143554687 147.2 102.58879089355467 148 102.59152221679687
		 148.8 102.58367919921876 149.6 102.57463836669922 150.4 102.57298278808594 151.2 102.57561492919922
		 152 102.5767822265625 152.8 102.57481384277344 153.6 102.57261657714844 154.4 102.568603515625
		 155.2 102.56126403808594 156 102.55745697021484 156.8 102.56149291992187 157.6 102.56919097900392
		 158.4 102.56965637207033 159.2 102.55809020996094 160 102.54666900634766 160.8 102.54380798339844
		 161.6 102.54332733154295 162.4 102.54029846191406 163.2 102.53557586669922 164 102.53125762939452
		 164.8 102.52819061279295 165.6 102.52427673339844 166.4 102.51712799072266 167.2 102.50696563720705
		 168 102.49501037597656 168.8 102.48497772216795 169.6 102.48207092285156 170.4 102.48302459716795
		 171.2 102.480224609375 172 102.47244262695312 172.8 102.45986938476562 173.6 102.44594573974608
		 174.4 102.43954467773436 175.2 102.44272613525392 176 102.44737243652344 176.8 102.44441223144533
		 177.6 102.43490600585936 178.4 102.42613220214844 179.2 102.4190673828125 180 102.41265106201172
		 180.8 102.40931701660156;
createNode animCurveTL -n "Bip01_Spine_translateZ17";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 227 ".ktv[0:226]"  0 3.4578561782836914 0.8 3.4637992382049561
		 1.6 3.4594721794128418 2.4 3.4497756958007812 3.2 3.4419763088226318 4 3.4359445571899414
		 4.8 3.4244327545166016 5.6 3.4072034358978271 6.4 3.3937325477600098 7.2 3.3818187713623047
		 8 3.3602907657623291 8.8 3.3452305793762207 9.6 3.3432223796844478 10.4 3.33888840675354
		 11.2 3.3324892520904541 12 3.3248858451843266 12.8 3.3121066093444824 13.6 3.3025181293487549
		 14.4 3.3005623817443848 15.2 3.312047958374023 16 3.3189373016357422 16.8 3.3002562522888184
		 17.6 3.2716820240020752 18.4 3.264091968536377 19.2 3.2791032791137695 20 3.2936840057373047
		 20.8 3.301870584487915 21.6 3.316781759262085 22.4 3.332751989364624 23.2 3.3344571590423584
		 24 3.3279449939727783 24.8 3.3261475563049316 25.6 3.3341817855834961 26.4 3.3545386791229248
		 27.2 3.3744735717773437 28 3.3736634254455566 28.8 3.3668754100799561 29.6 3.368964672088623
		 30.4 3.3768794536590576 31.2 3.37874436378479 32 3.3770880699157715 32.8 3.3820545673370361
		 33.6 3.3939847946166992 34.4 3.4010589122772217 35.2 3.3997032642364502 36 3.4040637016296387
		 36.8 3.4079921245574951 37.6 3.4054625034332275 38.4 3.4128131866455078 39.2 3.440129280090332
		 40 3.4651839733123779 40.8 3.4725534915924072 41.6 3.4773440361022949 42.4 3.4872803688049316
		 43.2 3.4963884353637695 44 3.5081427097320557 44.8 3.5217528343200684 45.6 3.5398597717285156
		 46.4 3.5679054260253902 47.2 3.5977396965026855 48 3.6177809238433838 48.8 3.6404051780700688
		 49.6 3.669608592987061 50.4 3.6892478466033936 51.2 3.7027840614318848 52 3.71514892578125
		 52.8 3.715679407119751 53.6 3.7053637504577641 54.4 3.710285902023315 55.2 3.7300286293029785
		 56 3.743222713470459 56.8 3.7535140514373775 57.6 3.7601511478424072 58.4 3.7813467979431152
		 59.2 3.8139567375183105 60 3.8535265922546382 60.8 3.9082868099212646 61.6 3.9748742580413814
		 62.4 4.0438718795776367 63.2 4.1211647987365723 64 4.2160162925720215 64.8 4.327333927154541
		 65.6 4.4422450065612793 66.4 4.5489950180053711 67.2 4.667762279510498 68 4.8098998069763184
		 68.8 4.9369521141052246 69.6 5.0254087448120117 70.4 5.0895748138427734 71.2 5.1405301094055176
		 72 5.1852269172668457 72.8 5.2306928634643555 73.6 5.2766671180725098 74.4 5.3155045509338379
		 75.2 5.3441882133483887 76 5.3758258819580078 76.8 5.4216499328613281 77.6 5.4778480529785156
		 78.4 5.5343880653381348 79.2 5.588188648223877 80 5.6520042419433594 80.8 5.7288122177124023
		 81.6 5.8059391975402832 82.4 5.8891630172729492 83.2 5.9810590744018555 84 6.065434455871582
		 84.8 6.1499390602111816 85.6 6.2492685317993164 86.4 6.3579754829406738 87.2 6.4816780090332031
		 88 6.6227965354919434 88.8 6.760744571685791 89.6 6.8858680725097656 90.4 7.0013618469238281
		 91.2 7.1032471656799316 92 7.1856179237365723 92.8 7.253939151763916 93.6 7.3191919326782227
		 94.4 7.3879637718200684 95.2 7.4636383056640625 96 7.532966136932373 96.8 7.5883998870849618
		 97.6 7.6390366554260254 98.4 7.6841597557067871 99.2 7.7097139358520508 100 7.7269935607910156
		 100.8 7.7548794746398926 101.6 7.7881908416748047 102.4 7.8175039291381836 103.2 7.8452544212341309
		 104 7.870258331298829 104.8 7.8815979957580558 105.6 7.871978759765625 106.4 7.8534269332885742
		 107.2 7.8431015014648429 108 7.8394069671630868 108.8 7.8360505104064941 109.6 7.8291802406311044
		 110.4 7.8099169731140146 111.2 7.7782235145568848 112 7.7444887161254883 112.8 7.7149949073791504
		 113.6 7.685755729675293 114.4 7.6459670066833505 115.2 7.5936784744262686 116 7.5381851196289054
		 116.8 7.4826078414916992 117.6 7.4253506660461426 118.4 7.3659462928771964 119.2 7.3011994361877441
		 120 7.2386651039123535 120.8 7.1749839782714844 121.6 7.1171483993530273 122.4 7.0519881248474121
		 123.2 6.9883909225463867 124 6.9246664047241211 124.8 6.8592104911804199 125.6 6.7918481826782227
		 126.4 6.7309374809265137 127.2 6.6816859245300293 128 6.6343154907226562 128.8 6.5871682167053223
		 129.6 6.5361366271972656 130.4 6.4867548942565918 131.2 6.4423642158508301 132 6.4000182151794434
		 132.8 6.3662371635437012 133.6 6.3319363594055176 134.4 6.2881159782409668 135.2 6.241976261138916
		 136 6.1982731819152832 136.8 6.1595783233642578 137.6 6.1190128326416016 138.4 6.0843610763549805
		 139.2 6.0505146980285645 140 6.0061101913452148 140.8 5.9568581581115723 141.6 5.9067306518554687
		 142.4 5.8519167900085449 143.2 5.8028397560119629 144 5.7671680450439453 144.8 5.7312021255493164
		 145.6 5.6864433288574219 146.4 5.6315398216247559 147.2 5.5625185966491699 148 5.4929795265197754
		 148.8 5.4334659576416016 149.6 5.3704738616943359 150.4 5.2974162101745605 151.2 5.2206630706787109
		 152 5.1454148292541504 152.8 5.0750112533569336 153.6 5.0053610801696777 154.4 4.9251832962036133
		 155.2 4.8321876525878906 156 4.7431855201721191 156.8 4.6682877540588379 157.6 4.58770751953125
		 158.4 4.4900879859924316 159.2 4.3987817764282227 160 4.3281574249267578 160.8 4.2606310844421387
		 161.6 4.1832752227783203 162.4 4.1075124740600586 163.2 4.040740966796875 164 3.975696325302124
		 164.8 3.9128358364105225 165.6 3.8559460639953618 166.4 3.804049015045166 167.2 3.7589390277862553
		 168 3.7175636291503902 168.8 3.6764888763427734 169.6 3.6437678337097168 170.4 3.6235640048980713
		 171.2 3.6018850803375244 172 3.5710976123809814 172.8 3.5536403656005859 173.6 3.5618541240692139
		 174.4 3.5721123218536377 175.2 3.5742301940917969 176 3.5859000682830811 176.8 3.6186044216156006
		 177.6 3.6691038608551025 178.4 3.7183232307434082 179.2 3.7510890960693359 180 3.7853436470031734
		 180.8 3.83487868309021;
createNode animCurveTA -n "Bip01_Spine_rotateX17";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 227 ".ktv[0:226]"  0 -0.47989296913146984 0.8 -0.48745414614677424
		 1.6 -0.48965340852737427 2.4 -0.48667261004447943 3.2 -0.47828301787376409 4 -0.4629450142383576
		 4.8 -0.44266813993453974 5.6 -0.42242869734764094 6.4 -0.42719125747680664 7.2 -0.41315093636512751
		 8 -0.4003676176071167 8.8 -0.3846530020236969 9.6 -0.36984232068061834 10.4 -0.36236780881881714
		 11.2 -0.36897897720336914 12 -0.36384189128875727 12.8 -0.37422162294387817 13.6 -0.37860584259033198
		 14.4 -0.37670999765396118 15.2 -0.37759131193161011 16 -0.38803070783615112 16.8 -0.40368589758872986
		 17.6 -0.42383086681365967 18.4 -0.44728237390518194 19.2 -0.46992692351341264 20 -0.48953792452812189
		 20.8 -0.51188629865646362 21.6 -0.54246747493743896 22.4 -0.57140171527862549 23.2 -0.59218549728393555
		 24 -0.61532688140869141 24.8 -0.64730530977249146 25.6 -0.67810124158859253 26.4 -0.69599455595016479
		 27.2 -0.70585501194000244 28 -0.71718168258667003 28.8 -0.7615034580230714 29.6 -0.76201671361923218
		 30.4 -0.75802427530288707 31.2 -0.75843286514282227 32 -0.76633298397064209 32.8 -0.77362680435180675
		 33.6 -0.77442073822021484 34.4 -0.75350153446197521 35.2 -0.75093019008636475 36 -0.7452046275138855
		 36.8 -0.73930448293685924 37.6 -0.7342265248298645 38.4 -0.72824788093566895 39.2 -0.7247312068939209
		 40 -0.72394406795501709 40.8 -0.72284126281738292 41.6 -0.72365868091583252 42.4 -0.72886043787002563
		 43.2 -0.7373013496398928 44 -0.76092106103897084 44.8 -0.77243685722351074 45.6 -0.78608572483062755
		 46.4 -0.80340176820755005 47.2 -0.82725751399993885 48 -0.85596632957458507 48.8 -0.88500881195068348
		 49.6 -0.91356974840164196 50.4 -0.94174838066101074 51.2 -0.96769475936889648 52 -0.98970973491668679
		 52.8 -1.0277488231658936 53.6 -1.0505572557449341 54.4 -1.0751602649688721 55.2 -1.100761890411377
		 56 -1.1259074211120603 56.8 -1.1549310684204102 57.6 -1.1673073768615725 58.4 -1.1837213039398191
		 59.2 -1.2075338363647461 60 -1.2290817499160769 60.8 -1.2519041299819946 61.6 -1.274752140045166
		 62.4 -1.2923489809036257 63.2 -1.3003560304641724 64 -1.3107639551162722 64.8 -1.3312376737594604
		 65.6 -1.3535438776016235 66.4 -1.3726005554199221 67.2 -1.3898522853851318 68 -1.4067625999450684
		 68.8 -1.427952766418457 69.6 -1.4589593410491943 70.4 -1.4991298913955693 71.2 -1.5433629751205444
		 72 -1.5909433364868164 72.8 -1.6473369598388672 73.6 -1.7150787115097046 74.4 -1.7919310331344602
		 75.2 -1.8744699954986579 76 -1.9552692174911499 76.8 -2.0332551002502441 77.6 -2.1195135116577148
		 78.4 -2.2187232971191406 79.2 -2.3249139785766602 80 -2.4316332340240483 80.8 -2.5366916656494141
		 81.6 -2.6461036205291748 82.4 -2.7632207870483398 83.2 -2.8784134387969971 84 -2.9832785129547119
		 84.8 -3.0822393894195557 85.6 -3.1827831268310547 86.4 -3.2871811389923096 87.2 -3.3899641036987309
		 88 -3.4793057441711426 88.8 -3.5555152893066406 89.6 -3.6306581497192383 90.4 -3.703808069229126
		 91.2 -3.7640295028686523 92 -3.805333375930787 92.8 -3.8294610977172838 93.6 -3.8491806983947749
		 94.4 -3.8748650550842281 95.2 -3.8959546089172363 96 -3.8893969058990479 96.8 -3.8754341602325439
		 97.6 -3.8523669242858887 98.4 -3.8145382404327393 99.2 -3.7657527923584002 100 -3.7203304767608643
		 100.8 -3.6792404651641846 101.6 -3.6328554153442383 102.4 -3.57513427734375 103.2 -3.5054481029510498
		 104 -3.4245238304138184 104.8 -3.3380372524261475 105.6 -3.2558584213256836 106.4 -3.175971508026123
		 107.2 -3.0895595550537109 108 -2.9967727661132813 108.8 -2.9050865173339844 109.6 -2.8206567764282227
		 110.4 -2.7377007007598877 111.2 -2.6504135131835938 112 -2.5668916702270508 112.8 -2.4881284236907959
		 113.6 -2.4102680683135982 114.4 -2.3386125564575195 115.2 -2.2708766460418701 116 -2.2060506343841553
		 116.8 -2.155562162399292 117.6 -2.1183643341064453 118.4 -2.0821304321289062 119.2 -2.0490200519561768
		 120 -2.0371687412261967 120.8 -2.0278186798095703 121.6 -1.9915690422058103 122.4 -2.0089428424835205
		 123.2 -2.0042366981506348 124 -1.9970030784606938 124.8 -2.0026044845581055 125.6 -2.0257813930511475
		 126.4 -2.0582339763641357 127.2 -2.0917236804962158 128 -2.112739086151123 128.8 -2.1433150768280029
		 129.6 -2.1757969856262207 130.4 -2.2019238471984863 131.2 -2.2380216121673584 132 -2.2517728805541992
		 132.8 -2.270209789276123 133.6 -2.2856771945953369 134.4 -2.2917525768280029 135.2 -2.293809175491333
		 136 -2.2780206203460693 136.8 -2.2692208290100098 137.6 -2.2440483570098877 138.4 -2.2061660289764404
		 139.2 -2.1567955017089844 140 -2.0994904041290288 140.8 -2.0404613018035889 141.6 -1.9754284620285032
		 142.4 -1.900554299354553 143.2 -1.8187177181243903 144 -1.7263448238372805 144.8 -1.6201286315917969
		 145.6 -1.5112006664276123 146.4 -1.4097548723220823 147.2 -1.3086684942245483 148 -1.201080322265625
		 148.8 -1.0935060977935791 149.6 -0.99224853515625 150.4 -0.89802706241607666 151.2 -0.80779874324798595
		 152 -0.71798664331436157 152.8 -0.63370102643966675 153.6 -0.56199735403060913 154.4 -0.50476789474487305
		 155.2 -0.4604550302028656 156 -0.42130020260810846 156.8 -0.38749054074287415 157.6 -0.36651214957237249
		 158.4 -0.35454019904136658 159.2 -0.34865480661392212 160 -0.35551869869232178 160.8 -0.37232488393783569
		 161.6 -0.38654342293739319 162.4 -0.40095263719558721 163.2 -0.42945215106010443
		 164 -0.47013908624649048 164.8 -0.51255190372467041 165.6 -0.55620187520980835 166.4 -0.60146737098693848
		 167.2 -0.64343357086181641 168 -0.68653810024261486 168.8 -0.73648130893707275 169.6 -0.78026622533798229
		 170.4 -0.81046825647354126 171.2 -0.8396129608154298 172 -0.87358564138412487 172.8 -0.90543907880783103
		 173.6 -0.92835038900375377 174.4 -0.94652318954467796 175.2 -0.96421819925308228
		 176 -0.97104007005691528 176.8 -0.96530908346176125 177.6 -0.95979881286621105 178.4 -0.95975655317306507
		 179.2 -0.95994848012924194 180 -0.94992870092391968 180.8 -0.92967492341995228;
createNode animCurveTA -n "Bip01_Spine_rotateY17";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 227 ".ktv[0:226]"  0 0.18540266156196591 0.8 0.15850846469402316
		 1.6 0.12540413439273837 2.4 0.097540907561779022 3.2 0.08916550874710083 4 0.092146880924701691
		 4.8 0.08243260532617569 5.6 0.056901227682828903 6.4 0.053060982376337051 7.2 0.033967029303312302
		 8 0.019765064120292664 8.8 0.0041505959816277027 9.6 -0.01218269020318985 10.4 -0.035458195954561234
		 11.2 -0.017972217872738838 12 -0.021013703197240829 12.8 -0.033749837428331375 13.6 -0.041199278086423881
		 14.4 -0.050667129456996918 15.2 -0.060291364789009094 16 -0.061490315943956375 16.8 -0.054946325719356537
		 17.6 -0.053935598582029343 18.4 -0.060971301048994064 19.2 -0.06514432281255722 20 -0.060487072914838798
		 20.8 -0.055908817797899246 21.6 -0.062404867261648178 22.4 -0.075863249599933638
		 23.2 -0.084615990519523621 24 -0.081970959901809692 24.8 -0.072102777659893036 25.6 -0.066480807960033417
		 26.4 -0.063158676028251648 27.2 -0.048839062452316291 28 -0.022134488448500633 28.8 -0.023098248988389969
		 29.6 0.004918957594782114 30.4 0.025359248742461205 31.2 0.043516281992197037 32 0.070279136300086975
		 32.8 0.10596573352813719 33.6 0.13974897563457489 34.4 0.19383019208908081 35.2 0.22439303994178772
		 36 0.26389625668525701 36.8 0.31231746077537537 37.6 0.36843574047088623 38.4 0.42691957950592041
		 39.2 0.48363393545150762 40 0.53999865055084229 40.8 0.59846019744873047 41.6 0.6594272255897522
		 42.4 0.71866273880004883 43.2 0.77054834365844715 44 0.8425748348236084 44.8 0.90401303768157959
		 45.6 0.97000336647033691 46.4 1.0327179431915285 47.2 1.0993430614471436 48 1.1761413812637329
		 48.8 1.2532193660736086 49.6 1.3161468505859375 50.4 1.3644405603408811 51.2 1.4035776853561399
		 52 1.43183434009552 52.8 1.4791176319122314 53.6 1.5029386281967163 54.4 1.5236184597015381
		 55.2 1.5295577049255371 56 1.5251933336257937 56.8 1.5115445852279663 57.6 1.4984403848648071
		 58.4 1.4992505311965942 59.2 1.4536099433898926 60 1.4411599636077881 60.8 1.4167718887329102
		 61.6 1.3818793296813965 62.4 1.3407734632492063 63.2 1.293806791305542 64 1.2421702146530151
		 64.8 1.191087007522583 65.6 1.1390516757965088 66.4 1.0827248096466064 67.2 1.0233725309371948
		 68 0.9618818163871764 68.8 0.9018176794052124 69.6 0.84589380025863647 70.4 0.79604893922805797
		 71.2 0.75305944681167603 72 0.70459139347076416 72.8 0.64553040266036987 73.6 0.58949697017669678
		 74.4 0.54276293516159058 75.2 0.50176191329956055 76 0.46721565723419189 76.8 0.43991684913635254
		 77.6 0.41234293580055242 78.4 0.37852528691291809 79.2 0.34435272216796875 80 0.3164505660533905
		 80.8 0.29659166932106018 81.6 0.28481251001358032 82.4 0.27730640769004827 83.2 0.26725512742996216
		 84 0.25041684508323669 84.8 0.23045776784420016 85.6 0.20890273153781891 86.4 0.18186882138252256
		 87.2 0.15807169675827029 88 0.14877885580062866 88.8 0.14343661069869995 89.6 0.12889908254146576
		 90.4 0.10887223482131958 91.2 0.084535777568817139 92 0.05238712951540947 92.8 0.02161015011370182
		 93.6 0.0023026198614388709 94.4 -0.0081296805292367935 95.2 -0.017921019345521927
		 96 -0.027289798483252525 96.8 -0.049686919897794717 97.6 -0.075766012072563171 98.4 -0.10624417662620544
		 99.2 -0.13717597723007205 100 -0.16479997336864471 100.8 -0.19686421751976016 101.6 -0.23705002665519709
		 102.4 -0.27622526884078985 103.2 -0.30905315279960632 104 -0.33879289031028748 104.8 -0.36902040243148798
		 105.6 -0.39795058965682983 106.4 -0.42105501890182495 107.2 -0.44250267744064326
		 108 -0.47147956490516663 108.8 -0.5061415433883667 109.6 -0.5359312891960144 110.4 -0.55434137582778931
		 111.2 -0.5624503493309021 112 -0.57064855098724365 112.8 -0.5907517671585083 113.6 -0.6188092827796936
		 114.4 -0.63859474658966064 115.2 -0.64393317699432373 116 -0.6444280743598938 116.8 -0.65085762739181519
		 117.6 -0.66571921110153198 118.4 -0.6806563138961792 119.2 -0.68825483322143555 120 -0.73307639360427856
		 120.8 -0.74747782945632923 121.6 -0.75765329599380493 122.4 -0.76910418272018433
		 123.2 -0.7805733084678651 124 -0.78975695371627808 124.8 -0.79010945558547963 125.6 -0.79244613647460949
		 126.4 -0.8059195876121521 127.2 -0.81882482767105103 128 -0.8109511137008667 128.8 -0.82172763347625732
		 129.6 -0.8365073800086974 130.4 -0.84501481056213379 131.2 -0.85082143545150768 132 -0.83840930461883556
		 132.8 -0.81628787517547596 133.6 -0.79689794778823853 134.4 -0.78505569696426403
		 135.2 -0.76935893297195435 136 -0.73805195093154907 136.8 -0.71754205226898193 137.6 -0.70144999027252197
		 138.4 -0.68233901262283325 139.2 -0.65575122833251953 140 -0.62342369556427002 140.8 -0.58660084009170532
		 141.6 -0.54614746570587158 142.4 -0.5069584846496582 143.2 -0.47345849871635448 144 -0.44462954998016357
		 144.8 -0.41793477535247808 145.6 -0.39138364791870123 146.4 -0.3679457306861878 147.2 -0.34915024042129517
		 148 -0.3274587094783783 148.8 -0.3063352108001709 149.6 -0.29467266798019409 150.4 -0.28992146253585815
		 151.2 -0.29320460557937622 152 -0.30267238616943365 152.8 -0.30925279855728149 153.6 -0.31806713342666626
		 154.4 -0.333362877368927 155.2 -0.34307146072387695 156 -0.34599000215530396 156.8 -0.35780775547027588
		 157.6 -0.38220798969268804 158.4 -0.4059430062770843 159.2 -0.41998240351676946 160 -0.42857491970062256
		 160.8 -0.43963354825973522 161.6 -0.45602956414222712 162.4 -0.47644287347793568
		 163.2 -0.49469530582427984 164 -0.50308239459991455 164.8 -0.50493800640106201 165.6 -0.5074341893196106
		 166.4 -0.5084417462348938 167.2 -0.50470823049545288 168 -0.4977064728736878 168.8 -0.49181661009788513
		 169.6 -0.48977497220039368 170.4 -0.4913687407970429 171.2 -0.49416655302047729 172 -0.48843502998352045
		 172.8 -0.46857059001922619 173.6 -0.44207313656806946 174.4 -0.41935437917709351
		 175.2 -0.40802472829818731 176 -0.40453150868415833 176.8 -0.39800497889518738 177.6 -0.38857647776603699
		 178.4 -0.37945684790611262 179.2 -0.3684237003326416 180 -0.35324490070343018 180.8 -0.33084326982498169;
createNode animCurveTA -n "Bip01_Spine_rotateZ17";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 227 ".ktv[0:226]"  0 -3.2264554500579834 0.8 -3.192085981369019
		 1.6 -3.1492099761962891 2.4 -3.0988121032714844 3.2 -3.053138256072998 4 -3.0199542045593266
		 4.8 -2.9976885318756104 5.6 -2.9813568592071533 6.4 -2.9743022918701172 7.2 -2.9546728134155273
		 8 -2.9391887187957764 8.8 -2.9332575798034668 9.6 -2.9301731586456299 10.4 -2.9481358528137207
		 11.2 -2.9711377620697026 12 -2.9918580055236816 12.8 -3.0301704406738281 13.6 -3.0678985118865967
		 14.4 -3.1136543750762939 15.2 -3.1642942428588872 16 -3.2195515632629395 16.8 -3.2739086151123047
		 17.6 -3.3264837265014648 18.4 -3.3848474025726318 19.2 -3.4512391090393062 20 -3.5221059322357178
		 20.8 -3.5936884880065918 21.6 -3.6644341945648193 22.4 -3.7325644493103023 23.2 -3.7926654815673828
		 24 -3.845022439956665 24.8 -3.8955895900726318 25.6 -3.9475126266479497 26.4 -4.0046191215515137
		 27.2 -4.0617260932922363 28 -4.1047782897949219 28.8 -4.1267180442810059 29.6 -4.1666264533996582
		 30.4 -4.1972956657409668 31.2 -4.2131261825561523 32 -4.2273836135864258 32.8 -4.2471837997436523
		 33.6 -4.2696256637573242 34.4 -4.274632453918457 35.2 -4.2781457901000977 36 -4.2798237800598145
		 36.8 -4.2820568084716797 37.6 -4.2844862937927246 38.4 -4.2914862632751465 39.2 -4.3032321929931641
		 40 -4.3136467933654785 40.8 -4.3174300193786621 41.6 -4.3157882690429687 42.4 -4.3138413429260254
		 43.2 -4.3135461807250985 44 -4.3081021308898926 44.8 -4.3051342964172363 45.6 -4.2971978187561035
		 46.4 -4.2902050018310547 47.2 -4.2884922027587891 48 -4.2847518920898437 48.8 -4.2727398872375488
		 49.6 -4.2556662559509277 50.4 -4.2386980056762695 51.2 -4.2249979972839355 52 -4.2131214141845703
		 52.8 -4.1884112358093262 53.6 -4.1666202545166016 54.4 -4.1429877281188965 55.2 -4.122307300567627
		 56 -4.1051664352416992 56.8 -4.0552754402160645 57.6 -4.0322551727294922 58.4 -4.0100970268249512
		 59.2 -3.9817786216735831 60 -3.95157790184021 60.8 -3.9203546047210702 61.6 -3.8896768093109122
		 62.4 -3.8591432571411137 63.2 -3.8313992023468018 64 -3.7984557151794438 64.8 -3.7536075115203853
		 65.6 -3.7022104263305664 66.4 -3.6543114185333252 67.2 -3.6172535419464102 68 -3.5823388099670415
		 68.8 -3.5403800010681152 69.6 -3.4992101192474365 70.4 -3.4616601467132568 71.2 -3.4223694801330566
		 72 -3.3807370662689209 72.8 -3.3389899730682373 73.6 -3.3015933036804199 74.4 -3.2700934410095215
		 75.2 -3.2423105239868164 76 -3.2200589179992676 76.8 -3.2044041156768799 77.6 -3.1879122257232666
		 78.4 -3.1655209064483643 79.2 -3.1452724933624268 80 -3.1359474658966064 80.8 -3.1312558650970459
		 81.6 -3.1217880249023437 82.4 -3.1142680644989018 83.2 -3.11411452293396 84 -3.1155302524566655
		 84.8 -3.1208488941192627 85.6 -3.1310303211212158 86.4 -3.1368758678436279 87.2 -3.1416199207305908
		 88 -3.1581366062164307 88.8 -3.1844513416290283 89.6 -3.2066717147827148 90.4 -3.2224998474121098
		 91.2 -3.2409718036651611 92 -3.2621457576751709 92.8 -3.2839252948760982 93.6 -3.3122308254241943
		 94.4 -3.3443145751953125 95.2 -3.3706021308898926 96 -3.4022116661071777 96.8 -3.437305212020874
		 97.6 -3.4764401912689209 98.4 -3.5075745582580566 99.2 -3.5359454154968266 100 -3.5753970146179199
		 100.8 -3.6263213157653809 101.6 -3.6850094795227046 102.4 -3.7552573680877681 103.2 -3.8322467803955083
		 104 -3.9043834209442139 104.8 -3.9671769142150879 105.6 -4.0223979949951172 106.4 -4.081061840057373
		 107.2 -4.1523280143737793 108 -4.2287516593933105 108.8 -4.3031487464904785 109.6 -4.3794822692871094
		 110.4 -4.4580326080322266 111.2 -4.5292186737060547 112 -4.5884242057800293 112.8 -4.6454057693481445
		 113.6 -4.7064967155456543 114.4 -4.7629795074462891 115.2 -4.8086185455322266 116 -4.8490419387817383
		 116.8 -4.8888740539550781 117.6 -4.9246568679809579 118.4 -4.9532990455627441 119.2 -4.9765539169311523
		 120 -4.9751181602478027 120.8 -4.9916439056396484 121.6 -4.9998841285705566 122.4 -5.0287065505981445
		 123.2 -5.0442991256713867 124 -5.0689821243286133 124.8 -5.0922269821166992 125.6 -5.103182315826416
		 126.4 -5.1063647270202637 127.2 -5.1144504547119141 128 -5.1465959548950195 128.8 -5.1703424453735352
		 129.6 -5.1970319747924805 130.4 -5.2246980667114258 131.2 -5.23931884765625 132 -5.2572793960571289
		 132.8 -5.2681188583374023 133.6 -5.2767724990844727 134.4 -5.2868204116821289 135.2 -5.2971181869506836
		 136 -5.3197693824768066 136.8 -5.3259778022766113 137.6 -5.3366112709045419 138.4 -5.348121166229248
		 139.2 -5.3482184410095215 140 -5.337806224822998 140.8 -5.3257789611816406 141.6 -5.3159809112548828
		 142.4 -5.3114280700683594 143.2 -5.3137321472167969 144 -5.3163847923278809 144.8 -5.3083891868591317
		 145.6 -5.2897882461547852 146.4 -5.2680301666259775 147.2 -5.2464046478271484 148 -5.2285795211791992
		 148.8 -5.2110724449157715 149.6 -5.1862215995788583 150.4 -5.160001277923584 151.2 -5.1395416259765625
		 152 -5.1224260330200195 152.8 -5.1043663024902344 153.6 -5.0816755294799805 154.4 -5.0496439933776855
		 155.2 -5.0091295242309579 156 -4.9736080169677734 156.8 -4.9501113891601571 157.6 -4.9229307174682617
		 158.4 -4.8762378692626953 159.2 -4.8191337585449219 160 -4.7696084976196289 160.8 -4.722492218017579
		 161.6 -4.6672430038452148 162.4 -4.6131925582885742 163.2 -4.5645885467529297 164 -4.5124740600585937
		 164.8 -4.450901985168457 165.6 -4.3800311088562012 166.4 -4.3100190162658691 167.2 -4.2486433982849121
		 168 -4.1878643035888672 168.8 -4.1187419891357422 169.6 -4.0468339920043945 170.4 -3.9848136901855464
		 171.2 -3.9317011833190918 172 -3.8765909671783465 172.8 -3.8216102123260494 173.6 -3.7775135040283203
		 174.4 -3.7440392971038818 175.2 -3.7121875286102295 176 -3.6803410053253169 176.8 -3.6568899154663086
		 177.6 -3.6477148532867432 178.4 -3.6440744400024414 179.2 -3.6359250545501713 180 -3.6308083534240718
		 180.8 -3.6387057304382302;
createNode animCurveTA -n "Bip01_Spine1_rotateX17";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 227 ".ktv[0:226]"  0 1.2244691848754885 0.8 1.2240734100341797
		 1.6 1.2219403982162476 2.4 1.219290018081665 3.2 1.2162529230117798 4 1.213020443916321
		 4.8 1.209742546081543 5.6 1.2063125371932986 6.4 1.2024697065353394 7.2 1.201407790184021
		 8 1.1983764171600342 8.8 1.1965349912643433 9.6 1.1955956220626831 10.4 1.1954376697540283
		 11.2 1.1963908672332764 12 1.1988632678985596 12.8 1.2001829147338869 13.6 1.2060064077377319
		 14.4 1.2092556953430176 15.2 1.2128947973251345 16 1.2227039337158203 16.8 1.2257107496261597
		 17.6 1.2350329160690308 18.4 1.243797779083252 19.2 1.2538638114929199 20 1.2569139003753662
		 20.8 1.2669929265975952 21.6 1.2753459215164185 22.4 1.2828109264373779 23.2 1.2897312641143801
		 24 1.2960103750228882 24.8 1.3015210628509521 25.6 1.3062674999237061 26.4 1.3103748559951782
		 27.2 1.3140332698822019 28 1.3173973560333252 28.8 1.3208633661270142 29.6 1.3251631259918215
		 30.4 1.3265678882598877 31.2 1.3323030471801758 32 1.339535117149353 32.8 1.342707633972168
		 33.6 1.3555467128753662 34.4 1.3599843978881836 35.2 1.3757995367050171 36 1.3921505212783811
		 36.8 1.4106905460357666 37.6 1.4342262744903564 38.4 1.4684777259826662 39.2 1.4894139766693115
		 40 1.5198572874069214 40.8 1.5552632808685305 41.6 1.5939500331878662 42.4 1.6358211040496826
		 43.2 1.6807548999786377 44 1.728499174118042 44.8 1.7787085771560669 45.6 1.831021189689636
		 46.4 1.8851457834243783 47.2 1.9408138990402224 48 1.997724175453186 48.8 2.0554652214050293
		 49.6 2.1136157512664795 50.4 2.1718237400054932 51.2 2.229811429977417 52 2.2873437404632568
		 52.8 2.3441731929779053 53.6 2.4000821113586426 54.4 2.4548616409301758 55.2 2.5083341598510742
		 56 2.560343980789185 56.8 2.610771656036377 57.6 2.659534215927124 58.4 2.7065441608428955
		 59.2 2.7516887187957764 60 2.7948262691497803 60.8 2.8359014987945557 61.6 2.8748815059661865
		 62.4 2.911710262298584 63.2 2.9463229179382324 64 2.9769163131713867 64.8 2.9999957084655762
		 65.6 3.043615341186523 66.4 3.0564522743225098 67.2 3.0919318199157715 68 3.1028530597686768
		 68.8 3.1330118179321289 69.6 3.1423094272613525 70.4 3.1679964065551758 71.2 3.1751103401184082
		 72 3.1940066814422607 72.8 3.2092514038085938 73.6 3.2229394912719727 74.4 3.2369685173034668
		 75.2 3.2529981136322021 76 3.2577223777770996 76.8 3.2732357978820801 77.6 3.2861995697021484
		 78.4 3.2982785701751713 79.2 3.3109924793243408 80 3.3256766796112061 80.8 3.3299758434295654
		 81.6 3.3443810939788814 82.4 3.3576512336730957 83.2 3.3611259460449219 84 3.3717491626739502
		 84.8 3.3799841403961186 85.6 3.3873934745788579 86.4 3.3891880512237549 87.2 3.3936281204223633
		 88 3.3954906463623047 88.8 3.3953206539154053 89.6 3.3935406208038335 90.4 3.3899085521697998
		 91.2 3.383731603622437 92 3.3809561729431152 92.8 3.3700551986694336 93.6 3.3665027618408203
		 94.4 3.3549258708953862 95.2 3.3432440757751465 96 3.3289847373962398 96.8 3.3245077133178711
		 97.6 3.309180736541748 98.4 3.2958204746246338 99.2 3.2828671932220459 100 3.2688281536102295
		 100.8 3.2521650791168213 101.6 3.2463245391845703 102.4 3.2263197898864746 103.2 3.2197315692901611
		 104 3.2002327442169189 104.8 3.1946036815643311 105.6 3.1779823303222661 106.4 3.1617562770843506
		 107.2 3.1565315723419189 108 3.1383864879608154 108.8 3.1307542324066162 109.6 3.1233201026916504
		 110.4 3.1071970462799072 111.2 3.1028473377227788 112 3.0910592079162602 112.8 3.0815665721893311
		 113.6 3.0731890201568608 114.4 3.0649623870849609 115.2 3.0560603141784668 116 3.0535633563995361
		 116.8 3.0458006858825688 117.6 3.0395128726959229 118.4 3.0332989692687988 119.2 3.0313198566436768
		 120 3.0248980522155762 120.8 3.023151159286499 121.6 3.0185675621032715 122.4 3.0148618221282959
		 123.2 3.0139040946960449 124 3.0109930038452148 124.8 3.0087137222290039 125.6 3.0066177845001225
		 126.4 3.0060670375823975 127.2 3.0043675899505615 128 3.00299072265625 128.8 3.0016715526580815
		 129.6 3.0002677440643311 130.4 2.9987094402313232 131.2 2.9969677925109863 132 2.9949579238891602
		 132.8 2.9925136566162109 133.6 2.989454030990601 134.4 2.9854202270507813 135.2 2.9797358512878418
		 136 2.9778091907501221 136.8 2.9700331687927246 137.6 2.960807085037231 138.4 2.9578218460083008
		 139.2 2.9466779232025142 140 2.9350676536560059 140.8 2.9204576015472412 141.6 2.9157438278198242
		 142.4 2.897985458374023 143.2 2.8776123523712158 144 2.8675680160522461 144.8 2.8573923110961914
		 145.6 2.8346552848815918 146.4 2.8153619766235352 147.2 2.7955751419067383 148 2.7722158432006836
		 148.8 2.7652285099029541 149.6 2.7420713901519775 150.4 2.7227427959442139 151.2 2.7041623592376709
		 152 2.6833870410919189 152.8 2.6766362190246582 153.6 2.6548919677734375 154.4 2.6489629745483398
		 155.2 2.6338412761688232 156 2.6220049858093266 156.8 2.6109814643859863 157.6 2.6078219413757324
		 158.4 2.5985813140869141 159.2 2.5963504314422612 160 2.5918087959289551 160.8 2.5892050266265869
		 161.6 2.5877234935760498 162.4 2.5868377685546875 163.2 2.5863637924194336 164 2.5860335826873779
		 164.8 2.5855016708374023 165.6 2.5844309329986572 166.4 2.5839886665344238 167.2 2.5819048881530762
		 168 2.5789926052093506 168.8 2.5779132843017578 169.6 2.5730071067810059 170.4 2.5665538311004639
		 171.2 2.5643954277038574 172 2.5563147068023682 172.8 2.548447847366333 173.6 2.5401077270507812
		 174.4 2.5310447216033936 175.2 2.5213885307312012 176 2.5105862617492676 176.8 2.4974761009216309
		 177.6 2.4935202598571777 178.4 2.4804165363311768 179.2 2.4694998264312744 180 2.4595019817352295
		 180.8 2.4498405456542969;
createNode animCurveTA -n "Bip01_Spine1_rotateY17";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 227 ".ktv[0:226]"  0 0.53237813711166382 0.8 0.53636318445205688
		 1.6 0.54935276508331299 2.4 0.56001430749893188 3.2 0.5696834921836853 4 0.57900172472000122
		 4.8 0.58790379762649536 5.6 0.59682470560073853 6.4 0.60677629709243774 7.2 0.60965710878372192
		 8 0.61890882253646862 8.8 0.62637579441070557 9.6 0.63298475742340088 10.4 0.63913613557815552
		 11.2 0.64515268802642822 12 0.65186268091201782 12.8 0.65414559841156017 13.6 0.66214001178741455
		 14.4 0.66558760404586792 15.2 0.6689947247505188 16 0.67654049396514893 16.8 0.67858296632766724
		 17.6 0.68412154912948608 18.4 0.68876200914382935 19.2 0.69347363710403442 20 0.69478946924209595
		 20.8 0.69875723123550415 21.6 0.70153433084487915 22.4 0.7034153938293457 23.2 0.70451486110687256
		 24 0.70480304956436157 24.8 0.70421206951141357 25.6 0.70268166065216064 26.4 0.70021408796310425
		 27.2 0.69688528776168823 28 0.69272422790527344 28.8 0.6872631311416626 29.6 0.67975121736526489
		 30.4 0.67731255292892456 31.2 0.66837334632873535 32 0.65912151336669922 32.8 0.65609568357467651
		 33.6 0.64611893892288208 34.4 0.64337080717086792 35.2 0.63632577657699585 36 0.63124978542327881
		 36.8 0.62766760587692261 37.6 0.62547034025192261 38.4 0.6251031756401062 39.2 0.62639379501342773
		 40 0.62914425134658813 40.8 0.63364887237548828 41.6 0.63994705677032471 42.4 0.64802175760269165
		 43.2 0.65782767534255993 44 0.66929733753204346 44.8 0.68235361576080322 45.6 0.69690620899200439
		 46.4 0.7128746509552002 47.2 0.73018753528594971 48 0.74878311157226574 48.8 0.76859712600708008
		 49.6 0.78956049680709839 50.4 0.81163132190704357 51.2 0.83475238084793091 52 0.85883796215057373
		 52.8 0.88380682468414318 53.6 0.90957057476043701 54.4 0.93609327077865601 55.2 0.96332937479019165
		 56 0.99123060703277599 56.8 1.0197068452835083 57.6 1.0486732721328735 58.4 1.0780274868011477
		 59.2 1.107660174369812 60 1.1374163627624512 60.8 1.1671733856201172 61.6 1.1967933177947998
		 62.4 1.2261494398117063 63.2 1.2550477981567385 64 1.2816592454910278 64.8 1.3024848699569702
		 65.6 1.3442181348800659 66.4 1.3569861650466919 67.2 1.3935937881469729 68 1.405120849609375
		 68.8 1.4376257658004761 69.6 1.4477458000183103 70.4 1.4760066270828249 71.2 1.48382568359375
		 72 1.5042413473129272 72.8 1.5200132131576538 73.6 1.5333783626556396 74.4 1.5460686683654783
		 75.2 1.5593913793563845 76 1.5630621910095217 76.8 1.5743331909179687 77.6 1.582844614982605
		 78.4 1.589902400970459 79.2 1.5964983701705933 80 1.6032669544219973 80.8 1.6050806045532229
		 81.6 1.610637903213501 82.4 1.6152852773666382 83.2 1.6164088249206543 84 1.61963951587677
		 84.8 1.6218132972717283 85.6 1.6234610080718994 86.4 1.6238054037094116 87.2 1.624504566192627
		 88 1.6245664358139038 88.8 1.6241304874420166 89.6 1.6234070062637329 90.4 1.622475266456604
		 91.2 1.6212645769119265 92 1.6208446025848389 92.8 1.6194261312484739 93.6 1.6190440654754641
		 94.4 1.6181392669677734 95.2 1.6176282167434692 96 1.6174697875976565 96.8 1.617529630661011
		 97.6 1.6181567907333374 98.4 1.619176983833313 99.2 1.6205328702926636 100 1.622369647026062
		 100.8 1.6249130964279177 101.6 1.6259040832519533 102.4 1.6295357942581177 103.2 1.630771279335022
		 104 1.6344772577285771 104.8 1.6355305910110474 105.6 1.6383563280105595 106.4 1.6405708789825439
		 107.2 1.6410120725631714 108 1.6417624950408936 108.8 1.6414941549301147 109.6 1.6408448219299316
		 110.4 1.6377514600753784 111.2 1.6365600824356079 112 1.6318715810775757 112.8 1.6268222332000737
		 113.6 1.621059775352478 114.4 1.6139156818389893 115.2 1.6043356657028198 116 1.6012083292007446
		 116.8 1.5901437997817991 117.6 1.5795848369598389 118.4 1.5672649145126345 119.2 1.5628025531768801
		 120 1.547216534614563 120.8 1.5426630973815918 121.6 1.5291075706481934 122.4 1.5160547494888306
		 123.2 1.5124188661575315 124 1.5005848407745359 124.8 1.4904040098190308 125.6 1.4796885251998899
		 126.4 1.4766087532043457 127.2 1.4667295217514038 128 1.4587340354919434 128.8 1.4516024589538574
		 129.6 1.4448602199554443 130.4 1.4384744167327881 131.2 1.4323650598526001 132 1.4264651536941528
		 132.8 1.4207686185836792 133.6 1.4151978492736816 134.4 1.4092965126037598 135.2 1.4023283720016479
		 136 1.4002423286437988 136.8 1.3930104970932009 137.6 1.3859068155288696 138.4 1.3839011192321775
		 139.2 1.3773127794265747 140 1.3715652227401731 140.8 1.365465521812439 141.6 1.3637280464172363
		 142.4 1.3581198453903198 143.2 1.3530595302581787 144 1.3510994911193848 144.8 1.3492003679275513
		 145.6 1.3458043336868286 146.4 1.3435484170913696 147.2 1.3418930768966677 148 1.3406530618667605
		 148.8 1.3404327630996704 149.6 1.3401418924331665 150.4 1.3403232097625732 151.2 1.3408684730529783
		 152 1.34180748462677 152.8 1.3422049283981323 153.6 1.3436288833618164 154.4 1.3440041542053225
		 155.2 1.344890832901001 156 1.34520423412323 156.8 1.3448635339736938 157.6 1.3444854021072388
		 158.4 1.3426299095153809 159.2 1.3418792486190796 160 1.3387299776077273 160.8 1.3350690603256226
		 161.6 1.3306455612182615 162.4 1.3252590894699097 163.2 1.3189322948455813 164 1.311764121055603
		 164.8 1.3032405376434326 165.6 1.2921226024627686 166.4 1.2885557413101196 167.2 1.2753918170928955
		 168 1.2615494728088379 168.8 1.257475733757019 169.6 1.2429559230804443 170.4 1.228204607963562
		 171.2 1.2239738702774048 172 1.2100567817687988 172.8 1.1985656023025513 173.6 1.1881743669509888
		 174.4 1.1783366203308103 175.2 1.1691961288452148 176 1.1604350805282593 176.8 1.1512256860733032
		 177.6 1.1487029790878296 178.4 1.1410895586013794 179.2 1.1355798244476318 180 1.1314079761505127
		 180.8 1.1281545162200928;
createNode animCurveTA -n "Bip01_Spine1_rotateZ17";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 227 ".ktv[0:226]"  0 0.95965933799743663 0.8 0.95872747898101807
		 1.6 0.95657688379287709 2.4 0.9560171365737915 3.2 0.9568687081336974 4 0.95896309614181519
		 4.8 0.96231180429458607 5.6 0.96746230125427246 6.4 0.97535908222198486 7.2 0.97812670469284047
		 8 0.98857557773590077 8.8 0.99879676103591908 9.6 1.0097631216049197 10.4 1.0218170881271362
		 11.2 1.0356733798980713 12 1.0528837442398071 12.8 1.0591683387756348 13.6 1.0820897817611694
		 14.4 1.0923129320144651 15.2 1.1025508642196655 16 1.1254432201385498 16.8 1.1316957473754885
		 17.6 1.1486475467681885 18.4 1.1625317335128784 19.2 1.1762676239013672 20 1.1800068616867063
		 20.8 1.1910824775695801 21.6 1.1986277103424072 22.4 1.20369553565979 23.2 1.2069207429885864
		 24 1.2083312273025513 24.8 1.2079545259475708 25.6 1.2057647705078125 26.4 1.2018353939056396
		 27.2 1.1962889432907104 28 1.1892333030700684 28.8 1.1801942586898804 29.6 1.1680839061737061
		 30.4 1.1642098426818848 31.2 1.1499937772750854 32 1.1353552341461182 32.8 1.1304903030395508
		 33.6 1.1141279935836792 34.4 1.1095004081726074 35.2 1.097049355506897 36 1.0873203277587893
		 36.8 1.0791330337524414 37.6 1.0716849565505979 38.4 1.0642735958099363 39.2 1.0614614486694336
		 40 1.0583384037017822 40.8 1.0561689138412476 41.6 1.0552968978881838 42.4 1.055694580078125
		 43.2 1.0573729276657104 44 1.0602319240570068 44.8 1.064200758934021 45.6 1.0691204071044922
		 46.4 1.0748498439788818 47.2 1.0812854766845703 48 1.0883302688598633 48.8 1.0958957672119141
		 49.6 1.103877067565918 50.4 1.1121736764907837 51.2 1.1207011938095095 52 1.1293270587921145
		 52.8 1.1380002498626709 53.6 1.1466959714889526 54.4 1.1553174257278442 55.2 1.1638257503509519
		 56 1.1722068786621094 56.8 1.180455207824707 57.6 1.1885552406311035 58.4 1.1964458227157593
		 59.2 1.2040530443191528 60 1.2113355398178101 60.8 1.2182836532592771 61.6 1.2248520851135254
		 62.4 1.2309887409210205 63.2 1.236646294593811 64 1.2414966821670532 64.8 1.2450040578842163
		 65.6 1.2508766651153564 66.4 1.2523041963577273 67.2 1.2552661895751951 68 1.2557960748672483
		 68.8 1.2559108734130859 69.6 1.2554398775100708 70.4 1.2525678873062134 71.2 1.2514463663101196
		 72 1.2467813491821289 72.8 1.241486668586731 73.6 1.2353649139404297 74.4 1.2279112339019775
		 75.2 1.2182198762893677 76 1.2151232957839966 76.8 1.20428466796875 77.6 1.1946183443069458
		 78.4 1.1852988004684448 79.2 1.1754568815231323 80 1.1641242504119873 80.8 1.1608027219772341
		 81.6 1.149888277053833 82.4 1.1402345895767212 83.2 1.1378076076507568 84 1.130734920501709
		 84.8 1.1260175704956057 85.6 1.1228444576263428 86.4 1.1223363876342771 87.2 1.1220777034759519
		 88 1.1235002279281616 88.8 1.1264255046844482 89.6 1.1307893991470337 90.4 1.1371084451675415
		 91.2 1.1463950872421265 92 1.150220513343811 92.8 1.1645054817199707 93.6 1.169009804725647
		 94.4 1.183474063873291 95.2 1.197995662689209 96 1.215472936630249 96.8 1.2209255695343018
		 97.6 1.2397736310958862 98.4 1.2564816474914553 99.2 1.2727622985839844 100 1.2906087636947632
		 100.8 1.3122087717056274 101.6 1.3199150562286377 102.4 1.3465303182601929 103.2 1.3553836345672607
		 104 1.3818235397338867 104.8 1.38947594165802 105.6 1.4119325876235962 106.4 1.4335885047912598
		 107.2 1.4404113292694092 108 1.4637093544006348 108.8 1.4732037782669067 109.6 1.4822142124176025
		 110.4 1.5005753040313721 111.2 1.5052359104156494 112 1.5166441202163696 112.8 1.5246485471725464
		 113.6 1.5305477380752563 114.4 1.5350879430770874 115.2 1.5385231971740725 116 1.5391421318054199
		 116.8 1.5398023128509519 117.6 1.538676381111145 118.4 1.5358365774154663 119.2 1.5344686508178711
		 120 1.5290745496749878 120.8 1.5273638963699341 121.6 1.521756649017334 122.4 1.5158498287200928
		 123.2 1.5141391754150393 124 1.5085722208023071 124.8 1.503940224647522 125.6 1.4994558095932009
		 126.4 1.4982936382293699 127.2 1.4950419664382937 128 1.4930524826049805 128.8 1.4919002056121826
		 129.6 1.4913692474365234 130.4 1.4913872480392456 131.2 1.4919371604919434 132 1.4930342435836792
		 132.8 1.4946589469909668 133.6 1.4967173337936399 134.4 1.4991521835327148 135.2 1.5022478103637695
		 136 1.5032204389572144 136.8 1.5066884756088257 137.6 1.5100268125534058 138.4 1.5109292268753054
		 139.2 1.5137017965316772 140 1.5157527923583984 140.8 1.5174928903579714 141.6 1.5179042816162107
		 142.4 1.518845796585083 143.2 1.5190232992172239 144 1.518767237663269 144.8 1.5184750556945801
		 145.6 1.5173929929733276 146.4 1.5161086320877075 147.2 1.5143096446990969 148 1.5116012096405027
		 148.8 1.5106267929077148 149.6 1.5069535970687866 150.4 1.5033196210861206 151.2 1.4992716312408447
		 152 1.4942649602890017 152.8 1.4923694133758545 153.6 1.4855141639709473 154.4 1.4833765029907229
		 155.2 1.4766404628753662 156 1.4701045751571655 156.8 1.4622772932052612 157.6 1.4593268632888794
		 158.4 1.4487693309783936 159.2 1.44557785987854 160 1.4358536005020142 160.8 1.4270466566085815
		 161.6 1.4182237386703491 162.4 1.4089984893798828 163.2 1.3993699550628662 164 1.3893678188323977
		 164.8 1.3784323930740356 165.6 1.3654404878616333 166.4 1.3615578413009644 167.2 1.3480180501937866
		 168 1.3346066474914553 168.8 1.3308205604553225 169.6 1.3178359270095823 170.4 1.3051848411560061
		 171.2 1.3016700744628906 172 1.2903523445129397 172.8 1.2812631130218506 173.6 1.2732352018356323
		 174.4 1.2657278776168823 175.2 1.2587107419967651 176 1.2518643140792849 176.8 1.2445638179779053
		 177.6 1.2425713539123535 178.4 1.2365932464599607 179.2 1.2322859764099121 180 1.2288390398025513
		 180.8 1.2258938550949097;
createNode animCurveTA -n "Bip01_Spine2_rotateX17";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 227 ".ktv[0:226]"  0 4.1152982711791992 0.8 4.1130380630493164
		 1.6 4.1096439361572266 2.4 4.0977835655212402 3.2 4.0932550430297852 4 4.078300952911377
		 4.8 4.0732870101928711 5.6 4.0576944351196289 6.4 4.050849437713623 7.2 4.043485164642334
		 8 4.0380048751831064 8.8 4.0305542945861816 9.6 4.0296101570129395 10.4 4.0305223464965829
		 11.2 4.0341782569885254 12 4.0410451889038086 12.8 4.0511059761047372 13.6 4.0642704963684082
		 14.4 4.0804948806762695 15.2 4.0997147560119629 16 4.1216831207275391 16.8 4.1459112167358398
		 17.6 4.1717038154602051 18.4 4.1984925270080566 19.2 4.2258667945861816 20 4.2534127235412598
		 20.8 4.2806406021118164 21.6 4.3055663108825684 22.4 4.3234233856201172 23.2 4.3558564186096191
		 24 4.3829851150512695 24.8 4.3898639678955078 25.6 4.4107398986816406 26.4 4.4287223815917969
		 27.2 4.4363484382629403 28 4.4463419914245605 28.8 4.4567122459411621 29.6 4.467411994934082
		 30.4 4.478304386138916 31.2 4.4882283210754395 32 4.5159687995910645 32.8 4.5337324142456055
		 33.6 4.5606045722961426 34.4 4.5956869125366211 35.2 4.6388182640075684 36 4.690762996673584
		 36.8 4.7523984909057617 37.6 4.824610710144043 38.4 4.9077892303466797 39.2 5.0017490386962891
		 40 5.1071052551269531 40.8 5.2252054214477539 41.6 5.3557348251342773 42.4 5.496828556060791
		 43.2 5.6475272178649902 44 5.807563304901123 44.8 5.9759840965270996 45.6 6.1515731811523437
		 46.4 6.3333888053894043 47.2 6.520571231842041 48 6.7120256423950195 48.8 6.9064216613769531
		 49.6 7.1023526191711426 50.4 7.2986268997192383 51.2 7.4943385124206543 52 7.6886591911315918
		 52.8 7.8807964324951172 53.6 8.0700035095214844 54.4 8.2555618286132812 55.2 8.436884880065918
		 56 8.6134414672851562 56.8 8.7848453521728516 57.6 8.9507904052734375 58.4 9.1109743118286133
		 59.2 9.2649993896484375 60 9.4124336242675781 60.8 9.5529842376708984 61.6 9.6865568161010742
		 62.4 9.8132534027099609 63.2 9.9333152770996094 64 10.045907974243164 64.8 10.14935874938965
		 65.6 10.243911743164062 66.4 10.331569671630859 67.2 10.413241386413574 68 10.488831520080566
		 68.8 10.558510780334473 69.6 10.622761726379396 70.4 10.682104110717772 71.2 10.737225532531738
		 72 10.788920402526855 72.8 10.837894439697266 73.6 10.88471794128418 74.4 10.929753303527832
		 75.2 10.973362922668455 76 11.015793800354004 76.8 11.057278633117676 77.6 11.098034858703612
		 78.4 11.138243675231934 79.2 11.177912712097168 80 11.216790199279783 80.8 11.254439353942873
		 81.6 11.290294647216797 82.4 11.323764801025392 83.2 11.354342460632326 84 11.38048267364502
		 84.8 11.398385047912598 85.6 11.426803588867188 86.4 11.444319725036619 87.2 11.455187797546388
		 88 11.460580825805664 88.8 11.460301399230955 89.6 11.453799247741699 90.4 11.438506126403809
		 91.2 11.426615715026855 92 11.408061981201172 92.8 11.384794235229494 93.6 11.357813835144045
		 94.4 11.327487945556641 95.2 11.294155120849608 96 11.258234024047852 96.8 11.220288276672363
		 97.6 11.180902481079102 98.4 11.140459060668944 99.2 11.099145889282228 100 11.057153701782228
		 100.8 11.014781951904297 101.6 10.97230339050293 102.4 10.929988861083984 103.2 10.888088226318359
		 104 10.84674072265625 104.8 10.805970191955566 105.6 10.765811920166016 106.4 10.72640895843506
		 107.2 10.687968254089355 108 10.650710105895996 108.8 10.614725112915041 109.6 10.580037117004396
		 110.4 10.546636581420898 111.2 10.514516830444336 112 10.483707427978516 112.8 10.454220771789553
		 113.6 10.427721977233888 114.4 10.408889770507812 115.2 10.374311447143556 116 10.342886924743652
		 116.8 10.328671455383301 117.6 10.31016254425049 118.4 10.291678428649902 119.2 10.274506568908691
		 120 10.258591651916504 120.8 10.244061470031738 121.6 10.231033325195312 122.4 10.219468116760254
		 123.2 10.209165573120115 124 10.199932098388672 124.8 10.191702842712402 125.6 10.184800148010254
		 126.4 10.179808616638184 127.2 10.170812606811523 128 10.16804027557373 128.8 10.15953254699707
		 129.6 10.15518856048584 130.4 10.149912834167482 131.2 10.145046234130859 132 10.132823944091797
		 132.8 10.125644683837892 133.6 10.11525821685791 134.4 10.103399276733398 135.2 10.092537879943848
		 136 10.065164566040041 136.8 10.054864883422852 137.6 10.017486572265623 138.4 9.9982986450195313
		 139.2 9.9774847030639648 140 9.9207553863525391 140.8 9.8907909393310547 141.6 9.8506021499633807
		 142.4 9.8047237396240234 143.2 9.7554292678833008 144 9.7027721405029297 144.8 9.64691162109375
		 145.6 9.5882425308227539 146.4 9.5274896621704102 147.2 9.46551513671875 148 9.4031066894531232
		 148.8 9.3408412933349609 149.6 9.2792158126831055 150.4 9.2187805175781232 151.2 9.1602277755737305
		 152 9.1043186187744141 152.8 9.0518150329589844 153.6 9.0033159255981445 154.4 8.959141731262207
		 155.2 8.9193134307861328 156 8.8855466842651367 156.8 8.8625926971435547 157.6 8.8255214691162109
		 158.4 8.7982492446899414 159.2 8.7915554046630859 160 8.7731103897094727 160.8 8.7685222625732422
		 161.6 8.7588844299316406 162.4 8.7557773590087891 163.2 8.7526607513427734 164 8.7495260238647461
		 164.8 8.7459859848022461 165.6 8.7414579391479492 166.4 8.7355813980102539 167.2 8.7281351089477539
		 168 8.7187519073486328 168.8 8.7069005966186523 169.6 8.6920967102050781 170.4 8.6741447448730469
		 171.2 8.6531200408935547 172 8.6291713714599627 172.8 8.602442741394043 173.6 8.5731439590454102
		 174.4 8.5415668487548828 175.2 8.5081233978271484 176 8.4732561111450195 176.8 8.4375314712524414
		 177.6 8.4015731811523437 178.4 8.3659534454345703 179.2 8.3331193923950195 180 8.3074464797973633
		 180.8 8.2562847137451172;
createNode animCurveTA -n "Bip01_Spine2_rotateY17";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 227 ".ktv[0:226]"  0 1.6933194398880005 0.8 1.7176492214202881
		 1.6 1.7415882349014282 2.4 1.7940865755081179 3.2 1.8102273941040037 4 1.8567847013473513
		 4.8 1.8715481758117676 5.6 1.9150927066802976 6.4 1.9346370697021484 7.2 1.9566493034362793
		 8 1.9748668670654297 8.8 2.0115060806274414 9.6 2.0269250869750977 10.4 2.0450212955474858
		 11.2 2.0630912780761719 12 2.0800273418426514 12.8 2.0961744785308838 13.6 2.1117672920227051
		 14.4 2.126943826675415 15.2 2.1417179107666016 16 2.1559686660766602 16.8 2.1695642471313477
		 17.6 2.182396411895752 18.4 2.194394588470459 19.2 2.2054719924926758 20 2.2154743671417236
		 20.8 2.2241699695587158 21.6 2.2310299873352051 22.4 2.2353446483612061 23.2 2.2397289276123047
		 24 2.23972487449646 24.8 2.2388303279876709 25.6 2.2322368621826176 26.4 2.2202961444854736
		 27.2 2.2118082046508789 28 2.1987466812133789 28.8 2.1824970245361328 29.6 2.1637942790985107
		 30.4 2.14459228515625 31.2 2.1283929347991943 32 2.0914115905761719 32.8 2.0746650695800781
		 33.6 2.0544664859771729 34.4 2.0343301296234131 35.2 2.0161874294281006 36 2.0006654262542725
		 36.8 1.9884574413299561 37.6 1.9802883863449099 38.4 1.9765983819961543 39.2 1.9775964021682739
		 40 1.983894944190979 40.8 1.9964498281478882 41.6 2.0152852535247803 42.4 2.0395123958587646
		 43.2 2.0687170028686523 44 2.1029767990112305 44.8 2.1421632766723633 45.6 2.185988187789917
		 46.4 2.2342376708984375 47.2 2.2867143154144287 48 2.3432154655456543 48.8 2.403548002243042
		 49.6 2.4675533771514893 50.4 2.5350978374481201 51.2 2.6060359477996826 52 2.6801235675811768
		 52.8 2.757102489471436 53.6 2.8367307186126709 54.4 2.9188735485076904 55.2 3.0034599304199219
		 56 3.0903089046478271 56.8 3.179161548614502 57.6 3.2697339057922363 58.4 3.3617184162139893
		 59.2 3.4547774791717529 60 3.5484638214111328 60.8 3.6422998905181885 61.6 3.7359378337860112
		 62.4 3.8290047645568848 63.2 3.9211831092834464 64 4.011695384979248 64.8 4.099428653717041
		 65.6 4.1838884353637695 66.4 4.2653212547302246 67.2 4.3435125350952148 68 4.4178466796875
		 68.8 4.487938404083252 69.6 4.5536885261535645 70.4 4.6151103973388672 71.2 4.6722316741943359
		 72 4.7250847816467285 72.8 4.7737412452697754 73.6 4.8182759284973145 74.4 4.85882568359375
		 75.2 4.8956484794616699 76 4.9290390014648437 76.8 4.9592552185058594 77.6 4.9865508079528809
		 78.4 5.0111045837402344 79.2 5.033088207244873 80 5.0525898933410645 80.8 5.0697083473205566
		 81.6 5.0845608711242676 82.4 5.0973033905029297 83.2 5.1080265045166025 84 5.1164555549621582
		 84.8 5.1218328475952148 85.6 5.1288666725158691 86.4 5.132258415222168 87.2 5.1334996223449707
		 88 5.1330041885375977 88.8 5.130894660949707 89.6 5.1273527145385751 90.4 5.1219906806945801
		 91.2 5.118842601776123 92 5.1144943237304687 92.8 5.1098728179931641 93.6 5.1054320335388184
		 94.4 5.101381778717041 95.2 5.0979108810424814 96 5.0952606201171875 96.8 5.0936651229858398
		 97.6 5.0933027267456055 98.4 5.0941934585571289 99.2 5.0962176322937012 100 5.0992584228515634
		 100.8 5.1032209396362305 101.6 5.1079530715942383 102.4 5.1132564544677743 103.2 5.118837833404541
		 104 5.1243772506713867 104.8 5.1295633316040039 105.6 5.1340165138244629 106.4 5.1372957229614258
		 107.2 5.1389665603637695 108 5.1386551856994629 108.8 5.1359748840332031 109.6 5.1306285858154297
		 110.4 5.122431755065918 111.2 5.1113605499267578 112 5.0974597930908203 112.8 5.0807766914367676
		 113.6 5.0629324913024911 114.4 5.0487642288208008 115.2 5.0155014991760254 116 4.9776740074157715
		 116.8 4.957181453704834 117.6 4.9286060333251953 118.4 4.8971834182739258 119.2 4.8647761344909668
		 120 4.8317484855651855 120.8 4.7986383438110352 121.6 4.7659626007080078 122.4 4.7340607643127441
		 123.2 4.7030830383300781 124 4.6731042861938477 124.8 4.6441993713378906 125.6 4.6180615425109863
		 126.4 4.597867488861084 127.2 4.5581607818603516 128 4.5460081100463867 128.8 4.5098276138305664
		 129.6 4.4934992790222168 130.4 4.4750456809997559 131.2 4.4596452713012695 132 4.4277997016906738
		 132.8 4.4136257171630859 133.6 4.3961372375488281 134.4 4.3787479400634766 135.2 4.3648900985717773
		 136 4.3361392021179199 136.8 4.3270854949951172 137.6 4.2993788719177246 138.4 4.2875332832336426
		 139.2 4.2759079933166504 140 4.2496504783630371 140.8 4.2384696006774902 141.6 4.2254190444946289
		 142.4 4.2128443717956543 143.2 4.2019271850585938 144 4.1927027702331543 144.8 4.1850948333740234
		 145.6 4.1790189743041992 146.4 4.1744732856750488 147.2 4.1715683937072754 148 4.1703858375549316
		 148.8 4.1708793640136719 149.6 4.1728715896606445 150.4 4.1760897636413574 151.2 4.1802477836608887
		 152 4.1851115226745605 152.8 4.1904358863830566 153.6 4.1958394050598145 154.4 4.2008762359619141
		 155.2 4.2050948143005371 156 4.2081074714660645 156.8 4.2097477912902832 157.6 4.209775447845459
		 158.4 4.2066020965576172 159.2 4.2043380737304687 160 4.1935720443725586 160.8 4.1885128021240234
		 161.6 4.167757511138916 162.4 4.1548752784729004 163.2 4.1365766525268555 164 4.11468505859375
		 164.8 4.0901646614074707 165.6 4.063103199005127 166.4 4.0337018966674805 167.2 4.0023689270019531
		 168 3.9695699214935303 168.8 3.935692787170411 169.6 3.9010794162750231 170.4 3.866070032119751
		 171.2 3.8310921192169194 172 3.7966377735137926 172.8 3.7631535530090328 173.6 3.7310516834259033
		 174.4 3.7007350921630855 175.2 3.6726291179656982 176 3.6470096111297616 176.8 3.6239538192749028
		 177.6 3.6034770011901855 178.4 3.5855822563171391 179.2 3.5709841251373291 180 3.5609695911407475
		 180.8 3.5457894802093506;
createNode animCurveTA -n "Bip01_Spine2_rotateZ17";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 227 ".ktv[0:226]"  0 3.2428610324859619 0.8 3.2381560802459717
		 1.6 3.2348935604095459 2.4 3.2337195873260498 3.2 3.2355015277862549 4 3.246990442276001
		 4.8 3.2529392242431641 5.6 3.278800249099731 6.4 3.2954344749450684 7.2 3.3173363208770752
		 8 3.3381562232971191 8.8 3.3919079303741455 9.6 3.420508861541748 10.4 3.4588322639465336
		 11.2 3.5022051334381104 12 3.5480382442474365 12.8 3.5958349704742432 13.6 3.645066738128663
		 14.4 3.6950817108154306 15.2 3.7451078891754155 16 3.794287681579589 16.8 3.8417990207672119
		 17.6 3.8867866992950439 18.4 3.9285154342651367 19.2 3.9664733409881592 20 4.0001621246337891
		 20.8 4.0289759635925293 21.6 4.0516247749328613 22.4 4.0658712387084961 23.2 4.0827741622924805
		 24 4.0886368751525879 24.8 4.0882253646850586 25.6 4.0795750617980957 26.4 4.0602006912231445
		 27.2 4.0455083847045898 28 4.0228509902954102 28.8 3.9949088096618652 29.6 3.9632568359375
		 30.4 3.9311139583587646 31.2 3.9042067527770983 32 3.8433020114898677 32.8 3.8154745101928702
		 33.6 3.7814533710479736 34.4 3.7467033863067627 35.2 3.7141408920288086 36 3.6843054294586177
		 36.8 3.6576788425445574 37.6 3.6346948146820068 38.4 3.6157140731811528 39.2 3.6009900569915771
		 40 3.5908257961273193 40.8 3.5854337215423588 41.6 3.5848612785339355 42.4 3.5888910293579106
		 43.2 3.5973854064941406 44 3.610254287719727 44.8 3.6271576881408691 45.6 3.6475875377655038
		 46.4 3.671089887619019 47.2 3.6972763538360596 48 3.7258961200714116 48.8 3.7565772533416766
		 49.6 3.7889888286590585 50.4 3.8227803707122803 51.2 3.8575944900512695 52 3.8931205272674561
		 52.8 3.9290847778320326 53.6 3.9653139114379887 54.4 4.0016350746154785 55.2 4.0378212928771973
		 56 4.0737757682800293 56.8 4.109492301940918 57.6 4.144890308380127 58.4 4.1797432899475098
		 59.2 4.2137727737426758 60 4.2468099594116211 60.8 4.2787437438964844 61.6 4.3093972206115723
		 62.4 4.3386492729187012 63.2 4.3663654327392578 64 4.3921365737915039 64.8 4.4151272773742676
		 65.6 4.4349818229675293 66.4 4.45184326171875 67.2 4.4656782150268555 68 4.4761357307434082
		 68.8 4.4829521179199219 69.6 4.4860668182373047 70.4 4.4855265617370605 71.2 4.481264591217041
		 72 4.4731121063232422 72.8 4.4610428810119629 73.6 4.4453539848327637 74.4 4.426567554473877
		 75.2 4.4051113128662118 76 4.3813610076904297 76.8 4.355705738067627 77.6 4.3286666870117187
		 78.4 4.3008522987365723 79.2 4.2728619575500488 80 4.2452058792114258 80.8 4.2184758186340332
		 81.6 4.1933841705322266 82.4 4.1706676483154297 83.2 4.1508774757385254 84 4.1349902153015137
		 84.8 4.1247482299804687 85.6 4.1120405197143555 86.4 4.1074037551879883 87.2 4.1081042289733896
		 88 4.1132888793945312 88.8 4.1229314804077148 89.6 4.138524055480957 90.4 4.1647367477416992
		 91.2 4.1821532249450684 92 4.207982063293457 92.8 4.2387022972106934 93.6 4.2730841636657715
		 94.4 4.3111705780029297 95.2 4.3527402877807617 96 4.3973259925842285 96.8 4.4444913864135742
		 97.6 4.4938769340515137 98.4 4.5452351570129395 99.2 4.5983762741088867 100 4.6531362533569336
		 100.8 4.7093629837036133 101.6 4.7667660713195801 102.4 4.8248381614685059 103.2 4.882997989654541
		 104 4.940800666809082 104.8 4.9978480339050293 105.6 5.0536737442016602 106.4 5.1077079772949219
		 107.2 5.1593408584594727 108 5.2080330848693848 108.8 5.2533531188964844 109.6 5.2948403358459473
		 110.4 5.3319764137268066 111.2 5.3644466400146484 112 5.3920035362243652 112.8 5.4145374298095703
		 113.6 5.4315762519836426 114.4 5.4420385360717773 115.2 5.4538245201110849 116 5.4567270278930664
		 116.8 5.4539422988891602 117.6 5.448035717010498 118.4 5.4387335777282715 119.2 5.4265322685241699
		 120 5.412116527557373 120.8 5.3962235450744629 121.6 5.3794231414794931 122.4 5.3622112274169922
		 123.2 5.345005989074707 124 5.3282723426818848 124.8 5.3125910758972168 125.6 5.2991037368774414
		 126.4 5.2894210815429687 127.2 5.2734928131103525 128 5.2696394920349121 128.8 5.2613043785095215
		 129.6 5.2592430114746094 130.4 5.2579617500305176 131.2 5.2577872276306152 132 5.2612838745117187
		 132.8 5.2645325660705566 133.6 5.2694911956787109 134.4 5.2749929428100586 135.2 5.2797656059265137
		 136 5.2906060218811035 136.8 5.2940940856933594 137.6 5.3045916557312012 138.4 5.3087124824523926
		 139.2 5.3124408721923828 140 5.3191251754760742 140.8 5.3209490776062012 141.6 5.3222489356994629
		 142.4 5.3224501609802246 143.2 5.3211855888366699 144 5.3185014724731445 144.8 5.3145952224731445
		 145.6 5.3095736503601074 146.4 5.3034510612487802 147.2 5.2960920333862305 148 5.2873878479003906
		 148.8 5.2772469520568848 149.6 5.2656874656677246 150.4 5.2528824806213379 151.2 5.2391476631164551
		 152 5.2246694564819336 152.8 5.209406852722168 153.6 5.1930837631225586 154.4 5.1754813194274902
		 155.2 5.1565361022949219 156 5.1377372741699219 156.8 5.1235036849975586 157.6 5.092562198638916
		 158.4 5.0606813430786133 159.2 5.0493249893188477 160 5.0088410377502441 160.8 4.994537353515625
		 161.6 4.9470372200012207 162.4 4.9228177070617676 163.2 4.8914675712585449 164 4.856926441192627
		 164.8 4.8214187622070313 165.6 4.7853317260742187 166.4 4.7490177154541016 167.2 4.7126979827880868
		 168 4.6765975952148437 168.8 4.6408524513244629 169.6 4.6056246757507324 170.4 4.5711302757263192
		 171.2 4.5375881195068368 172 4.5052886009216317 172.8 4.4744873046875 173.6 4.4453268051147461
		 174.4 4.4178781509399414 175.2 4.3922128677368164 176 4.3683867454528809 176.8 4.3465571403503427
		 177.6 4.326958179473877 178.4 4.3096771240234375 179.2 4.295351505279541 180 4.2851557731628418
		 180.8 4.2676501274108887;
createNode animCurveTA -n "Bip01_R_Clavicle_rotateX17";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 227 ".ktv[0:226]"  0 0.25855493545532227 0.8 0.25864097476005554
		 1.6 0.25909492373466492 2.4 0.25977426767349243 3.2 0.25997212529182434 4 0.26072260737419128
		 4.8 0.26149663329124451 5.6 0.26171568036079407 6.4 0.26243206858634949 7.2 0.26261153817176819
		 8 0.26302233338356018 8.8 0.26311686635017395 9.6 0.26298478245735168 10.4 0.26283359527587891
		 11.2 0.26250609755516052 12 0.26187688112258911 12.8 0.26091101765632629 13.6 0.26053646206855774
		 14.4 0.25921636819839478 15.2 0.25756931304931641 16 0.25701245665550232 16.8 0.25527158379554749
		 17.6 0.25375267863273621 18.4 0.25228622555732727 19.2 0.2507174015045166 20 0.25020235776901245
		 20.8 0.248798593878746 21.6 0.24800831079483027 22.4 0.24742564558982852 23.2 0.247222900390625
		 24 0.24738986790180206 24.8 0.24816912412643433 25.6 0.24854232370853421 26.4 0.2500976026058197
		 27.2 0.25180193781852722 28 0.25398111343383789 28.8 0.25689974427223206 29.6 0.25794738531112671
		 30.4 0.26139768958091741 31.2 0.26451072096824646 32 0.26750668883323669 32.8 0.27053290605545044
		 33.6 0.2733643651008606 34.4 0.27603918313980103 35.2 0.27856189012527466 36 0.28105875849723816
		 36.8 0.28167229890823364 37.6 0.28342750668525696 38.4 0.28438270092010498 39.2 0.28468623757362366
		 40 0.28449645638465881 40.8 0.28376874327659607 41.6 0.28254681825637817 42.4 0.28085547685623169
		 43.2 0.27874308824539185 44 0.27602154016494751 44.8 0.27225774526596069 45.6 0.27025926113128662
		 46.4 0.26800704002380371 47.2 0.26263999938964844 48 0.26111513376235962 48.8 0.25655162334442139
		 49.6 0.25241580605506897 50.4 0.24779772758483887 51.2 0.24645751714706421 52 0.2419110387563706
		 52.8 0.23820371925830841 53.6 0.23483946919441223 54.4 0.23147402703762057 55.2 0.22815006971359253
		 56 0.22488623857498169 56.8 0.22163684666156769 57.6 0.21833258867263791 58.4 0.21511046588420868
		 59.2 0.21152941882610321 60 0.20732191205024719 60.8 0.20598524808883667 61.6 0.20163999497890472
		 62.4 0.19789138436317444 63.2 0.19432419538497925 64 0.19083534181118011 64.8 0.18724037706851959
		 65.6 0.1838005930185318 66.4 0.1803995668888092 67.2 0.17705003917217255 68 0.17342300713062286
		 68.8 0.17251424491405487 69.6 0.16939330101013184 70.4 0.16708250343799591 71.2 0.16498821973800659
		 72 0.1644919365644455 72.8 0.16321772336959839 73.6 0.16263464093208313 74.4 0.16246375441551208
		 75.2 0.16263224184513092 76 0.16318568587303162 76.8 0.16408127546310425 77.6 0.16515530645847321
		 78.4 0.1665075272321701 79.2 0.1679915189743042 80 0.16955608129501343 80.8 0.17127974331378937
		 81.6 0.17292049527168277 82.4 0.17452745139598846 83.2 0.17612425982952118 84 0.17790812253952026
		 84.8 0.17839661240577698 85.6 0.17998269200325012 86.4 0.18106681108474731 87.2 0.18125621974468231
		 88 0.18204371631145477 88.8 0.18233901262283328 89.6 0.1824486702680588 90.4 0.18226565420627591
		 91.2 0.18203827738761905 92 0.18171398341655731 92.8 0.18129265308380127 93.6 0.18035607039928436
		 94.4 0.17956364154815674 95.2 0.17865578830242157 96 0.17774258553981781 96.8 0.17672440409660342
		 97.6 0.17558068037033081 98.4 0.17413139343261719 99.2 0.17369161546230319 100 0.17229217290878296
		 100.8 0.17100991308689115 101.6 0.16986656188964844 102.4 0.16868108510971069 103.2 0.1673104465007782
		 104 0.16686557233333588 104.8 0.16548360884189606 105.6 0.16512212157249451 106.4 0.16419215500354767
		 107.2 0.16353890299797058 108 0.16308556497097015 108.8 0.16283129155635834 109.6 0.16284863650798798
		 110.4 0.16297520697116852 111.2 0.1635865718126297 112 0.1637888848781586 112.8 0.16498050093650818
		 113.6 0.16620557010173798 114.4 0.16800536215305328 115.2 0.16863249242305756 116 0.17101985216140747
		 116.8 0.17376700043678284 117.6 0.17461791634559631 118.4 0.17767536640167236 119.2 0.18100495636463165
		 120 0.18190854787826535 120.8 0.18525172770023343 121.6 0.18806985020637512 122.4 0.19067609310150144
		 123.2 0.19329193234443665 124 0.19601619243621823 124.8 0.19913597404956815 125.6 0.20008231699466705
		 126.4 0.20308801531791687 127.2 0.20570513606071472 128 0.20860426127910617 128.8 0.20940060913562775
		 129.6 0.21221786737442019 130.4 0.21493285894393921 131.2 0.21570530533790588 132 0.21820832788944244
		 132.8 0.22015257179737091 133.6 0.22195647656917572 134.4 0.22378148138523105 135.2 0.22583588957786563
		 136 0.22657102346420288 136.8 0.22873926162719729 137.6 0.22935248911380768 138.4 0.23090095818042761
		 139.2 0.23207025229930883 140 0.23318518698215485 140.8 0.23346674442291257 141.6 0.23420178890228271
		 142.4 0.23445084691047668 143.2 0.2344339340925217 144 0.23434689640998843 144.8 0.2338625639677048
		 145.6 0.23333239555358887 146.4 0.23252548277378079 147.2 0.23162077367305761 148 0.23051571846008304
		 148.8 0.22921006381511683 149.6 0.22788676619529724 150.4 0.226346105337143 151.2 0.22481608390808103
		 152 0.22319664061069489 152.8 0.22163023054599759 153.6 0.21995481848716736 154.4 0.2183479368686676
		 155.2 0.21651498973369604 156 0.21596881747245789 156.8 0.21442584693431857 157.6 0.21327601373195648
		 158.4 0.21248771250247955 159.2 0.2116374671459198 160 0.21146371960639954 160.8 0.21113421022892004
		 161.6 0.21111808717250824 162.4 0.21140561997890472 163.2 0.21184559166431427 164 0.21256725490093231
		 164.8 0.21367497742176056 165.6 0.21406058967113495 166.4 0.21551676094532016 167.2 0.21693302690982821
		 168 0.21869023144245148 168.8 0.21928852796554563 169.6 0.22130882740020752 170.4 0.22200725972652435
		 171.2 0.22408042848110199 172 0.22466200590133667 172.8 0.22618365287780756 173.6 0.22753885388374326
		 174.4 0.22788199782371524 175.2 0.22887042164802551 176 0.22961191833019257 176.8 0.22976821660995483
		 177.6 0.23016156256198883 178.4 0.23034544289112091 179.2 0.23036345839500427 180 0.23027375340461728
		 180.8 0.23006010055541984;
createNode animCurveTA -n "Bip01_R_Clavicle_rotateY17";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 227 ".ktv[0:226]"  0 79.223533630371094 0.8 79.223342895507813
		 1.6 79.222442626953125 2.4 79.220977783203125 3.2 79.22052001953125 4 79.218955993652358
		 4.8 79.217361450195313 5.6 79.21685791015625 6.4 79.215324401855469 7.2 79.214973449707017
		 8 79.214164733886719 8.8 79.213951110839844 9.6 79.214134216308594 10.4 79.214530944824219
		 11.2 79.215293884277344 12 79.216529846191406 12.8 79.218551635742188 13.6 79.219276428222656
		 14.4 79.222183227539063 15.2 79.225677490234375 16 79.226707458496094 16.8 79.230400085449219
		 17.6 79.233573913574219 18.4 79.23663330078125 19.2 79.240066528320327 20 79.241035461425781
		 20.8 79.243881225585937 21.6 79.245758056640625 22.4 79.24688720703125 23.2 79.247329711914063
		 24 79.246871948242188 24.8 79.245285034179673 25.6 79.244529724121094 26.4 79.241325378417969
		 27.2 79.237686157226562 28 79.233177185058594 28.8 79.226875305175781 29.6 79.224853515625
		 30.4 79.217567443847642 31.2 79.211082458496094 32 79.204734802246094 32.8 79.198463439941406
		 33.6 79.192451477050781 34.4 79.186904907226563 35.2 79.181663513183622 36 79.176307678222656
		 36.8 79.174949645996094 37.6 79.171310424804687 38.4 79.169273376464844 39.2 79.1685791015625
		 40 79.16900634765625 40.8 79.170562744140625 41.6 79.17313385009767 42.4 79.17669677734375
		 43.2 79.181137084960937 44 79.186790466308594 44.8 79.194808959960938 45.6 79.198959350585937
		 46.4 79.203651428222656 47.2 79.214935302734375 48 79.21820068359375 48.8 79.22772216796875
		 49.6 79.2364501953125 50.4 79.24609375 51.2 79.24896240234375 52 79.258430480957017
		 52.8 79.266227722167969 53.6 79.273368835449233 54.4 79.280326843261719 55.2 79.2872314453125
		 56 79.294097900390625 56.8 79.3009033203125 57.6 79.307693481445313 58.4 79.314567565917969
		 59.2 79.322006225585938 60 79.330879211425781 60.8 79.333549499511719 61.6 79.342681884765625
		 62.4 79.3505859375 63.2 79.358016967773437 64 79.365486145019531 64.8 79.372886657714844
		 65.6 79.380149841308594 66.4 79.387100219726562 67.2 79.394126892089844 68 79.40164947509767
		 68.8 79.403732299804688 69.6 79.410102844238281 70.4 79.414970397949219 71.2 79.419288635253906
		 72 79.420364379882813 72.8 79.423042297363281 73.6 79.424362182617188 74.4 79.424674987792983
		 75.2 79.424179077148438 76 79.423011779785156 76.8 79.421234130859375 77.6 79.418937683105469
		 78.4 79.416213989257813 79.2 79.413101196289063 80 79.409751892089844 80.8 79.406288146972656
		 81.6 79.402801513671875 82.4 79.399414062500014 83.2 79.396018981933594 84 79.392311096191406
		 84.8 79.391281127929702 85.6 79.388092041015625 86.4 79.385757446289063 87.2 79.385231018066406
		 88 79.383804321289063 88.8 79.383163452148438 89.6 79.382865905761719 90.4 79.383216857910156
		 91.2 79.383804321289063 92 79.384536743164062 92.8 79.385299682617202 93.6 79.387252807617188
		 94.4 79.388908386230469 95.2 79.390769958496094 96 79.39276123046875 96.8 79.394882202148438
		 97.6 79.397293090820327 98.4 79.400283813476562 99.2 79.401214599609375 100 79.404220581054688
		 100.8 79.406768798828125 101.6 79.409149169921875 102.4 79.411605834960937 103.2 79.414382934570327
		 104 79.415290832519531 104.8 79.418281555175781 105.6 79.419044494628906 106.4 79.421051025390625
		 107.2 79.422401428222656 108 79.423324584960938 108.8 79.423828125 109.6 79.423919677734375
		 110.4 79.423530578613281 111.2 79.422294616699219 112 79.4217529296875 112.8 79.419418334960938
		 113.6 79.416778564453139 114.4 79.412986755371094 115.2 79.411720275878906 116 79.406723022460938
		 116.8 79.400955200195313 117.6 79.39923095703125 118.4 79.392936706542983 119.2 79.385986328125
		 120 79.383979797363281 120.8 79.377021789550781 121.6 79.37115478515625 122.4 79.365631103515625
		 123.2 79.360214233398437 124 79.35453033447267 124.8 79.347908020019531 125.6 79.346000671386719
		 126.4 79.339706420898437 127.2 79.334228515625 128 79.328315734863281 128.8 79.32659912109375
		 129.6 79.3206787109375 130.4 79.314949035644531 131.2 79.313369750976563 132 79.308212280273438
		 132.8 79.304054260253906 133.6 79.30029296875 134.4 79.296432495117187 135.2 79.292098999023438
		 136 79.290657043457017 136.8 79.285987854003906 137.6 79.284713745117188 138.4 79.281455993652358
		 139.2 79.279098510742188 140 79.276779174804687 140.8 79.276191711425781 141.6 79.274658203125
		 142.4 79.274063110351563 143.2 79.274116516113281 144 79.274253845214844 144.8 79.275222778320327
		 145.6 79.276458740234375 146.4 79.278060913085938 147.2 79.280014038085937 148 79.282333374023438
		 148.8 79.284950256347642 149.6 79.287918090820327 150.4 79.291061401367187 151.2 79.294319152832031
		 152 79.297607421875 152.8 79.301025390625 153.6 79.304374694824233 154.4 79.307830810546875
		 155.2 79.311660766601562 156 79.312736511230469 156.8 79.316017150878906 157.6 79.318374633789063
		 158.4 79.320144653320327 159.2 79.321807861328125 160 79.322158813476562 160.8 79.322944641113281
		 161.6 79.322944641113281 162.4 79.322402954101563 163.2 79.321426391601563 164 79.319908142089844
		 164.8 79.317596435546875 165.6 79.3167724609375 166.4 79.313751220703125 167.2 79.310676574707031
		 168 79.307098388671875 168.8 79.305885314941406 169.6 79.301521301269531 170.4 79.300079345703125
		 171.2 79.295806884765625 172 79.29461669921875 172.8 79.291328430175781 173.6 79.288520812988281
		 174.4 79.287796020507813 175.2 79.285682678222656 176 79.284172058105469 176.8 79.283821105957017
		 177.6 79.282974243164062 178.4 79.282676696777344 179.2 79.282638549804688 180 79.282844543457017
		 180.8 79.28326416015625;
createNode animCurveTA -n "Bip01_R_Clavicle_rotateZ17";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 227 ".ktv[0:226]"  0 174.26473999023435 0.8 174.26475524902344
		 1.6 174.26486206054687 2.4 174.26499938964847 3.2 174.2650146484375 4 174.26515197753906
		 4.8 174.26531982421875 5.6 174.26533508300781 6.4 174.26545715332031 7.2 174.2655029296875
		 8 174.26560974121094 8.8 174.26560974121094 9.6 174.26554870605469 10.4 174.26556396484375
		 11.2 174.26553344726562 12 174.265380859375 12.8 174.26519775390625 13.6 174.26509094238281
		 14.4 174.264892578125 15.2 174.26457214355469 16 174.26443481445312 16.8 174.26411437988281
		 17.6 174.26382446289062 18.4 174.26353454589844 19.2 174.26329040527344 20 174.26313781738281
		 20.8 174.26283264160156 21.6 174.26277160644531 22.4 174.26261901855469 23.2 174.26258850097656
		 24 174.26258850097656 24.8 174.26274108886719 25.6 174.26284790039065 26.4 174.26315307617187
		 27.2 174.26345825195312 28 174.26390075683594 28.8 174.264404296875 29.6 174.2646484375
		 30.4 174.26528930664062 31.2 174.26589965820312 32 174.26646423339847 32.8 174.26705932617187
		 33.6 174.267578125 34.4 174.26811218261719 35.2 174.26860046386719 36 174.26904296875
		 36.8 174.26913452148435 37.6 174.26948547363281 38.4 174.2696533203125 39.2 174.26969909667969
		 40 174.2696533203125 40.8 174.26954650878906 41.6 174.26931762695312 42.4 174.26899719238281
		 43.2 174.26860046386719 44 174.26806640625 44.8 174.26737976074219 45.6 174.26698303222656
		 46.4 174.26652526855469 47.2 174.26553344726562 48 174.26525878906253 48.8 174.26437377929687
		 49.6 174.26359558105469 50.4 174.2626953125 51.2 174.26246643066406 52 174.26156616210935
		 52.8 174.2608642578125 53.6 174.26023864746094 54.4 174.25956726074219 55.2 174.2589111328125
		 56 174.25828552246097 56.8 174.25765991210935 57.6 174.25698852539062 58.4 174.25639343261722
		 59.2 174.25567626953125 60 174.25489807128906 60.8 174.25460815429688 61.6 174.25376892089847
		 62.4 174.25306701660156 63.2 174.25236511230469 64 174.25173950195312 64.8 174.25099182128906
		 65.6 174.25035095214844 66.4 174.24964904785156 67.2 174.24899291992187 68 174.2482604980469
		 68.8 174.2481689453125 69.6 174.24749755859375 70.4 174.24705505371097 71.2 174.24662780761719
		 72 174.24653625488281 72.8 174.24629211425781 73.6 174.24623107910156 74.4 174.24617004394531
		 75.2 174.24615478515625 76 174.24626159667969 76.8 174.2464599609375 77.6 174.24665832519531
		 78.4 174.2469482421875 79.2 174.24723815917969 80 174.24751281738281 80.8 174.24790954589844
		 81.6 174.24819946289062 82.4 174.24850463867187 83.2 174.24879455566406 84 174.24916076660156
		 84.8 174.24925231933594 85.6 174.24960327148435 86.4 174.24980163574219 87.2 174.24978637695312
		 88 174.25 88.8 174.25004577636719 89.6 174.25006103515625 90.4 174.25001525878906
		 91.2 174.25 92 174.24995422363281 92.8 174.24983215332031 93.6 174.24966430664062
		 94.4 174.24951171875 95.2 174.24929809570312 96 174.24916076660156 96.8 174.24894714355472
		 97.6 174.24874877929687 98.4 174.24845886230469 99.2 174.24836730957031 100 174.24812316894531
		 100.8 174.24783325195312 101.6 174.24758911132812 102.4 174.24736022949219 103.2 174.2470703125
		 104 174.24696350097656 104.8 174.24671936035156 105.6 174.24667358398435 106.4 174.24650573730469
		 107.2 174.24636840820312 108 174.24627685546875 108.8 174.2462158203125 109.6 174.24627685546875
		 110.4 174.24626159667969 111.2 174.24638366699219 112 174.24638366699219 112.8 174.24667358398435
		 113.6 174.24685668945312 114.4 174.24720764160156 115.2 174.24734497070313 116 174.24781799316406
		 116.8 174.24835205078125 117.6 174.24851989746094 118.4 174.24916076660156 119.2 174.24981689453125
		 120 174.24993896484378 120.8 174.2506103515625 121.6 174.25115966796875 122.4 174.25164794921875
		 123.2 174.25216674804687 124 174.25270080566406 124.8 174.25328063964844 125.6 174.25347900390625
		 126.4 174.25407409667969 127.2 174.25457763671875 128 174.25518798828128 128.8 174.25532531738281
		 129.6 174.255859375 130.4 174.25636291503906 131.2 174.25653076171875 132 174.25704956054687
		 132.8 174.25738525390625 133.6 174.25773620605469 134.4 174.25808715820312 135.2 174.25846862792969
		 136 174.25865173339844 136.8 174.25900268554687 137.6 174.25914001464844 138.4 174.25942993164062
		 139.2 174.25970458984375 140 174.25990295410156 140.8 174.25997924804687 141.6 174.26010131835935
		 142.4 174.2601318359375 143.2 174.2601318359375 144 174.26011657714844 144.8 174.25997924804687
		 145.6 174.25993347167969 146.4 174.25973510742187 147.2 174.25959777832031 148 174.25936889648435
		 148.8 174.25909423828125 149.6 174.2589111328125 150.4 174.25857543945312 151.2 174.25828552246097
		 152 174.25796508789065 152.8 174.2576904296875 153.6 174.25732421875 154.4 174.25704956054687
		 155.2 174.25669860839844 156 174.25656127929687 156.8 174.25627136230469 157.6 174.25604248046875
		 158.4 174.25593566894531 159.2 174.25572204589844 160 174.25569152832031 160.8 174.25564575195312
		 161.6 174.25563049316406 162.4 174.25570678710935 163.2 174.25578308105469 164 174.25592041015628
		 164.8 174.25613403320315 165.6 174.25619506835935 166.4 174.25648498535156 167.2 174.25672912597656
		 168 174.25711059570312 168.8 174.25723266601562 169.6 174.257568359375 170.4 174.25770568847656
		 171.2 174.25814819335935 172 174.25827026367187 172.8 174.25852966308594 173.6 174.25880432128909
		 174.4 174.25886535644531 175.2 174.259033203125 176 174.25920104980469 176.8 174.25920104980469
		 177.6 174.25927734375003 178.4 174.25933837890625 179.2 174.25933837890625 180 174.25932312011719
		 180.8 174.25927734375003;
createNode animCurveTA -n "Bip01_R_UpperArm_rotateX17";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 227 ".ktv[0:226]"  0 -37.423885345458984 0.8 -37.311901092529297
		 1.6 -37.185825347900391 2.4 -37.052814483642578 3.2 -36.968528747558594 4 -37.05609130859375
		 4.8 -37.21173095703125 5.6 -37.167335510253906 6.4 -36.966121673583984 7.2 -36.800628662109382
		 8 -36.665042877197266 8.8 -36.541229248046875 9.6 -36.504905700683594 10.4 -36.540939331054687
		 11.2 -36.501140594482422 12 -36.331539154052734 12.8 -36.130577087402344 13.6 -35.986087799072266
		 14.4 -35.971576690673828 15.2 -36.070003509521491 16 -36.10162353515625 16.8 -35.978530883789063
		 17.6 -35.776691436767578 18.4 -35.570480346679687 19.2 -35.434188842773438 20 -35.435527801513672
		 20.8 -35.538616180419922 21.6 -35.655765533447266 22.4 -35.726211547851563 23.2 -35.705486297607422
		 24 -35.623516082763672 24.8 -35.558132171630859 25.6 -35.541049957275391 26.4 -35.574409484863281
		 27.2 -35.597560882568359 28 -35.564407348632813 28.8 -35.577526092529297 29.6 -35.675117492675781
		 30.4 -35.738853454589844 31.2 -35.759998321533203 32 -35.838600158691406 32.8 -35.998409271240234
		 33.6 -36.180416107177734 34.4 -36.289485931396477 35.2 -36.321479797363288 36 -36.369777679443359
		 36.8 -36.417449951171875 37.6 -36.408622741699219 38.4 -36.458950042724609 39.2 -36.636604309082031
		 40 -36.775859832763672 40.8 -36.787063598632813 41.6 -36.782958984375 42.4 -36.825714111328125
		 43.2 -36.848400115966797 44 -36.811462402343757 44.8 -36.759532928466797 45.6 -36.711265563964851
		 46.4 -36.629158020019531 47.2 -36.486343383789062 48 -36.304943084716797 48.8 -36.167278289794922
		 49.6 -36.107696533203125 50.4 -36.066673278808594 51.2 -36.012908935546875 52 -35.976531982421875
		 52.8 -35.987194061279297 53.6 -36.055568695068359 54.4 -36.157432556152344 55.2 -36.199871063232422
		 56 -36.152801513671875 56.8 -36.043788909912109 57.6 -35.848499298095703 58.4 -35.593852996826172
		 59.2 -35.324546813964844 60 -35.060047149658203 60.8 -34.843441009521491 61.6 -34.721355438232422
		 62.4 -34.691848754882813 63.2 -34.600902557373047 64 -34.260242462158203 64.8 -33.744361877441406
		 65.6 -33.313899993896484 66.4 -33.069759368896484 67.2 -32.997920989990234 68 -33.053146362304688
		 68.8 -33.029544830322266 69.6 -32.843116760253906 70.4 -32.674057006835938 71.2 -32.595123291015625
		 72 -32.527172088623047 72.8 -32.467864990234375 73.6 -32.444038391113281 74.4 -32.377376556396484
		 75.2 -32.232234954833984 76 -32.133399963378906 76.8 -32.181873321533203 77.6 -32.342624664306641
		 78.4 -32.473636627197266 79.2 -32.503467559814453 80 -32.565212249755859 80.8 -32.752914428710937
		 81.6 -32.976287841796875 82.4 -33.129146575927734 83.2 -33.178470611572266 84 -33.126010894775391
		 84.8 -33.077499389648438 85.6 -33.198646545410156 86.4 -33.398166656494141 87.2 -33.482376098632812
		 88 -33.542362213134766 88.8 -33.728710174560547 89.6 -33.971343994140625 90.4 -34.124687194824219
		 91.2 -34.154964447021484 92 -34.166252136230469 92.8 -34.246967315673828 93.6 -34.318954467773438
		 94.4 -34.310138702392578 95.2 -34.303993225097656 96 -34.318019866943359 96.8 -34.398609161376953
		 97.6 -34.377182006835938 98.4 -34.364322662353516 99.2 -34.259777069091797 100 -34.129001617431641
		 100.8 -34.088809967041016 101.6 -34.05865478515625 102.4 -34.019035339355469 103.2 -34.096229553222656
		 104 -34.266929626464844 104.8 -34.331649780273438 105.6 -34.170680999755859 106.4 -33.975791931152344
		 107.2 -34.013710021972656 108 -34.24884033203125 108.8 -34.458965301513672 109.6 -34.487636566162109
		 110.4 -34.382801055908203 111.2 -34.334732055664063 112 -34.366855621337891 112.8 -34.488548278808594
		 113.6 -34.582553863525391 114.4 -34.557392120361328 115.2 -34.541004180908203 116 -34.609710693359375
		 116.8 -34.618537902832031 117.6 -34.557392120361328 118.4 -34.535438537597656 119.2 -34.49786376953125
		 120 -34.432285308837891 120.8 -34.474586486816406 121.6 -34.606117248535156 122.4 -34.7001953125
		 123.2 -34.740852355957031 124 -34.699378967285156 124.8 -34.569614410400391 125.6 -34.459342956542976
		 126.4 -34.398632049560547 127.2 -34.357292175292969 128 -34.339267730712891 128.8 -34.310352325439453
		 129.6 -34.253711700439453 130.4 -34.21234130859375 131.2 -34.197128295898438 132 -34.219978332519538
		 132.8 -34.305973052978516 133.6 -34.367263793945313 134.4 -34.296623229980469 135.2 -34.128349304199219
		 136 -33.968719482421875 136.8 -33.877895355224609 137.6 -33.878231048583984 138.4 -33.993881225585937
		 139.2 -34.128055572509766 140 -34.088809967041016 140.8 -33.934394836425781 141.6 -33.896583557128906
		 142.4 -33.961013793945312 143.2 -34.029159545898437 144 -34.150913238525391 144.8 -34.313796997070313
		 145.6 -34.458290100097656 146.4 -34.559055328369141 147.2 -34.584423065185547 148 -34.606952667236328
		 148.8 -34.663238525390625 149.6 -34.665851593017585 150.4 -34.645839691162109 151.2 -34.677146911621094
		 152 -34.761016845703125 152.8 -34.927776336669922 153.6 -35.127006530761719 154.4 -35.219615936279297
		 155.2 -35.231170654296875 156 -35.308692932128906 156.8 -35.438308715820312 157.6 -35.484004974365234
		 158.4 -35.485458374023438 159.2 -35.588417053222656 160 -35.721996307373047 160.8 -35.763587951660156
		 161.6 -35.789405822753906 162.4 -35.87286376953125 163.2 -35.949573516845703 164 -35.963005065917969
		 164.8 -35.969379425048828 165.6 -36.040950775146477 166.4 -36.128730773925781 167.2 -36.143959045410163
		 168 -36.070892333984375 168.8 -35.971923828125 169.6 -35.969757080078125 170.4 -36.060325622558594
		 171.2 -36.041908264160163 172 -35.860244750976563 172.8 -35.709682464599609 173.6 -35.671661376953125
		 174.4 -35.598884582519531 175.2 -35.438182830810547 176 -35.369010925292969 176.8 -35.468616485595703
		 177.6 -35.568515777587891 178.4 -35.539760589599609 179.2 -35.408496856689453 180 -35.296016693115241
		 180.8 -35.270053863525391;
createNode animCurveTA -n "Bip01_R_UpperArm_rotateY17";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 227 ".ktv[0:226]"  0 -62.479930877685547 0.8 -62.51922607421875
		 1.6 -62.592880249023438 2.4 -62.65953063964843 3.2 -62.671478271484375 4 -62.590927124023438
		 4.8 -62.488044738769531 5.6 -62.495086669921875 6.4 -62.579509735107422 7.2 -62.630073547363274
		 8 -62.660377502441406 8.8 -62.703113555908189 9.6 -62.697071075439453 10.4 -62.609336853027344
		 11.2 -62.547687530517578 12 -62.592746734619155 12.8 -62.674510955810554 13.6 -62.714008331298842
		 14.4 -62.688301086425781 15.2 -62.621109008789063 16 -62.566318511962891 16.8 -62.529769897460937
		 17.6 -62.509422302246101 18.4 -62.54901123046875 19.2 -62.622707366943359 20 -62.619232177734375
		 20.8 -62.522098541259766 21.6 -62.426456451416016 22.4 -62.387783050537102 23.2 -62.400169372558594
		 24 -62.427913665771477 24.8 -62.444412231445312 25.6 -62.465339660644538 26.4 -62.496650695800781
		 27.2 -62.527851104736335 28 -62.540992736816399 28.8 -62.523437499999993 29.6 -62.530891418457031
		 30.4 -62.603981018066406 31.2 -62.658164978027344 32 -62.629177093505866 32.8 -62.590183258056641
		 33.6 -62.582519531249993 34.4 -62.563442230224616 35.2 -62.544494628906257 36 -62.54647064208983
		 36.8 -62.55719375610353 37.6 -62.589023590087898 38.4 -62.611103057861335 39.2 -62.56452941894532
		 40 -62.495594024658203 40.8 -62.48279953002929 41.6 -62.484138488769531 42.4 -62.419933319091797
		 43.2 -62.321926116943359 44 -62.263221740722656 44.8 -62.228107452392571 45.6 -62.148078918457031
		 46.4 -62.015800476074219 47.2 -61.91114807128907 48 -61.88396072387696 48.8 -61.881935119628906
		 49.6 -61.84840393066407 50.4 -61.809761047363288 51.2 -61.84913635253907 52 -61.997413635253899
		 52.8 -62.157176971435547 53.6 -62.193820953369141 54.4 -62.103046417236335 55.2 -62.051738739013672
		 56 -62.157409667968757 56.8 -62.337261199951165 57.6 -62.51347351074218 58.4 -62.700920104980469
		 59.2 -62.854873657226562 60 -62.909729003906243 60.8 -62.943092346191406 61.6 -63.029911041259759
		 62.4 -63.089252471923835 63.2 -63.049343109130845 64 -62.882537841796875 64.8 -62.658382415771491
		 65.6 -62.539005279541009 66.4 -62.520412445068352 67.2 -62.512748718261719 68 -62.463809967041016
		 68.8 -62.335369110107415 69.6 -62.191490173339851 70.4 -62.068172454833977 71.2 -61.920860290527344
		 72 -61.801605224609382 72.8 -61.768115997314446 73.6 -61.784870147705078 74.4 -61.830406188964858
		 75.2 -61.882999420166016 76 -61.912021636962884 76.8 -61.919857025146477 77.6 -61.901760101318352
		 78.4 -61.893455505371094 79.2 -61.934909820556648 80 -61.97136306762696 80.8 -61.962509155273445
		 81.6 -61.939155578613281 82.4 -61.928169250488274 83.2 -61.953891754150398 84 -62.043251037597656
		 84.8 -62.130886077880866 85.6 -62.134876251220703 86.4 -62.127109527587891 87.2 -62.159309387207031
		 88 -62.133659362792962 88.8 -62.015224456787109 89.6 -61.885459899902337 90.4 -61.81596755981446
		 91.2 -61.824104309082031 92 -61.862407684326179 92.8 -61.873668670654297 93.6 -61.869426727294922
		 94.4 -61.860328674316399 95.2 -61.80925369262696 96 -61.742221832275391 96.8 -61.734420776367188
		 97.6 -61.73626708984375 98.4 -61.720455169677741 99.2 -61.736335754394531 100 -61.764411926269531
		 100.8 -61.756298065185554 101.6 -61.784957885742187 102.4 -61.868251800537116 103.2 -61.901145935058587
		 104 -61.855987548828125 104.8 -61.82152175903321 105.6 -61.860172271728516 106.4 -61.906105041503906
		 107.2 -61.865543365478523 108 -61.752510070800781 108.8 -61.671653747558594 109.6 -61.694492340087891
		 110.4 -61.767658233642578 111.2 -61.838016510009766 112 -61.841136932373047 112.8 -61.809848785400391
		 113.6 -61.791065216064453 114.4 -61.824287414550774 115.2 -61.848655700683594 116 -61.847774505615227
		 116.8 -61.895709991455085 117.6 -61.972434997558594 118.4 -62.012569427490227 119.2 -62.043754577636726
		 120 -62.089447021484375 120.8 -62.113258361816399 121.6 -62.099735260009759 122.4 -62.065052032470696
		 123.2 -62.034458160400391 124 -62.040245056152344 124.8 -62.070713043212891 125.6 -62.093914031982422
		 126.4 -62.130046844482429 127.2 -62.177860260009773 128 -62.208408355712891 128.8 -62.249397277832024
		 129.6 -62.310050964355462 130.4 -62.343109130859375 131.2 -62.345172882080085 132 -62.328578948974609
		 132.8 -62.279151916503913 133.6 -62.246639251708984 134.4 -62.294750213623047 135.2 -62.400535583496094
		 136 -62.507190704345703 136.8 -62.564937591552734 137.6 -62.559406280517578 138.4 -62.510456085205078
		 139.2 -62.455902099609375 140 -62.473270416259759 140.8 -62.55926513671875 141.6 -62.609233856201165
		 142.4 -62.595607757568366 143.2 -62.550525665283203 144 -62.487312316894531 144.8 -62.425037384033203
		 145.6 -62.360511779785149 146.4 -62.306449890136719 147.2 -62.307544708251946 148 -62.336639404296875
		 148.8 -62.358757019042969 149.6 -62.382663726806634 150.4 -62.381084442138679 151.2 -62.353748321533203
		 152 -62.33708572387696 152.8 -62.316272735595703 153.6 -62.285675048828118 154.4 -62.2662353515625
		 155.2 -62.245605468750007 156 -62.227767944335945 156.8 -62.236003875732415 157.6 -62.261528015136726
		 158.4 -62.260292053222656 159.2 -62.200950622558594 160 -62.147785186767571 160.8 -62.166042327880859
		 161.6 -62.200267791748047 162.4 -62.19739913940429 163.2 -62.198600769042969 164 -62.238380432128899
		 164.8 -62.266502380371087 165.6 -62.233367919921868 166.4 -62.176288604736335 167.2 -62.162742614746101
		 168 -62.220355987548835 168.8 -62.309143066406257 169.6 -62.329502105712891 170.4 -62.271007537841818
		 171.2 -62.261188507080078 172 -62.332588195800781 172.8 -62.379547119140632 173.6 -62.365810394287116
		 174.4 -62.363296508789063 175.2 -62.412822723388679 176 -62.429729461669922 176.8 -62.363361358642571
		 177.6 -62.316375732421882 178.4 -62.337493896484375 179.2 -62.367675781249993 180 -62.372695922851562
		 180.8 -62.343948364257813;
createNode animCurveTA -n "Bip01_R_UpperArm_rotateZ17";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 227 ".ktv[0:226]"  0 9.1595401763916016 0.8 8.9867677688598633
		 1.6 8.7682905197143555 2.4 8.5520963668823242 3.2 8.4564380645751953 4 8.6878890991210937
		 4.8 9.0413703918457031 5.6 9.0210552215576172 6.4 8.7149772644042969 7.2 8.4940252304077148
		 8 8.3387670516967773 8.8 8.1951417922973633 9.6 8.2236747741699237 10.4 8.4314107894897461
		 11.2 8.5148677825927734 12 8.3253068923950195 12.8 8.0614833831787109 13.6 7.9176955223083487
		 14.4 8.0192031860351562 15.2 8.3191204071044922 16 8.5128278732299805 16.8 8.4663763046264648
		 17.6 8.2844552993774414 18.4 8.044520378112793 19.2 7.8867073059082031 20 7.9915828704833949
		 20.8 8.3086309432983398 21.6 8.6322708129882812 22.4 8.8289909362792969 23.2 8.8404502868652344
		 24 8.7411651611328125 24.8 8.662785530090332 25.6 8.6351566314697266 26.4 8.6590423583984375
		 27.2 8.6579265594482422 28 8.5790119171142578 28.8 8.5805845260620117 29.6 8.6719951629638672
		 30.4 8.6495895385742187 31.2 8.5759048461914062 32 8.6511116027832031 32.8 8.8437681198120117
		 33.6 9.0321788787841797 34.4 9.1208057403564453 35.2 9.0997934341430664 36 9.088007926940918
		 36.8 9.0680065155029297 37.6 8.9443588256835938 38.4 8.9150028228759766 39.2 9.1360864639282227
		 40 9.3291587829589844 40.8 9.2958564758300781 41.6 9.2351713180541992 42.4 9.3045625686645508
		 43.2 9.3822116851806641 44 9.3526916503906232 44.8 9.2941637039184553 45.6 9.2872400283813477
		 46.4 9.2823429107666016 47.2 9.1756772994995117 48 8.9557352066040039 48.8 8.7848987579345703
		 49.6 8.7687368392944336 50.4 8.8042011260986328 51.2 8.7657079696655273 52 8.6625413894653338
		 52.8 8.626068115234375 53.6 8.7913923263549805 54.4 9.1278591156005859 55.2 9.3566465377807617
		 56 9.3261260986328125 56.8 9.1473608016967773 57.6 8.8564586639404297 58.4 8.4792881011962891
		 59.2 8.1154775619506836 60 7.8456387519836452 60.8 7.6627469062805167 61.6 7.5675911903381339
		 62.4 7.6270103454589844 63.2 7.6838130950927725 64 7.4939746856689462 64.8 7.0964326858520508
		 65.6 6.7181954383850098 66.4 6.5107011795043945 67.2 6.534721851348877 68 6.7700285911560059
		 68.8 6.952143669128418 69.6 6.901984691619873 70.4 6.845090389251709 71.2 6.9274511337280273
		 72 6.9925985336303711 72.8 6.9854764938354492 73.6 6.9722771644592285 74.4 6.856231689453125
		 75.2 6.604720115661622 76 6.427405834197998 76.8 6.4679226875305176 77.6 6.6749916076660165
		 78.4 6.8102364540100098 79.2 6.737335205078125 80 6.6979641914367676 80.8 6.8616743087768555
		 81.6 7.0704612731933594 82.4 7.1520514488220215 83.2 7.0406780242919922 84 6.7154603004455566
		 84.8 6.3891048431396484 85.6 6.3756790161132812 86.4 6.4805898666381836 87.2 6.3781952857971191
		 88 6.2874693870544434 88.8 6.4624209403991699 89.6 6.7363543510437012 90.4 6.8369503021240234
		 91.2 6.6988773345947266 92 6.5158839225769043 92.8 6.4696135520935059 93.6 6.4386577606201172
		 94.4 6.3084855079650879 95.2 6.2371301651000977 96 6.2387990951538086 96.8 6.2280092239379883
		 97.6 6.1441092491149902 98.4 6.0825190544128418 99.2 5.8641128540039062 100 5.6043949127197266
		 100.8 5.5123572349548349 101.6 5.4066963195800781 102.4 5.2422957420349121 103.2 5.2915191650390625
		 104 5.5432448387145996 104.8 5.6310563087463379 105.6 5.3290414810180664 106.4 4.9730720520019531
		 107.2 5.0304865837097168 108 5.4376120567321777 108.8 5.7818293571472168 109.6 5.7755188941955566
		 110.4 5.5387496948242196 111.2 5.3666834831237793 112 5.4006829261779785 112.8 5.5974645614624032
		 113.6 5.7517185211181641 114.4 5.7005305290222177 115.2 5.6831855773925781 116 5.823915958404541
		 116.8 5.8458461761474618 117.6 5.7497034072875977 118.4 5.7516970634460449 119.2 5.7506294250488281
		 120 5.707371711730957 120.8 5.8466243743896484 121.6 6.1524286270141602 122.4 6.4271621704101563
		 123.2 6.6235847473144531 124 6.668910026550293 124.8 6.5629754066467285 125.6 6.4881677627563477
		 126.4 6.4698476791381836 127.2 6.4656343460083008 128 6.5056672096252441 128.8 6.5147004127502441
		 129.6 6.4580206871032724 130.4 6.4366307258605957 131.2 6.4716629981994629 132 6.5746045112609872
		 132.8 6.7927536964416504 133.6 6.953498363494873 134.4 6.8476700782775888 135.2 6.5442399978637695
		 136 6.2441115379333496 136.8 6.0785870552062988 137.6 6.0937952995300293 138.4 6.3043408393859863
		 139.2 6.537592887878418 140 6.4553160667419434 140.8 6.1444635391235352 141.6 6.0229506492614746
		 142.4 6.0914716720581055 143.2 6.1822857856750488 144 6.3577165603637695 144.8 6.5850057601928711
		 145.6 6.7833957672119141 146.4 6.9066739082336426 147.2 6.8682188987731934 148 6.7956347465515137
		 148.8 6.7742476463317871 149.6 6.6704368591308594 150.4 6.5517802238464355 151.2 6.5244207382202148
		 152 6.5594620704650888 152.8 6.7158021926879883 153.6 6.9268689155578613 154.4 6.9756813049316406
		 155.2 6.9106488227844238 156 6.9413738250732422 156.8 7.0322890281677255 157.6 6.9971389770507804
		 158.4 6.9258713722229004 159.2 7.0520877838134766 160 7.2240686416626012 160.8 7.2135372161865261
		 161.6 7.1765527725219718 162.4 7.2622904777526847 163.2 7.3428635597228995 164 7.3075265884399414
		 164.8 7.2799148559570321 165.6 7.4074463844299308 166.4 7.5880255699157715 167.2 7.632305622100831
		 168 7.4912872314453116 168.8 7.2908935546874991 169.6 7.292405605316163 170.4 7.4952578544616717
		 171.2 7.499222755432128 172 7.202885627746582 172.8 6.9792318344116211 173.6 6.9714703559875488
		 174.4 6.9030184745788574 175.2 6.6640048027038574 176 6.5862946510314941 176.8 6.8253278732299805
		 177.6 7.0514621734619141 178.4 7.0362915992736816 179.2 6.8680005073547363 180 6.7502031326293945
		 180.8 6.7868843078613281;
createNode animCurveTA -n "Bip01_R_Forearm_rotateX17";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 227 ".ktv[0:226]"  0 -0.16732057929038999 0.8 -0.16583430767059326
		 1.6 -0.16279233992099762 2.4 -0.15921074151992798 3.2 -0.15750753879547119 4 -0.16135787963867188
		 4.8 -0.16708578169345856 5.6 -0.16567137837409973 6.4 -0.15904575586318973 7.2 -0.15494987368583679
		 8 -0.1530788391828537 8.8 -0.15102490782737732 9.6 -0.15123972296714783 10.4 -0.1551126092672348
		 11.2 -0.15761308372020719 12 -0.15554934740066528 12.8 -0.15247443318367004 13.6 -0.1511952131986618
		 14.4 -0.15295937657356262 15.2 -0.1574070006608963 16 -0.16068595647811892 16.8 -0.16173478960990906
		 17.6 -0.16214528679847717 18.4 -0.16046673059463501 19.2 -0.1575988233089447 20 -0.15889061987400055
		 20.8 -0.16529534757137301 21.6 -0.17235545814037323 22.4 -0.17665456235408783 23.2 -0.17753513157367706
		 24 -0.17672769725322723 24.8 -0.17519374191761017 25.6 -0.17203287780284882 26.4 -0.16838368773460388
		 27.2 -0.16587355732917786 28 -0.16604727506637573 28.8 -0.16919229924678802 29.6 -0.17038674652576449
		 30.4 -0.16662350296974182 31.2 -0.16303020715713501 32 -0.16372695565223694 32.8 -0.16604213416576383
		 33.6 -0.16775558888912201 34.4 -0.16866557300090793 35.2 -0.16744233667850494 36 -0.16499073803424835
		 36.8 -0.16274614632129669 37.6 -0.16012252867221832 38.4 -0.15930368006229401 39.2 -0.16199158132076263
		 40 -0.16195172071456909 40.8 -0.15961048007011414 41.6 -0.15673704445362091 42.4 -0.15694810450077057
		 43.2 -0.15844522416591644 44 -0.15807038545608521 44.8 -0.15642641484737396 45.6 -0.15564936399459842
		 46.4 -0.15558721125125885 47.2 -0.15503521263599396 48 -0.1553213894367218 48.8 -0.15406669676303866
		 49.6 -0.15252779424190521 50.4 -0.15268579125404361 51.2 -0.1527734249830246 52 -0.15199014544487
		 52.8 -0.15281827747821808 53.6 -0.15776966512203217 54.4 -0.16557998955249786 55.2 -0.17125003039836884
		 56 -0.17267774045467377 56.8 -0.17225009202957153 57.6 -0.17051967978477478 58.4 -0.16757303476333618
		 59.2 -0.16440355777740481 60 -0.16154874861240387 60.8 -0.15876594185829165 61.6 -0.15678562223911283
		 62.4 -0.15755875408649445 63.2 -0.15952660143375397 64 -0.15941506624221802 64.8 -0.15599806606769562
		 65.6 -0.15131950378417969 66.4 -0.14913918077945709 67.2 -0.15059594810009003 68 -0.1546979546546936
		 68.8 -0.1577795147895813 69.6 -0.15634198486804962 70.4 -0.15391987562179563 71.2 -0.15419018268585205
		 72 -0.15443280339241028 72.8 -0.15253978967666626 73.6 -0.14947539567947388 74.4 -0.14436715841293338
		 75.2 -0.13705427944660187 76 -0.13094967603683472 76.8 -0.12899008393287659 77.6 -0.13091364502906799
		 78.4 -0.132386714220047 79.2 -0.13047276437282562 80 -0.1287863701581955 80.8 -0.12975738942623138
		 81.6 -0.13095597922801971 82.4 -0.1298290491104126 83.2 -0.12559279799461365 84 -0.11884337663650511
		 84.8 -0.11334943771362305 85.6 -0.11316984891891478 86.4 -0.11432521045207977 87.2 -0.11132451146841048
		 88 -0.1079876497387886 88.8 -0.10935299098491667 89.6 -0.11323978006839751 90.4 -0.11424176394939425
		 91.2 -0.11090189218521118 92 -0.10787563771009444 92.8 -0.10865679383277892 93.6 -0.11062468588352205
		 94.4 -0.1113165095448494 95.2 -0.11195544898509978 96 -0.11220259219408038 96.8 -0.11043118685483932
		 97.6 -0.10892380774021149 98.4 -0.10941212624311449 99.2 -0.10893868654966354 100 -0.10772456973791124
		 100.8 -0.10833333432674408 101.6 -0.10810030996799468 102.4 -0.10618530213832857
		 103.2 -0.1068507954478264 104 -0.11055859178304672 104.8 -0.11264576762914662 105.6 -0.11012966185808182
		 106.4 -0.10697216540575027 107.2 -0.10866323113441469 108 -0.11476148664951324 108.8 -0.12009108811616898
		 109.6 -0.12056849151849744 110.4 -0.11819146573543549 111.2 -0.11691431701183319
		 112 -0.11779406666755676 112.8 -0.12052576243877412 113.6 -0.12218914180994038 114.4 -0.12073197960853578
		 115.2 -0.120416097342968 116 -0.1225990504026413 116.8 -0.12233985960483552 117.6 -0.11986946314573288
		 118.4 -0.11893985420465469 119.2 -0.11849371343851089 120 -0.11882671713829041 120.8 -0.12157135456800458
		 121.6 -0.12600773572921753 122.4 -0.12913858890533447 123.2 -0.13053816556930542
		 124 -0.12932267785072327 124.8 -0.12631422281265259 125.6 -0.12465797364711766 126.4 -0.12403805553913118
		 127.2 -0.12329541891813275 128 -0.12308192253112793 128.8 -0.12252648919820786 129.6 -0.1210632547736168
		 130.4 -0.12019190192222595 131.2 -0.12036898732185364 132 -0.12182472646236418 132.8 -0.12491711974143982
		 133.6 -0.1266493946313858 134.4 -0.12374734878540042 135.2 -0.11763577163219453 136 -0.11171844601631165
		 136.8 -0.10846611857414246 137.6 -0.10885939002037048 138.4 -0.11196617782115936
		 139.2 -0.11401507258415222 140 -0.11074132472276688 140.8 -0.1054995059967041 141.6 -0.10495565086603165
		 142.4 -0.1070219948887825 143.2 -0.10738625377416612 144 -0.10819968581199647 144.8 -0.11148592829704286
		 145.6 -0.11585810780525209 146.4 -0.11888985335826872 147.2 -0.11920399963855743
		 148 -0.11877131462097168 148.8 -0.11804810166358948 149.6 -0.11561108380556109 150.4 -0.11373375356197357
		 151.2 -0.11350910365581512 152 -0.11342060565948488 152.8 -0.1141702756285667 153.6 -0.11676751822233201
		 154.4 -0.11868217587471008 155.2 -0.12038973718881607 156 -0.12321252375841138 156.8 -0.1259370893239975
		 157.6 -0.1268523782491684 158.4 -0.12703830003738403 159.2 -0.12865874171257019 160 -0.12962990999221802
		 160.8 -0.12789422273635864 161.6 -0.12656359374523163 162.4 -0.12913975119590759
		 163.2 -0.13038934767246246 164 -0.12870566546916962 164.8 -0.12773926556110382 165.6 -0.1292538195848465
		 166.4 -0.13233628869056702 167.2 -0.1337813138961792 168 -0.13220009207725525 168.8 -0.12945950031280518
		 169.6 -0.13007631897926333 170.4 -0.13415594398975372 171.2 -0.13488700985908508
		 172 -0.13059066236019137 172.8 -0.12726637721061709 173.6 -0.12731389701366425 174.4 -0.12737911939620972
		 175.2 -0.12566140294075012 176 -0.12611435353755951 176.8 -0.13091854751110077 177.6 -0.13525797426700592
		 178.4 -0.13632570207118988 179.2 -0.13632786273956299 180 -0.13677507638931274 180.8 -0.13777081668376925;
createNode animCurveTA -n "Bip01_R_Forearm_rotateY17";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 227 ".ktv[0:226]"  0 0.98743230104446411 0.8 0.97797930240631092
		 1.6 0.96901434659957875 2.4 0.96225172281265259 3.2 0.96065044403076183 4 0.97153747081756592
		 4.8 0.98672032356262207 5.6 0.98574352264404286 6.4 0.97179126739501953 7.2 0.96205842494964611
		 8 0.95784175395965587 8.8 0.95507723093032837 9.6 0.95844548940658558 10.4 0.96904832124710072
		 11.2 0.97422462701797485 12 0.96717053651809703 12.8 0.95692074298858643 13.6 0.95119726657867432
		 14.4 0.95501911640167225 15.2 0.96799933910369873 16 0.97839164733886719 16.8 0.98119324445724476
		 17.6 0.97994351387023926 18.4 0.97268891334533691 19.2 0.96264541149139415 20 0.96258479356765714
		 20.8 0.97444063425064076 21.6 0.98784762620925937 22.4 0.99599027633666981 23.2 0.99790799617767323
		 24 0.99717020988464355 24.8 0.99620866775512673 25.6 0.99345588684082031 26.4 0.98913258314132702
		 27.2 0.98272317647933971 28 0.97809541225433372 28.8 0.981542408466339 29.6 0.98543190956115712
		 30.4 0.98109865188598644 31.2 0.97636324167251587 32 0.97851377725601196 32.8 0.9825417399406432
		 33.6 0.98602747917175293 34.4 0.98917198181152344 35.2 0.98802024126052879 36 0.98469388484954845
		 36.8 0.98161017894744873 37.6 0.97500079870223999 38.4 0.97030335664749134 39.2 0.97465395927429188
		 40 0.97903853654861439 40.8 0.97534143924713168 41.6 0.96911895275115989 42.4 0.9676603674888612
		 43.2 0.96735858917236317 44 0.96325200796127297 44.8 0.95975166559219349 45.6 0.96185505390167247
		 46.4 0.96527940034866344 47.2 0.96495848894119263 48 0.96180212497711182 48.8 0.95914930105209351
		 49.6 0.95854187011718761 50.4 0.95836931467056274 51.2 0.95710712671279907 52 0.95363152027130149
		 52.8 0.95441484451293956 53.6 0.96397644281387329 54.4 0.97823286056518555 55.2 0.98761016130447388
		 56 0.99058151245117176 56.8 0.99206101894378673 57.6 0.98986160755157493 58.4 0.98325890302658081
		 59.2 0.97678244113922108 60 0.97268396615982045 60.8 0.96917271614074707 61.6 0.96717447042465188
		 62.4 0.97068923711776767 63.2 0.97448939085006703 64 0.97074848413467396 64.8 0.9588005542755127
		 65.6 0.94506406784057617 66.4 0.93877297639846813 67.2 0.94216948747634865 68 0.95271104574203491
		 68.8 0.96116518974304188 69.6 0.95819991827011108 70.4 0.95190793275833119 71.2 0.95207464694976796
		 72 0.95312243700027455 72.8 0.95036220550537109 73.6 0.94638329744338978 74.4 0.93899035453796398
		 75.2 0.92688608169555653 76 0.91639256477355968 76.8 0.91299879550933838 77.6 0.91696500778198242
		 78.4 0.91949909925460815 79.2 0.91412055492401112 80 0.90988051891326927 80.8 0.9134249687194822
		 81.6 0.91889286041259777 82.4 0.91900640726089466 83.2 0.91070568561553966 84 0.89432400465011597
		 84.8 0.878495693206787 85.6 0.87465697526931763 86.4 0.87415581941604614 87.2 0.86344492435455333
		 88 0.85281378030776978 88.8 0.85524487495422363 89.6 0.86455619335174561 90.4 0.86768829822540283
		 91.2 0.86131757497787476 92 0.8553500771522522 92.8 0.85628026723861694 93.6 0.85728037357330333
		 94.4 0.85537827014923107 95.2 0.85688412189483643 96 0.86057114601135243 96.8 0.85990571975707997
		 97.6 0.8594127893447876 98.4 0.86270850896835349 99.2 0.86130988597869873 100 0.85696679353713989
		 100.8 0.85808497667312622 101.6 0.85696142911911011 102.4 0.85118967294692993 103.2 0.85377174615859985
		 104 0.86575120687484741 104.8 0.8722045421600344 105.6 0.86366361379623413 106.4 0.85235738754272461
		 107.2 0.85670888423919678 108 0.87566012144088756 108.8 0.89188760519027732 109.6 0.89206194877624512
		 110.4 0.88162958621978771 111.2 0.87361425161361694 112 0.873859703540802 112.8 0.88271617889404308
		 113.6 0.89057546854019165 114.4 0.8890567421913147 115.2 0.88792681694030773 116 0.89169275760650635
		 116.8 0.88918775320053101 117.6 0.88276445865631104 118.4 0.88226598501205444 119.2 0.88251531124115012
		 120 0.88029313087463379 120.8 0.88372433185577393 121.6 0.89207679033279419 122.4 0.9002455472946167
		 123.2 0.90681886672973644 124 0.90697723627090454 124.8 0.90113604068756115 125.6 0.89769029617309581
		 126.4 0.89586985111236561 127.2 0.89229536056518555 128 0.88934540748596203 128.8 0.88621538877487183
		 129.6 0.88233625888824463 130.4 0.88078457117080688 131.2 0.88110643625259399 132 0.88371962308883667
		 132.8 0.89124763011932373 133.6 0.89645141363143921 134.4 0.88991862535476685 135.2 0.87506920099258412
		 136 0.86151427030563354 136.8 0.85509061813354492 137.6 0.85700827836990356 138.4 0.86596208810806274
		 139.2 0.87436240911483765 140 0.86989778280258179 140.8 0.85701161623001099 141.6 0.85279840230941772
		 142.4 0.85657083988189697 143.2 0.85997986793518066 144 0.86547756195068348 144.8 0.87355130910873413
		 145.6 0.88148003816604603 146.4 0.88687831163406372 147.2 0.88689887523651134 148 0.88549548387527477
		 148.8 0.8832244873046875 149.6 0.87652790546417247 150.4 0.87236303091049205 151.2 0.87415546178817749
		 152 0.87648582458496094 152.8 0.88167852163314819 153.6 0.88797694444656372 154.4 0.88971340656280506
		 155.2 0.88851821422576893 156 0.89010512828826915 156.8 0.89311355352401745 157.6 0.89296513795852661
		 158.4 0.89238232374191295 159.2 0.89769542217254628 160 0.90259867906570446 160.8 0.89985209703445435
		 161.6 0.8967888355255127 162.4 0.89937543869018544 163.2 0.90176546573638905 164 0.89911687374114979
		 164.8 0.89819663763046287 165.6 0.90430533885955811 166.4 0.9137147068977356 167.2 0.91761922836303711
		 168 0.91297245025634766 168.8 0.90498077869415283 169.6 0.90575188398361217 170.4 0.91636991500854492
		 171.2 0.92037177085876476 172 0.91343218088150013 172.8 0.90863871574401867 173.6 0.90982639789581299
		 174.4 0.90817314386367776 175.2 0.90040808916091908 176 0.89881324768066417 176.8 0.91040641069412231
		 177.6 0.92170208692550659 178.4 0.92351496219635021 179.2 0.92099076509475708 180 0.91973412036895741
		 180.8 0.92172169685363758;
createNode animCurveTA -n "Bip01_R_Forearm_rotateZ17";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 227 ".ktv[0:226]"  0 -17.194334030151367 0.8 -16.950254440307617
		 1.6 -16.701543807983398 2.4 -16.499895095825195 3.2 -16.442970275878906 4 -16.746618270874023
		 4.8 -17.174018859863281 5.6 -17.135395050048828 6.4 -16.728324890136719 7.2 -16.450199127197266
		 8 -16.328666687011719 8.8 -16.240726470947266 9.6 -16.32464599609375 10.4 -16.622081756591797
		 11.2 -16.773372650146484 12 -16.580694198608398 12.8 -16.300027847290039 13.6 -16.147964477539063
		 14.4 -16.258909225463867 15.2 -16.62000846862793 16 -16.905845642089844 16.8 -16.984560012817383
		 17.6 -16.958612442016605 18.4 -16.765283584594727 19.2 -16.491998672485352 20 -16.504484176635742
		 20.8 -16.859088897705078 21.6 -17.258293151855469 22.4 -17.501077651977539 23.2 -17.556573867797852
		 24 -17.530023574829102 24.8 -17.489946365356445 25.6 -17.389509201049805 26.4 -17.246059417724609
		 27.2 -17.064872741699219 28 -16.955583572387695 28.8 -17.07264518737793 29.6 -17.17864990234375
		 30.4 -17.033624649047852 31.2 -16.881166458129883 32 -16.940170288085937 32.8 -17.062082290649414
		 33.6 -17.164173126220703 34.4 -17.249917984008789 35.2 -17.209009170532227 36 -17.103237152099609
		 36.8 -17.00511360168457 37.6 -16.817600250244141 38.4 -16.695444107055664 39.2 -16.828556060791016
		 40 -16.934539794921875 40.8 -16.8206787109375 41.6 -16.640102386474609 42.4 -16.606952667236328
		 43.2 -16.615186691284183 44 -16.51182746887207 44.8 -16.409946441650391 45.6 -16.452638626098633
		 46.4 -16.535276412963867 47.2 -16.52177619934082 48 -16.447811126708984 48.8 -16.370447158813477
		 49.6 -16.340120315551758 50.4 -16.337314605712891 51.2 -16.307500839233398 52 -16.215248107910156
		 52.8 -16.242879867553714 53.6 -16.526145935058594 54.4 -16.953336715698242 55.2 -17.240537643432621
		 56 -17.327436447143555 56.8 -17.358198165893555 57.6 -17.286272048950195 58.4 -17.095710754394531
		 59.2 -16.905794143676758 60 -16.776462554931641 60.8 -16.662431716918945 61.6 -16.593364715576172
		 62.4 -16.686676025390625 63.2 -16.799068450927734 64 -16.707632064819336 64.8 -16.382347106933597
		 65.6 -16.001039505004883 66.4 -15.82567024230957 67.2 -15.923427581787108 68 -16.221401214599609
		 68.8 -16.458171844482422 69.6 -16.371547698974609 70.4 -16.193626403808594 71.2 -16.200868606567383
		 72 -16.228614807128906 72.8 -16.141853332519531 73.6 -16.013324737548828 74.4 -15.781463623046882
		 75.2 -15.413589477539066 76 -15.097526550292969 76.8 -14.995384216308592 77.6 -15.110969543457031
		 78.4 -15.187078475952147 79.2 -15.036797523498535 80 -14.916396141052246 80.8 -15.012693405151369
		 81.6 -15.158895492553713 82.4 -15.151473999023438 83.2 -14.907515525817871 84 -14.440812110900882
		 84.8 -13.99960517883301 85.6 -13.902663230895996 86.4 -13.900862693786619 87.2 -13.608523368835447
		 88 -13.31513786315918 88.8 -13.387874603271486 89.6 -13.65395545959473 90.4 -13.740654945373535
		 91.2 -13.552288055419922 92 -13.376729965209959 92.8 -13.407037734985352 93.6 -13.450393676757813
		 94.4 -13.409872055053713 95.2 -13.453237533569338 96 -13.546446800231934 96.8 -13.51301383972168
		 97.6 -13.486910820007324 98.4 -13.572828292846681 99.2 -13.533849716186523 100 -13.415169715881348
		 100.8 -13.448655128479004 101.6 -13.418776512145996 102.4 -13.258424758911133 103.2 -13.328395843505859
		 104 -13.658528327941896 104.8 -13.837175369262695 105.6 -13.603046417236328 106.4 -13.294573783874512
		 107.2 -13.417744636535645 108 -13.941986083984377 108.8 -14.391950607299805 109.6 -14.400790214538574
		 110.4 -14.121397018432615 111.2 -13.912006378173828 112 -13.926420211791992 112.8 -14.169962882995604
		 113.6 -14.379184722900392 114.4 -14.327893257141112 115.2 -14.297048568725586 116 -14.410672187805176
		 116.8 -14.346729278564457 117.6 -14.164801597595217 118.4 -14.143985748291016 119.2 -14.146077156066896
		 120 -14.094359397888184 120.8 -14.205181121826172 121.6 -14.453116416931152 122.4 -14.683869361877443
		 123.2 -14.858593940734862 124 -14.850517272949221 124.8 -14.678162574768068 125.6 -14.577417373657228
		 126.4 -14.526824951171875 127.2 -14.431791305541994 128 -14.357395172119141 128.8 -14.275308609008787
		 129.6 -14.166017532348633 130.4 -14.119430541992188 131.2 -14.129049301147459 132 -14.207077026367188
		 132.8 -14.421995162963869 133.6 -14.566400527954103 134.4 -14.377827644348145 135.2 -13.954553604125977
		 136 -13.564792633056641 136.8 -13.376020431518556 137.6 -13.426835060119627 138.4 -13.676771163940431
		 139.2 -13.903318405151367 140 -13.763042449951172 140.8 -13.396731376647947 141.6 -13.287112236022947
		 142.4 -13.398984909057617 143.2 -13.487385749816896 144 -13.630752563476562 144.8 -13.859956741333008
		 145.6 -14.09622287750244 146.4 -14.256915092468262 147.2 -14.2603759765625 148 -14.222029685974119
		 148.8 -14.15912914276123 149.6 -13.971129417419434 150.4 -13.851128578186035 151.2 -13.893375396728516
		 152 -13.950395584106444 152.8 -14.085632324218752 153.6 -14.26486873626709 154.4 -14.325465202331545
		 155.2 -14.31194019317627 156 -14.377463340759279 156.8 -14.477782249450682 157.6 -14.483189582824712
		 158.4 -14.471079826354979 159.2 -14.616584777832026 160 -14.746006965637209 160.8 -14.661959648132326
		 161.6 -14.573591232299805 162.4 -14.662422180175781 163.2 -14.733569145202638 164 -14.651986122131348
		 164.8 -14.619757652282718 165.6 -14.783989906311035 166.4 -15.044544219970705 167.2 -15.154416084289551
		 168 -15.025295257568359 168.8 -14.802699089050293 169.6 -14.827591896057134 170.4 -15.127614974975588
		 171.2 -15.232825279235842 172 -15.021217346191406 172.8 -14.871665000915526 173.6 -14.90147590637207
		 174.4 -14.86122512817383 175.2 -14.654115676879876 176 -14.619267463684082 176.8 -14.949777603149416
		 177.6 -15.268609046936032 178.4 -15.323740005493164 179.2 -15.262055397033693 180 -15.235812187194824
		 180.8 -15.294297218322754;
createNode animCurveTA -n "Bip01_R_Hand_rotateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 227 ".ktv[0:226]"  0 90.133308410644531 0.8 90.133316040039063
		 1.6 90.133316040039063 2.4 90.133308410644531 3.2 90.133308410644531 4 90.133316040039063
		 4.8 90.133308410644531 5.6 90.133316040039063 6.4 90.133316040039063 7.2 90.133331298828125
		 8 90.133316040039063 8.8 90.133316040039063 9.6 90.133316040039063 10.4 90.133316040039063
		 11.2 90.133308410644531 12 90.133316040039063 12.8 90.133308410644531 13.6 90.133308410644531
		 14.4 90.133316040039063 15.2 90.133316040039063 16 90.133316040039063 16.8 90.133308410644531
		 17.6 90.133308410644531 18.4 90.133316040039063 19.2 90.133316040039063 20 90.133308410644531
		 20.8 90.133308410644531 21.6 90.133308410644531 22.4 90.133316040039063 23.2 90.133316040039063
		 24 90.133316040039063 24.8 90.133316040039063 25.6 90.133316040039063 26.4 90.133316040039063
		 27.2 90.133316040039063 28 90.133316040039063 28.8 90.133316040039063 29.6 90.133316040039063
		 30.4 90.133331298828125 31.2 90.133316040039063 32 90.133316040039063 32.8 90.133316040039063
		 33.6 90.133331298828125 34.4 90.133331298828125 35.2 90.133316040039063 36 90.133316040039063
		 36.8 90.133316040039063 37.6 90.133316040039063 38.4 90.133316040039063 39.2 90.133316040039063
		 40 90.133316040039063 40.8 90.133316040039063 41.6 90.133316040039063 42.4 90.133316040039063
		 43.2 90.133316040039063 44 90.133316040039063 44.8 90.133308410644531 45.6 90.133308410644531
		 46.4 90.133316040039063 47.2 90.133308410644531 48 90.133331298828125 48.8 90.133316040039063
		 49.6 90.133316040039063 50.4 90.133316040039063 51.2 90.133316040039063 52 90.133316040039063
		 52.8 90.133316040039063 53.6 90.133316040039063 54.4 90.133331298828125 55.2 90.133316040039063
		 56 90.133316040039063 56.8 90.133316040039063 57.6 90.133316040039063 58.4 90.133316040039063
		 59.2 90.133316040039063 60 90.133316040039063 60.8 90.133331298828125 61.6 90.133316040039063
		 62.4 90.133316040039063 63.2 90.133316040039063 64 90.133316040039063 64.8 90.133308410644531
		 65.6 90.133316040039063 66.4 90.133308410644531 67.2 90.133316040039063 68 90.133316040039063
		 68.8 90.133316040039063 69.6 90.133316040039063 70.4 90.133308410644531 71.2 90.133316040039063
		 72 90.133308410644531 72.8 90.133316040039063 73.6 90.133316040039063 74.4 90.133316040039063
		 75.2 90.133308410644531 76 90.133316040039063 76.8 90.133316040039063 77.6 90.133316040039063
		 78.4 90.133308410644531 79.2 90.133308410644531 80 90.133316040039063 80.8 90.133316040039063
		 81.6 90.133316040039063 82.4 90.133316040039063 83.2 90.133316040039063 84 90.133316040039063
		 84.8 90.133316040039063 85.6 90.133316040039063 86.4 90.133308410644531 87.2 90.133316040039063
		 88 90.133316040039063 88.8 90.133316040039063 89.6 90.133316040039063 90.4 90.133316040039063
		 91.2 90.133316040039063 92 90.133316040039063 92.8 90.133308410644531 93.6 90.133308410644531
		 94.4 90.133316040039063 95.2 90.133316040039063 96 90.133331298828125 96.8 90.133316040039063
		 97.6 90.133308410644531 98.4 90.133308410644531 99.2 90.133316040039063 100 90.133316040039063
		 100.8 90.133316040039063 101.6 90.133316040039063 102.4 90.133316040039063 103.2 90.133308410644531
		 104 90.133308410644531 104.8 90.133316040039063 105.6 90.133316040039063 106.4 90.133316040039063
		 107.2 90.133316040039063 108 90.133308410644531 108.8 90.133316040039063 109.6 90.133316040039063
		 110.4 90.133316040039063 111.2 90.133316040039063 112 90.133316040039063 112.8 90.133331298828125
		 113.6 90.133316040039063 114.4 90.133308410644531 115.2 90.133316040039063 116 90.133316040039063
		 116.8 90.133316040039063 117.6 90.133316040039063 118.4 90.133316040039063 119.2 90.133316040039063
		 120 90.133316040039063 120.8 90.133316040039063 121.6 90.133316040039063 122.4 90.133316040039063
		 123.2 90.133316040039063 124 90.133316040039063 124.8 90.133316040039063 125.6 90.133316040039063
		 126.4 90.133316040039063 127.2 90.133308410644531 128 90.133316040039063 128.8 90.133316040039063
		 129.6 90.133316040039063 130.4 90.133308410644531 131.2 90.133308410644531 132 90.133316040039063
		 132.8 90.133316040039063 133.6 90.133316040039063 134.4 90.133316040039063 135.2 90.133316040039063
		 136 90.133316040039063 136.8 90.133316040039063 137.6 90.133316040039063 138.4 90.133316040039063
		 139.2 90.133316040039063 140 90.133316040039063 140.8 90.133316040039063 141.6 90.133308410644531
		 142.4 90.133331298828125 143.2 90.133316040039063 144 90.133308410644531 144.8 90.133316040039063
		 145.6 90.133316040039063 146.4 90.133316040039063 147.2 90.133316040039063 148 90.133308410644531
		 148.8 90.133316040039063 149.6 90.133316040039063 150.4 90.133308410644531 151.2 90.133316040039063
		 152 90.133316040039063 152.8 90.133316040039063 153.6 90.133316040039063 154.4 90.133316040039063
		 155.2 90.133308410644531 156 90.133316040039063 156.8 90.133331298828125 157.6 90.133316040039063
		 158.4 90.133316040039063 159.2 90.133331298828125 160 90.133316040039063 160.8 90.133316040039063
		 161.6 90.133316040039063 162.4 90.133331298828125 163.2 90.133331298828125 164 90.133316040039063
		 164.8 90.133316040039063 165.6 90.133316040039063 166.4 90.133316040039063 167.2 90.133316040039063
		 168 90.133316040039063 168.8 90.133316040039063 169.6 90.133316040039063 170.4 90.133316040039063
		 171.2 90.133316040039063 172 90.133308410644531 172.8 90.133316040039063 173.6 90.133331298828125
		 174.4 90.133316040039063 175.2 90.133316040039063 176 90.133331298828125 176.8 90.133316040039063
		 177.6 90.133316040039063 178.4 90.133316040039063 179.2 90.133316040039063 180 90.133308410644531
		 180.8 90.133316040039063;
createNode animCurveTA -n "Bip01_R_Hand_rotateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 227 ".ktv[0:226]"  0 -6.3004002571105957 0.8 -6.3003993034362793
		 1.6 -6.3004021644592285 2.4 -6.3004021644592285 3.2 -6.3003964424133301 4 -6.3004031181335449
		 4.8 -6.3003964424133301 5.6 -6.3004031181335449 6.4 -6.3004117012023926 7.2 -6.3004026412963876
		 8 -6.3003983497619629 8.8 -6.3003954887390137 9.6 -6.3004045486450195 10.4 -6.3003950119018555
		 11.2 -6.3004021644592285 12 -6.3004007339477539 12.8 -6.3004074096679687 13.6 -6.3004059791564941
		 14.4 -6.3004021644592285 15.2 -6.3004045486450195 16 -6.3004016876220703 16.8 -6.3004045486450195
		 17.6 -6.3004045486450195 18.4 -6.3003959655761728 19.2 -6.3004016876220703 20 -6.3004002571105957
		 20.8 -6.3004026412963876 21.6 -6.3004002571105957 22.4 -6.3003973960876465 23.2 -6.3004012107849121
		 24 -6.3003983497619629 24.8 -6.3004021644592285 25.6 -6.300407886505127 26.4 -6.3004069328308105
		 27.2 -6.3003993034362793 28 -6.3004021644592285 28.8 -6.3003950119018555 29.6 -6.3004045486450195
		 30.4 -6.3004035949707031 31.2 -6.3003931045532227 32 -6.3004031181335449 32.8 -6.3004031181335449
		 33.6 -6.3004035949707031 34.4 -6.3003945350646973 35.2 -6.3004026412963876 36 -6.3004021644592285
		 36.8 -6.3003988265991211 37.6 -6.3003964424133301 38.4 -6.3003988265991211 39.2 -6.3004040718078622
		 40 -6.3004074096679687 40.8 -6.3004007339477539 41.6 -6.3004055023193359 42.4 -6.3003959655761728
		 43.2 -6.3003983497619629 44 -6.3004012107849121 44.8 -6.3004016876220703 45.6 -6.3004002571105957
		 46.4 -6.3004016876220703 47.2 -6.3004016876220703 48 -6.3003988265991211 48.8 -6.3004035949707031
		 49.6 -6.3004088401794434 50.4 -6.3004016876220703 51.2 -6.3004007339477539 52 -6.3004040718078622
		 52.8 -6.3004035949707031 53.6 -6.3003988265991211 54.4 -6.3004021644592285 55.2 -6.3003969192504883
		 56 -6.3004045486450195 56.8 -6.3003988265991211 57.6 -6.3004064559936523 58.4 -6.3004021644592285
		 59.2 -6.3004007339477539 60 -6.3003978729248047 60.8 -6.3004021644592285 61.6 -6.300410270690918
		 62.4 -6.3004074096679687 63.2 -6.3004074096679687 64 -6.3003978729248047 64.8 -6.3004021644592285
		 65.6 -6.3004002571105957 66.4 -6.3004088401794434 67.2 -6.3004059791564941 68 -6.3003959655761728
		 68.8 -6.3004031181335449 69.6 -6.3003993034362793 70.4 -6.3004040718078622 71.2 -6.3004055023193359
		 72 -6.3003954887390137 72.8 -6.3004016876220703 73.6 -6.3004050254821777 74.4 -6.3004035949707031
		 75.2 -6.3003964424133301 76 -6.3004045486450195 76.8 -6.3003983497619629 77.6 -6.3004012107849121
		 78.4 -6.3004055023193359 79.2 -6.3003978729248047 80 -6.3004012107849121 80.8 -6.3004088401794434
		 81.6 -6.3003988265991211 82.4 -6.3003978729248047 83.2 -6.3003940582275391 84 -6.3003969192504883
		 84.8 -6.3003983497619629 85.6 -6.3004088401794434 86.4 -6.3004002571105957 87.2 -6.3004064559936523
		 88 -6.3003983497619629 88.8 -6.3004055023193359 89.6 -6.3003935813903809 90.4 -6.3004007339477539
		 91.2 -6.3003988265991211 92 -6.3003954887390137 92.8 -6.3004059791564941 93.6 -6.3003935813903809
		 94.4 -6.3004035949707031 95.2 -6.3004031181335449 96 -6.3003959655761728 96.8 -6.3004074096679687
		 97.6 -6.3004031181335449 98.4 -6.3003993034362793 99.2 -6.3004016876220703 100 -6.3003950119018555
		 100.8 -6.3004050254821777 101.6 -6.3003926277160645 102.4 -6.3004002571105957 103.2 -6.3004035949707031
		 104 -6.3003973960876465 104.8 -6.3004031181335449 105.6 -6.3003954887390137 106.4 -6.3004021644592285
		 107.2 -6.3004007339477539 108 -6.3003973960876465 108.8 -6.3003997802734375 109.6 -6.3004059791564941
		 110.4 -6.3003983497619629 111.2 -6.3004007339477539 112 -6.3004031181335449 112.8 -6.3003973960876465
		 113.6 -6.3004026412963876 114.4 -6.3004007339477539 115.2 -6.3003973960876465 116 -6.3004007339477539
		 116.8 -6.3003964424133301 117.6 -6.3004007339477539 118.4 -6.3003978729248047 119.2 -6.3004040718078622
		 120 -6.3004050254821777 120.8 -6.3003983497619629 121.6 -6.3003964424133301 122.4 -6.3004035949707031
		 123.2 -6.3004016876220703 124 -6.3004007339477539 124.8 -6.3004012107849121 125.6 -6.3003964424133301
		 126.4 -6.3003973960876465 127.2 -6.3004040718078622 128 -6.3004012107849121 128.8 -6.3003983497619629
		 129.6 -6.3004045486450195 130.4 -6.3004031181335449 131.2 -6.3003978729248047 132 -6.3004031181335449
		 132.8 -6.3004026412963876 133.6 -6.3004012107849121 134.4 -6.3004069328308105 135.2 -6.3004012107849121
		 136 -6.3004031181335449 136.8 -6.3003983497619629 137.6 -6.3003983497619629 138.4 -6.3004007339477539
		 139.2 -6.3004002571105957 140 -6.3003969192504883 140.8 -6.3003964424133301 141.6 -6.3004035949707031
		 142.4 -6.3004016876220703 143.2 -6.3003973960876465 144 -6.3004083633422852 144.8 -6.3003950119018555
		 145.6 -6.3004055023193359 146.4 -6.3004007339477539 147.2 -6.3004045486450195 148 -6.3004055023193359
		 148.8 -6.3004031181335449 149.6 -6.3003959655761728 150.4 -6.3004031181335449 151.2 -6.3004050254821777
		 152 -6.3004045486450195 152.8 -6.3004050254821777 153.6 -6.3003973960876465 154.4 -6.3004031181335449
		 155.2 -6.3004016876220703 156 -6.3004045486450195 156.8 -6.3004040718078622 157.6 -6.3003993034362793
		 158.4 -6.3004040718078622 159.2 -6.3003993034362793 160 -6.3004040718078622 160.8 -6.3003988265991211
		 161.6 -6.3004031181335449 162.4 -6.3004069328308105 163.2 -6.3003988265991211 164 -6.3004002571105957
		 164.8 -6.3004031181335449 165.6 -6.3004045486450195 166.4 -6.3004021644592285 167.2 -6.3003978729248047
		 168 -6.3004007339477539 168.8 -6.3004007339477539 169.6 -6.3003954887390137 170.4 -6.300391674041748
		 171.2 -6.3004035949707031 172 -6.3004007339477539 172.8 -6.3004031181335449 173.6 -6.3004059791564941
		 174.4 -6.3004097938537598 175.2 -6.3003964424133301 176 -6.3003964424133301 176.8 -6.3003945350646973
		 177.6 -6.3004026412963876 178.4 -6.3004045486450195 179.2 -6.3004007339477539 180 -6.3004035949707031
		 180.8 -6.3004012107849121;
createNode animCurveTA -n "Bip01_R_Hand_rotateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 227 ".ktv[0:226]"  0 2.565335750579834 0.8 2.5653321743011475
		 1.6 2.5653336048126221 2.4 2.565330982208252 3.2 2.5653347969055176 4 2.5653302669525142
		 4.8 2.5653400421142578 5.6 2.5653305053710938 6.4 2.5653254985809326 7.2 2.5653305053710938
		 8 2.5653338432312016 8.8 2.5653364658355713 9.6 2.5653305053710938 10.4 2.5653295516967773
		 11.2 2.5653336048126221 12 2.5653297901153564 12.8 2.5653297901153564 13.6 2.5653307437896729
		 14.4 2.5653302669525142 15.2 2.5653290748596191 16 2.5653278827667236 16.8 2.5653252601623535
		 17.6 2.5653314590454106 18.4 2.5653376579284668 19.2 2.5653328895568848 20 2.5653367042541504
		 20.8 2.565330982208252 21.6 2.5653302669525142 22.4 2.5653345584869385 23.2 2.5653319358825684
		 24 2.5653316974639893 24.8 2.5653319358825684 25.6 2.565330982208252 26.4 2.5653305053710938
		 27.2 2.5653314590454106 28 2.5653300285339355 28.8 2.5653355121612549 29.6 2.5653328895568848
		 30.4 2.5653281211853027 31.2 2.5653326511383057 32 2.5653314590454106 32.8 2.5653290748596191
		 33.6 2.5653312206268311 34.4 2.5653383731842041 35.2 2.5653352737426758 36 2.5653338432312016
		 36.8 2.5653331279754639 37.6 2.5653307437896729 38.4 2.5653350353240967 39.2 2.5653290748596191
		 40 2.5653214454650879 40.8 2.5653352737426758 41.6 2.5653350353240967 42.4 2.5653371810913086
		 43.2 2.565338134765625 44 2.5653281211853027 44.8 2.5653331279754639 45.6 2.5653324127197266
		 46.4 2.5653278827667236 47.2 2.5653285980224609 48 2.5653283596038814 48.8 2.5653319358825684
		 49.6 2.5653278827667236 50.4 2.5653328895568848 51.2 2.565333366394043 52 2.5653300285339355
		 52.8 2.5653347969055176 53.6 2.5653331279754639 54.4 2.5653319358825684 55.2 2.5653436183929443
		 56 2.5653338432312016 56.8 2.5653266906738281 57.6 2.5653302669525142 58.4 2.5653312206268311
		 59.2 2.5653383731842041 60 2.5653281211853027 60.8 2.5653324127197266 61.6 2.5653285980224609
		 62.4 2.5653307437896729 63.2 2.565333366394043 64 2.5653402805328369 64.8 2.5653338432312016
		 65.6 2.5653350353240967 66.4 2.5653307437896729 67.2 2.5653352737426758 68 2.565338134765625
		 68.8 2.5653336048126221 69.6 2.5653314590454106 70.4 2.565333366394043 71.2 2.5653278827667236
		 72 2.565335750579834 72.8 2.5653285980224609 73.6 2.5653297901153564 74.4 2.5653355121612549
		 75.2 2.5653345584869385 76 2.5653285980224609 76.8 2.5653319358825684 77.6 2.56532883644104
		 78.4 2.56532883644104 79.2 2.5653331279754639 80 2.5653312206268311 80.8 2.5653307437896729
		 81.6 2.5653293132781982 82.4 2.5653312206268311 83.2 2.5653338432312016 84 2.5653350353240967
		 84.8 2.5653345584869385 85.6 2.5653302669525142 86.4 2.5653297901153564 87.2 2.5653297901153564
		 88 2.5653300285339355 88.8 2.5653271675109863 89.6 2.5653383731842041 90.4 2.5653331279754639
		 91.2 2.5653297901153564 92 2.5653295516967773 92.8 2.5653331279754639 93.6 2.5653407573699951
		 94.4 2.5653297901153564 95.2 2.5653319358825684 96 2.5653321743011475 96.8 2.5653262138366699
		 97.6 2.5653245449066162 98.4 2.565335750579834 99.2 2.5653293132781982 100 2.5653364658355713
		 100.8 2.5653297901153564 101.6 2.5653400421142578 102.4 2.5653314590454106 103.2 2.5653319358825684
		 104 2.5653364658355713 104.8 2.5653283596038814 105.6 2.5653350353240967 106.4 2.5653340816497803
		 107.2 2.5653347969055176 108 2.5653328895568848 108.8 2.5653359889984131 109.6 2.5653266906738281
		 110.4 2.5653278827667236 111.2 2.56532883644104 112 2.5653319358825684 112.8 2.5653328895568848
		 113.6 2.5653312206268311 114.4 2.5653300285339355 115.2 2.5653302669525142 116 2.565333366394043
		 116.8 2.565335750579834 117.6 2.5653297901153564 118.4 2.5653300285339355 119.2 2.5653312206268311
		 120 2.5653374195098877 120.8 2.5653343200683594 121.6 2.5653319358825684 122.4 2.5653328895568848
		 123.2 2.5653336048126221 124 2.5653281211853027 124.8 2.565333366394043 125.6 2.5653347969055176
		 126.4 2.5653297901153564 127.2 2.565321683883667 128 2.5653326511383057 128.8 2.5653314590454106
		 129.6 2.5653295516967773 130.4 2.5653305053710938 131.2 2.5653307437896729 132 2.5653285980224609
		 132.8 2.5653328895568848 133.6 2.5653319358825684 134.4 2.5653331279754639 135.2 2.5653345584869385
		 136 2.5653352737426758 136.8 2.5653278827667236 137.6 2.5653297901153564 138.4 2.5653398036956787
		 139.2 2.5653350353240967 140 2.5653374195098877 140.8 2.5653386116027832 141.6 2.565330982208252
		 142.4 2.5653285980224609 143.2 2.5653362274169922 144 2.5653257369995117 144.8 2.5653328895568848
		 145.6 2.5653297901153564 146.4 2.5653336048126221 147.2 2.5653312206268311 148 2.5653295516967773
		 148.8 2.5653321743011475 149.6 2.5653359889984131 150.4 2.5653307437896729 151.2 2.5653281211853027
		 152 2.565338134765625 152.8 2.5653250217437744 153.6 2.5653302669525142 154.4 2.565335750579834
		 155.2 2.5653321743011475 156 2.5653269290924072 156.8 2.5653326511383057 157.6 2.5653307437896729
		 158.4 2.5653293132781982 159.2 2.5653295516967773 160 2.5653383731842041 160.8 2.5653324127197266
		 161.6 2.5653338432312016 162.4 2.5653319358825684 163.2 2.5653343200683594 164 2.5653352737426758
		 164.8 2.5653321743011475 165.6 2.5653269290924072 166.4 2.5653326511383057 167.2 2.5653278827667236
		 168 2.5653328895568848 168.8 2.5653355121612549 169.6 2.5653340816497803 170.4 2.5653417110443115
		 171.2 2.565330982208252 172 2.565330982208252 172.8 2.5653259754180908 173.6 2.565333366394043
		 174.4 2.5653281211853027 175.2 2.5653359889984131 176 2.5653359889984131 176.8 2.5653326511383057
		 177.6 2.5653319358825684 178.4 2.5653300285339355 179.2 2.5653340816497803 180 2.5653297901153564
		 180.8 2.5653326511383057;
createNode animCurveTA -n "Bip01_L_Clavicle_rotateX17";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 227 ".ktv[0:226]"  0 -0.62246006727218628 0.8 -0.62291628122329712
		 1.6 -0.62459862232208252 2.4 -0.62511193752288818 3.2 -0.62673819065093994 4 -0.6283571720123291
		 4.8 -0.63024824857711792 5.6 -0.63086545467376709 6.4 -0.63299572467803955 7.2 -0.63491308689117432
		 8 -0.63680040836334229 8.8 -0.63879406452178955 9.6 -0.64104574918746948 10.4 -0.64364379644393921
		 11.2 -0.64440238475799561 12 -0.64705163240432739 12.8 -0.64929318428039551 13.6 -0.65153360366821289
		 14.4 -0.65404045581817627 15.2 -0.65478312969207764 16 -0.65726166963577271 16.8 -0.65928953886032104
		 17.6 -0.66131353378295898 18.4 -0.66194576025009155 19.2 -0.66388458013534546 20 -0.66467046737670898
		 20.8 -0.66532492637634277 21.6 -0.66605746746063232 22.4 -0.66639077663421631 23.2 -0.66627568006515503
		 24 -0.66588300466537476 24.8 -0.66512316465377808 25.6 -0.66400045156478882 26.4 -0.66256380081176758
		 27.2 -0.66080015897750854 28 -0.65833503007888794 28.8 -0.65741103887557983 29.6 -0.65371686220169067
		 30.4 -0.65262472629547119 31.2 -0.64930433034896851 32 -0.64649516344070446 32.8 -0.64386862516403198
		 33.6 -0.64144766330718994 34.4 -0.63920730352401733 35.2 -0.63721174001693726 36 -0.6355431079864502
		 36.8 -0.63423913717269897 37.6 -0.63333719968795776 38.4 -0.63291126489639282 39.2 -0.63328033685684204
		 40 -0.63353055715560924 40.8 -0.63496530055999767 41.6 -0.63678580522537231 42.4 -0.63937914371490479
		 43.2 -0.64319872856140137 44 -0.64480304718017578 44.8 -0.65069460868835449 45.6 -0.65258544683456432
		 46.4 -0.658497154712677 47.2 -0.66400754451751709 48 -0.66997218132019043 48.8 -0.67740863561630249
		 49.6 -0.67972379922866832 50.4 -0.68767851591110229 51.2 -0.69506782293319702 52 -0.70359104871749878
		 52.8 -0.70615154504776001 53.6 -0.71474546194076538 54.4 -0.72218924760818481 55.2 -0.72956234216690063
		 56 -0.73820662498474132 56.8 -0.74117547273635875 57.6 -0.75177687406539917 58.4 -0.75630861520767212
		 59.2 -0.76076811552047729 60 -0.77060955762863148 60.8 -0.77335524559020996 61.6 -0.7806968092918396
		 62.4 -0.78699213266372692 63.2 -0.79360496997833252 64 -0.79544979333877563 64.8 -0.80152612924575806
		 65.6 -0.80653035640716564 66.4 -0.81166613101959229 67.2 -0.81310904026031483 68 -0.81759905815124512
		 68.8 -0.82115697860717773 69.6 -0.82485902309417725 70.4 -0.8258633017539978 71.2 -0.8290514349937439
		 72 -0.83164060115814209 72.8 -0.83424979448318481 73.6 -0.83493655920028687 74.4 -0.83721148967742931
		 75.2 -0.83903306722640991 76 -0.84056687355041504 76.8 -0.84196388721466064 77.6 -0.84332472085952748
		 78.4 -0.84455668926239003 79.2 -0.84577035903930653 80 -0.84711253643035889 80.8 -0.84743839502334595
		 81.6 -0.84862357378005981 82.4 -0.84950727224349987 83.2 -0.8502429723739624 84 -0.85083174705505371
		 84.8 -0.85130321979522705 85.6 -0.85160374641418468 86.4 -0.85187572240829468 87.2 -0.85203254222869873
		 88 -0.85193908214569092 88.8 -0.85178112983703613 89.6 -0.85158902406692516 90.4 -0.85117429494857788
		 91.2 -0.85101336240768433 92 -0.85048764944076549 92.8 -0.85011309385299683 93.6 -0.84964674711227428
		 94.4 -0.84950309991836548 95.2 -0.84904938936233521 96 -0.84857332706451427 96.8 -0.8484576940536499
		 97.6 -0.84802764654159557 98.4 -0.847822666168213 99.2 -0.84757387638092052 100 -0.84747737646102916
		 100.8 -0.84727382659912109 101.6 -0.84709542989730835 102.4 -0.84693574905395508
		 103.2 -0.84681296348571777 104 -0.84661835432052612 104.8 -0.84637260437011708 105.6 -0.84599101543426514
		 106.4 -0.8457711935043335 107.2 -0.84553241729736317 108 -0.84492641687393188 108.8 -0.84415906667709339
		 109.6 -0.8438565731048584 110.4 -0.84294867515563965 111.2 -0.84212517738342285 112 -0.8411409854888916
		 112.8 -0.83978831768035889 113.6 -0.83941388130187988 114.4 -0.83793807029724132
		 115.2 -0.83663183450698853 116 -0.83531588315963745 116.8 -0.83392906188964844 117.6 -0.83245736360549916
		 118.4 -0.83084243535995483 119.2 -0.82893264293670654 120 -0.82841241359710693 120.8 -0.82638394832611084
		 121.6 -0.82466673851013184 122.4 -0.8230823278427124 123.2 -0.82129639387130748 124 -0.81918662786483765
		 124.8 -0.81847429275512695 125.6 -0.81588554382324219 126.4 -0.81513363122940063
		 127.2 -0.8129279613494873 128 -0.81093317270278931 128.8 -0.80908745527267456 129.6 -0.80718111991882324
		 130.4 -0.80521672964096069 131.2 -0.80323809385299683 132 -0.80118447542190563 132.8 -0.79908132553100597
		 133.6 -0.79650461673736572 134.4 -0.79574382305145264 135.2 -0.79301625490188588
		 136 -0.79024648666381836 136.8 -0.78954434394836437 137.6 -0.78684818744659424 138.4 -0.78430211544036865
		 139.2 -0.78355807065963745 140 -0.78126305341720581 140.8 -0.77931803464889515 141.6 -0.77730417251586914
		 142.4 -0.77676963806152355 143.2 -0.77504295110702515 144 -0.77374255657196045 144.8 -0.77270221710205067
		 145.6 -0.77172940969467163 146.4 -0.77092486619949341 147.2 -0.77021175622940075
		 148 -0.76965057849884033 148.8 -0.76924961805343628 149.6 -0.76884251832962036 150.4 -0.76861965656280518
		 151.2 -0.7683977484703064 152 -0.7683110237121582 152.8 -0.768213450908661 153.6 -0.76825445890426636
		 154.4 -0.76821058988571167 155.2 -0.76820844411849987 156 -0.76819819211959839 156.8 -0.76810407638549805
		 157.6 -0.76796513795852661 158.4 -0.7676548957824707 159.2 -0.7672252655029298 160 -0.76706564426422108
		 160.8 -0.76630228757858276 161.6 -0.76529580354690563 162.4 -0.7649080753326416 163.2 -0.76361727714538574
		 164 -0.76233482360839844 164.8 -0.76089775562286388 165.6 -0.75925344228744507 166.4 -0.75743556022644043
		 167.2 -0.75529289245605469 168 -0.75247186422348011 168.8 -0.75158250331878651 169.6 -0.7485663890838623
		 170.4 -0.74560678005218517 171.2 -0.74214518070220947 172 -0.74107933044433605 172.8 -0.73751968145370483
		 173.6 -0.73433405160903931 174.4 -0.73114317655563354 175.2 -0.72744870185852051
		 176 -0.72631984949111938 176.8 -0.7226797342300415 177.6 -0.71963965892791759 178.4 -0.71680891513824463
		 179.2 -0.71397405862808228 180 -0.71078217029571533 180.8 -0.70991331338882446;
createNode animCurveTA -n "Bip01_L_Clavicle_rotateY17";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 227 ".ktv[0:226]"  0 -78.456710815429673 0.8 -78.455711364746094
		 1.6 -78.452102661132813 2.4 -78.450965881347642 3.2 -78.447555541992188 4 -78.444129943847642
		 4.8 -78.440017700195313 5.6 -78.438743591308594 6.4 -78.434249877929702 7.2 -78.430152893066406
		 8 -78.426078796386719 8.8 -78.421798706054687 9.6 -78.417083740234375 10.4 -78.411468505859375
		 11.2 -78.409797668457017 12 -78.4041748046875 12.8 -78.399398803710937 13.6 -78.394668579101563
		 14.4 -78.389205932617188 15.2 -78.387580871582031 16 -78.382392883300781 16.8 -78.378059387207017
		 17.6 -78.37371826171875 18.4 -78.372383117675781 19.2 -78.368110656738281 20 -78.366539001464858
		 20.8 -78.365272521972656 21.6 -78.363456726074219 22.4 -78.362838745117188 23.2 -78.363075256347642
		 24 -78.364006042480469 24.8 -78.3656005859375 25.6 -78.367942810058594 26.4 -78.370964050292983
		 27.2 -78.374794006347642 28 -78.380012512207017 28.8 -78.382102966308594 29.6 -78.389907836914063
		 30.4 -78.392265319824219 31.2 -78.399330139160156 32 -78.405349731445313 32.8 -78.410911560058594
		 33.6 -78.416114807128906 34.4 -78.420875549316406 35.2 -78.425094604492188 36 -78.428741455078125
		 36.8 -78.431594848632813 37.6 -78.433486938476563 38.4 -78.434349060058594 39.2 -78.433624267578125
		 40 -78.433128356933594 40.8 -78.429985046386719 41.6 -78.426116943359375 42.4 -78.4205322265625
		 43.2 -78.412368774414063 44 -78.409011840820327 44.8 -78.3963623046875 45.6 -78.392402648925795
		 46.4 -78.379730224609375 47.2 -78.367965698242188 48 -78.35516357421875 48.8 -78.339241027832031
		 49.6 -78.334266662597685 50.4 -78.317298889160156 51.2 -78.301445007324219 52 -78.283256530761719
		 52.8 -78.277740478515625 53.6 -78.259201049804687 54.4 -78.243415832519531 55.2 -78.227622985839844
		 56 -78.209129333496094 56.8 -78.202720642089844 57.6 -78.180122375488281 58.4 -78.170265197753906
		 59.2 -78.160751342773452 60 -78.139541625976563 60.8 -78.133758544921875 61.6 -78.117935180664063
		 62.4 -78.104393005371094 63.2 -78.090187072753906 64 -78.086128234863281 64.8 -78.073150634765625
		 65.6 -78.062339782714844 66.4 -78.051376342773438 67.2 -78.048301696777344 68 -78.038642883300795
		 68.8 -78.030853271484375 69.6 -78.023040771484375 70.4 -78.020851135253906 71.2 -78.014030456542997
		 72 -78.008399963378906 72.8 -78.002777099609375 73.6 -78.001235961914062 74.4 -77.996353149414062
		 75.2 -77.992546081542983 76 -77.9892578125 76.8 -77.986190795898437 77.6 -77.983322143554688
		 78.4 -77.980590820312514 79.2 -77.977951049804687 80 -77.975120544433594 80.8 -77.974319458007813
		 81.6 -77.971855163574233 82.4 -77.969955444335937 83.2 -77.968421936035156 84 -77.967132568359375
		 84.8 -77.96610260009767 85.6 -77.965339660644531 86.4 -77.964820861816406 87.2 -77.964591979980469
		 88 -77.964675903320327 88.8 -77.964996337890625 89.6 -77.965461730957031 90.4 -77.96636962890625
		 91.2 -77.966667175292983 92 -77.967735290527344 92.8 -77.968582153320327 93.6 -77.969627380371094
		 94.4 -77.969947814941406 95.2 -77.970939636230469 96 -77.971908569335938 96.8 -77.972206115722656
		 97.6 -77.973045349121094 98.4 -77.973541259765625 99.2 -77.974082946777344 100 -77.974243164062514
		 100.8 -77.974746704101563 101.6 -77.975143432617188 102.4 -77.9754638671875 103.2 -77.975837707519531
		 104 -77.97625732421875 104.8 -77.976699829101563 105.6 -77.977546691894531 106.4 -77.977935791015625
		 107.2 -77.978477478027344 108 -77.979812622070327 108.8 -77.981483459472656 109.6 -77.98199462890625
		 110.4 -77.984024047851563 111.2 -77.985816955566406 112 -77.98799896240233 112.8 -77.990821838378906
		 113.6 -77.991683959960937 114.4 -77.994857788085938 115.2 -77.997711181640625 116 -78.000534057617188
		 116.8 -78.003517150878906 117.6 -78.006668090820327 118.4 -78.010017395019531 119.2 -78.014144897460938
		 120 -78.015388488769531 120.8 -78.019676208496094 121.6 -78.023384094238281 122.4 -78.026847839355469
		 123.2 -78.0306396484375 124 -78.035171508789063 124.8 -78.036788940429673 125.6 -78.042312622070327
		 126.4 -78.043930053710938 127.2 -78.048629760742187 128 -78.052803039550781 128.8 -78.056816101074219
		 129.6 -78.060966491699219 130.4 -78.065193176269531 131.2 -78.069442749023438 132 -78.073753356933594
		 132.8 -78.078392028808594 133.6 -78.083908081054687 134.4 -78.085563659667969 135.2 -78.091453552246094
		 136 -78.097328186035156 136.8 -78.098991394042983 137.6 -78.104705810546875 138.4 -78.110176086425781
		 139.2 -78.1116943359375 140 -78.116630554199219 140.8 -78.120834350585937 141.6 -78.125114440917969
		 142.4 -78.126335144042983 143.2 -78.130020141601563 144 -78.132804870605469 144.8 -78.135116577148437
		 145.6 -78.137107849121094 146.4 -78.138885498046875 147.2 -78.140396118164077 148 -78.141616821289062
		 148.8 -78.142547607421875 149.6 -78.143257141113281 150.4 -78.14383697509767 151.2 -78.14424896240233
		 152 -78.144515991210938 152.8 -78.144630432128906 153.6 -78.144645690917969 154.4 -78.144676208496094
		 155.2 -78.144683837890625 156 -78.144752502441406 156.8 -78.144950866699219 157.6 -78.145263671875
		 158.4 -78.145797729492202 159.2 -78.146842956542983 160 -78.147201538085938 160.8 -78.148765563964844
		 161.6 -78.150993347167969 162.4 -78.151725769042983 163.2 -78.154518127441406 164 -78.157310485839844
		 164.8 -78.160369873046875 165.6 -78.163871765136733 166.4 -78.167854309082031 167.2 -78.172515869140625
		 168 -78.17842864990233 168.8 -78.180328369140625 169.6 -78.186958312988281 170.4 -78.193275451660156
		 171.2 -78.200645446777344 172 -78.202903747558594 172.8 -78.210601806640625 173.6 -78.217399597167969
		 174.4 -78.224220275878906 175.2 -78.232208251953125 176 -78.234588623046875 176.8 -78.242431640625
		 177.6 -78.248947143554688 178.4 -78.254890441894531 179.2 -78.260993957519531 180 -78.267822265625
		 180.8 -78.269752502441406;
createNode animCurveTA -n "Bip01_L_Clavicle_rotateZ17";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 227 ".ktv[0:226]"  0 174.33221435546875 0.8 174.332275390625
		 1.6 174.33256530761719 2.4 174.3326416015625 3.2 174.33293151855469 4 174.33325195312503
		 4.8 174.33354187011719 5.6 174.33366394042969 6.4 174.33404541015625 7.2 174.33438110351562
		 8 174.33470153808594 8.8 174.33502197265625 9.6 174.33544921875 10.4 174.33587646484378
		 11.2 174.33598327636719 12 174.33644104003906 12.8 174.33685302734375 13.6 174.33724975585937
		 14.4 174.33763122558594 15.2 174.33775329589844 16 174.33822631835935 16.8 174.33856201171875
		 17.6 174.33891296386719 18.4 174.33901977539062 19.2 174.33929443359375 20 174.33949279785159
		 20.8 174.33964538574219 21.6 174.33967590332031 22.4 174.33978271484375 23.2 174.33975219726562
		 24 174.3397216796875 24.8 174.33956909179687 25.6 174.33937072753906 26.4 174.339111328125
		 27.2 174.33882141113281 28 174.33837890625 28.8 174.3382568359375 29.6 174.33758544921875
		 30.4 174.33741760253906 31.2 174.33682250976565 32 174.33636474609375 32.8 174.33587646484378
		 33.6 174.33547973632812 34.4 174.3350830078125 35.2 174.33470153808594 36 174.33445739746097
		 36.8 174.33425903320312 37.6 174.33409118652344 38.4 174.33399963378906 39.2 174.3341064453125
		 40 174.33415222167969 40.8 174.33436584472656 41.6 174.33468627929687 42.4 174.33511352539062
		 43.2 174.33576965332031 44 174.33607482910156 44.8 174.33706665039062 45.6 174.33741760253906
		 46.4 174.33842468261719 47.2 174.33938598632812 48 174.34037780761719 48.8 174.34162902832031
		 49.6 174.34202575683594 50.4 174.34341430664062 51.2 174.34463500976562 52 174.34613037109375
		 52.8 174.34654235839844 53.6 174.34794616699219 54.4 174.34927368164062 55.2 174.35050964355469
		 56 174.35197448730469 56.8 174.35246276855469 57.6 174.35427856445312 58.4 174.35501098632812
		 59.2 174.35577392578125 60 174.35739135742187 60.8 174.35789489746094 61.6 174.35908508300781
		 62.4 174.36013793945313 63.2 174.36122131347656 64 174.36149597167969 64.8 174.36251831054687
		 65.6 174.36332702636719 66.4 174.36421203613281 67.2 174.36445617675781 68 174.36521911621094
		 68.8 174.36573791503906 69.6 174.36639404296875 70.4 174.36656188964844 71.2 174.36708068847656
		 72 174.36749267578125 72.8 174.36793518066406 73.6 174.36801147460935 74.4 174.36837768554687
		 75.2 174.36872863769531 76 174.36898803710935 76.8 174.36918640136719 77.6 174.36943054199219
		 78.4 174.36961364746094 79.2 174.36979675292969 80 174.37004089355469 80.8 174.37005615234375
		 81.6 174.37028503417969 82.4 174.37043762207031 83.2 174.37055969238281 84 174.37065124511719
		 84.8 174.37071228027344 85.6 174.3707275390625 86.4 174.37080383300781 87.2 174.37088012695312
		 88 174.3708190917969 88.8 174.37077331542969 89.6 174.37077331542969 90.4 174.37069702148435
		 91.2 174.37066650390625 92 174.37054443359375 92.8 174.37049865722656 93.6 174.37043762207031
		 94.4 174.37042236328125 95.2 174.37034606933597 96 174.37025451660156 96.8 174.37025451660156
		 97.6 174.37014770507812 98.4 174.37013244628909 99.2 174.37008666992187 100 174.37005615234375
		 100.8 174.37005615234375 101.6 174.37002563476562 102.4 174.37001037597656 103.2 174.37002563476562
		 104 174.36997985839844 104.8 174.36991882324219 105.6 174.36985778808594 106.4 174.36978149414062
		 107.2 174.36976623535156 108 174.36965942382815 108.8 174.36955261230469 109.6 174.36944580078125
		 110.4 174.36933898925781 111.2 174.36920166015625 112 174.36906433105469 112.8 174.36882019042969
		 113.6 174.36878967285156 114.4 174.3685302734375 115.2 174.36833190917969 116 174.36810302734375
		 116.8 174.36788940429687 117.6 174.36763000488281 118.4 174.36734008789068 119.2 174.36701965332031
		 120 174.36698913574222 120.8 174.36662292480469 121.6 174.3663330078125 122.4 174.36610412597656
		 123.2 174.36578369140625 124 174.36544799804687 124.8 174.3653564453125 125.6 174.36491394042969
		 126.4 174.36477661132812 127.2 174.36442565917969 128 174.36402893066406 128.8 174.36375427246094
		 129.6 174.36346435546875 130.4 174.36312866210935 131.2 174.36280822753906 132 174.3624267578125
		 132.8 174.36212158203125 133.6 174.3616943359375 134.4 174.361572265625 135.2 174.36112976074219
		 136 174.36064147949219 136.8 174.36056518554687 137.6 174.360107421875 138.4 174.35968017578125
		 139.2 174.35952758789062 140 174.359130859375 140.8 174.35882568359375 141.6 174.35847473144531
		 142.4 174.35842895507812 143.2 174.3581237792969 144 174.35791015625003 144.8 174.35775756835935
		 145.6 174.35755920410156 146.4 174.35743713378909 147.2 174.35731506347656 148 174.35723876953125
		 148.8 174.35719299316406 149.6 174.35707092285156 150.4 174.3570556640625 151.2 174.35700988769531
		 152 174.35700988769531 152.8 174.35696411132815 153.6 174.35700988769531 154.4 174.35697937011719
		 155.2 174.35697937011719 156 174.35700988769531 156.8 174.35697937011719 157.6 174.35696411132815
		 158.4 174.35685729980469 159.2 174.35682678222656 160 174.3568115234375 160.8 174.35665893554687
		 161.6 174.35650634765625 162.4 174.35639953613281 163.2 174.356201171875 164 174.35600280761719
		 164.8 174.35575866699222 165.6 174.35546875 166.4 174.35519409179687 167.2 174.35487365722656
		 168 174.35433959960935 168.8 174.35418701171875 169.6 174.35374450683594 170.4 174.35322570800781
		 171.2 174.35263061523435 172 174.35243225097656 172.8 174.35188293457031 173.6 174.35130310058594
		 174.4 174.35076904296875 175.2 174.35017395019531 176 174.34996032714844 176.8 174.349365234375
		 177.6 174.34886169433594 178.4 174.34834289550781 179.2 174.34786987304688 180 174.34732055664062
		 180.8 174.34721374511719;
createNode animCurveTA -n "Bip01_L_UpperArm_rotateX17";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 227 ".ktv[0:226]"  0 44.977920532226563 0.8 45.024513244628906
		 1.6 45.118476867675781 2.4 45.194328308105469 3.2 45.210849761962891 4 45.124671936035149
		 4.8 44.874065399169922 5.6 44.609401702880859 6.4 44.572406768798835 7.2 44.581874847412109
		 8 44.387741088867188 8.8 44.147293090820313 9.6 44.016761779785149 10.4 43.937747955322266
		 11.2 43.849346160888672 12 43.745487213134766 12.8 43.578746795654297 13.6 43.331069946289063
		 14.4 43.210819244384766 15.2 43.312175750732422 16 43.273639678955078 16.8 42.840404510498047
		 17.6 42.356227874755859 18.4 42.281929016113281 19.2 42.481204986572266 20 42.513523101806641
		 20.8 42.399913787841797 21.6 42.425029754638672 22.4 42.490379333496094 23.2 42.393726348876953
		 24 42.214614868164063 24.8 42.115718841552734 25.6 42.14642333984375 26.4 42.208450317382813
		 27.2 42.229934692382813 28 42.247688293457031 28.8 42.213005065917969 29.6 42.073040008544922
		 30.4 41.935550689697266 31.2 41.8599853515625 32 41.771514892578132 32.8 41.659778594970703
		 33.6 41.565021514892578 34.4 41.503028869628906 35.2 41.544338226318359 36 41.665645599365234
		 36.8 41.645957946777344 37.6 41.441722869873047 38.4 41.316444396972656 39.2 41.414783477783203
		 40 41.651016235351562 40.8 41.787681579589837 41.6 41.779911041259766 42.4 41.680561065673821
		 43.2 41.581291198730469 44 41.580013275146484 44.8 41.688865661621094 45.6 41.904529571533203
		 46.4 42.177268981933594 47.2 42.337280273437507 48 42.406753540039062 48.8 42.615036010742187
		 49.6 42.861885070800781 50.4 42.944633483886719 51.2 43.023036956787109 52 43.17024230957032
		 52.8 43.222801208496094 53.6 43.222049713134766 54.4 43.333835601806648 55.2 43.479801177978509
		 56 43.520126342773437 56.8 43.493793487548835 57.6 43.370162963867188 58.4 43.1085205078125
		 59.2 42.923076629638672 60 42.952735900878906 60.8 43.040481567382813 61.6 43.111515045166016
		 62.4 43.241462707519531 63.2 43.361297607421875 64 43.268520355224609 64.8 42.913658142089837
		 65.6 42.378368377685547 66.4 41.655784606933594 67.2 40.813457489013672 68 40.070568084716797
		 68.8 39.6248779296875 69.6 39.581035614013672 70.4 39.676166534423828 71.2 39.469993591308594
		 72 39.069999694824219 72.8 38.8944091796875 73.6 38.9127197265625 74.4 38.870677947998047
		 75.2 38.785915374755859 76 38.743179321289063 76.8 38.687980651855469 77.6 38.684768676757813
		 78.4 38.846969604492195 79.2 39.088191986083984 80 39.311599731445313 80.8 39.480484008789063
		 81.6 39.605808258056641 82.4 39.823226928710938 83.2 40.074375152587891 84 40.128707885742195
		 84.8 40.047348022460938 85.6 39.991004943847656 86.4 39.943748474121094 87.2 39.918449401855469
		 88 39.959033966064453 88.8 40.022205352783203 89.6 40.017200469970703 90.4 39.848201751708984
		 91.2 39.619911193847656 92 39.563667297363281 92.8 39.610435485839837 93.6 39.558765411376953
		 94.4 39.490104675292969 95.2 39.51605224609375 96 39.483436584472656 96.8 39.374393463134773
		 97.6 39.332740783691406 98.4 39.245643615722656 99.2 39.011779785156257 100 38.762039184570313
		 100.8 38.681007385253906 101.6 38.792648315429688 102.4 38.839462280273438 103.2 38.667411804199219
		 104 38.411869049072266 104.8 38.205101013183601 105.6 38.012168884277344 106.4 37.777782440185547
		 107.2 37.624916076660163 108 37.624351501464851 108.8 37.674884796142578 109.6 37.758766174316406
		 110.4 37.850444793701165 111.2 37.843658447265625 112 37.825347900390625 112.8 37.963668823242195
		 113.6 38.132160186767578 114.4 38.143257141113281 115.2 38.102371215820313 116 38.129138946533203
		 116.8 38.176433563232422 117.6 38.201652526855469 118.4 38.188789367675781 119.2 38.232551574707031
		 120 38.458076477050781 120.8 38.779300689697273 121.6 39.010406494140625 122.4 39.032691955566406
		 123.2 38.908321380615234 124 38.801162719726563 124.8 38.790519714355469 125.6 39.066360473632813
		 126.4 39.344741821289062 127.2 39.529712677001953 128 39.582141876220703 128.8 39.613445281982422
		 129.6 39.634021759033203 130.4 39.713001251220703 131.2 39.734867095947266 132 39.795295715332031
		 132.8 39.951835632324219 133.6 40.121303558349609 134.4 40.149673461914063 135.2 40.068183898925795
		 136 39.994327545166023 136.8 39.952484130859375 137.6 39.94464111328125 138.4 39.977561950683594
		 139.2 39.975360870361328 140 39.943325042724609 140.8 39.937946319580078 141.6 39.88677978515625
		 142.4 39.760299682617188 143.2 39.657676696777344 144 39.600540161132813 144.8 39.52972412109375
		 145.6 39.514202117919922 146.4 39.588756561279297 147.2 39.560260772705078 148 39.409637451171875
		 148.8 39.347438812255859 149.6 39.337127685546875 150.4 39.217380523681641 151.2 39.069828033447266
		 152 39.0380859375 152.8 39.115165710449219 153.6 39.228622436523438 154.4 39.304424285888672
		 155.2 39.28265380859375 156 39.207901000976562 156.8 39.195060729980483 157.6 39.192054748535149
		 158.4 39.100112915039063 159.2 39.037197113037109 160 39.112350463867188 160.8 39.200717926025391
		 161.6 39.139835357666016 162.4 39.021636962890625 163.2 39.012458801269531 164 39.046726226806641
		 164.8 39.086883544921875 165.6 39.160633087158203 166.4 39.180397033691413 167.2 39.148193359375
		 168 39.165069580078125 168.8 39.265209197998047 169.6 39.364059448242187 170.4 39.367424011230469
		 171.2 39.283195495605469 172 39.153121948242195 172.8 39.125713348388672 173.6 39.245491027832031
		 174.4 39.238628387451165 175.2 39.017871856689453 176 38.826835632324219 176.8 38.820442199707031
		 177.6 38.946369171142578 178.4 39.015655517578125 179.2 38.892040252685547 180 38.735118865966797
		 180.8 38.708850860595703;
createNode animCurveTA -n "Bip01_L_UpperArm_rotateY17";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 227 ".ktv[0:226]"  0 66.12005615234375 0.8 66.151901245117188
		 1.6 66.098159790039063 2.4 66.027091979980469 3.2 66.032333374023437 4 66.149017333984375
		 4.8 66.324531555175781 5.6 66.44061279296875 6.4 66.413787841796875 7.2 66.345649719238281
		 8 66.369285583496094 8.8 66.441169738769531 9.6 66.498825073242188 10.4 66.540740966796875
		 11.2 66.542572021484375 12 66.502464294433594 12.8 66.497184753417969 13.6 66.566574096679687
		 14.4 66.59576416015625 15.2 66.518051147460938 16 66.505142211914063 16.8 66.66998291015625
		 17.6 66.824638366699219 18.4 66.78900146484375 19.2 66.669532775878906 20 66.657722473144531
		 20.8 66.69915771484375 21.6 66.647605895996094 22.4 66.576393127441406 23.2 66.588493347167969
		 24 66.637969970703125 24.8 66.655105590820313 25.6 66.621200561523438 26.4 66.589302062988281
		 27.2 66.570213317871094 28 66.508453369140625 28.8 66.466407775878906 29.6 66.503257751464844
		 30.4 66.525123596191406 31.2 66.508460998535156 32 66.524803161621094 32.8 66.551666259765625
		 33.6 66.552597045898437 34.4 66.556159973144531 35.2 66.546516418457031 36 66.487335205078125
		 36.8 66.437744140625 37.6 66.464530944824219 38.4 66.513618469238281 39.2 66.515579223632812
		 40 66.445640563964844 40.8 66.416923522949219 41.6 66.487380981445313 42.4 66.64556884765625
		 43.2 66.797309875488281 44 66.8504638671875 44.8 66.816535949707031 45.6 66.751892089843764
		 46.4 66.707107543945313 47.2 66.7227783203125 48 66.793663024902344 48.8 66.883270263671875
		 49.6 67.004913330078125 50.4 67.143913269042969 51.2 67.177436828613281 52 67.099884033203125
		 52.8 67.093864440917969 53.6 67.248687744140625 54.4 67.423439025878906 55.2 67.459877014160156
		 56 67.369117736816406 56.8 67.230880737304688 57.6 67.146087646484375 58.4 67.195457458496094
		 59.2 67.318565368652344 60 67.45904541015625 60.8 67.599044799804687 61.6 67.682395935058594
		 62.4 67.759231567382812 63.2 67.921676635742187 64 68.118659973144531 64.8 68.182456970214844
		 65.6 68.074287414550781 66.4 68.00238037109375 67.2 68.053947448730469 68 68.0986328125
		 68.8 68.068191528320313 69.6 67.975875854492188 70.4 67.952354431152344 71.2 68.118522644042969
		 72 68.318389892578125 72.8 68.386634826660156 73.6 68.396705627441406 74.4 68.445030212402344
		 75.2 68.518516540527344 76 68.612960815429687 76.8 68.741401672363281 77.6 68.827522277832031
		 78.4 68.805198669433594 79.2 68.711181640625 80 68.613433837890625 80.8 68.574317932128906
		 81.6 68.598258972167969 82.4 68.644485473632813 83.2 68.721458435058594 84 68.83258056640625
		 84.8 68.908164978027344 85.6 68.898536682128906 86.4 68.858596801757813 87.2 68.892387390136719
		 88 69.030670166015625 88.8 69.168174743652358 89.6 69.198173522949219 90.4 69.182289123535156
		 91.2 69.196372985839844 92 69.192680358886719 92.8 69.171661376953125 93.6 69.19708251953125
		 94.4 69.241378784179688 95.2 69.246543884277344 96 69.233306884765625 96.8 69.2222900390625
		 97.6 69.209426879882813 98.4 69.213394165039062 99.2 69.229644775390625 100 69.249671936035156
		 100.8 69.246238708496094 101.6 69.184463500976563 102.4 69.12998199462892 103.2 69.14263916015625
		 104 69.188819885253906 104.8 69.226005554199219 105.6 69.257987976074219 106.4 69.301918029785156
		 107.2 69.324432373046875 108 69.309959411621094 108.8 69.287643432617188 109.6 69.257568359375
		 110.4 69.21905517578125 111.2 69.203689575195313 112 69.199226379394531 112.8 69.146018981933594
		 113.6 69.069358825683594 114.4 69.048881530761719 115.2 69.064659118652344 116 69.055084228515625
		 116.8 69.01904296875 117.6 68.981307983398438 118.4 68.957466125488281 119.2 68.916770935058608
		 120 68.816909790039063 120.8 68.681877136230469 121.6 68.609779357910156 122.4 68.623023986816406
		 123.2 68.672988891601563 124 68.70261383056642 124.8 68.7001953125 125.6 68.61517333984375
		 126.4 68.504547119140625 127.2 68.449302673339844 128 68.453788757324219 128.8 68.44091796875
		 129.6 68.403396606445313 130.4 68.346977233886719 131.2 68.340316772460937 132 68.339324951171875
		 132.8 68.306243896484375 133.6 68.243095397949219 134.4 68.211235046386719 135.2 68.220169067382812
		 136 68.219505310058594 136.8 68.180168151855469 137.6 68.160446166992187 138.4 68.11627197265625
		 139.2 68.079818725585938 140 68.067100524902344 140.8 68.05029296875 141.6 68.037155151367188
		 142.4 68.0382080078125 143.2 68.042915344238281 144 68.058074951171889 144.8 68.081741333007813
		 145.6 68.067649841308608 146.4 68.006134033203125 147.2 67.989021301269531 148 68.045089721679688
		 148.8 68.064796447753906 149.6 68.029983520507812 150.4 68.04254150390625 151.2 68.090133666992188
		 152 68.083480834960937 152.8 68.0242919921875 153.6 67.959793090820312 154.4 67.917556762695312
		 155.2 67.922012329101563 156 67.961761474609375 156.8 67.987060546875 157.6 68.004295349121094
		 158.4 68.043846130371094 159.2 68.067039489746094 160 68.036933898925781 160.8 67.993423461914063
		 161.6 67.997039794921875 162.4 68.022109985351563 163.2 68.008697509765625 164 67.980613708496094
		 164.8 67.959228515625 165.6 67.929229736328125 166.4 67.901374816894531 167.2 67.867027282714844
		 168 67.81463623046875 168.8 67.751922607421875 169.6 67.695327758789063 170.4 67.658035278320313
		 171.2 67.624763488769531 172 67.601699829101563 172.8 67.577079772949219 173.6 67.526580810546875
		 174.4 67.50555419921875 175.2 67.534149169921875 176 67.550788879394531 176.8 67.515625
		 177.6 67.425537109375 178.4 67.343070983886719 179.2 67.334716796875 180 67.340080261230469
		 180.8 67.307846069335938;
createNode animCurveTA -n "Bip01_L_UpperArm_rotateZ17";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 227 ".ktv[0:226]"  0 11.278805732727053 0.8 11.256917953491213
		 1.6 11.39272403717041 2.4 11.539828300476074 3.2 11.559769630432127 4 11.363541603088381
		 4.8 10.91619396209717 5.6 10.517409324645996 6.4 10.558354377746582 7.2 10.713850021362305
		 8 10.53596115112305 8.8 10.268093109130859 9.6 10.17151927947998 10.4 10.168643951416016
		 11.2 10.196000099182127 12 10.246249198913574 12.8 10.18897819519043 13.6 9.9638957977294922
		 14.4 9.9441270828247088 15.2 10.308176040649414 16 10.427238464355469 16.8 9.8684310913085938
		 17.6 9.25396728515625 18.4 9.3456850051879883 19.2 9.8652677536010742 20 10.064028739929199
		 20.8 10.01937770843506 21.6 10.233198165893556 22.4 10.510780334472656 23.2 10.496977806091309
		 24 10.338798522949221 24.8 10.311517715454102 25.6 10.497024536132812 26.4 10.722070693969728
		 27.2 10.88569164276123 28 11.084322929382324 28.8 11.192264556884766 29.6 11.083767890930176
		 30.4 10.986993789672852 31.2 11.005250930786133 32 10.976067543029783 32.8 10.902666091918944
		 33.6 10.871086120605469 34.4 10.875351905822754 35.2 11.015969276428224 36 11.292367935180664
		 36.8 11.360647201538086 37.6 11.100274085998535 38.4 10.906959533691406 39.2 11.072749137878418
		 40 11.439944267272947 40.8 11.628813743591309 41.6 11.527976989746094 42.4 11.218082427978516
		 43.2 10.903933525085447 44 10.795910835266112 44.8 10.901121139526367 45.6 11.17068576812744
		 46.4 11.499456405639648 47.2 11.627419471740724 48 11.586870193481444 48.8 11.71389865875244
		 49.6 11.871936798095705 50.4 11.809591293334961 51.2 11.842740058898926 52 12.074921607971191
		 52.8 12.131788253784181 53.6 11.986573219299316 54.4 11.98130989074707 55.2 12.155542373657228
		 56 12.314816474914554 56.8 12.431626319885254 57.6 12.376392364501951 58.4 12.032209396362305
		 59.2 11.731070518493652 60 11.697304725646973 60.8 11.738327026367188 61.6 11.808941841125488
		 62.4 11.967090606689451 63.2 12.040255546569824 64 11.809773445129396 64.8 11.352169990539553
		 65.6 10.794615745544434 66.4 9.9479665756225586 67.2 8.833704948425293 68 7.8484001159667969
		 68.8 7.3067755699157715 69.6 7.336634635925293 70.4 7.4835553169250524 71.2 7.0686688423156738
		 72 6.3635234832763672 72.8 6.0518012046813965 73.6 6.0321540832519531 74.4 5.8906350135803223
		 75.2 5.6587419509887695 76 5.4521174430847168 76.8 5.194028377532959 77.6 5.0335903167724609
		 78.4 5.1703057289123535 79.2 5.4584331512451181 80 5.7179841995239258 80.8 5.854853630065918
		 81.6 5.8819127082824707 82.4 6.0100998878479004 83.2 6.1589131355285645 84 6.0267224311828613
		 84.8 5.7518739700317383 85.6 5.5880446434020996 86.4 5.4716463088989258 87.2 5.3345723152160645
		 88 5.2098379135131836 88.8 5.1292815208435059 89.6 5.063134193420411 90.4 4.834104061126709
		 91.2 4.516148567199707 92 4.4524717330932617 92.8 4.551271915435791 93.6 4.4960942268371582
		 94.4 4.4148073196411133 95.2 4.4970293045043945 96 4.5231189727783203 96.8 4.4523406028747559
		 97.6 4.4751100540161133 98.4 4.4259271621704102 99.2 4.1727046966552734 100 3.8926820755004887
		 100.8 3.8499391078948975 101.6 4.1033849716186523 102.4 4.2622928619384766 103.2 4.0755782127380371
		 104 3.7461750507354736 104.8 3.4819827079772949 105.6 3.23638916015625 106.4 2.9251511096954346
		 107.2 2.7354934215545654 108 2.7699823379516602 108.8 2.8725786209106445 109.6 3.0246319770812988
		 110.4 3.1967284679412842 111.2 3.2230396270751953 112 3.22655200958252 112.8 3.4750430583953857
		 113.6 3.7799839973449707 114.4 3.834259986877441 115.2 3.7995383739471444 116 3.8763420581817631
		 116.8 3.9914555549621586 117.6 4.0702180862426758 118.4 4.0846447944641113 119.2 4.1833896636962891
		 120 4.563568115234375 120.8 5.1030101776123047 121.6 5.4597444534301758 122.4 5.4671449661254883
		 123.2 5.2464199066162109 124 5.0627827644348145 124.8 5.0341963768005371 125.6 5.4215359687805176
		 126.4 5.8506298065185547 127.2 6.1041955947875977 128 6.1330075263977051 128.8 6.1506357192993173
		 129.6 6.1809659004211426 130.4 6.2718081474304199 131.2 6.283566951751709 132 6.3396453857421875
		 132.8 6.5506887435913086 133.6 6.806337833404541 134.4 6.8540997505187988 135.2 6.7298502922058114
		 136 6.6323161125183105 136.8 6.5800027847290039 137.6 6.6081185340881348 138.4 6.7096762657165527
		 139.2 6.7544140815734863 140 6.7415242195129395 140.8 6.769505500793457 141.6 6.7347321510314941
		 142.4 6.5904455184936523 143.2 6.4757423400878906 144 6.414341926574707 144.8 6.3322262763977051
		 145.6 6.3546419143676758 146.4 6.5299930572509766 147.2 6.5316376686096191 148 6.3159737586975098
		 148.8 6.2469377517700195 149.6 6.2844223976135263 150.4 6.1322813034057617 151.2 5.9110894203186044
		 152 5.882746696472168 152.8 6.0378999710083008 153.6 6.2436981201171875 154.4 6.3787727355957031
		 155.2 6.3449015617370605 156 6.211578369140625 156.8 6.1705894470214844 157.6 6.150066852569581
		 158.4 5.9976658821105957 159.2 5.8958353996276855 160 6.0135979652404785 160.8 6.159325122833252
		 161.6 6.0749320983886719 162.4 5.9019021987915039 163.2 5.9098019599914551 164 5.9940915107727051
		 164.8 6.0854601860046387 165.6 6.230961799621582 166.4 6.3062071800231934 167.2 6.321291446685791
		 168 6.4185276031494141 168.8 6.63675880432129 169.6 6.8526582717895508 170.4 6.9321846961975098
		 171.2 6.8968753814697266 172 6.7944545745849609 172.8 6.8305439949035653 173.6 7.085301399230957
		 174.4 7.150428295135498 175.2 6.8936119079589844 176 6.6902880668640137 176.8 6.7787141799926758
		 177.6 7.0890583992004395 178.4 7.3177719116210973 179.2 7.2295584678649902 180 7.0870461463928223
		 180.8 7.1497573852539062;
createNode animCurveTA -n "Bip01_L_Forearm_rotateX17";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 227 ".ktv[0:226]"  0 0.18810364603996277 0.8 0.1869640052318573
		 1.6 0.19092862308025363 2.4 0.19713917374610901 3.2 0.20029303431510925 4 0.19689904153347015
		 4.8 0.18808665871620175 5.6 0.18072187900543213 6.4 0.18084363639354703 7.2 0.18404921889305115
		 8 0.18386909365653992 8.8 0.18296436965465543 9.6 0.18401806056499481 10.4 0.18547806143760681
		 11.2 0.18786671757698059 12 0.19248872995376587 12.8 0.1955312192440033 13.6 0.19374361634254456
		 14.4 0.19371737539768219 15.2 0.19981877505779264 16 0.20294070243835449 16.8 0.19667039811611178
		 17.6 0.19079837203025815 18.4 0.19548439979553223 19.2 0.20578089356422424 20 0.21058373153209689
		 20.8 0.21202573180198667 21.6 0.21828895807266235 22.4 0.22537317872047427 23.2 0.22688727080821991
		 24 0.22569823265075681 24.8 0.2267216593027114 25.6 0.23082694411277771 26.4 0.23392887413501745
		 27.2 0.23468342423439023 28 0.23652319610118863 28.8 0.23680776357650757 29.6 0.23313248157501215
		 30.4 0.23156279325485224 31.2 0.23347477614879605 32 0.23376812040805814 32.8 0.23243217170238495
		 33.6 0.23222473263740537 34.4 0.23339250683784488 35.2 0.23629733920097351 36 0.24045215547084808
		 36.8 0.24107994139194489 37.6 0.2357495129108429 38.4 0.2300595939159393 39.2 0.23026351630687711
		 40 0.23536312580108648 40.8 0.23925323784351343 41.6 0.23739615082740784 42.4 0.23000511527061465
		 43.2 0.22173106670379641 44 0.21710087358951569 44.8 0.21789687871932983 45.6 0.22355014085769653
		 46.4 0.22936168313026428 47.2 0.23014552891254428 48 0.22761175036430359 48.8 0.22682011127471929
		 49.6 0.22570729255676272 50.4 0.22176371514797211 51.2 0.22054171562194824 52 0.22302620112895971
		 52.8 0.22312532365322113 53.6 0.22000874578952792 54.4 0.21820811927318573 55.2 0.21979816257953644
		 56 0.22310015559196472 56.8 0.22716479003429413 57.6 0.22856034338474271 58.4 0.22463293373584745
		 59.2 0.22113083302974701 60 0.22109019756317139 60.8 0.22071537375450137 61.6 0.22114704549312592
		 62.4 0.22515954077243805 63.2 0.22795179486274719 64 0.22359426319599152 64.8 0.2155475914478302
		 65.6 0.20876225829124451 66.4 0.19895754754543304 67.2 0.18441899120807648 68 0.17169153690338135
		 68.8 0.16704057157039642 69.6 0.17248186469078064 70.4 0.17938454449176788 71.2 0.17563204467296603
		 72 0.16554859280586243 72.8 0.16064663231372833 73.6 0.1608402281999588 74.4 0.1605355441570282
		 75.2 0.1590710133314133 76 0.15632465481758118 76.8 0.15091666579246521 77.6 0.14618510007858276
		 78.4 0.14641194045543671 79.2 0.1509394496679306 80 0.15629078447818756 80.8 0.15858073532581329
		 81.6 0.15671665966510773 82.4 0.15396879613399506 83.2 0.15156435966491699 84 0.14787013828754425
		 84.8 0.14439761638641355 85.6 0.14304237067699432 86.4 0.14217247068881989 87.2 0.13928669691085815
		 88 0.13418616354465485 88.8 0.1304144412279129 89.6 0.13048593699932098 90.4 0.1308460533618927
		 91.2 0.12970565259456637 92 0.13084174692630768 92.8 0.13388323783874512 93.6 0.13460057973861694
		 94.4 0.13400626182556152 95.2 0.13499562442302704 96 0.13581876456737518 96.8 0.13561105728149414
		 97.6 0.13587361574172976 98.4 0.13600289821624756 99.2 0.13631382584571838 100 0.13782614469528198
		 100.8 0.14102718234062195 101.6 0.14526090025901794 102.4 0.14600321650505066 103.2 0.14202439785003662
		 104 0.13784576952457428 104.8 0.13622528314590457 105.6 0.13572698831558228 106.4 0.13430072367191315
		 107.2 0.13468411564826965 108 0.1356208324432373 108.8 0.13756908476352692 109.6 0.13992886245250702
		 110.4 0.14216315746307373 111.2 0.14254316687583923 112 0.14233630895614624 112.8 0.14454163610935211
		 113.6 0.14669331908226013 114.4 0.14490038156509399 115.2 0.14229288697242737 116 0.14240396022796631
		 116.8 0.14341561496257782 117.6 0.14360159635543823 118.4 0.14332683384418488 119.2 0.14446085691452026
		 120 0.14888857305049896 120.8 0.15502458810806274 121.6 0.15783914923667908 122.4 0.15397974848747251
		 123.2 0.1473049521446228 124 0.14301688969135284 124.8 0.14187131822109222 125.6 0.14487878978252411
		 126.4 0.15028432011604309 127.2 0.15201881527900696 128 0.14998002350330353 128.8 0.14890369772911072
		 129.6 0.1497097909450531 130.4 0.14953583478927615 131.2 0.14873671531677246 132 0.14849385619163513
		 132.8 0.15065933763980863 133.6 0.15445025265216827 134.4 0.15615834295749664 135.2 0.15510889887809751
		 136 0.15443339943885803 136.8 0.15529240667819977 137.6 0.1569141298532486 138.4 0.16092006862163544
		 139.2 0.16288721561431885 140 0.16381190717220306 140.8 0.16379889845848083 141.6 0.1620233952999115
		 142.4 0.15930269658565524 143.2 0.15774185955524445 144 0.1566372811794281 144.8 0.15458609163761139
		 145.6 0.15447139739990234 146.4 0.15824013948440552 147.2 0.16040767729282379 148 0.15834677219390869
		 148.8 0.15720230340957642 149.6 0.15724627673625946 150.4 0.15410700440406799 151.2 0.15056808292865753
		 152 0.15150351822376251 152.8 0.15540565550327301 153.6 0.15885914862155914 154.4 0.16053764522075653
		 155.2 0.15989218652248385 156 0.15761815011501312 156.8 0.15577107667922974 157.6 0.1539839506149292
		 158.4 0.15134948492050171 159.2 0.15038011968135834 160 0.15258923172950745 160.8 0.1548641175031662
		 161.6 0.15322950482368469 162.4 0.14942008256912231 163.2 0.14831894636154175 164 0.14975014328956604
		 164.8 0.15165501832962036 165.6 0.15335988998413086 166.4 0.15352441370487213 167.2 0.15290482342243197
		 168 0.15357586741447449 168.8 0.15643247961997986 169.6 0.15979656577110293 170.4 0.16104690730571747
		 171.2 0.16048501431941986 172 0.15960793197154999 172.8 0.16118116676807404 173.6 0.16560904681682587
		 174.4 0.16661094129085541 175.2 0.16220960021018982 176 0.15837690234184265 176.8 0.15820193290710452
		 177.6 0.15985214710235596 178.4 0.1606224924325943 179.2 0.15946078300476074 180 0.1595129668712616
		 180.8 0.16294841468334198;
createNode animCurveTA -n "Bip01_L_Forearm_rotateY17";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 227 ".ktv[0:226]"  0 -1.0244011878967283 0.8 -1.0217622518539429
		 1.6 -1.02916419506073 2.4 -1.0380750894546509 3.2 -1.0404309034347534 4 -1.0337684154510498
		 4.8 -1.0198115110397341 5.6 -1.0092153549194336 6.4 -1.0138435363769531 7.2 -1.0232446193695068
		 8 -1.0212510824203491 8.8 -1.0140235424041748 9.6 -1.0115629434585571 10.4 -1.0137228965759275
		 11.2 -1.0206146240234375 12 -1.0320582389831543 12.8 -1.0387574434280396 13.6 -1.0356149673461914
		 14.4 -1.0380262136459353 15.2 -1.0534845590591433 16 -1.060085654258728 16.8 -1.0451425313949585
		 17.6 -1.0313419103622437 18.4 -1.0410617589950562 19.2 -1.0614843368530271 20 -1.0683584213256836
		 20.8 -1.0673552751541138 21.6 -1.0764667987823486 22.4 -1.0879365205764773 23.2 -1.0894064903259275
		 24 -1.0867822170257568 24.8 -1.0884764194488523 25.6 -1.094745397567749 26.4 -1.0983709096908567
		 27.2 -1.099740743637085 28 -1.1072734594345093 28.8 -1.1138432025909424 29.6 -1.1113663911819458
		 30.4 -1.1094868183135986 31.2 -1.1107527017593384 32 -1.1069574356079102 32.8 -1.1001437902450562
		 33.6 -1.0965049266815186 34.4 -1.0961900949478147 35.2 -1.0999622344970703 36 -1.1076424121856689
		 36.8 -1.1104291677474976 37.6 -1.1028528213500977 38.4 -1.0942585468292236 39.2 -1.0962798595428469
		 40 -1.1068512201309204 40.8 -1.1142455339431765 41.6 -1.1107926368713379 42.4 -1.0978876352310181
		 43.2 -1.0842094421386721 44 -1.0774838924407959 44.8 -1.0806736946105957 45.6 -1.0926259756088257
		 46.4 -1.1037905216217041 47.2 -1.1041785478591919 48 -1.0979151725769043 48.8 -1.0955983400344849
		 49.6 -1.0933167934417725 50.4 -1.0855242013931274 51.2 -1.0829991102218628 52 -1.0882394313812256
		 52.8 -1.0893622636795044 53.6 -1.0842812061309814 54.4 -1.0811656713485718 55.2 -1.0853629112243652
		 56 -1.0931966304779053 56.8 -1.1018738746643066 57.6 -1.1036659479141235 58.4 -1.0931135416030884
		 59.2 -1.082823634147644 60 -1.0810815095901487 60.8 -1.0813002586364746 61.6 -1.0854287147521973
		 62.4 -1.0974527597427368 63.2 -1.1060440540313721 64 -1.0988237857818604 64.8 -1.0836145877838137
		 65.6 -1.0717399120330813 66.4 -1.0541778802871704 67.2 -1.0249679088592527 68 -0.99575638771057151
		 68.8 -0.98040884733200062 69.6 -0.98566389083862282 70.4 -0.9950445294380188 71.2 -0.98512274026870716
		 72 -0.96638411283493042 72.8 -0.96125560998916626 73.6 -0.96462124586105347 74.4 -0.96296292543411244
		 75.2 -0.9580422043800354 76 -0.95224231481552124 76.8 -0.94289094209671021 77.6 -0.93683624267578147
		 78.4 -0.94181567430496205 79.2 -0.95403873920440641 80 -0.96599197387695324 80.8 -0.9710838794708252
		 81.6 -0.96864765882492065 82.4 -0.96638011932373047 83.2 -0.96465277671813965 84 -0.95657700300216675
		 84.8 -0.94666278362274181 85.6 -0.94352507591247547 86.4 -0.94408363103866544 87.2 -0.93916761875152588
		 88 -0.92622053623199463 88.8 -0.9144876003265382 89.6 -0.91094589233398438 90.4 -0.90713298320770275
		 91.2 -0.90035432577133179 92 -0.90145576000213634 92.8 -0.90797704458236694 93.6 -0.907792627811432
		 94.4 -0.90490418672561646 95.2 -0.90837627649307262 96 -0.91351276636123668 96.8 -0.91741782426834095
		 97.6 -0.9228166937828064 98.4 -0.92519623041152965 99.2 -0.92305040359497059 100 -0.9204363226890564
		 100.8 -0.9235767126083374 101.6 -0.93459713459014915 102.4 -0.94174665212631237 103.2 -0.9381641149520874
		 104 -0.93113273382186901 104.8 -0.9266102910041808 105.6 -0.92252337932586692 106.4 -0.91564089059829712
		 107.2 -0.90973299741744995 108 -0.91136640310287476 108.8 -0.91576409339904785 109.6 -0.92120039463043213
		 110.4 -0.92677581310272206 111.2 -0.92802566289901722 112 -0.92755788564681996 112.8 -0.93340742588043202
		 113.6 -0.94061893224716187 114.4 -0.93944859504699696 115.2 -0.93587505817413341
		 116 -0.93724364042282093 116.8 -0.93905448913574241 117.6 -0.93751543760299683 118.4 -0.93355512619018555
		 119.2 -0.93280988931655884 120 -0.94205093383789063 120.8 -0.95809054374694835 121.6 -0.96781104803085316
		 122.4 -0.96149134635925282 123.2 -0.94687920808792114 124 -0.93662333488464355 124.8 -0.93357849121093761
		 125.6 -0.94080322980880748 126.4 -0.95404618978500366 127.2 -0.95838916301727284
		 128 -0.95383983850479137 128.8 -0.95161902904510509 129.6 -0.95209872722625732 130.4 -0.95198243856430054
		 131.2 -0.95081520080566417 132 -0.95024436712264992 132.8 -0.95413178205490112 133.6 -0.96203452348709106
		 134.4 -0.96605294942855835 135.2 -0.96354389190673828 136 -0.96027803421020508 136.8 -0.95854741334915172
		 137.6 -0.95842158794403076 138.4 -0.96094417572021484 139.2 -0.96556556224822976
		 140 -0.96878898143768311 140.8 -0.97118496894836437 141.6 -0.97095102071762085 142.4 -0.96824979782104492
		 143.2 -0.96621835231780995 144 -0.96376174688339233 144.8 -0.95921152830123901 145.6 -0.95971614122390747
		 146.4 -0.96871715784072865 147.2 -0.97271847724914551 148 -0.96641737222671509 148.8 -0.96334081888198853
		 149.6 -0.96446353197097756 150.4 -0.95858842134475697 151.2 -0.9508740305900576 152 -0.95196449756622314
		 152.8 -0.95900619029998779 153.6 -0.96632122993469227 154.4 -0.97222805023193359
		 155.2 -0.9734508991241454 156 -0.96885675191879272 156.8 -0.96354722976684559 157.6 -0.95927155017852783
		 158.4 -0.95433753728866577 159.2 -0.95216685533523571 160 -0.95527213811874379 160.8 -0.95893025398254395
		 161.6 -0.95632666349411022 162.4 -0.95002400875091553 163.2 -0.94801634550094604
		 164 -0.94892877340316772 164.8 -0.95106160640716564 165.6 -0.95539045333862305 166.4 -0.95786875486373901
		 167.2 -0.95794177055358887 168 -0.95927047729492188 168.8 -0.96452724933624256 169.6 -0.97097229957580566
		 170.4 -0.97283607721328735 171.2 -0.97056758403778076 172 -0.96699476242065452 172.8 -0.96845948696136475
		 173.6 -0.97700715065002464 174.4 -0.97912633419036876 175.2 -0.970201075077057 176 -0.96305161714553855
		 176.8 -0.96473556756973267 177.6 -0.97158056497573841 178.4 -0.97637999057769764
		 179.2 -0.9746915102005006 180 -0.97274142503738414 180.8 -0.97612202167510986;
createNode animCurveTA -n "Bip01_L_Forearm_rotateZ17";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 227 ".ktv[0:226]"  0 -18.306978225708008 0.8 -18.231189727783203
		 1.6 -18.452457427978516 2.4 -18.735752105712891 3.2 -18.82841682434082 4 -18.630693435668945
		 4.8 -18.197990417480469 5.6 -17.862071990966797 6.4 -17.974325180053711 7.2 -18.235044479370117
		 8 -18.185371398925785 8.8 -18.002574920654297 9.6 -17.955148696899414 10.4 -18.023250579833984
		 11.2 -18.214054107666016 12 -18.538946151733398 12.8 -18.732969284057617 13.6 -18.637813568115231
		 14.4 -18.695465087890625 15.2 -19.132648468017575 16 -19.32505989074707 16.8 -18.898252487182617
		 17.6 -18.502704620361328 18.4 -18.787338256835937 19.2 -19.390117645263672 20 -19.608753204345703
		 20.8 -19.601556777954105 21.6 -19.890647888183594 22.4 -20.245138168334961 23.2 -20.297855377197266
		 24 -20.222126007080082 24.8 -20.274402618408203 25.6 -20.47132682800293 26.4 -20.594440460205082
		 27.2 -20.635871887207031 28 -20.834115982055664 28.8 -20.99090576171875 29.6 -20.88831901550293
		 30.4 -20.825618743896484 31.2 -20.877986907958984 32 -20.792734146118164 32.8 -20.61693000793457
		 33.6 -20.529682159423828 34.4 -20.537277221679688 35.2 -20.661386489868164 36 -20.891923904418945
		 36.8 -20.964506149291992 37.6 -20.721225738525391 38.4 -20.450496673583984 39.2 -20.499843597412109
		 40 -20.809017181396484 40.8 -21.02943229675293 41.6 -20.926406860351559 42.4 -20.534397125244141
		 43.2 -20.113624572753903 44 -19.900428771972656 44.8 -19.984807968139648 45.6 -20.333597183227539
		 46.4 -20.665105819702148 47.2 -20.683568954467773 48 -20.506000518798832 48.8 -20.442087173461911
		 49.6 -20.375289916992188 50.4 -20.144842147827148 51.2 -20.070911407470703 52 -20.223993301391602
		 52.8 -20.251453399658203 53.6 -20.094488143920895 54.4 -20.000234603881836 55.2 -20.117786407470703
		 56 -20.341791152954105 56.8 -20.594076156616211 57.6 -20.652788162231445 58.4 -20.358047485351563
		 59.2 -20.074050903320313 60 -20.032678604125977 60.8 -20.033184051513672 61.6 -20.135726928710938
		 62.4 -20.466512680053711 63.2 -20.702316284179687 64 -20.480985641479492 64.8 -20.027666091918945
		 65.6 -19.668378829956055 66.4 -19.139642715454105 67.2 -18.280179977416992 68 -17.440450668334961
		 68.8 -17.021305084228516 69.6 -17.207786560058594 70.4 -17.509990692138672 71.2 -17.230611801147461
		 72 -16.669448852539063 72.8 -16.491552352905273 73.6 -16.573879241943359 74.4 -16.530960083007813
		 75.2 -16.396883010864258 76 -16.227357864379883 76.8 -15.943929672241206 77.6 -15.747385978698729
		 78.4 -15.870712280273439 79.2 -16.213676452636719 80 -16.558708190917969 80.8 -16.706232070922852
		 81.6 -16.62782096862793 82.4 -16.545303344726562 83.2 -16.479238510131836 84 -16.245471954345703
		 84.8 -15.968868255615233 85.6 -15.87869453430176 86.4 -15.884284019470218 87.2 -15.735984802246092
		 88 -15.369041442871092 88.8 -15.044615745544434 89.6 -14.958451271057127 90.4 -14.868655204772946
		 91.2 -14.69133949279785 92 -14.729966163635254 92.8 -14.919270515441896 93.6 -14.922531127929687
		 94.4 -14.846114158630368 95.2 -14.940768241882324 96 -15.073988914489744 96.8 -15.16713905334473
		 97.6 -15.30184268951416 98.4 -15.361547470092775 99.2 -15.31220626831055 100 -15.263412475585938
		 100.8 -15.371987342834473 101.6 -15.683511734008789 102.4 -15.865223884582518 103.2 -15.737589836120607
		 104 -15.524580001831056 104.8 -15.398148536682124 105.6 -15.293079376220703 106.4 -15.11079216003418
		 107.2 -14.970262527465822 108 -15.01984119415283 108.8 -15.14665603637695 109.6 -15.303089141845703
		 110.4 -15.461379051208494 111.2 -15.49567127227783 112 -15.482163429260252 112.8 -15.647027015686035
		 113.6 -15.844183921813968 114.4 -15.797579765319822 115.2 -15.684263229370124 116 -15.718578338623043
		 116.8 -15.772943496704102 117.6 -15.73741626739502 118.4 -15.638222694396973 119.2 -15.631679534912106
		 120 -15.901782989501951 120.8 -16.354215621948242 121.6 -16.619058609008789 122.4 -16.425865173339844
		 123.2 -16.002557754516605 124 -15.709510803222656 124.8 -15.623868942260742 125.6 -15.830184936523439
		 126.4 -16.207124710083008 127.2 -16.330425262451172 128 -16.199161529541016 128.8 -16.134189605712891
		 129.6 -16.154079437255859 130.4 -16.149269104003906 131.2 -16.113142013549805 132 -16.096885681152344
		 132.8 -16.213150024414063 133.6 -16.443729400634769 134.4 -16.558933258056641 135.2 -16.48756217956543
		 136 -16.401252746582031 136.8 -16.368358612060547 137.6 -16.382364273071289 138.4 -16.48680305480957
		 139.2 -16.618949890136719 140 -16.706413269042969 140.8 -16.764068603515625 141.6 -16.739315032958984
		 142.4 -16.645120620727539 143.2 -16.579305648803711 144 -16.508134841918949 144.8 -16.37687873840332
		 145.6 -16.387712478637695 146.4 -16.64494514465332 147.2 -16.764686584472656 148 -16.590490341186523
		 148.8 -16.504108428955078 149.6 -16.531753540039063 150.4 -16.356834411621094 151.2 -16.133014678955082
		 152 -16.169118881225586 152.8 -16.380182266235352 153.6 -16.593725204467773 154.4 -16.754091262817383
		 155.2 -16.777008056640625 156 -16.641910552978516 156.8 -16.493921279907227 157.6 -16.372024536132812
		 158.4 -16.225181579589844 159.2 -16.162656784057617 160 -16.260726928710937 160.8 -16.373144149780273
		 161.6 -16.29289436340332 162.4 -16.100671768188477 163.2 -16.04062652587891 164 -16.077402114868164
		 164.8 -16.148763656616211 165.6 -16.271371841430664 166.4 -16.333217620849609 167.2 -16.32866096496582
		 168 -16.368024826049805 168.8 -16.524848937988281 169.6 -16.715948104858398 170.4 -16.77427864074707
		 171.2 -16.713457107543945 172 -16.617849349975586 172.8 -16.66999626159668 173.6 -16.923406600952148
		 174.4 -16.98529052734375 175.2 -16.72288703918457 176 -16.509462356567383 176.8 -16.548479080200195
		 177.6 -16.731498718261719 178.4 -16.855724334716797 179.2 -16.802806854248047 180 -16.756099700927734
		 180.8 -16.874134063720703;
createNode animCurveTA -n "Bip01_L_Hand_rotateX7";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 227 ".ktv[0:226]"  0 -90.133316040039063 0.8 -90.133316040039063
		 1.6 -90.133316040039063 2.4 -90.133308410644531 3.2 -90.133316040039063 4 -90.133316040039063
		 4.8 -90.133308410644531 5.6 -90.133316040039063 6.4 -90.133316040039063 7.2 -90.133316040039063
		 8 -90.133316040039063 8.8 -90.133316040039063 9.6 -90.133316040039063 10.4 -90.133308410644531
		 11.2 -90.133316040039063 12 -90.133316040039063 12.8 -90.133316040039063 13.6 -90.133316040039063
		 14.4 -90.133316040039063 15.2 -90.133316040039063 16 -90.133331298828125 16.8 -90.133316040039063
		 17.6 -90.133316040039063 18.4 -90.133316040039063 19.2 -90.133316040039063 20 -90.133331298828125
		 20.8 -90.133316040039063 21.6 -90.133316040039063 22.4 -90.133316040039063 23.2 -90.133316040039063
		 24 -90.133316040039063 24.8 -90.133308410644531 25.6 -90.133316040039063 26.4 -90.133316040039063
		 27.2 -90.133316040039063 28 -90.133316040039063 28.8 -90.133316040039063 29.6 -90.133316040039063
		 30.4 -90.133316040039063 31.2 -90.133331298828125 32 -90.133316040039063 32.8 -90.133316040039063
		 33.6 -90.133308410644531 34.4 -90.133316040039063 35.2 -90.133316040039063 36 -90.133316040039063
		 36.8 -90.133316040039063 37.6 -90.133316040039063 38.4 -90.133316040039063 39.2 -90.133316040039063
		 40 -90.133316040039063 40.8 -90.133308410644531 41.6 -90.133308410644531 42.4 -90.133308410644531
		 43.2 -90.133316040039063 44 -90.133308410644531 44.8 -90.133316040039063 45.6 -90.133316040039063
		 46.4 -90.133316040039063 47.2 -90.133316040039063 48 -90.133316040039063 48.8 -90.133316040039063
		 49.6 -90.133316040039063 50.4 -90.133316040039063 51.2 -90.133308410644531 52 -90.133316040039063
		 52.8 -90.133316040039063 53.6 -90.133316040039063 54.4 -90.133308410644531 55.2 -90.133308410644531
		 56 -90.133316040039063 56.8 -90.133316040039063 57.6 -90.133331298828125 58.4 -90.133316040039063
		 59.2 -90.133316040039063 60 -90.133316040039063 60.8 -90.133331298828125 61.6 -90.133316040039063
		 62.4 -90.133331298828125 63.2 -90.133316040039063 64 -90.133331298828125 64.8 -90.133316040039063
		 65.6 -90.133316040039063 66.4 -90.133316040039063 67.2 -90.133308410644531 68 -90.133308410644531
		 68.8 -90.133316040039063 69.6 -90.133331298828125 70.4 -90.133308410644531 71.2 -90.133316040039063
		 72 -90.133316040039063 72.8 -90.133316040039063 73.6 -90.133316040039063 74.4 -90.133308410644531
		 75.2 -90.133308410644531 76 -90.133308410644531 76.8 -90.133308410644531 77.6 -90.133316040039063
		 78.4 -90.133316040039063 79.2 -90.133316040039063 80 -90.133316040039063 80.8 -90.133316040039063
		 81.6 -90.133316040039063 82.4 -90.133308410644531 83.2 -90.133316040039063 84 -90.133316040039063
		 84.8 -90.133316040039063 85.6 -90.133316040039063 86.4 -90.133316040039063 87.2 -90.133316040039063
		 88 -90.133316040039063 88.8 -90.133308410644531 89.6 -90.133308410644531 90.4 -90.133308410644531
		 91.2 -90.133316040039063 92 -90.133316040039063 92.8 -90.133316040039063 93.6 -90.133316040039063
		 94.4 -90.133331298828125 95.2 -90.133308410644531 96 -90.133316040039063 96.8 -90.133316040039063
		 97.6 -90.133316040039063 98.4 -90.133316040039063 99.2 -90.133316040039063 100 -90.133316040039063
		 100.8 -90.133308410644531 101.6 -90.133316040039063 102.4 -90.133308410644531 103.2 -90.133308410644531
		 104 -90.133308410644531 104.8 -90.133316040039063 105.6 -90.133316040039063 106.4 -90.133316040039063
		 107.2 -90.133316040039063 108 -90.133316040039063 108.8 -90.133316040039063 109.6 -90.133308410644531
		 110.4 -90.133316040039063 111.2 -90.133316040039063 112 -90.133316040039063 112.8 -90.133316040039063
		 113.6 -90.133316040039063 114.4 -90.133316040039063 115.2 -90.133316040039063 116 -90.133308410644531
		 116.8 -90.133316040039063 117.6 -90.133316040039063 118.4 -90.133316040039063 119.2 -90.133316040039063
		 120 -90.133316040039063 120.8 -90.133316040039063 121.6 -90.133316040039063 122.4 -90.133316040039063
		 123.2 -90.133316040039063 124 -90.133316040039063 124.8 -90.133308410644531 125.6 -90.133316040039063
		 126.4 -90.133308410644531 127.2 -90.133308410644531 128 -90.133308410644531 128.8 -90.133316040039063
		 129.6 -90.133308410644531 130.4 -90.133316040039063 131.2 -90.133316040039063 132 -90.133308410644531
		 132.8 -90.133308410644531 133.6 -90.133316040039063 134.4 -90.133308410644531 135.2 -90.133316040039063
		 136 -90.133316040039063 136.8 -90.133316040039063 137.6 -90.133316040039063 138.4 -90.133316040039063
		 139.2 -90.133316040039063 140 -90.133308410644531 140.8 -90.133316040039063 141.6 -90.133308410644531
		 142.4 -90.133308410644531 143.2 -90.133316040039063 144 -90.133316040039063 144.8 -90.133316040039063
		 145.6 -90.133316040039063 146.4 -90.133316040039063 147.2 -90.133316040039063 148 -90.133316040039063
		 148.8 -90.133316040039063 149.6 -90.133316040039063 150.4 -90.133316040039063 151.2 -90.133316040039063
		 152 -90.133316040039063 152.8 -90.133316040039063 153.6 -90.133316040039063 154.4 -90.133308410644531
		 155.2 -90.133316040039063 156 -90.133308410644531 156.8 -90.133316040039063 157.6 -90.133316040039063
		 158.4 -90.133308410644531 159.2 -90.133316040039063 160 -90.133308410644531 160.8 -90.133308410644531
		 161.6 -90.133308410644531 162.4 -90.133316040039063 163.2 -90.133316040039063 164 -90.133308410644531
		 164.8 -90.133316040039063 165.6 -90.133316040039063 166.4 -90.133316040039063 167.2 -90.133316040039063
		 168 -90.133316040039063 168.8 -90.133316040039063 169.6 -90.133316040039063 170.4 -90.133316040039063
		 171.2 -90.133316040039063 172 -90.133331298828125 172.8 -90.133316040039063 173.6 -90.133316040039063
		 174.4 -90.133308410644531 175.2 -90.133316040039063 176 -90.133316040039063 176.8 -90.133331298828125
		 177.6 -90.133316040039063 178.4 -90.133316040039063 179.2 -90.133316040039063 180 -90.133316040039063
		 180.8 -90.133316040039063;
createNode animCurveTA -n "Bip01_L_Hand_rotateY7";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 227 ".ktv[0:226]"  0 6.3003978729248047 0.8 6.3003926277160645
		 1.6 6.3004026412963876 2.4 6.3003959655761728 3.2 6.3003945350646973 4 6.3003954887390137
		 4.8 6.3004002571105957 5.6 6.3003964424133301 6.4 6.3003993034362793 7.2 6.3003959655761728
		 8 6.3004031181335449 8.8 6.3003988265991211 9.6 6.3003964424133301 10.4 6.3003888130187988
		 11.2 6.3003978729248047 12 6.3003945350646973 12.8 6.3003940582275391 13.6 6.3003926277160645
		 14.4 6.3003988265991211 15.2 6.3003978729248047 16 6.3003964424133301 16.8 6.3004007339477539
		 17.6 6.3003978729248047 18.4 6.3003935813903809 19.2 6.3003978729248047 20 6.3003969192504883
		 20.8 6.3004002571105957 21.6 6.3003940582275391 22.4 6.3003993034362793 23.2 6.3003964424133301
		 24 6.3003950119018555 24.8 6.3004050254821777 25.6 6.3003973960876465 26.4 6.3003959655761728
		 27.2 6.3003993034362793 28 6.3003940582275391 28.8 6.3004035949707031 29.6 6.3004007339477539
		 30.4 6.3003978729248047 31.2 6.3003950119018555 32 6.3003945350646973 32.8 6.3004031181335449
		 33.6 6.3003964424133301 34.4 6.3003988265991211 35.2 6.3004012107849121 36 6.3004007339477539
		 36.8 6.3003983497619629 37.6 6.3003931045532227 38.4 6.3003959655761728 39.2 6.3004007339477539
		 40 6.3004040718078622 40.8 6.3003983497619629 41.6 6.3004007339477539 42.4 6.3003940582275391
		 43.2 6.3003964424133301 44 6.3003993034362793 44.8 6.3004050254821777 45.6 6.3003907203674316
		 46.4 6.3003921508789063 47.2 6.3004002571105957 48 6.3003973960876465 48.8 6.3004040718078622
		 49.6 6.3003973960876465 50.4 6.3003978729248047 51.2 6.3004007339477539 52 6.3003978729248047
		 52.8 6.3004007339477539 53.6 6.3004002571105957 54.4 6.3004031181335449 55.2 6.3004045486450195
		 56 6.3004050254821777 56.8 6.3003940582275391 57.6 6.3003926277160645 58.4 6.3003969192504883
		 59.2 6.3003964424133301 60 6.3003997802734375 60.8 6.3003993034362793 61.6 6.3003983497619629
		 62.4 6.3003983497619629 63.2 6.3003993034362793 64 6.3003921508789063 64.8 6.3003950119018555
		 65.6 6.3004007339477539 66.4 6.3003964424133301 67.2 6.3003950119018555 68 6.3003983497619629
		 68.8 6.3003954887390137 69.6 6.3003926277160645 70.4 6.3003945350646973 71.2 6.3003988265991211
		 72 6.300391674041748 72.8 6.3003954887390137 73.6 6.3003964424133301 74.4 6.3003973960876465
		 75.2 6.3003983497619629 76 6.3004007339477539 76.8 6.3004035949707031 77.6 6.3003931045532227
		 78.4 6.3003911972045898 79.2 6.3003883361816406 80 6.3003959655761728 80.8 6.3003964424133301
		 81.6 6.3003959655761728 82.4 6.300391674041748 83.2 6.300389289855957 84 6.3003964424133301
		 84.8 6.3004007339477539 85.6 6.3003883361816406 86.4 6.3003983497619629 87.2 6.3003945350646973
		 88 6.3004026412963876 88.8 6.3004016876220703 89.6 6.3003988265991211 90.4 6.3003911972045898
		 91.2 6.3003950119018555 92 6.3003911972045898 92.8 6.3004007339477539 93.6 6.3003878593444833
		 94.4 6.3003897666931152 95.2 6.3003935813903809 96 6.3003983497619629 96.8 6.3003954887390137
		 97.6 6.3003935813903809 98.4 6.3003997802734375 99.2 6.3003926277160645 100 6.3003978729248047
		 100.8 6.3004040718078622 101.6 6.3003973960876465 102.4 6.3003959655761728 103.2 6.3003950119018555
		 104 6.3003988265991211 104.8 6.3004002571105957 105.6 6.3004026412963876 106.4 6.3003964424133301
		 107.2 6.3003978729248047 108 6.3003926277160645 108.8 6.3003935813903809 109.6 6.3004002571105957
		 110.4 6.3003931045532227 111.2 6.3003973960876465 112 6.3003945350646973 112.8 6.3003935813903809
		 113.6 6.3003921508789063 114.4 6.3003959655761728 115.2 6.3003964424133301 116 6.3003931045532227
		 116.8 6.3003959655761728 117.6 6.3003973960876465 118.4 6.300391674041748 119.2 6.3003964424133301
		 120 6.3003997802734375 120.8 6.3003973960876465 121.6 6.3004035949707031 122.4 6.3003978729248047
		 123.2 6.3004021644592285 124 6.3003959655761728 124.8 6.3003954887390137 125.6 6.3003964424133301
		 126.4 6.3003964424133301 127.2 6.3004074096679687 128 6.3003997802734375 128.8 6.3003988265991211
		 129.6 6.3003969192504883 130.4 6.3003926277160645 131.2 6.3004007339477539 132 6.3003969192504883
		 132.8 6.3004026412963876 133.6 6.3004002571105957 134.4 6.3003969192504883 135.2 6.3003993034362793
		 136 6.3003997802734375 136.8 6.3003964424133301 137.6 6.3003969192504883 138.4 6.3003983497619629
		 139.2 6.3004007339477539 140 6.3003973960876465 140.8 6.3003988265991211 141.6 6.3003964424133301
		 142.4 6.3004012107849121 143.2 6.3003997802734375 144 6.3003973960876465 144.8 6.3003888130187988
		 145.6 6.3003978729248047 146.4 6.3003969192504883 147.2 6.3003978729248047 148 6.3003935813903809
		 148.8 6.3003940582275391 149.6 6.3003935813903809 150.4 6.3003950119018555 151.2 6.3003921508789063
		 152 6.3003935813903809 152.8 6.3003959655761728 153.6 6.3003888130187988 154.4 6.3003964424133301
		 155.2 6.3003978729248047 156 6.3004031181335449 156.8 6.3004012107849121 157.6 6.3003945350646973
		 158.4 6.3003988265991211 159.2 6.3003926277160645 160 6.3003931045532227 160.8 6.3003907203674316
		 161.6 6.3003926277160645 162.4 6.3003935813903809 163.2 6.3003978729248047 164 6.3004016876220703
		 164.8 6.3003969192504883 165.6 6.300391674041748 166.4 6.3003935813903809 167.2 6.3003911972045898
		 168 6.3003926277160645 168.8 6.3004016876220703 169.6 6.3003926277160645 170.4 6.3003954887390137
		 171.2 6.300391674041748 172 6.3003921508789063 172.8 6.3003964424133301 173.6 6.3003988265991211
		 174.4 6.3004007339477539 175.2 6.3003969192504883 176 6.3003945350646973 176.8 6.3003911972045898
		 177.6 6.3003997802734375 178.4 6.3003926277160645 179.2 6.3003959655761728 180 6.3003945350646973
		 180.8 6.3003926277160645;
createNode animCurveTA -n "Bip01_L_Hand_rotateZ7";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 227 ".ktv[0:226]"  0 2.5653324127197266 0.8 2.5653338432312016
		 1.6 2.5653319358825684 2.4 2.5653383731842041 3.2 2.5653369426727295 4 2.5653350353240967
		 4.8 2.5653340816497803 5.6 2.5653302669525142 6.4 2.5653302669525142 7.2 2.5653331279754639
		 8 2.5653345584869385 8.8 2.565335750579834 9.6 2.5653297901153564 10.4 2.5653367042541504
		 11.2 2.5653324127197266 12 2.5653300285339355 12.8 2.5653340816497803 13.6 2.5653343200683594
		 14.4 2.5653297901153564 15.2 2.5653338432312016 16 2.5653295516967773 16.8 2.5653378963470463
		 17.6 2.5653345584869385 18.4 2.5653352737426758 19.2 2.5653328895568848 20 2.5653347969055176
		 20.8 2.5653316974639893 21.6 2.5653319358825684 22.4 2.5653336048126221 23.2 2.5653319358825684
		 24 2.565338134765625 24.8 2.5653257369995117 25.6 2.5653266906738281 26.4 2.5653352737426758
		 27.2 2.5653297901153564 28 2.5653281211853027 28.8 2.5653305053710938 29.6 2.5653340816497803
		 30.4 2.5653331279754639 31.2 2.5653319358825684 32 2.5653319358825684 32.8 2.565333366394043
		 33.6 2.5653338432312016 34.4 2.5653274059295654 35.2 2.5653293132781982 36 2.5653331279754639
		 36.8 2.565338134765625 37.6 2.565330982208252 38.4 2.5653321743011475 39.2 2.56532883644104
		 40 2.5653283596038814 40.8 2.5653259754180908 41.6 2.565330982208252 42.4 2.5653305053710938
		 43.2 2.5653331279754639 44 2.5653295516967773 44.8 2.5653343200683594 45.6 2.5653359889984131
		 46.4 2.5653343200683594 47.2 2.5653281211853027 48 2.5653345584869385 48.8 2.5653293132781982
		 49.6 2.5653362274169922 50.4 2.5653359889984131 51.2 2.5653314590454106 52 2.5653352737426758
		 52.8 2.5653338432312016 53.6 2.5653314590454106 54.4 2.5653319358825684 55.2 2.565324068069458
		 56 2.5653319358825684 56.8 2.5653305053710938 57.6 2.5653278827667236 58.4 2.5653326511383057
		 59.2 2.5653359889984131 60 2.5653362274169922 60.8 2.5653350353240967 61.6 2.5653328895568848
		 62.4 2.5653312206268311 63.2 2.5653297901153564 64 2.5653305053710938 64.8 2.5653302669525142
		 65.6 2.5653359889984131 66.4 2.5653350353240967 67.2 2.5653326511383057 68 2.5653347969055176
		 68.8 2.5653376579284668 69.6 2.5653305053710938 70.4 2.5653338432312016 71.2 2.5653319358825684
		 72 2.5653374195098877 72.8 2.5653331279754639 73.6 2.5653262138366699 74.4 2.5653314590454106
		 75.2 2.5653374195098877 76 2.5653326511383057 76.8 2.5653376579284668 77.6 2.5653326511383057
		 78.4 2.5653359889984131 79.2 2.5653312206268311 80 2.5653319358825684 80.8 2.5653359889984131
		 81.6 2.5653307437896729 82.4 2.5653269290924072 83.2 2.5653321743011475 84 2.5653319358825684
		 84.8 2.565335750579834 85.6 2.5653364658355713 86.4 2.5653350353240967 87.2 2.5653283596038814
		 88 2.5653278827667236 88.8 2.5653359889984131 89.6 2.565333366394043 90.4 2.5653324127197266
		 91.2 2.5653338432312016 92 2.5653302669525142 92.8 2.5653293132781982 93.6 2.5653324127197266
		 94.4 2.5653331279754639 95.2 2.5653376579284668 96 2.5653350353240967 96.8 2.5653302669525142
		 97.6 2.5653367042541504 98.4 2.5653319358825684 99.2 2.5653338432312016 100 2.56532883644104
		 100.8 2.565333366394043 101.6 2.5653390884399414 102.4 2.5653367042541504 103.2 2.5653336048126221
		 104 2.56532883644104 104.8 2.5653278827667236 105.6 2.5653278827667236 106.4 2.5653278827667236
		 107.2 2.5653347969055176 108 2.5653338432312016 108.8 2.5653331279754639 109.6 2.5653367042541504
		 110.4 2.5653362274169922 111.2 2.5653362274169922 112 2.5653293132781982 112.8 2.5653355121612549
		 113.6 2.5653362274169922 114.4 2.5653326511383057 115.2 2.5653371810913086 116 2.5653347969055176
		 116.8 2.5653278827667236 117.6 2.5653336048126221 118.4 2.5653295516967773 119.2 2.5653338432312016
		 120 2.5653278827667236 120.8 2.565338134765625 121.6 2.5653314590454106 122.4 2.5653352737426758
		 123.2 2.5653312206268311 124 2.5653338432312016 124.8 2.5653336048126221 125.6 2.5653297901153564
		 126.4 2.5653362274169922 127.2 2.56532883644104 128 2.5653321743011475 128.8 2.5653338432312016
		 129.6 2.5653345584869385 130.4 2.5653307437896729 131.2 2.5653364658355713 132 2.5653316974639893
		 132.8 2.5653336048126221 133.6 2.5653324127197266 134.4 2.5653407573699951 135.2 2.5653290748596191
		 136 2.5653283596038814 136.8 2.5653312206268311 137.6 2.5653276443481445 138.4 2.5653350353240967
		 139.2 2.5653347969055176 140 2.5653338432312016 140.8 2.5653362274169922 141.6 2.5653319358825684
		 142.4 2.5653245449066162 143.2 2.5653326511383057 144 2.5653347969055176 144.8 2.5653338432312016
		 145.6 2.5653336048126221 146.4 2.5653338432312016 147.2 2.5653257369995117 148 2.5653281211853027
		 148.8 2.5653319358825684 149.6 2.5653336048126221 150.4 2.5653328895568848 151.2 2.5653285980224609
		 152 2.56532883644104 152.8 2.5653314590454106 153.6 2.5653331279754639 154.4 2.5653302669525142
		 155.2 2.5653350353240967 156 2.5653297901153564 156.8 2.5653331279754639 157.6 2.5653369426727295
		 158.4 2.5653367042541504 159.2 2.5653338432312016 160 2.5653331279754639 160.8 2.5653345584869385
		 161.6 2.5653278827667236 162.4 2.5653331279754639 163.2 2.5653355121612549 164 2.5653319358825684
		 164.8 2.5653326511383057 165.6 2.5653307437896729 166.4 2.5653278827667236 167.2 2.5653293132781982
		 168 2.5653371810913086 168.8 2.565333366394043 169.6 2.5653340816497803 170.4 2.5653347969055176
		 171.2 2.5653319358825684 172 2.5653343200683594 172.8 2.5653319358825684 173.6 2.5653369426727295
		 174.4 2.5653316974639893 175.2 2.5653283596038814 176 2.5653345584869385 176.8 2.5653338432312016
		 177.6 2.5653297901153564 178.4 2.5653290748596191 179.2 2.56532883644104 180 2.565338134765625
		 180.8 2.5653331279754639;
createNode animCurveTA -n "Bip01_L_Thigh_rotateX17";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 227 ".ktv[0:226]"  0 -6.747051715850831 0.8 -7.0977954864501953
		 1.6 -7.4698224067687988 2.4 -7.8607821464538574 3.2 -8.2777242660522461 4 -8.7026987075805664
		 4.8 -9.1493110656738281 5.6 -9.6038742065429687 6.4 -10.07816219329834 7.2 -10.54939079284668
		 8 -11.010428428649902 8.8 -11.457177162170412 9.6 -11.880457878112791 10.4 -12.281280517578123
		 11.2 -12.641449928283691 12 -12.971921920776367 12.8 -13.266657829284668 13.6 -13.503310203552246
		 14.4 -13.705948829650881 15.2 -13.866567611694336 16 -13.974593162536619 16.8 -14.043605804443359
		 17.6 -14.067702293395996 18.4 -14.057867050170898 19.2 -14.021967887878418 20 -13.943556785583496
		 20.8 -13.836596488952637 21.6 -13.711958885192873 22.4 -13.588169097900392 23.2 -13.450936317443848
		 24 -13.307857513427734 24.8 -13.166445732116699 25.6 -13.027164459228516 26.4 -12.903073310852053
		 27.2 -12.795320510864258 28 -12.694452285766603 28.8 -12.618609428405762 29.6 -12.569842338562012
		 30.4 -12.548395156860352 31.2 -12.524298667907717 32 -12.515228271484377 32.8 -12.509490966796877
		 33.6 -12.525227546691898 34.4 -12.54017162322998 35.2 -12.544133186340332 36 -12.54008960723877
		 36.8 -12.540308952331545 37.6 -12.530964851379396 38.4 -12.510802268981934 39.2 -12.475503921508787
		 40 -12.430150985717772 40.8 -12.366985321044922 41.6 -12.283766746520996 42.4 -12.187734603881836
		 43.2 -12.080937385559082 44 -11.964550971984863 44.8 -11.830406188964844 45.6 -11.67437744140625
		 46.4 -11.512392044067385 47.2 -11.350161552429199 48 -11.17399787902832 48.8 -10.990238189697266
		 49.6 -10.807407379150392 50.4 -10.61129951477051 51.2 -10.416912078857422 52 -10.221541404724119
		 52.8 -10.024723052978516 53.6 -9.8225221633911133 54.4 -9.6200199127197266 55.2 -9.4030666351318359
		 56 -9.1794729232788086 56.8 -8.9489679336547852 57.6 -8.7268772125244141 58.4 -8.5081472396850586
		 59.2 -8.2892541885375977 60 -8.0650596618652344 60.8 -7.8323140144348145 61.6 -7.6036119461059561
		 62.4 -7.3768773078918493 63.2 -7.180058479309082 64 -6.9610280990600586 64.8 -6.750002384185791
		 65.6 -6.5417637825012207 66.4 -6.3607363700866699 67.2 -6.1878504753112802 68 -6.0411653518676758
		 68.8 -5.9114737510681152 69.6 -5.7916994094848633 70.4 -5.6921429634094247 71.2 -5.5963563919067383
		 72 -5.5292563438415527 72.8 -5.4794235229492187 73.6 -5.4398083686828613 74.4 -5.4388518333435059
		 75.2 -5.4374861717224121 76 -5.4693694114685059 76.8 -5.4992308616638184 77.6 -5.5517687797546387
		 78.4 -5.6085138320922852 79.2 -5.6717066764831543 80 -5.7469482421875 80.8 -5.8524065017700195
		 81.6 -5.954531192779541 82.4 -6.0788407325744629 83.2 -6.1854462623596191 84 -6.3039908409118652
		 84.8 -6.4186835289001465 85.6 -6.5263547897338867 86.4 -6.6416482925415039 87.2 -6.7488002777099609
		 88 -6.8396415710449228 88.8 -6.9290351867675781 89.6 -7.0287013053894043 90.4 -7.1141881942749023
		 91.2 -7.2007403373718262 92 -7.2569661140441895 92.8 -7.301772117614747 93.6 -7.3369612693786621
		 94.4 -7.3632440567016593 95.2 -7.4002361297607431 96 -7.414443016052247 96.8 -7.4284586906433105
		 97.6 -7.4258084297180158 98.4 -7.4206724166870108 99.2 -7.4001545906066868 100 -7.3690361976623517
		 100.8 -7.3334918022155762 101.6 -7.3113346099853507 102.4 -7.2669110298156765 103.2 -7.2424864768981942
		 104 -7.206942081451416 104.8 -7.132110595703125 105.6 -7.0725784301757804 106.4 -6.9956159591674805
		 107.2 -6.9527225494384766 108 -6.9037089347839355 108.8 -6.8716888427734375 109.6 -6.8499417304992676
		 110.4 -6.8211455345153809 111.2 -6.7792901992797852 112 -6.7412323951721191 112.8 -6.713010311126709
		 113.6 -6.7077646255493164 114.4 -6.716097354888916 115.2 -6.7204413414001465 116 -6.724921703338623
		 116.8 -6.7319707870483398 117.6 -6.7436637878417969 118.4 -6.7632255554199219 119.2 -6.7730884552001962
		 120 -6.7903003692626953 120.8 -6.7906012535095215 121.6 -6.8051900863647461 122.4 -6.8063921928405762
		 123.2 -6.8166923522949219 124 -6.8318009376525879 124.8 -6.8262820243835449 125.6 -6.8201074600219727
		 126.4 -6.8030047416687012 127.2 -6.7630615234375 128 -6.7481446266174316 128.8 -6.7170262336730966
		 129.6 -6.6656904220581055 130.4 -6.6144366264343262 131.2 -6.5487031936645508 132 -6.478297233581543
		 132.8 -6.4080557823181161 133.6 -6.3134713172912598 134.4 -6.2096524238586426 135.2 -6.1014895439147949
		 136 -5.9797754287719735 136.8 -5.8509855270385742 137.6 -5.7060761451721191 138.4 -5.5688714981079102
		 139.2 -5.4375677108764648 140 -5.2764849662780771 140.8 -5.1276412010192871 141.6 -4.9712300300598145
		 142.4 -4.8354730606079102 143.2 -4.6926946640014648 144 -4.551091194152832 144.8 -4.4241867065429687
		 145.6 -4.2946863174438477 146.4 -4.1745295524597177 147.2 -4.0639896392822266 148 -3.9535865783691406
		 148.8 -3.8582096099853511 149.6 -3.7924759387969975 150.4 -3.7226440906524658 151.2 -3.6676747798919687
		 152 -3.6401081085205078 152.8 -3.582597970962524 153.6 -3.5716149806976318 154.4 -3.5534193515777588
		 155.2 -3.5373001098632813 156 -3.5454690456390381 156.8 -3.5600583553314209 157.6 -3.5905482769012447
		 158.4 -3.597515106201171 159.2 -3.5823793411254878 160 -3.5861496925354004 160.8 -3.6113393306732178
		 161.6 -3.631064891815186 162.4 -3.6419386863708496 163.2 -3.6684944629669194 164 -3.6923453807830802
		 164.8 -3.7055959701538086 165.6 -3.7242288589477535 166.4 -3.7388179302215576 167.2 -3.7523691654205322
		 168 -3.7654011249542241 168.8 -3.746686458587646 169.6 -3.748872041702271 170.4 -3.7763020992279057
		 171.2 -3.7638711929321298 172 -3.7514402866363525 172.8 -3.7261412143707271 173.6 -3.715158224105835
		 174.4 -3.7007875442504878 175.2 -3.6756250858306889 176 -3.6540143489837646 176.8 -3.5971872806549077
		 177.6 -3.5694839954376221 178.4 -3.5300874710083012 179.2 -3.4619495868682861 180 -3.4034831523895264
		 180.8 -3.3452351093292236;
createNode animCurveTA -n "Bip01_L_Thigh_rotateY17";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 227 ".ktv[0:226]"  0 -177.0003662109375 0.8 -176.91923522949219
		 1.6 -176.82020568847656 2.4 -176.72935485839844 3.2 -176.68055725097656 4 -176.65602111816406
		 4.8 -176.61111450195312 5.6 -176.55155944824219 6.4 -176.48173522949219 7.2 -176.40174865722656
		 8 -176.32328796386719 8.8 -176.26828002929687 9.6 -176.21878051757813 10.4 -176.15841674804687
		 11.2 -176.09756469726562 12 -176.04263305664062 12.8 -176.00700378417969 13.6 -176.00068664550781
		 14.4 -175.98066711425781 15.2 -175.94877624511719 16 -175.94425964355469 16.8 -175.95834350585935
		 17.6 -175.96627807617187 18.4 -175.96919250488281 19.2 -176.00114440917969 20 -176.04313659667969
		 20.8 -176.06837463378909 21.6 -176.05909729003906 22.4 -176.03102111816409 23.2 -176.01036071777344
		 24 -176.02244567871094 24.8 -176.05857849121094 25.6 -176.07986450195315 26.4 -176.09388732910156
		 27.2 -176.11747741699219 28 -176.13235473632812 28.8 -176.1282958984375 29.6 -176.12518310546875
		 30.4 -176.1177978515625 31.2 -176.11428833007812 32 -176.1336669921875 32.8 -176.17984008789062
		 33.6 -176.21427917480469 34.4 -176.206787109375 35.2 -176.17926025390625 36 -176.16302490234375
		 36.8 -176.16731262207031 37.6 -176.17405700683594 38.4 -176.17877197265625 39.2 -176.19915771484375
		 40 -176.22102355957031 40.8 -176.20823669433594 41.6 -176.198974609375 42.4 -176.20367431640625
		 43.2 -176.21559143066409 44 -176.23448181152344 44.8 -176.21900939941406 45.6 -176.14785766601565
		 46.4 -176.05195617675781 47.2 -175.9711608886719 48 -175.90777587890625 48.8 -175.84890747070312
		 49.6 -175.77764892578125 50.4 -175.64952087402344 51.2 -175.43794250488281 52 -175.16835021972656
		 52.8 -174.89588928222656 53.6 -174.68927001953128 54.4 -174.53369140625 55.2 -174.32778930664062
		 56 -174.00119018554687 56.8 -173.59507751464844 57.6 -173.20046997070312 58.4 -172.83589172363284
		 59.2 -172.5025634765625 60 -172.22659301757812 60.8 -171.95909118652344 61.6 -171.66433715820312
		 62.4 -171.37371826171875 63.2 -171.08349609375 64 -170.70970153808594 64.8 -170.26960754394531
		 65.6 -169.81199645996094 66.4 -169.35726928710935 67.2 -168.89871215820313 68 -168.50442504882812
		 68.8 -168.22943115234375 69.6 -168.06352233886719 70.4 -167.95381164550781 71.2 -167.87129211425781
		 72 -167.76185607910156 72.8 -167.622802734375 73.6 -167.52117919921875 74.4 -167.47970581054687
		 75.2 -167.50334167480469 76 -167.523193359375 76.8 -167.52557373046875 77.6 -167.51863098144531
		 78.4 -167.51419067382812 79.2 -167.48991394042969 80 -167.469970703125 80.8 -167.50457763671875
		 81.6 -167.57783508300781 82.4 -167.67604064941406 83.2 -167.77719116210935 84 -167.85458374023435
		 84.8 -167.92881774902344 85.6 -167.9912109375 86.4 -168.07086181640625 87.2 -168.19894409179687
		 88 -168.37408447265625 88.8 -168.55599975585935 89.6 -168.70506286621094 90.4 -168.83445739746094
		 91.2 -168.95719909667969 92 -169.03158569335935 92.8 -169.06669616699219 93.6 -169.11886596679687
		 94.4 -169.20144653320312 95.2 -169.29679870605469 96 -169.38414001464844 96.8 -169.44088745117187
		 97.6 -169.47305297851565 98.4 -169.47395324707031 99.2 -169.43832397460938 100 -169.42707824707031
		 100.8 -169.42933654785159 101.6 -169.41595458984375 102.4 -169.38394165039062 103.2 -169.38420104980469
		 104 -169.39614868164062 104.8 -169.37568664550781 105.6 -169.34535217285156 106.4 -169.32521057128906
		 107.2 -169.32942199707031 108 -169.30941772460938 108.8 -169.29220581054687 109.6 -169.27285766601562
		 110.4 -169.26832580566406 111.2 -169.24626159667969 112 -169.22737121582031 112.8 -169.19635009765625
		 113.6 -169.1640319824219 114.4 -169.14588928222656 115.2 -169.1405029296875 116 -169.15402221679687
		 116.8 -169.11943054199219 117.6 -169.08451843261719 118.4 -169.046142578125 119.2 -169.000244140625
		 120 -168.95066833496094 120.8 -168.87794494628909 121.6 -168.82888793945312 122.4 -168.79708862304688
		 123.2 -168.7691650390625 124 -168.75440979003906 124.8 -168.77928161621094 125.6 -168.77928161621094
		 126.4 -168.75057983398435 127.2 -168.74198913574219 128 -168.75527954101562 128.8 -168.75025939941406
		 129.6 -168.73448181152344 130.4 -168.70668029785156 131.2 -168.69747924804687 132 -168.72560119628906
		 132.8 -168.78619384765625 133.6 -168.81782531738284 134.4 -168.83732604980469 135.2 -168.87098693847656
		 136 -168.89103698730469 136.8 -168.92355346679687 137.6 -168.93341064453125 138.4 -168.9580078125
		 139.2 -169.01055908203125 140 -169.04966735839844 140.8 -169.07173156738281 141.6 -169.11183166503906
		 142.4 -169.16175842285156 143.2 -169.21067810058594 144 -169.24684143066406 144.8 -169.29154968261719
		 145.6 -169.33047485351562 146.4 -169.37437438964844 147.2 -169.380126953125 148 -169.37265014648435
		 148.8 -169.38053894042969 149.6 -169.3951416015625 150.4 -169.40628051757812 151.2 -169.38737487792969
		 152 -169.34478759765625 152.8 -169.30680847167969 153.6 -169.28173828125 154.4 -169.24150085449219
		 155.2 -169.21386718750003 156 -169.19888305664062 156.8 -169.17247009277344 157.6 -169.12802124023435
		 158.4 -169.06741333007813 159.2 -169.01980590820312 160 -169.02781677246094 160.8 -169.03570556640628
		 161.6 -169.00624084472656 162.4 -168.9525146484375 163.2 -168.90628051757812 164 -168.88368225097656
		 164.8 -168.88395690917969 165.6 -168.87847900390625 166.4 -168.86090087890625 167.2 -168.86381530761719
		 168 -168.90565490722656 168.8 -168.95600891113284 169.6 -168.96682739257812 170.4 -168.9541015625
		 171.2 -168.90998840332031 172 -168.90190124511719 172.8 -168.93862915039062 173.6 -168.99324035644531
		 174.4 -169.04411315917969 175.2 -169.08131408691406 176 -169.09468078613281 176.8 -169.11419677734375
		 177.6 -169.14138793945312 178.4 -169.14790344238281 179.2 -169.17637634277344 180 -169.22671508789062
		 180.8 -169.28857421875;
createNode animCurveTA -n "Bip01_L_Thigh_rotateZ17";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 227 ".ktv[0:226]"  0 2.4647829532623291 0.8 2.5012969970703125
		 1.6 2.4660122394561768 2.4 2.3578903675079346 3.2 2.2579374313354492 4 2.1802918910980225
		 4.8 2.1260328292846684 5.6 2.1073863506317139 6.4 2.0488791465759277 7.2 1.9044615030288703
		 8 1.7476130723953247 8.8 1.6948020458221436 9.6 1.746288061141968 10.4 1.7608363628387451
		 11.2 1.7275596857070925 12 1.7550443410873413 12.8 1.7924464941024782 13.6 1.8033200502395632
		 14.4 1.8323347568511963 15.2 1.9105813503265383 16 1.9914098978042605 16.8 2.0218861103057861
		 17.6 2.0556135177612305 18.4 2.1485314369201665 19.2 2.2904901504516602 20 2.4361233711242676
		 20.8 2.5355572700500488 21.6 2.6366167068481445 22.4 2.7201910018920894 23.2 2.7670187950134277
		 24 2.8297061920166016 24.8 2.922105073928833 25.6 3.0145175457000732 26.4 3.1539762020111088
		 27.2 3.293708324432373 28 3.258068323135376 28.8 3.1625139713287354 29.6 3.2256250381469727
		 30.4 3.3605895042419438 31.2 3.4157228469848633 32 3.4278805255889893 32.8 3.4774267673492432
		 33.6 3.5685276985168457 34.4 3.5672574043273926 35.2 3.4349839687347412 36 3.2969322204589844
		 36.8 3.3040764331817627 37.6 3.3006751537322998 38.4 3.2219092845916748 39.2 3.2181527614593506
		 40 3.2649941444396973 40.8 3.2103116512298584 41.6 3.1729369163513184 42.4 3.1763930320739746
		 43.2 3.1743166446685791 44 3.1786196231842041 44.8 3.1249752044677734 45.6 3.0110750198364258
		 46.4 2.983385562896729 47.2 3.047152042388916 48 3.096998929977417 48.8 3.1123805046081543
		 49.6 3.1410808563232426 50.4 3.0910019874572754 51.2 2.9655313491821289 52 2.8757007122039795
		 52.8 2.8143110275268555 53.6 2.7208194732666016 54.4 2.6708223819732666 55.2 2.6924331188201904
		 56 2.7082791328430176 56.8 2.6591427326202393 57.6 2.6001026630401611 58.4 2.5522503852844238
		 59.2 2.5527284145355225 60 2.547783374786377 60.8 2.5613207817077637 61.6 2.5639026165008545
		 62.4 2.6482553482055664 63.2 2.7839028835296631 64 2.7065715789794922 64.8 2.4480216503143311
		 65.6 2.2394685745239258 66.4 2.1883242130279541 67.2 2.1925997734069824 68 2.2791383266448979
		 68.8 2.3554861545562744 69.6 2.3885717391967773 70.4 2.3594067096710205 71.2 2.3599121570587158
		 72 2.3387796878814697 72.8 2.2768843173980713 73.6 2.2400970458984375 74.4 2.2399740219116211
		 75.2 2.2838239669799805 76 2.3704171180725098 76.8 2.4343748092651367 77.6 2.4426941871643066
		 78.4 2.419963121414185 79.2 2.3435196876525879 80 2.3073880672454834 80.8 2.4266021251678467
		 81.6 2.5379343032836914 82.4 2.6774475574493408 83.2 2.8260588645935059 84 2.952308177947998
		 84.8 3.0941848754882813 85.6 3.2225787639617924 86.4 3.3757114410400391 87.2 3.487590074539185
		 88 3.5810134410858159 88.8 3.7196934223175049 89.6 3.9864397048950191 90.4 4.2693595886230469
		 91.2 4.5150690078735352 92 4.6323432922363281 92.8 4.6493096351623535 93.6 4.6643357276916504
		 94.4 4.7214908599853516 95.2 4.8167586326599121 96 4.8788857460021973 96.8 4.9421062469482422
		 97.6 5.0023617744445801 98.4 5.0073342323303232 99.2 4.8988847732543945 100 4.8169498443603516
		 100.8 4.8306784629821777 101.6 4.8855657577514648 102.4 4.9496874809265137 103.2 5.0583696365356445
		 104 5.1340479850769043 104.8 5.0799117088317871 105.6 4.9414777755737305 106.4 4.8381233215332031
		 107.2 4.8756074905395508 108 4.9344563484191895 108.8 5.0605826377868652 109.6 5.2727828025817871
		 110.4 5.4318304061889648 111.2 5.3628864288330078 112 5.2877135276794434 112.8 5.2563629150390625
		 113.6 5.3231620788574219 114.4 5.4282927513122559 115.2 5.4446167945861816 116 5.3564934730529785
		 116.8 5.3255391120910645 117.6 5.3103213310241699 118.4 5.2856783866882324 119.2 5.211024284362793
		 120 5.1214256286621094 120.8 5.0075392723083496 121.6 4.9483213424682617 122.4 4.9222574234008789
		 123.2 4.9273667335510254 124 4.949059009552002 124.8 4.9266290664672852 125.6 4.8360743522644043
		 126.4 4.794792652130127 127.2 4.8081250190734863 128 4.8723287582397461 128.8 4.910550594329834
		 129.6 4.9520506858825684 130.4 4.9481711387634277 131.2 4.9367785453796387 132 4.9444417953491211
		 132.8 4.9648914337158203 133.6 4.9299345016479492 134.4 4.863873004913331 135.2 4.8329596519470215
		 136 4.7780585289001465 136.8 4.6768350601196289 137.6 4.491887092590332 138.4 4.4584741592407227
		 139.2 4.5064630508422852 140 4.4015102386474609 140.8 4.2732391357421875 141.6 4.201235294342041
		 142.4 4.1504464149475098 143.2 4.1288762092590332 144 4.1097517013549805 144.8 4.0752320289611816
		 145.6 3.9743638038635254 146.4 3.8905301094055176 147.2 3.7634749412536617 148 3.5679130554199219
		 148.8 3.418646097183228 149.6 3.298503160476685 150.4 3.2064595222473145 151.2 3.1263277530670166
		 152 3.0465784072875977 152.8 2.9478137493133545 153.6 2.917610883712769 154.4 2.8207859992980957
		 155.2 2.6953973770141602 156 2.6448540687561035 156.8 2.6610960960388184 157.6 2.6742238998413086
		 158.4 2.5109958648681641 159.2 2.2123527526855469 160 2.0942587852478027 160.8 2.0829889774322514
		 161.6 1.9909317493438721 162.4 1.8668681383132937 163.2 1.8233052492141717 164 1.7620930671691897
		 164.8 1.6533428430557251 165.6 1.5478984117507937 166.4 1.4770146608352659 167.2 1.4406098127365112
		 168 1.3066424131393433 168.8 1.1276367902755735 169.6 0.98356086015701294 170.4 1.0060321092605593
		 171.2 0.8416295051574707 172 0.71547591686248779 172.8 0.55390101671218872 173.6 0.48451992869377131
		 174.4 0.50384938716888428 175.2 0.52003693580627441 176 0.49532529711723322 176.8 0.47618710994720453
		 177.6 0.51102107763290405 178.4 0.45211750268936157 179.2 0.39587774872779846 180 0.37526422739028931
		 180.8 0.3725048303604126;
createNode animCurveTA -n "Bip01_L_Calf_rotateX17";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 227 ".ktv[0:226]"  0 0.1152919977903366 0.8 0.11281587928533555
		 1.6 0.11241202056407928 2.4 0.11321841180324554 3.2 0.11401648819446562 4 0.1148139163851738
		 4.8 0.11501389741897583 5.6 0.11452757567167278 6.4 0.11498578637838364 7.2 0.1179052144289017
		 8 0.1212969645857811 8.8 0.12174671143293381 9.6 0.1203579306602478 10.4 0.12059731036424638
		 11.2 0.12194710969924928 12 0.12204298377037048 12.8 0.12167286127805714 13.6 0.12230104207992552
		 14.4 0.12286911904811859 15.2 0.12260706722736356 16 0.12240810692310336 16.8 0.12284209579229352
		 17.6 0.12340138107538226 18.4 0.12275951355695723 19.2 0.12078005820512772 20 0.119343101978302
		 20.8 0.11914616078138351 21.6 0.11858080327510832 22.4 0.11931740492582321 23.2 0.12040177732706069
		 24 0.12029688805341721 24.8 0.1193726658821106 25.6 0.11877389997243881 26.4 0.11694625020027161
		 27.2 0.11498914659023284 28 0.11726602911949158 28.8 0.12101177871227266 29.6 0.1206219494342804
		 30.4 0.11822260171175002 31.2 0.11737276613712314 32 0.11741386353969574 32.8 0.11646177619695665
		 33.6 0.11477662622928621 34.4 0.11531268805265422 35.2 0.11956540495157242 36 0.1239139661192894
		 36.8 0.12385363131761552 37.6 0.1234285831451416 38.4 0.12617874145507813 39.2 0.12710015475749967
		 40 0.12653052806854248 40.8 0.12817461788654327 41.6 0.12924385070800781 42.4 0.12896779179573059
		 43.2 0.12922543287277222 44 0.12956811487674713 44.8 0.1309475302696228 45.6 0.13434548676013949
		 46.4 0.13588070869445801 47.2 0.13427542150020599 48 0.13274161517620087 48.8 0.13190847635269165
		 49.6 0.13097238540649414 50.4 0.13211378455162048 51.2 0.13564752042293551 52 0.13833293318748474
		 52.8 0.1395186185836792 53.6 0.14085452258586884 54.4 0.14146776497364044 55.2 0.14038471877574921
		 56 0.1391737312078476 56.8 0.13918609917163849 57.6 0.14013060927391052 58.4 0.14069148898124695
		 59.2 0.14017188549041748 60 0.13910903036594391 60.8 0.13859613239765167 61.6 0.13854749500751495
		 62.4 0.13619484007358551 63.2 0.13291017711162567 64 0.13576066493988037 64.8 0.14444336295127869
		 65.6 0.15134634077548981 66.4 0.15407609939575195 67.2 0.15437723696231842 68 0.1534036248922348
		 68.8 0.15246409177780151 69.6 0.15263991057872772 70.4 0.15361443161964417 71.2 0.15401971340179443
		 72 0.15450878441333771 72.8 0.1563899964094162 73.6 0.15803542733192447 74.4 0.15797853469848633
		 75.2 0.156265988945961 76 0.15459996461868286 76.8 0.15394778549671173 77.6 0.15487068891525269
		 78.4 0.15669271349906921 79.2 0.15965723991394043 80 0.1621624082326889 80.8 0.16177856922149661
		 81.6 0.16001266241073608 82.4 0.1585480272769928 83.2 0.15692293643951416 84 0.15512949228286743
		 84.8 0.1534506231546402 85.6 0.15198975801467896 86.4 0.15100815892219543 87.2 0.15111503005027771
		 88 0.15234291553497314 88.8 0.15198349952697754 89.6 0.14798450469970703 90.4 0.1426289826631546
		 91.2 0.13845737278461456 92 0.13704770803451538 92.8 0.13857020437717441 93.6 0.14033514261245728
		 94.4 0.14069926738739014 95.2 0.14045044779777527 96 0.14043502509593964 96.8 0.1408650130033493
		 97.6 0.14129151403903961 98.4 0.14288687705993652 99.2 0.14724385738372803 100 0.15086938440799713
		 100.8 0.15199966728687286 101.6 0.15288926661014557 102.4 0.15343789756298065 103.2 0.1525963693857193
		 104 0.15306699275970459 104.8 0.15587274730205536 105.6 0.16086044907569885 106.4 0.1642463207244873
		 107.2 0.16444861888885498 108 0.16439163684844971 108.8 0.16304993629455566 109.6 0.15822590887546539
		 110.4 0.15546487271785736 111.2 0.15799778699874878 112 0.16129335761070251 112.8 0.16261087357997894
		 113.6 0.16181263327598572 114.4 0.15971691906452179 115.2 0.1590084433555603 116 0.16119600832462311
		 116.8 0.16266575455665588 117.6 0.16282585263252258 118.4 0.16283534467220306 119.2 0.16388659179210663
		 120 0.16606743633747101 120.8 0.16821202635765076 121.6 0.16984301805496216 122.4 0.16981521248817447
		 123.2 0.16877472400665283 124 0.16820618510246277 124.8 0.1689925491809845 125.6 0.17064554989337921
		 126.4 0.17154404520988467 127.2 0.17110541462898254 128 0.16969342529773715 128.8 0.16832219064235687
		 129.6 0.16807688772678375 130.4 0.16850961744785312 131.2 0.16863344609737396 132 0.16824987530708313
		 132.8 0.16768425703048706 133.6 0.16810537874698639 134.4 0.16960744559764862 135.2 0.17055687308311465
		 136 0.17127096652984622 136.8 0.17444747686386108 137.6 0.17865227162837982 138.4 0.17916078865528107
		 139.2 0.17759917676448822 140 0.17839394509792328 140.8 0.18010899424552915 141.6 0.1803676635026932
		 142.4 0.17987553775310519 143.2 0.1786269694566727 144 0.17673052847385406 144.8 0.17622998356819153
		 145.6 0.17682451009750369 146.4 0.17693500220775604 147.2 0.17792938649654388 148 0.18024757504463196
		 148.8 0.18209381401538849 149.6 0.18293911218643188 150.4 0.18284919857978824 151.2 0.18229615688323977
		 152 0.18194589018821716 152.8 0.18107976019382477 153.6 0.17972064018249512 154.4 0.17897816002368927
		 155.2 0.17915619909763336 156 0.17775274813175199 156.8 0.17452153563499451 157.6 0.17094087600708008
		 158.4 0.17175908386707306 159.2 0.17635613679885864 160 0.17751531302928925 160.8 0.17516797780990601
		 161.6 0.17445386946201327 162.4 0.17452412843704224 163.2 0.17305365204811096 164 0.17178633809089661
		 164.8 0.17195342481136322 165.6 0.17202989757061007 166.4 0.17078830301761627 167.2 0.16968904435634613
		 168 0.17088441550731659 168.8 0.1736559271812439 169.6 0.17517349123954773 170.4 0.17484526336193085
		 171.2 0.1746644526720047 172 0.17597185075283053 172.8 0.17834670841693878 173.6 0.17950507998466492
		 174.4 0.17817233502864838 175.2 0.1764872670173645 176 0.17620569467544556 176.8 0.17648348212242126
		 177.6 0.17681825160980225 178.4 0.17890109121799469 179.2 0.18146409094333651 180 0.18229994177818296
		 180.8 0.18285439908504489;
createNode animCurveTA -n "Bip01_L_Calf_rotateY17";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 227 ".ktv[0:226]"  0 -1.0306549072265625 0.8 -1.0312401056289673
		 1.6 -1.0313217639923096 2.4 -1.0311695337295532 3.2 -1.0310078859329224 4 -1.0308581590652466
		 4.8 -1.0308593511581421 5.6 -1.0309569835662842 6.4 -1.0308459997177124 7.2 -1.0301995277404783
		 8 -1.0294355154037476 8.8 -1.029376745223999 9.6 -1.0297129154205322 10.4 -1.0296696424484253
		 11.2 -1.0294243097305298 12 -1.0294052362442017 12.8 -1.0295101404190063 13.6 -1.0293830633163452
		 14.4 -1.0292695760726929 15.2 -1.0293213129043579 16 -1.0293456315994265 16.8 -1.0292727947235107
		 17.6 -1.0291413068771362 18.4 -1.0292692184448242 19.2 -1.0297235250473022 20 -1.0300219058990481
		 20.8 -1.0300565958023071 21.6 -1.0301928520202637 22.4 -1.0300151109695437 23.2 -1.0297634601593018
		 24 -1.0297983884811399 24.8 -1.0300084352493286 25.6 -1.0301307439804075 26.4 -1.0305273532867432
		 27.2 -1.0309613943099976 28 -1.0304239988327026 28.8 -1.0295528173446655 29.6 -1.0296519994735718
		 30.4 -1.0302056074142456 31.2 -1.0304100513458252 32 -1.0304064750671389 32.8 -1.0306228399276731
		 33.6 -1.0309780836105349 34.4 -1.0308718681335447 35.2 -1.0299344062805176 36 -1.0289596319198608
		 36.8 -1.028942346572876 37.6 -1.0290510654449463 38.4 -1.028375506401062 39.2 -1.0281604528427124
		 40 -1.0282796621322632 40.8 -1.0279062986373899 41.6 -1.0276397466659546 42.4 -1.0277074575424194
		 43.2 -1.0276319980621338 44 -1.0275261402130127 44.8 -1.0272077322006226 45.6 -1.0263851881027222
		 46.4 -1.0259853601455688 47.2 -1.0263868570327761 48 -1.0267300605773926 48.8 -1.0269030332565308
		 49.6 -1.0270881652832031 50.4 -1.0267910957336426 51.2 -1.0259144306182859 52 -1.0251952409744265
		 52.8 -1.02484130859375 53.6 -1.0244894027709961 54.4 -1.0243314504623413 55.2 -1.0245593786239624
		 56 -1.0248204469680786 56.8 -1.0247734785079956 57.6 -1.0244672298431401 58.4 -1.0242877006530762
		 59.2 -1.0243303775787354 60 -1.0245199203491211 60.8 -1.024550199508667 61.6 -1.0245332717895508
		 62.4 -1.0251203775405884 63.2 -1.0258746147155762 64 -1.0251260995864868 64.8 -1.022913932800293
		 65.6 -1.0211254358291626 66.4 -1.0203661918640137 67.2 -1.0203084945678711 68 -1.0205336809158323
		 68.8 -1.020775318145752 69.6 -1.0206893682479858 70.4 -1.0204362869262695 71.2 -1.0203108787536621
		 72 -1.0201900005340576 72.8 -1.0196834802627563 73.6 -1.0192316770553589 74.4 -1.0192723274230957
		 75.2 -1.0197457075119021 76 -1.0201387405395508 76.8 -1.0202867984771729 77.6 -1.0200551748275757
		 78.4 -1.0195575952529907 79.2 -1.018776535987854 80 -1.0181297063827517 80.8 -1.0181975364685061
		 81.6 -1.0187221765518188 82.4 -1.0190842151641846 83.2 -1.0195046663284302 84 -1.0199970006942749
		 84.8 -1.0204368829727173 85.6 -1.0208643674850464 86.4 -1.0211235284805298 87.2 -1.021080493927002
		 88 -1.0207477807998655 88.8 -1.0208715200424194 89.6 -1.0219612121582031 90.4 -1.02337646484375
		 91.2 -1.0244008302688601 92 -1.02476966381073 92.8 -1.0243723392486572 93.6 -1.0239053964614868
		 94.4 -1.0238251686096191 95.2 -1.0238629579544067 96 -1.0238809585571289 96.8 -1.0237727165222168
		 97.6 -1.0237113237380979 98.4 -1.02332603931427 99.2 -1.0222359895706177 100 -1.0212832689285278
		 100.8 -1.0209914445877075 101.6 -1.0207277536392212 102.4 -1.0206042528152466 103.2 -1.0208351612091064
		 104 -1.0206975936889648 104.8 -1.0199871063232422 105.6 -1.0186600685119629 106.4 -1.017736554145813
		 107.2 -1.0176322460174565 108 -1.0176810026168825 108.8 -1.018047571182251 109.6 -1.0194219350814819
		 110.4 -1.020150899887085 111.2 -1.0195000171661377 112 -1.0185843706130979 112.8 -1.0182706117630005
		 113.6 -1.0184801816940308 114.4 -1.0190106630325315 115.2 -1.0192208290100098 116 -1.0186476707458496
		 116.8 -1.0182138681411743 117.6 -1.0181872844696045 118.4 -1.0181816816329956 119.2 -1.0178986787796021
		 120 -1.01729416847229 120.8 -1.0167446136474607 121.6 -1.0162723064422607 122.4 -1.0163100957870483
		 123.2 -1.0166311264038086 124 -1.0167514085769651 124.8 -1.016485333442688 125.6 -1.0160298347473145
		 126.4 -1.0157743692398071 127.2 -1.0158692598342896 128 -1.0162606239318848 128.8 -1.0166968107223513
		 129.6 -1.0167158842086792 130.4 -1.0165877342224121 131.2 -1.0165783166885376 132 -1.0166741609573364
		 132.8 -1.0168015956878662 133.6 -1.016681432723999 134.4 -1.0162389278411863 135.2 -1.0159207582473757
		 136 -1.0157455205917358 136.8 -1.0147923231124878 137.6 -1.0136276483535769 138.4 -1.0134478807449343
		 139.2 -1.0138208866119385 140 -1.0136333703994751 140.8 -1.0131388902664185 141.6 -1.0130618810653689
		 142.4 -1.0131747722625732 143.2 -1.013525128364563 144 -1.014110803604126 144.8 -1.0142146348953247
		 145.6 -1.0140519142150879 146.4 -1.0139976739883425 147.2 -1.0136781930923462 148 -1.0130285024642944
		 148.8 -1.0125007629394531 149.6 -1.0122624635696411 150.4 -1.0122884511947632 151.2 -1.0124294757843018
		 152 -1.0125290155410769 152.8 -1.0127885341644287 153.6 -1.013154149055481 154.4 -1.0134061574935913
		 155.2 -1.0133923292160034 156 -1.0137732028961182 156.8 -1.0147221088409424 157.6 -1.0157345533370972
		 158.4 -1.0154750347137451 159.2 -1.014168381690979 160 -1.0138118267059326 160.8 -1.0145204067230225
		 161.6 -1.014716625213623 162.4 -1.0147085189819336 163.2 -1.0151392221450806 164 -1.015508770942688
		 164.8 -1.0154294967651367 165.6 -1.0154004096984863 166.4 -1.0157746076583862 167.2 -1.0160640478134155
		 168 -1.0157167911529541 168.8 -1.0148862600326538 169.6 -1.0144224166870115 170.4 -1.0142977237701416
		 171.2 -1.014625072479248 172 -1.0142567157745359 172.8 -1.0136280059814451 173.6 -1.0133007764816284
		 174.4 -1.0136955976486206 175.2 -1.0141605138778689 176 -1.0142256021499634 176.8 -1.0141589641571045
		 177.6 -1.0140440464019775 178.4 -1.0135414600372314 179.2 -1.0127679109573364 180 -1.0125365257263184
		 180.8 -1.0124009847640991;
createNode animCurveTA -n "Bip01_L_Calf_rotateZ17";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 227 ".ktv[0:226]"  0 -11.143221855163574 0.8 -11.017413139343262
		 1.6 -10.986400604248049 2.4 -11.057148933410645 3.2 -11.119940757751465 4 -11.191123008728027
		 4.8 -11.245501518249512 5.6 -11.204298973083496 6.4 -11.224575996398926 7.2 -11.405538558959959
		 8 -11.624235153198242 8.8 -11.690918922424316 9.6 -11.619024276733398 10.4 -11.639449119567873
		 11.2 -11.774259567260742 12 -11.78983211517334 12.8 -11.780211448669434 13.6 -11.833083152770996
		 14.4 -11.88039493560791 15.2 -11.860840797424316 16 -11.828312873840332 16.8 -11.874022483825684
		 17.6 -11.910143852233888 18.4 -11.859148979187012 19.2 -11.729321479797363 20 -11.620370864868164
		 20.8 -11.600616455078125 21.6 -11.571693420410156 22.4 -11.608036041259766 23.2 -11.674920082092283
		 24 -11.67332935333252 24.8 -11.621718406677246 25.6 -11.564592361450195 26.4 -11.442171096801758
		 27.2 -11.31834888458252 28 -11.440759658813478 28.8 -11.648148536682127 29.6 -11.632030487060549
		 30.4 -11.494168281555176 31.2 -11.45688533782959 32 -11.461615562438965 32.8 -11.402074813842772
		 33.6 -11.284912109375 34.4 -11.330209732055664 35.2 -11.60806941986084 36 -11.89364528656006
		 36.8 -11.868963241577148 37.6 -11.84597873687744 38.4 -11.986876487731934 39.2 -12.040544509887695
		 40 -11.998091697692873 40.8 -12.109318733215332 41.6 -12.164971351623535 42.4 -12.151604652404783
		 43.2 -12.155196189880376 44 -12.153156280517578 44.8 -12.247424125671388 45.6 -12.452890396118164
		 46.4 -12.529006004333496 47.2 -12.44062614440918 48 -12.324091911315918 48.8 -12.247723579406738
		 49.6 -12.156559944152832 50.4 -12.214471817016602 51.2 -12.411540031433104 52 -12.527652740478516
		 52.8 -12.552746772766112 53.6 -12.621028900146484 54.4 -12.644673347473145 55.2 -12.555602073669434
		 56 -12.444554328918455 56.8 -12.411663055419922 57.6 -12.41279125213623 58.4 -12.414034843444824
		 59.2 -12.302726745605469 60 -12.17764949798584 60.8 -12.073062896728516 61.6 -12.048905372619627
		 62.4 -11.904049873352053 63.2 -11.667181968688968 64 -11.79949951171875 64.8 -12.292770385742188
		 65.6 -12.708439826965332 66.4 -12.840032577514648 67.2 -12.876518249511721 68 -12.79615306854248
		 68.8 -12.731977462768556 69.6 -12.708735466003418 70.4 -12.769993782043455 71.2 -12.775093078613279
		 72 -12.82175350189209 72.8 -12.92933177947998 73.6 -13.026307106018066 74.4 -13.041123390197754
		 75.2 -12.945554733276367 76 -12.799511909484863 76.8 -12.748263359069826 77.6 -12.811916351318359
		 78.4 -12.912238121032717 79.2 -13.107812881469728 80 -13.288734436035156 80.8 -13.228071212768556
		 81.6 -13.159533500671388 82.4 -13.049945831298828 83.2 -12.935171127319336 84 -12.834065437316896
		 84.8 -12.723559379577637 85.6 -12.673177719116214 86.4 -12.615664482116699 87.2 -12.612690925598145
		 88 -12.674465179443359 88.8 -12.675754547119141 89.6 -12.467452049255376 90.4 -12.181471824645996
		 91.2 -11.915642738342283 92 -11.841842651367189 92.8 -11.912957191467283 93.6 -12.00430965423584
		 94.4 -12.033723831176758 95.2 -12.001444816589355 96 -12.008742332458496 96.8 -12.03766918182373
		 97.6 -12.097027778625488 98.4 -12.210521697998049 99.2 -12.497193336486816 100 -12.707676887512209
		 100.8 -12.779293060302734 101.6 -12.811944007873535 102.4 -12.863740921020508 103.2 -12.818844795227053
		 104 -12.82738208770752 104.8 -13.029988288879396 105.6 -13.354867935180664 106.4 -13.559983253479004
		 107.2 -13.538229942321776 108 -13.552591323852541 108.8 -13.471930503845218 109.6 -13.226581573486328
		 110.4 -13.053919792175291 111.2 -13.224601745605469 112 -13.411026000976562 112.8 -13.532340049743652
		 113.6 -13.477213859558104 114.4 -13.311785697937012 115.2 -13.284403800964355 116 -13.431361198425291
		 116.8 -13.498248100280763 117.6 -13.518477439880373 118.4 -13.526678085327148 119.2 -13.596379280090332
		 120 -13.719607353210447 120.8 -13.891098976135254 121.6 -13.974082946777344 122.4 -13.999625205993652
		 123.2 -13.963195800781252 124 -13.89553165435791 124.8 -13.902886390686035 125.6 -14.021690368652344
		 126.4 -14.075603485107422 127.2 -14.016338348388672 128 -13.93007755279541 128.8 -13.877588272094728
		 129.6 -13.827991485595703 130.4 -13.854392051696776 131.2 -13.875749588012695 132 -13.846673965454103
		 132.8 -13.779668807983398 133.6 -13.810260772705078 134.4 -13.884330749511721 135.2 -13.90110969543457
		 136 -13.966955184936523 136.8 -14.118725776672363 137.6 -14.417483329772946 138.4 -14.429309844970703
		 139.2 -14.265241622924805 140 -14.345998764038084 140.8 -14.458198547363279 141.6 -14.466971397399902
		 142.4 -14.421475410461426 143.2 -14.331543922424316 144 -14.243306159973145 144.8 -14.174779891967772
		 145.6 -14.219840049743652 146.4 -14.21092700958252 147.2 -14.248447418212892 148 -14.409058570861816
		 148.8 -14.531945228576658 149.6 -14.601925849914554 150.4 -14.593649864196779 151.2 -14.539548873901362
		 152 -14.511574745178221 152.8 -14.467212677001951 153.6 -14.361595153808594 154.4 -14.336140632629396
		 155.2 -14.375735282897946 156 -14.275175094604492 156.8 -14.086743354797363 157.6 -13.859734535217283
		 158.4 -13.888615608215334 159.2 -14.175400733947754 160 -14.229841232299806 160.8 -14.107510566711426
		 161.6 -14.060305595397947 162.4 -14.080970764160156 163.2 -13.999794960021973 164 -13.922725677490234
		 164.8 -13.913463592529297 165.6 -13.90031909942627 166.4 -13.845952033996582 167.2 -13.759634017944336
		 168 -13.834528923034668 168.8 -13.966822624206545 169.6 -14.035097122192385 170.4 -13.840606689453123
		 171.2 -14.051257133483888 172 -14.143538475036619 172.8 -14.329631805419922 173.6 -14.405507087707518
		 174.4 -14.334493637084959 175.2 -14.208674430847168 176 -14.179861068725586 176.8 -14.203304290771484
		 177.6 -14.217531204223633 178.4 -14.425056457519533 179.2 -14.565327644348146 180 -14.62788200378418
		 180.8 -14.680179595947266;
createNode animCurveTA -n "Bip01_L_Foot_rotateX17";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 227 ".ktv[0:226]"  0 -8.4145593643188477 0.8 -8.1899023056030273
		 1.6 -8.0388832092285156 2.4 -7.9642219543457031 3.2 -7.9389352798461914 4 -7.9146299362182626
		 4.8 -7.847863197326661 5.6 -7.810862064361574 6.4 -7.7786917686462402 7.2 -7.7405829429626465
		 8 -7.7511320114135742 8.8 -7.906435489654541 9.6 -7.9324207305908221 10.4 -7.7796816825866717
		 11.2 -7.4649686813354492 12 -7.1024007797241211 12.8 -6.9916563034057617 13.6 -7.212775707244873
		 14.4 -7.3634295463562012 15.2 -7.289710044860839 16 -7.2944555282592756 16.8 -7.375929355621337
		 17.6 -7.412062644958497 18.4 -7.4322075843811053 19.2 -7.5590963363647452 20 -7.7009558677673331
		 20.8 -7.7346243858337393 21.6 -7.6021904945373517 22.4 -7.494462013244628 23.2 -7.4948096275329599
		 24 -7.5481500625610352 24.8 -7.6127595901489267 25.6 -7.6390538215637216 26.4 -7.6311306953430158
		 27.2 -7.6652331352233887 28 -7.7864680290222168 28.8 -7.8875565528869629 29.6 -7.7445292472839364
		 30.4 -7.4941864013671875 31.2 -7.4039816856384286 32 -7.5044989585876465 32.8 -7.6670455932617196
		 33.6 -7.693012237548829 34.4 -7.5842690467834482 35.2 -7.5638337135314915 36 -7.6590304374694824
		 36.8 -7.536384105682373 37.6 -7.4143953323364258 38.4 -7.553586959838869 39.2 -7.839210033416748
		 40 -7.9644193649291992 40.8 -7.9272794723510733 41.6 -7.8600974082946768 42.4 -7.861555099487302
		 43.2 -7.9306550025939924 44 -8.0669612884521484 44.8 -8.1299228668212891 45.6 -8.0668315887451172
		 46.4 -7.9702410697937012 47.2 -7.9503116607666016 48 -8.1999721527099609 48.8 -8.5940780639648437
		 49.6 -8.7976570129394531 50.4 -8.7717428207397461 51.2 -8.5413103103637695 52 -8.2639799118041992
		 52.8 -8.0451278686523437 53.6 -7.9836430549621582 54.4 -7.9634561538696289 55.2 -7.8516521453857422
		 56 -7.7862982749938947 56.8 -7.754002571105957 57.6 -7.6941914558410636 58.4 -7.7183346748352051
		 59.2 -7.922235012054446 60 -8.2059545516967773 60.8 -8.5474157333374023 61.6 -8.8379735946655273
		 62.4 -9.0530557632446289 63.2 -9.2622890472412109 64 -9.4846506118774414 64.8 -9.7091035842895508
		 65.6 -9.7709503173828125 66.4 -9.3235607147216797 67.2 -8.5942058563232422 68 -8.2225503921508789
		 68.8 -8.3127946853637695 69.6 -8.4870424270629883 70.4 -8.6016664505004883 71.2 -8.705841064453125
		 72 -8.6812124252319336 72.8 -8.5206060409545898 73.6 -8.318397521972658 74.4 -8.464996337890625
		 75.2 -8.9675130844116211 76 -9.2710399627685547 76.8 -9.2319431304931641 77.6 -9.1170949935913086
		 78.4 -9.0736207962036133 79.2 -9.0207691192626953 80 -8.9757595062255859 80.8 -9.1109066009521484
		 81.6 -9.3265504837036133 82.4 -9.3596372604370117 83.2 -9.3935604095458984 84 -9.4556798934936523
		 84.8 -9.5134363174438477 85.6 -9.4440717697143555 86.4 -9.3915929794311541 87.2 -9.5757293701171875
		 88 -9.9238195419311523 88.8 -10.135604858398436 89.6 -10.184045791625977 90.4 -10.258923530578612
		 91.2 -10.332858085632324 92 -10.371236801147459 92.8 -10.317527770996094 93.6 -10.356398582458496
		 94.4 -10.45108127593994 95.2 -10.486427307128906 96 -10.541599273681641 96.8 -10.603017807006836
		 97.6 -10.586634635925291 98.4 -10.372714042663574 99.2 -10.075874328613279 100 -9.8703918457031232
		 100.8 -9.9087715148925781 101.6 -9.8180437088012695 102.4 -9.6084690093994141 103.2 -9.5786705017089844
		 104 -9.7028341293334961 104.8 -9.7037992477416992 105.6 -9.5726671218872088 106.4 -9.4882068634033203
		 107.2 -9.4527082443237305 108 -9.3773889541625977 108.8 -9.2721595764160156 109.6 -9.1794214248657227
		 110.4 -9.0844097137451207 111.2 -9.1026630401611328 112 -9.0617952346801758 112.8 -8.989415168762207
		 113.6 -8.9123811721801758 114.4 -8.94610595703125 115.2 -8.9931917190551758 116 -9.0359735488891602
		 116.8 -8.949313163757326 117.6 -8.8319005966186523 118.4 -8.7475519180297852 119.2 -8.7232322692871094
		 120 -8.6433277130126953 120.8 -8.5334615707397479 121.6 -8.2918605804443359 122.4 -8.0459003448486328
		 123.2 -7.9312324523925772 124 -8.034581184387207 124.8 -8.3200054168701172 125.6 -8.4796314239501953
		 126.4 -8.4444971084594727 127.2 -8.3441085815429687 128 -8.3313064575195313 128.8 -8.3700227737426758
		 129.6 -8.3898162841796875 130.4 -8.3676462173461914 131.2 -8.3621597290039063 132 -8.4560518264770526
		 132.8 -8.5460538864135742 133.6 -8.5765895843505859 134.4 -8.6697092056274432 135.2 -8.7416715621948242
		 136 -8.7124042510986328 136.8 -8.7056732177734375 137.6 -8.7554788589477539 138.4 -8.8111686706542969
		 139.2 -8.9426851272583008 140 -9.037806510925293 140.8 -8.9496488571166992 141.6 -8.8206624984741211
		 142.4 -8.797694206237793 143.2 -8.8679666519165039 144 -8.9561758041381836 144.8 -9.0391473770141602
		 145.6 -9.1742906570434553 146.4 -9.3389978408813477 147.2 -9.3726177215576207 148 -9.2991075515747088
		 148.8 -9.2741451263427734 149.6 -9.3616876602172852 150.4 -9.478424072265625 151.2 -9.4488735198974609
		 152 -9.3100852966308594 152.8 -9.2346668243408203 153.6 -9.2421474456787109 154.4 -9.2513761520385742
		 155.2 -9.2330694198608398 156 -9.2173871994018555 156.8 -9.1535205841064453 157.6 -9.0508728027343768
		 158.4 -8.9723596572875977 159.2 -9.0633535385131836 160 -9.3610658645629901 160.8 -9.5785741806030273
		 161.6 -9.5004348754882812 162.4 -9.2990837097167987 163.2 -9.1757898330688477 164 -9.1715822219848633
		 164.8 -9.2626886367797852 165.6 -9.2432718276977539 166.4 -9.1205720901489258 167.2 -9.1521453857421875
		 168 -9.4771995544433594 168.8 -10.003524780273436 169.6 -10.156800270080566 170.4 -9.8310127258300781
		 171.2 -9.4118318557739258 172 -9.2681713104248047 172.8 -9.3457832336425781 173.6 -9.4215087890625
		 174.4 -9.4649772644042969 175.2 -9.4967508316040039 176 -9.4589347839355469 176.8 -9.361328125
		 177.6 -9.2197084426879883 178.4 -9.0313396453857422 179.2 -8.9814119338989258 180 -9.0062217712402344
		 180.8 -9.0851631164550781;
createNode animCurveTA -n "Bip01_L_Foot_rotateY17";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 227 ".ktv[0:226]"  0 0.72921282052993797 0.8 0.62830424308776855
		 1.6 0.5459616780281068 2.4 0.50528603792190552 3.2 0.50662308931350708 4 0.49598398804664601
		 4.8 0.45575839281082153 5.6 0.4172806441783905 6.4 0.41975134611129761 7.2 0.39296171069145203
		 8 0.42165470123291016 8.8 0.49977317452430725 9.6 0.49396464228630071 10.4 0.41583061218261719
		 11.2 0.29933658242225647 12 0.10979585349559784 12.8 0.030765475705265999 13.6 0.16665433347225189
		 14.4 0.20587499439716339 15.2 0.20099525153636935 16 0.20628415048122409 16.8 0.22505095601081845
		 17.6 0.27572256326675421 18.4 0.26199570298194885 19.2 0.34618315100669861 20 0.42874506115913391
		 20.8 0.44141396880149841 21.6 0.364418625831604 22.4 0.32001852989196777 23.2 0.31262645125389099
		 24 0.31046596169471741 24.8 0.34505766630172729 25.6 0.36728686094284063 26.4 0.38435575366020203
		 27.2 0.36513584852218622 28 0.45950740575790405 28.8 0.49943599104881303 29.6 0.39655447006225586
		 30.4 0.3097994327545166 31.2 0.25832071900367737 32 0.2896689772605896 32.8 0.3980197012424469
		 33.6 0.41612070798873901 34.4 0.35432004928588867 35.2 0.35710775852203369 36 0.38429021835327154
		 36.8 0.30928966403007507 37.6 0.26106911897659302 38.4 0.33893260359764099 39.2 0.46326279640197759
		 40 0.53194206953048706 40.8 0.53359347581863403 41.6 0.49159067869186396 42.4 0.49310815334320068
		 43.2 0.50680679082870483 44 0.57615810632705688 44.8 0.62174332141876221 45.6 0.58730196952819824
		 46.4 0.52959680557250977 47.2 0.53017956018447876 48 0.66128402948379517 48.8 0.85544538497924816
		 49.6 0.93805384635925293 50.4 0.93517184257507324 51.2 0.82734596729278564 52 0.68891406059265137
		 52.8 0.58000582456588745 53.6 0.55202949047088623 54.4 0.53782302141189575 55.2 0.48731213808059698
		 56 0.44562426209449768 56.8 0.43548780679702759 57.6 0.39430922269821178 58.4 0.41271927952766424
		 59.2 0.51229673624038696 60 0.65043425559997559 60.8 0.78764396905899037 61.6 0.93165653944015525
		 62.4 1.0203574895858765 63.2 1.1424456834793093 64 1.2194311618804932 64.8 1.3535418510437012
		 65.6 1.3844205141067505 66.4 1.1913261413574221 67.2 0.85676062107086171 68 0.67063283920288086
		 68.8 0.72080963850021373 69.6 0.8138352632522583 70.4 0.87427562475204468 71.2 0.91699117422103871
		 72 0.89912658929824829 72.8 0.80822509527206421 73.6 0.74065333604812622 74.4 0.81085795164108276
		 75.2 1.0300912857055664 76 1.1745041608810425 76.8 1.158574104309082 77.6 1.102262020111084
		 78.4 1.0916242599487305 79.2 1.0519974231719973 80 1.0375281572341919 80.8 1.0990573167800903
		 81.6 1.1859931945800779 82.4 1.23077392578125 83.2 1.2601765394210815 84 1.2596420049667358
		 84.8 1.2825145721435549 85.6 1.2417672872543335 86.4 1.2442425489425659 87.2 1.3346512317657473
		 88 1.4635051488876345 88.8 1.5985934734344482 89.6 1.6364943981170654 90.4 1.6530288457870483
		 91.2 1.695987343788147 92 1.7010011672973633 92.8 1.6728845834732056 93.6 1.6812114715576172
		 94.4 1.7158313989639282 95.2 1.7501344680786133 96 1.774658203125 96.8 1.7595694065093994
		 97.6 1.7678475379943848 98.4 1.7096763849258425 99.2 1.5457570552825928 100 1.4623041152954102
		 100.8 1.4789261817932129 101.6 1.4513746500015261 102.4 1.3357851505279541 103.2 1.3311184644699097
		 104 1.386449933052063 104.8 1.3839213848114016 105.6 1.3316751718521118 106.4 1.2839146852493286
		 107.2 1.2866557836532593 108 1.2174351215362549 108.8 1.2060004472732544 109.6 1.1440786123275757
		 110.4 1.0916992425918579 111.2 1.0885344743728638 112 1.1088662147521973 112.8 1.0754610300064087
		 113.6 1.0345149040222168 114.4 1.0189176797866819 115.2 1.0762858390808103 116 1.0731251239776611
		 116.8 1.0538561344146729 117.6 0.99550098180770863 118.4 0.95098626613616954 119.2 0.91382986307144176
		 120 0.92248272895812988 120.8 0.83582663536071777 121.6 0.71765708923339844 122.4 0.61765879392623901
		 123.2 0.54458951950073242 124 0.61039072275161743 124.8 0.7409052848815918 125.6 0.83075356483459484
		 126.4 0.79409396648406982 127.2 0.74531590938568115 128 0.76825731992721547 128.8 0.77001959085464478
		 129.6 0.78796297311782837 130.4 0.77084046602249168 131.2 0.74920469522476196 132 0.81239950656890869
		 132.8 0.84690564870834351 133.6 0.85433667898178101 134.4 0.90707254409790039 135.2 0.9509459733963016
		 136 0.91764760017395008 136.8 0.94057625532150269 137.6 0.94084739685058594 138.4 0.96628987789154064
		 139.2 1.0120365619659424 140 1.0952842235565186 140.8 1.0337446928024292 141.6 0.98468720912933339
		 142.4 0.97424638271331798 143.2 1.0117788314819336 144 1.0331507921218872 144.8 1.0827863216400146
		 145.6 1.1242069005966189 146.4 1.217327356338501 147.2 1.2398334741592407 148 1.2043921947479248
		 148.8 1.1975730657577517 149.6 1.2242840528488159 150.4 1.3109053373336792 151.2 1.2888703346252439
		 152 1.2150799036026001 152.8 1.1827001571655271 153.6 1.1875312328338623 154.4 1.1778560876846311
		 155.2 1.1574008464813232 156 1.1635457277297974 156.8 1.1386988162994385 157.6 1.0852552652359009
		 158.4 1.0504950284957886 159.2 1.0973564386367798 160 1.234540581703186 160.8 1.3342053890228271
		 161.6 1.299297571182251 162.4 1.1996645927429199 163.2 1.1334807872772217 164 1.1485737562179563
		 164.8 1.1978942155838013 165.6 1.189035177230835 166.4 1.1219165325164795 167.2 1.1361372470855713
		 168 1.2896982431411745 168.8 1.5241935253143313 169.6 1.6192959547042849 170.4 1.4709315299987793
		 171.2 1.2564622163772583 172 1.2044391632080078 172.8 1.2289718389511108 173.6 1.2801591157913208
		 174.4 1.2911994457244873 175.2 1.311244010925293 176 1.2911554574966433 176.8 1.2217934131622314
		 177.6 1.1652696132659912 178.4 1.1004412174224854 179.2 1.081670880317688 180 1.1062371730804443
		 180.8 1.1243234872817991;
createNode animCurveTA -n "Bip01_L_Foot_rotateZ17";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 227 ".ktv[0:226]"  0 0.091750249266624451 0.8 0.090066641569137573
		 1.6 0.075058393180370331 2.4 0.062123581767082214 3.2 0.064821995794773102 4 0.20748852193355563
		 4.8 0.36568599939346313 5.6 0.30349606275558472 6.4 0.08540797233581543 7.2 -0.058659855276346207
		 8 -0.040612731128931046 8.8 0.04021839052438736 9.6 0.13862399756908417 10.4 0.2331304252147674
		 11.2 0.317583829164505 12 0.33435842394828796 12.8 0.32652643322944641 13.6 0.39832478761672974
		 14.4 0.47058334946632391 15.2 0.39831909537315374 16 0.31009361147880554 16.8 0.36160936951637263
		 17.6 0.44171544909477234 18.4 0.38576382398605352 19.2 0.27617651224136353 20 0.23701427876949313
		 20.8 0.22166010737419128 21.6 0.1686377227306366 22.4 0.14531424641609192 23.2 0.19468934834003448
		 24 0.28280195593833929 24.8 0.34477221965789795 25.6 0.31504839658737183 26.4 0.19655352830886841
		 27.2 0.058824926614761359 28 -0.04352613165974617 28.8 -0.094976946711540222 29.6 -0.061760913580656038
		 30.4 0.014502885751426218 31.2 0.10079912841320038 32 0.12660928070545197 32.8 0.10406762361526488
		 33.6 0.073085449635982513 34.4 0.091669760644435883 35.2 0.29454398155212402 36 0.53255975246429443
		 36.8 0.44045349955558777 37.6 0.19383127987384796 38.4 0.13490206003189087 39.2 0.22213971614837649
		 40 0.30781403183937073 40.8 0.33683976531028748 41.6 0.27192166447639465 42.4 0.19332347810268399
		 43.2 0.16888090968132022 44 0.1892441064119339 44.8 0.22693827748298648 45.6 0.28476607799530029
		 46.4 0.32840287685394287 47.2 0.33636552095413208 48 0.26289212703704834 48.8 0.16099034249782562
		 49.6 0.069745592772960677 50.4 -0.056570947170257575 51.2 -0.21552054584026337 52 -0.45965623855590815
		 52.8 -0.74741274118423462 53.6 -0.92383682727813732 54.4 -0.97528636455535866 55.2 -1.0495835542678833
		 56 -1.2853742837905884 56.8 -1.5955948829650879 57.6 -1.8094542026519771 58.4 -2.0217804908752441
		 59.2 -2.4080872535705566 60 -2.849479198455811 60.8 -3.1507120132446289 61.6 -3.1653385162353516
		 62.4 -3.0884709358215332 63.2 -3.1658511161804199 64 -3.263451099395752 64.8 -3.1827414035797119
		 65.6 -2.9975287914276123 66.4 -2.9417150020599365 67.2 -3.1277570724487305 68 -3.2787294387817383
		 68.8 -3.2297956943511967 69.6 -3.1838645935058594 70.4 -3.1705303192138672 71.2 -3.0858149528503418
		 72 -2.9887382984161377 72.8 -2.9675672054290771 73.6 -2.9129264354705811 74.4 -2.8680484294891357
		 75.2 -2.9017579555511475 76 -2.990821361541748 76.8 -3.0344839096069336 77.6 -3.0413894653320313
		 78.4 -3.0597729682922363 79.2 -3.0340340137481689 80 -2.9256949424743652 80.8 -2.8649139404296875
		 81.6 -2.8580362796783447 82.4 -2.8573901653289795 83.2 -2.9154233932495117 84 -3.022785902023315
		 84.8 -3.084752082824707 85.6 -3.0696089267730713 86.4 -3.0081119537353516 87.2 -3.0217795372009277
		 88 -3.0281107425689697 88.8 -2.90134596824646 89.6 -2.6508927345275879 90.4 -2.4880335330963135
		 91.2 -2.5627763271331787 92 -2.7137532234191895 92.8 -2.7931413650512695 93.6 -2.8881571292877197
		 94.4 -3.0052640438079834 95.2 -3.0445153713226318 96 -3.013136625289917 96.8 -2.9450571537017822
		 97.6 -2.7792267799377441 98.4 -2.5238871574401855 99.2 -2.3726391792297363 100 -2.4566042423248291
		 100.8 -2.6103451251983643 101.6 -2.5657944679260254 102.4 -2.4176299571990967 103.2 -2.3855881690979004
		 104 -2.3818032741546631 104.8 -2.1643815040588379 105.6 -1.972779393196106 106.4 -2.1002638339996338
		 107.2 -2.3021910190582275 108 -2.228995561599731 108.8 -2.0187392234802246 109.6 -1.9405767917633054
		 110.4 -1.9557722806930546 111.2 -1.9206657409667969 112 -1.807350754737854 112.8 -1.6547616720199585
		 113.6 -1.612265944480896 114.4 -1.6969101428985598 115.2 -1.6829349994659424 116 -1.5789991617202761
		 116.8 -1.5038307905197144 117.6 -1.496046781539917 118.4 -1.5705846548080444 119.2 -1.6267731189727783
		 120 -1.55279541015625 120.8 -1.4015138149261477 121.6 -1.3116117715835571 122.4 -1.3584227561950684
		 123.2 -1.4801528453826904 124 -1.5603101253509521 124.8 -1.5465701818466189 125.6 -1.4661316871643066
		 126.4 -1.4172556400299072 127.2 -1.467140316963196 128 -1.5573409795761108 128.8 -1.5897238254547119
		 129.6 -1.6071799993515017 130.4 -1.6183724403381348 131.2 -1.6364014148712158 132 -1.7039096355438232
		 132.8 -1.7737268209457395 133.6 -1.8034266233444218 134.4 -1.8457784652709961 135.2 -1.9243379831314087
		 136 -1.980802536010742 136.8 -1.9585410356521604 137.6 -1.8984479904174798 138.4 -1.9133678674697876
		 139.2 -1.9574815034866333 140 -1.9295041561126711 140.8 -1.9180814027786259 141.6 -1.9948798418045044
		 142.4 -2.0891861915588379 143.2 -2.214954137802124 144 -2.3448390960693359 144.8 -2.3724844455718994
		 145.6 -2.2588546276092529 146.4 -2.1722936630249023 147.2 -2.1990687847137451 148 -2.1836161613464355
		 148.8 -1.9961197376251223 149.6 -1.7806668281555176 150.4 -1.751190185546875 151.2 -1.876325845718384
		 152 -1.976009249687195 152.8 -2.0269794464111328 153.6 -2.0774664878845215 154.4 -2.0603830814361572
		 155.2 -1.9067829847335815 156 -1.8006865978240969 156.8 -1.8752955198287959 157.6 -2.0535104274749756
		 158.4 -2.1908800601959233 159.2 -2.1704115867614746 160 -1.9862287044525149 160.8 -1.8350616693496702
		 161.6 -1.8228659629821777 162.4 -1.7903218269348145 163.2 -1.8147294521331787 164 -1.9801172018051147
		 164.8 -2.080312967300415 165.6 -2.0637738704681396 166.4 -2.0540683269500732 167.2 -2.0858144760131836
		 168 -2.104506254196167 168.8 -2.0644547939300537 169.6 -2.0545070171356201 170.4 -2.1852343082427979
		 171.2 -1.9662672281265263 172 -1.9588582515716553 172.8 -1.7915550470352175 173.6 -1.5886315107345581
		 174.4 -1.4999167919158936 175.2 -1.63857102394104 176 -1.8046498298645015 176.8 -1.806334972381592
		 177.6 -1.5679563283920288 178.4 -1.2855501174926758 179.2 -1.136461615562439 180 -1.1137008666992187
		 180.8 -1.1820882558822632;
createNode animCurveTA -n "Bip01_L_Toe0_rotateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 227 ".ktv[0:226]"  0 -5.5797040658944752e-006 0.8 -4.1293251342722215e-006
		 1.6 -2.205848886660533e-006 2.4 -2.5977490167861106e-006 3.2 -6.7960031628899742e-006
		 4 2.4204442183872743e-007 4.8 -9.9193448477308266e-006 5.6 -3.8526172829733696e-006
		 6.4 -4.1836742639134172e-006 7.2 2.3502911972173024e-006 8 -2.9857069421268538e-006
		 8.8 -3.8391603993659382e-006 9.6 -6.7464243329595774e-006 10.4 -4.4540897761180531e-006
		 11.2 4.7901539801387116e-007 12 -5.7980073506769259e-006 12.8 6.6614387606023229e-007
		 13.6 -4.8700449042371474e-006 14.4 -4.8973079174174927e-006 15.2 3.6991966112509544e-007
		 16 9.7719021141529083e-007 16.8 -8.6256022768793628e-006 17.6 -4.1191524360328913e-006
		 18.4 -5.4781348808319316e-006 19.2 -8.6109585026861168e-006 20 -3.2723685308155837e-006
		 20.8 -8.1003390732803382e-006 21.6 -2.8691292754956521e-006 22.4 -7.3567566687415811e-006
		 23.2 -5.4910447033762466e-006 24 -4.460975560505176e-006 24.8 -3.830525656667306e-006
		 25.6 -1.2661582786677172e-006 26.4 -6.7322935137781315e-006 27.2 -7.8281236710608971e-006
		 28 -8.9892737378249876e-006 28.8 -3.3253984383918578e-006 29.6 -4.2213218875986058e-006
		 30.4 -2.4037287857936462e-006 31.2 -3.2008932748794905e-007 32 -2.4833557290548924e-006
		 32.8 -3.8124283037177524e-006 33.6 3.3246467410208425e-006 34.4 -4.3620148062473163e-006
		 35.2 -8.0988475019694306e-006 36 5.169304628793725e-008 36.8 -4.2312149162171409e-006
		 37.6 -3.8771008803450968e-006 38.4 -4.9147211029776372e-006 39.2 -1.3877868241252145e-006
		 40 -5.2877721827826463e-006 40.8 -2.9603438633785117e-006 41.6 -8.9519326138542965e-006
		 42.4 -2.7531559680937789e-006 43.2 -3.8938801480981056e-006 44 -1.256414407180273e-006
		 44.8 -1.8045924434773042e-006 45.6 -6.0275178839219734e-006 46.4 -7.8573084465460852e-006
		 47.2 -1.0234801266051363e-005 48 -5.7365823522559367e-006 48.8 -4.2807582758541693e-006
		 49.6 -2.3969530502654379e-006 50.4 -1.1915742106793914e-005 51.2 1.1010436118397138e-008
		 52 -1.0699674930947367e-005 52.8 -7.5132862775717523e-007 53.6 -5.620170668407809e-006
		 54.4 -1.7514921637484806e-006 55.2 -4.219530637783464e-006 56 -1.773025473994494e-006
		 56.8 -6.0117949942650739e-006 57.6 -2.6456157229404198e-006 58.4 -1.7050338101398663e-006
		 59.2 -7.1248864514927854e-006 60 -5.8218693084199913e-006 60.8 -5.5230339057743549e-006
		 61.6 -4.1985522329923697e-006 62.4 -5.8733489822770935e-006 63.2 -1.8956511667056473e-006
		 64 -1.3008706218897714e-006 64.8 -3.9975948311621306e-006 65.6 -2.3886561848485144e-006
		 66.4 -4.6649684009025805e-006 67.2 -1.2222153600305321e-005 68 -4.1438665903115179e-006
		 68.8 -3.6073431601835182e-006 69.6 -6.9412585617101286e-006 70.4 -3.0348262498591794e-006
		 71.2 -4.047638412885135e-006 72 -5.102018803881947e-006 72.8 -7.1765189204597854e-006
		 73.6 -8.4951881262895768e-007 74.4 -6.1560276662930846e-006 75.2 -9.1251104095135815e-006
		 76 -7.5261359597789132e-006 76.8 -1.584257688591606e-006 77.6 -8.6484833445865678e-006
		 78.4 -4.7875887503323611e-006 79.2 -3.7458423776115524e-006 80 -4.1410603444091976e-006
		 80.8 -3.8619282349827682e-006 81.6 -6.4131063481909223e-006 82.4 -6.5299573179800063e-006
		 83.2 -4.1389453144802246e-006 84 -8.9917839432018809e-006 84.8 -6.1132673181418795e-006
		 85.6 -8.3786244431394152e-006 86.4 -7.0796063482703184e-006 87.2 -1.3358936485019512e-005
		 88 -4.4319417611404788e-006 88.8 -6.639864750468405e-006 89.6 -3.7601832900691079e-006
		 90.4 -5.1058427743555512e-006 91.2 -4.6860827751515899e-006 92 -3.0323549253807873e-006
		 92.8 -1.4743837937203352e-006 93.6 -6.0377351474016905e-006 94.4 -2.9761204132228158e-006
		 95.2 -2.7008557026420021e-006 96 -1.7755428416421637e-006 96.8 -3.2880413982638856e-006
		 97.6 -3.7152431104914289e-006 98.4 -3.723149575307616e-006 99.2 -5.8782625274034217e-006
		 100 -3.3639892649262043e-007 100.8 -8.7946646090131253e-006 101.6 -6.1285913943720516e-006
		 102.4 -1.0823407137650063e-005 103.2 -5.4618576541543007e-006 104 -3.8704133658029622e-006
		 104.8 -5.0810817810997833e-006 105.6 -4.1992871047114022e-006 106.4 -9.1303490989957936e-006
		 107.2 -5.6242629398184363e-006 108 -7.6623327913694084e-006 108.8 -3.7688498650823026e-006
		 109.6 -9.5725056326045888e-007 110.4 -3.8838625187054268e-006 111.2 -2.8289650799706574e-006
		 112 4.1475909711152781e-006 112.8 -8.1656244219630025e-006 113.6 -2.647176870596013e-006
		 114.4 -8.1039188444265164e-006 115.2 3.1939052860252563e-006 116 -6.4152109189308248e-006
		 116.8 -1.8951559468405319e-006 117.6 -4.4033536141796503e-006 118.4 -7.1338204179483009e-006
		 119.2 -8.339047781191768e-006 120 -9.6252697403542697e-006 120.8 -4.7994276428653393e-006
		 121.6 -5.811713776893157e-007 122.4 -2.9775685561617138e-006 123.2 -4.6877089516783599e-006
		 124 9.2555166020247292e-007 124.8 -3.518886614983785e-006 125.6 -3.7635472835972905e-006
		 126.4 -2.9751879537798231e-006 127.2 1.2843973991039093e-006 128 -3.5598186514107515e-006
		 128.8 -1.3079564951112843e-006 129.6 -4.2039791878778487e-006 130.4 2.2733290734322509e-006
		 131.2 -6.762134489690653e-006 132 -4.9224800022784621e-006 132.8 -5.9871758821827834e-006
		 133.6 -6.5988688220386402e-006 134.4 -4.5609226617671084e-006 135.2 -3.0296291697595734e-006
		 136 -2.9231259759399109e-006 136.8 -3.6061589980818094e-006 137.6 -6.6859552134701516e-006
		 138.4 -2.9481750516424658e-006 139.2 2.9498255571525078e-006 140 -6.9834059104323387e-006
		 140.8 -7.0267324190353983e-006 141.6 -6.0021711760782637e-006 142.4 -3.9819078665459529e-006
		 143.2 -6.8670065047626849e-006 144 -5.2478858378890436e-006 144.8 2.568325285778883e-008
		 145.6 -1.0322559319320137e-005 146.4 -8.2995356933679432e-006 147.2 -4.9253171710006427e-006
		 148 3.1272060141418478e-007 148.8 -6.2903918660595082e-006 149.6 -1.1065323633374646e-005
		 150.4 -6.0017455325578339e-006 151.2 -6.753743036824745e-006 152 -5.5179075388878118e-006
		 152.8 -1.3453050087264271e-006 153.6 -3.7922261526546212e-006 154.4 -3.4932850212499029e-006
		 155.2 -2.6828070076589938e-006 156 -3.8647640394628962e-006 156.8 -2.9725902095378842e-006
		 157.6 -2.8503006888058735e-006 158.4 -1.8551912717157393e-006 159.2 -4.1798311940510757e-006
		 160 -5.8363907555758487e-006 160.8 -4.5037154450255912e-006 161.6 -6.7598348323372193e-006
		 162.4 -5.4377628657675822e-006 163.2 -6.2096387409837916e-006 164 -7.2135944719775571e-006
		 164.8 -8.4356879597180523e-006 165.6 -6.612015567952767e-006 166.4 -8.9730219769990072e-006
		 167.2 -5.9053509176010266e-006 168 -7.1578374445380169e-006 168.8 -1.745357621985022e-006
		 169.6 -5.951987077423837e-006 170.4 -2.5317276595160365e-006 171.2 -3.9031592677929439e-006
		 172 -5.8673350622484577e-007 172.8 -6.6589968810149003e-006 173.6 -6.0296447372820694e-006
		 174.4 -1.0070089047076181e-006 175.2 -4.6650725380459335e-006 176 1.6234572512985326e-006
		 176.8 -7.0037613113527186e-006 177.6 -6.915158337506e-006 178.4 -1.1774093763960991e-005
		 179.2 -4.7755188461451326e-006 180 -4.0364207052334677e-006 180.8 -1.1699640936058131e-006;
createNode animCurveTA -n "Bip01_L_Toe0_rotateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 227 ".ktv[0:226]"  0 8.3183140304754488e-006 0.8 6.4001878854469396e-006
		 1.6 2.4781709271337608e-006 2.4 6.4130081227631308e-006 3.2 4.4022381189279258e-006
		 4 5.6755557125143241e-006 4.8 1.2426809007592965e-005 5.6 5.1243596317362972e-006
		 6.4 6.119706085883081e-006 7.2 2.2103583887655982e-006 8 1.0197795745625626e-005
		 8.8 7.3847918429237333e-006 9.6 6.2456833802571055e-006 10.4 4.3108493628096767e-006
		 11.2 3.4166455407103058e-006 12 -3.6074527542950823e-006 12.8 1.0176599062106106e-005
		 13.6 4.1673406485642772e-006 14.4 3.4491488349885913e-006 15.2 1.3685936437468629e-006
		 16 4.4430812522477936e-006 16.8 4.2662504711188376e-006 17.6 5.441461325972341e-007
		 18.4 4.3757258936238941e-006 19.2 8.1255229815724306e-006 20 -7.4760839652299182e-007
		 20.8 4.0121294659911655e-006 21.6 6.8056274358241353e-006 22.4 6.2848780544300098e-006
		 23.2 1.2852331110480009e-006 24 4.0429285945720039e-006 24.8 8.2854285210487433e-006
		 25.6 -1.4176986269376357e-006 26.4 7.0532018980884459e-006 27.2 7.8852162914699875e-006
		 28 4.6053055484662764e-006 28.8 1.9953190530941356e-006 29.6 2.048958549494273e-006
		 30.4 7.2867433118517502e-006 31.2 -6.4056600024287036e-008 32 3.506253506202484e-006
		 32.8 8.6549916886724532e-006 33.6 -1.9349649846844841e-006 34.4 -8.2095573361584684e-007
		 35.2 4.3563022700254805e-006 36 9.869320820143912e-006 36.8 8.1483158282935619e-006
		 37.6 2.8156246116850525e-006 38.4 1.6679557575116633e-006 39.2 4.3589020606304985e-006
		 40 3.7364959553087829e-006 40.8 5.6381090871582273e-006 41.6 8.9061813923763111e-006
		 42.4 3.4827455692720832e-006 43.2 1.6033633301049122e-006 44 4.5177714014243975e-008
		 44.8 6.2176277424441651e-006 45.6 6.2333019741345197e-006 46.4 1.3403815728452173e-006
		 47.2 1.2300787602725904e-005 48 7.9109249782050028e-006 48.8 9.1925285232719034e-006
		 49.6 -1.3092568451611442e-007 50.4 3.2745701901149005e-006 51.2 1.054811855283333e-005
		 52 8.9310651674168184e-006 52.8 6.3207744460669346e-006 53.6 2.2375327262125211e-006
		 54.4 3.1672586828790372e-006 55.2 8.9910872702603228e-006 56 1.2101229913241696e-005
		 56.8 -8.1635084825393278e-007 57.6 6.4643222685845112e-006 58.4 4.9512373152538203e-006
		 59.2 1.2470090041460935e-005 60 3.6380854453454963e-006 60.8 3.6848803119937665e-006
		 61.6 2.4416185624431819e-006 62.4 1.3136096640664618e-005 63.2 4.4677431105810683e-006
		 64 1.5027158042357771e-005 64.8 -1.1954590490859118e-006 65.6 3.436490942476667e-006
		 66.4 1.0062218507300713e-006 67.2 6.8487588578136646e-006 68 6.9774051780768787e-007
		 68.8 2.9797015486110467e-006 69.6 4.3436780288175214e-006 70.4 5.7879411485828314e-008
		 71.2 5.5495083870482631e-006 72 1.1824541616078932e-005 72.8 7.0853620854904875e-006
		 73.6 -2.3154291284299693e-006 74.4 2.4111227503453851e-006 75.2 4.4062808228773065e-006
		 76 6.4144819589273538e-006 76.8 2.1064961401862092e-006 77.6 8.6266363723552786e-006
		 78.4 1.6427341051894473e-006 79.2 2.4975131509563653e-006 80 5.4681763685948681e-006
		 80.8 5.8363871175970417e-006 81.6 -1.0747418173195913e-006 82.4 3.2075101898954017e-006
		 83.2 1.1240485036978498e-005 84 4.2633282646420412e-006 84.8 5.1925744628533729e-006
		 85.6 4.4278008317633066e-006 86.4 4.0745298974798069e-006 87.2 9.363913704873994e-006
		 88 3.9623596421733981e-006 88.8 4.1296557355963159e-006 89.6 6.7464463882060954e-007
		 90.4 3.1976451282389462e-006 91.2 2.009602667385479e-006 92 -1.9077181150350953e-006
		 92.8 6.864543138362933e-006 93.6 1.0946562269964488e-006 94.4 7.6458836701931398e-006
		 95.2 -1.4126844405382144e-007 96 -2.5177055249514524e-006 96.8 4.2513552216405515e-006
		 97.6 -1.9163048818882089e-006 98.4 2.35639134871235e-007 99.2 2.2365509266819572e-006
		 100 -9.6645055691624293e-007 100.8 7.2541320150776301e-006 101.6 1.2911345038446598e-005
		 102.4 1.1029450433852617e-005 103.2 1.251204253094329e-006 104 3.3070375593524659e-006
		 104.8 2.3113841507438337e-006 105.6 6.1702667153440416e-006 106.4 1.1650419764919208e-005
		 107.2 7.1270783337240573e-006 108 -1.9429980966378939e-006 108.8 8.4874991443939507e-006
		 109.6 2.4126647986122407e-006 110.4 2.0056070297869155e-006 111.2 3.1084080092114164e-006
		 112 5.2962705012760125e-006 112.8 8.7869930212036707e-006 113.6 1.919514716064441e-006
		 114.4 -1.7238359077964562e-006 115.2 9.3666885732091032e-006 116 -4.9070076784119018e-006
		 116.8 3.0813728244538652e-006 117.6 3.4750084978441022e-006 118.4 -8.7234172951866608e-007
		 119.2 3.4586539641168206e-006 120 3.047808831979637e-006 120.8 4.085232376382919e-006
		 121.6 1.2123305168643128e-005 122.4 -1.7273788444072125e-007 123.2 7.1691533776174774e-006
		 124 5.1539805099309888e-006 124.8 -1.2260163657629164e-006 125.6 4.6051181925577112e-006
		 126.4 7.8768907769699581e-006 127.2 3.2761229249445027e-006 128 3.0066073577472707e-006
		 128.8 1.1089322470070329e-005 129.6 2.887437858589692e-006 130.4 7.2887569331214755e-006
		 131.2 3.841753368760692e-006 132 6.322467015706934e-006 132.8 2.3224476990435505e-006
		 133.6 7.4096778917009908e-006 134.4 6.6506258917797823e-006 135.2 1.0063546369565302e-006
		 136 3.9354649743472692e-006 136.8 2.6235827590426197e-006 137.6 1.1296925549686421e-005
		 138.4 2.0645074982894585e-006 139.2 5.2141558626317419e-006 140 4.2983660932804932e-006
		 140.8 4.1130683712253813e-006 141.6 9.0994984702774652e-007 142.4 2.7007647531718249e-006
		 143.2 2.5490855932730483e-006 144 5.9089566093462054e-006 144.8 5.5413693189620972e-006
		 145.6 6.7479213612386957e-006 146.4 -1.2195516774227144e-006 147.2 -7.0840025045981758e-007
		 148 3.1152849260251969e-006 148.8 9.837053767114412e-006 149.6 7.5216439654468567e-006
		 150.4 1.0471310815773904e-005 151.2 7.0414380388683648e-006 152 6.0693619161611423e-006
		 152.8 1.109801814891398e-005 153.6 6.1787914091837601e-006 154.4 6.9348407123470687e-006
		 155.2 3.7323475226003203e-006 156 5.9414737734186937e-006 156.8 5.3676367315347306e-006
		 157.6 1.9796809738181764e-006 158.4 6.4046253100968897e-006 159.2 1.0737703632912603e-006
		 160 6.4507876231800765e-006 160.8 2.0290156044211471e-007 161.6 9.7102912377522443e-008
		 162.4 3.2578930131421657e-007 163.2 2.7199632768315496e-006 164 -6.9491784415731664e-006
		 164.8 3.4387012419756506e-006 165.6 1.0908544254562005e-006 166.4 4.4116045501141343e-006
		 167.2 9.6918829513015226e-006 168 2.6135003281524405e-006 168.8 5.2207024054951043e-006
		 169.6 1.3591298738901969e-005 170.4 -2.9000452741456684e-006 171.2 1.1346582141413819e-005
		 172 1.1065335456805768e-005 172.8 5.9408148445072584e-006 173.6 9.2563286671065725e-006
		 174.4 3.1757597298565092e-006 175.2 -1.2523557870736113e-006 176 -2.0011279957543593e-006
		 176.8 9.5676807632116834e-007 177.6 5.3585772548103705e-006 178.4 -8.8906887185657979e-007
		 179.2 4.3744107642851313e-006 180 1.0202181783824926e-006 180.8 6.0202432905498426e-006;
createNode animCurveTA -n "Bip01_L_Toe0_rotateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 227 ".ktv[0:226]"  0 89.999977111816406 0.8 89.999961853027344
		 1.6 89.999961853027344 2.4 89.999961853027344 3.2 89.999977111816406 4 89.999977111816406
		 4.8 89.999977111816406 5.6 89.999977111816406 6.4 89.999977111816406 7.2 89.999977111816406
		 8 89.999977111816406 8.8 89.999961853027344 9.6 89.999961853027344 10.4 89.999977111816406
		 11.2 89.999961853027344 12 89.999977111816406 12.8 89.999984741210937 13.6 89.999961853027344
		 14.4 89.999977111816406 15.2 89.999961853027344 16 89.999977111816406 16.8 89.999961853027344
		 17.6 89.999977111816406 18.4 89.999977111816406 19.2 89.999977111816406 20 89.999984741210937
		 20.8 89.999961853027344 21.6 89.999961853027344 22.4 89.999977111816406 23.2 89.999977111816406
		 24 89.999984741210937 24.8 89.999977111816406 25.6 89.999977111816406 26.4 89.999984741210937
		 27.2 89.999977111816406 28 89.999977111816406 28.8 89.999977111816406 29.6 89.999977111816406
		 30.4 89.999977111816406 31.2 89.999977111816406 32 89.999977111816406 32.8 89.999977111816406
		 33.6 89.999977111816406 34.4 89.999977111816406 35.2 89.999977111816406 36 89.999977111816406
		 36.8 89.999977111816406 37.6 89.999977111816406 38.4 89.999977111816406 39.2 89.999977111816406
		 40 89.999977111816406 40.8 89.999961853027344 41.6 89.999977111816406 42.4 89.999977111816406
		 43.2 89.999977111816406 44 89.999961853027344 44.8 89.999961853027344 45.6 89.999977111816406
		 46.4 89.999977111816406 47.2 89.999961853027344 48 89.999977111816406 48.8 89.999984741210937
		 49.6 89.999977111816406 50.4 89.999961853027344 51.2 89.999977111816406 52 89.999961853027344
		 52.8 89.999977111816406 53.6 89.999961853027344 54.4 89.999977111816406 55.2 89.999984741210937
		 56 89.999977111816406 56.8 89.999977111816406 57.6 89.999977111816406 58.4 89.999977111816406
		 59.2 89.999977111816406 60 89.999961853027344 60.8 89.999977111816406 61.6 89.999961853027344
		 62.4 89.999961853027344 63.2 89.999954223632813 64 89.999977111816406 64.8 89.999977111816406
		 65.6 89.999961853027344 66.4 89.999977111816406 67.2 89.999977111816406 68 89.999961853027344
		 68.8 89.999977111816406 69.6 89.999977111816406 70.4 89.999977111816406 71.2 89.999984741210937
		 72 89.999977111816406 72.8 89.999977111816406 73.6 89.999977111816406 74.4 89.999977111816406
		 75.2 89.999977111816406 76 89.999961853027344 76.8 89.999977111816406 77.6 89.999977111816406
		 78.4 89.999961853027344 79.2 89.999977111816406 80 89.999977111816406 80.8 89.999961853027344
		 81.6 89.999977111816406 82.4 89.999977111816406 83.2 89.999977111816406 84 89.999977111816406
		 84.8 89.999961853027344 85.6 89.999977111816406 86.4 89.999977111816406 87.2 89.999984741210937
		 88 89.999961853027344 88.8 89.999977111816406 89.6 89.999977111816406 90.4 89.999977111816406
		 91.2 89.999977111816406 92 89.999977111816406 92.8 89.999977111816406 93.6 89.999977111816406
		 94.4 89.999977111816406 95.2 89.999961853027344 96 89.999977111816406 96.8 89.999977111816406
		 97.6 89.999977111816406 98.4 89.999977111816406 99.2 89.999977111816406 100 89.999977111816406
		 100.8 89.999977111816406 101.6 89.999961853027344 102.4 89.999961853027344 103.2 89.999961853027344
		 104 89.999977111816406 104.8 89.999977111816406 105.6 89.999977111816406 106.4 89.999977111816406
		 107.2 89.999977111816406 108 89.999961853027344 108.8 89.999984741210937 109.6 89.999977111816406
		 110.4 89.999977111816406 111.2 89.999961853027344 112 89.999977111816406 112.8 89.999977111816406
		 113.6 89.999977111816406 114.4 89.999977111816406 115.2 89.999977111816406 116 89.999961853027344
		 116.8 89.999977111816406 117.6 89.999977111816406 118.4 89.999977111816406 119.2 89.999977111816406
		 120 89.999977111816406 120.8 89.999961853027344 121.6 89.999977111816406 122.4 89.999977111816406
		 123.2 89.999977111816406 124 89.999977111816406 124.8 89.999977111816406 125.6 89.999977111816406
		 126.4 89.999977111816406 127.2 89.999961853027344 128 89.999961853027344 128.8 89.999961853027344
		 129.6 89.999977111816406 130.4 89.999977111816406 131.2 89.999977111816406 132 89.999977111816406
		 132.8 89.999977111816406 133.6 89.999961853027344 134.4 89.999977111816406 135.2 89.999977111816406
		 136 89.999961853027344 136.8 89.999977111816406 137.6 89.999977111816406 138.4 89.999977111816406
		 139.2 89.999961853027344 140 89.999961853027344 140.8 89.999977111816406 141.6 89.999961853027344
		 142.4 89.999977111816406 143.2 89.999977111816406 144 89.999961853027344 144.8 89.999977111816406
		 145.6 89.999977111816406 146.4 89.999977111816406 147.2 89.999977111816406 148 89.999977111816406
		 148.8 89.999977111816406 149.6 89.999977111816406 150.4 89.999961853027344 151.2 89.999977111816406
		 152 89.999977111816406 152.8 89.999977111816406 153.6 89.999977111816406 154.4 89.999977111816406
		 155.2 89.999961853027344 156 89.999977111816406 156.8 89.999977111816406 157.6 89.999961853027344
		 158.4 89.999977111816406 159.2 89.999977111816406 160 89.999977111816406 160.8 89.999977111816406
		 161.6 89.999961853027344 162.4 89.999977111816406 163.2 89.999977111816406 164 89.999977111816406
		 164.8 89.999977111816406 165.6 89.999977111816406 166.4 89.999977111816406 167.2 89.999977111816406
		 168 89.999961853027344 168.8 89.999977111816406 169.6 89.999977111816406 170.4 89.999977111816406
		 171.2 89.999977111816406 172 89.999977111816406 172.8 89.999961853027344 173.6 89.999977111816406
		 174.4 89.999977111816406 175.2 89.999977111816406 176 89.999977111816406 176.8 89.999961853027344
		 177.6 89.999961853027344 178.4 89.999977111816406 179.2 89.999977111816406 180 89.999977111816406
		 180.8 89.999977111816406;
createNode animCurveTA -n "Bip01_R_Thigh_rotateX17";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 227 ".ktv[0:226]"  0 1.0872294902801514 0.8 1.0751537084579468
		 1.6 1.0646488666534424 2.4 1.0557559728622437 3.2 1.0449368953704834 4 1.0449506044387815
		 4.8 1.0413988828659058 5.6 1.0352927446365356 6.4 1.0384619235992432 7.2 1.0334212779998782
		 8 1.0333255529403689 8.8 1.0336260795593262 9.6 1.0363172292709353 10.4 1.0463166236877439
		 11.2 1.0497590303421021 12 1.0626134872436523 12.8 1.0645805597305298 13.6 1.0683234930038452
		 14.4 1.0667798519134519 15.2 1.0858224630355835 16 1.0917236804962158 16.8 1.1012040376663208
		 17.6 1.1017504930496216 18.4 1.1101106405258181 19.2 1.112514853477478 20 1.122746467590332
		 20.8 1.1275002956390381 21.6 1.1278964281082151 22.4 1.1289482116699221 23.2 1.1241670846939087
		 24 1.119686484336853 24.8 1.1161757707595823 25.6 1.0983762741088867 26.4 1.0954257249832151
		 27.2 1.0942645072937012 28 1.075891375541687 28.8 1.0533380508422852 29.6 1.0372461080551147
		 30.4 1.0058135986328125 31.2 0.98790484666824319 32 0.96924477815628052 32.8 0.95395880937576305
		 33.6 0.93508017063140869 34.4 0.92446607351303101 35.2 0.90575134754180908 36 0.90367496013641357
		 36.8 0.892514407634735 37.6 0.88364887237548828 38.4 0.88676339387893699 39.2 0.89726823568344127
		 40 0.90051943063735984 40.8 0.91732168197631836 41.6 0.94025743007659923 42.4 0.95092618465423573
		 43.2 0.96458655595779408 44 0.97345215082168612 44.8 0.99754905700683616 45.6 1.0300061702728271
		 46.4 1.0594989061355593 47.2 1.106586217880249 48 1.1464744806289673 48.8 1.1739455461502075
		 49.6 1.2168664932250977 50.4 1.2620686292648315 51.2 1.3173521757125854 52 1.3687561750411987
		 52.8 1.4190536737442017 53.6 1.4722745418548584 54.4 1.537625789642334 55.2 1.6096706390380859
		 56 1.6843382120132446 56.8 1.7630356550216679 57.6 1.8621826171874998 58.4 1.9596084356307983
		 59.2 2.048250675201416 60 2.1512634754180908 60.8 2.2471458911895752 61.6 2.3543796539306641
		 62.4 2.4643046855926514 63.2 2.5684378147125244 64 2.6632955074310303 64.8 2.763371467590332
		 65.6 2.852737665176392 66.4 2.9247961044311523 67.2 3.0083155632019047 68 3.0589547157287602
		 68.8 3.1005914211273193 69.6 3.1107547283172607 70.4 3.125767707824707 71.2 3.1156315803527832
		 72 3.1022171974182129 72.8 3.0742542743682861 73.6 3.024188995361329 74.4 2.9694383144378662
		 75.2 2.8827905654907227 76 2.8181359767913814 76.8 2.7367746829986572 77.6 2.6460015773773193
		 78.4 2.5504744052886967 79.2 2.4542508125305176 80 2.3555271625518799 80.8 2.2576780319213867
		 81.6 2.1615633964538574 82.4 2.0603947639465332 83.2 1.9714111089706421 84 1.888861417770386
		 84.8 1.8042763471603391 85.6 1.7300732135772705 86.4 1.6462258100509644 87.2 1.579235315322876
		 88 1.5050320625305176 88.8 1.4478497505187993 89.6 1.3940961360931396 90.4 1.3503420352935791
		 91.2 1.3072161674499512 92 1.2774912118911743 92.8 1.2477525472640991 93.6 1.2147763967514038
		 94.4 1.1810488700866699 95.2 1.1581541299819946 96 1.1427862644195561 96.8 1.1166812181472778
		 97.6 1.1023788452148437 98.4 1.0981167554855349 99.2 1.0904805660247805 100 1.0944967269897461
		 100.8 1.1030209064483645 101.6 1.1102608442306521 102.4 1.121803879737854 103.2 1.1406961679458618
		 104 1.1418709754943848 104.8 1.1691097021102903 105.6 1.1922094821929932 106.4 1.2134922742843628
		 107.2 1.2361001968383789 108 1.2995116710662842 108.8 1.3320097923278809 109.6 1.3810504674911499
		 110.4 1.4298044443130493 111.2 1.4801565408706665 112 1.5428167581558228 112.8 1.6103808879852295
		 113.6 1.6804176568984983 114.4 1.75098717212677 115.2 1.8263105154037476 116 1.9069614410400393
		 116.8 1.9828038215637207 117.6 2.066596508026123 118.4 2.1488592624664307 119.2 2.2245514392852783
		 120 2.2918970584869385 120.8 2.3619475364685059 121.6 2.4283232688903809 122.4 2.4859974384307861
		 123.2 2.5331666469573975 124 2.5666346549987793 124.8 2.604337215423584 125.6 2.6234071254730225
		 126.4 2.640127420425415 127.2 2.638023853302002 128 2.6195685863494873 128.8 2.609022855758667
		 129.6 2.5776722431182861 130.4 2.5364999771118164 131.2 2.4935927391052246 132 2.4547288417816162
		 132.8 2.4081196784973145 133.6 2.3524398803710937 134.4 2.302811861038208 135.2 2.2560250759124756
		 136 2.2144565582275391 136.8 2.1749916076660156 137.6 2.13804030418396 138.4 2.0950784683227539
		 139.2 2.0835354328155522 140 2.0803799629211426 140.8 2.0839042663574219 141.6 2.095146656036377
		 142.4 2.1272623538970947 143.2 2.173762321472168 144 2.2279665470123291 144.8 2.2955172061920166
		 145.6 2.3727529048919682 146.4 2.4725830554962158 147.2 2.5752406120300293 148 2.7011895179748535
		 148.8 2.8209362030029297 149.6 2.9668290615081787 150.4 3.1171889305114746 151.2 3.2712368965148926
		 152 3.4285907745361328 152.8 3.5815324783325195 153.6 3.737506628036499 154.4 3.8938496112823486
		 155.2 4.0491681098937997 156 4.1941866874694824 156.8 4.3302712440490723 157.6 4.4626269340515137
		 158.4 4.586212158203125 159.2 4.7007956504821777 160 4.7995328903198242 160.8 4.8872871398925781
		 161.6 4.9733881950378418 162.4 5.0436301231384277 163.2 5.1169452667236337 164 5.1804795265197754
		 164.8 5.2439184188842773 165.6 5.2962102890014648 166.4 5.3440489768981934 167.2 5.3937864303588867
		 168 5.4418301582336426 168.8 5.4783029556274414 169.6 5.5189156532287598 170.4 5.5654702186584473
		 171.2 5.6040196418762207 172 5.6419544219970703 172.8 5.6727585792541504 173.6 5.7061581611633301
		 174.4 5.7350773811340332 175.2 5.7570157051086426 176 5.7672200202941895 176.8 5.7784218788146973
		 177.6 5.7773013114929199 178.4 5.7640509605407715 179.2 5.7417845726013184 180 5.7039318084716797
		 180.8 5.659508228302002;
createNode animCurveTA -n "Bip01_R_Thigh_rotateY17";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 227 ".ktv[0:226]"  0 175.5743408203125 0.8 175.61669921875
		 1.6 175.6851806640625 2.4 175.75741577148435 3.2 175.78172302246094 4 175.75428771972656
		 4.8 175.74234008789065 5.6 175.78582763671875 6.4 175.84014892578125 7.2 175.88435363769531
		 8 175.92340087890625 8.8 175.95266723632812 9.6 175.97340393066406 10.4 175.96185302734375
		 11.2 175.95979309082031 12 175.98155212402344 12.8 176.01020812988281 13.6 176.02381896972656
		 14.4 176.02912902832031 15.2 176.04682922363281 16 176.045166015625 16.8 176.0167236328125
		 17.6 175.99909973144531 18.4 175.99404907226562 19.2 175.99609375 20 175.9849853515625
		 20.8 175.97755432128906 21.6 176.00173950195312 22.4 176.03997802734375 23.2 176.06533813476562
		 24 176.06982421875 24.8 176.05610656738281 25.6 176.03297424316406 26.4 176.01617431640628
		 27.2 176.00401306152344 28 176.00555419921875 28.8 176.00239562988281 29.6 175.99273681640625
		 30.4 175.98895263671875 31.2 175.98271179199219 32 175.96076965332031 32.8 175.93014526367187
		 33.6 175.89518737792969 34.4 175.88067626953125 35.2 175.88142395019531 36 175.87496948242187
		 36.8 175.84722900390625 37.6 175.81694030761719 38.4 175.80307006835935 39.2 175.80195617675781
		 40 175.80438232421875 40.8 175.80815124511719 41.6 175.79840087890625 42.4 175.75468444824219
		 43.2 175.70964050292969 44 175.70088195800781 44.8 175.71871948242187 45.6 175.77630615234375
		 46.4 175.85249328613281 47.2 175.93832397460935 48 176.04568481445312 48.8 176.1512451171875
		 49.6 176.24385070800781 50.4 176.36515808105469 51.2 176.54298400878906 52 176.77206420898435
		 52.8 177.00459289550781 53.6 177.18197631835935 54.4 177.32060241699219 55.2 177.54383850097656
		 56 177.88923645019531 56.8 178.29931640625 57.6 178.70260620117187 58.4 179.07408142089844
		 59.2 179.40167236328125 60 179.6771240234375 60.8 180.00704956054687 61.6 180.39537048339844
		 62.4 180.7301025390625 63.2 181.03430175781253 64 181.41307067871097 64.8 181.85447692871097
		 65.6 182.2860107421875 66.4 182.68515014648435 67.2 183.10426330566409 68 183.51263427734375
		 68.8 183.79869079589844 69.6 183.95579528808591 70.4 184.05427551269531 71.2 184.1387939453125
		 72 184.242919921875 72.8 184.36805725097656 73.6 184.47535705566409 74.4 184.535888671875
		 75.2 184.56700134277344 76 184.57423400878903 76.8 184.57388305664065 77.6 184.57710266113281
		 78.4 184.57893371582031 79.2 184.58250427246097 80 184.58160400390625 80.8 184.56282043457031
		 81.6 184.53089904785156 82.4 184.48945617675781 83.2 184.42756652832031 84 184.37132263183591
		 84.8 184.31744384765625 85.6 184.25236511230469 86.4 184.19009399414065 87.2 184.08332824707031
		 88 183.92356872558591 88.8 183.77398681640625 89.6 183.66023254394531 90.4 183.56004333496097
		 91.2 183.46540832519537 92 183.41835021972656 92.8 183.38819885253903 93.6 183.34197998046875
		 94.4 183.25918579101562 95.2 183.16571044921875 96 183.08795166015625 96.8 183.03721618652344
		 97.6 183.00790405273435 98.4 182.98355102539065 99.2 182.95726013183594 100 182.91816711425781
		 100.8 182.90013122558591 101.6 182.92382812500003 102.4 182.95297241210935 103.2 182.94252014160156
		 104 182.92388916015625 104.8 182.91456604003903 105.6 182.90382385253903 106.4 182.889404296875
		 107.2 182.87310791015625 108 182.87474060058591 108.8 182.89553833007812 109.6 182.90821838378903
		 110.4 182.91067504882812 111.2 182.90760803222656 112 182.90400695800781 112.8 182.89970397949219
		 113.6 182.92314147949219 114.4 182.93104553222656 115.2 182.92681884765625 116 182.912353515625
		 116.8 182.92411804199219 117.6 182.95082092285156 118.4 182.97262573242187 119.2 182.99070739746099
		 120 183.02714538574219 120.8 183.0618896484375 121.6 183.07218933105469 122.4 183.07403564453125
		 123.2 183.08642578125 124 183.10903930664065 124.8 183.12322998046875 125.6 183.12559509277344
		 126.4 183.14250183105469 127.2 183.15884399414065 128 183.15818786621097 128.8 183.17124938964844
		 129.6 183.2122802734375 130.4 183.25605773925781 131.2 183.27801513671875 132 183.26821899414065
		 132.8 183.23797607421881 133.6 183.21394348144531 134.4 183.20365905761719 135.2 183.17807006835935
		 136 183.14118957519531 136.8 183.12966918945313 137.6 183.13253784179687 138.4 183.12321472167969
		 139.2 183.10452270507812 140 183.07815551757812 140.8 183.04312133789065 141.6 183.0001220703125
		 142.4 182.95611572265625 143.2 182.91441345214844 144 182.8594970703125 144.8 182.81184387207031
		 145.6 182.77235412597656 146.4 182.74398803710935 147.2 182.70301818847656 148 182.64764404296875
		 148.8 182.61495971679687 149.6 182.61503601074219 150.4 182.60926818847656 151.2 182.59768676757812
		 152 182.60499572753903 152.8 182.62451171875 153.6 182.64503479003903 154.4 182.65292358398435
		 155.2 182.64097595214844 156 182.61520385742187 156.8 182.62611389160156 157.6 182.65711975097656
		 158.4 182.67225646972656 159.2 182.6761474609375 160 182.66963195800781 160.8 182.67137145996097
		 161.6 182.68304443359375 162.4 182.70649719238281 163.2 182.74089050292972 164 182.74925231933591
		 164.8 182.73690795898435 165.6 182.72361755371097 166.4 182.71339416503903 167.2 182.70252990722656
		 168 182.68748474121097 168.8 182.66238403320312 169.6 182.63308715820312 170.4 182.6287841796875
		 171.2 182.64506530761719 172 182.64802551269537 172.8 182.60263061523435 173.6 182.52374267578128
		 174.4 182.45262145996097 175.2 182.40715026855469 176 182.37422180175781 176.8 182.33906555175781
		 177.6 182.31001281738281 178.4 182.30003356933591 179.2 182.2725830078125 180 182.24754333496097
		 180.8 182.21151733398435;
createNode animCurveTA -n "Bip01_R_Thigh_rotateZ17";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 227 ".ktv[0:226]"  0 -4.7211904525756845 0.8 -4.7594666481018066
		 1.6 -4.8325772285461426 2.4 -4.9440183639526367 3.2 -5.0389037132263184 4 -5.1264939308166504
		 4.8 -5.170097827911377 5.6 -5.2243566513061523 6.4 -5.288724422454834 7.2 -5.3802490234375
		 8 -5.4531407356262207 8.8 -5.4794235229492187 9.6 -5.4901056289672852 10.4 -5.508847713470459
		 11.2 -5.4680581092834473 12 -5.4312839508056641 12.8 -5.4676752090454102 13.6 -5.4331421852111816
		 14.4 -5.3633103370666504 15.2 -5.2368149757385254 16 -5.1392254829406738 16.8 -5.1534318923950195
		 17.6 -5.2090573310852051 18.4 -5.1647157669067383 19.2 -5.0410890579223633 20 -4.9161238670349121
		 20.8 -4.7882080078125 21.6 -4.6619315147399902 22.4 -4.5119953155517578 23.2 -4.4271917343139648
		 24 -4.3898167610168457 24.8 -4.4159355163574219 25.6 -4.3843526840209961 26.4 -4.1336030960083017
		 27.2 -3.823512315750123 28 -3.8029944896697998 28.8 -4.0176262855529785 29.6 -4.1140413284301758
		 30.4 -4.0844254493713379 31.2 -4.0743989944458008 32 -4.1399960517883301 32.8 -4.1032223701477051
		 33.6 -4.0393190383911133 34.4 -4.0857367515563965 35.2 -4.2193355560302734 36 -4.2450714111328125
		 36.8 -4.3318967819213867 37.6 -4.3909096717834473 38.4 -4.3678512573242187 39.2 -4.2553167343139648
		 40 -4.227668285369873 40.8 -4.2295808792114258 41.6 -4.2070956230163574 42.4 -4.2184066772460938
		 43.2 -4.3441638946533203 44 -4.5059304237365723 44.8 -4.5850238800048828 45.6 -4.5878376960754395
		 46.4 -4.5796418190002441 47.2 -4.5830841064453125 48 -4.7343864440917969 48.8 -4.9198670387268066
		 49.6 -4.9312596321105957 50.4 -4.811403751373291 51.2 -4.7915959358215332 52 -4.8774924278259277
		 52.8 -5.0908126831054687 53.6 -5.3061280250549316 54.4 -5.4157934188842782 55.2 -5.4752159118652344
		 56 -5.5455942153930664 56.8 -5.6182947158813477 57.6 -5.6472272872924805 58.4 -5.656243324279786
		 59.2 -5.727386474609375 60 -5.8005514144897461 60.8 -5.8082561492919931 61.6 -5.8200583457946777
		 62.4 -5.9210906028747559 63.2 -5.9520998001098633 64 -5.9183859825134277 64.8 -5.9414172172546387
		 65.6 -6.0190081596374512 66.4 -6.0774202346801758 67.2 -6.0217404365539551 68 -5.9055995941162109
		 68.8 -5.9122934341430664 69.6 -5.9392862319946289 70.4 -5.9565801620483407 71.2 -6.0837311744689941
		 72 -6.2175207138061523 72.8 -6.2882814407348633 73.6 -6.3525400161743164 74.4 -6.4189839363098145
		 75.2 -6.4916572570800781 76 -6.5487031936645508 76.8 -6.5773353576660156 77.6 -6.5800399780273437
		 78.4 -6.5618171691894531 79.2 -6.5612707138061523 80 -6.5527462959289551 80.8 -6.5096616744995117
		 81.6 -6.4718499183654785 82.4 -6.4546651840209961 83.2 -6.4182190895080566 84 -6.3494253158569336
		 84.8 -6.2968602180480957 85.6 -6.2309079170227051 86.4 -6.1032652854919434 87.2 -5.9757866859436044
		 88 -5.8206324577331552 88.8 -5.7112674713134775 89.6 -5.6014652252197266 90.4 -5.5363597869873047
		 91.2 -5.4367485046386719 92 -5.3623814582824707 92.8 -5.284052848815918 93.6 -5.1282424926757804
		 94.4 -4.9575967788696289 95.2 -4.8620834350585938 96 -4.7929072380065918 96.8 -4.7208900451660156
		 97.6 -4.6635165214538574 98.4 -4.6355400085449219 99.2 -4.6375617980957031 100 -4.6271796226501465
		 100.8 -4.5599160194396973 101.6 -4.4094333648681641 102.4 -4.2405362129211426 103.2 -4.0749177932739258
		 104 -3.8700122833251953 104.8 -3.7470688819885254 105.6 -3.7661662101745614 106.4 -3.8188405036926265
		 107.2 -3.8072292804718018 108 -3.7154314517974862 108.8 -3.644397497177124 109.6 -3.522219181060791
		 110.4 -3.3710534572601318 111.2 -3.2535467147827148 112 -3.2046699523925781 112.8 -3.1260135173797607
		 113.6 -3.0244348049163814 114.4 -2.9503955841064453 115.2 -2.966733455657959 116 -2.9561877250671387
		 116.8 -2.8498551845550537 117.6 -2.7858700752258301 118.4 -2.7445611953735352 119.2 -2.734452486038208
		 120 -2.7610628604888916 120.8 -2.7763078212738037 121.6 -2.7893671989440918 122.4 -2.8510572910308838
		 123.2 -2.8869023323059082 124 -2.844773530960083 124.8 -2.8222887516021729 125.6 -2.9611599445343022
		 126.4 -3.118964672088623 127.2 -3.0623013973236084 128 -2.939166784286499 128.8 -2.9113543033599858
		 129.6 -2.944084644317627 130.4 -2.8983223438262939 131.2 -2.9098789691925049 132 -2.9384565353393555
		 132.8 -3.0036711692810059 133.6 -3.0238065719604492 134.4 -3.0026602745056152 135.2 -2.994218111038208
		 136 -3.0135066509246826 136.8 -3.0490508079528809 137.6 -3.0866715908050537 138.4 -3.0698692798614502
		 139.2 -3.054105281829834 140 -3.089594841003418 140.8 -3.1273248195648193 141.6 -3.1168882846832275
		 142.4 -3.1565308570861816 143.2 -3.2040688991546635 144 -3.2183849811553955 144.8 -3.1970748901367187
		 145.6 -3.2356517314910889 146.4 -3.2911946773529053 147.2 -3.316848993301392 148 -3.2482464313507085
		 148.8 -3.1801631450653076 149.6 -3.2417442798614506 150.4 -3.3162205219268799 151.2 -3.319990873336792
		 152 -3.2994182109832764 152.8 -3.2867140769958496 153.6 -3.3042540550231938 154.4 -3.3529942035675049
		 155.2 -3.4689435958862305 156 -3.5440211296081543 156.8 -3.5296776294708252 157.6 -3.5277924537658691
		 158.4 -3.6444520950317383 159.2 -3.7950987815856934 160 -3.8898744583129878 160.8 -3.9685308933258057
		 161.6 -4.0503292083740234 162.4 -4.1617431640625009 163.2 -4.2911891937255859 164 -4.4355521202087402
		 164.8 -4.5216398239135742 165.6 -4.6271252632141113 166.4 -4.7717885971069336 167.2 -4.879295825958252
		 168 -4.9575695991516113 168.8 -5.091851234436036 169.6 -5.212308406829834 170.4 -5.3123841285705566
		 171.2 -5.419590950012207 172 -5.5757017135620117 172.8 -5.6988363265991211 173.6 -5.7921366691589355
		 174.4 -5.8777599334716797 175.2 -5.9518537521362305 176 -6.0138721466064453 176.8 -6.0188989639282227
		 177.6 -5.9286584854125977 178.4 -5.7768096923828125 179.2 -5.7990217208862305 180 -5.9051899909973145
		 180.8 -5.898277759552002;
createNode animCurveTA -n "Bip01_R_Calf_rotateX17";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 227 ".ktv[0:226]"  0 -0.21420177817344663 0.8 -0.21388965845108032
		 1.6 -0.21468186378479004 2.4 -0.21603508293628693 3.2 -0.2172781378030777 4 -0.21831440925598145
		 4.8 -0.2191170156002045 5.6 -0.21990428864955905 6.4 -0.22093437612056732 7.2 -0.22253234684467316
		 8 -0.22372721135616305 8.8 -0.22386322915554047 9.6 -0.2245438396930694 10.4 -0.22530663013458249
		 11.2 -0.22461248934268954 12 -0.22478087246417999 12.8 -0.22636108100414273 13.6 -0.22644779086112973
		 14.4 -0.22523492574691775 15.2 -0.22399809956550598 16 -0.22336113452911377 16.8 -0.22493420541286469
		 17.6 -0.22760112583637243 18.4 -0.22823083400726316 19.2 -0.22676669061183932 20 -0.22530536353588104
		 20.8 -0.22432050108909607 21.6 -0.22279137372970581 22.4 -0.2208778262138367 23.2 -0.21981985867023468
		 24 -0.2202819287776947 24.8 -0.22210657596588135 25.6 -0.22270889580249784 26.4 -0.21781526505947113
		 27.2 -0.21054966747760773 28 -0.21069207787513733 28.8 -0.21737775206565857 29.6 -0.22131671011447904
		 30.4 -0.22134815156459808 31.2 -0.22157497704029083 32 -0.22271758317947388 32.8 -0.22202377021312711
		 33.6 -0.22043636441230777 34.4 -0.22251592576503751 35.2 -0.22595471143722534 36 -0.22716428339481351
		 36.8 -0.2291969358921051 37.6 -0.23169371485710141 38.4 -0.23078559339046475 39.2 -0.22818270325660706
		 40 -0.22749309241771698 40.8 -0.22791883349418637 41.6 -0.2271772027015686 42.4 -0.22706757485866547
		 43.2 -0.23067423701286319 44 -0.23550960421562195 44.8 -0.23783765733242032 45.6 -0.23837454617023468
		 46.4 -0.23833289742469788 47.2 -0.23909945785999298 48 -0.24350079894065857 48.8 -0.24863895773887634
		 49.6 -0.24857071042060852 50.4 -0.24518267810344696 51.2 -0.24374334514141083 52 -0.24587124586105344
		 52.8 -0.25105848908424377 53.6 -0.2563016414642334 54.4 -0.25898090004920959 55.2 -0.26069155335426331
		 56 -0.26262447237968445 56.8 -0.26353177428245544 57.6 -0.26323375105857849 58.4 -0.26374205946922302
		 59.2 -0.26514455676078796 60 -0.26704126596450806 60.8 -0.26761436462402344 61.6 -0.26878583431243896
		 62.4 -0.27155229449272156 63.2 -0.27343809604644775 64 -0.27419576048851013 64.8 -0.27637135982513428
		 65.6 -0.27973359823226929 66.4 -0.2822037935256958 67.2 -0.28200516104698181 68 -0.28103405237197876
		 68.8 -0.28242343664169312 69.6 -0.28388336300849915 70.4 -0.28491842746734625 71.2 -0.28831711411476135
		 72 -0.29177793860435486 72.8 -0.29335272312164307 73.6 -0.29494613409042358 74.4 -0.2968178391456604
		 75.2 -0.29852554202079773 76 -0.30025911331176758 76.8 -0.30164793133735657 77.6 -0.30257126688957214
		 78.4 -0.30225357413291931 79.2 -0.30258426070213318 80 -0.30357316136360168 80.8 -0.30379354953765869
		 81.6 -0.3039492666721344 82.4 -0.30464309453964233 83.2 -0.30479124188423157 84 -0.30463749170303345
		 84.8 -0.30490761995315552 85.6 -0.30492037534713745 86.4 -0.30383139848709106 87.2 -0.30200424790382385
		 88 -0.30069252848625183 88.8 -0.30053809285163879 89.6 -0.30097314715385437 90.4 -0.30109128355979919
		 91.2 -0.30043321847915649 92 -0.30012667179107672 92.8 -0.29959625005722046 93.6 -0.29692283272743225
		 94.4 -0.29402533173561096 95.2 -0.29326179623603821 96 -0.29339611530303955 96.8 -0.29350978136062622
		 97.6 -0.29383587837219238 98.4 -0.29432621598243713 99.2 -0.29549312591552734 100 -0.29676154255867004
		 100.8 -0.2964051365852356 101.6 -0.29446840286254883 102.4 -0.29219406843185425 103.2 -0.28960740566253662
		 104 -0.28648656606674194 104.8 -0.28477531671524048 105.6 -0.28665357828140259 106.4 -0.28941226005554199
		 107.2 -0.29042929410934448 108 -0.29078850150108337 108.8 -0.29087677597999573 109.6 -0.2895013689994812
		 110.4 -0.28707069158554077 111.2 -0.28538691997528076 112 -0.28488481044769293 112.8 -0.28397694230079651
		 113.6 -0.28199368715286255 114.4 -0.28096622228622437 115.2 -0.2819332480430603 116 -0.28187847137451172
		 116.8 -0.27937906980514526 117.6 -0.2771736085414887 118.4 -0.27580302953720093 119.2 -0.27477043867111212
		 120 -0.27463510632514954 120.8 -0.27418169379234314 121.6 -0.27382361888885498 122.4 -0.27497589588165283
		 123.2 -0.27570664882659912 124 -0.27417445182800293 124.8 -0.27326071262359619 125.6 -0.27634412050247192
		 126.4 -0.27949929237365723 127.2 -0.2774980366230011 128 -0.27384939789772034 128.8 -0.27304625511169434
		 129.6 -0.27323341369628906 130.4 -0.27237749099731445 131.2 -0.27199003100395203
		 132 -0.27320927381515503 132.8 -0.27481263875961304 133.6 -0.27498230338096619 134.4 -0.27381780743598938
		 135.2 -0.27281701564788818 136 -0.27266767621040344 136.8 -0.27351826429367065 137.6 -0.27396371960639954
		 138.4 -0.2730327844619751 139.2 -0.27230888605117798 140 -0.27235940098762512 140.8 -0.27164927124977112
		 141.6 -0.27043342590332031 142.4 -0.27054834365844727 143.2 -0.27107563614845276
		 144 -0.27029141783714294 144.8 -0.26928701996803284 145.6 -0.26904752850532532 146.4 -0.26881971955299377
		 147.2 -0.26721280813217163 148 -0.26341584324836731 148.8 -0.26020315289497375 149.6 -0.25983771681785589
		 150.4 -0.25985583662986755 151.2 -0.25812253355979919 152 -0.25570973753929138 152.8 -0.25351062417030334
		 153.6 -0.25209915637969971 154.4 -0.25138720870018011 155.2 -0.25166276097297668
		 156 -0.25097700953483582 156.8 -0.24835006892681125 157.6 -0.2459864467382431 158.4 -0.24570812284946444
		 159.2 -0.24662242829799655 160 -0.24705123901367188 160.8 -0.24667477607727051 161.6 -0.24599976837635043
		 162.4 -0.24595798552036283 163.2 -0.2472119331359863 164 -0.24819689989089969 164.8 -0.24756793677806857
		 165.6 -0.24717213213443753 166.4 -0.24827957153320313 167.2 -0.24910984933376312
		 168 -0.249031811952591 168.8 -0.24917787313461304 169.6 -0.24940378963947296 170.4 -0.24965895712375641
		 171.2 -0.25087624788284302 172 -0.25282752513885498 172.8 -0.25432154536247253 173.6 -0.2556011974811554
		 174.4 -0.25737056136131287 175.2 -0.25855392217636108 176 -0.25897908210754395 176.8 -0.25940951704978943
		 177.6 -0.25735467672348022 178.4 -0.25390523672103882 179.2 -0.25451916456222534
		 180 -0.25772389769554138 180.8 -0.25916591286659241;
createNode animCurveTA -n "Bip01_R_Calf_rotateY17";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 227 ".ktv[0:226]"  0 1.0025850534439087 0.8 1.0027064085006714
		 1.6 1.0024018287658691 2.4 1.001926064491272 3.2 1.0014854669570925 4 1.0011706352233889
		 4.8 1.0008926391601562 5.6 1.0006047487258911 6.4 1.0002309083938601 7.2 0.99966740608215332
		 8 0.99922084808349598 8.8 0.99917900562286399 9.6 0.99894148111343406 10.4 0.9987199306488036
		 11.2 0.99896943569183361 12 0.99885857105255127 12.8 0.99832344055175792 13.6 0.99827927350997936
		 14.4 0.99873387813568115 15.2 0.99912565946578979 16 0.99932312965393078 16.8 0.99882644414901733
		 17.6 0.99792206287384044 18.4 0.99767309427261364 19.2 0.99819517135620117 20 0.99874800443649281
		 20.8 0.99909132719039917 21.6 0.99962866306304943 22.4 1.0002464056015017 23.2 1.0006049871444704
		 24 1.0004441738128662 24.8 0.99985241889953613 25.6 0.99968683719634988 26.4 1.001348614692688
		 27.2 1.0037678480148315 28 1.003696084022522 28.8 1.0014796257019043 29.6 1.0001720190048218
		 30.4 1.000220775604248 31.2 1.000126838684082 32 0.99977451562881492 32.8 1.0000033378601074
		 33.6 1.0005499124526978 34.4 0.99982130527496305 35.2 0.99866962432861328 36 0.99824339151382446
		 36.8 0.99759989976882923 37.6 0.99667012691497792 38.4 0.99704641103744518 39.2 0.99795782566070557
		 40 0.99820339679718018 40.8 0.99802929162979126 41.6 0.99826145172119141 42.4 0.99831509590148959
		 43.2 0.99708557128906261 44 0.99542444944381714 44.8 0.99462211132049583 45.6 0.9944269061088562
		 46.4 0.99447476863861084 47.2 0.99421548843383778 48 0.99263721704483021 48.8 0.99079328775405884
		 49.6 0.99088418483734131 50.4 0.99211394786834717 51.2 0.99261713027954113 52 0.99177294969558716
		 52.8 0.98984283208847024 53.6 0.98790162801742576 54.4 0.98694723844528209 55.2 0.98635298013687134
		 56 0.98562228679656982 56.8 0.98527097702026367 57.6 0.98538619279861439 58.4 0.98520821332931519
		 59.2 0.98478198051452626 60 0.98412454128265381 60.8 0.9839066267013552 61.6 0.98341870307922352
		 62.4 0.98239660263061535 63.2 0.98169034719467163 64 0.98144221305847157 64.8 0.98063814640045166
		 65.6 0.97933626174926747 66.4 0.97837257385253917 67.2 0.97847652435302723 68 0.9788799285888673
		 68.8 0.97838717699050892 69.6 0.97781866788864125 70.4 0.977392017841339 71.2 0.97599536180496238
		 72 0.97454369068145741 72.8 0.97387129068374634 73.6 0.9731854796409608 74.4 0.97235023975372326
		 75.2 0.97161108255386364 76 0.97085028886795055 76.8 0.97026973962783802 77.6 0.96986544132232655
		 78.4 0.96999526023864757 79.2 0.96984100341796875 80 0.96940720081329323 80.8 0.96932214498519897
		 81.6 0.96923995018005371 82.4 0.96893268823623668 83.2 0.96890014410018943 84 0.96891224384307872
		 84.8 0.96875578165054332 85.6 0.96876335144042958 86.4 0.9692014455795287 87.2 0.97002822160720836
		 88 0.97058689594268788 88.8 0.97068166732788097 89.6 0.97041505575180054 90.4 0.97039276361465443
		 91.2 0.97066223621368397 92 0.97078198194503795 92.8 0.97099393606185924 93.6 0.97212302684783924
		 94.4 0.97338229417800892 95.2 0.97372537851333618 96 0.97368162870407104 96.8 0.97361427545547474
		 97.6 0.97346645593643188 98.4 0.97326922416687012 99.2 0.97278088331222534 100 0.97223353385925315
		 100.8 0.97240513563156128 101.6 0.97321075201034557 102.4 0.97414892911911011 103.2 0.97525864839553844
		 104 0.97653597593307473 104.8 0.97724348306655895 105.6 0.97646665573120128 106.4 0.97534447908401489
		 107.2 0.97494715452194203 108 0.97473931312561024 108.8 0.97469580173492432 109.6 0.97526854276657104
		 110.4 0.9762999415397644 111.2 0.97698527574539196 112 0.9771960973739624 112.8 0.97756499052047729
		 113.6 0.97843754291534413 114.4 0.97883886098861694 115.2 0.97840744256973233 116 0.97844994068145763
		 116.8 0.97947192192077637 117.6 0.98038685321807839 118.4 0.98091620206832875 119.2 0.98130720853805531
		 120 0.98137253522872925 120.8 0.9815446734428408 121.6 0.98169982433319092 122.4 0.98123914003372192
		 123.2 0.980951488018036 124 0.98156303167343151 124.8 0.9819486141204834 125.6 0.980704665184021
		 126.4 0.97946381568908691 127.2 0.98024940490722656 128 0.98171550035476685 128.8 0.98205053806304932
		 129.6 0.98202097415924039 130.4 0.9822961688041687 131.2 0.98247998952865601 132 0.98193728923797596
		 132.8 0.9813016653060912 133.6 0.98125159740447987 134.4 0.98172724246978771 135.2 0.98213827610015869
		 136 0.9822039008140564 136.8 0.98185598850250255 137.6 0.98168510198593129 138.4 0.98206013441085793
		 139.2 0.98231399059295643 140 0.98226547241210949 140.8 0.98260581493377674 141.6 0.98308163881301891
		 142.4 0.98302978277206421 143.2 0.98281294107437156 144 0.98316180706024181 144.8 0.9835059642791748
		 145.6 0.98362392187118541 146.4 0.98371195793151855 147.2 0.98436123132705677 148 0.98585474491119396
		 148.8 0.98709523677825906 149.6 0.98726940155029297 150.4 0.98724436759948719 151.2 0.98787218332290638
		 152 0.98877102136611938 152.8 0.98961091041564919 153.6 0.99016374349594127 154.4 0.99044340848922741
		 155.2 0.99035280942916892 156 0.99060189723968517 156.8 0.99157243967056241 157.6 0.99242472648620605
		 158.4 0.99253541231155396 159.2 0.99219065904617343 160 0.9920295476913451 160.8 0.99219805002212558
		 161.6 0.99245536327362061 162.4 0.99246966838836681 163.2 0.99198633432388283 164 0.99164140224456787
		 164.8 0.99187433719634988 165.6 0.99203783273696899 166.4 0.99163967370986927 167.2 0.9913368821144104
		 168 0.99137073755264271 168.8 0.99135303497314464 169.6 0.99125760793685913 170.4 0.99114078283309925
		 171.2 0.99064439535140991 172 0.98990792036056507 172.8 0.98935818672180176 173.6 0.98892790079116799
		 174.4 0.9882630109786984 175.2 0.98779511451721191 176 0.98767840862274159 176.8 0.98749309778213501
		 177.6 0.98830634355545044 178.4 0.98962134122848511 179.2 0.98938840627670299 180 0.98817509412765492
		 180.8 0.98758596181869507;
createNode animCurveTA -n "Bip01_R_Calf_rotateZ17";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 227 ".ktv[0:226]"  0 -16.627603530883789 0.8 -16.619956970214844
		 1.6 -16.636013031005859 2.4 -16.695821762084961 3.2 -16.753078460693359 4 -16.840883255004883
		 4.8 -16.88429069519043 5.6 -16.912397384643555 6.4 -16.954313278198242 7.2 -17.030311584472656
		 8 -17.07774543762207 8.8 -17.0911865234375 9.6 -17.130147933959961 10.4 -17.205293655395508
		 11.2 -17.171049118041992 12 -17.139791488647461 12.8 -17.242416381835938 13.6 -17.2366943359375
		 14.4 -17.190805435180664 15.2 -17.091297149658203 16 -17.040618896484375 16.8 -17.167976379394531
		 17.6 -17.342205047607422 18.4 -17.354524612426758 19.2 -17.276323318481445 20 -17.228460311889648
		 20.8 -17.171604156494141 21.6 -17.091964721679688 22.4 -16.94849967956543 23.2 -16.885339736938477
		 24 -16.909023284912109 24.8 -17.044328689575195 25.6 -17.109426498413086 26.4 -16.820947647094727
		 27.2 -16.386684417724609 28 -16.380035400390625 28.8 -16.779815673828125 29.6 -17.03569221496582
		 30.4 -17.07832145690918 31.2 -17.079727172851563 32 -17.185647964477543 32.8 -17.127176284790039
		 33.6 -17.041667938232422 34.4 -17.149785995483398 35.2 -17.38292121887207 36 -17.449028015136719
		 36.8 -17.617475509643555 37.6 -17.724452972412109 38.4 -17.715742111206055 39.2 -17.560707092285156
		 40 -17.52833366394043 40.8 -17.527261734008789 41.6 -17.46844482421875 42.4 -17.471124649047852
		 43.2 -17.711311340332031 44 -18.040760040283203 44.8 -18.204120635986328 45.6 -18.229215621948239
		 46.4 -18.253978729248047 47.2 -18.311500549316406 48 -18.590625762939457 48.8 -18.936132431030273
		 49.6 -18.985025405883789 50.4 -18.762439727783203 51.2 -18.658899307250977 52 -18.743448257446289
		 52.8 -19.042564392089844 53.6 -19.388235092163089 54.4 -19.606462478637695 55.2 -19.75853157043457
		 56 -19.88493537902832 56.8 -19.942867279052734 57.6 -19.924184799194336 58.4 -19.971319198608398
		 59.2 -20.145818710327148 60 -20.323480606079105 60.8 -20.365705490112305 61.6 -20.418584823608398
		 62.4 -20.640087127685547 63.2 -20.79205322265625 64 -20.878786087036136 64.8 -21.069299697875977
		 65.6 -21.321121215820313 66.4 -21.504522323608398 67.2 -21.512092590332031 68 -21.455049514770508
		 68.8 -21.596357345581055 69.6 -21.713336944580082 70.4 -21.775896072387695 71.2 -21.993444442749023
		 72 -22.2042236328125 72.8 -22.294404983520508 73.6 -22.388679504394531 74.4 -22.476486206054687
		 75.2 -22.571163177490231 76 -22.670431137084961 76.8 -22.767642974853516 77.6 -22.824855804443359
		 78.4 -22.797029495239254 79.2 -22.807027816772464 80 -22.869047164916992 80.8 -22.888833999633789
		 81.6 -22.887685775756836 82.4 -22.931791305541992 83.2 -22.958049774169918 84 -22.911582946777344
		 84.8 -22.895210266113281 85.6 -22.906217575073239 86.4 -22.814716339111328 87.2 -22.726531982421875
		 88 -22.633371353149418 88.8 -22.641311645507813 89.6 -22.615291595458984 90.4 -22.641410827636719
		 91.2 -22.590435028076172 92 -22.569921493530273 92.8 -22.518543243408203 93.6 -22.342586517333984
		 94.4 -22.177841186523437 95.2 -22.14979362487793 96 -22.170017242431641 96.8 -22.159992218017575
		 97.6 -22.170944213867188 98.4 -22.206926345825195 99.2 -22.288839340209961 100 -22.359121322631839
		 100.8 -22.358936309814453 101.6 -22.220922470092773 102.4 -22.061637878417969 103.2 -21.925182342529297
		 104 -21.715276718139652 104.8 -21.608861923217773 105.6 -21.725975036621097 106.4 -21.913597106933594
		 107.2 -22.000480651855469 108 -21.980409622192383 108.8 -21.972545623779297 109.6 -21.885763168334961
		 110.4 -21.749538421630859 111.2 -21.632574081420895 112 -21.612510681152344 112.8 -21.553630828857425
		 113.6 -21.471275329589847 114.4 -21.395610809326172 115.2 -21.429874420166016 116 -21.437562942504883
		 116.8 -21.28862190246582 117.6 -21.173778533935547 118.4 -21.068386077880859 119.2 -20.991851806640625
		 120 -20.984191894531257 120.8 -20.957159042358398 121.6 -20.936641693115231 122.4 -21.016885757446289
		 123.2 -21.057266235351563 124 -20.966815948486328 124.8 -20.92359733581543 125.6 -21.114767074584961
		 126.4 -21.327554702758789 127.2 -21.183761596679688 128 -20.962739944458008 128.8 -20.923065185546875
		 129.6 -20.967292785644531 130.4 -20.869087219238285 131.2 -20.868772506713867 132 -20.894546508789063
		 132.8 -20.996906280517575 133.6 -21.023550033569336 134.4 -20.956144332885746 135.2 -20.906869888305664
		 136 -20.906290054321293 136.8 -20.944704055786133 137.6 -20.980632781982425 138.4 -20.923616409301761
		 139.2 -20.854106903076172 140 -20.838844299316406 140.8 -20.839179992675781 141.6 -20.758106231689453
		 142.4 -20.762140274047852 143.2 -20.786586761474609 144 -20.76536750793457 144.8 -20.672632217407227
		 145.6 -20.664608001708984 146.4 -20.656850814819336 147.2 -20.573974609375 148 -20.350086212158203
		 148.8 -20.158292770385746 149.6 -20.159566879272461 150.4 -20.146392822265625 151.2 -20.007980346679688
		 152 -19.853630065917969 152.8 -19.733192443847656 153.6 -19.665409088134769 154.4 -19.623600006103516
		 155.2 -19.656105041503903 156 -19.607694625854492 156.8 -19.446971893310547 157.6 -19.287946701049805
		 158.4 -19.277778625488285 159.2 -19.323923110961911 160 -19.347511291503903 160.8 -19.351032257080082
		 161.6 -19.311588287353516 162.4 -19.309103012084961 163.2 -19.36787223815918 164 -19.444721221923828
		 164.8 -19.408132553100582 165.6 -19.392555236816406 166.4 -19.474851608276367 167.2 -19.528423309326172
		 168 -19.525178909301761 168.8 -19.565984725952148 169.6 -19.565019607543945 170.4 -19.56524658203125
		 171.2 -19.606525421142575 172 -19.718534469604492 172.8 -19.814983367919918 173.6 -19.937911987304688
		 174.4 -20.051885604858398 175.2 -20.113908767700195 176 -20.168258666992187 176.8 -20.178548812866211
		 177.6 -20.081878662109375 178.4 -19.877809524536136 179.2 -19.912311553955082 180 -20.109891891479492
		 180.8 -20.161678314208984;
createNode animCurveTA -n "Bip01_R_Foot_rotateX17";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 227 ".ktv[0:226]"  0 11.511316299438478 0.8 11.572379112243652
		 1.6 11.800886154174805 2.4 12.11981201171875 3.2 12.314223289489746 4 12.246156692504885
		 4.8 12.201790809631349 5.6 12.395042419433594 6.4 12.666712760925291 7.2 12.90819835662842
		 8 13.097640991210938 8.8 13.254761695861816 9.6 13.259807586669922 10.4 13.129422187805176
		 11.2 13.088238716125488 12 13.297840118408203 12.8 13.563368797302246 13.6 13.602577209472656
		 14.4 13.56729316711426 15.2 13.666172027587892 16 13.731331825256348 16.8 13.644618988037108
		 17.6 13.502142906188965 18.4 13.361331939697266 19.2 13.267855644226074 20 13.133605003356934
		 20.8 13.066061019897459 21.6 13.145574569702148 22.4 13.228410720825195 23.2 13.217373847961426
		 24 13.195541381835936 24.8 13.081209182739258 25.6 12.830044746398926 26.4 12.48727321624756
		 27.2 12.34175968170166 28 12.364806175231934 28.8 12.325722694396974 29.6 12.115718841552734
		 30.4 11.867532730102541 31.2 11.674562454223633 32 11.482335090637209 32.8 11.316344261169434
		 33.6 11.19991397857666 34.4 11.182303428649902 35.2 11.20970344543457 36 11.096806526184082
		 36.8 10.893975257873535 37.6 10.696437835693359 38.4 10.593698501586914 39.2 10.532207489013672
		 40 10.529120445251465 40.8 10.636187553405762 41.6 10.491137504577637 42.4 10.095795631408691
		 43.2 9.6775445938110352 44 9.5279855728149414 44.8 9.5614280700683594 45.6 9.5958948135375977
		 46.4 9.556396484375 47.2 9.5097684860229492 48 9.4575023651123047 48.8 9.3257207870483398
		 49.6 9.1578674316406232 50.4 9.1816749572753906 51.2 9.3134098052978516 52 9.4337282180786133
		 52.8 9.4352817535400391 53.6 9.3939504623413086 54.4 9.3530406951904297 55.2 9.480097770690918
		 56 9.6564073562622088 56.8 9.7986669540405273 57.6 9.9332046508789063 58.4 9.9478359222412109
		 59.2 9.5592269897460937 60 9.1378641128540039 60.8 9.2733640670776367 61.6 9.7895727157592773
		 62.4 10.036023139953612 63.2 9.9552097320556641 64 9.9185562133789063 64.8 10.046507835388184
		 65.6 10.135015487670898 66.4 10.213869094848636 67.2 10.510772705078123 68 10.805497169494627
		 68.8 10.849817276000977 69.6 10.70775318145752 70.4 10.514387130737305 71.2 10.474336624145508
		 72 10.531187057495115 72.8 10.623193740844728 73.6 10.720449447631838 74.4 10.704044342041016
		 75.2 10.604146957397459 76 10.44110107421875 76.8 10.400032043457031 77.6 10.44936466217041
		 78.4 10.435294151306154 79.2 10.300745010375977 80 10.118575096130373 80.8 9.9662113189697266
		 81.6 9.9915199279785156 82.4 10.152199745178224 83.2 10.263568878173828 84 10.316220283508301
		 84.8 10.337663650512695 85.6 10.313108444213867 86.4 10.207559585571287 87.2 10.036411285400392
		 88 9.8802127838134766 88.8 9.85748291015625 89.6 9.8473749160766602 90.4 9.8095235824584979
		 91.2 9.7686405181884766 92 9.8581571578979528 92.8 10.006997108459473 93.6 10.104287147521973
		 94.4 9.9720058441162127 95.2 9.8008279800415039 96 9.6475343704223633 96.8 9.6388931274414062
		 97.6 9.6746110916137695 98.4 9.7673225402832031 99.2 9.7080106735229492 100 9.5689277648925781
		 100.8 9.4550256729125977 101.6 9.5545568466186523 102.4 9.6881351470947266 103.2 9.6219263076782227
		 104 9.4392232894897479 104.8 9.3184595108032227 105.6 9.2405996322631836 106.4 9.2331933975219727
		 107.2 9.1732025146484375 108 9.1770448684692383 108.8 9.3004045486450195 109.6 9.2984418869018555
		 110.4 9.2670106887817383 111.2 9.2860536575317383 112 9.2950868606567383 112.8 9.2390604019165039
		 113.6 9.1924505233764648 114.4 9.1560916900634766 115.2 9.0862007141113281 116 9.0547151565551758
		 116.8 9.1159381866455078 117.6 9.235137939453125 118.4 9.3005084991455078 119.2 9.3520088195800781
		 120 9.3574447631835937 120.8 9.3593378067016602 121.6 9.2965593338012695 122.4 9.2453422546386719
		 123.2 9.2177858352661133 124 9.3030385971069336 124.8 9.3656044006347656 125.6 9.3841905593872088
		 126.4 9.3289241790771484 127.2 9.2543478012084961 128 9.1186304092407227 128.8 9.0105609893798846
		 129.6 9.0870800018310547 130.4 9.1957569122314471 131.2 9.2814531326293945 132 9.3138542175292969
		 132.8 9.2388448715209961 133.6 9.1168022155761719 134.4 8.9819116592407227 135.2 8.7874345779418945
		 136 8.6059780120849609 136.8 8.6384344100952166 137.6 8.7090110778808594 138.4 8.6829595565795898
		 139.2 8.7197818756103516 140 8.8242483139038086 140.8 8.8671054840087891 141.6 8.8079042434692383
		 142.4 8.828181266784668 143.2 8.819488525390625 144 8.7673311233520508 144.8 8.7252883911132812
		 145.6 8.7750940322875977 146.4 8.7657089233398437 147.2 8.6462841033935547 148 8.4996757507324219
		 148.8 8.4958667755126953 149.6 8.6677989959716797 150.4 8.7940387725830078 151.2 8.8192148208618164
		 152 8.9339933395385742 152.8 9.0900382995605469 153.6 9.2269821166992187 154.4 9.2557258605957031
		 155.2 9.2989377975463867 156 9.2850179672241211 156.8 9.3286981582641602 157.6 9.3365592956542969
		 158.4 9.3214607238769531 159.2 9.3830795288085937 160 9.3477239608764648 160.8 9.3000679016113281
		 161.6 9.3409366607666016 162.4 9.5072250366210937 163.2 9.6626405715942383 164 9.7356214523315465
		 164.8 9.6635341644287127 165.6 9.5876817703247088 166.4 9.5413007736206055 167.2 9.5413522720336914
		 168 9.5894870758056641 168.8 9.5157499313354492 169.6 9.4412078857421875 170.4 9.4924221038818359
		 171.2 9.73236083984375 172 9.9962711334228516 172.8 9.9754552841186523 173.6 9.7477340698242187
		 174.4 9.5313491821289062 175.2 9.4272336959838867 176 9.4015483856201172 176.8 9.2576541900634766
		 177.6 9.1191339492797852 178.4 9.0219221115112305 179.2 9.0732221603393555 180 9.2574052810668945
		 180.8 9.3640327453613281;
createNode animCurveTA -n "Bip01_R_Foot_rotateY17";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 227 ".ktv[0:226]"  0 -2.239001989364624 0.8 -2.2950253486633301
		 1.6 -2.3967571258544922 2.4 -2.5485119819641113 3.2 -2.6234133243560795 4 -2.6125996112823486
		 4.8 -2.5801117420196533 5.6 -2.6816897392272949 6.4 -2.8120059967041016 7.2 -2.9116067886352539
		 8 -2.9785265922546387 8.8 -3.0742332935333252 9.6 -3.0830931663513184 10.4 -3.0191411972045898
		 11.2 -2.9963822364807129 12 -3.1172506809234619 12.8 -3.2414019107818604 13.6 -3.257378101348877
		 14.4 -3.2311286926269531 15.2 -3.2644889354705811 16 -3.2902610301971436 16.8 -3.2790877819061279
		 17.6 -3.2203071117401123 18.4 -3.1345353126525879 19.2 -3.0906991958618164 20 -3.0469908714294438
		 20.8 -3.0217459201812744 21.6 -3.0502796173095703 22.4 -3.0929498672485352 23.2 -3.0843813419342041
		 24 -3.0602462291717529 24.8 -3.0363776683807377 25.6 -2.8832440376281734 26.4 -2.7281033992767334
		 27.2 -2.6474785804748535 28 -2.671229362487793 28.8 -2.6535594463348389 29.6 -2.5596730709075928
		 30.4 -2.4252123832702637 31.2 -2.3039600849151611 32 -2.2249007225036621 32.8 -2.1616446971893311
		 33.6 -2.0761969089508057 34.4 -2.09049391746521 35.2 -2.0967843532562256 36 -2.053361177444458
		 36.8 -1.9333248138427732 37.6 -1.8501216173171997 38.4 -1.8219714164733889 39.2 -1.7789986133575439
		 40 -1.7729995250701904 40.8 -1.8233803510665894 41.6 -1.7338927984237671 42.4 -1.5594618320465088
		 43.2 -1.3540226221084597 44 -1.2821683883666992 44.8 -1.2898622751235962 45.6 -1.318540096282959
		 46.4 -1.2684986591339111 47.2 -1.2540181875228884 48 -1.2265366315841677 48.8 -1.2053316831588743
		 49.6 -1.0929083824157717 50.4 -1.1039236783981323 51.2 -1.1613082885742187 52 -1.2314877510070801
		 52.8 -1.2180036306381226 53.6 -1.188634991645813 54.4 -1.1781742572784424 55.2 -1.2687684297561646
		 56 -1.3347012996673584 56.8 -1.399962902069092 57.6 -1.4724279642105105 58.4 -1.4388843774795532
		 59.2 -1.269532322883606 60 -1.0603193044662476 60.8 -1.1261454820632937 61.6 -1.3568164110183716
		 62.4 -1.5052641630172727 63.2 -1.4731719493865969 64 -1.4579739570617676 64.8 -1.5352936983108521
		 65.6 -1.574225902557373 66.4 -1.6112889051437378 67.2 -1.7454253435134888 68 -1.9165987968444826
		 68.8 -1.9445003271102907 69.6 -1.8548015356063847 70.4 -1.8082122802734373 71.2 -1.7525920867919922
		 72 -1.7366348505020142 72.8 -1.8119579553604128 73.6 -1.8739343881607058 74.4 -1.8505017757415771
		 75.2 -1.7740722894668579 76 -1.6960028409957886 76.8 -1.6400632858276367 77.6 -1.7157806158065796
		 78.4 -1.6889753341674805 79.2 -1.6177686452865601 80 -1.5278444290161133 80.8 -1.4379357099533081
		 81.6 -1.4316219091415403 82.4 -1.5513777732849121 83.2 -1.6145116090774536 84 -1.6252727508544922
		 84.8 -1.6621431112289429 85.6 -1.636305570602417 86.4 -1.571750283241272 87.2 -1.4754087924957275
		 88 -1.4011167287826538 88.8 -1.4030131101608276 89.6 -1.3812409639358521 90.4 -1.3828352689743042
		 91.2 -1.3591674566268921 92 -1.4177471399307251 92.8 -1.4859074354171755 93.6 -1.5369424819946291
		 94.4 -1.4723039865493774 95.2 -1.3652253150939939 96 -1.3120883703231812 96.8 -1.2919180393218994
		 97.6 -1.3318382501602173 98.4 -1.3624728918075562 99.2 -1.3342368602752688 100 -1.2681792974472046
		 100.8 -1.1985114812850952 101.6 -1.2687404155731201 102.4 -1.3343433141708374 103.2 -1.289385199546814
		 104 -1.1846311092376709 104.8 -1.1274327039718628 105.6 -1.1031125783920288 106.4 -1.0498777627944946
		 107.2 -1.0391368865966797 108 -1.0551638603210447 108.8 -1.1003715991973877 109.6 -1.1244632005691528
		 110.4 -1.1041661500930786 111.2 -1.0798875093460083 112 -1.0858656167984009 112.8 -1.0552723407745359
		 113.6 -1.0574239492416382 114.4 -1.0215953588485718 115.2 -0.99032014608383179 116 -0.98041588068008434
		 116.8 -1.0203074216842651 117.6 -1.0713218450546265 118.4 -1.1199078559875488 119.2 -1.105054497718811
		 120 -1.1329802274703979 120.8 -1.1329845190048218 121.6 -1.0973318815231323 122.4 -1.081978440284729
		 123.2 -1.0734374523162842 124 -1.0962837934494021 124.8 -1.1408894062042236 125.6 -1.1380822658538818
		 126.4 -1.1297421455383301 127.2 -1.0895392894744873 128 -1.0096460580825806 128.8 -0.96729314327239979
		 129.6 -0.99499130249023438 130.4 -1.0405896902084353 131.2 -1.083850622177124 132 -1.0755584239959717
		 132.8 -1.0625886917114258 133.6 -1.0115503072738647 134.4 -0.95317399501800548 135.2 -0.84652286767959595
		 136 -0.75499242544174194 136.8 -0.77774930000305187 137.6 -0.7960699200630188 138.4 -0.76542335748672485
		 139.2 -0.81697303056716908 140 -0.85269153118133556 140.8 -0.89771264791488659 141.6 -0.87438714504241932
		 142.4 -0.86734455823898327 143.2 -0.87431234121322621 144 -0.83968341350555431 144.8 -0.805897057056427
		 145.6 -0.80393677949905396 146.4 -0.84596794843673717 147.2 -0.76579809188842773
		 148 -0.6927940845489502 148.8 -0.69406771659851074 149.6 -0.7948697805404662 150.4 -0.85654157400131237
		 151.2 -0.86843425035476685 152 -0.91950446367263794 152.8 -0.99561357498168945 153.6 -1.0470542907714844
		 154.4 -1.0990163087844849 155.2 -1.0815081596374512 156 -1.1150888204574585 156.8 -1.1364821195602417
		 157.6 -1.144406795501709 158.4 -1.1293476819992063 159.2 -1.1563676595687866 160 -1.1519858837127686
		 160.8 -1.1056777238845823 161.6 -1.1456553936004641 162.4 -1.2126230001449585 163.2 -1.2958569526672363
		 164 -1.311336874961853 164.8 -1.2985849380493164 165.6 -1.257542610168457 166.4 -1.2421222925186155
		 167.2 -1.2452784776687622 168 -1.2477089166641235 168.8 -1.2153055667877195 169.6 -1.1762583255767822
		 170.4 -1.2118957042694092 171.2 -1.338157653808594 172 -1.4660819768905642 172.8 -1.4531043767929075
		 173.6 -1.3595477342605593 174.4 -1.2560328245162964 175.2 -1.1631195545196531 176 -1.149243950843811
		 176.8 -1.1052626371383667 177.6 -1.0205808877944946 178.4 -0.98769170045852639 179.2 -1.0058587789535522
		 180 -1.0861698389053345 180.8 -1.1516637802124023;
createNode animCurveTA -n "Bip01_R_Foot_rotateZ17";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 227 ".ktv[0:226]"  0 -1.2249692678451538 0.8 -1.2938683032989502
		 1.6 -1.38512122631073 2.4 -1.4677708148956301 3.2 -1.5090404748916626 4 -1.4700822830200195
		 4.8 -1.41942298412323 5.6 -1.4734296798706059 6.4 -1.5989418029785156 7.2 -1.7222703695297239
		 8 -1.8354142904281616 8.8 -1.8844535350799561 9.6 -1.8250128030776978 10.4 -1.7318334579467771
		 11.2 -1.6949949264526367 12 -1.690618634223938 12.8 -1.7109930515289309 13.6 -1.7681467533111572
		 14.4 -1.8132089376449585 15.2 -1.7821580171585083 16 -1.6947534084320068 16.8 -1.6039705276489258
		 17.6 -1.5646305084228516 18.4 -1.6220018863677981 19.2 -1.6289716958999634 20 -1.4743449687957764
		 20.8 -1.2850717306137085 21.6 -1.2535285949707031 22.4 -1.3759766817092896 23.2 -1.4705733060836792
		 24 -1.4495552778244021 24.8 -1.3583618402481079 25.6 -1.2304565906524658 26.4 -1.1953239440917969
		 27.2 -1.3000859022140503 28 -1.3692468404769895 28.8 -1.3214759826660156 29.6 -1.2123539447784424
		 30.4 -1.1416351795196531 31.2 -1.1670066118240356 32 -1.232333779335022 32.8 -1.3006157875061035
		 33.6 -1.3283076286315918 34.4 -1.2874816656112673 35.2 -1.1907905340194702 36 -1.1229338645935061
		 36.8 -1.0725802183151243 37.6 -0.97784864902496338 38.4 -0.89272230863571167 39.2 -0.92079442739486694
		 40 -0.96907538175582875 40.8 -1.014407753944397 41.6 -1.168738842010498 42.4 -1.3451788425445557
		 43.2 -1.2913379669189451 44 -1.0550183057785034 44.8 -0.8565714955329895 45.6 -0.71924704313278198
		 46.4 -0.57200127840042114 47.2 -0.46877175569534302 48 -0.41243350505828863 48.8 -0.3238804042339325
		 49.6 -0.22701768577098844 50.4 -0.26427140831947327 51.2 -0.51652497053146362 52 -0.90963822603225708
		 52.8 -1.1476417779922483 53.6 -1.0041543245315552 54.4 -0.60407894849777222 55.2 -0.2138947993516922
		 56 -0.066873662173748016 56.8 -0.24431061744689941 57.6 -0.43718132376670837 58.4 -0.3394768238067627
		 59.2 -0.076056912541389465 60 0.0916547030210495 60.8 0.13297194242477417 61.6 0.16612939536571505
		 62.4 0.28918546438217163 63.2 0.50640559196472168 64 0.88071584701538075 64.8 1.3654681444168093
		 65.6 1.7379739284515381 66.4 1.9136556386947632 67.2 2.0642154216766357 68 2.2758777141571045
		 68.8 2.4266095161437988 69.6 2.4864799976348877 70.4 2.4711856842041016 71.2 2.415712833404541
		 72 2.3653721809387207 72.8 2.2984452247619629 73.6 2.1743018627166748 74.4 1.9838430881500253
		 75.2 1.7626761198043823 76 1.6702049970626833 76.8 1.7094453573226929 77.6 1.6977006196975708
		 78.4 1.5752263069152832 79.2 1.4977235794067385 80 1.5521966218948366 80.8 1.6241996288299561
		 81.6 1.6666133403778076 82.4 1.7618651390075684 83.2 1.7746109962463379 84 1.6170340776443479
		 84.8 1.481207013130188 85.6 1.4872419834136963 86.4 1.4928528070449829 87.2 1.447983980178833
		 88 1.4129093885421753 88.8 1.4493080377578735 89.6 1.4915322065353394 90.4 1.5124191045761108
		 91.2 1.5155715942382813 92 1.4936058521270752 92.8 1.439234733581543 93.6 1.3566229343414309
		 94.4 1.373369574546814 95.2 1.5319663286209106 96 1.6671884059906006 96.8 1.668169379234314
		 97.6 1.6410163640975952 98.4 1.6380865573883057 99.2 1.6704413890838623 100 1.7085193395614624
		 100.8 1.6974073648452761 101.6 1.5980829000473022 102.4 1.4972044229507446 103.2 1.4438225030899048
		 104 1.3862286806106567 104.8 1.2848904132843018 105.6 1.235434889793396 106.4 1.2896177768707275
		 107.2 1.2927958965301514 108 1.1586329936981199 108.8 1.0366050004959106 109.6 1.020175576210022
		 110.4 1.053969144821167 111.2 1.0046961307525637 112 0.89199167490005493 112.8 0.88402789831161499
		 113.6 0.91506409645080544 114.4 0.82600373029708862 115.2 0.72133892774581909 116 0.82753801345825195
		 116.8 1.0113788843154907 117.6 1.00359046459198 118.4 0.82983869314193726 119.2 0.68542885780334473
		 120 0.61187940835952759 120.8 0.59147942066192627 121.6 0.58206599950790405 122.4 0.57982385158538818
		 123.2 0.59228932857513428 124 0.64016014337539673 124.8 0.73939293622970581 125.6 0.82607394456863403
		 126.4 0.80350887775421131 127.2 0.70120781660079956 128 0.65094894170761108 128.8 0.64494806528091431
		 129.6 0.5975877046585083 130.4 0.52814513444900513 131.2 0.47062098979949951 132 0.44990822672843944
		 132.8 0.5038301944732666 133.6 0.58328324556350708 134.4 0.56480515003204346 135.2 0.49490591883659363
		 136 0.46626940369606024 136.8 0.46362388134002691 137.6 0.44034275412559509 138.4 0.42182025313377386
		 139.2 0.435057133436203 140 0.50353360176086426 140.8 0.64603757858276367 141.6 0.74708306789398193
		 142.4 0.70856714248657227 143.2 0.64245569705963146 144 0.61468654870986938 144.8 0.59955799579620361
		 145.6 0.63842988014221191 146.4 0.68924933671951294 147.2 0.66685193777084351 148 0.69553440809249878
		 148.8 0.78929680585861195 149.6 0.80001318454742432 150.4 0.69586265087127697 151.2 0.55023640394210815
		 152 0.50672346353530884 152.8 0.60743653774261475 153.6 0.70054721832275391 154.4 0.732832431793213
		 155.2 0.74539262056350708 156 0.71365123987197887 156.8 0.64978981018066406 157.6 0.57710838317871094
		 158.4 0.46612432599067694 159.2 0.40554359555244446 160 0.48816502094268799 160.8 0.58756434917449951
		 161.6 0.55377012491226207 162.4 0.47070923447608942 163.2 0.46033358573913574 164 0.51462990045547485
		 164.8 0.54212474822998047 165.6 0.56114107370376587 166.4 0.67944228649139404 167.2 0.89005225896835327
		 168 1.0410367250442505 168.8 1.0454568862915039 169.6 0.90522754192352284 170.4 0.77073037624359131
		 171.2 0.71852362155914307 172 0.72664886713027954 172.8 0.82321596145629883 173.6 0.98929840326309193
		 174.4 1.0643996000289917 175.2 1.0189931392669678 176 0.95785886049270619 176.8 1.0284467935562134
		 177.6 1.2350902557373049 178.4 1.3346450328826904 179.2 1.2619285583496094 180 1.217848539352417
		 180.8 1.249337911605835;
createNode animCurveTA -n "Bip01_R_Toe0_rotateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 227 ".ktv[0:226]"  0 6.6674324443738442e-006 0.8 2.1035177724115783e-006
		 1.6 -9.1562873194561657e-007 2.4 5.2471261824393878e-007 3.2 2.956001026177546e-006
		 4 9.7604049642541213e-007 4.8 4.4739595068676863e-006 5.6 4.1300768316432368e-006
		 6.4 7.5599541560222869e-006 7.2 3.9798678699298762e-006 8 2.9022969556535827e-006
		 8.8 -2.4645494249853077e-006 9.6 -8.9167286887459341e-007 10.4 -1.7668777445578598e-006
		 11.2 1.0833479109351174e-006 12 3.0615678952017338e-006 12.8 5.132535079610534e-006
		 13.6 -2.2217413970793132e-006 14.4 2.8690251383522995e-006 15.2 2.175720965169603e-006
		 16 -1.7929204432221013e-006 16.8 7.043960522423732e-006 17.6 6.1177038332971279e-006
		 18.4 5.9049348237749655e-006 19.2 3.7515101780627447e-007 20 3.6939804886060305e-006
		 20.8 1.2391324162308592e-006 21.6 1.8106641164195025e-006 22.4 -7.6687946659603754e-008
		 23.2 4.4711891860060859e-006 24 8.545437708562531e-007 24.8 -1.743654252095439e-006
		 25.6 -3.1667494226894632e-007 26.4 3.1781307825440308e-006 27.2 -1.5769800256748567e-006
		 28 2.8433164516172837e-006 28.8 -3.5430457501206547e-006 29.6 -6.5159865698660724e-006
		 30.4 3.2686368740542093e-006 31.2 -4.4992066250415519e-006 32 1.7918716821441194e-006
		 32.8 5.5294240155490115e-006 33.6 3.9429091884812806e-006 34.4 2.2144538434076821e-006
		 35.2 4.7342164180008695e-006 36 -1.9016123360415804e-006 36.8 4.0111749513016548e-006
		 37.6 4.1633943510532845e-006 38.4 1.2596613032656023e-006 39.2 4.8579595386399888e-006
		 40 3.7427400911838049e-006 40.8 3.7871022868785072e-006 41.6 3.4576712550915545e-006
		 42.4 3.6433268633118124e-006 43.2 9.0805988293141127e-006 44 2.9194516173447478e-006
		 44.8 1.2833944538215292e-006 45.6 -4.7708709871585597e-007 46.4 2.1329167338990378e-006
		 47.2 4.5910765038570389e-006 48 2.6065742986247642e-006 48.8 7.8015079907345363e-007
		 49.6 4.205327968520578e-006 50.4 1.9467445326881716e-006 51.2 -1.2813236480724299e-006
		 52 4.6118175305309705e-006 52.8 2.2562526282854374e-006 53.6 -1.2065230521329795e-006
		 54.4 -6.4378614297311287e-006 55.2 -1.2738446457660757e-006 56 -3.8467931062768912e-007
		 56.8 5.2409818636078853e-006 57.6 2.3363190848613158e-006 58.4 3.6475144042924512e-006
		 59.2 6.0331499298627031e-006 60 1.2661204209507559e-006 60.8 1.3530782325688051e-006
		 61.6 2.2886877104610903e-006 62.4 2.4106336695695063e-006 63.2 -1.7872239368443843e-006
		 64 -3.8922385670048243e-007 64.8 1.6200111758735147e-006 65.6 -7.3985816015920128e-006
		 66.4 3.96672703573131e-006 67.2 1.6773307152107009e-006 68 -1.2808391147700604e-006
		 68.8 -5.1699504410862573e-007 69.6 -4.4341086322674528e-006 70.4 -4.0701088437344879e-006
		 71.2 7.616245056851767e-007 72 1.4796547702644602e-006 72.8 -8.1041866906161886e-007
		 73.6 -3.0010880891495617e-006 74.4 -1.0904074088102789e-006 75.2 -4.4938051360077225e-006
		 76 -3.8957823562668653e-006 76.8 8.1318743468727916e-005 77.6 3.6661160265794031e-005
		 78.4 9.2960857728030533e-005 79.2 1.3886913620808627e-005 80 1.0214514986728318e-005
		 80.8 -3.9765623114362825e-006 81.6 -2.6080379029735923e-006 82.4 2.5040658329089638e-006
		 83.2 -1.4345434465212747e-005 84 -4.5647349907085299e-005 84.8 -6.0212078096810728e-006
		 85.6 0.00013300703722052276 86.4 -2.3918733859318312e-005 87.2 9.1347061470514714e-007
		 88 1.6797481293906458e-005 88.8 2.5295755676779663e-006 89.6 2.9682314561796375e-006
		 90.4 4.4130874812253751e-006 91.2 -4.2087049223482609e-006 92 3.4063773455272894e-006
		 92.8 6.8930467023164974e-006 93.6 -4.3185559661651496e-006 94.4 4.6777017814747524e-006
		 95.2 3.901925083482638e-006 96 5.0934527280332986e-006 96.8 4.9574814511288423e-006
		 97.6 -1.0287906661687886e-005 98.4 -3.7254685594234616e-005 99.2 -8.3851391536882147e-006
		 100 5.7949628171627418e-006 100.8 -2.7634853267954895e-006 101.6 6.521314389829059e-006
		 102.4 -3.3994770092249387e-006 103.2 6.64588469589944e-006 104 -1.7708567838781164e-006
		 104.8 2.5141009700746513e-006 105.6 5.0763619583449326e-006 106.4 9.7270515198033536e-007
		 107.2 1.5989911844371818e-005 108 1.3633582057082096e-005 108.8 5.4753099902882241e-006
		 109.6 3.2756845484982482e-006 110.4 -8.0913614510791376e-006 111.2 -8.1915072769334074e-007
		 112 1.7164295059046708e-005 112.8 -9.0631729108281434e-006 113.6 -3.6091609217692167e-005
		 114.4 4.4083571992814541e-006 115.2 5.1351030379009899e-006 116 7.8701505117351172e-006
		 116.8 -6.3608831624151208e-007 117.6 8.3353165791777428e-007 118.4 -5.8037076087202877e-006
		 119.2 2.6091672680195188e-006 120 2.2466058453574078e-006 120.8 -4.9952734570979384e-007
		 121.6 3.3792728117987281e-006 122.4 -4.8986663614414283e-007 123.2 7.7027976885801763e-007
		 124 -4.1486091504339129e-006 124.8 3.7915840493951687e-006 125.6 -1.7490464188085753e-006
		 126.4 5.3853777899348643e-006 127.2 -9.1235267518641205e-007 128 1.0772608050046983e-007
		 128.8 3.0681949283462018e-006 129.6 5.7392694543523248e-006 130.4 4.8778138079796918e-006
		 131.2 3.6849983189313207e-006 132 2.5011404432007112e-005 132.8 9.5885501650627702e-005
		 133.6 9.1326051915530115e-005 134.4 4.7417011955985799e-005 135.2 1.9279863408883106e-005
		 136 1.5882211300777271e-005 136.8 -5.6164381021517329e-006 137.6 7.2987800194823649e-006
		 138.4 1.2403961591189727e-006 139.2 -1.6385530443585594e-006 140 5.1755509957729373e-006
		 140.8 -3.1719400794827379e-006 141.6 2.6301909201720268e-006 142.4 -3.7726161394857622e-007
		 143.2 1.3343981208890909e-006 144 1.579304012011562e-006 144.8 -4.8076444727485068e-006
		 145.6 5.2749392125406303e-006 146.4 -3.4039733236568281e-006 147.2 1.4437417803492281e-006
		 148 2.4304647467943141e-006 148.8 -1.4935807257643321e-006 149.6 -5.9340152347431285e-007
		 150.4 3.592967232179944e-006 151.2 1.8454715018378922e-006 152 6.5756586309362319e-007
		 152.8 2.6731038360594539e-006 153.6 -2.3397640234179562e-006 154.4 4.2892811507044826e-006
		 155.2 2.5735182589414758e-006 156 -7.5947255595565366e-008 156.8 1.2208312227812714e-006
		 157.6 7.1801000558480155e-006 158.4 -1.3847878399531055e-008 159.2 2.3511954623245401e-006
		 160 -5.7126311503452598e-007 160.8 2.6295554107491625e-006 161.6 1.1151742000947706e-006
		 162.4 2.3770820689605893e-007 163.2 2.8318745535216294e-006 164 -2.0452018816285999e-006
		 164.8 4.294403481708285e-007 165.6 1.4931373470972176e-006 166.4 -6.2331656636160915e-007
		 167.2 1.6055732885433829e-006 168 3.817841388809029e-006 168.8 1.1020781585102668e-006
		 169.6 3.9571477827848858e-006 170.4 1.9807432636298472e-006 171.2 5.9853169886991964e-007
		 172 5.097398343423265e-007 172.8 2.534276973165106e-006 173.6 5.0091348384739831e-006
		 174.4 9.4965554353621005e-007 175.2 -4.9013328862201888e-007 176 -7.1171052695717663e-007
		 176.8 4.7086791710171383e-006 177.6 -2.6221368898404762e-006 178.4 1.5038039009596105e-006
		 179.2 3.8041171137592751e-006 180 3.9031056076055393e-006 180.8 3.8045554902055305e-006;
createNode animCurveTA -n "Bip01_R_Toe0_rotateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 227 ".ktv[0:226]"  0 -1.144786801887676e-005 0.8 -1.3217643754614984e-005
		 1.6 -5.3572271099255886e-006 2.4 -7.8860248322598636e-006 3.2 -5.8194987104798201e-006
		 4 -8.909321877581533e-006 4.8 -1.9955593870690791e-006 5.6 6.8861618274240755e-007
		 6.4 -8.0171321314992383e-006 7.2 -3.4649924600671511e-006 8 -4.7630760491301771e-006
		 8.8 -8.330208061124722e-007 9.6 -8.7020744103938341e-006 10.4 -2.4125952222675551e-006
		 11.2 -5.2775862968701404e-006 12 -5.3535363804257941e-006 12.8 2.6550649181444896e-006
		 13.6 -5.1562160479079466e-006 14.4 -8.8842543846112676e-006 15.2 -8.4412522483034991e-006
		 16 -1.0931415999948513e-005 16.8 -1.0980336810462177e-005 17.6 -3.5639798170450381e-006
		 18.4 -5.4532915783056524e-006 19.2 -2.7307648906571562e-006 20 -7.5033412940683775e-006
		 20.8 -1.1037802323698996e-005 21.6 -6.7498663156584371e-006 22.4 -6.8659096541523468e-006
		 23.2 -8.7885418906807899e-006 24 -2.5437752810830716e-006 24.8 -8.7175048975041136e-006
		 25.6 -5.1237861953268293e-006 26.4 -9.2521950136870146e-006 27.2 -5.8783430176845286e-006
		 28 -5.9854828577954322e-006 28.8 -5.8389800869917963e-006 29.6 1.1247237097222751e-007
		 30.4 -5.3194580686977133e-006 31.2 -2.8417700832505943e-006 32 -1.3912770555180032e-005
		 32.8 -5.5031046031217548e-006 33.6 -6.2487547438649935e-006 34.4 7.5795645670950773e-007
		 35.2 -8.7866465037222952e-006 36 -6.2602234720543493e-006 36.8 4.7464973249589093e-006
		 37.6 -2.5310002911282936e-006 38.4 2.4361375494663662e-007 39.2 -4.4538764996104874e-006
		 40 -8.9621007646201178e-006 40.8 -5.5852619880170096e-006 41.6 -2.3478592083847616e-006
		 42.4 -3.5161010600859299e-006 43.2 -1.6741073523007799e-006 44 -8.2491797002148814e-006
		 44.8 -8.3424338299664669e-006 45.6 -9.8808695838670246e-006 46.4 -1.2380917269183556e-007
		 47.2 -7.4683134698716466e-006 48 -6.5958151935774376e-006 48.8 -4.1958164729294367e-006
		 49.6 -1.6183589650609067e-006 50.4 -2.177747774112504e-006 51.2 -9.5121558842947707e-006
		 52 5.6552944442955777e-006 52.8 -2.3080099254002562e-006 53.6 3.4078145745297665e-006
		 54.4 9.0173358557876782e-007 55.2 -4.2453748392290436e-006 56 -7.068938430165872e-006
		 56.8 -5.1968436309834942e-006 57.6 -1.1228225957893301e-005 58.4 -6.0499637584143784e-006
		 59.2 1.7452028941988829e-006 60 -9.8220507425139658e-006 60.8 -7.9202673077816144e-006
		 61.6 -3.9379551708407229e-007 62.4 -3.0974827041063691e-006 63.2 -6.8333633862494017e-006
		 64 3.9908016447043337e-007 64.8 -7.0678438532922874e-006 65.6 -4.7321086640295107e-006
		 66.4 -5.3959815886628348e-006 67.2 -1.4097805433266331e-005 68 -1.6271527783828788e-005
		 68.8 -1.0005061085394118e-005 69.6 -3.6633393847296251e-006 70.4 -5.9765052355942316e-006
		 71.2 -8.2723172454279847e-006 72 -7.5551370173343457e-006 72.8 -1.5720979718025776e-006
		 73.6 -9.2140035121701658e-006 74.4 -8.2392898548278026e-006 75.2 -2.9501154585886984e-006
		 76 -1.0869775906030554e-005 76.8 -6.2020503719395492e-006 77.6 -8.1107082223752514e-006
		 78.4 -1.15184238893562e-005 79.2 -3.984958766523051e-006 80 8.8536967268737509e-007
		 80.8 -1.0566066521278117e-005 81.6 -8.793143933871761e-006 82.4 6.0092041564985266e-008
		 83.2 -6.3134662013908391e-006 84 -1.1599716344790068e-005 84.8 5.8100971500607557e-007
		 85.6 2.4236503577412805e-006 86.4 -6.5143426581926187e-006 87.2 -1.0237478818453386e-005
		 88 4.4439557314035483e-006 88.8 -1.0697607649490236e-005 89.6 -1.7802179286263708e-007
		 90.4 -4.3567588363657705e-006 91.2 -2.8003553325106623e-006 92 -5.0873818508989643e-006
		 92.8 -1.1417821042414289e-005 93.6 -3.2959299005597131e-006 94.4 -3.486040895950282e-006
		 95.2 -9.8965974757447839e-006 96 -6.2295707721204963e-006 96.8 -8.568541488784831e-006
		 97.6 -6.1162231759226415e-006 98.4 -5.5075574891816359e-006 99.2 -8.3625918705365621e-006
		 100 -1.0734594070527237e-005 100.8 -5.8747418734128587e-006 101.6 -8.6003337855800055e-006
		 102.4 -7.203536370070652e-006 103.2 -2.5505371468170779e-006 104 -1.0570417998678747e-006
		 104.8 -1.2562263691506814e-005 105.6 -6.7422956817608792e-006 106.4 -9.5380919447052293e-006
		 107.2 -5.1745346354437061e-006 108 -1.4396009646588936e-006 108.8 -3.3213518690899946e-006
		 109.6 -1.8906011973740535e-006 110.4 -2.6349414383730613e-007 111.2 -2.2388691434116481e-007
		 112 -6.2180561144487001e-006 112.8 -3.851478140859399e-006 113.6 -2.263921942358138e-006
		 114.4 -1.6853203987921006e-006 115.2 -1.6226706520683365e-006 116 -7.2083289524016445e-006
		 116.8 -6.6332086134934798e-006 117.6 -8.4268585851532407e-006 118.4 -5.3688881962443702e-006
		 119.2 -1.6428804883616976e-005 120 -7.1923636824067216e-006 120.8 -5.5038012760633137e-006
		 121.6 -4.1280136429122649e-006 122.4 1.0741874802988605e-006 123.2 1.064287289409549e-006
		 124 -3.867155101033859e-006 124.8 -1.0792595276143402e-005 125.6 -9.94450874713948e-006
		 126.4 -7.1201870923687238e-006 127.2 -5.326202199285035e-007 128 -1.288945668420638e-006
		 128.8 -2.3431136924045859e-006 129.6 -1.2483380487537945e-005 130.4 -9.4857678050175309e-006
		 131.2 -1.2174742550996598e-005 132 -3.618436494434718e-006 132.8 -6.7689952629734762e-006
		 133.6 -4.811128292203648e-006 134.4 -3.3774240364436992e-006 135.2 -9.3148310043034144e-006
		 136 -6.057322934793774e-006 136.8 -9.0917310444638133e-006 137.6 -9.9594753919518553e-006
		 138.4 -9.8302753031020984e-006 139.2 -9.8673363027046435e-006 140 -2.6985944714397192e-006
		 140.8 -4.1460352804278955e-006 141.6 -6.6590177993930411e-006 142.4 -1.1155859283462632e-005
		 143.2 -9.0348266894579865e-006 144 -5.4668903430865612e-006 144.8 -4.8700603656470776e-006
		 145.6 -4.2591259443724994e-006 146.4 -8.4255373167252401e-007 147.2 -2.9620949248965189e-007
		 148 -1.5946029634505976e-006 148.8 -4.4044131755072158e-006 149.6 1.613344835504904e-007
		 150.4 -1.2122482985432724e-005 151.2 -2.0271781977498904e-006 152 -1.0943892448267434e-005
		 152.8 -1.6091883026092546e-006 153.6 -4.2994106479454786e-006 154.4 -5.9881535889871884e-006
		 155.2 -9.7524962257011794e-006 156 -1.0285895768902266e-005 156.8 -6.4269029280694667e-006
		 157.6 -2.757996071522939e-006 158.4 -4.3161885514564347e-006 159.2 -2.972383072119555e-006
		 160 -8.7862708824104629e-006 160.8 -6.763929377484601e-006 161.6 -4.6014615691092331e-006
		 162.4 -8.1640182543196715e-006 163.2 -1.3998374015500303e-005 164 -7.5091870712640238e-006
		 164.8 -1.0874305189645384e-005 165.6 -9.2467225840664469e-006 166.4 -8.5108094936003909e-006
		 167.2 -4.9843033593788277e-006 168 -1.7534764992888086e-005 168.8 -1.2953462828591e-005
		 169.6 -3.8224411582632456e-006 170.4 2.223091996711446e-006 171.2 1.0510841548239114e-006
		 172 -5.6031699386949185e-006 172.8 -6.0101742747065146e-006 173.6 -7.2429807005391913e-006
		 174.4 1.3033123877903563e-006 175.2 -1.5980624084477313e-005 176 -6.5085991991509218e-006
		 176.8 -6.6501197579782456e-006 177.6 -1.3794055121252315e-005 178.4 -2.5074270979530411e-006
		 179.2 -9.9081980806658976e-006 180 -8.2701162682496943e-006 180.8 7.7380894936140976e-007;
createNode animCurveTA -n "Bip01_R_Toe0_rotateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 227 ".ktv[0:226]"  0 89.999977111816406 0.8 89.999977111816406
		 1.6 89.999977111816406 2.4 89.999977111816406 3.2 89.999977111816406 4 89.999977111816406
		 4.8 89.999977111816406 5.6 89.999984741210937 6.4 89.999977111816406 7.2 89.999977111816406
		 8 89.999977111816406 8.8 89.999977111816406 9.6 89.999977111816406 10.4 89.999977111816406
		 11.2 89.999977111816406 12 89.999977111816406 12.8 89.999977111816406 13.6 89.999977111816406
		 14.4 89.999977111816406 15.2 89.999977111816406 16 89.999984741210937 16.8 89.999977111816406
		 17.6 89.999977111816406 18.4 89.999961853027344 19.2 89.999984741210937 20 89.999977111816406
		 20.8 89.999977111816406 21.6 89.999977111816406 22.4 89.999977111816406 23.2 89.999977111816406
		 24 89.999977111816406 24.8 89.999984741210937 25.6 89.999984741210937 26.4 89.999977111816406
		 27.2 89.999977111816406 28 89.999977111816406 28.8 89.999984741210937 29.6 89.999984741210937
		 30.4 89.999977111816406 31.2 89.999977111816406 32 89.999977111816406 32.8 89.999977111816406
		 33.6 89.999977111816406 34.4 89.999977111816406 35.2 89.999984741210937 36 89.999977111816406
		 36.8 89.999984741210937 37.6 89.999977111816406 38.4 89.999977111816406 39.2 89.999977111816406
		 40 89.999984741210937 40.8 89.999984741210937 41.6 89.999977111816406 42.4 89.999984741210937
		 43.2 89.999984741210937 44 89.999977111816406 44.8 89.999977111816406 45.6 89.999977111816406
		 46.4 89.999977111816406 47.2 89.999961853027344 48 89.999977111816406 48.8 89.999977111816406
		 49.6 89.999977111816406 50.4 89.999977111816406 51.2 89.999977111816406 52 89.999977111816406
		 52.8 89.999984741210937 53.6 89.999984741210937 54.4 89.999977111816406 55.2 89.999984741210937
		 56 89.999984741210937 56.8 89.999977111816406 57.6 89.999977111816406 58.4 89.999977111816406
		 59.2 89.999977111816406 60 89.999977111816406 60.8 89.999984741210937 61.6 89.999977111816406
		 62.4 89.999977111816406 63.2 89.999977111816406 64 89.999977111816406 64.8 89.999977111816406
		 65.6 89.999984741210937 66.4 89.999977111816406 67.2 89.999984741210937 68 89.999977111816406
		 68.8 89.999984741210937 69.6 89.999984741210937 70.4 89.999984741210937 71.2 89.999977111816406
		 72 89.999977111816406 72.8 89.999977111816406 73.6 89.999977111816406 74.4 89.999954223632813
		 75.2 89.999977111816406 76 89.999946594238281 76.8 90.000640869140625 77.6 90.000259399414063
		 78.4 90.000717163085938 79.2 90.000083923339844 80 90.000030517578125 80.8 89.999984741210937
		 81.6 89.999946594238281 82.4 89.999977111816406 83.2 89.999893188476562 84 89.999610900878906
		 84.8 89.999946594238281 85.6 90.001075744628906 86.4 89.99981689453125 87.2 89.999984741210937
		 88 90.00009918212892 88.8 89.999977111816406 89.6 90.000007629394531 90.4 90.000007629394531
		 91.2 89.999984741210937 92 89.999977111816406 92.8 89.999977111816406 93.6 89.999984741210937
		 94.4 89.999961853027344 95.2 89.999977111816406 96 90.000007629394531 96.8 89.999961853027344
		 97.6 89.999916076660156 98.4 89.999664306640625 99.2 89.99993896484375 100 89.999984741210937
		 100.8 89.999923706054688 101.6 90.000007629394531 102.4 89.999977111816406 103.2 89.999984741210937
		 104 89.999977111816406 104.8 89.999977111816406 105.6 89.999992370605483 106.4 89.999946594238281
		 107.2 90.0001220703125 108 90.000076293945313 108.8 89.999992370605483 109.6 90.000007629394531
		 110.4 89.99993896484375 111.2 90.000007629394531 112 90.0001220703125 112.8 89.999908447265625
		 113.6 89.9996337890625 114.4 90.000007629394531 115.2 90.000022888183594 116 89.999992370605483
		 116.8 89.999961853027344 117.6 89.999984741210937 118.4 89.999946594238281 119.2 89.99993896484375
		 120 89.999984741210937 120.8 89.999984741210937 121.6 89.999977111816406 122.4 89.999977111816406
		 123.2 89.999977111816406 124 89.999977111816406 124.8 89.999977111816406 125.6 89.999977111816406
		 126.4 89.999977111816406 127.2 89.999984741210937 128 89.999984741210937 128.8 89.999984741210937
		 129.6 89.999977111816406 130.4 89.999977111816406 131.2 89.999977111816406 132 90.000152587890625
		 132.8 90.000679016113281 133.6 90.000656127929702 134.4 90.000404357910156 135.2 90.000076293945313
		 136 90.000076293945313 136.8 89.999923706054688 137.6 90.000007629394531 138.4 89.99993896484375
		 139.2 89.999961853027344 140 89.999977111816406 140.8 89.999984741210937 141.6 89.999977111816406
		 142.4 89.999977111816406 143.2 89.999977111816406 144 89.999977111816406 144.8 89.999923706054688
		 145.6 89.999977111816406 146.4 89.999984741210937 147.2 89.999992370605483 148 89.999984741210937
		 148.8 89.999977111816406 149.6 89.999977111816406 150.4 89.999977111816406 151.2 89.999977111816406
		 152 89.999977111816406 152.8 89.999984741210937 153.6 89.999984741210937 154.4 89.999977111816406
		 155.2 89.999977111816406 156 89.999977111816406 156.8 89.999977111816406 157.6 89.999977111816406
		 158.4 89.999977111816406 159.2 89.999984741210937 160 89.999977111816406 160.8 89.999977111816406
		 161.6 89.999977111816406 162.4 89.999977111816406 163.2 89.999984741210937 164 89.999984741210937
		 164.8 89.999984741210937 165.6 89.999977111816406 166.4 89.999977111816406 167.2 89.999977111816406
		 168 89.999977111816406 168.8 89.999984741210937 169.6 89.999977111816406 170.4 89.999977111816406
		 171.2 89.999977111816406 172 89.999984741210937 172.8 89.999977111816406 173.6 89.999984741210937
		 174.4 89.999977111816406 175.2 89.999977111816406 176 89.999984741210937 176.8 89.999961853027344
		 177.6 89.999977111816406 178.4 89.999977111816406 179.2 89.999977111816406 180 89.999977111816406
		 180.8 89.999977111816406;
select -ne :time1;
	setAttr ".o" 199;
	setAttr ".unw" 199;
select -ne :renderPartition;
	setAttr -s 126 ".st";
select -ne :initialShadingGroup;
	setAttr ".ro" yes;
select -ne :initialParticleSE;
	setAttr ".ro" yes;
select -ne :defaultShaderList1;
	setAttr -s 64 ".s";
select -ne :defaultTextureList1;
	setAttr -s 31 ".tx";
select -ne :postProcessList1;
	setAttr -s 2 ".p";
select -ne :defaultRenderUtilityList1;
	setAttr -s 31 ".u";
select -ne :defaultRenderingList1;
select -ne :renderGlobalsList1;
select -ne :defaultResolution;
	setAttr ".pa" 1;
select -ne :hardwareRenderGlobals;
	setAttr ".ctrs" 256;
	setAttr ".btrs" 512;
select -ne :hardwareRenderingGlobals;
	setAttr ".otfna" -type "stringArray" 18 "NURBS Curves" "NURBS Surfaces" "Polygons" "Subdiv Surfaces" "Particles" "Fluids" "Image Planes" "UI:" "Lights" "Cameras" "Locators" "Joints" "IK Handles" "Deformers" "Motion Trails" "Components" "Misc. UI" "Ornaments"  ;
	setAttr ".otfva" -type "Int32Array" 18 0 1 1 1 1 1
		 1 0 0 0 0 0 0 0 0 0 0 0 ;
select -ne :defaultHardwareRenderGlobals;
	setAttr ".fn" -type "string" "im";
	setAttr ".res" -type "string" "ntsc_4d 646 485 1.333";
select -ne :hyperGraphLayout;
	setAttr ".hyp[1].isc" yes;
connectAttr "Bip01_Spine_translateX17.o" "Bip01_Spine.tx";
connectAttr "Bip01_Spine_translateY17.o" "Bip01_Spine.ty";
connectAttr "Bip01_Spine_translateZ17.o" "Bip01_Spine.tz";
connectAttr "Bip01_Spine_rotateX17.o" "Bip01_Spine.rx";
connectAttr "Bip01_Spine_rotateY17.o" "Bip01_Spine.ry";
connectAttr "Bip01_Spine_rotateZ17.o" "Bip01_Spine.rz";
connectAttr "Bip01_Spine1_rotateX17.o" "Bip01_Spine1.rx";
connectAttr "Bip01_Spine1_rotateY17.o" "Bip01_Spine1.ry";
connectAttr "Bip01_Spine1_rotateZ17.o" "Bip01_Spine1.rz";
connectAttr "Bip01_Spine.s" "Bip01_Spine1.is";
connectAttr "Bip01_Spine2_rotateX17.o" "Bip01_Spine2.rx";
connectAttr "Bip01_Spine2_rotateY17.o" "Bip01_Spine2.ry";
connectAttr "Bip01_Spine2_rotateZ17.o" "Bip01_Spine2.rz";
connectAttr "Bip01_Spine1.s" "Bip01_Spine2.is";
connectAttr "Bip01_Spine2.s" "Bip01_Neck.is";
connectAttr "Bip01_Neck.s" "Bip01_Head.is";
connectAttr "Bip01_Head.s" "_notused_Bip01_HeadNub.is";
connectAttr "Bip01_R_Clavicle_rotateX17.o" "Bip01_R_Clavicle.rx";
connectAttr "Bip01_R_Clavicle_rotateY17.o" "Bip01_R_Clavicle.ry";
connectAttr "Bip01_R_Clavicle_rotateZ17.o" "Bip01_R_Clavicle.rz";
connectAttr "Bip01_Neck.s" "Bip01_R_Clavicle.is";
connectAttr "Bip01_R_UpperArm_rotateX17.o" "Bip01_R_UpperArm.rx";
connectAttr "Bip01_R_UpperArm_rotateY17.o" "Bip01_R_UpperArm.ry";
connectAttr "Bip01_R_UpperArm_rotateZ17.o" "Bip01_R_UpperArm.rz";
connectAttr "Bip01_R_Clavicle.s" "Bip01_R_UpperArm.is";
connectAttr "Bip01_R_Forearm_rotateX17.o" "Bip01_R_Forearm.rx";
connectAttr "Bip01_R_Forearm_rotateY17.o" "Bip01_R_Forearm.ry";
connectAttr "Bip01_R_Forearm_rotateZ17.o" "Bip01_R_Forearm.rz";
connectAttr "Bip01_R_UpperArm.s" "Bip01_R_Forearm.is";
connectAttr "Bip01_R_Hand_rotateX.o" "Bip01_R_Hand.rx";
connectAttr "Bip01_R_Hand_rotateY.o" "Bip01_R_Hand.ry";
connectAttr "Bip01_R_Hand_rotateZ.o" "Bip01_R_Hand.rz";
connectAttr "Bip01_R_Forearm.s" "Bip01_R_Hand.is";
connectAttr "Bip01_R_Hand.s" "Bip01_R_Finger0.is";
connectAttr "Bip01_R_Finger0.s" "Bip01_R_Finger01.is";
connectAttr "Bip01_R_Finger01.s" "_notused_Bip01_R_Finger0Nub.is";
connectAttr "Bip01_R_Hand.s" "Bip01_R_Finger2.is";
connectAttr "Bip01_R_Finger2.s" "Bip01_R_Finger21.is";
connectAttr "Bip01_R_Finger21.s" "_notused_Bip01_R_Finger2Nub.is";
connectAttr "Bip01_R_Hand.s" "Bip01_R_Finger1.is";
connectAttr "Bip01_R_Finger1.s" "Bip01_R_Finger11.is";
connectAttr "Bip01_R_Finger11.s" "_notused_Bip01_R_Finger1Nub.is";
connectAttr "Bip01_R_Hand.s" "Bip01_R_Finger3.is";
connectAttr "Bip01_R_Finger3.s" "Bip01_R_Finger31.is";
connectAttr "Bip01_R_Finger31.s" "_notused_Bip01_R_Finger3Nub.is";
connectAttr "Bip01_R_Hand.s" "Bip01_R_Finger4.is";
connectAttr "Bip01_R_Finger4.s" "Bip01_R_Finger41.is";
connectAttr "Bip01_R_Finger41.s" "_notused_Bip01_R_Finger4Nub.is";
connectAttr "Bip01_L_Clavicle_rotateX17.o" "Bip01_L_Clavicle.rx";
connectAttr "Bip01_L_Clavicle_rotateY17.o" "Bip01_L_Clavicle.ry";
connectAttr "Bip01_L_Clavicle_rotateZ17.o" "Bip01_L_Clavicle.rz";
connectAttr "Bip01_Neck.s" "Bip01_L_Clavicle.is";
connectAttr "Bip01_L_UpperArm_rotateX17.o" "Bip01_L_UpperArm.rx";
connectAttr "Bip01_L_UpperArm_rotateY17.o" "Bip01_L_UpperArm.ry";
connectAttr "Bip01_L_UpperArm_rotateZ17.o" "Bip01_L_UpperArm.rz";
connectAttr "Bip01_L_Clavicle.s" "Bip01_L_UpperArm.is";
connectAttr "Bip01_L_Forearm_rotateX17.o" "Bip01_L_Forearm.rx";
connectAttr "Bip01_L_Forearm_rotateY17.o" "Bip01_L_Forearm.ry";
connectAttr "Bip01_L_Forearm_rotateZ17.o" "Bip01_L_Forearm.rz";
connectAttr "Bip01_L_UpperArm.s" "Bip01_L_Forearm.is";
connectAttr "Bip01_L_Hand_rotateX7.o" "Bip01_L_Hand.rx";
connectAttr "Bip01_L_Hand_rotateY7.o" "Bip01_L_Hand.ry";
connectAttr "Bip01_L_Hand_rotateZ7.o" "Bip01_L_Hand.rz";
connectAttr "Bip01_L_Forearm.s" "Bip01_L_Hand.is";
connectAttr "Bip01_L_Hand.s" "Bip01_L_Finger0.is";
connectAttr "Bip01_L_Finger0.s" "Bip01_L_Finger01.is";
connectAttr "Bip01_L_Finger01.s" "_notused_Bip01_L_Finger0Nub.is";
connectAttr "Bip01_L_Hand.s" "Bip01_L_Finger2.is";
connectAttr "Bip01_L_Finger2.s" "Bip01_L_Finger21.is";
connectAttr "Bip01_L_Finger21.s" "_notused_Bip01_L_Finger2Nub.is";
connectAttr "Bip01_L_Hand.s" "Bip01_L_Finger1.is";
connectAttr "Bip01_L_Finger1.s" "Bip01_L_Finger11.is";
connectAttr "Bip01_L_Finger11.s" "_notused_Bip01_L_Finger1Nub.is";
connectAttr "Bip01_L_Hand.s" "Bip01_L_Finger3.is";
connectAttr "Bip01_L_Finger3.s" "Bip01_L_Finger31.is";
connectAttr "Bip01_L_Finger31.s" "_notused_Bip01_L_Finger3Nub.is";
connectAttr "Bip01_L_Hand.s" "Bip01_L_Finger4.is";
connectAttr "Bip01_L_Finger4.s" "Bip01_L_Finger41.is";
connectAttr "Bip01_L_Finger41.s" "_notused_Bip01_L_Finger4Nub.is";
connectAttr "Bip01_L_Thigh_rotateX17.o" "Bip01_L_Thigh.rx";
connectAttr "Bip01_L_Thigh_rotateY17.o" "Bip01_L_Thigh.ry";
connectAttr "Bip01_L_Thigh_rotateZ17.o" "Bip01_L_Thigh.rz";
connectAttr "Bip01_Spine.s" "Bip01_L_Thigh.is";
connectAttr "Bip01_L_Calf_rotateX17.o" "Bip01_L_Calf.rx";
connectAttr "Bip01_L_Calf_rotateY17.o" "Bip01_L_Calf.ry";
connectAttr "Bip01_L_Calf_rotateZ17.o" "Bip01_L_Calf.rz";
connectAttr "Bip01_L_Thigh.s" "Bip01_L_Calf.is";
connectAttr "Bip01_L_Foot_rotateX17.o" "Bip01_L_Foot.rx";
connectAttr "Bip01_L_Foot_rotateY17.o" "Bip01_L_Foot.ry";
connectAttr "Bip01_L_Foot_rotateZ17.o" "Bip01_L_Foot.rz";
connectAttr "Bip01_L_Calf.s" "Bip01_L_Foot.is";
connectAttr "Bip01_L_Toe0_rotateX.o" "Bip01_L_Toe0.rx";
connectAttr "Bip01_L_Toe0_rotateY.o" "Bip01_L_Toe0.ry";
connectAttr "Bip01_L_Toe0_rotateZ.o" "Bip01_L_Toe0.rz";
connectAttr "Bip01_L_Foot.s" "Bip01_L_Toe0.is";
connectAttr "Bip01_L_Toe0.s" "_notused_Bip01_L_Toe0Nub.is";
connectAttr "Bip01_R_Thigh_rotateX17.o" "Bip01_R_Thigh.rx";
connectAttr "Bip01_R_Thigh_rotateY17.o" "Bip01_R_Thigh.ry";
connectAttr "Bip01_R_Thigh_rotateZ17.o" "Bip01_R_Thigh.rz";
connectAttr "Bip01_Spine.s" "Bip01_R_Thigh.is";
connectAttr "Bip01_R_Calf_rotateX17.o" "Bip01_R_Calf.rx";
connectAttr "Bip01_R_Calf_rotateY17.o" "Bip01_R_Calf.ry";
connectAttr "Bip01_R_Calf_rotateZ17.o" "Bip01_R_Calf.rz";
connectAttr "Bip01_R_Thigh.s" "Bip01_R_Calf.is";
connectAttr "Bip01_R_Foot_rotateX17.o" "Bip01_R_Foot.rx";
connectAttr "Bip01_R_Foot_rotateY17.o" "Bip01_R_Foot.ry";
connectAttr "Bip01_R_Foot_rotateZ17.o" "Bip01_R_Foot.rz";
connectAttr "Bip01_R_Calf.s" "Bip01_R_Foot.is";
connectAttr "Bip01_R_Toe0_rotateX.o" "Bip01_R_Toe0.rx";
connectAttr "Bip01_R_Toe0_rotateY.o" "Bip01_R_Toe0.ry";
connectAttr "Bip01_R_Toe0_rotateZ.o" "Bip01_R_Toe0.rz";
connectAttr "Bip01_R_Foot.s" "Bip01_R_Toe0.is";
connectAttr "Bip01_R_Toe0.s" "_notused_Bip01_R_Toe0Nub.is";
relationship "link" ":lightLinker1" ":initialShadingGroup.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" ":initialParticleSE.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" "business05_f_highpolySG.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" "business05_f_highpolySG1.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" "business05_f_highpoly_hairSG.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" "business05_f_highpoly_hairSG1.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" "MoCap_Result_Animations_v1_business05_f_highpolySG.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" "MoCap_Result_Animations_v1_business05_f_highpolySG1.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" "MoCap_Result_Animations_v1_business05_f_highpoly_hairSG.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" "MoCap_Result_Animations_v1_business05_f_highpoly_hairSG1.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" "idle_v1:business05_f_highpolySG.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" "idle_v1:business05_f_highpolySG1.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" "idle_v1:business05_f_highpoly_hairSG.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" "idle_v1:business05_f_highpoly_hairSG1.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" "idle_v1_business05_f_highpolySG.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" "idle_v1_business05_f_highpolySG1.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" "idle_v1_business05_f_highpoly_hairSG.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" "idle_v1_business05_f_highpoly_hairSG1.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" "idle_v1_business05_f_highpolySG2.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" "idle_v1_business05_f_highpolySG3.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" "idle_v1_business05_f_highpoly_hairSG2.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" "idle_v1_business05_f_highpoly_hairSG3.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" "stand_v1_business05_f_highpolySG.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" "stand_v1_business05_f_highpolySG1.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" "stand_v1_business05_f_highpoly_hairSG.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" "stand_v1_business05_f_highpoly_hairSG1.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" "sit_idle_v1_business05_f_highpolySG.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" "sit_idle_v1_business05_f_highpolySG1.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" "sit_idle_v1_business05_f_highpoly_hairSG.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" "sit_idle_v1_business05_f_highpoly_hairSG1.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" "sit_talk_v1_business05_f_highpolySG.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" "sit_talk_v1_business05_f_highpolySG1.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" "sit_talk_v1_business05_f_highpoly_hairSG.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" "sit_talk_v1_business05_f_highpoly_hairSG1.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" "Animations_v3_business05_f_highpolySG.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" "Animations_v3_business05_f_highpolySG1.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" "Animations_v3_business05_f_highpoly_hairSG.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" "Animations_v3_business05_f_highpoly_hairSG1.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" "Jessica_Take_2_2_business05_f_highpolySG.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" "Jessica_Take_2_2_business05_f_highpolySG1.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" "Jessica_Take_2_2_business05_f_highpoly_hairSG.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" "Jessica_Take_2_2_business05_f_highpoly_hairSG1.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" "Jessica_Take_2_2_business05_f_highpolySG2.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" "Jessica_Take_2_2_business05_f_highpolySG3.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" "Jessica_Take_2_2_business05_f_highpoly_hairSG2.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" "Jessica_Take_2_2_business05_f_highpoly_hairSG3.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" "Jessica_Take_2_2_business05_f_highpolySG4.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" "Jessica_Take_2_2_business05_f_highpolySG5.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" "Jessica_Take_2_2_business05_f_highpoly_hairSG4.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" "Jessica_Take_2_2_business05_f_highpoly_hairSG5.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" "Jessica_Take_2_2_business05_f_highpolySG6.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" "Jessica_Take_2_2_business05_f_highpolySG7.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" "Jessica_Take_2_2_business05_f_highpoly_hairSG6.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" "Jessica_Take_2_2_business05_f_highpoly_hairSG7.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" "Jessica_Take_2_2_business05_f_highpolySG8.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" "Jessica_Take_2_2_business05_f_highpolySG9.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" "Jessica_Take_2_2_business05_f_highpoly_hairSG8.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" "Jessica_Take_2_2_business05_f_highpoly_hairSG9.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" "Jessica_Take_2_2_business05_f_highpolySG10.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" "Jessica_Take_2_2_business05_f_highpolySG11.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" "Jessica_Take_2_2_business05_f_highpoly_hairSG10.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" "Jessica_Take_2_2_business05_f_highpoly_hairSG11.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" "Candice_Take_1_business05_f_highpolySG.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" "Candice_Take_1_business05_f_highpolySG1.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" "Candice_Take_1_business05_f_highpoly_hairSG.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" "Candice_Take_1_business05_f_highpoly_hairSG1.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" "Candice_Take_1_business05_f_highpolySG2.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" "Candice_Take_1_business05_f_highpolySG3.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" "Candice_Take_1_business05_f_highpoly_hairSG2.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" "Candice_Take_1_business05_f_highpoly_hairSG3.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" "Candice_Take_1_business05_f_highpolySG4.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" "Candice_Take_1_business05_f_highpolySG5.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" "Candice_Take_1_business05_f_highpoly_hairSG4.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" "Candice_Take_1_business05_f_highpoly_hairSG5.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" "Candice_Take_1_business05_f_highpolySG6.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" "Candice_Take_1_business05_f_highpolySG7.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" "Candice_Take_1_business05_f_highpoly_hairSG6.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" "Candice_Take_1_business05_f_highpoly_hairSG7.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" "Candice_Take_1_business05_f_highpolySG8.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" "Candice_Take_1_business05_f_highpolySG9.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" "Candice_Take_1_business05_f_highpoly_hairSG8.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" "Candice_Take_1_business05_f_highpoly_hairSG9.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" "Candice_Take_1_business05_f_highpolySG10.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" "Candice_Take_1_business05_f_highpolySG11.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" "Candice_Take_1_business05_f_highpoly_hairSG10.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" "Candice_Take_1_business05_f_highpoly_hairSG11.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" "Candice_Take_1_business05_f_highpolySG12.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" "Candice_Take_1_business05_f_highpolySG13.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" "Candice_Take_1_business05_f_highpoly_hairSG12.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" "Candice_Take_1_business05_f_highpoly_hairSG13.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" "Candice_Take_1_business05_f_highpolySG14.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" "Candice_Take_1_business05_f_highpolySG15.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" "Candice_Take_1_business05_f_highpoly_hairSG14.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" "Candice_Take_1_business05_f_highpoly_hairSG15.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" "Animations_v3_business05_f_highpolySG2.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" "Animations_v3_business05_f_highpolySG3.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" "Animations_v3_business05_f_highpoly_hairSG2.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" "Animations_v3_business05_f_highpoly_hairSG3.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" "Kuan_take_1_business05_f_highpolySG.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" "Kuan_take_1_business05_f_highpolySG1.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" "Kuan_take_1_business05_f_highpoly_hairSG.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" "Kuan_take_1_business05_f_highpoly_hairSG1.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" "Kuan_take_1_business05_f_highpolySG2.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" "Kuan_take_1_business05_f_highpolySG3.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" "Kuan_take_1_business05_f_highpoly_hairSG2.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" "Kuan_take_1_business05_f_highpoly_hairSG3.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" "Kuan_take_1_business05_f_highpolySG4.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" "Kuan_take_1_business05_f_highpolySG5.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" "Kuan_take_1_business05_f_highpoly_hairSG4.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" "Kuan_take_1_business05_f_highpoly_hairSG5.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" "Kuan_take_1_business05_f_highpolySG6.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" "Kuan_take_1_business05_f_highpolySG7.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" "Kuan_take_1_business05_f_highpoly_hairSG6.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" "Kuan_take_1_business05_f_highpoly_hairSG7.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" "Kuan_take_1_business05_f_highpolySG8.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" "Kuan_take_1_business05_f_highpolySG9.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" "Kuan_take_1_business05_f_highpoly_hairSG8.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" "Kuan_take_1_business05_f_highpoly_hairSG9.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" "Kuan_take_1_business05_f_highpolySG10.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" "Kuan_take_1_business05_f_highpolySG11.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" "Kuan_take_1_business05_f_highpoly_hairSG10.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" "Kuan_take_1_business05_f_highpoly_hairSG11.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" "Kuan_take_1_business05_f_highpolySG12.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" "Kuan_take_1_business05_f_highpolySG13.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" "Kuan_take_1_business05_f_highpoly_hairSG12.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" "Kuan_take_1_business05_f_highpoly_hairSG13.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" ":initialShadingGroup.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" ":initialParticleSE.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" "business05_f_highpolySG.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" "business05_f_highpolySG1.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" "business05_f_highpoly_hairSG.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" "business05_f_highpoly_hairSG1.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" "MoCap_Result_Animations_v1_business05_f_highpolySG.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" "MoCap_Result_Animations_v1_business05_f_highpolySG1.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" "MoCap_Result_Animations_v1_business05_f_highpoly_hairSG.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" "MoCap_Result_Animations_v1_business05_f_highpoly_hairSG1.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" "idle_v1:business05_f_highpolySG.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" "idle_v1:business05_f_highpolySG1.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" "idle_v1:business05_f_highpoly_hairSG.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" "idle_v1:business05_f_highpoly_hairSG1.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" "idle_v1_business05_f_highpolySG.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" "idle_v1_business05_f_highpolySG1.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" "idle_v1_business05_f_highpoly_hairSG.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" "idle_v1_business05_f_highpoly_hairSG1.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" "idle_v1_business05_f_highpolySG2.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" "idle_v1_business05_f_highpolySG3.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" "idle_v1_business05_f_highpoly_hairSG2.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" "idle_v1_business05_f_highpoly_hairSG3.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" "stand_v1_business05_f_highpolySG.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" "stand_v1_business05_f_highpolySG1.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" "stand_v1_business05_f_highpoly_hairSG.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" "stand_v1_business05_f_highpoly_hairSG1.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" "sit_idle_v1_business05_f_highpolySG.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" "sit_idle_v1_business05_f_highpolySG1.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" "sit_idle_v1_business05_f_highpoly_hairSG.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" "sit_idle_v1_business05_f_highpoly_hairSG1.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" "sit_talk_v1_business05_f_highpolySG.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" "sit_talk_v1_business05_f_highpolySG1.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" "sit_talk_v1_business05_f_highpoly_hairSG.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" "sit_talk_v1_business05_f_highpoly_hairSG1.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" "Animations_v3_business05_f_highpolySG.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" "Animations_v3_business05_f_highpolySG1.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" "Animations_v3_business05_f_highpoly_hairSG.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" "Animations_v3_business05_f_highpoly_hairSG1.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" "Jessica_Take_2_2_business05_f_highpolySG.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" "Jessica_Take_2_2_business05_f_highpolySG1.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" "Jessica_Take_2_2_business05_f_highpoly_hairSG.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" "Jessica_Take_2_2_business05_f_highpoly_hairSG1.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" "Jessica_Take_2_2_business05_f_highpolySG2.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" "Jessica_Take_2_2_business05_f_highpolySG3.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" "Jessica_Take_2_2_business05_f_highpoly_hairSG2.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" "Jessica_Take_2_2_business05_f_highpoly_hairSG3.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" "Jessica_Take_2_2_business05_f_highpolySG4.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" "Jessica_Take_2_2_business05_f_highpolySG5.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" "Jessica_Take_2_2_business05_f_highpoly_hairSG4.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" "Jessica_Take_2_2_business05_f_highpoly_hairSG5.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" "Jessica_Take_2_2_business05_f_highpolySG6.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" "Jessica_Take_2_2_business05_f_highpolySG7.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" "Jessica_Take_2_2_business05_f_highpoly_hairSG6.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" "Jessica_Take_2_2_business05_f_highpoly_hairSG7.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" "Jessica_Take_2_2_business05_f_highpolySG8.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" "Jessica_Take_2_2_business05_f_highpolySG9.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" "Jessica_Take_2_2_business05_f_highpoly_hairSG8.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" "Jessica_Take_2_2_business05_f_highpoly_hairSG9.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" "Jessica_Take_2_2_business05_f_highpolySG10.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" "Jessica_Take_2_2_business05_f_highpolySG11.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" "Jessica_Take_2_2_business05_f_highpoly_hairSG10.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" "Jessica_Take_2_2_business05_f_highpoly_hairSG11.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" "Candice_Take_1_business05_f_highpolySG.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" "Candice_Take_1_business05_f_highpolySG1.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" "Candice_Take_1_business05_f_highpoly_hairSG.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" "Candice_Take_1_business05_f_highpoly_hairSG1.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" "Candice_Take_1_business05_f_highpolySG2.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" "Candice_Take_1_business05_f_highpolySG3.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" "Candice_Take_1_business05_f_highpoly_hairSG2.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" "Candice_Take_1_business05_f_highpoly_hairSG3.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" "Candice_Take_1_business05_f_highpolySG4.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" "Candice_Take_1_business05_f_highpolySG5.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" "Candice_Take_1_business05_f_highpoly_hairSG4.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" "Candice_Take_1_business05_f_highpoly_hairSG5.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" "Candice_Take_1_business05_f_highpolySG6.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" "Candice_Take_1_business05_f_highpolySG7.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" "Candice_Take_1_business05_f_highpoly_hairSG6.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" "Candice_Take_1_business05_f_highpoly_hairSG7.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" "Candice_Take_1_business05_f_highpolySG8.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" "Candice_Take_1_business05_f_highpolySG9.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" "Candice_Take_1_business05_f_highpoly_hairSG8.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" "Candice_Take_1_business05_f_highpoly_hairSG9.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" "Candice_Take_1_business05_f_highpolySG10.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" "Candice_Take_1_business05_f_highpolySG11.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" "Candice_Take_1_business05_f_highpoly_hairSG10.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" "Candice_Take_1_business05_f_highpoly_hairSG11.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" "Candice_Take_1_business05_f_highpolySG12.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" "Candice_Take_1_business05_f_highpolySG13.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" "Candice_Take_1_business05_f_highpoly_hairSG12.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" "Candice_Take_1_business05_f_highpoly_hairSG13.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" "Candice_Take_1_business05_f_highpolySG14.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" "Candice_Take_1_business05_f_highpolySG15.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" "Candice_Take_1_business05_f_highpoly_hairSG14.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" "Candice_Take_1_business05_f_highpoly_hairSG15.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" "Animations_v3_business05_f_highpolySG2.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" "Animations_v3_business05_f_highpolySG3.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" "Animations_v3_business05_f_highpoly_hairSG2.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" "Animations_v3_business05_f_highpoly_hairSG3.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" "Kuan_take_1_business05_f_highpolySG.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" "Kuan_take_1_business05_f_highpolySG1.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" "Kuan_take_1_business05_f_highpoly_hairSG.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" "Kuan_take_1_business05_f_highpoly_hairSG1.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" "Kuan_take_1_business05_f_highpolySG2.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" "Kuan_take_1_business05_f_highpolySG3.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" "Kuan_take_1_business05_f_highpoly_hairSG2.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" "Kuan_take_1_business05_f_highpoly_hairSG3.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" "Kuan_take_1_business05_f_highpolySG4.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" "Kuan_take_1_business05_f_highpolySG5.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" "Kuan_take_1_business05_f_highpoly_hairSG4.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" "Kuan_take_1_business05_f_highpoly_hairSG5.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" "Kuan_take_1_business05_f_highpolySG6.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" "Kuan_take_1_business05_f_highpolySG7.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" "Kuan_take_1_business05_f_highpoly_hairSG6.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" "Kuan_take_1_business05_f_highpoly_hairSG7.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" "Kuan_take_1_business05_f_highpolySG8.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" "Kuan_take_1_business05_f_highpolySG9.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" "Kuan_take_1_business05_f_highpoly_hairSG8.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" "Kuan_take_1_business05_f_highpoly_hairSG9.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" "Kuan_take_1_business05_f_highpolySG10.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" "Kuan_take_1_business05_f_highpolySG11.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" "Kuan_take_1_business05_f_highpoly_hairSG10.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" "Kuan_take_1_business05_f_highpoly_hairSG11.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" "Kuan_take_1_business05_f_highpolySG12.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" "Kuan_take_1_business05_f_highpolySG13.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" "Kuan_take_1_business05_f_highpoly_hairSG12.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" "Kuan_take_1_business05_f_highpoly_hairSG13.message" ":defaultLightSet.message";
connectAttr "layerManager.dli[0]" "defaultLayer.id";
connectAttr "renderLayerManager.rlmi[0]" "defaultRenderLayer.rlid";
connectAttr "Map1.oc" "business05_f_highpolyPhong.c";
connectAttr "business05_f_highpolyPhong.oc" "business05_f_highpolySG.ss";
connectAttr "business05_f_highpolySG.msg" "materialInfo1.sg";
connectAttr "business05_f_highpolyPhong.msg" "materialInfo1.m";
connectAttr "Map1.msg" "materialInfo1.t" -na;
connectAttr "business05_f_highpolyPhong.oc" "business05_f_highpolySG1.ss";
connectAttr "business05_f_highpolySG1.msg" "materialInfo2.sg";
connectAttr "business05_f_highpolyPhong.msg" "materialInfo2.m";
connectAttr "Map1.msg" "materialInfo2.t" -na;
connectAttr "place2dTexture1.o" "Map1.uv";
connectAttr "place2dTexture1.ofu" "Map1.ofu";
connectAttr "place2dTexture1.ofv" "Map1.ofv";
connectAttr "place2dTexture1.rf" "Map1.rf";
connectAttr "place2dTexture1.reu" "Map1.reu";
connectAttr "place2dTexture1.rev" "Map1.rev";
connectAttr "place2dTexture1.vt1" "Map1.vt1";
connectAttr "place2dTexture1.vt2" "Map1.vt2";
connectAttr "place2dTexture1.vt3" "Map1.vt3";
connectAttr "place2dTexture1.vc1" "Map1.vc1";
connectAttr "place2dTexture1.ofs" "Map1.fs";
connectAttr "business05_f_highpoly_hairPhong.oc" "business05_f_highpoly_hairSG.ss"
		;
connectAttr "business05_f_highpoly_hairSG.msg" "materialInfo3.sg";
connectAttr "business05_f_highpoly_hairPhong.msg" "materialInfo3.m";
connectAttr "business05_f_highpoly_hairPhong.oc" "business05_f_highpoly_hairSG1.ss"
		;
connectAttr "business05_f_highpoly_hairSG1.msg" "materialInfo4.sg";
connectAttr "business05_f_highpoly_hairPhong.msg" "materialInfo4.m";
connectAttr "MoCap_Result_Animations_v1_Map1.oc" "MoCap_Result_Animations_v1_business05_f_highpolyPhong.c"
		;
connectAttr "MoCap_Result_Animations_v1_business05_f_highpolyPhong.oc" "MoCap_Result_Animations_v1_business05_f_highpolySG.ss"
		;
connectAttr "MoCap_Result_Animations_v1_business05_f_highpolySG.msg" "MoCap_Result_Animations_v1_materialInfo1.sg"
		;
connectAttr "MoCap_Result_Animations_v1_business05_f_highpolyPhong.msg" "MoCap_Result_Animations_v1_materialInfo1.m"
		;
connectAttr "MoCap_Result_Animations_v1_Map1.msg" "MoCap_Result_Animations_v1_materialInfo1.t"
		 -na;
connectAttr "MoCap_Result_Animations_v1_business05_f_highpolyPhong.oc" "MoCap_Result_Animations_v1_business05_f_highpolySG1.ss"
		;
connectAttr "MoCap_Result_Animations_v1_business05_f_highpolySG1.msg" "MoCap_Result_Animations_v1_materialInfo2.sg"
		;
connectAttr "MoCap_Result_Animations_v1_business05_f_highpolyPhong.msg" "MoCap_Result_Animations_v1_materialInfo2.m"
		;
connectAttr "MoCap_Result_Animations_v1_Map1.msg" "MoCap_Result_Animations_v1_materialInfo2.t"
		 -na;
connectAttr "MoCap_Result_Animations_v1_place2dTexture1.o" "MoCap_Result_Animations_v1_Map1.uv"
		;
connectAttr "MoCap_Result_Animations_v1_place2dTexture1.ofu" "MoCap_Result_Animations_v1_Map1.ofu"
		;
connectAttr "MoCap_Result_Animations_v1_place2dTexture1.ofv" "MoCap_Result_Animations_v1_Map1.ofv"
		;
connectAttr "MoCap_Result_Animations_v1_place2dTexture1.rf" "MoCap_Result_Animations_v1_Map1.rf"
		;
connectAttr "MoCap_Result_Animations_v1_place2dTexture1.reu" "MoCap_Result_Animations_v1_Map1.reu"
		;
connectAttr "MoCap_Result_Animations_v1_place2dTexture1.rev" "MoCap_Result_Animations_v1_Map1.rev"
		;
connectAttr "MoCap_Result_Animations_v1_place2dTexture1.vt1" "MoCap_Result_Animations_v1_Map1.vt1"
		;
connectAttr "MoCap_Result_Animations_v1_place2dTexture1.vt2" "MoCap_Result_Animations_v1_Map1.vt2"
		;
connectAttr "MoCap_Result_Animations_v1_place2dTexture1.vt3" "MoCap_Result_Animations_v1_Map1.vt3"
		;
connectAttr "MoCap_Result_Animations_v1_place2dTexture1.vc1" "MoCap_Result_Animations_v1_Map1.vc1"
		;
connectAttr "MoCap_Result_Animations_v1_place2dTexture1.ofs" "MoCap_Result_Animations_v1_Map1.fs"
		;
connectAttr "MoCap_Result_Animations_v1_business05_f_highpoly_hairPhong.oc" "MoCap_Result_Animations_v1_business05_f_highpoly_hairSG.ss"
		;
connectAttr "MoCap_Result_Animations_v1_business05_f_highpoly_hairSG.msg" "MoCap_Result_Animations_v1_materialInfo3.sg"
		;
connectAttr "MoCap_Result_Animations_v1_business05_f_highpoly_hairPhong.msg" "MoCap_Result_Animations_v1_materialInfo3.m"
		;
connectAttr "MoCap_Result_Animations_v1_business05_f_highpoly_hairPhong.oc" "MoCap_Result_Animations_v1_business05_f_highpoly_hairSG1.ss"
		;
connectAttr "MoCap_Result_Animations_v1_business05_f_highpoly_hairSG1.msg" "MoCap_Result_Animations_v1_materialInfo4.sg"
		;
connectAttr "MoCap_Result_Animations_v1_business05_f_highpoly_hairPhong.msg" "MoCap_Result_Animations_v1_materialInfo4.m"
		;
connectAttr "idle_v1:Map1.oc" "idle_v1:business05_f_highpolyPhong.c";
connectAttr "idle_v1:business05_f_highpolyPhong.oc" "idle_v1:business05_f_highpolySG.ss"
		;
connectAttr "idle_v1:business05_f_highpolySG.msg" "idle_v1:materialInfo1.sg";
connectAttr "idle_v1:business05_f_highpolyPhong.msg" "idle_v1:materialInfo1.m";
connectAttr "idle_v1:Map1.msg" "idle_v1:materialInfo1.t" -na;
connectAttr "idle_v1:business05_f_highpolyPhong.oc" "idle_v1:business05_f_highpolySG1.ss"
		;
connectAttr "idle_v1:business05_f_highpolySG1.msg" "idle_v1:materialInfo2.sg";
connectAttr "idle_v1:business05_f_highpolyPhong.msg" "idle_v1:materialInfo2.m";
connectAttr "idle_v1:Map1.msg" "idle_v1:materialInfo2.t" -na;
connectAttr "idle_v1:place2dTexture1.o" "idle_v1:Map1.uv";
connectAttr "idle_v1:place2dTexture1.ofu" "idle_v1:Map1.ofu";
connectAttr "idle_v1:place2dTexture1.ofv" "idle_v1:Map1.ofv";
connectAttr "idle_v1:place2dTexture1.rf" "idle_v1:Map1.rf";
connectAttr "idle_v1:place2dTexture1.reu" "idle_v1:Map1.reu";
connectAttr "idle_v1:place2dTexture1.rev" "idle_v1:Map1.rev";
connectAttr "idle_v1:place2dTexture1.vt1" "idle_v1:Map1.vt1";
connectAttr "idle_v1:place2dTexture1.vt2" "idle_v1:Map1.vt2";
connectAttr "idle_v1:place2dTexture1.vt3" "idle_v1:Map1.vt3";
connectAttr "idle_v1:place2dTexture1.vc1" "idle_v1:Map1.vc1";
connectAttr "idle_v1:place2dTexture1.ofs" "idle_v1:Map1.fs";
connectAttr "idle_v1:business05_f_highpoly_hairPhong.oc" "idle_v1:business05_f_highpoly_hairSG.ss"
		;
connectAttr "idle_v1:business05_f_highpoly_hairSG.msg" "idle_v1:materialInfo3.sg"
		;
connectAttr "idle_v1:business05_f_highpoly_hairPhong.msg" "idle_v1:materialInfo3.m"
		;
connectAttr "idle_v1:business05_f_highpoly_hairPhong.oc" "idle_v1:business05_f_highpoly_hairSG1.ss"
		;
connectAttr "idle_v1:business05_f_highpoly_hairSG1.msg" "idle_v1:materialInfo4.sg"
		;
connectAttr "idle_v1:business05_f_highpoly_hairPhong.msg" "idle_v1:materialInfo4.m"
		;
connectAttr "idle_v1_Map1.oc" "idle_v1_business05_f_highpolyPhong.c";
connectAttr "idle_v1_business05_f_highpolyPhong.oc" "idle_v1_business05_f_highpolySG.ss"
		;
connectAttr "idle_v1_business05_f_highpolySG.msg" "idle_v1_materialInfo1.sg";
connectAttr "idle_v1_business05_f_highpolyPhong.msg" "idle_v1_materialInfo1.m";
connectAttr "idle_v1_Map1.msg" "idle_v1_materialInfo1.t" -na;
connectAttr "idle_v1_business05_f_highpolyPhong.oc" "idle_v1_business05_f_highpolySG1.ss"
		;
connectAttr "idle_v1_business05_f_highpolySG1.msg" "idle_v1_materialInfo2.sg";
connectAttr "idle_v1_business05_f_highpolyPhong.msg" "idle_v1_materialInfo2.m";
connectAttr "idle_v1_Map1.msg" "idle_v1_materialInfo2.t" -na;
connectAttr "idle_v1_place2dTexture1.o" "idle_v1_Map1.uv";
connectAttr "idle_v1_place2dTexture1.ofu" "idle_v1_Map1.ofu";
connectAttr "idle_v1_place2dTexture1.ofv" "idle_v1_Map1.ofv";
connectAttr "idle_v1_place2dTexture1.rf" "idle_v1_Map1.rf";
connectAttr "idle_v1_place2dTexture1.reu" "idle_v1_Map1.reu";
connectAttr "idle_v1_place2dTexture1.rev" "idle_v1_Map1.rev";
connectAttr "idle_v1_place2dTexture1.vt1" "idle_v1_Map1.vt1";
connectAttr "idle_v1_place2dTexture1.vt2" "idle_v1_Map1.vt2";
connectAttr "idle_v1_place2dTexture1.vt3" "idle_v1_Map1.vt3";
connectAttr "idle_v1_place2dTexture1.vc1" "idle_v1_Map1.vc1";
connectAttr "idle_v1_place2dTexture1.ofs" "idle_v1_Map1.fs";
connectAttr "idle_v1_business05_f_highpoly_hairPhong.oc" "idle_v1_business05_f_highpoly_hairSG.ss"
		;
connectAttr "idle_v1_business05_f_highpoly_hairSG.msg" "idle_v1_materialInfo3.sg"
		;
connectAttr "idle_v1_business05_f_highpoly_hairPhong.msg" "idle_v1_materialInfo3.m"
		;
connectAttr "idle_v1_business05_f_highpoly_hairPhong.oc" "idle_v1_business05_f_highpoly_hairSG1.ss"
		;
connectAttr "idle_v1_business05_f_highpoly_hairSG1.msg" "idle_v1_materialInfo4.sg"
		;
connectAttr "idle_v1_business05_f_highpoly_hairPhong.msg" "idle_v1_materialInfo4.m"
		;
connectAttr "idle_v1_Map2.oc" "idle_v1_business05_f_highpolyPhong1.c";
connectAttr "idle_v1_business05_f_highpolyPhong1.oc" "idle_v1_business05_f_highpolySG2.ss"
		;
connectAttr "idle_v1_business05_f_highpolySG2.msg" "idle_v1_materialInfo5.sg";
connectAttr "idle_v1_business05_f_highpolyPhong1.msg" "idle_v1_materialInfo5.m";
connectAttr "idle_v1_Map2.msg" "idle_v1_materialInfo5.t" -na;
connectAttr "idle_v1_business05_f_highpolyPhong1.oc" "idle_v1_business05_f_highpolySG3.ss"
		;
connectAttr "idle_v1_business05_f_highpolySG3.msg" "idle_v1_materialInfo6.sg";
connectAttr "idle_v1_business05_f_highpolyPhong1.msg" "idle_v1_materialInfo6.m";
connectAttr "idle_v1_Map2.msg" "idle_v1_materialInfo6.t" -na;
connectAttr "idle_v1_place2dTexture2.o" "idle_v1_Map2.uv";
connectAttr "idle_v1_place2dTexture2.ofu" "idle_v1_Map2.ofu";
connectAttr "idle_v1_place2dTexture2.ofv" "idle_v1_Map2.ofv";
connectAttr "idle_v1_place2dTexture2.rf" "idle_v1_Map2.rf";
connectAttr "idle_v1_place2dTexture2.reu" "idle_v1_Map2.reu";
connectAttr "idle_v1_place2dTexture2.rev" "idle_v1_Map2.rev";
connectAttr "idle_v1_place2dTexture2.vt1" "idle_v1_Map2.vt1";
connectAttr "idle_v1_place2dTexture2.vt2" "idle_v1_Map2.vt2";
connectAttr "idle_v1_place2dTexture2.vt3" "idle_v1_Map2.vt3";
connectAttr "idle_v1_place2dTexture2.vc1" "idle_v1_Map2.vc1";
connectAttr "idle_v1_place2dTexture2.ofs" "idle_v1_Map2.fs";
connectAttr "idle_v1_business05_f_highpoly_hairPhong1.oc" "idle_v1_business05_f_highpoly_hairSG2.ss"
		;
connectAttr "idle_v1_business05_f_highpoly_hairSG2.msg" "idle_v1_materialInfo7.sg"
		;
connectAttr "idle_v1_business05_f_highpoly_hairPhong1.msg" "idle_v1_materialInfo7.m"
		;
connectAttr "idle_v1_business05_f_highpoly_hairPhong1.oc" "idle_v1_business05_f_highpoly_hairSG3.ss"
		;
connectAttr "idle_v1_business05_f_highpoly_hairSG3.msg" "idle_v1_materialInfo8.sg"
		;
connectAttr "idle_v1_business05_f_highpoly_hairPhong1.msg" "idle_v1_materialInfo8.m"
		;
connectAttr "Bip01_Spine.msg" "idle_v1_bindPose1.m[0]";
connectAttr "Bip01_R_Thigh.msg" "idle_v1_bindPose1.m[1]";
connectAttr "Bip01_Spine1.msg" "idle_v1_bindPose1.m[2]";
connectAttr "Bip01_Spine2.msg" "idle_v1_bindPose1.m[3]";
connectAttr "Bip01_Neck.msg" "idle_v1_bindPose1.m[4]";
connectAttr "Bip01_R_Clavicle.msg" "idle_v1_bindPose1.m[5]";
connectAttr "Bip01_L_Clavicle.msg" "idle_v1_bindPose1.m[6]";
connectAttr "Bip01_R_UpperArm.msg" "idle_v1_bindPose1.m[7]";
connectAttr "Bip01_R_Forearm.msg" "idle_v1_bindPose1.m[8]";
connectAttr "Bip01_R_Hand.msg" "idle_v1_bindPose1.m[9]";
connectAttr "Bip01_R_Finger0.msg" "idle_v1_bindPose1.m[10]";
connectAttr "Bip01_L_Thigh.msg" "idle_v1_bindPose1.m[11]";
connectAttr "Bip01_Head.msg" "idle_v1_bindPose1.m[12]";
connectAttr "Bip01_L_Calf.msg" "idle_v1_bindPose1.m[13]";
connectAttr "Bip01_R_Finger01.msg" "idle_v1_bindPose1.m[14]";
connectAttr "Bip01_R_Finger3.msg" "idle_v1_bindPose1.m[15]";
connectAttr "Bip01_R_Finger4.msg" "idle_v1_bindPose1.m[16]";
connectAttr "Bip01_R_Finger1.msg" "idle_v1_bindPose1.m[17]";
connectAttr "Bip01_R_Finger11.msg" "idle_v1_bindPose1.m[18]";
connectAttr "Bip01_R_Finger2.msg" "idle_v1_bindPose1.m[19]";
connectAttr "Bip01_R_Finger21.msg" "idle_v1_bindPose1.m[20]";
connectAttr "Bip01_R_Finger31.msg" "idle_v1_bindPose1.m[21]";
connectAttr "Bip01_R_Finger41.msg" "idle_v1_bindPose1.m[22]";
connectAttr "Bip01_R_Calf.msg" "idle_v1_bindPose1.m[23]";
connectAttr "Bip01_R_Foot.msg" "idle_v1_bindPose1.m[24]";
connectAttr "Bip01_R_Toe0.msg" "idle_v1_bindPose1.m[25]";
connectAttr "Bip01_L_Foot.msg" "idle_v1_bindPose1.m[26]";
connectAttr "Bip01_L_Toe0.msg" "idle_v1_bindPose1.m[27]";
connectAttr "Bip01_L_UpperArm.msg" "idle_v1_bindPose1.m[28]";
connectAttr "Bip01_L_Forearm.msg" "idle_v1_bindPose1.m[29]";
connectAttr "Bip01_L_Hand.msg" "idle_v1_bindPose1.m[30]";
connectAttr "Bip01_L_Finger0.msg" "idle_v1_bindPose1.m[31]";
connectAttr "Bip01_L_Finger01.msg" "idle_v1_bindPose1.m[32]";
connectAttr "Bip01_L_Finger3.msg" "idle_v1_bindPose1.m[33]";
connectAttr "Bip01_L_Finger4.msg" "idle_v1_bindPose1.m[34]";
connectAttr "Bip01_L_Finger1.msg" "idle_v1_bindPose1.m[35]";
connectAttr "Bip01_L_Finger11.msg" "idle_v1_bindPose1.m[36]";
connectAttr "Bip01_L_Finger2.msg" "idle_v1_bindPose1.m[37]";
connectAttr "Bip01_L_Finger21.msg" "idle_v1_bindPose1.m[38]";
connectAttr "Bip01_L_Finger31.msg" "idle_v1_bindPose1.m[39]";
connectAttr "Bip01_L_Finger41.msg" "idle_v1_bindPose1.m[40]";
connectAttr "idle_v1_bindPose1.w" "idle_v1_bindPose1.p[0]";
connectAttr "idle_v1_bindPose1.m[0]" "idle_v1_bindPose1.p[1]";
connectAttr "idle_v1_bindPose1.m[0]" "idle_v1_bindPose1.p[2]";
connectAttr "idle_v1_bindPose1.m[2]" "idle_v1_bindPose1.p[3]";
connectAttr "idle_v1_bindPose1.m[3]" "idle_v1_bindPose1.p[4]";
connectAttr "idle_v1_bindPose1.m[4]" "idle_v1_bindPose1.p[5]";
connectAttr "idle_v1_bindPose1.m[4]" "idle_v1_bindPose1.p[6]";
connectAttr "idle_v1_bindPose1.m[5]" "idle_v1_bindPose1.p[7]";
connectAttr "idle_v1_bindPose1.m[7]" "idle_v1_bindPose1.p[8]";
connectAttr "idle_v1_bindPose1.m[8]" "idle_v1_bindPose1.p[9]";
connectAttr "idle_v1_bindPose1.m[9]" "idle_v1_bindPose1.p[10]";
connectAttr "idle_v1_bindPose1.m[0]" "idle_v1_bindPose1.p[11]";
connectAttr "idle_v1_bindPose1.m[4]" "idle_v1_bindPose1.p[12]";
connectAttr "idle_v1_bindPose1.m[11]" "idle_v1_bindPose1.p[13]";
connectAttr "idle_v1_bindPose1.m[10]" "idle_v1_bindPose1.p[14]";
connectAttr "idle_v1_bindPose1.m[9]" "idle_v1_bindPose1.p[15]";
connectAttr "idle_v1_bindPose1.m[9]" "idle_v1_bindPose1.p[16]";
connectAttr "idle_v1_bindPose1.m[9]" "idle_v1_bindPose1.p[17]";
connectAttr "idle_v1_bindPose1.m[17]" "idle_v1_bindPose1.p[18]";
connectAttr "idle_v1_bindPose1.m[9]" "idle_v1_bindPose1.p[19]";
connectAttr "idle_v1_bindPose1.m[19]" "idle_v1_bindPose1.p[20]";
connectAttr "idle_v1_bindPose1.m[15]" "idle_v1_bindPose1.p[21]";
connectAttr "idle_v1_bindPose1.m[16]" "idle_v1_bindPose1.p[22]";
connectAttr "idle_v1_bindPose1.m[1]" "idle_v1_bindPose1.p[23]";
connectAttr "idle_v1_bindPose1.m[23]" "idle_v1_bindPose1.p[24]";
connectAttr "idle_v1_bindPose1.m[24]" "idle_v1_bindPose1.p[25]";
connectAttr "idle_v1_bindPose1.m[13]" "idle_v1_bindPose1.p[26]";
connectAttr "idle_v1_bindPose1.m[26]" "idle_v1_bindPose1.p[27]";
connectAttr "idle_v1_bindPose1.m[6]" "idle_v1_bindPose1.p[28]";
connectAttr "idle_v1_bindPose1.m[28]" "idle_v1_bindPose1.p[29]";
connectAttr "idle_v1_bindPose1.m[29]" "idle_v1_bindPose1.p[30]";
connectAttr "idle_v1_bindPose1.m[30]" "idle_v1_bindPose1.p[31]";
connectAttr "idle_v1_bindPose1.m[31]" "idle_v1_bindPose1.p[32]";
connectAttr "idle_v1_bindPose1.m[30]" "idle_v1_bindPose1.p[33]";
connectAttr "idle_v1_bindPose1.m[30]" "idle_v1_bindPose1.p[34]";
connectAttr "idle_v1_bindPose1.m[30]" "idle_v1_bindPose1.p[35]";
connectAttr "idle_v1_bindPose1.m[35]" "idle_v1_bindPose1.p[36]";
connectAttr "idle_v1_bindPose1.m[30]" "idle_v1_bindPose1.p[37]";
connectAttr "idle_v1_bindPose1.m[37]" "idle_v1_bindPose1.p[38]";
connectAttr "idle_v1_bindPose1.m[33]" "idle_v1_bindPose1.p[39]";
connectAttr "idle_v1_bindPose1.m[34]" "idle_v1_bindPose1.p[40]";
connectAttr "Bip01_Spine.bps" "idle_v1_bindPose1.wm[0]";
connectAttr "Bip01_R_Thigh.bps" "idle_v1_bindPose1.wm[1]";
connectAttr "Bip01_Spine1.bps" "idle_v1_bindPose1.wm[2]";
connectAttr "Bip01_Spine2.bps" "idle_v1_bindPose1.wm[3]";
connectAttr "Bip01_Neck.bps" "idle_v1_bindPose1.wm[4]";
connectAttr "Bip01_R_Clavicle.bps" "idle_v1_bindPose1.wm[5]";
connectAttr "Bip01_L_Clavicle.bps" "idle_v1_bindPose1.wm[6]";
connectAttr "Bip01_R_UpperArm.bps" "idle_v1_bindPose1.wm[7]";
connectAttr "Bip01_R_Forearm.bps" "idle_v1_bindPose1.wm[8]";
connectAttr "Bip01_R_Hand.bps" "idle_v1_bindPose1.wm[9]";
connectAttr "Bip01_R_Finger0.bps" "idle_v1_bindPose1.wm[10]";
connectAttr "Bip01_L_Thigh.bps" "idle_v1_bindPose1.wm[11]";
connectAttr "Bip01_Head.bps" "idle_v1_bindPose1.wm[12]";
connectAttr "Bip01_L_Calf.bps" "idle_v1_bindPose1.wm[13]";
connectAttr "Bip01_R_Finger01.bps" "idle_v1_bindPose1.wm[14]";
connectAttr "Bip01_R_Finger3.bps" "idle_v1_bindPose1.wm[15]";
connectAttr "Bip01_R_Finger4.bps" "idle_v1_bindPose1.wm[16]";
connectAttr "Bip01_R_Finger1.bps" "idle_v1_bindPose1.wm[17]";
connectAttr "Bip01_R_Finger11.bps" "idle_v1_bindPose1.wm[18]";
connectAttr "Bip01_R_Finger2.bps" "idle_v1_bindPose1.wm[19]";
connectAttr "Bip01_R_Finger21.bps" "idle_v1_bindPose1.wm[20]";
connectAttr "Bip01_R_Finger31.bps" "idle_v1_bindPose1.wm[21]";
connectAttr "Bip01_R_Finger41.bps" "idle_v1_bindPose1.wm[22]";
connectAttr "Bip01_R_Calf.bps" "idle_v1_bindPose1.wm[23]";
connectAttr "Bip01_R_Foot.bps" "idle_v1_bindPose1.wm[24]";
connectAttr "Bip01_R_Toe0.bps" "idle_v1_bindPose1.wm[25]";
connectAttr "Bip01_L_Foot.bps" "idle_v1_bindPose1.wm[26]";
connectAttr "Bip01_L_Toe0.bps" "idle_v1_bindPose1.wm[27]";
connectAttr "Bip01_L_UpperArm.bps" "idle_v1_bindPose1.wm[28]";
connectAttr "Bip01_L_Forearm.bps" "idle_v1_bindPose1.wm[29]";
connectAttr "Bip01_L_Hand.bps" "idle_v1_bindPose1.wm[30]";
connectAttr "Bip01_L_Finger0.bps" "idle_v1_bindPose1.wm[31]";
connectAttr "Bip01_L_Finger01.bps" "idle_v1_bindPose1.wm[32]";
connectAttr "Bip01_L_Finger3.bps" "idle_v1_bindPose1.wm[33]";
connectAttr "Bip01_L_Finger4.bps" "idle_v1_bindPose1.wm[34]";
connectAttr "Bip01_L_Finger1.bps" "idle_v1_bindPose1.wm[35]";
connectAttr "Bip01_L_Finger11.bps" "idle_v1_bindPose1.wm[36]";
connectAttr "Bip01_L_Finger2.bps" "idle_v1_bindPose1.wm[37]";
connectAttr "Bip01_L_Finger21.bps" "idle_v1_bindPose1.wm[38]";
connectAttr "Bip01_L_Finger31.bps" "idle_v1_bindPose1.wm[39]";
connectAttr "Bip01_L_Finger41.bps" "idle_v1_bindPose1.wm[40]";
connectAttr "stand_v1_Map1.oc" "stand_v1_business05_f_highpolyPhong.c";
connectAttr "stand_v1_business05_f_highpolyPhong.oc" "stand_v1_business05_f_highpolySG.ss"
		;
connectAttr "stand_v1_business05_f_highpolySG.msg" "stand_v1_materialInfo1.sg";
connectAttr "stand_v1_business05_f_highpolyPhong.msg" "stand_v1_materialInfo1.m"
		;
connectAttr "stand_v1_Map1.msg" "stand_v1_materialInfo1.t" -na;
connectAttr "stand_v1_business05_f_highpolyPhong.oc" "stand_v1_business05_f_highpolySG1.ss"
		;
connectAttr "stand_v1_business05_f_highpolySG1.msg" "stand_v1_materialInfo2.sg";
connectAttr "stand_v1_business05_f_highpolyPhong.msg" "stand_v1_materialInfo2.m"
		;
connectAttr "stand_v1_Map1.msg" "stand_v1_materialInfo2.t" -na;
connectAttr "stand_v1_place2dTexture1.o" "stand_v1_Map1.uv";
connectAttr "stand_v1_place2dTexture1.ofu" "stand_v1_Map1.ofu";
connectAttr "stand_v1_place2dTexture1.ofv" "stand_v1_Map1.ofv";
connectAttr "stand_v1_place2dTexture1.rf" "stand_v1_Map1.rf";
connectAttr "stand_v1_place2dTexture1.reu" "stand_v1_Map1.reu";
connectAttr "stand_v1_place2dTexture1.rev" "stand_v1_Map1.rev";
connectAttr "stand_v1_place2dTexture1.vt1" "stand_v1_Map1.vt1";
connectAttr "stand_v1_place2dTexture1.vt2" "stand_v1_Map1.vt2";
connectAttr "stand_v1_place2dTexture1.vt3" "stand_v1_Map1.vt3";
connectAttr "stand_v1_place2dTexture1.vc1" "stand_v1_Map1.vc1";
connectAttr "stand_v1_place2dTexture1.ofs" "stand_v1_Map1.fs";
connectAttr "stand_v1_business05_f_highpoly_hairPhong.oc" "stand_v1_business05_f_highpoly_hairSG.ss"
		;
connectAttr "stand_v1_business05_f_highpoly_hairSG.msg" "stand_v1_materialInfo3.sg"
		;
connectAttr "stand_v1_business05_f_highpoly_hairPhong.msg" "stand_v1_materialInfo3.m"
		;
connectAttr "stand_v1_business05_f_highpoly_hairPhong.oc" "stand_v1_business05_f_highpoly_hairSG1.ss"
		;
connectAttr "stand_v1_business05_f_highpoly_hairSG1.msg" "stand_v1_materialInfo4.sg"
		;
connectAttr "stand_v1_business05_f_highpoly_hairPhong.msg" "stand_v1_materialInfo4.m"
		;
connectAttr "sit_idle_v1_Map1.oc" "sit_idle_v1_business05_f_highpolyPhong.c";
connectAttr "sit_idle_v1_business05_f_highpolyPhong.oc" "sit_idle_v1_business05_f_highpolySG.ss"
		;
connectAttr "sit_idle_v1_business05_f_highpolySG.msg" "sit_idle_v1_materialInfo1.sg"
		;
connectAttr "sit_idle_v1_business05_f_highpolyPhong.msg" "sit_idle_v1_materialInfo1.m"
		;
connectAttr "sit_idle_v1_Map1.msg" "sit_idle_v1_materialInfo1.t" -na;
connectAttr "sit_idle_v1_business05_f_highpolyPhong.oc" "sit_idle_v1_business05_f_highpolySG1.ss"
		;
connectAttr "sit_idle_v1_business05_f_highpolySG1.msg" "sit_idle_v1_materialInfo2.sg"
		;
connectAttr "sit_idle_v1_business05_f_highpolyPhong.msg" "sit_idle_v1_materialInfo2.m"
		;
connectAttr "sit_idle_v1_Map1.msg" "sit_idle_v1_materialInfo2.t" -na;
connectAttr "sit_idle_v1_place2dTexture1.o" "sit_idle_v1_Map1.uv";
connectAttr "sit_idle_v1_place2dTexture1.ofu" "sit_idle_v1_Map1.ofu";
connectAttr "sit_idle_v1_place2dTexture1.ofv" "sit_idle_v1_Map1.ofv";
connectAttr "sit_idle_v1_place2dTexture1.rf" "sit_idle_v1_Map1.rf";
connectAttr "sit_idle_v1_place2dTexture1.reu" "sit_idle_v1_Map1.reu";
connectAttr "sit_idle_v1_place2dTexture1.rev" "sit_idle_v1_Map1.rev";
connectAttr "sit_idle_v1_place2dTexture1.vt1" "sit_idle_v1_Map1.vt1";
connectAttr "sit_idle_v1_place2dTexture1.vt2" "sit_idle_v1_Map1.vt2";
connectAttr "sit_idle_v1_place2dTexture1.vt3" "sit_idle_v1_Map1.vt3";
connectAttr "sit_idle_v1_place2dTexture1.vc1" "sit_idle_v1_Map1.vc1";
connectAttr "sit_idle_v1_place2dTexture1.ofs" "sit_idle_v1_Map1.fs";
connectAttr "sit_idle_v1_business05_f_highpoly_hairPhong.oc" "sit_idle_v1_business05_f_highpoly_hairSG.ss"
		;
connectAttr "sit_idle_v1_business05_f_highpoly_hairSG.msg" "sit_idle_v1_materialInfo3.sg"
		;
connectAttr "sit_idle_v1_business05_f_highpoly_hairPhong.msg" "sit_idle_v1_materialInfo3.m"
		;
connectAttr "sit_idle_v1_business05_f_highpoly_hairPhong.oc" "sit_idle_v1_business05_f_highpoly_hairSG1.ss"
		;
connectAttr "sit_idle_v1_business05_f_highpoly_hairSG1.msg" "sit_idle_v1_materialInfo4.sg"
		;
connectAttr "sit_idle_v1_business05_f_highpoly_hairPhong.msg" "sit_idle_v1_materialInfo4.m"
		;
connectAttr "sit_talk_v1_Map1.oc" "sit_talk_v1_business05_f_highpolyPhong.c";
connectAttr "sit_talk_v1_business05_f_highpolyPhong.oc" "sit_talk_v1_business05_f_highpolySG.ss"
		;
connectAttr "sit_talk_v1_business05_f_highpolySG.msg" "sit_talk_v1_materialInfo1.sg"
		;
connectAttr "sit_talk_v1_business05_f_highpolyPhong.msg" "sit_talk_v1_materialInfo1.m"
		;
connectAttr "sit_talk_v1_Map1.msg" "sit_talk_v1_materialInfo1.t" -na;
connectAttr "sit_talk_v1_business05_f_highpolyPhong.oc" "sit_talk_v1_business05_f_highpolySG1.ss"
		;
connectAttr "sit_talk_v1_business05_f_highpolySG1.msg" "sit_talk_v1_materialInfo2.sg"
		;
connectAttr "sit_talk_v1_business05_f_highpolyPhong.msg" "sit_talk_v1_materialInfo2.m"
		;
connectAttr "sit_talk_v1_Map1.msg" "sit_talk_v1_materialInfo2.t" -na;
connectAttr "sit_talk_v1_place2dTexture1.o" "sit_talk_v1_Map1.uv";
connectAttr "sit_talk_v1_place2dTexture1.ofu" "sit_talk_v1_Map1.ofu";
connectAttr "sit_talk_v1_place2dTexture1.ofv" "sit_talk_v1_Map1.ofv";
connectAttr "sit_talk_v1_place2dTexture1.rf" "sit_talk_v1_Map1.rf";
connectAttr "sit_talk_v1_place2dTexture1.reu" "sit_talk_v1_Map1.reu";
connectAttr "sit_talk_v1_place2dTexture1.rev" "sit_talk_v1_Map1.rev";
connectAttr "sit_talk_v1_place2dTexture1.vt1" "sit_talk_v1_Map1.vt1";
connectAttr "sit_talk_v1_place2dTexture1.vt2" "sit_talk_v1_Map1.vt2";
connectAttr "sit_talk_v1_place2dTexture1.vt3" "sit_talk_v1_Map1.vt3";
connectAttr "sit_talk_v1_place2dTexture1.vc1" "sit_talk_v1_Map1.vc1";
connectAttr "sit_talk_v1_place2dTexture1.ofs" "sit_talk_v1_Map1.fs";
connectAttr "sit_talk_v1_business05_f_highpoly_hairPhong.oc" "sit_talk_v1_business05_f_highpoly_hairSG.ss"
		;
connectAttr "sit_talk_v1_business05_f_highpoly_hairSG.msg" "sit_talk_v1_materialInfo3.sg"
		;
connectAttr "sit_talk_v1_business05_f_highpoly_hairPhong.msg" "sit_talk_v1_materialInfo3.m"
		;
connectAttr "sit_talk_v1_business05_f_highpoly_hairPhong.oc" "sit_talk_v1_business05_f_highpoly_hairSG1.ss"
		;
connectAttr "sit_talk_v1_business05_f_highpoly_hairSG1.msg" "sit_talk_v1_materialInfo4.sg"
		;
connectAttr "sit_talk_v1_business05_f_highpoly_hairPhong.msg" "sit_talk_v1_materialInfo4.m"
		;
connectAttr ":mentalrayGlobals.msg" ":mentalrayItemsList.glb";
connectAttr ":miDefaultOptions.msg" ":mentalrayItemsList.opt" -na;
connectAttr ":miDefaultFramebuffer.msg" ":mentalrayItemsList.fb" -na;
connectAttr ":miDefaultOptions.msg" ":mentalrayGlobals.opt";
connectAttr ":miDefaultFramebuffer.msg" ":mentalrayGlobals.fb";
connectAttr "Animations_v3_Map1.oc" "Animations_v3_business05_f_highpolyPhong.c"
		;
connectAttr "Animations_v3_business05_f_highpolyPhong.oc" "Animations_v3_business05_f_highpolySG.ss"
		;
connectAttr "Animations_v3_business05_f_highpolySG.msg" "Animations_v3_materialInfo1.sg"
		;
connectAttr "Animations_v3_business05_f_highpolyPhong.msg" "Animations_v3_materialInfo1.m"
		;
connectAttr "Animations_v3_Map1.msg" "Animations_v3_materialInfo1.t" -na;
connectAttr "Animations_v3_business05_f_highpolyPhong.oc" "Animations_v3_business05_f_highpolySG1.ss"
		;
connectAttr "Animations_v3_business05_f_highpolySG1.msg" "Animations_v3_materialInfo2.sg"
		;
connectAttr "Animations_v3_business05_f_highpolyPhong.msg" "Animations_v3_materialInfo2.m"
		;
connectAttr "Animations_v3_Map1.msg" "Animations_v3_materialInfo2.t" -na;
connectAttr "Animations_v3_place2dTexture1.o" "Animations_v3_Map1.uv";
connectAttr "Animations_v3_place2dTexture1.ofu" "Animations_v3_Map1.ofu";
connectAttr "Animations_v3_place2dTexture1.ofv" "Animations_v3_Map1.ofv";
connectAttr "Animations_v3_place2dTexture1.rf" "Animations_v3_Map1.rf";
connectAttr "Animations_v3_place2dTexture1.reu" "Animations_v3_Map1.reu";
connectAttr "Animations_v3_place2dTexture1.rev" "Animations_v3_Map1.rev";
connectAttr "Animations_v3_place2dTexture1.vt1" "Animations_v3_Map1.vt1";
connectAttr "Animations_v3_place2dTexture1.vt2" "Animations_v3_Map1.vt2";
connectAttr "Animations_v3_place2dTexture1.vt3" "Animations_v3_Map1.vt3";
connectAttr "Animations_v3_place2dTexture1.vc1" "Animations_v3_Map1.vc1";
connectAttr "Animations_v3_place2dTexture1.ofs" "Animations_v3_Map1.fs";
connectAttr "Animations_v3_business05_f_highpoly_hairPhong.oc" "Animations_v3_business05_f_highpoly_hairSG.ss"
		;
connectAttr "Animations_v3_business05_f_highpoly_hairSG.msg" "Animations_v3_materialInfo3.sg"
		;
connectAttr "Animations_v3_business05_f_highpoly_hairPhong.msg" "Animations_v3_materialInfo3.m"
		;
connectAttr "Animations_v3_business05_f_highpoly_hairPhong.oc" "Animations_v3_business05_f_highpoly_hairSG1.ss"
		;
connectAttr "Animations_v3_business05_f_highpoly_hairSG1.msg" "Animations_v3_materialInfo4.sg"
		;
connectAttr "Animations_v3_business05_f_highpoly_hairPhong.msg" "Animations_v3_materialInfo4.m"
		;
connectAttr "Jessica_Take_2_2_Map1.oc" "Jessica_Take_2_2_business05_f_highpolyPhong.c"
		;
connectAttr "Jessica_Take_2_2_business05_f_highpolyPhong.oc" "Jessica_Take_2_2_business05_f_highpolySG.ss"
		;
connectAttr "Jessica_Take_2_2_business05_f_highpolySG.msg" "Jessica_Take_2_2_materialInfo1.sg"
		;
connectAttr "Jessica_Take_2_2_business05_f_highpolyPhong.msg" "Jessica_Take_2_2_materialInfo1.m"
		;
connectAttr "Jessica_Take_2_2_Map1.msg" "Jessica_Take_2_2_materialInfo1.t" -na;
connectAttr "Jessica_Take_2_2_business05_f_highpolyPhong.oc" "Jessica_Take_2_2_business05_f_highpolySG1.ss"
		;
connectAttr "Jessica_Take_2_2_business05_f_highpolySG1.msg" "Jessica_Take_2_2_materialInfo2.sg"
		;
connectAttr "Jessica_Take_2_2_business05_f_highpolyPhong.msg" "Jessica_Take_2_2_materialInfo2.m"
		;
connectAttr "Jessica_Take_2_2_Map1.msg" "Jessica_Take_2_2_materialInfo2.t" -na;
connectAttr "Jessica_Take_2_2_place2dTexture1.o" "Jessica_Take_2_2_Map1.uv";
connectAttr "Jessica_Take_2_2_place2dTexture1.ofu" "Jessica_Take_2_2_Map1.ofu";
connectAttr "Jessica_Take_2_2_place2dTexture1.ofv" "Jessica_Take_2_2_Map1.ofv";
connectAttr "Jessica_Take_2_2_place2dTexture1.rf" "Jessica_Take_2_2_Map1.rf";
connectAttr "Jessica_Take_2_2_place2dTexture1.reu" "Jessica_Take_2_2_Map1.reu";
connectAttr "Jessica_Take_2_2_place2dTexture1.rev" "Jessica_Take_2_2_Map1.rev";
connectAttr "Jessica_Take_2_2_place2dTexture1.vt1" "Jessica_Take_2_2_Map1.vt1";
connectAttr "Jessica_Take_2_2_place2dTexture1.vt2" "Jessica_Take_2_2_Map1.vt2";
connectAttr "Jessica_Take_2_2_place2dTexture1.vt3" "Jessica_Take_2_2_Map1.vt3";
connectAttr "Jessica_Take_2_2_place2dTexture1.vc1" "Jessica_Take_2_2_Map1.vc1";
connectAttr "Jessica_Take_2_2_place2dTexture1.ofs" "Jessica_Take_2_2_Map1.fs";
connectAttr "Jessica_Take_2_2_business05_f_highpoly_hairPhong.oc" "Jessica_Take_2_2_business05_f_highpoly_hairSG.ss"
		;
connectAttr "Jessica_Take_2_2_business05_f_highpoly_hairSG.msg" "Jessica_Take_2_2_materialInfo3.sg"
		;
connectAttr "Jessica_Take_2_2_business05_f_highpoly_hairPhong.msg" "Jessica_Take_2_2_materialInfo3.m"
		;
connectAttr "Jessica_Take_2_2_business05_f_highpoly_hairPhong.oc" "Jessica_Take_2_2_business05_f_highpoly_hairSG1.ss"
		;
connectAttr "Jessica_Take_2_2_business05_f_highpoly_hairSG1.msg" "Jessica_Take_2_2_materialInfo4.sg"
		;
connectAttr "Jessica_Take_2_2_business05_f_highpoly_hairPhong.msg" "Jessica_Take_2_2_materialInfo4.m"
		;
connectAttr "Jessica_Take_2_2_Map2.oc" "Jessica_Take_2_2_business05_f_highpolyPhong1.c"
		;
connectAttr "Jessica_Take_2_2_business05_f_highpolyPhong1.oc" "Jessica_Take_2_2_business05_f_highpolySG2.ss"
		;
connectAttr "Jessica_Take_2_2_business05_f_highpolySG2.msg" "Jessica_Take_2_2_materialInfo5.sg"
		;
connectAttr "Jessica_Take_2_2_business05_f_highpolyPhong1.msg" "Jessica_Take_2_2_materialInfo5.m"
		;
connectAttr "Jessica_Take_2_2_Map2.msg" "Jessica_Take_2_2_materialInfo5.t" -na;
connectAttr "Jessica_Take_2_2_business05_f_highpolyPhong1.oc" "Jessica_Take_2_2_business05_f_highpolySG3.ss"
		;
connectAttr "Jessica_Take_2_2_business05_f_highpolySG3.msg" "Jessica_Take_2_2_materialInfo6.sg"
		;
connectAttr "Jessica_Take_2_2_business05_f_highpolyPhong1.msg" "Jessica_Take_2_2_materialInfo6.m"
		;
connectAttr "Jessica_Take_2_2_Map2.msg" "Jessica_Take_2_2_materialInfo6.t" -na;
connectAttr "Jessica_Take_2_2_place2dTexture2.o" "Jessica_Take_2_2_Map2.uv";
connectAttr "Jessica_Take_2_2_place2dTexture2.ofu" "Jessica_Take_2_2_Map2.ofu";
connectAttr "Jessica_Take_2_2_place2dTexture2.ofv" "Jessica_Take_2_2_Map2.ofv";
connectAttr "Jessica_Take_2_2_place2dTexture2.rf" "Jessica_Take_2_2_Map2.rf";
connectAttr "Jessica_Take_2_2_place2dTexture2.reu" "Jessica_Take_2_2_Map2.reu";
connectAttr "Jessica_Take_2_2_place2dTexture2.rev" "Jessica_Take_2_2_Map2.rev";
connectAttr "Jessica_Take_2_2_place2dTexture2.vt1" "Jessica_Take_2_2_Map2.vt1";
connectAttr "Jessica_Take_2_2_place2dTexture2.vt2" "Jessica_Take_2_2_Map2.vt2";
connectAttr "Jessica_Take_2_2_place2dTexture2.vt3" "Jessica_Take_2_2_Map2.vt3";
connectAttr "Jessica_Take_2_2_place2dTexture2.vc1" "Jessica_Take_2_2_Map2.vc1";
connectAttr "Jessica_Take_2_2_place2dTexture2.ofs" "Jessica_Take_2_2_Map2.fs";
connectAttr "Jessica_Take_2_2_business05_f_highpoly_hairPhong1.oc" "Jessica_Take_2_2_business05_f_highpoly_hairSG2.ss"
		;
connectAttr "Jessica_Take_2_2_business05_f_highpoly_hairSG2.msg" "Jessica_Take_2_2_materialInfo7.sg"
		;
connectAttr "Jessica_Take_2_2_business05_f_highpoly_hairPhong1.msg" "Jessica_Take_2_2_materialInfo7.m"
		;
connectAttr "Jessica_Take_2_2_business05_f_highpoly_hairPhong1.oc" "Jessica_Take_2_2_business05_f_highpoly_hairSG3.ss"
		;
connectAttr "Jessica_Take_2_2_business05_f_highpoly_hairSG3.msg" "Jessica_Take_2_2_materialInfo8.sg"
		;
connectAttr "Jessica_Take_2_2_business05_f_highpoly_hairPhong1.msg" "Jessica_Take_2_2_materialInfo8.m"
		;
connectAttr "Jessica_Take_2_2_Map3.oc" "Jessica_Take_2_2_business05_f_highpolyPhong2.c"
		;
connectAttr "Jessica_Take_2_2_business05_f_highpolyPhong2.oc" "Jessica_Take_2_2_business05_f_highpolySG4.ss"
		;
connectAttr "Jessica_Take_2_2_business05_f_highpolySG4.msg" "Jessica_Take_2_2_materialInfo9.sg"
		;
connectAttr "Jessica_Take_2_2_business05_f_highpolyPhong2.msg" "Jessica_Take_2_2_materialInfo9.m"
		;
connectAttr "Jessica_Take_2_2_Map3.msg" "Jessica_Take_2_2_materialInfo9.t" -na;
connectAttr "Jessica_Take_2_2_business05_f_highpolyPhong2.oc" "Jessica_Take_2_2_business05_f_highpolySG5.ss"
		;
connectAttr "Jessica_Take_2_2_business05_f_highpolySG5.msg" "Jessica_Take_2_2_materialInfo10.sg"
		;
connectAttr "Jessica_Take_2_2_business05_f_highpolyPhong2.msg" "Jessica_Take_2_2_materialInfo10.m"
		;
connectAttr "Jessica_Take_2_2_Map3.msg" "Jessica_Take_2_2_materialInfo10.t" -na;
connectAttr "Jessica_Take_2_2_place2dTexture3.o" "Jessica_Take_2_2_Map3.uv";
connectAttr "Jessica_Take_2_2_place2dTexture3.ofu" "Jessica_Take_2_2_Map3.ofu";
connectAttr "Jessica_Take_2_2_place2dTexture3.ofv" "Jessica_Take_2_2_Map3.ofv";
connectAttr "Jessica_Take_2_2_place2dTexture3.rf" "Jessica_Take_2_2_Map3.rf";
connectAttr "Jessica_Take_2_2_place2dTexture3.reu" "Jessica_Take_2_2_Map3.reu";
connectAttr "Jessica_Take_2_2_place2dTexture3.rev" "Jessica_Take_2_2_Map3.rev";
connectAttr "Jessica_Take_2_2_place2dTexture3.vt1" "Jessica_Take_2_2_Map3.vt1";
connectAttr "Jessica_Take_2_2_place2dTexture3.vt2" "Jessica_Take_2_2_Map3.vt2";
connectAttr "Jessica_Take_2_2_place2dTexture3.vt3" "Jessica_Take_2_2_Map3.vt3";
connectAttr "Jessica_Take_2_2_place2dTexture3.vc1" "Jessica_Take_2_2_Map3.vc1";
connectAttr "Jessica_Take_2_2_place2dTexture3.ofs" "Jessica_Take_2_2_Map3.fs";
connectAttr "Jessica_Take_2_2_business05_f_highpoly_hairPhong2.oc" "Jessica_Take_2_2_business05_f_highpoly_hairSG4.ss"
		;
connectAttr "Jessica_Take_2_2_business05_f_highpoly_hairSG4.msg" "Jessica_Take_2_2_materialInfo11.sg"
		;
connectAttr "Jessica_Take_2_2_business05_f_highpoly_hairPhong2.msg" "Jessica_Take_2_2_materialInfo11.m"
		;
connectAttr "Jessica_Take_2_2_business05_f_highpoly_hairPhong2.oc" "Jessica_Take_2_2_business05_f_highpoly_hairSG5.ss"
		;
connectAttr "Jessica_Take_2_2_business05_f_highpoly_hairSG5.msg" "Jessica_Take_2_2_materialInfo12.sg"
		;
connectAttr "Jessica_Take_2_2_business05_f_highpoly_hairPhong2.msg" "Jessica_Take_2_2_materialInfo12.m"
		;
connectAttr "Jessica_Take_2_2_Map4.oc" "Jessica_Take_2_2_business05_f_highpolyPhong3.c"
		;
connectAttr "Jessica_Take_2_2_business05_f_highpolyPhong3.oc" "Jessica_Take_2_2_business05_f_highpolySG6.ss"
		;
connectAttr "Jessica_Take_2_2_business05_f_highpolySG6.msg" "Jessica_Take_2_2_materialInfo13.sg"
		;
connectAttr "Jessica_Take_2_2_business05_f_highpolyPhong3.msg" "Jessica_Take_2_2_materialInfo13.m"
		;
connectAttr "Jessica_Take_2_2_Map4.msg" "Jessica_Take_2_2_materialInfo13.t" -na;
connectAttr "Jessica_Take_2_2_business05_f_highpolyPhong3.oc" "Jessica_Take_2_2_business05_f_highpolySG7.ss"
		;
connectAttr "Jessica_Take_2_2_business05_f_highpolySG7.msg" "Jessica_Take_2_2_materialInfo14.sg"
		;
connectAttr "Jessica_Take_2_2_business05_f_highpolyPhong3.msg" "Jessica_Take_2_2_materialInfo14.m"
		;
connectAttr "Jessica_Take_2_2_Map4.msg" "Jessica_Take_2_2_materialInfo14.t" -na;
connectAttr "Jessica_Take_2_2_place2dTexture4.o" "Jessica_Take_2_2_Map4.uv";
connectAttr "Jessica_Take_2_2_place2dTexture4.ofu" "Jessica_Take_2_2_Map4.ofu";
connectAttr "Jessica_Take_2_2_place2dTexture4.ofv" "Jessica_Take_2_2_Map4.ofv";
connectAttr "Jessica_Take_2_2_place2dTexture4.rf" "Jessica_Take_2_2_Map4.rf";
connectAttr "Jessica_Take_2_2_place2dTexture4.reu" "Jessica_Take_2_2_Map4.reu";
connectAttr "Jessica_Take_2_2_place2dTexture4.rev" "Jessica_Take_2_2_Map4.rev";
connectAttr "Jessica_Take_2_2_place2dTexture4.vt1" "Jessica_Take_2_2_Map4.vt1";
connectAttr "Jessica_Take_2_2_place2dTexture4.vt2" "Jessica_Take_2_2_Map4.vt2";
connectAttr "Jessica_Take_2_2_place2dTexture4.vt3" "Jessica_Take_2_2_Map4.vt3";
connectAttr "Jessica_Take_2_2_place2dTexture4.vc1" "Jessica_Take_2_2_Map4.vc1";
connectAttr "Jessica_Take_2_2_place2dTexture4.ofs" "Jessica_Take_2_2_Map4.fs";
connectAttr "Jessica_Take_2_2_business05_f_highpoly_hairPhong3.oc" "Jessica_Take_2_2_business05_f_highpoly_hairSG6.ss"
		;
connectAttr "Jessica_Take_2_2_business05_f_highpoly_hairSG6.msg" "Jessica_Take_2_2_materialInfo15.sg"
		;
connectAttr "Jessica_Take_2_2_business05_f_highpoly_hairPhong3.msg" "Jessica_Take_2_2_materialInfo15.m"
		;
connectAttr "Jessica_Take_2_2_business05_f_highpoly_hairPhong3.oc" "Jessica_Take_2_2_business05_f_highpoly_hairSG7.ss"
		;
connectAttr "Jessica_Take_2_2_business05_f_highpoly_hairSG7.msg" "Jessica_Take_2_2_materialInfo16.sg"
		;
connectAttr "Jessica_Take_2_2_business05_f_highpoly_hairPhong3.msg" "Jessica_Take_2_2_materialInfo16.m"
		;
connectAttr "Jessica_Take_2_2_Map5.oc" "Jessica_Take_2_2_business05_f_highpolyPhong4.c"
		;
connectAttr "Jessica_Take_2_2_business05_f_highpolyPhong4.oc" "Jessica_Take_2_2_business05_f_highpolySG8.ss"
		;
connectAttr "Jessica_Take_2_2_business05_f_highpolySG8.msg" "Jessica_Take_2_2_materialInfo17.sg"
		;
connectAttr "Jessica_Take_2_2_business05_f_highpolyPhong4.msg" "Jessica_Take_2_2_materialInfo17.m"
		;
connectAttr "Jessica_Take_2_2_Map5.msg" "Jessica_Take_2_2_materialInfo17.t" -na;
connectAttr "Jessica_Take_2_2_business05_f_highpolyPhong4.oc" "Jessica_Take_2_2_business05_f_highpolySG9.ss"
		;
connectAttr "Jessica_Take_2_2_business05_f_highpolySG9.msg" "Jessica_Take_2_2_materialInfo18.sg"
		;
connectAttr "Jessica_Take_2_2_business05_f_highpolyPhong4.msg" "Jessica_Take_2_2_materialInfo18.m"
		;
connectAttr "Jessica_Take_2_2_Map5.msg" "Jessica_Take_2_2_materialInfo18.t" -na;
connectAttr "Jessica_Take_2_2_place2dTexture5.o" "Jessica_Take_2_2_Map5.uv";
connectAttr "Jessica_Take_2_2_place2dTexture5.ofu" "Jessica_Take_2_2_Map5.ofu";
connectAttr "Jessica_Take_2_2_place2dTexture5.ofv" "Jessica_Take_2_2_Map5.ofv";
connectAttr "Jessica_Take_2_2_place2dTexture5.rf" "Jessica_Take_2_2_Map5.rf";
connectAttr "Jessica_Take_2_2_place2dTexture5.reu" "Jessica_Take_2_2_Map5.reu";
connectAttr "Jessica_Take_2_2_place2dTexture5.rev" "Jessica_Take_2_2_Map5.rev";
connectAttr "Jessica_Take_2_2_place2dTexture5.vt1" "Jessica_Take_2_2_Map5.vt1";
connectAttr "Jessica_Take_2_2_place2dTexture5.vt2" "Jessica_Take_2_2_Map5.vt2";
connectAttr "Jessica_Take_2_2_place2dTexture5.vt3" "Jessica_Take_2_2_Map5.vt3";
connectAttr "Jessica_Take_2_2_place2dTexture5.vc1" "Jessica_Take_2_2_Map5.vc1";
connectAttr "Jessica_Take_2_2_place2dTexture5.ofs" "Jessica_Take_2_2_Map5.fs";
connectAttr "Jessica_Take_2_2_business05_f_highpoly_hairPhong4.oc" "Jessica_Take_2_2_business05_f_highpoly_hairSG8.ss"
		;
connectAttr "Jessica_Take_2_2_business05_f_highpoly_hairSG8.msg" "Jessica_Take_2_2_materialInfo19.sg"
		;
connectAttr "Jessica_Take_2_2_business05_f_highpoly_hairPhong4.msg" "Jessica_Take_2_2_materialInfo19.m"
		;
connectAttr "Jessica_Take_2_2_business05_f_highpoly_hairPhong4.oc" "Jessica_Take_2_2_business05_f_highpoly_hairSG9.ss"
		;
connectAttr "Jessica_Take_2_2_business05_f_highpoly_hairSG9.msg" "Jessica_Take_2_2_materialInfo20.sg"
		;
connectAttr "Jessica_Take_2_2_business05_f_highpoly_hairPhong4.msg" "Jessica_Take_2_2_materialInfo20.m"
		;
connectAttr "Jessica_Take_2_2_Map6.oc" "Jessica_Take_2_2_business05_f_highpolyPhong5.c"
		;
connectAttr "Jessica_Take_2_2_business05_f_highpolyPhong5.oc" "Jessica_Take_2_2_business05_f_highpolySG10.ss"
		;
connectAttr "Jessica_Take_2_2_business05_f_highpolySG10.msg" "Jessica_Take_2_2_materialInfo21.sg"
		;
connectAttr "Jessica_Take_2_2_business05_f_highpolyPhong5.msg" "Jessica_Take_2_2_materialInfo21.m"
		;
connectAttr "Jessica_Take_2_2_Map6.msg" "Jessica_Take_2_2_materialInfo21.t" -na;
connectAttr "Jessica_Take_2_2_business05_f_highpolyPhong5.oc" "Jessica_Take_2_2_business05_f_highpolySG11.ss"
		;
connectAttr "Jessica_Take_2_2_business05_f_highpolySG11.msg" "Jessica_Take_2_2_materialInfo22.sg"
		;
connectAttr "Jessica_Take_2_2_business05_f_highpolyPhong5.msg" "Jessica_Take_2_2_materialInfo22.m"
		;
connectAttr "Jessica_Take_2_2_Map6.msg" "Jessica_Take_2_2_materialInfo22.t" -na;
connectAttr "Jessica_Take_2_2_place2dTexture6.o" "Jessica_Take_2_2_Map6.uv";
connectAttr "Jessica_Take_2_2_place2dTexture6.ofu" "Jessica_Take_2_2_Map6.ofu";
connectAttr "Jessica_Take_2_2_place2dTexture6.ofv" "Jessica_Take_2_2_Map6.ofv";
connectAttr "Jessica_Take_2_2_place2dTexture6.rf" "Jessica_Take_2_2_Map6.rf";
connectAttr "Jessica_Take_2_2_place2dTexture6.reu" "Jessica_Take_2_2_Map6.reu";
connectAttr "Jessica_Take_2_2_place2dTexture6.rev" "Jessica_Take_2_2_Map6.rev";
connectAttr "Jessica_Take_2_2_place2dTexture6.vt1" "Jessica_Take_2_2_Map6.vt1";
connectAttr "Jessica_Take_2_2_place2dTexture6.vt2" "Jessica_Take_2_2_Map6.vt2";
connectAttr "Jessica_Take_2_2_place2dTexture6.vt3" "Jessica_Take_2_2_Map6.vt3";
connectAttr "Jessica_Take_2_2_place2dTexture6.vc1" "Jessica_Take_2_2_Map6.vc1";
connectAttr "Jessica_Take_2_2_place2dTexture6.ofs" "Jessica_Take_2_2_Map6.fs";
connectAttr "Jessica_Take_2_2_business05_f_highpoly_hairPhong5.oc" "Jessica_Take_2_2_business05_f_highpoly_hairSG10.ss"
		;
connectAttr "Jessica_Take_2_2_business05_f_highpoly_hairSG10.msg" "Jessica_Take_2_2_materialInfo23.sg"
		;
connectAttr "Jessica_Take_2_2_business05_f_highpoly_hairPhong5.msg" "Jessica_Take_2_2_materialInfo23.m"
		;
connectAttr "Jessica_Take_2_2_business05_f_highpoly_hairPhong5.oc" "Jessica_Take_2_2_business05_f_highpoly_hairSG11.ss"
		;
connectAttr "Jessica_Take_2_2_business05_f_highpoly_hairSG11.msg" "Jessica_Take_2_2_materialInfo24.sg"
		;
connectAttr "Jessica_Take_2_2_business05_f_highpoly_hairPhong5.msg" "Jessica_Take_2_2_materialInfo24.m"
		;
connectAttr "Candice_Take_1_Map1.oc" "Candice_Take_1_business05_f_highpolyPhong.c"
		;
connectAttr "Candice_Take_1_business05_f_highpolyPhong.oc" "Candice_Take_1_business05_f_highpolySG.ss"
		;
connectAttr "Candice_Take_1_business05_f_highpolySG.msg" "Candice_Take_1_materialInfo1.sg"
		;
connectAttr "Candice_Take_1_business05_f_highpolyPhong.msg" "Candice_Take_1_materialInfo1.m"
		;
connectAttr "Candice_Take_1_Map1.msg" "Candice_Take_1_materialInfo1.t" -na;
connectAttr "Candice_Take_1_business05_f_highpolyPhong.oc" "Candice_Take_1_business05_f_highpolySG1.ss"
		;
connectAttr "Candice_Take_1_business05_f_highpolySG1.msg" "Candice_Take_1_materialInfo2.sg"
		;
connectAttr "Candice_Take_1_business05_f_highpolyPhong.msg" "Candice_Take_1_materialInfo2.m"
		;
connectAttr "Candice_Take_1_Map1.msg" "Candice_Take_1_materialInfo2.t" -na;
connectAttr "Candice_Take_1_place2dTexture1.o" "Candice_Take_1_Map1.uv";
connectAttr "Candice_Take_1_place2dTexture1.ofu" "Candice_Take_1_Map1.ofu";
connectAttr "Candice_Take_1_place2dTexture1.ofv" "Candice_Take_1_Map1.ofv";
connectAttr "Candice_Take_1_place2dTexture1.rf" "Candice_Take_1_Map1.rf";
connectAttr "Candice_Take_1_place2dTexture1.reu" "Candice_Take_1_Map1.reu";
connectAttr "Candice_Take_1_place2dTexture1.rev" "Candice_Take_1_Map1.rev";
connectAttr "Candice_Take_1_place2dTexture1.vt1" "Candice_Take_1_Map1.vt1";
connectAttr "Candice_Take_1_place2dTexture1.vt2" "Candice_Take_1_Map1.vt2";
connectAttr "Candice_Take_1_place2dTexture1.vt3" "Candice_Take_1_Map1.vt3";
connectAttr "Candice_Take_1_place2dTexture1.vc1" "Candice_Take_1_Map1.vc1";
connectAttr "Candice_Take_1_place2dTexture1.ofs" "Candice_Take_1_Map1.fs";
connectAttr "Candice_Take_1_business05_f_highpoly_hairPhong.oc" "Candice_Take_1_business05_f_highpoly_hairSG.ss"
		;
connectAttr "Candice_Take_1_business05_f_highpoly_hairSG.msg" "Candice_Take_1_materialInfo3.sg"
		;
connectAttr "Candice_Take_1_business05_f_highpoly_hairPhong.msg" "Candice_Take_1_materialInfo3.m"
		;
connectAttr "Candice_Take_1_business05_f_highpoly_hairPhong.oc" "Candice_Take_1_business05_f_highpoly_hairSG1.ss"
		;
connectAttr "Candice_Take_1_business05_f_highpoly_hairSG1.msg" "Candice_Take_1_materialInfo4.sg"
		;
connectAttr "Candice_Take_1_business05_f_highpoly_hairPhong.msg" "Candice_Take_1_materialInfo4.m"
		;
connectAttr "Candice_Take_1_Map2.oc" "Candice_Take_1_business05_f_highpolyPhong1.c"
		;
connectAttr "Candice_Take_1_business05_f_highpolyPhong1.oc" "Candice_Take_1_business05_f_highpolySG2.ss"
		;
connectAttr "Candice_Take_1_business05_f_highpolySG2.msg" "Candice_Take_1_materialInfo5.sg"
		;
connectAttr "Candice_Take_1_business05_f_highpolyPhong1.msg" "Candice_Take_1_materialInfo5.m"
		;
connectAttr "Candice_Take_1_Map2.msg" "Candice_Take_1_materialInfo5.t" -na;
connectAttr "Candice_Take_1_business05_f_highpolyPhong1.oc" "Candice_Take_1_business05_f_highpolySG3.ss"
		;
connectAttr "Candice_Take_1_business05_f_highpolySG3.msg" "Candice_Take_1_materialInfo6.sg"
		;
connectAttr "Candice_Take_1_business05_f_highpolyPhong1.msg" "Candice_Take_1_materialInfo6.m"
		;
connectAttr "Candice_Take_1_Map2.msg" "Candice_Take_1_materialInfo6.t" -na;
connectAttr "Candice_Take_1_place2dTexture2.o" "Candice_Take_1_Map2.uv";
connectAttr "Candice_Take_1_place2dTexture2.ofu" "Candice_Take_1_Map2.ofu";
connectAttr "Candice_Take_1_place2dTexture2.ofv" "Candice_Take_1_Map2.ofv";
connectAttr "Candice_Take_1_place2dTexture2.rf" "Candice_Take_1_Map2.rf";
connectAttr "Candice_Take_1_place2dTexture2.reu" "Candice_Take_1_Map2.reu";
connectAttr "Candice_Take_1_place2dTexture2.rev" "Candice_Take_1_Map2.rev";
connectAttr "Candice_Take_1_place2dTexture2.vt1" "Candice_Take_1_Map2.vt1";
connectAttr "Candice_Take_1_place2dTexture2.vt2" "Candice_Take_1_Map2.vt2";
connectAttr "Candice_Take_1_place2dTexture2.vt3" "Candice_Take_1_Map2.vt3";
connectAttr "Candice_Take_1_place2dTexture2.vc1" "Candice_Take_1_Map2.vc1";
connectAttr "Candice_Take_1_place2dTexture2.ofs" "Candice_Take_1_Map2.fs";
connectAttr "Candice_Take_1_business05_f_highpoly_hairPhong1.oc" "Candice_Take_1_business05_f_highpoly_hairSG2.ss"
		;
connectAttr "Candice_Take_1_business05_f_highpoly_hairSG2.msg" "Candice_Take_1_materialInfo7.sg"
		;
connectAttr "Candice_Take_1_business05_f_highpoly_hairPhong1.msg" "Candice_Take_1_materialInfo7.m"
		;
connectAttr "Candice_Take_1_business05_f_highpoly_hairPhong1.oc" "Candice_Take_1_business05_f_highpoly_hairSG3.ss"
		;
connectAttr "Candice_Take_1_business05_f_highpoly_hairSG3.msg" "Candice_Take_1_materialInfo8.sg"
		;
connectAttr "Candice_Take_1_business05_f_highpoly_hairPhong1.msg" "Candice_Take_1_materialInfo8.m"
		;
connectAttr "Candice_Take_1_Map3.oc" "Candice_Take_1_business05_f_highpolyPhong2.c"
		;
connectAttr "Candice_Take_1_business05_f_highpolyPhong2.oc" "Candice_Take_1_business05_f_highpolySG4.ss"
		;
connectAttr "Candice_Take_1_business05_f_highpolySG4.msg" "Candice_Take_1_materialInfo9.sg"
		;
connectAttr "Candice_Take_1_business05_f_highpolyPhong2.msg" "Candice_Take_1_materialInfo9.m"
		;
connectAttr "Candice_Take_1_Map3.msg" "Candice_Take_1_materialInfo9.t" -na;
connectAttr "Candice_Take_1_business05_f_highpolyPhong2.oc" "Candice_Take_1_business05_f_highpolySG5.ss"
		;
connectAttr "Candice_Take_1_business05_f_highpolySG5.msg" "Candice_Take_1_materialInfo10.sg"
		;
connectAttr "Candice_Take_1_business05_f_highpolyPhong2.msg" "Candice_Take_1_materialInfo10.m"
		;
connectAttr "Candice_Take_1_Map3.msg" "Candice_Take_1_materialInfo10.t" -na;
connectAttr "Candice_Take_1_place2dTexture3.o" "Candice_Take_1_Map3.uv";
connectAttr "Candice_Take_1_place2dTexture3.ofu" "Candice_Take_1_Map3.ofu";
connectAttr "Candice_Take_1_place2dTexture3.ofv" "Candice_Take_1_Map3.ofv";
connectAttr "Candice_Take_1_place2dTexture3.rf" "Candice_Take_1_Map3.rf";
connectAttr "Candice_Take_1_place2dTexture3.reu" "Candice_Take_1_Map3.reu";
connectAttr "Candice_Take_1_place2dTexture3.rev" "Candice_Take_1_Map3.rev";
connectAttr "Candice_Take_1_place2dTexture3.vt1" "Candice_Take_1_Map3.vt1";
connectAttr "Candice_Take_1_place2dTexture3.vt2" "Candice_Take_1_Map3.vt2";
connectAttr "Candice_Take_1_place2dTexture3.vt3" "Candice_Take_1_Map3.vt3";
connectAttr "Candice_Take_1_place2dTexture3.vc1" "Candice_Take_1_Map3.vc1";
connectAttr "Candice_Take_1_place2dTexture3.ofs" "Candice_Take_1_Map3.fs";
connectAttr "Candice_Take_1_business05_f_highpoly_hairPhong2.oc" "Candice_Take_1_business05_f_highpoly_hairSG4.ss"
		;
connectAttr "Candice_Take_1_business05_f_highpoly_hairSG4.msg" "Candice_Take_1_materialInfo11.sg"
		;
connectAttr "Candice_Take_1_business05_f_highpoly_hairPhong2.msg" "Candice_Take_1_materialInfo11.m"
		;
connectAttr "Candice_Take_1_business05_f_highpoly_hairPhong2.oc" "Candice_Take_1_business05_f_highpoly_hairSG5.ss"
		;
connectAttr "Candice_Take_1_business05_f_highpoly_hairSG5.msg" "Candice_Take_1_materialInfo12.sg"
		;
connectAttr "Candice_Take_1_business05_f_highpoly_hairPhong2.msg" "Candice_Take_1_materialInfo12.m"
		;
connectAttr "Candice_Take_1_Map4.oc" "Candice_Take_1_business05_f_highpolyPhong3.c"
		;
connectAttr "Candice_Take_1_business05_f_highpolyPhong3.oc" "Candice_Take_1_business05_f_highpolySG6.ss"
		;
connectAttr "Candice_Take_1_business05_f_highpolySG6.msg" "Candice_Take_1_materialInfo13.sg"
		;
connectAttr "Candice_Take_1_business05_f_highpolyPhong3.msg" "Candice_Take_1_materialInfo13.m"
		;
connectAttr "Candice_Take_1_Map4.msg" "Candice_Take_1_materialInfo13.t" -na;
connectAttr "Candice_Take_1_business05_f_highpolyPhong3.oc" "Candice_Take_1_business05_f_highpolySG7.ss"
		;
connectAttr "Candice_Take_1_business05_f_highpolySG7.msg" "Candice_Take_1_materialInfo14.sg"
		;
connectAttr "Candice_Take_1_business05_f_highpolyPhong3.msg" "Candice_Take_1_materialInfo14.m"
		;
connectAttr "Candice_Take_1_Map4.msg" "Candice_Take_1_materialInfo14.t" -na;
connectAttr "Candice_Take_1_place2dTexture4.o" "Candice_Take_1_Map4.uv";
connectAttr "Candice_Take_1_place2dTexture4.ofu" "Candice_Take_1_Map4.ofu";
connectAttr "Candice_Take_1_place2dTexture4.ofv" "Candice_Take_1_Map4.ofv";
connectAttr "Candice_Take_1_place2dTexture4.rf" "Candice_Take_1_Map4.rf";
connectAttr "Candice_Take_1_place2dTexture4.reu" "Candice_Take_1_Map4.reu";
connectAttr "Candice_Take_1_place2dTexture4.rev" "Candice_Take_1_Map4.rev";
connectAttr "Candice_Take_1_place2dTexture4.vt1" "Candice_Take_1_Map4.vt1";
connectAttr "Candice_Take_1_place2dTexture4.vt2" "Candice_Take_1_Map4.vt2";
connectAttr "Candice_Take_1_place2dTexture4.vt3" "Candice_Take_1_Map4.vt3";
connectAttr "Candice_Take_1_place2dTexture4.vc1" "Candice_Take_1_Map4.vc1";
connectAttr "Candice_Take_1_place2dTexture4.ofs" "Candice_Take_1_Map4.fs";
connectAttr "Candice_Take_1_business05_f_highpoly_hairPhong3.oc" "Candice_Take_1_business05_f_highpoly_hairSG6.ss"
		;
connectAttr "Candice_Take_1_business05_f_highpoly_hairSG6.msg" "Candice_Take_1_materialInfo15.sg"
		;
connectAttr "Candice_Take_1_business05_f_highpoly_hairPhong3.msg" "Candice_Take_1_materialInfo15.m"
		;
connectAttr "Candice_Take_1_business05_f_highpoly_hairPhong3.oc" "Candice_Take_1_business05_f_highpoly_hairSG7.ss"
		;
connectAttr "Candice_Take_1_business05_f_highpoly_hairSG7.msg" "Candice_Take_1_materialInfo16.sg"
		;
connectAttr "Candice_Take_1_business05_f_highpoly_hairPhong3.msg" "Candice_Take_1_materialInfo16.m"
		;
connectAttr "Candice_Take_1_Map5.oc" "Candice_Take_1_business05_f_highpolyPhong4.c"
		;
connectAttr "Candice_Take_1_business05_f_highpolyPhong4.oc" "Candice_Take_1_business05_f_highpolySG8.ss"
		;
connectAttr "Candice_Take_1_business05_f_highpolySG8.msg" "Candice_Take_1_materialInfo17.sg"
		;
connectAttr "Candice_Take_1_business05_f_highpolyPhong4.msg" "Candice_Take_1_materialInfo17.m"
		;
connectAttr "Candice_Take_1_Map5.msg" "Candice_Take_1_materialInfo17.t" -na;
connectAttr "Candice_Take_1_business05_f_highpolyPhong4.oc" "Candice_Take_1_business05_f_highpolySG9.ss"
		;
connectAttr "Candice_Take_1_business05_f_highpolySG9.msg" "Candice_Take_1_materialInfo18.sg"
		;
connectAttr "Candice_Take_1_business05_f_highpolyPhong4.msg" "Candice_Take_1_materialInfo18.m"
		;
connectAttr "Candice_Take_1_Map5.msg" "Candice_Take_1_materialInfo18.t" -na;
connectAttr "Candice_Take_1_place2dTexture5.o" "Candice_Take_1_Map5.uv";
connectAttr "Candice_Take_1_place2dTexture5.ofu" "Candice_Take_1_Map5.ofu";
connectAttr "Candice_Take_1_place2dTexture5.ofv" "Candice_Take_1_Map5.ofv";
connectAttr "Candice_Take_1_place2dTexture5.rf" "Candice_Take_1_Map5.rf";
connectAttr "Candice_Take_1_place2dTexture5.reu" "Candice_Take_1_Map5.reu";
connectAttr "Candice_Take_1_place2dTexture5.rev" "Candice_Take_1_Map5.rev";
connectAttr "Candice_Take_1_place2dTexture5.vt1" "Candice_Take_1_Map5.vt1";
connectAttr "Candice_Take_1_place2dTexture5.vt2" "Candice_Take_1_Map5.vt2";
connectAttr "Candice_Take_1_place2dTexture5.vt3" "Candice_Take_1_Map5.vt3";
connectAttr "Candice_Take_1_place2dTexture5.vc1" "Candice_Take_1_Map5.vc1";
connectAttr "Candice_Take_1_place2dTexture5.ofs" "Candice_Take_1_Map5.fs";
connectAttr "Candice_Take_1_business05_f_highpoly_hairPhong4.oc" "Candice_Take_1_business05_f_highpoly_hairSG8.ss"
		;
connectAttr "Candice_Take_1_business05_f_highpoly_hairSG8.msg" "Candice_Take_1_materialInfo19.sg"
		;
connectAttr "Candice_Take_1_business05_f_highpoly_hairPhong4.msg" "Candice_Take_1_materialInfo19.m"
		;
connectAttr "Candice_Take_1_business05_f_highpoly_hairPhong4.oc" "Candice_Take_1_business05_f_highpoly_hairSG9.ss"
		;
connectAttr "Candice_Take_1_business05_f_highpoly_hairSG9.msg" "Candice_Take_1_materialInfo20.sg"
		;
connectAttr "Candice_Take_1_business05_f_highpoly_hairPhong4.msg" "Candice_Take_1_materialInfo20.m"
		;
connectAttr "Candice_Take_1_Map6.oc" "Candice_Take_1_business05_f_highpolyPhong5.c"
		;
connectAttr "Candice_Take_1_business05_f_highpolyPhong5.oc" "Candice_Take_1_business05_f_highpolySG10.ss"
		;
connectAttr "Candice_Take_1_business05_f_highpolySG10.msg" "Candice_Take_1_materialInfo21.sg"
		;
connectAttr "Candice_Take_1_business05_f_highpolyPhong5.msg" "Candice_Take_1_materialInfo21.m"
		;
connectAttr "Candice_Take_1_Map6.msg" "Candice_Take_1_materialInfo21.t" -na;
connectAttr "Candice_Take_1_business05_f_highpolyPhong5.oc" "Candice_Take_1_business05_f_highpolySG11.ss"
		;
connectAttr "Candice_Take_1_business05_f_highpolySG11.msg" "Candice_Take_1_materialInfo22.sg"
		;
connectAttr "Candice_Take_1_business05_f_highpolyPhong5.msg" "Candice_Take_1_materialInfo22.m"
		;
connectAttr "Candice_Take_1_Map6.msg" "Candice_Take_1_materialInfo22.t" -na;
connectAttr "Candice_Take_1_place2dTexture6.o" "Candice_Take_1_Map6.uv";
connectAttr "Candice_Take_1_place2dTexture6.ofu" "Candice_Take_1_Map6.ofu";
connectAttr "Candice_Take_1_place2dTexture6.ofv" "Candice_Take_1_Map6.ofv";
connectAttr "Candice_Take_1_place2dTexture6.rf" "Candice_Take_1_Map6.rf";
connectAttr "Candice_Take_1_place2dTexture6.reu" "Candice_Take_1_Map6.reu";
connectAttr "Candice_Take_1_place2dTexture6.rev" "Candice_Take_1_Map6.rev";
connectAttr "Candice_Take_1_place2dTexture6.vt1" "Candice_Take_1_Map6.vt1";
connectAttr "Candice_Take_1_place2dTexture6.vt2" "Candice_Take_1_Map6.vt2";
connectAttr "Candice_Take_1_place2dTexture6.vt3" "Candice_Take_1_Map6.vt3";
connectAttr "Candice_Take_1_place2dTexture6.vc1" "Candice_Take_1_Map6.vc1";
connectAttr "Candice_Take_1_place2dTexture6.ofs" "Candice_Take_1_Map6.fs";
connectAttr "Candice_Take_1_business05_f_highpoly_hairPhong5.oc" "Candice_Take_1_business05_f_highpoly_hairSG10.ss"
		;
connectAttr "Candice_Take_1_business05_f_highpoly_hairSG10.msg" "Candice_Take_1_materialInfo23.sg"
		;
connectAttr "Candice_Take_1_business05_f_highpoly_hairPhong5.msg" "Candice_Take_1_materialInfo23.m"
		;
connectAttr "Candice_Take_1_business05_f_highpoly_hairPhong5.oc" "Candice_Take_1_business05_f_highpoly_hairSG11.ss"
		;
connectAttr "Candice_Take_1_business05_f_highpoly_hairSG11.msg" "Candice_Take_1_materialInfo24.sg"
		;
connectAttr "Candice_Take_1_business05_f_highpoly_hairPhong5.msg" "Candice_Take_1_materialInfo24.m"
		;
connectAttr "Candice_Take_1_Map7.oc" "Candice_Take_1_business05_f_highpolyPhong6.c"
		;
connectAttr "Candice_Take_1_business05_f_highpolyPhong6.oc" "Candice_Take_1_business05_f_highpolySG12.ss"
		;
connectAttr "Candice_Take_1_business05_f_highpolySG12.msg" "Candice_Take_1_materialInfo25.sg"
		;
connectAttr "Candice_Take_1_business05_f_highpolyPhong6.msg" "Candice_Take_1_materialInfo25.m"
		;
connectAttr "Candice_Take_1_Map7.msg" "Candice_Take_1_materialInfo25.t" -na;
connectAttr "Candice_Take_1_business05_f_highpolyPhong6.oc" "Candice_Take_1_business05_f_highpolySG13.ss"
		;
connectAttr "Candice_Take_1_business05_f_highpolySG13.msg" "Candice_Take_1_materialInfo26.sg"
		;
connectAttr "Candice_Take_1_business05_f_highpolyPhong6.msg" "Candice_Take_1_materialInfo26.m"
		;
connectAttr "Candice_Take_1_Map7.msg" "Candice_Take_1_materialInfo26.t" -na;
connectAttr "Candice_Take_1_place2dTexture7.o" "Candice_Take_1_Map7.uv";
connectAttr "Candice_Take_1_place2dTexture7.ofu" "Candice_Take_1_Map7.ofu";
connectAttr "Candice_Take_1_place2dTexture7.ofv" "Candice_Take_1_Map7.ofv";
connectAttr "Candice_Take_1_place2dTexture7.rf" "Candice_Take_1_Map7.rf";
connectAttr "Candice_Take_1_place2dTexture7.reu" "Candice_Take_1_Map7.reu";
connectAttr "Candice_Take_1_place2dTexture7.rev" "Candice_Take_1_Map7.rev";
connectAttr "Candice_Take_1_place2dTexture7.vt1" "Candice_Take_1_Map7.vt1";
connectAttr "Candice_Take_1_place2dTexture7.vt2" "Candice_Take_1_Map7.vt2";
connectAttr "Candice_Take_1_place2dTexture7.vt3" "Candice_Take_1_Map7.vt3";
connectAttr "Candice_Take_1_place2dTexture7.vc1" "Candice_Take_1_Map7.vc1";
connectAttr "Candice_Take_1_place2dTexture7.ofs" "Candice_Take_1_Map7.fs";
connectAttr "Candice_Take_1_business05_f_highpoly_hairPhong6.oc" "Candice_Take_1_business05_f_highpoly_hairSG12.ss"
		;
connectAttr "Candice_Take_1_business05_f_highpoly_hairSG12.msg" "Candice_Take_1_materialInfo27.sg"
		;
connectAttr "Candice_Take_1_business05_f_highpoly_hairPhong6.msg" "Candice_Take_1_materialInfo27.m"
		;
connectAttr "Candice_Take_1_business05_f_highpoly_hairPhong6.oc" "Candice_Take_1_business05_f_highpoly_hairSG13.ss"
		;
connectAttr "Candice_Take_1_business05_f_highpoly_hairSG13.msg" "Candice_Take_1_materialInfo28.sg"
		;
connectAttr "Candice_Take_1_business05_f_highpoly_hairPhong6.msg" "Candice_Take_1_materialInfo28.m"
		;
connectAttr "Candice_Take_1_Map8.oc" "Candice_Take_1_business05_f_highpolyPhong7.c"
		;
connectAttr "Candice_Take_1_business05_f_highpolyPhong7.oc" "Candice_Take_1_business05_f_highpolySG14.ss"
		;
connectAttr "Candice_Take_1_business05_f_highpolySG14.msg" "Candice_Take_1_materialInfo29.sg"
		;
connectAttr "Candice_Take_1_business05_f_highpolyPhong7.msg" "Candice_Take_1_materialInfo29.m"
		;
connectAttr "Candice_Take_1_Map8.msg" "Candice_Take_1_materialInfo29.t" -na;
connectAttr "Candice_Take_1_business05_f_highpolyPhong7.oc" "Candice_Take_1_business05_f_highpolySG15.ss"
		;
connectAttr "Candice_Take_1_business05_f_highpolySG15.msg" "Candice_Take_1_materialInfo30.sg"
		;
connectAttr "Candice_Take_1_business05_f_highpolyPhong7.msg" "Candice_Take_1_materialInfo30.m"
		;
connectAttr "Candice_Take_1_Map8.msg" "Candice_Take_1_materialInfo30.t" -na;
connectAttr "Candice_Take_1_place2dTexture8.o" "Candice_Take_1_Map8.uv";
connectAttr "Candice_Take_1_place2dTexture8.ofu" "Candice_Take_1_Map8.ofu";
connectAttr "Candice_Take_1_place2dTexture8.ofv" "Candice_Take_1_Map8.ofv";
connectAttr "Candice_Take_1_place2dTexture8.rf" "Candice_Take_1_Map8.rf";
connectAttr "Candice_Take_1_place2dTexture8.reu" "Candice_Take_1_Map8.reu";
connectAttr "Candice_Take_1_place2dTexture8.rev" "Candice_Take_1_Map8.rev";
connectAttr "Candice_Take_1_place2dTexture8.vt1" "Candice_Take_1_Map8.vt1";
connectAttr "Candice_Take_1_place2dTexture8.vt2" "Candice_Take_1_Map8.vt2";
connectAttr "Candice_Take_1_place2dTexture8.vt3" "Candice_Take_1_Map8.vt3";
connectAttr "Candice_Take_1_place2dTexture8.vc1" "Candice_Take_1_Map8.vc1";
connectAttr "Candice_Take_1_place2dTexture8.ofs" "Candice_Take_1_Map8.fs";
connectAttr "Candice_Take_1_business05_f_highpoly_hairPhong7.oc" "Candice_Take_1_business05_f_highpoly_hairSG14.ss"
		;
connectAttr "Candice_Take_1_business05_f_highpoly_hairSG14.msg" "Candice_Take_1_materialInfo31.sg"
		;
connectAttr "Candice_Take_1_business05_f_highpoly_hairPhong7.msg" "Candice_Take_1_materialInfo31.m"
		;
connectAttr "Candice_Take_1_business05_f_highpoly_hairPhong7.oc" "Candice_Take_1_business05_f_highpoly_hairSG15.ss"
		;
connectAttr "Candice_Take_1_business05_f_highpoly_hairSG15.msg" "Candice_Take_1_materialInfo32.sg"
		;
connectAttr "Candice_Take_1_business05_f_highpoly_hairPhong7.msg" "Candice_Take_1_materialInfo32.m"
		;
connectAttr "Animations_v3_Map2.oc" "Animations_v3_business05_f_highpolyPhong1.c"
		;
connectAttr "Animations_v3_business05_f_highpolyPhong1.oc" "Animations_v3_business05_f_highpolySG2.ss"
		;
connectAttr "Animations_v3_business05_f_highpolySG2.msg" "Animations_v3_materialInfo5.sg"
		;
connectAttr "Animations_v3_business05_f_highpolyPhong1.msg" "Animations_v3_materialInfo5.m"
		;
connectAttr "Animations_v3_Map2.msg" "Animations_v3_materialInfo5.t" -na;
connectAttr "Animations_v3_business05_f_highpolyPhong1.oc" "Animations_v3_business05_f_highpolySG3.ss"
		;
connectAttr "Animations_v3_business05_f_highpolySG3.msg" "Animations_v3_materialInfo6.sg"
		;
connectAttr "Animations_v3_business05_f_highpolyPhong1.msg" "Animations_v3_materialInfo6.m"
		;
connectAttr "Animations_v3_Map2.msg" "Animations_v3_materialInfo6.t" -na;
connectAttr "Animations_v3_place2dTexture2.o" "Animations_v3_Map2.uv";
connectAttr "Animations_v3_place2dTexture2.ofu" "Animations_v3_Map2.ofu";
connectAttr "Animations_v3_place2dTexture2.ofv" "Animations_v3_Map2.ofv";
connectAttr "Animations_v3_place2dTexture2.rf" "Animations_v3_Map2.rf";
connectAttr "Animations_v3_place2dTexture2.reu" "Animations_v3_Map2.reu";
connectAttr "Animations_v3_place2dTexture2.rev" "Animations_v3_Map2.rev";
connectAttr "Animations_v3_place2dTexture2.vt1" "Animations_v3_Map2.vt1";
connectAttr "Animations_v3_place2dTexture2.vt2" "Animations_v3_Map2.vt2";
connectAttr "Animations_v3_place2dTexture2.vt3" "Animations_v3_Map2.vt3";
connectAttr "Animations_v3_place2dTexture2.vc1" "Animations_v3_Map2.vc1";
connectAttr "Animations_v3_place2dTexture2.ofs" "Animations_v3_Map2.fs";
connectAttr "Animations_v3_business05_f_highpoly_hairPhong1.oc" "Animations_v3_business05_f_highpoly_hairSG2.ss"
		;
connectAttr "Animations_v3_business05_f_highpoly_hairSG2.msg" "Animations_v3_materialInfo7.sg"
		;
connectAttr "Animations_v3_business05_f_highpoly_hairPhong1.msg" "Animations_v3_materialInfo7.m"
		;
connectAttr "Animations_v3_business05_f_highpoly_hairPhong1.oc" "Animations_v3_business05_f_highpoly_hairSG3.ss"
		;
connectAttr "Animations_v3_business05_f_highpoly_hairSG3.msg" "Animations_v3_materialInfo8.sg"
		;
connectAttr "Animations_v3_business05_f_highpoly_hairPhong1.msg" "Animations_v3_materialInfo8.m"
		;
connectAttr "Kuan_take_1_Map1.oc" "Kuan_take_1_business05_f_highpolyPhong.c";
connectAttr "Kuan_take_1_business05_f_highpolyPhong.oc" "Kuan_take_1_business05_f_highpolySG.ss"
		;
connectAttr "Kuan_take_1_business05_f_highpolySG.msg" "Kuan_take_1_materialInfo1.sg"
		;
connectAttr "Kuan_take_1_business05_f_highpolyPhong.msg" "Kuan_take_1_materialInfo1.m"
		;
connectAttr "Kuan_take_1_Map1.msg" "Kuan_take_1_materialInfo1.t" -na;
connectAttr "Kuan_take_1_business05_f_highpolyPhong.oc" "Kuan_take_1_business05_f_highpolySG1.ss"
		;
connectAttr "Kuan_take_1_business05_f_highpolySG1.msg" "Kuan_take_1_materialInfo2.sg"
		;
connectAttr "Kuan_take_1_business05_f_highpolyPhong.msg" "Kuan_take_1_materialInfo2.m"
		;
connectAttr "Kuan_take_1_Map1.msg" "Kuan_take_1_materialInfo2.t" -na;
connectAttr "Kuan_take_1_place2dTexture1.o" "Kuan_take_1_Map1.uv";
connectAttr "Kuan_take_1_place2dTexture1.ofu" "Kuan_take_1_Map1.ofu";
connectAttr "Kuan_take_1_place2dTexture1.ofv" "Kuan_take_1_Map1.ofv";
connectAttr "Kuan_take_1_place2dTexture1.rf" "Kuan_take_1_Map1.rf";
connectAttr "Kuan_take_1_place2dTexture1.reu" "Kuan_take_1_Map1.reu";
connectAttr "Kuan_take_1_place2dTexture1.rev" "Kuan_take_1_Map1.rev";
connectAttr "Kuan_take_1_place2dTexture1.vt1" "Kuan_take_1_Map1.vt1";
connectAttr "Kuan_take_1_place2dTexture1.vt2" "Kuan_take_1_Map1.vt2";
connectAttr "Kuan_take_1_place2dTexture1.vt3" "Kuan_take_1_Map1.vt3";
connectAttr "Kuan_take_1_place2dTexture1.vc1" "Kuan_take_1_Map1.vc1";
connectAttr "Kuan_take_1_place2dTexture1.ofs" "Kuan_take_1_Map1.fs";
connectAttr "Kuan_take_1_business05_f_highpoly_hairPhong.oc" "Kuan_take_1_business05_f_highpoly_hairSG.ss"
		;
connectAttr "Kuan_take_1_business05_f_highpoly_hairSG.msg" "Kuan_take_1_materialInfo3.sg"
		;
connectAttr "Kuan_take_1_business05_f_highpoly_hairPhong.msg" "Kuan_take_1_materialInfo3.m"
		;
connectAttr "Kuan_take_1_business05_f_highpoly_hairPhong.oc" "Kuan_take_1_business05_f_highpoly_hairSG1.ss"
		;
connectAttr "Kuan_take_1_business05_f_highpoly_hairSG1.msg" "Kuan_take_1_materialInfo4.sg"
		;
connectAttr "Kuan_take_1_business05_f_highpoly_hairPhong.msg" "Kuan_take_1_materialInfo4.m"
		;
connectAttr "Kuan_take_1_Map2.oc" "Kuan_take_1_business05_f_highpolyPhong1.c";
connectAttr "Kuan_take_1_business05_f_highpolyPhong1.oc" "Kuan_take_1_business05_f_highpolySG2.ss"
		;
connectAttr "Kuan_take_1_business05_f_highpolySG2.msg" "Kuan_take_1_materialInfo5.sg"
		;
connectAttr "Kuan_take_1_business05_f_highpolyPhong1.msg" "Kuan_take_1_materialInfo5.m"
		;
connectAttr "Kuan_take_1_Map2.msg" "Kuan_take_1_materialInfo5.t" -na;
connectAttr "Kuan_take_1_business05_f_highpolyPhong1.oc" "Kuan_take_1_business05_f_highpolySG3.ss"
		;
connectAttr "Kuan_take_1_business05_f_highpolySG3.msg" "Kuan_take_1_materialInfo6.sg"
		;
connectAttr "Kuan_take_1_business05_f_highpolyPhong1.msg" "Kuan_take_1_materialInfo6.m"
		;
connectAttr "Kuan_take_1_Map2.msg" "Kuan_take_1_materialInfo6.t" -na;
connectAttr "Kuan_take_1_place2dTexture2.o" "Kuan_take_1_Map2.uv";
connectAttr "Kuan_take_1_place2dTexture2.ofu" "Kuan_take_1_Map2.ofu";
connectAttr "Kuan_take_1_place2dTexture2.ofv" "Kuan_take_1_Map2.ofv";
connectAttr "Kuan_take_1_place2dTexture2.rf" "Kuan_take_1_Map2.rf";
connectAttr "Kuan_take_1_place2dTexture2.reu" "Kuan_take_1_Map2.reu";
connectAttr "Kuan_take_1_place2dTexture2.rev" "Kuan_take_1_Map2.rev";
connectAttr "Kuan_take_1_place2dTexture2.vt1" "Kuan_take_1_Map2.vt1";
connectAttr "Kuan_take_1_place2dTexture2.vt2" "Kuan_take_1_Map2.vt2";
connectAttr "Kuan_take_1_place2dTexture2.vt3" "Kuan_take_1_Map2.vt3";
connectAttr "Kuan_take_1_place2dTexture2.vc1" "Kuan_take_1_Map2.vc1";
connectAttr "Kuan_take_1_place2dTexture2.ofs" "Kuan_take_1_Map2.fs";
connectAttr "Kuan_take_1_business05_f_highpoly_hairPhong1.oc" "Kuan_take_1_business05_f_highpoly_hairSG2.ss"
		;
connectAttr "Kuan_take_1_business05_f_highpoly_hairSG2.msg" "Kuan_take_1_materialInfo7.sg"
		;
connectAttr "Kuan_take_1_business05_f_highpoly_hairPhong1.msg" "Kuan_take_1_materialInfo7.m"
		;
connectAttr "Kuan_take_1_business05_f_highpoly_hairPhong1.oc" "Kuan_take_1_business05_f_highpoly_hairSG3.ss"
		;
connectAttr "Kuan_take_1_business05_f_highpoly_hairSG3.msg" "Kuan_take_1_materialInfo8.sg"
		;
connectAttr "Kuan_take_1_business05_f_highpoly_hairPhong1.msg" "Kuan_take_1_materialInfo8.m"
		;
connectAttr "Kuan_take_1_Map3.oc" "Kuan_take_1_business05_f_highpolyPhong2.c";
connectAttr "Kuan_take_1_business05_f_highpolyPhong2.oc" "Kuan_take_1_business05_f_highpolySG4.ss"
		;
connectAttr "Kuan_take_1_business05_f_highpolySG4.msg" "Kuan_take_1_materialInfo9.sg"
		;
connectAttr "Kuan_take_1_business05_f_highpolyPhong2.msg" "Kuan_take_1_materialInfo9.m"
		;
connectAttr "Kuan_take_1_Map3.msg" "Kuan_take_1_materialInfo9.t" -na;
connectAttr "Kuan_take_1_business05_f_highpolyPhong2.oc" "Kuan_take_1_business05_f_highpolySG5.ss"
		;
connectAttr "Kuan_take_1_business05_f_highpolySG5.msg" "Kuan_take_1_materialInfo10.sg"
		;
connectAttr "Kuan_take_1_business05_f_highpolyPhong2.msg" "Kuan_take_1_materialInfo10.m"
		;
connectAttr "Kuan_take_1_Map3.msg" "Kuan_take_1_materialInfo10.t" -na;
connectAttr "Kuan_take_1_place2dTexture3.o" "Kuan_take_1_Map3.uv";
connectAttr "Kuan_take_1_place2dTexture3.ofu" "Kuan_take_1_Map3.ofu";
connectAttr "Kuan_take_1_place2dTexture3.ofv" "Kuan_take_1_Map3.ofv";
connectAttr "Kuan_take_1_place2dTexture3.rf" "Kuan_take_1_Map3.rf";
connectAttr "Kuan_take_1_place2dTexture3.reu" "Kuan_take_1_Map3.reu";
connectAttr "Kuan_take_1_place2dTexture3.rev" "Kuan_take_1_Map3.rev";
connectAttr "Kuan_take_1_place2dTexture3.vt1" "Kuan_take_1_Map3.vt1";
connectAttr "Kuan_take_1_place2dTexture3.vt2" "Kuan_take_1_Map3.vt2";
connectAttr "Kuan_take_1_place2dTexture3.vt3" "Kuan_take_1_Map3.vt3";
connectAttr "Kuan_take_1_place2dTexture3.vc1" "Kuan_take_1_Map3.vc1";
connectAttr "Kuan_take_1_place2dTexture3.ofs" "Kuan_take_1_Map3.fs";
connectAttr "Kuan_take_1_business05_f_highpoly_hairPhong2.oc" "Kuan_take_1_business05_f_highpoly_hairSG4.ss"
		;
connectAttr "Kuan_take_1_business05_f_highpoly_hairSG4.msg" "Kuan_take_1_materialInfo11.sg"
		;
connectAttr "Kuan_take_1_business05_f_highpoly_hairPhong2.msg" "Kuan_take_1_materialInfo11.m"
		;
connectAttr "Kuan_take_1_business05_f_highpoly_hairPhong2.oc" "Kuan_take_1_business05_f_highpoly_hairSG5.ss"
		;
connectAttr "Kuan_take_1_business05_f_highpoly_hairSG5.msg" "Kuan_take_1_materialInfo12.sg"
		;
connectAttr "Kuan_take_1_business05_f_highpoly_hairPhong2.msg" "Kuan_take_1_materialInfo12.m"
		;
connectAttr "Kuan_take_1_Map4.oc" "Kuan_take_1_business05_f_highpolyPhong3.c";
connectAttr "Kuan_take_1_business05_f_highpolyPhong3.oc" "Kuan_take_1_business05_f_highpolySG6.ss"
		;
connectAttr "Kuan_take_1_business05_f_highpolySG6.msg" "Kuan_take_1_materialInfo13.sg"
		;
connectAttr "Kuan_take_1_business05_f_highpolyPhong3.msg" "Kuan_take_1_materialInfo13.m"
		;
connectAttr "Kuan_take_1_Map4.msg" "Kuan_take_1_materialInfo13.t" -na;
connectAttr "Kuan_take_1_business05_f_highpolyPhong3.oc" "Kuan_take_1_business05_f_highpolySG7.ss"
		;
connectAttr "Kuan_take_1_business05_f_highpolySG7.msg" "Kuan_take_1_materialInfo14.sg"
		;
connectAttr "Kuan_take_1_business05_f_highpolyPhong3.msg" "Kuan_take_1_materialInfo14.m"
		;
connectAttr "Kuan_take_1_Map4.msg" "Kuan_take_1_materialInfo14.t" -na;
connectAttr "Kuan_take_1_place2dTexture4.o" "Kuan_take_1_Map4.uv";
connectAttr "Kuan_take_1_place2dTexture4.ofu" "Kuan_take_1_Map4.ofu";
connectAttr "Kuan_take_1_place2dTexture4.ofv" "Kuan_take_1_Map4.ofv";
connectAttr "Kuan_take_1_place2dTexture4.rf" "Kuan_take_1_Map4.rf";
connectAttr "Kuan_take_1_place2dTexture4.reu" "Kuan_take_1_Map4.reu";
connectAttr "Kuan_take_1_place2dTexture4.rev" "Kuan_take_1_Map4.rev";
connectAttr "Kuan_take_1_place2dTexture4.vt1" "Kuan_take_1_Map4.vt1";
connectAttr "Kuan_take_1_place2dTexture4.vt2" "Kuan_take_1_Map4.vt2";
connectAttr "Kuan_take_1_place2dTexture4.vt3" "Kuan_take_1_Map4.vt3";
connectAttr "Kuan_take_1_place2dTexture4.vc1" "Kuan_take_1_Map4.vc1";
connectAttr "Kuan_take_1_place2dTexture4.ofs" "Kuan_take_1_Map4.fs";
connectAttr "Kuan_take_1_business05_f_highpoly_hairPhong3.oc" "Kuan_take_1_business05_f_highpoly_hairSG6.ss"
		;
connectAttr "Kuan_take_1_business05_f_highpoly_hairSG6.msg" "Kuan_take_1_materialInfo15.sg"
		;
connectAttr "Kuan_take_1_business05_f_highpoly_hairPhong3.msg" "Kuan_take_1_materialInfo15.m"
		;
connectAttr "Kuan_take_1_business05_f_highpoly_hairPhong3.oc" "Kuan_take_1_business05_f_highpoly_hairSG7.ss"
		;
connectAttr "Kuan_take_1_business05_f_highpoly_hairSG7.msg" "Kuan_take_1_materialInfo16.sg"
		;
connectAttr "Kuan_take_1_business05_f_highpoly_hairPhong3.msg" "Kuan_take_1_materialInfo16.m"
		;
connectAttr "Kuan_take_1_Map5.oc" "Kuan_take_1_business05_f_highpolyPhong4.c";
connectAttr "Kuan_take_1_business05_f_highpolyPhong4.oc" "Kuan_take_1_business05_f_highpolySG8.ss"
		;
connectAttr "Kuan_take_1_business05_f_highpolySG8.msg" "Kuan_take_1_materialInfo17.sg"
		;
connectAttr "Kuan_take_1_business05_f_highpolyPhong4.msg" "Kuan_take_1_materialInfo17.m"
		;
connectAttr "Kuan_take_1_Map5.msg" "Kuan_take_1_materialInfo17.t" -na;
connectAttr "Kuan_take_1_business05_f_highpolyPhong4.oc" "Kuan_take_1_business05_f_highpolySG9.ss"
		;
connectAttr "Kuan_take_1_business05_f_highpolySG9.msg" "Kuan_take_1_materialInfo18.sg"
		;
connectAttr "Kuan_take_1_business05_f_highpolyPhong4.msg" "Kuan_take_1_materialInfo18.m"
		;
connectAttr "Kuan_take_1_Map5.msg" "Kuan_take_1_materialInfo18.t" -na;
connectAttr "Kuan_take_1_place2dTexture5.o" "Kuan_take_1_Map5.uv";
connectAttr "Kuan_take_1_place2dTexture5.ofu" "Kuan_take_1_Map5.ofu";
connectAttr "Kuan_take_1_place2dTexture5.ofv" "Kuan_take_1_Map5.ofv";
connectAttr "Kuan_take_1_place2dTexture5.rf" "Kuan_take_1_Map5.rf";
connectAttr "Kuan_take_1_place2dTexture5.reu" "Kuan_take_1_Map5.reu";
connectAttr "Kuan_take_1_place2dTexture5.rev" "Kuan_take_1_Map5.rev";
connectAttr "Kuan_take_1_place2dTexture5.vt1" "Kuan_take_1_Map5.vt1";
connectAttr "Kuan_take_1_place2dTexture5.vt2" "Kuan_take_1_Map5.vt2";
connectAttr "Kuan_take_1_place2dTexture5.vt3" "Kuan_take_1_Map5.vt3";
connectAttr "Kuan_take_1_place2dTexture5.vc1" "Kuan_take_1_Map5.vc1";
connectAttr "Kuan_take_1_place2dTexture5.ofs" "Kuan_take_1_Map5.fs";
connectAttr "Kuan_take_1_business05_f_highpoly_hairPhong4.oc" "Kuan_take_1_business05_f_highpoly_hairSG8.ss"
		;
connectAttr "Kuan_take_1_business05_f_highpoly_hairSG8.msg" "Kuan_take_1_materialInfo19.sg"
		;
connectAttr "Kuan_take_1_business05_f_highpoly_hairPhong4.msg" "Kuan_take_1_materialInfo19.m"
		;
connectAttr "Kuan_take_1_business05_f_highpoly_hairPhong4.oc" "Kuan_take_1_business05_f_highpoly_hairSG9.ss"
		;
connectAttr "Kuan_take_1_business05_f_highpoly_hairSG9.msg" "Kuan_take_1_materialInfo20.sg"
		;
connectAttr "Kuan_take_1_business05_f_highpoly_hairPhong4.msg" "Kuan_take_1_materialInfo20.m"
		;
connectAttr "Kuan_take_1_Map6.oc" "Kuan_take_1_business05_f_highpolyPhong5.c";
connectAttr "Kuan_take_1_business05_f_highpolyPhong5.oc" "Kuan_take_1_business05_f_highpolySG10.ss"
		;
connectAttr "Kuan_take_1_business05_f_highpolySG10.msg" "Kuan_take_1_materialInfo21.sg"
		;
connectAttr "Kuan_take_1_business05_f_highpolyPhong5.msg" "Kuan_take_1_materialInfo21.m"
		;
connectAttr "Kuan_take_1_Map6.msg" "Kuan_take_1_materialInfo21.t" -na;
connectAttr "Kuan_take_1_business05_f_highpolyPhong5.oc" "Kuan_take_1_business05_f_highpolySG11.ss"
		;
connectAttr "Kuan_take_1_business05_f_highpolySG11.msg" "Kuan_take_1_materialInfo22.sg"
		;
connectAttr "Kuan_take_1_business05_f_highpolyPhong5.msg" "Kuan_take_1_materialInfo22.m"
		;
connectAttr "Kuan_take_1_Map6.msg" "Kuan_take_1_materialInfo22.t" -na;
connectAttr "Kuan_take_1_place2dTexture6.o" "Kuan_take_1_Map6.uv";
connectAttr "Kuan_take_1_place2dTexture6.ofu" "Kuan_take_1_Map6.ofu";
connectAttr "Kuan_take_1_place2dTexture6.ofv" "Kuan_take_1_Map6.ofv";
connectAttr "Kuan_take_1_place2dTexture6.rf" "Kuan_take_1_Map6.rf";
connectAttr "Kuan_take_1_place2dTexture6.reu" "Kuan_take_1_Map6.reu";
connectAttr "Kuan_take_1_place2dTexture6.rev" "Kuan_take_1_Map6.rev";
connectAttr "Kuan_take_1_place2dTexture6.vt1" "Kuan_take_1_Map6.vt1";
connectAttr "Kuan_take_1_place2dTexture6.vt2" "Kuan_take_1_Map6.vt2";
connectAttr "Kuan_take_1_place2dTexture6.vt3" "Kuan_take_1_Map6.vt3";
connectAttr "Kuan_take_1_place2dTexture6.vc1" "Kuan_take_1_Map6.vc1";
connectAttr "Kuan_take_1_place2dTexture6.ofs" "Kuan_take_1_Map6.fs";
connectAttr "Kuan_take_1_business05_f_highpoly_hairPhong5.oc" "Kuan_take_1_business05_f_highpoly_hairSG10.ss"
		;
connectAttr "Kuan_take_1_business05_f_highpoly_hairSG10.msg" "Kuan_take_1_materialInfo23.sg"
		;
connectAttr "Kuan_take_1_business05_f_highpoly_hairPhong5.msg" "Kuan_take_1_materialInfo23.m"
		;
connectAttr "Kuan_take_1_business05_f_highpoly_hairPhong5.oc" "Kuan_take_1_business05_f_highpoly_hairSG11.ss"
		;
connectAttr "Kuan_take_1_business05_f_highpoly_hairSG11.msg" "Kuan_take_1_materialInfo24.sg"
		;
connectAttr "Kuan_take_1_business05_f_highpoly_hairPhong5.msg" "Kuan_take_1_materialInfo24.m"
		;
connectAttr "Kuan_take_1_Map7.oc" "Kuan_take_1_business05_f_highpolyPhong6.c";
connectAttr "Kuan_take_1_business05_f_highpolyPhong6.oc" "Kuan_take_1_business05_f_highpolySG12.ss"
		;
connectAttr "Kuan_take_1_business05_f_highpolySG12.msg" "Kuan_take_1_materialInfo25.sg"
		;
connectAttr "Kuan_take_1_business05_f_highpolyPhong6.msg" "Kuan_take_1_materialInfo25.m"
		;
connectAttr "Kuan_take_1_Map7.msg" "Kuan_take_1_materialInfo25.t" -na;
connectAttr "Kuan_take_1_business05_f_highpolyPhong6.oc" "Kuan_take_1_business05_f_highpolySG13.ss"
		;
connectAttr "Kuan_take_1_business05_f_highpolySG13.msg" "Kuan_take_1_materialInfo26.sg"
		;
connectAttr "Kuan_take_1_business05_f_highpolyPhong6.msg" "Kuan_take_1_materialInfo26.m"
		;
connectAttr "Kuan_take_1_Map7.msg" "Kuan_take_1_materialInfo26.t" -na;
connectAttr "Kuan_take_1_place2dTexture7.o" "Kuan_take_1_Map7.uv";
connectAttr "Kuan_take_1_place2dTexture7.ofu" "Kuan_take_1_Map7.ofu";
connectAttr "Kuan_take_1_place2dTexture7.ofv" "Kuan_take_1_Map7.ofv";
connectAttr "Kuan_take_1_place2dTexture7.rf" "Kuan_take_1_Map7.rf";
connectAttr "Kuan_take_1_place2dTexture7.reu" "Kuan_take_1_Map7.reu";
connectAttr "Kuan_take_1_place2dTexture7.rev" "Kuan_take_1_Map7.rev";
connectAttr "Kuan_take_1_place2dTexture7.vt1" "Kuan_take_1_Map7.vt1";
connectAttr "Kuan_take_1_place2dTexture7.vt2" "Kuan_take_1_Map7.vt2";
connectAttr "Kuan_take_1_place2dTexture7.vt3" "Kuan_take_1_Map7.vt3";
connectAttr "Kuan_take_1_place2dTexture7.vc1" "Kuan_take_1_Map7.vc1";
connectAttr "Kuan_take_1_place2dTexture7.ofs" "Kuan_take_1_Map7.fs";
connectAttr "Kuan_take_1_business05_f_highpoly_hairPhong6.oc" "Kuan_take_1_business05_f_highpoly_hairSG12.ss"
		;
connectAttr "Kuan_take_1_business05_f_highpoly_hairSG12.msg" "Kuan_take_1_materialInfo27.sg"
		;
connectAttr "Kuan_take_1_business05_f_highpoly_hairPhong6.msg" "Kuan_take_1_materialInfo27.m"
		;
connectAttr "Kuan_take_1_business05_f_highpoly_hairPhong6.oc" "Kuan_take_1_business05_f_highpoly_hairSG13.ss"
		;
connectAttr "Kuan_take_1_business05_f_highpoly_hairSG13.msg" "Kuan_take_1_materialInfo28.sg"
		;
connectAttr "Kuan_take_1_business05_f_highpoly_hairPhong6.msg" "Kuan_take_1_materialInfo28.m"
		;
connectAttr "business05_f_highpolySG.pa" ":renderPartition.st" -na;
connectAttr "business05_f_highpolySG1.pa" ":renderPartition.st" -na;
connectAttr "business05_f_highpoly_hairSG.pa" ":renderPartition.st" -na;
connectAttr "business05_f_highpoly_hairSG1.pa" ":renderPartition.st" -na;
connectAttr "MoCap_Result_Animations_v1_business05_f_highpolySG.pa" ":renderPartition.st"
		 -na;
connectAttr "MoCap_Result_Animations_v1_business05_f_highpolySG1.pa" ":renderPartition.st"
		 -na;
connectAttr "MoCap_Result_Animations_v1_business05_f_highpoly_hairSG.pa" ":renderPartition.st"
		 -na;
connectAttr "MoCap_Result_Animations_v1_business05_f_highpoly_hairSG1.pa" ":renderPartition.st"
		 -na;
connectAttr "idle_v1:business05_f_highpolySG.pa" ":renderPartition.st" -na;
connectAttr "idle_v1:business05_f_highpolySG1.pa" ":renderPartition.st" -na;
connectAttr "idle_v1:business05_f_highpoly_hairSG.pa" ":renderPartition.st" -na;
connectAttr "idle_v1:business05_f_highpoly_hairSG1.pa" ":renderPartition.st" -na
		;
connectAttr "idle_v1_business05_f_highpolySG.pa" ":renderPartition.st" -na;
connectAttr "idle_v1_business05_f_highpolySG1.pa" ":renderPartition.st" -na;
connectAttr "idle_v1_business05_f_highpoly_hairSG.pa" ":renderPartition.st" -na;
connectAttr "idle_v1_business05_f_highpoly_hairSG1.pa" ":renderPartition.st" -na
		;
connectAttr "idle_v1_business05_f_highpolySG2.pa" ":renderPartition.st" -na;
connectAttr "idle_v1_business05_f_highpolySG3.pa" ":renderPartition.st" -na;
connectAttr "idle_v1_business05_f_highpoly_hairSG2.pa" ":renderPartition.st" -na
		;
connectAttr "idle_v1_business05_f_highpoly_hairSG3.pa" ":renderPartition.st" -na
		;
connectAttr "stand_v1_business05_f_highpolySG.pa" ":renderPartition.st" -na;
connectAttr "stand_v1_business05_f_highpolySG1.pa" ":renderPartition.st" -na;
connectAttr "stand_v1_business05_f_highpoly_hairSG.pa" ":renderPartition.st" -na
		;
connectAttr "stand_v1_business05_f_highpoly_hairSG1.pa" ":renderPartition.st" -na
		;
connectAttr "sit_idle_v1_business05_f_highpolySG.pa" ":renderPartition.st" -na;
connectAttr "sit_idle_v1_business05_f_highpolySG1.pa" ":renderPartition.st" -na;
connectAttr "sit_idle_v1_business05_f_highpoly_hairSG.pa" ":renderPartition.st" 
		-na;
connectAttr "sit_idle_v1_business05_f_highpoly_hairSG1.pa" ":renderPartition.st"
		 -na;
connectAttr "sit_talk_v1_business05_f_highpolySG.pa" ":renderPartition.st" -na;
connectAttr "sit_talk_v1_business05_f_highpolySG1.pa" ":renderPartition.st" -na;
connectAttr "sit_talk_v1_business05_f_highpoly_hairSG.pa" ":renderPartition.st" 
		-na;
connectAttr "sit_talk_v1_business05_f_highpoly_hairSG1.pa" ":renderPartition.st"
		 -na;
connectAttr "Animations_v3_business05_f_highpolySG.pa" ":renderPartition.st" -na
		;
connectAttr "Animations_v3_business05_f_highpolySG1.pa" ":renderPartition.st" -na
		;
connectAttr "Animations_v3_business05_f_highpoly_hairSG.pa" ":renderPartition.st"
		 -na;
connectAttr "Animations_v3_business05_f_highpoly_hairSG1.pa" ":renderPartition.st"
		 -na;
connectAttr "Jessica_Take_2_2_business05_f_highpolySG.pa" ":renderPartition.st" 
		-na;
connectAttr "Jessica_Take_2_2_business05_f_highpolySG1.pa" ":renderPartition.st"
		 -na;
connectAttr "Jessica_Take_2_2_business05_f_highpoly_hairSG.pa" ":renderPartition.st"
		 -na;
connectAttr "Jessica_Take_2_2_business05_f_highpoly_hairSG1.pa" ":renderPartition.st"
		 -na;
connectAttr "Jessica_Take_2_2_business05_f_highpolySG2.pa" ":renderPartition.st"
		 -na;
connectAttr "Jessica_Take_2_2_business05_f_highpolySG3.pa" ":renderPartition.st"
		 -na;
connectAttr "Jessica_Take_2_2_business05_f_highpoly_hairSG2.pa" ":renderPartition.st"
		 -na;
connectAttr "Jessica_Take_2_2_business05_f_highpoly_hairSG3.pa" ":renderPartition.st"
		 -na;
connectAttr "Jessica_Take_2_2_business05_f_highpolySG4.pa" ":renderPartition.st"
		 -na;
connectAttr "Jessica_Take_2_2_business05_f_highpolySG5.pa" ":renderPartition.st"
		 -na;
connectAttr "Jessica_Take_2_2_business05_f_highpoly_hairSG4.pa" ":renderPartition.st"
		 -na;
connectAttr "Jessica_Take_2_2_business05_f_highpoly_hairSG5.pa" ":renderPartition.st"
		 -na;
connectAttr "Jessica_Take_2_2_business05_f_highpolySG6.pa" ":renderPartition.st"
		 -na;
connectAttr "Jessica_Take_2_2_business05_f_highpolySG7.pa" ":renderPartition.st"
		 -na;
connectAttr "Jessica_Take_2_2_business05_f_highpoly_hairSG6.pa" ":renderPartition.st"
		 -na;
connectAttr "Jessica_Take_2_2_business05_f_highpoly_hairSG7.pa" ":renderPartition.st"
		 -na;
connectAttr "Jessica_Take_2_2_business05_f_highpolySG8.pa" ":renderPartition.st"
		 -na;
connectAttr "Jessica_Take_2_2_business05_f_highpolySG9.pa" ":renderPartition.st"
		 -na;
connectAttr "Jessica_Take_2_2_business05_f_highpoly_hairSG8.pa" ":renderPartition.st"
		 -na;
connectAttr "Jessica_Take_2_2_business05_f_highpoly_hairSG9.pa" ":renderPartition.st"
		 -na;
connectAttr "Jessica_Take_2_2_business05_f_highpolySG10.pa" ":renderPartition.st"
		 -na;
connectAttr "Jessica_Take_2_2_business05_f_highpolySG11.pa" ":renderPartition.st"
		 -na;
connectAttr "Jessica_Take_2_2_business05_f_highpoly_hairSG10.pa" ":renderPartition.st"
		 -na;
connectAttr "Jessica_Take_2_2_business05_f_highpoly_hairSG11.pa" ":renderPartition.st"
		 -na;
connectAttr "Candice_Take_1_business05_f_highpolySG.pa" ":renderPartition.st" -na
		;
connectAttr "Candice_Take_1_business05_f_highpolySG1.pa" ":renderPartition.st" -na
		;
connectAttr "Candice_Take_1_business05_f_highpoly_hairSG.pa" ":renderPartition.st"
		 -na;
connectAttr "Candice_Take_1_business05_f_highpoly_hairSG1.pa" ":renderPartition.st"
		 -na;
connectAttr "Candice_Take_1_business05_f_highpolySG2.pa" ":renderPartition.st" -na
		;
connectAttr "Candice_Take_1_business05_f_highpolySG3.pa" ":renderPartition.st" -na
		;
connectAttr "Candice_Take_1_business05_f_highpoly_hairSG2.pa" ":renderPartition.st"
		 -na;
connectAttr "Candice_Take_1_business05_f_highpoly_hairSG3.pa" ":renderPartition.st"
		 -na;
connectAttr "Candice_Take_1_business05_f_highpolySG4.pa" ":renderPartition.st" -na
		;
connectAttr "Candice_Take_1_business05_f_highpolySG5.pa" ":renderPartition.st" -na
		;
connectAttr "Candice_Take_1_business05_f_highpoly_hairSG4.pa" ":renderPartition.st"
		 -na;
connectAttr "Candice_Take_1_business05_f_highpoly_hairSG5.pa" ":renderPartition.st"
		 -na;
connectAttr "Candice_Take_1_business05_f_highpolySG6.pa" ":renderPartition.st" -na
		;
connectAttr "Candice_Take_1_business05_f_highpolySG7.pa" ":renderPartition.st" -na
		;
connectAttr "Candice_Take_1_business05_f_highpoly_hairSG6.pa" ":renderPartition.st"
		 -na;
connectAttr "Candice_Take_1_business05_f_highpoly_hairSG7.pa" ":renderPartition.st"
		 -na;
connectAttr "Candice_Take_1_business05_f_highpolySG8.pa" ":renderPartition.st" -na
		;
connectAttr "Candice_Take_1_business05_f_highpolySG9.pa" ":renderPartition.st" -na
		;
connectAttr "Candice_Take_1_business05_f_highpoly_hairSG8.pa" ":renderPartition.st"
		 -na;
connectAttr "Candice_Take_1_business05_f_highpoly_hairSG9.pa" ":renderPartition.st"
		 -na;
connectAttr "Candice_Take_1_business05_f_highpolySG10.pa" ":renderPartition.st" 
		-na;
connectAttr "Candice_Take_1_business05_f_highpolySG11.pa" ":renderPartition.st" 
		-na;
connectAttr "Candice_Take_1_business05_f_highpoly_hairSG10.pa" ":renderPartition.st"
		 -na;
connectAttr "Candice_Take_1_business05_f_highpoly_hairSG11.pa" ":renderPartition.st"
		 -na;
connectAttr "Candice_Take_1_business05_f_highpolySG12.pa" ":renderPartition.st" 
		-na;
connectAttr "Candice_Take_1_business05_f_highpolySG13.pa" ":renderPartition.st" 
		-na;
connectAttr "Candice_Take_1_business05_f_highpoly_hairSG12.pa" ":renderPartition.st"
		 -na;
connectAttr "Candice_Take_1_business05_f_highpoly_hairSG13.pa" ":renderPartition.st"
		 -na;
connectAttr "Candice_Take_1_business05_f_highpolySG14.pa" ":renderPartition.st" 
		-na;
connectAttr "Candice_Take_1_business05_f_highpolySG15.pa" ":renderPartition.st" 
		-na;
connectAttr "Candice_Take_1_business05_f_highpoly_hairSG14.pa" ":renderPartition.st"
		 -na;
connectAttr "Candice_Take_1_business05_f_highpoly_hairSG15.pa" ":renderPartition.st"
		 -na;
connectAttr "Animations_v3_business05_f_highpolySG2.pa" ":renderPartition.st" -na
		;
connectAttr "Animations_v3_business05_f_highpolySG3.pa" ":renderPartition.st" -na
		;
connectAttr "Animations_v3_business05_f_highpoly_hairSG2.pa" ":renderPartition.st"
		 -na;
connectAttr "Animations_v3_business05_f_highpoly_hairSG3.pa" ":renderPartition.st"
		 -na;
connectAttr "Kuan_take_1_business05_f_highpolySG.pa" ":renderPartition.st" -na;
connectAttr "Kuan_take_1_business05_f_highpolySG1.pa" ":renderPartition.st" -na;
connectAttr "Kuan_take_1_business05_f_highpoly_hairSG.pa" ":renderPartition.st" 
		-na;
connectAttr "Kuan_take_1_business05_f_highpoly_hairSG1.pa" ":renderPartition.st"
		 -na;
connectAttr "Kuan_take_1_business05_f_highpolySG2.pa" ":renderPartition.st" -na;
connectAttr "Kuan_take_1_business05_f_highpolySG3.pa" ":renderPartition.st" -na;
connectAttr "Kuan_take_1_business05_f_highpoly_hairSG2.pa" ":renderPartition.st"
		 -na;
connectAttr "Kuan_take_1_business05_f_highpoly_hairSG3.pa" ":renderPartition.st"
		 -na;
connectAttr "Kuan_take_1_business05_f_highpolySG4.pa" ":renderPartition.st" -na;
connectAttr "Kuan_take_1_business05_f_highpolySG5.pa" ":renderPartition.st" -na;
connectAttr "Kuan_take_1_business05_f_highpoly_hairSG4.pa" ":renderPartition.st"
		 -na;
connectAttr "Kuan_take_1_business05_f_highpoly_hairSG5.pa" ":renderPartition.st"
		 -na;
connectAttr "Kuan_take_1_business05_f_highpolySG6.pa" ":renderPartition.st" -na;
connectAttr "Kuan_take_1_business05_f_highpolySG7.pa" ":renderPartition.st" -na;
connectAttr "Kuan_take_1_business05_f_highpoly_hairSG6.pa" ":renderPartition.st"
		 -na;
connectAttr "Kuan_take_1_business05_f_highpoly_hairSG7.pa" ":renderPartition.st"
		 -na;
connectAttr "Kuan_take_1_business05_f_highpolySG8.pa" ":renderPartition.st" -na;
connectAttr "Kuan_take_1_business05_f_highpolySG9.pa" ":renderPartition.st" -na;
connectAttr "Kuan_take_1_business05_f_highpoly_hairSG8.pa" ":renderPartition.st"
		 -na;
connectAttr "Kuan_take_1_business05_f_highpoly_hairSG9.pa" ":renderPartition.st"
		 -na;
connectAttr "Kuan_take_1_business05_f_highpolySG10.pa" ":renderPartition.st" -na
		;
connectAttr "Kuan_take_1_business05_f_highpolySG11.pa" ":renderPartition.st" -na
		;
connectAttr "Kuan_take_1_business05_f_highpoly_hairSG10.pa" ":renderPartition.st"
		 -na;
connectAttr "Kuan_take_1_business05_f_highpoly_hairSG11.pa" ":renderPartition.st"
		 -na;
connectAttr "Kuan_take_1_business05_f_highpolySG12.pa" ":renderPartition.st" -na
		;
connectAttr "Kuan_take_1_business05_f_highpolySG13.pa" ":renderPartition.st" -na
		;
connectAttr "Kuan_take_1_business05_f_highpoly_hairSG12.pa" ":renderPartition.st"
		 -na;
connectAttr "Kuan_take_1_business05_f_highpoly_hairSG13.pa" ":renderPartition.st"
		 -na;
connectAttr "business05_f_highpolyPhong.msg" ":defaultShaderList1.s" -na;
connectAttr "business05_f_highpoly_hairPhong.msg" ":defaultShaderList1.s" -na;
connectAttr "MoCap_Result_Animations_v1_business05_f_highpolyPhong.msg" ":defaultShaderList1.s"
		 -na;
connectAttr "MoCap_Result_Animations_v1_business05_f_highpoly_hairPhong.msg" ":defaultShaderList1.s"
		 -na;
connectAttr "idle_v1:business05_f_highpolyPhong.msg" ":defaultShaderList1.s" -na
		;
connectAttr "idle_v1:business05_f_highpoly_hairPhong.msg" ":defaultShaderList1.s"
		 -na;
connectAttr "idle_v1_business05_f_highpolyPhong.msg" ":defaultShaderList1.s" -na
		;
connectAttr "idle_v1_business05_f_highpoly_hairPhong.msg" ":defaultShaderList1.s"
		 -na;
connectAttr "idle_v1_business05_f_highpolyPhong1.msg" ":defaultShaderList1.s" -na
		;
connectAttr "idle_v1_business05_f_highpoly_hairPhong1.msg" ":defaultShaderList1.s"
		 -na;
connectAttr "stand_v1_business05_f_highpolyPhong.msg" ":defaultShaderList1.s" -na
		;
connectAttr "stand_v1_business05_f_highpoly_hairPhong.msg" ":defaultShaderList1.s"
		 -na;
connectAttr "sit_idle_v1_business05_f_highpolyPhong.msg" ":defaultShaderList1.s"
		 -na;
connectAttr "sit_idle_v1_business05_f_highpoly_hairPhong.msg" ":defaultShaderList1.s"
		 -na;
connectAttr "sit_talk_v1_business05_f_highpolyPhong.msg" ":defaultShaderList1.s"
		 -na;
connectAttr "sit_talk_v1_business05_f_highpoly_hairPhong.msg" ":defaultShaderList1.s"
		 -na;
connectAttr "Animations_v3_business05_f_highpolyPhong.msg" ":defaultShaderList1.s"
		 -na;
connectAttr "Animations_v3_business05_f_highpoly_hairPhong.msg" ":defaultShaderList1.s"
		 -na;
connectAttr "Jessica_Take_2_2_business05_f_highpolyPhong.msg" ":defaultShaderList1.s"
		 -na;
connectAttr "Jessica_Take_2_2_business05_f_highpoly_hairPhong.msg" ":defaultShaderList1.s"
		 -na;
connectAttr "Jessica_Take_2_2_business05_f_highpolyPhong1.msg" ":defaultShaderList1.s"
		 -na;
connectAttr "Jessica_Take_2_2_business05_f_highpoly_hairPhong1.msg" ":defaultShaderList1.s"
		 -na;
connectAttr "Jessica_Take_2_2_business05_f_highpolyPhong2.msg" ":defaultShaderList1.s"
		 -na;
connectAttr "Jessica_Take_2_2_business05_f_highpoly_hairPhong2.msg" ":defaultShaderList1.s"
		 -na;
connectAttr "Jessica_Take_2_2_business05_f_highpolyPhong3.msg" ":defaultShaderList1.s"
		 -na;
connectAttr "Jessica_Take_2_2_business05_f_highpoly_hairPhong3.msg" ":defaultShaderList1.s"
		 -na;
connectAttr "Jessica_Take_2_2_business05_f_highpolyPhong4.msg" ":defaultShaderList1.s"
		 -na;
connectAttr "Jessica_Take_2_2_business05_f_highpoly_hairPhong4.msg" ":defaultShaderList1.s"
		 -na;
connectAttr "Jessica_Take_2_2_business05_f_highpolyPhong5.msg" ":defaultShaderList1.s"
		 -na;
connectAttr "Jessica_Take_2_2_business05_f_highpoly_hairPhong5.msg" ":defaultShaderList1.s"
		 -na;
connectAttr "Candice_Take_1_business05_f_highpolyPhong.msg" ":defaultShaderList1.s"
		 -na;
connectAttr "Candice_Take_1_business05_f_highpoly_hairPhong.msg" ":defaultShaderList1.s"
		 -na;
connectAttr "Candice_Take_1_business05_f_highpolyPhong1.msg" ":defaultShaderList1.s"
		 -na;
connectAttr "Candice_Take_1_business05_f_highpoly_hairPhong1.msg" ":defaultShaderList1.s"
		 -na;
connectAttr "Candice_Take_1_business05_f_highpolyPhong2.msg" ":defaultShaderList1.s"
		 -na;
connectAttr "Candice_Take_1_business05_f_highpoly_hairPhong2.msg" ":defaultShaderList1.s"
		 -na;
connectAttr "Candice_Take_1_business05_f_highpolyPhong3.msg" ":defaultShaderList1.s"
		 -na;
connectAttr "Candice_Take_1_business05_f_highpoly_hairPhong3.msg" ":defaultShaderList1.s"
		 -na;
connectAttr "Candice_Take_1_business05_f_highpolyPhong4.msg" ":defaultShaderList1.s"
		 -na;
connectAttr "Candice_Take_1_business05_f_highpoly_hairPhong4.msg" ":defaultShaderList1.s"
		 -na;
connectAttr "Candice_Take_1_business05_f_highpolyPhong5.msg" ":defaultShaderList1.s"
		 -na;
connectAttr "Candice_Take_1_business05_f_highpoly_hairPhong5.msg" ":defaultShaderList1.s"
		 -na;
connectAttr "Candice_Take_1_business05_f_highpolyPhong6.msg" ":defaultShaderList1.s"
		 -na;
connectAttr "Candice_Take_1_business05_f_highpoly_hairPhong6.msg" ":defaultShaderList1.s"
		 -na;
connectAttr "Candice_Take_1_business05_f_highpolyPhong7.msg" ":defaultShaderList1.s"
		 -na;
connectAttr "Candice_Take_1_business05_f_highpoly_hairPhong7.msg" ":defaultShaderList1.s"
		 -na;
connectAttr "Animations_v3_business05_f_highpolyPhong1.msg" ":defaultShaderList1.s"
		 -na;
connectAttr "Animations_v3_business05_f_highpoly_hairPhong1.msg" ":defaultShaderList1.s"
		 -na;
connectAttr "Kuan_take_1_business05_f_highpolyPhong.msg" ":defaultShaderList1.s"
		 -na;
connectAttr "Kuan_take_1_business05_f_highpoly_hairPhong.msg" ":defaultShaderList1.s"
		 -na;
connectAttr "Kuan_take_1_business05_f_highpolyPhong1.msg" ":defaultShaderList1.s"
		 -na;
connectAttr "Kuan_take_1_business05_f_highpoly_hairPhong1.msg" ":defaultShaderList1.s"
		 -na;
connectAttr "Kuan_take_1_business05_f_highpolyPhong2.msg" ":defaultShaderList1.s"
		 -na;
connectAttr "Kuan_take_1_business05_f_highpoly_hairPhong2.msg" ":defaultShaderList1.s"
		 -na;
connectAttr "Kuan_take_1_business05_f_highpolyPhong3.msg" ":defaultShaderList1.s"
		 -na;
connectAttr "Kuan_take_1_business05_f_highpoly_hairPhong3.msg" ":defaultShaderList1.s"
		 -na;
connectAttr "Kuan_take_1_business05_f_highpolyPhong4.msg" ":defaultShaderList1.s"
		 -na;
connectAttr "Kuan_take_1_business05_f_highpoly_hairPhong4.msg" ":defaultShaderList1.s"
		 -na;
connectAttr "Kuan_take_1_business05_f_highpolyPhong5.msg" ":defaultShaderList1.s"
		 -na;
connectAttr "Kuan_take_1_business05_f_highpoly_hairPhong5.msg" ":defaultShaderList1.s"
		 -na;
connectAttr "Kuan_take_1_business05_f_highpolyPhong6.msg" ":defaultShaderList1.s"
		 -na;
connectAttr "Kuan_take_1_business05_f_highpoly_hairPhong6.msg" ":defaultShaderList1.s"
		 -na;
connectAttr "Map1.msg" ":defaultTextureList1.tx" -na;
connectAttr "MoCap_Result_Animations_v1_Map1.msg" ":defaultTextureList1.tx" -na;
connectAttr "idle_v1:Map1.msg" ":defaultTextureList1.tx" -na;
connectAttr "idle_v1_Map1.msg" ":defaultTextureList1.tx" -na;
connectAttr "idle_v1_Map2.msg" ":defaultTextureList1.tx" -na;
connectAttr "stand_v1_Map1.msg" ":defaultTextureList1.tx" -na;
connectAttr "sit_idle_v1_Map1.msg" ":defaultTextureList1.tx" -na;
connectAttr "sit_talk_v1_Map1.msg" ":defaultTextureList1.tx" -na;
connectAttr "Animations_v3_Map1.msg" ":defaultTextureList1.tx" -na;
connectAttr "Jessica_Take_2_2_Map1.msg" ":defaultTextureList1.tx" -na;
connectAttr "Jessica_Take_2_2_Map2.msg" ":defaultTextureList1.tx" -na;
connectAttr "Jessica_Take_2_2_Map3.msg" ":defaultTextureList1.tx" -na;
connectAttr "Jessica_Take_2_2_Map4.msg" ":defaultTextureList1.tx" -na;
connectAttr "Jessica_Take_2_2_Map5.msg" ":defaultTextureList1.tx" -na;
connectAttr "Jessica_Take_2_2_Map6.msg" ":defaultTextureList1.tx" -na;
connectAttr "Candice_Take_1_Map1.msg" ":defaultTextureList1.tx" -na;
connectAttr "Candice_Take_1_Map2.msg" ":defaultTextureList1.tx" -na;
connectAttr "Candice_Take_1_Map3.msg" ":defaultTextureList1.tx" -na;
connectAttr "Candice_Take_1_Map4.msg" ":defaultTextureList1.tx" -na;
connectAttr "Candice_Take_1_Map5.msg" ":defaultTextureList1.tx" -na;
connectAttr "Candice_Take_1_Map6.msg" ":defaultTextureList1.tx" -na;
connectAttr "Candice_Take_1_Map7.msg" ":defaultTextureList1.tx" -na;
connectAttr "Candice_Take_1_Map8.msg" ":defaultTextureList1.tx" -na;
connectAttr "Animations_v3_Map2.msg" ":defaultTextureList1.tx" -na;
connectAttr "Kuan_take_1_Map1.msg" ":defaultTextureList1.tx" -na;
connectAttr "Kuan_take_1_Map2.msg" ":defaultTextureList1.tx" -na;
connectAttr "Kuan_take_1_Map3.msg" ":defaultTextureList1.tx" -na;
connectAttr "Kuan_take_1_Map4.msg" ":defaultTextureList1.tx" -na;
connectAttr "Kuan_take_1_Map5.msg" ":defaultTextureList1.tx" -na;
connectAttr "Kuan_take_1_Map6.msg" ":defaultTextureList1.tx" -na;
connectAttr "Kuan_take_1_Map7.msg" ":defaultTextureList1.tx" -na;
connectAttr "place2dTexture1.msg" ":defaultRenderUtilityList1.u" -na;
connectAttr "MoCap_Result_Animations_v1_place2dTexture1.msg" ":defaultRenderUtilityList1.u"
		 -na;
connectAttr "idle_v1:place2dTexture1.msg" ":defaultRenderUtilityList1.u" -na;
connectAttr "idle_v1_place2dTexture1.msg" ":defaultRenderUtilityList1.u" -na;
connectAttr "idle_v1_place2dTexture2.msg" ":defaultRenderUtilityList1.u" -na;
connectAttr "stand_v1_place2dTexture1.msg" ":defaultRenderUtilityList1.u" -na;
connectAttr "sit_idle_v1_place2dTexture1.msg" ":defaultRenderUtilityList1.u" -na
		;
connectAttr "sit_talk_v1_place2dTexture1.msg" ":defaultRenderUtilityList1.u" -na
		;
connectAttr "Animations_v3_place2dTexture1.msg" ":defaultRenderUtilityList1.u" -na
		;
connectAttr "Jessica_Take_2_2_place2dTexture1.msg" ":defaultRenderUtilityList1.u"
		 -na;
connectAttr "Jessica_Take_2_2_place2dTexture2.msg" ":defaultRenderUtilityList1.u"
		 -na;
connectAttr "Jessica_Take_2_2_place2dTexture3.msg" ":defaultRenderUtilityList1.u"
		 -na;
connectAttr "Jessica_Take_2_2_place2dTexture4.msg" ":defaultRenderUtilityList1.u"
		 -na;
connectAttr "Jessica_Take_2_2_place2dTexture5.msg" ":defaultRenderUtilityList1.u"
		 -na;
connectAttr "Jessica_Take_2_2_place2dTexture6.msg" ":defaultRenderUtilityList1.u"
		 -na;
connectAttr "Candice_Take_1_place2dTexture1.msg" ":defaultRenderUtilityList1.u" 
		-na;
connectAttr "Candice_Take_1_place2dTexture2.msg" ":defaultRenderUtilityList1.u" 
		-na;
connectAttr "Candice_Take_1_place2dTexture3.msg" ":defaultRenderUtilityList1.u" 
		-na;
connectAttr "Candice_Take_1_place2dTexture4.msg" ":defaultRenderUtilityList1.u" 
		-na;
connectAttr "Candice_Take_1_place2dTexture5.msg" ":defaultRenderUtilityList1.u" 
		-na;
connectAttr "Candice_Take_1_place2dTexture6.msg" ":defaultRenderUtilityList1.u" 
		-na;
connectAttr "Candice_Take_1_place2dTexture7.msg" ":defaultRenderUtilityList1.u" 
		-na;
connectAttr "Candice_Take_1_place2dTexture8.msg" ":defaultRenderUtilityList1.u" 
		-na;
connectAttr "Animations_v3_place2dTexture2.msg" ":defaultRenderUtilityList1.u" -na
		;
connectAttr "Kuan_take_1_place2dTexture1.msg" ":defaultRenderUtilityList1.u" -na
		;
connectAttr "Kuan_take_1_place2dTexture2.msg" ":defaultRenderUtilityList1.u" -na
		;
connectAttr "Kuan_take_1_place2dTexture3.msg" ":defaultRenderUtilityList1.u" -na
		;
connectAttr "Kuan_take_1_place2dTexture4.msg" ":defaultRenderUtilityList1.u" -na
		;
connectAttr "Kuan_take_1_place2dTexture5.msg" ":defaultRenderUtilityList1.u" -na
		;
connectAttr "Kuan_take_1_place2dTexture6.msg" ":defaultRenderUtilityList1.u" -na
		;
connectAttr "Kuan_take_1_place2dTexture7.msg" ":defaultRenderUtilityList1.u" -na
		;
connectAttr "defaultRenderLayer.msg" ":defaultRenderingList1.r" -na;
connectAttr "Bip01_Spine.msg" ":hyperGraphLayout.hyp[1].dn";
// End of Animation v6 - Stand Idle 1.ma
