//Maya ASCII 2014 scene
//Name: Animation v5 - Stand Wave 3.ma
//Last modified: Tue, Apr 14, 2015 07:29:00 AM
//Codeset: 1252
requires maya "2014";
currentUnit -l centimeter -a degree -t film;
fileInfo "application" "maya";
fileInfo "product" "Maya 2014";
fileInfo "version" "2014 x64";
fileInfo "cutIdentifier" "201303010241-864206";
fileInfo "osv" "Microsoft Windows 7 Enterprise Edition, 64-bit Windows 7 Service Pack 1 (Build 7601)\n";
fileInfo "license" "education";
createNode transform -n "Stand_Wave_3";
	setAttr ".v" no;
createNode joint -n "Bip01_Spine" -p "Stand_Wave_3";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".s" -type "double3" 1 0.99999999999999956 1 ;
	setAttr ".jo" -type "double3" 90.000171 0 89.999999999999986 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" 0.00041359021872633406 0.99974830939735237 0.022430934003442312 0
		 0.70710082718146761 -0.016153575523873306 0.70692820158561975 0 0.70711261418702975 0.015568553398769388 -0.70692953750897569 0
		 -6.7139604917915256e-006 99.443700000000007 -2.2495999999899592 1;
	setAttr -k on ".liw";
	setAttr ".fbxID" 5;
createNode joint -n "Bip01_Spine1" -p "|Stand_Wave_3|Bip01_Spine";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".t" -type "double3" 13.3984 -0.0107 0 ;
	setAttr ".s" -type "double3" 0.99999999999999989 0.99999999999999989 1 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" 0.0038255966723317676 0.015307468004230922 0.99987551536848818 0
		 0.011000641830761762 -0.999822973655216 0.015264574365256002 0 0.99993217304445803 0.010940876315020786 -0.0039933114137866372 0
		 9.9999999999999995e-007 2.2603360000000001 112.84212599999999 1;
	setAttr -k on ".liw";
	setAttr ".fbxID" 5;
createNode joint -n "Bip01_Spine2" -p "|Stand_Wave_3|Bip01_Spine|Bip01_Spine1";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".t" -type "double3" 13.3983 -0.0107 0 ;
	setAttr ".s" -type "double3" 0.99999999999999989 1 1 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" 0 0 1 0 0 -1 0 0 1 0 0 0 -3.9999999999999998e-006 -9.6239790000000003 -19.074804 1;
	setAttr -k on ".liw";
	setAttr ".fbxID" 5;
createNode joint -n "Bip01_Neck" -p "|Stand_Wave_3|Bip01_Spine|Bip01_Spine1|Bip01_Spine2";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".t" -type "double3" 13.3985 -0.007899999999999999 0 ;
	setAttr ".r" -type "double3" 1.1848489498583726e-023 -5.9999999999999985e-006 1.1479959999999994 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" 0 -0.020830999999999999 0.99978299999999998 0 0 -0.99978299999999998 -0.020830999999999999 0
		 1 0 0 0 -1.0000000000000001e-005 -9.6266999999999996 -5.6762839999999999 1;
	setAttr -k on ".liw";
	setAttr ".fbxID" 5;
createNode joint -n "Bip01_Head" -p "|Stand_Wave_3|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".t" -type "double3" 9.9829 0 0 ;
	setAttr ".r" -type "double3" 9.4787915988671744e-023 -1.3000000000000004e-005 4.622390999999995 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" 0 -0.10133399999999999 0.99485299999999999 0 0 -0.99485199999999996 -0.10133399999999999 0
		 1 0 0 0 -4.4000000000000012e-005 -9.8346470000000004 4.3044190000000002 1;
	setAttr -k on ".liw";
	setAttr ".fbxID" 5;
createNode joint -n "_notused_Bip01_HeadNub" -p "|Stand_Wave_3|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_Head";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".t" -type "double3" 18.3537 0 0 ;
	setAttr ".ssc" no;
	setAttr ".fbxID" 5;
createNode joint -n "Bip01_R_Clavicle" -p "|Stand_Wave_3|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".t" -type "double3" -0.8268 -0.6612 -3.9165 ;
	setAttr ".s" -type "double3" 1 1.0000000000000002 1 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" -0.99454799999999999 -9.9454800000000002e-007 -0.104286 0
		 -9.8906500000000008e-007 1 -1.0428599999999999e-007 0 0.104286 -5.7179000000000015e-010 -0.99454799999999999 0
		 -3.9165049999999999 2.9251839999999998 138.82619800000001 1;
	setAttr -k on ".liw";
	setAttr ".fbxID" 5;
createNode joint -n "Bip01_R_UpperArm" -p "|Stand_Wave_3|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_R_Clavicle";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".t" -type "double3" 10.6745 0 0 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" -0.75332600000000005 0.10000199999999999 -0.65000000000000002 0
		 0.071782399999999996 0.99496899999999999 0.0698825 0 0.65371900000000005 0.0059857399999999998 -0.75671500000000003 0
		 -14.532831 2.9251239999999998 137.712976 1;
	setAttr -k on ".liw";
	setAttr ".fbxID" 5;
createNode joint -n "Bip01_R_Forearm" -p "|Stand_Wave_3|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_R_Clavicle|Bip01_R_UpperArm";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".t" -type "double3" 25.3375 0 0 ;
	setAttr ".s" -type "double3" 0.99999999999999989 1 1 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" -0.752861 -0.095945600000000006 -0.65114899999999998 0
		 -0.076500899999999997 0.99536800000000003 -0.058215000000000003 0 0.65371900000000005 0.0059856600000000003 -0.756714 0
		 -33.620182 5.4589869999999996 121.243574 1;
	setAttr -k on ".liw";
	setAttr ".fbxID" 5;
createNode joint -n "Bip01_R_Hand" -p "|Stand_Wave_3|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_R_Clavicle|Bip01_R_UpperArm|Bip01_R_Forearm";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".t" -type "double3" 24.9881 0 0 ;
	setAttr ".r" -type "double3" 90.133325000000013 -6.3004020000000009 2.5653330000000008 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" -0.67922700000000003 -0.050331500000000001 -0.73219999999999996 0
		 0.73278100000000002 0.0092545500000000003 -0.68040299999999998 0 0.0410219 -0.99868900000000005 0.030596000000000002 0
		 -52.432737000000003 3.0614050000000002 104.972679 1;
	setAttr -k on ".liw";
	setAttr ".fbxID" 5;
createNode joint -n "Bip01_R_Finger0" -p "|Stand_Wave_3|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_R_Clavicle|Bip01_R_UpperArm|Bip01_R_Forearm|Bip01_R_Hand";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".t" -type "double3" 2.3036000000000003 1.3941 2.9222 ;
	setAttr ".r" -type "double3" -81.93018 -23.314181 18.295908 ;
	setAttr ".s" -type "double3" 0.99999999999999989 1 1 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" -0.36474400000000001 -0.436471 -0.82246900000000001 0
		 -0.072261699999999998 0.89392799999999994 -0.44234699999999999 0 0.92829899999999999 -0.10191 -0.357595 0
		 -52.855969000000002 0.0400224 102.426821 1;
	setAttr -k on ".liw";
	setAttr ".fbxID" 5;
createNode joint -n "Bip01_R_Finger01" -p "|Stand_Wave_3|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_R_Clavicle|Bip01_R_UpperArm|Bip01_R_Forearm|Bip01_R_Hand|Bip01_R_Finger0";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".t" -type "double3" 4.0806 0 0 ;
	setAttr ".r" -type "double3" 9.4787915988669464e-023 3.0000000000000005e-006 24.064549 ;
	setAttr ".s" -type "double3" 1 1 0.99999999999999989 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" -0.36250900000000003 -0.034022900000000002 -0.93135900000000005 0
		 0.082749500000000004 0.99421099999999996 -0.068527199999999996 0 0.92830000000000001 -0.101911 -0.357595 0
		 -54.344414999999998 -1.7410159999999999 99.070615000000004 1;
	setAttr -k on ".liw";
	setAttr ".fbxID" 5;
createNode joint -n "_notused_Bip01_R_Finger0Nub" -p "|Stand_Wave_3|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_R_Clavicle|Bip01_R_UpperArm|Bip01_R_Forearm|Bip01_R_Hand|Bip01_R_Finger0|Bip01_R_Finger01";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".t" -type "double3" 3.3413 0 0 ;
	setAttr ".r" -type "double3" 1e-006 0 -180 ;
	setAttr ".ssc" no;
	setAttr ".fbxID" 5;
createNode joint -n "Bip01_R_Finger2" -p "|Stand_Wave_3|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_R_Clavicle|Bip01_R_UpperArm|Bip01_R_Forearm|Bip01_R_Hand";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".t" -type "double3" 7.3709 -0.4816 0.6341 ;
	setAttr ".r" -type "double3" 0.043615000000000029 -0.013407000000000007 17.086613999999997 ;
	setAttr ".s" -type "double3" 0.99999999999999989 0.99999999999999989 0.99999999999999989 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" -0.43393399999999999 -0.045623999999999998 -0.89978899999999995 0
		 0.90003599999999995 0.022875199999999998 -0.43521399999999999 0 0.040439000000000003 -0.99869600000000003 0.031137000000000001 0
		 -57.766043000000003 2.0527600000000001 99.922762000000006 1;
	setAttr -k on ".liw";
	setAttr ".fbxID" 5;
createNode joint -n "Bip01_R_Finger21" -p "|Stand_Wave_3|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_R_Clavicle|Bip01_R_UpperArm|Bip01_R_Forearm|Bip01_R_Hand|Bip01_R_Finger2";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".t" -type "double3" 3.8208 0 0 ;
	setAttr ".r" -type "double3" 0 0 29.946389999999987 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" 0.0732873 -0.028113900000000001 -0.99691399999999997 0
		 0.99648999999999999 0.042598999999999998 0.072054900000000005 0 0.0404418 -0.99869600000000003 0.0311372 0
		 -59.424045999999997 1.8784400000000001 96.484806000000006 1;
	setAttr -k on ".liw";
	setAttr ".fbxID" 5;
createNode joint -n "_notused_Bip01_R_Finger2Nub" -p "|Stand_Wave_3|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_R_Clavicle|Bip01_R_UpperArm|Bip01_R_Forearm|Bip01_R_Hand|Bip01_R_Finger2|Bip01_R_Finger21";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".t" -type "double3" 4.7174000000000005 0 0 ;
	setAttr ".r" -type "double3" 0 0 180 ;
	setAttr ".ssc" no;
	setAttr ".fbxID" 5;
createNode joint -n "Bip01_R_Finger1" -p "|Stand_Wave_3|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_R_Clavicle|Bip01_R_UpperArm|Bip01_R_Forearm|Bip01_R_Hand";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".t" -type "double3" 7.3263 -0.211 2.2784 ;
	setAttr ".r" -type "double3" -0.73310399999999953 -5.3145149999999983 22.646732999999994 ;
	setAttr ".s" -type "double3" 1 1 0.99999999999999989 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" -0.33941900000000003 -0.13520499999999999 -0.93086800000000003 0
		 0.93680799999999997 0.040593299999999999 -0.34748099999999998 0 0.084767999999999996 -0.98998600000000003 0.112883 0
		 -57.470132999999997 0.41539700000000002 99.821579 1;
	setAttr -k on ".liw";
	setAttr ".fbxID" 5;
createNode joint -n "Bip01_R_Finger11" -p "|Stand_Wave_3|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_R_Clavicle|Bip01_R_UpperArm|Bip01_R_Forearm|Bip01_R_Hand|Bip01_R_Finger1";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".t" -type "double3" 3.5597999999999996 0 0 ;
	setAttr ".r" -type "double3" -2e-006 3.999999999999999e-006 22.627677999999996 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" 0.047135799999999999 -0.10918 -0.99290400000000001 0
		 0.995286 0.089482699999999998 0.037409400000000002 0 0.084763400000000003 -0.98998600000000003 0.112883 0
		 -58.678344000000003 -0.065903900000000001 96.507863999999998 1;
	setAttr -k on ".liw";
	setAttr ".fbxID" 5;
createNode joint -n "_notused_Bip01_R_Finger1Nub" -p "|Stand_Wave_3|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_R_Clavicle|Bip01_R_UpperArm|Bip01_R_Forearm|Bip01_R_Hand|Bip01_R_Finger1|Bip01_R_Finger11";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".t" -type "double3" 4.1404000000000005 0 0 ;
	setAttr ".r" -type "double3" 0 0 180 ;
	setAttr ".ssc" no;
	setAttr ".fbxID" 5;
createNode joint -n "Bip01_R_Finger3" -p "|Stand_Wave_3|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_R_Clavicle|Bip01_R_UpperArm|Bip01_R_Forearm|Bip01_R_Hand";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".t" -type "double3" 7.2345 -0.5997 -0.9332 ;
	setAttr ".r" -type "double3" 0.043363999999999805 3.826455 18.509659999999997 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" -0.41327900000000001 0.0219578 -0.91034000000000004 0
		 0.91051400000000005 0.0239971 -0.41277999999999998 0 0.0127817 -0.999471 -0.0299104 0
		 -57.824306 3.6237469999999998 100.05504999999999 1;
	setAttr -k on ".liw";
	setAttr ".fbxID" 5;
createNode joint -n "Bip01_R_Finger31" -p "|Stand_Wave_3|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_R_Clavicle|Bip01_R_UpperArm|Bip01_R_Forearm|Bip01_R_Hand|Bip01_R_Finger3";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".t" -type "double3" 3.7241 0 0 ;
	setAttr ".r" -type "double3" 3.0000000000000013e-006 3.0000000000000005e-006 27.984287000000009 ;
	setAttr ".s" -type "double3" 1 0.99999999999999989 0.99999999999999989 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" 0.0622821 0.0306512 -0.99758800000000003 0 0.99797599999999997 0.0108983 0.062641199999999994 0
		 0.012792100000000001 -0.999471 -0.0299104 0 -59.363360999999998 3.705527 96.664748000000003 1;
	setAttr -k on ".liw";
	setAttr ".fbxID" 5;
createNode joint -n "_notused_Bip01_R_Finger3Nub" -p "|Stand_Wave_3|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_R_Clavicle|Bip01_R_UpperArm|Bip01_R_Forearm|Bip01_R_Hand|Bip01_R_Finger3|Bip01_R_Finger31";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".t" -type "double3" 4.2397 0 0 ;
	setAttr ".r" -type "double3" 3e-006 -5e-006 180 ;
	setAttr ".ssc" no;
	setAttr ".fbxID" 5;
createNode joint -n "Bip01_R_Finger4" -p "|Stand_Wave_3|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_R_Clavicle|Bip01_R_UpperArm|Bip01_R_Forearm|Bip01_R_Hand";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".t" -type "double3" 7.2292 -0.4187 -2.3639 ;
	setAttr ".r" -type "double3" -0.17713400000000007 5.5619560000000012 16.615208 ;
	setAttr ".s" -type "double3" 0.99999999999999989 0.99999999999999989 1 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" -0.44323200000000001 0.051426199999999998 -0.89493100000000003 0
		 0.89640699999999995 0.026346700000000001 -0.44244899999999998 0 0.00082491599999999997 -0.99832900000000002 -0.057776500000000001 0
		 -57.746822000000002 5.0544450000000003 99.892002000000005 1;
	setAttr -k on ".liw";
	setAttr ".fbxID" 5;
createNode joint -n "Bip01_R_Finger41" -p "|Stand_Wave_3|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_R_Clavicle|Bip01_R_UpperArm|Bip01_R_Forearm|Bip01_R_Hand|Bip01_R_Finger4";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".t" -type "double3" 3.4791000000000003 0 0 ;
	setAttr ".r" -type "double3" -1.9999999999999995e-006 -3.0000000000000005e-006 29.444368 ;
	setAttr ".s" -type "double3" 1 0.99999999999999989 1 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" 0.054672699999999998 0.057735700000000001 -0.996834 0
		 0.99850399999999995 -0.0023359700000000001 0.054628999999999997 0 0.00082546799999999999 -0.99832900000000002 -0.057777000000000002 0
		 -59.288860999999997 5.2333790000000002 96.778379000000001 1;
	setAttr -k on ".liw";
	setAttr ".fbxID" 5;
createNode joint -n "_notused_Bip01_R_Finger4Nub" -p "|Stand_Wave_3|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_R_Clavicle|Bip01_R_UpperArm|Bip01_R_Forearm|Bip01_R_Hand|Bip01_R_Finger4|Bip01_R_Finger41";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".t" -type "double3" 3.3974 0 0 ;
	setAttr ".r" -type "double3" 0 0 -180 ;
	setAttr ".ssc" no;
	setAttr ".fbxID" 5;
createNode joint -n "Bip01_L_Clavicle" -p "|Stand_Wave_3|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".t" -type "double3" -0.8268 -0.6612 3.9165 ;
	setAttr ".s" -type "double3" 1 1.0000000000000002 1 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" 0.99454799999999999 9.9454800000000002e-007 -0.104286 0
		 -9.8906500000000008e-007 1 1.0428599999999999e-007 0 0.104286 -5.7179000000000015e-010 0.99454799999999999 0
		 3.9165070000000002 2.9252039999999999 138.82619700000001 1;
	setAttr -k on ".liw";
	setAttr ".fbxID" 5;
createNode joint -n "Bip01_L_UpperArm" -p "|Stand_Wave_3|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_L_Clavicle";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".t" -type "double3" 10.6745 0 0 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" 0.75332500000000002 0.100004 -0.65000000000000002 0
		 -0.071785600000000005 0.99496899999999999 0.069882299999999994 0 0.65371900000000005 -0.0059834299999999997 0.756714 0
		 14.532825000000001 2.925173 137.712974 1;
	setAttr -k on ".liw";
	setAttr ".fbxID" 5;
createNode joint -n "Bip01_L_Forearm" -p "|Stand_Wave_3|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_L_Clavicle|Bip01_L_UpperArm";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".t" -type "double3" 25.3374 0 0 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" 0.752861 -0.095943299999999995 -0.65114899999999998 0
		 0.076497700000000002 0.99536899999999995 -0.058215299999999998 0 0.65371900000000005 -0.0059834199999999997 0.756714 0
		 33.620189000000003 5.4590990000000001 121.243576 1;
	setAttr -k on ".liw";
	setAttr ".fbxID" 5;
createNode joint -n "Bip01_L_Hand" -p "|Stand_Wave_3|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_L_Clavicle|Bip01_L_UpperArm|Bip01_L_Forearm";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".t" -type "double3" 24.9881 0 0 ;
	setAttr ".r" -type "double3" -90.133325 6.300397000000002 2.565332999999999 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" 0.67922800000000005 -0.050329499999999999 -0.73219999999999996 0
		 -0.73278100000000002 0.0092525000000000003 -0.68040299999999998 0 0.041019 0.99868999999999997 -0.030595899999999999 0
		 52.432744999999997 3.0615610000000002 104.972662 1;
	setAttr -k on ".liw";
	setAttr ".fbxID" 5;
createNode joint -n "Bip01_L_Finger0" -p "|Stand_Wave_3|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_L_Clavicle|Bip01_L_UpperArm|Bip01_L_Forearm|Bip01_L_Hand";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".t" -type "double3" 2.3036000000000003 1.3941 -2.9222 ;
	setAttr ".r" -type "double3" 81.930180000000021 23.314181000000019 18.295909 ;
	setAttr ".s" -type "double3" 0.99999999999999989 1 1.0000000000000002 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" 0.36474600000000001 -0.43647000000000002 -0.82246900000000001 0
		 0.072258799999999998 0.89392799999999994 -0.44234699999999999 0 0.92830000000000001 0.101913 0.357595 0
		 52.856034999999999 0.0402022 102.42681 1;
	setAttr -k on ".liw";
	setAttr ".fbxID" 5;
createNode joint -n "Bip01_L_Finger01" -p "|Stand_Wave_3|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_L_Clavicle|Bip01_L_UpperArm|Bip01_L_Forearm|Bip01_L_Hand|Bip01_L_Finger0";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".t" -type "double3" 4.0806 0 0 ;
	setAttr ".r" -type "double3" 0 0 24.06455200000001 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" 0.36250900000000003 -0.034021900000000001 -0.93135900000000005 0
		 -0.082751900000000003 0.99421199999999998 -0.068527000000000005 0 0.92829899999999999 0.101913 0.357595 0
		 54.344436000000002 -1.7408459999999999 99.070606999999995 1;
	setAttr -k on ".liw";
	setAttr ".fbxID" 5;
createNode joint -n "_notused_Bip01_L_Finger0Nub" -p "|Stand_Wave_3|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_L_Clavicle|Bip01_L_UpperArm|Bip01_L_Forearm|Bip01_L_Hand|Bip01_L_Finger0|Bip01_L_Finger01";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".t" -type "double3" 3.3413 0 0 ;
	setAttr ".ssc" no;
	setAttr ".fbxID" 5;
createNode joint -n "Bip01_L_Finger2" -p "|Stand_Wave_3|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_L_Clavicle|Bip01_L_UpperArm|Bip01_L_Forearm|Bip01_L_Hand";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".t" -type "double3" 7.3709 -0.4816 -0.6341 ;
	setAttr ".r" -type "double3" -0.043610000000000031 0.01341 17.086619000000006 ;
	setAttr ".s" -type "double3" 1 0.99999999999999989 0.99999999999999989 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" 0.43393500000000002 -0.045622700000000002 -0.89978899999999995 0
		 -0.90003599999999995 0.022871599999999999 -0.43521399999999999 0 0.040435199999999998 0.99869699999999995 -0.031137399999999999 0
		 57.766060000000003 2.0529269999999999 99.922754999999995 1;
	setAttr -k on ".liw";
	setAttr ".fbxID" 5;
createNode joint -n "Bip01_L_Finger21" -p "|Stand_Wave_3|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_L_Clavicle|Bip01_L_UpperArm|Bip01_L_Forearm|Bip01_L_Hand|Bip01_L_Finger2";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".t" -type "double3" 3.8208 0 0 ;
	setAttr ".r" -type "double3" -4.0000000000000024e-006 -4.0000000000000015e-006 29.946390000000019 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" -0.0732873 -0.0281141 -0.99691399999999997 0 -0.99649100000000002 0.042592999999999999 0.072054999999999994 0
		 0.040435899999999997 0.99869699999999995 -0.031137000000000001 0 59.424058000000002 1.878614 96.484800000000007 1;
	setAttr -k on ".liw";
	setAttr ".fbxID" 5;
createNode joint -n "_notused_Bip01_L_Finger2Nub" -p "|Stand_Wave_3|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_L_Clavicle|Bip01_L_UpperArm|Bip01_L_Forearm|Bip01_L_Hand|Bip01_L_Finger2|Bip01_L_Finger21";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".t" -type "double3" 4.7174000000000005 0 0 ;
	setAttr ".ssc" no;
	setAttr ".fbxID" 5;
createNode joint -n "Bip01_L_Finger1" -p "|Stand_Wave_3|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_L_Clavicle|Bip01_L_UpperArm|Bip01_L_Forearm|Bip01_L_Hand";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".t" -type "double3" 7.3263 -0.211 -2.2784 ;
	setAttr ".r" -type "double3" 0.73310799999999965 5.3145129999999989 22.646742999999997 ;
	setAttr ".s" -type "double3" 1 0.99999999999999989 0.99999999999999989 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" 0.33942 -0.13520399999999999 -0.93086800000000003 0
		 -0.93680799999999997 0.040590800000000003 -0.34748099999999998 0 0.084765400000000005 0.98998600000000003 -0.112883 0
		 57.470143999999998 0.415574 99.821611000000004 1;
	setAttr -k on ".liw";
	setAttr ".fbxID" 5;
createNode joint -n "Bip01_L_Finger11" -p "|Stand_Wave_3|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_L_Clavicle|Bip01_L_UpperArm|Bip01_L_Forearm|Bip01_L_Hand|Bip01_L_Finger1";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".t" -type "double3" 3.5597999999999996 0 0 ;
	setAttr ".r" -type "double3" -2.0000000000000012e-006 3.0000000000000005e-006 22.627677000000009 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" -0.047135499999999997 -0.10918 -0.99290400000000001 0
		 -0.99528499999999998 0.089479600000000006 0.037409299999999999 0 0.0847604 0.98998699999999995 -0.112883 0
		 58.678319000000002 -0.065726099999999996 96.507845000000003 1;
	setAttr -k on ".liw";
	setAttr ".fbxID" 5;
createNode joint -n "_notused_Bip01_L_Finger1Nub" -p "|Stand_Wave_3|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_L_Clavicle|Bip01_L_UpperArm|Bip01_L_Forearm|Bip01_L_Hand|Bip01_L_Finger1|Bip01_L_Finger11";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".t" -type "double3" 4.1404000000000005 0 0 ;
	setAttr ".ssc" no;
	setAttr ".fbxID" 5;
createNode joint -n "Bip01_L_Finger3" -p "|Stand_Wave_3|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_L_Clavicle|Bip01_L_UpperArm|Bip01_L_Forearm|Bip01_L_Hand";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".t" -type "double3" 7.2345 -0.5997 0.9332 ;
	setAttr ".r" -type "double3" -0.043362000000000123 -3.826458 18.509666999999997 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" 0.41327900000000001 0.021958700000000001 -0.91034000000000004 0
		 -0.91051400000000005 0.0239966 -0.41277999999999998 0 0.012781000000000001 0.999471 0.029911 0
		 57.824314000000001 3.6238670000000002 100.055049 1;
	setAttr -k on ".liw";
	setAttr ".fbxID" 5;
createNode joint -n "Bip01_L_Finger31" -p "|Stand_Wave_3|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_L_Clavicle|Bip01_L_UpperArm|Bip01_L_Forearm|Bip01_L_Hand|Bip01_L_Finger3";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".t" -type "double3" 3.7241 0 0 ;
	setAttr ".r" -type "double3" 2e-006 -9.4787915988669346e-023 27.984288 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" -0.062282200000000003 0.030651100000000001 -0.99758800000000003 0
		 -0.99797599999999997 0.0108933 0.062641199999999994 0 0.012787099999999999 0.999471 0.029910599999999999 0
		 59.363363 3.7056490000000002 96.664751999999993 1;
	setAttr -k on ".liw";
	setAttr ".fbxID" 5;
createNode joint -n "_notused_Bip01_L_Finger3Nub" -p "|Stand_Wave_3|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_L_Clavicle|Bip01_L_UpperArm|Bip01_L_Forearm|Bip01_L_Hand|Bip01_L_Finger3|Bip01_L_Finger31";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".t" -type "double3" 4.2397 0 0 ;
	setAttr ".ssc" no;
	setAttr ".fbxID" 5;
createNode joint -n "Bip01_L_Finger4" -p "|Stand_Wave_3|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_L_Clavicle|Bip01_L_UpperArm|Bip01_L_Forearm|Bip01_L_Hand";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".t" -type "double3" 7.2292 -0.4187 2.3639 ;
	setAttr ".r" -type "double3" 0.17713199999999998 -5.5619539999999983 16.615218000000002 ;
	setAttr ".s" -type "double3" 1 0.99999999999999989 1 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" 0.44323200000000001 0.051427599999999997 -0.89493100000000003 0
		 -0.89640600000000004 0.026343800000000001 -0.44244899999999998 0 0.00082181400000000003 0.99832900000000002 0.057776399999999999 0
		 57.746775 5.054627 99.891990000000007 1;
	setAttr -k on ".liw";
	setAttr ".fbxID" 5;
createNode joint -n "Bip01_L_Finger41" -p "|Stand_Wave_3|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_L_Clavicle|Bip01_L_UpperArm|Bip01_L_Forearm|Bip01_L_Hand|Bip01_L_Finger4";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".t" -type "double3" 3.4791000000000003 0 0 ;
	setAttr ".r" -type "double3" 2e-006 -3.0000000000000009e-006 29.444363999999997 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" -0.054672800000000001 0.057735500000000002 -0.996834 0
		 -0.99850399999999995 -0.00234096 0.054628799999999998 0 0.00082047800000000003 0.99832900000000002 0.057777200000000001 0
		 59.288862000000002 5.2335589999999996 96.778389000000004 1;
	setAttr -k on ".liw";
	setAttr ".fbxID" 5;
createNode joint -n "_notused_Bip01_L_Finger4Nub" -p "|Stand_Wave_3|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_L_Clavicle|Bip01_L_UpperArm|Bip01_L_Forearm|Bip01_L_Hand|Bip01_L_Finger4|Bip01_L_Finger41";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".t" -type "double3" 3.3974 0 0 ;
	setAttr ".ssc" no;
	setAttr ".fbxID" 5;
createNode joint -n "Bip01_L_Thigh" -p "|Stand_Wave_3|Bip01_Spine";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".t" -type "double3" -11.0456 2.2584 8.5201999999999991 ;
	setAttr ".s" -type "double3" 0.99999999999999989 0.99999999999999989 0.99999999999999989 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" 0.059694200000000003 0.0067868399999999997 -0.998193 0
		 0.0013515700000000001 -0.99997599999999998 -0.00671814 0 -0.99821599999999999 -0.00094809699999999996 -0.059701999999999998 0
		 8.5202019999999994 4.3064900000000002e-005 88.396292000000003 1;
	setAttr -k on ".liw";
	setAttr ".fbxID" 5;
createNode joint -n "Bip01_L_Calf" -p "|Stand_Wave_3|Bip01_Spine|Bip01_L_Thigh";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".t" -type "double3" 36.1527 0 0 ;
	setAttr ".s" -type "double3" 1 0.99999999999999989 0.99999999999999989 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" 0.059157700000000001 0.119882 -0.99102400000000002 0
		 0.0080950799999999993 -0.99278699999999998 -0.119612 0 -0.99821599999999999 -0.00094644599999999998 -0.059701400000000002 0
		 10.678299000000001 0.245397 52.308965999999998 1;
	setAttr -k on ".liw";
	setAttr ".fbxID" 5;
createNode joint -n "Bip01_L_Foot" -p "|Stand_Wave_3|Bip01_Spine|Bip01_L_Thigh|Bip01_L_Calf";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".t" -type "double3" 43.2543 0 0 ;
	setAttr ".s" -type "double3" 0.99999999999999989 1 1 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" 0 0 -1 0 0.123471 -0.99234800000000001 0 0 -0.99234800000000001 -0.123471 0 0
		 13.237131 5.4308170000000002 9.4429110000000005 1;
	setAttr -k on ".liw";
	setAttr ".fbxID" 5;
createNode joint -n "Bip01_L_Toe0" -p "|Stand_Wave_3|Bip01_Spine|Bip01_L_Thigh|Bip01_L_Calf|Bip01_L_Foot";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".t" -type "double3" 9.4714999999999989 11.2377 0 ;
	setAttr ".r" -type "double3" -5.000000000000003e-006 1.9999999999999995e-006 89.999979999999979 ;
	setAttr ".s" -type "double3" 0.99999999999999989 1 1 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" 0.123471 -0.99234800000000001 0 0 0 0 1 0 -0.99234800000000001 -0.123471 0 0
		 14.624665999999999 -5.7209120000000002 -0.028559000000000001 1;
	setAttr -k on ".liw";
	setAttr ".fbxID" 5;
createNode joint -n "_notused_Bip01_L_Toe0Nub" -p "|Stand_Wave_3|Bip01_Spine|Bip01_L_Thigh|Bip01_L_Calf|Bip01_L_Foot|Bip01_L_Toe0";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".t" -type "double3" 7.2685 0 0 ;
	setAttr ".r" -type "double3" 0 0 180 ;
	setAttr ".ssc" no;
	setAttr ".fbxID" 5;
createNode joint -n "Bip01_R_Thigh" -p "|Stand_Wave_3|Bip01_Spine";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".t" -type "double3" -11.0456 2.2585 -8.5201999999999991 ;
	setAttr ".s" -type "double3" 0.99999999999999956 1 0.99999999999999989 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" -0.059694200000000003 0.0067826700000000002 -0.998193 0
		 -0.00134545 -0.99997599999999998 -0.0067143300000000001 0 -0.99821599999999999 0.00094221699999999997 0.059701900000000002 0
		 -8.5201879999999992 1.40303e-005 88.396298000000002 1;
	setAttr -k on ".liw";
	setAttr ".fbxID" 5;
createNode joint -n "Bip01_R_Calf" -p "|Stand_Wave_3|Bip01_Spine|Bip01_R_Thigh";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".t" -type "double3" 36.1527 0 0 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" -0.059158000000000002 0.11988699999999999 -0.99102400000000002 0
		 -0.0080972100000000005 -0.99278699999999998 -0.119617 0 -0.99821599999999999 0.00094822899999999998 0.059701999999999998 0
		 -10.678288 0.24518599999999999 52.308996999999998 1;
	setAttr -k on ".liw";
	setAttr ".fbxID" 5;
createNode joint -n "Bip01_R_Foot" -p "|Stand_Wave_3|Bip01_Spine|Bip01_R_Thigh|Bip01_R_Calf";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".t" -type "double3" 43.2543 0 0 ;
	setAttr ".s" -type "double3" 1 1 0.99999999999999989 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" 0 0 -1 0 -0.123469 -0.99234800000000001 0 0 -0.99234800000000001 0.123469 0 0
		 -13.237124 5.4308209999999999 9.442933 1;
	setAttr -k on ".liw";
	setAttr ".fbxID" 5;
createNode joint -n "Bip01_R_Toe0" -p "|Stand_Wave_3|Bip01_Spine|Bip01_R_Thigh|Bip01_R_Calf|Bip01_R_Foot";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".t" -type "double3" 9.4714999999999989 11.2377 0 ;
	setAttr ".r" -type "double3" 1.9999999999999991e-006 -1.9999999999999995e-006 89.999986 ;
	setAttr ".s" -type "double3" 0.99999999999999989 0.99999999999999989 1 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" -0.12346799999999999 -0.99234800000000001 0 0 0 0 1 0
		 -0.99234800000000001 0.12346799999999999 0 0 -14.624625999999999 -5.7209019999999997 -0.028538999999999998 1;
	setAttr -k on ".liw";
	setAttr ".fbxID" 5;
createNode joint -n "_notused_Bip01_R_Toe0Nub" -p "|Stand_Wave_3|Bip01_Spine|Bip01_R_Thigh|Bip01_R_Calf|Bip01_R_Foot|Bip01_R_Toe0";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".t" -type "double3" 7.2685 0 0 ;
	setAttr ".ssc" no;
	setAttr ".fbxID" 5;
createNode animCurveTL -n "Bip01_Spine_translateX6";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 63 ".ktv[0:62]"  0 3.9734039306640625 0.8 3.9261553287506104
		 1.6 3.8777883052825928 2.4 3.8430254459381104 3.2 3.8085041046142578 4 3.7527506351470943
		 4.8 3.6738100051879878 5.6 3.5733215808868408 6.4 3.4385745525360107 7.2 3.267536878585815
		 8 3.0654258728027344 8.8 2.8452358245849609 9.6 2.6438696384429932 10.4 2.4704079627990723
		 11.2 2.2942211627960205 12 2.1233253479003902 12.8 1.9942458868026731 13.6 1.9031420946121216
		 14.4 1.8180888891220093 15.2 1.7190521955490112 16 1.6223037242889404 16.8 1.5541696548461914
		 17.6 1.5060317516326904 18.4 1.4571672677993774 19.2 1.4004768133163452 20 1.3447147607803345
		 20.8 1.3103731870651243 21.6 1.2973818778991699 22.4 1.2875691652297974 23.2 1.2815260887145996
		 24 1.283402681350708 24.8 1.2793862819671633 25.6 1.2669471502304075 26.4 1.2603657245635986
		 27.2 1.2576268911361694 28 1.2531006336212158 28.8 1.2643365859985352 29.6 1.2984037399291992
		 30.4 1.3388761281967163 31.2 1.3815248012542725 32 1.4270981550216677 32.8 1.4668354988098145
		 33.6 1.5017827749252319 34.4 1.5521845817565918 35.2 1.644750714302063 36 1.7564928531646729
		 36.8 1.8371323347091677 37.6 1.9224028587341309 38.4 2.0715806484222412 39.2 2.2781429290771484
		 40 2.5160064697265625 40.8 2.7229232788085937 41.6 2.8513343334197998 42.4 2.9731903076171875
		 43.2 3.1639688014984131 44 3.4094202518463135 44.8 3.6952874660491943 45.6 4.0028567314147949
		 46.4 4.2747468948364258 47.2 4.4945549964904785 48 4.7112908363342285 48.8 4.9854373931884766
		 49.6 5.3332271575927734;
createNode animCurveTL -n "Bip01_Spine_translateY6";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 63 ".ktv[0:62]"  0 101.88526153564452 0.8 101.88018798828124
		 1.6 101.8710479736328 2.4 101.85999298095705 3.2 101.85195159912108 4 101.84804534912108
		 4.8 101.84568786621094 5.6 101.84557342529295 6.4 101.84934997558594 7.2 101.85319519042967
		 8 101.85274505615234 8.8 101.84741973876952 9.6 101.83958435058594 10.4 101.83485412597656
		 11.2 101.83454132080078 12 101.83261108398436 12.8 101.82511901855467 13.6 101.81430053710936
		 14.4 101.80773162841795 15.2 101.80860137939452 16 101.80994415283205 16.8 101.80832672119141
		 17.6 101.80411529541016 18.4 101.7974853515625 19.2 101.79096221923828 20 101.78424835205078
		 20.8 101.77664184570312 21.6 101.7685089111328 22.4 101.75865936279295 23.2 101.74754333496094
		 24 101.73624420166016 24.8 101.7266845703125 25.6 101.7209014892578 26.4 101.71624755859376
		 27.2 101.70906829833984 28 101.70160675048828 28.8 101.69561767578124 29.6 101.68910980224608
		 30.4 101.68467712402344 31.2 101.68385314941406 32 101.68044281005859 32.8 101.67253112792967
		 33.6 101.66824340820312 34.4 101.6735382080078 35.2 101.6865234375 36 101.70346832275392
		 36.8 101.72008514404295 37.6 101.73433685302734 38.4 101.75078582763672 39.2 101.77333068847656
		 40 101.80121612548828 40.8 101.83156585693359 41.6 101.85758209228516 42.4 101.88201141357422
		 43.2 101.91490173339844 44 101.95085906982422 44.8 101.97613525390624 45.6 101.98723602294922
		 46.4 101.98947143554687 47.2 101.99111175537108 48 101.9980926513672 48.8 102.00588989257812
		 49.6 102.00740051269533;
createNode animCurveTL -n "Bip01_Spine_translateZ6";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 63 ".ktv[0:62]"  0 32.393707275390625 0.8 32.466312408447266
		 1.6 32.544910430908203 2.4 32.608417510986328 3.2 32.656074523925781 4 32.708789825439453
		 4.8 32.780406951904297 5.6 32.868869781494141 6.4 32.968959808349609 7.2 33.070148468017578
		 8 33.169662475585937 8.8 33.276222229003906 9.6 33.384353637695313 10.4 33.482894897460937
		 11.2 33.574993133544922 12 33.662307739257813 12.8 33.748580932617187 13.6 33.843887329101563
		 14.4 33.932079315185547 15.2 33.988811492919922 16 34.0252685546875 16.8 34.061592102050781
		 17.6 34.093528747558594 18.4 34.11737060546875 19.2 34.142539978027344 20 34.167308807373047
		 20.8 34.181076049804687 21.6 34.184341430664063 22.4 34.182613372802734 23.2 34.172187805175781
		 24 34.150413513183594 24.8 34.129661560058594 25.6 34.122333526611328 26.4 34.116245269775391
		 27.2 34.089424133300781 28 34.048553466796875 28.8 34.011848449707031 29.6 33.975536346435547
		 30.4 33.933364868164062 31.2 33.880775451660156 32 33.808174133300781 32.8 33.724193572998047
		 33.6 33.647411346435547 34.4 33.568134307861328 35.2 33.449638366699219 36 33.298381805419922
		 36.8 33.168605804443359 37.6 33.055267333984375 38.4 32.916671752929688 39.2 32.756328582763672
		 40 32.578449249267578 40.8 32.391979217529297 41.6 32.227500915527344 42.4 32.065929412841797
		 43.2 31.871833801269531 44 31.664228439331055 44.8 31.460840225219727 45.6 31.254398345947266
		 46.4 31.062808990478516 47.2 30.89811897277832 48 30.726589202880859 48.8 30.518083572387695
		 49.6 30.274618148803711;
createNode animCurveTA -n "Bip01_Spine_rotateX6";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 63 ".ktv[0:62]"  0 49.930576324462891 0.8 50.084968566894531
		 1.6 50.241958618164062 2.4 50.408985137939453 3.2 50.573070526123047 4 50.720512390136719
		 4.8 50.854507446289062 5.6 50.980915069580078 6.4 51.09832763671875 7.2 51.20661926269532
		 8 51.304595947265632 8.8 51.392189025878906 9.6 51.478588104248047 10.4 51.565284729003913
		 11.2 51.636375427246094 12 51.687889099121094 12.8 51.734249114990234 13.6 51.770442962646477
		 14.4 51.777805328369141 15.2 51.769920349121101 16 51.769596099853523 16.8 51.76904296875
		 17.6 51.7606201171875 18.4 51.745880126953125 19.2 51.715930938720703 20 51.669136047363288
		 20.8 51.618309020996094 21.6 51.573230743408203 22.4 51.529960632324219 23.2 51.474739074707031
		 24 51.404117584228509 24.8 51.330207824707031 25.6 51.260723114013679 26.4 51.192783355712891
		 27.2 51.12451171875 28 51.059619903564453 28.8 50.999492645263672 29.6 50.936679840087891
		 30.4 50.870113372802734 31.2 50.817272186279297 32 50.785594940185547 32.8 50.755172729492195
		 33.6 50.705776214599609 34.4 50.630435943603523 35.2 50.537960052490241 36 50.450569152832038
		 36.8 50.366867065429688 37.6 50.172103881835938 38.4 49.588512420654297 39.2 48.451183319091797
		 40 47.056297302246094 40.8 45.931228637695313 41.6 45.293254852294922 42.4 45.000576019287109
		 43.2 44.867698669433594 44 44.754894256591797 44.8 44.577655792236328 45.6 44.370998382568359
		 46.4 44.154884338378906 47.2 43.878704071044922 48 43.578937530517578 48.8 43.298187255859375
		 49.6 42.975238800048835;
createNode animCurveTA -n "Bip01_Spine_rotateY6";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 63 ".ktv[0:62]"  0 0.93788266181945823 0.8 1.0174398422241211
		 1.6 1.0927102565765381 2.4 1.1304211616516111 3.2 1.1635104417800903 4 1.2880792617797852
		 4.8 1.4915083646774292 5.6 1.6807177066802981 6.4 1.836961507797241 7.2 1.9851845502853394
		 8 2.1357555389404297 8.8 2.2947812080383301 9.6 2.4555909633636479 10.4 2.6122174263000488
		 11.2 2.766995906829834 12 2.9341814517974858 12.8 3.1545100212097168 13.6 3.4182448387145996
		 14.4 3.6143100261688232 15.2 3.6636970043182382 16 3.6377806663513184 16.8 3.6581659317016597
		 17.6 3.7470374107360831 18.4 3.8556346893310547 19.2 3.9547049999237061 20 4.0393190383911133
		 20.8 4.1132059097290039 21.6 4.1844372749328613 22.4 4.2535285949707031 23.2 4.3095083236694336
		 24 4.3471717834472656 24.8 4.3766698837280273 25.6 4.4061627388000488 26.4 4.432309627532959
		 27.2 4.4494795799255371 28 4.4572229385375977 28.8 4.4589123725891113 29.6 4.4569549560546875
		 30.4 4.4587888717651367 31.2 4.4784731864929208 32 4.5093250274658203 32.8 4.5206661224365234
		 33.6 4.4939136505126953 34.4 4.4277567863464355 35.2 4.3366050720214844 36 4.2567024230957031
		 36.8 4.1912221908569336 37.6 3.9923658370971684 38.4 3.3607423305511475 39.2 2.211522102355957
		 40 0.9857235550880431 40.8 0.21930523216724396 41.6 -0.052801430225372314 42.4 -0.15193882584571838
		 43.2 -0.27704450488090515 44 -0.45084235072135925 44.8 -0.66894733905792236 45.6 -0.89260405302047741
		 46.4 -1.0409059524536133 47.2 -1.0774506330490112 48 -1.0607980489730835 48.8 -1.0069406032562256
		 49.6 -0.88730847835540771;
createNode animCurveTA -n "Bip01_Spine_rotateZ6";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 63 ".ktv[0:62]"  0 0.44414076209068298 0.8 0.44617441296577454
		 1.6 0.43488436937332159 2.4 0.40132749080657959 3.2 0.35221385955810552 4 0.32221710681915283
		 4.8 0.32343292236328125 5.6 0.33007815480232239 6.4 0.32581570744514465 7.2 0.31379064917564392
		 8 0.29801958799362183 8.8 0.27738305926322937 9.6 0.24693220853805539 10.4 0.20613670349121097
		 11.2 0.16190776228904724 12 0.12676112353801727 12.8 0.11726219952106476 13.6 0.12367226928472522
		 14.4 0.096065469086170197 15.2 0.010084814392030241 16 -0.096681453287601471 16.8 -0.18485970795154572
		 17.6 -0.25491753220558167 18.4 -0.32256230711936951 19.2 -0.39254570007324219 20 -0.46226617693901062
		 20.8 -0.53116798400878906 21.6 -0.59999614953994762 22.4 -0.66915947198867798 23.2 -0.73641353845596325
		 24 -0.79907995462417603 24.8 -0.86166846752166759 25.6 -0.92896765470504772 26.4 -0.99578237533569336
		 27.2 -1.054324746131897 28 -1.1045801639556885 28.8 -1.1523134708404541 29.6 -1.2019367218017578
		 30.4 -1.2567610740661621 31.2 -1.319915771484375 32 -1.3849965333938601 32.8 -1.4395225048065186
		 33.6 -1.4768383502960205 34.4 -1.4914964437484739 35.2 -1.4906140565872192 36 -1.4978593587875366
		 36.8 -1.5065259933471682 37.6 -1.4185012578964231 38.4 -1.0640126466751101 39.2 -0.37726327776908875
		 40 0.4509371817111969 40.8 1.0880507230758667 41.6 1.3786566257476809 42.4 1.4508794546127319
		 43.2 1.4776045083999634 44 1.5048121213912964 44.8 1.4889253377914429 45.6 1.3864215612411499
		 46.4 1.2496551275253296 47.2 1.1706538200378418 48 1.1437188386917114 48.8 1.1339970827102659
		 49.6 1.1436052322387695;
createNode animCurveTA -n "Bip01_Spine1_rotateX6";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 63 ".ktv[0:62]"  0 1.3671120405197144 0.8 1.3118793964385986
		 1.6 1.2540361881256104 2.4 1.193623423576355 3.2 1.1309159994125368 4 1.0662552118301392
		 4.8 1.0000134706497192 5.6 0.93249523639678966 6.4 0.86415177583694469 7.2 0.795487880706787
		 8 0.72705775499343872 8.8 0.65938514471054077 9.6 0.5930016040802002 10.4 0.52845364809036255
		 11.2 0.46625274419784546 12 0.40690663456916809 12.8 0.35089936852455139 13.6 0.29872661828994751
		 14.4 0.25089606642723083 15.2 0.20783068239688873 16 0.16999301314353943 16.8 0.13901036977767944
		 17.6 0.11861123144626616 18.4 0.08878837525844574 19.2 0.069854073226451874 20 0.066283173859119415
		 20.8 0.059656348079442978 21.6 0.06031382828950882 22.4 0.0672793909907341 23.2 0.079842336475849152
		 24 0.099625110626220703 24.8 0.13017009198665619 25.6 0.14096918702125549 26.4 0.1851385086774826
		 27.2 0.24059340357780457 28 0.27289542555809021 28.8 0.31938213109970093 29.6 0.37261739373207087
		 30.4 0.4297507107257843 31.2 0.49034580588340754 32 0.55386841297149658 32.8 0.61968636512756348
		 33.6 0.68697530031204224 34.4 0.75479567050933838 35.2 0.82209014892578125 36 0.88779574632644653
		 36.8 0.95079320669174161 37.6 1.0099303722381592 38.4 1.064129114151001 39.2 1.1124304533004761
		 40 1.1539539098739624 40.8 1.1868393421173096 41.6 1.2082589864730835 42.4 1.2359006404876709
		 43.2 1.2394218444824221 44 1.238123893737793 44.8 1.232452392578125 45.6 1.2007570266723633
		 46.4 1.1779325008392334 47.2 1.1436636447906494 48 1.1011736392974854 48.8 1.0525895357131958
		 49.6 0.9990062713623048;
createNode animCurveTA -n "Bip01_Spine1_rotateY6";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 63 ".ktv[0:62]"  0 0.15069329738616943 0.8 0.18660058081150055
		 1.6 0.22430633008480072 2.4 0.2634652853012085 3.2 0.30378720164299011 4 0.34506434202194214
		 4.8 0.38706034421920776 5.6 0.42947140336036682 6.4 0.47191673517227184 7.2 0.51402997970581055
		 8 0.55549818277359009 8.8 0.59609365463256836 9.6 0.63559943437576294 10.4 0.67381781339645386
		 11.2 0.71056604385375977 12 0.7457004189491272 12.8 0.77906852960586548 13.6 0.8105757236480714
		 14.4 0.84016287326812733 15.2 0.86779636144638062 16 0.89344322681427024 16.8 0.91587215662002563
		 17.6 0.9315258264541626 18.4 0.95921921730041515 19.2 0.98233932256698597 20 0.98825263977050759
		 20.8 1.0059190988540647 21.6 1.0186281204223633 22.4 1.0283554792404177 23.2 1.0359926223754885
		 24 1.0414943695068359 24.8 1.0449326038360596 25.6 1.0452767610549929 26.4 1.0436903238296509
		 27.2 1.0378422737121582 28 1.0326247215270996 28.8 1.0241146087646484 29.6 1.0128047466278076
		 30.4 0.99888509511947643 31.2 0.98234444856643677 32 0.96320933103561401 32.8 0.94150131940841664
		 33.6 0.91728579998016369 34.4 0.89068067073822021 35.2 0.86188000440597534 36 0.83111721277236938
		 36.8 0.79861128330230724 37.6 0.76453351974487316 38.4 0.72903716564178478 39.2 0.69238793849945068
		 40 0.65494495630264282 40.8 0.61946791410446167 41.6 0.59103429317474365 42.4 0.52990549802780151
		 43.2 0.50255167484283447 44 0.4712978601455689 44.8 0.44525682926177979 45.6 0.39244204759597784
		 46.4 0.37005069851875305 47.2 0.34374946355819702 48 0.31784495711326599 48.8 0.29457324743270874
		 49.6 0.27400538325309753;
createNode animCurveTA -n "Bip01_Spine1_rotateZ6";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 63 ".ktv[0:62]"  0 -0.38168871402740479 0.8 -0.39852029085159302
		 1.6 -0.41533577442169189 2.4 -0.43184536695480347 3.2 -0.44780039787292486 4 -0.46302187442779552
		 4.8 -0.47729620337486267 5.6 -0.49043759703636158 6.4 -0.50222295522689819 7.2 -0.51244616508483887
		 8 -0.52088749408721924 8.8 -0.52742093801498413 9.6 -0.53195428848266602 10.4 -0.53448456525802612
		 11.2 -0.53496044874191284 12 -0.53340250253677368 12.8 -0.52983975410461426 13.6 -0.52446544170379639
		 14.4 -0.51749914884567261 15.2 -0.50913858413696289 16 -0.49949091672897333 16.8 -0.48941290378570562
		 17.6 -0.48151266574859625 18.4 -0.4636711478233338 19.2 -0.4452815055847168 20 -0.43982544541358942
		 20.8 -0.42141476273536682 21.6 -0.4057916402816773 22.4 -0.39137327671051025 23.2 -0.37746313214302063
		 24 -0.36358374357223511 24.8 -0.34865531325340271 25.6 -0.34458154439926147 26.4 -0.33230674266815186
		 27.2 -0.32284176349639893 28 -0.32009822130203247 28.8 -0.31754368543624878 29.6 -0.31672006845474243
		 30.4 -0.3181840181350708 31.2 -0.32200950384140015 32 -0.32819280028343201 32.8 -0.33656904101371765
		 33.6 -0.34694314002990723 34.4 -0.35904443264007568 35.2 -0.37261989712715154 36 -0.38729864358901978
		 36.8 -0.40261998772621149 37.6 -0.41804158687591553 38.4 -0.43304324150085449 39.2 -0.44717037677764898
		 40 -0.4600039422512055 40.8 -0.47068822383880615 41.6 -0.47798103094100958 42.4 -0.4885140061378479
		 43.2 -0.4905027449131012 44 -0.49106052517890936 44.8 -0.48995298147201538 45.6 -0.48053029179573059
		 46.4 -0.47314575314521784 47.2 -0.46175143122673035 48 -0.44725865125656128 48.8 -0.43029630184173584
		 49.6 -0.41121900081634521;
createNode animCurveTA -n "Bip01_Spine2_rotateX6";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 63 ".ktv[0:62]"  0 4.7812972068786621 0.8 4.9103827476501465
		 1.6 4.9138917922973633 2.4 4.5795764923095703 3.2 4.0415811538696289 4 3.5990471839904785
		 4.8 3.3206169605255127 5.6 3.0938296318054199 6.4 2.8642354011535645 7.2 2.6335844993591309
		 8 2.3977673053741455 8.8 2.107421875 9.6 1.6879463195800781 10.4 1.2022837400436399
		 11.2 0.84355390071868896 12 0.70486772060394287 12.8 0.70811641216278076 13.6 0.68418186902999878
		 14.4 0.54491740465164185 15.2 0.38175126910209656 16 0.27645388245582581 16.8 0.20474161207675937
		 17.6 0.14942917227745056 18.4 0.11763692647218706 19.2 0.087669424712657915 20 0.036541052162647247
		 20.8 -0.013429867103695868 21.6 -0.03609455376863481 22.4 -0.037346966564655304 23.2 -0.02291872538626194
		 24 0.012948660179972649 24.8 0.058571308851242058 25.6 0.085634112358093262 26.4 0.093677319586277008
		 27.2 0.10984791815280914 28 0.14392474293708801 28.8 0.18686693906784055 29.6 0.23334814608097076
		 30.4 0.28274577856063843 31.2 0.33465561270713806 32 0.38872230052947998 32.8 0.4445188045501709
		 33.6 0.5017123818397522 34.4 0.55987465381622314 35.2 0.61868268251419067 36 0.70237845182418823
		 36.8 0.96603864431381214 37.6 1.6412521600723269 38.4 2.8464972972869873 39.2 4.7263875007629395
		 40 7.0309381484985352 40.8 8.6927595138549805 41.6 9.1171865463256836 42.4 8.7899150848388672
		 43.2 8.1449480056762695 44 7.2935452461242676 44.8 6.3853626251220703 45.6 5.3644189834594727
		 46.4 4.2691001892089844 47.2 3.3946382999420166 48 2.8637006282806396 48.8 2.5712401866912842
		 49.6 2.388336181640625;
createNode animCurveTA -n "Bip01_Spine2_rotateY6";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 63 ".ktv[0:62]"  0 0.42186951637268066 0.8 0.38101285696029663
		 1.6 0.40655606985092158 2.4 0.60597389936447144 3.2 0.90669465065002464 4 1.1564117670059204
		 4.8 1.3230233192443848 5.6 1.4630699157714844 6.4 1.6030160188674929 7.2 1.7416436672210691
		 8 1.881419897079468 8.8 2.0570456981658936 9.6 2.3216657638549805 10.4 2.6287088394165039
		 11.2 2.8444616794586186 12 2.9153420925140381 12.8 2.899019718170166 13.6 2.9031374454498291
		 14.4 2.9856643676757817 15.2 3.0876734256744385 16 3.1543807983398437 16.8 3.2025179862976074
		 17.6 3.2443838119506836 18.4 3.2754101753234868 19.2 3.3093461990356445 20 3.3606560230255127
		 20.8 3.4148604869842538 21.6 3.4553439617156982 22.4 3.4856915473937988 23.2 3.5107278823852539
		 24 3.5301582813262939 24.8 3.5470743179321289 25.6 3.5664937496185303 26.4 3.5875785350799561
		 27.2 3.6052241325378418 28 3.617651224136353 28.8 3.6261382102966313 29.6 3.6314833164215088
		 30.4 3.6339089870452876 31.2 3.6336889266967769 32 3.6311349868774419 32.8 3.6265146732330318
		 33.6 3.6200127601623535 34.4 3.6118426322937007 35.2 3.6023368835449219 36 3.5542869567871098
		 36.8 3.4077396392822266 37.6 3.1795845031738281 38.4 2.9184038639068608 39.2 2.683779239654541
		 40 2.5429863929748535 40.8 2.4082748889923096 41.6 2.2097649574279785 42.4 1.973473072052002
		 43.2 1.6882108449935913 44 1.3574786186218262 44.8 1.1629049777984619 45.6 1.0999135971069336
		 46.4 0.93392288684844971 47.2 0.61643761396408081 48 0.37361171841621399 48.8 0.30866435170173645
		 49.6 0.32191610336303711;
createNode animCurveTA -n "Bip01_Spine2_rotateZ6";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 63 ".ktv[0:62]"  0 -1.2362806797027588 0.8 -1.2585146427154541
		 1.6 -1.2941945791244509 2.4 -1.36236572265625 3.2 -1.4452383518218994 4 -1.5141793489456177
		 4.8 -1.5650409460067749 5.6 -1.6080396175384519 6.4 -1.6469575166702273 7.2 -1.6810544729232788
		 8 -1.7116491794586182 8.8 -1.7505823373794556 9.6 -1.812152624130249 10.4 -1.8771555423736577
		 11.2 -1.9039387702941897 12 -1.8821773529052737 12.8 -1.8335344791412354 13.6 -1.7868990898132324
		 14.4 -1.7568268775939939 15.2 -1.7291920185089111 16 -1.6900342702865601 16.8 -1.6434639692306521
		 17.6 -1.5935007333755491 18.4 -1.5398150682449341 19.2 -1.4842065572738647 20 -1.4285820722579956
		 20.8 -1.3716658353805544 21.6 -1.312139630317688 22.4 -1.250967264175415 23.2 -1.1906780004501345
		 24 -1.1349700689315796 24.8 -1.0814405679702761 25.6 -1.0205368995666504 26.4 -0.95179778337478627
		 27.2 -0.88546431064605724 28 -0.82574796676635742 28.8 -0.76960664987564076 29.6 -0.71542614698410034
		 30.4 -0.66347688436508179 31.2 -0.61413943767547607 32 -0.56764590740203857 32.8 -0.52405309677124023
		 33.6 -0.48330879211425781 34.4 -0.44549265503883362 35.2 -0.41075867414474487 36 -0.33124208450317383
		 36.8 -0.16486270725727081 37.6 -0.12964662909507751 38.4 -0.67247891426086426 39.2 -1.932550311088562
		 40 -3.333332777023315 40.8 -4.0070185661315918 41.6 -3.870952367782591 42.4 -3.5371749401092529
		 43.2 -3.2562918663024902 44 -2.9325699806213379 44.8 -2.5628073215484619 45.6 -2.1441452503204346
		 46.4 -1.7268209457397461 47.2 -1.4333406686782837 48 -1.2725791931152346 48.8 -1.1744257211685181
		 49.6 -1.1004226207733154;
createNode animCurveTA -n "Bip01_R_Clavicle_rotateX6";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 63 ".ktv[0:62]"  0 -0.74009519815444946 0.8 -0.83558666706085205
		 1.6 -0.937574803829193 2.4 -1.0458188056945801 3.2 -1.1597276926040647 4 -1.2789070606231687
		 4.8 -1.4025658369064331 5.6 -1.5300567150115969 6.4 -1.6605113744735718 7.2 -1.7930923700332642
		 8 -1.926791191101074 8.8 -2.0607681274414062 9.6 -2.1941666603088379 10.4 -2.3258557319641113
		 11.2 -2.4548578262329106 12 -2.5804188251495361 12.8 -2.7014436721801762 13.6 -2.8172883987426758
		 14.4 -2.9271576404571533 15.2 -3.030095100402832 16 -3.1258141994476318 16.8 -3.2135457992553711
		 17.6 -3.2927803993225098 18.4 -3.3633553981781006 19.2 -3.4249560832977295 20 -3.4771602153778076
		 20.8 -3.518547534942627 21.6 -3.5450632572174077 22.4 -3.5799024105072021 23.2 -3.5970442295074463
		 24 -3.5977208614349365 24.8 -3.5897059440612789 25.6 -3.5822095870971684 26.4 -3.5424454212188721
		 27.2 -3.5140688419342045 28 -3.471454381942749 28.8 -3.4177229404449463 29.6 -3.3544108867645264
		 30.4 -3.2820215225219727 31.2 -3.2006006240844731 32 -3.1107406616210937 32.8 -3.0129015445709229
		 33.6 -2.9078478813171387 34.4 -2.7958781719207764 35.2 -2.6780519485473633 36 -2.5550076961517334
		 36.8 -2.4276282787322998 37.6 -2.2967796325683594 38.4 -2.1634809970855713 39.2 -2.0286104679107666
		 40 -1.8931994438171391 40.8 -1.7582733631134031 41.6 -1.6247900724411013 42.4 -1.4935303926467896
		 43.2 -1.3654437065124512 44 -1.2414544820785522 44.8 -1.1221331357955933 45.6 -1.0082457065582275
		 46.4 -0.90010201930999756 47.2 -0.79841667413711548 48 -0.70349198579788208 48.8 -0.61551684141159058
		 49.6 -0.53462255001068115;
createNode animCurveTA -n "Bip01_R_Clavicle_rotateY6";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 63 ".ktv[0:62]"  0 79.998924255371094 0.8 80.170585632324219
		 1.6 80.35284423828125 2.4 80.544784545898437 3.2 80.745292663574219 4 80.953102111816406
		 4.8 81.166717529296875 5.6 81.384506225585937 6.4 81.60491943359375 7.2 81.826004028320327
		 8 82.046134948730469 8.8 82.263450622558594 9.6 82.476425170898438 10.4 82.683418273925781
		 11.2 82.882919311523438 12 83.073677062988281 12.8 83.25457763671875 13.6 83.424644470214858
		 14.4 83.583023071289063 15.2 83.729133605957017 16 83.862495422363281 16.8 83.982933044433594
		 17.6 84.090255737304688 18.4 84.184486389160156 19.2 84.265617370605469 20 84.333740234375
		 20.8 84.387100219726563 21.6 84.421058654785156 22.4 84.465629577636719 23.2 84.4874267578125
		 24 84.488265991210938 24.8 84.478141784667969 25.6 84.468582153320341 26.4 84.417739868164062
		 27.2 84.381393432617202 28 84.326255798339844 28.8 84.256072998046875 29.6 84.172599792480469
		 30.4 84.075706481933594 31.2 83.965377807617188 32 83.841728210449233 32.8 83.704971313476562
		 33.6 83.555335998535156 34.4 83.393478393554688 35.2 83.219886779785156 36 83.035446166992202
		 36.8 82.841163635253906 37.6 82.63812255859375 38.4 82.427780151367188 39.2 82.21158599853517
		 40 81.991203308105469 40.8 81.768348693847642 41.6 81.544822692871094 42.4 81.32244873046875
		 43.2 81.102928161621108 44 80.888023376464844 44.8 80.679374694824219 45.6 80.478263854980469
		 46.4 80.286102294921875 47.2 80.103988647460938 48 79.93292236328125 48.8 79.773529052734389
		 49.6 79.626289367675781;
createNode animCurveTA -n "Bip01_R_Clavicle_rotateZ6";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 63 ".ktv[0:62]"  0 173.10638427734375 0.8 173.08377075195312
		 1.6 173.05909729003906 2.4 173.03218078613281 3.2 173.00311279296875 4 172.97172546386719
		 4.8 172.93821716308594 5.6 172.90252685546875 6.4 172.86482238769531 7.2 172.82513427734375
		 8 172.7838134765625 8.8 172.74090576171875 9.6 172.6966552734375 10.4 172.6514892578125
		 11.2 172.60577392578125 12 172.55976867675781 12.8 172.51408386230472 13.6 172.46897888183594
		 14.4 172.42494201660156 15.2 172.38267517089844 16 172.34228515625 16.8 172.30447387695312
		 17.6 172.26972961425781 18.4 172.2381591796875 19.2 172.21012878417969 20 172.18611145019531
		 20.8 172.16677856445312 21.6 172.15432739257812 22.4 172.13789367675781 23.2 172.12974548339844
		 24 172.12942504882812 24.8 172.13325500488281 25.6 172.13681030273435 26.4 172.15556335449219
		 27.2 172.16891479492187 28 172.18873596191406 28.8 172.21340942382812 29.6 172.2421875
		 30.4 172.27444458007812 31.2 172.31016540527344 32 172.3487548828125 32.8 172.38983154296875
		 33.6 172.43275451660156 34.4 172.47743225097656 35.2 172.52302551269531 36 172.56924438476562
		 36.8 172.61557006835935 37.6 172.66165161132812 38.4 172.70698547363281 39.2 172.7513427734375
		 40 172.79435729980469 40.8 172.83573913574219 41.6 172.87525939941406 42.4 172.91290283203125
		 43.2 172.94845581054687 44 172.98171997070313 44.8 173.0128173828125 45.6 173.04159545898435
		 46.4 173.06828308105469 47.2 173.09269714355469 48 173.11492919921875 48.8 173.13511657714844
		 49.6 173.1533203125;
createNode animCurveTA -n "Bip01_R_UpperArm_rotateX6";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 63 ".ktv[0:62]"  0 -57.953960418701165 0.8 -57.513393402099609
		 1.6 -55.797798156738281 2.4 -53.205520629882813 3.2 -51.964740753173821 4 -51.360355377197266
		 4.8 -47.736324310302734 5.6 -39.958858489990234 6.4 -30.066135406494141 7.2 -20.171710968017575
		 8 -10.47968578338623 8.8 -0.61206334829330444 9.6 9.2463674545288086 10.4 18.748748779296875
		 11.2 27.692420959472656 12 35.862674713134773 12.8 42.669609069824226 13.6 47.285881042480469
		 14.4 49.781028747558594 15.2 51.102329254150391 16 51.890060424804695 16.8 52.379165649414063
		 17.6 52.778297424316406 18.4 53.241371154785149 19.2 53.828975677490234 20 54.519306182861335
		 20.8 55.194362640380859 21.6 55.776321411132812 22.4 56.296142578125 23.2 56.761672973632812
		 24 57.152835845947266 24.8 57.508209228515632 25.6 57.858596801757805 26.4 58.165401458740234
		 27.2 58.392356872558594 28 58.542961120605462 28.8 58.619964599609382 29.6 58.638210296630859
		 30.4 58.630867004394531 31.2 58.585220336914055 32 58.464900970458984 32.8 58.296939849853516
		 33.6 58.125919342041016 34.4 57.917427062988281 35.2 57.567569732666009 36 56.969463348388672
		 36.8 55.984344482421875 37.6 54.368015289306641 38.4 51.675884246826179 39.2 47.343029022216797
		 40 41.719635009765625 40.8 36.276565551757813 41.6 31.560861587524411 42.4 26.782400131225582
		 43.2 21.427753448486328 44 15.458069801330565 44.8 8.820465087890625 45.6 1.0893352031707764
		 46.4 -8.4336462020874023 47.2 -18.982660293579105 48 -27.951629638671875 48.8 -33.900135040283203
		 49.6 -37.838699340820312;
createNode animCurveTA -n "Bip01_R_UpperArm_rotateY6";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 63 ".ktv[0:62]"  0 -68.166343688964844 0.8 -68.321670532226563
		 1.6 -68.501571655273437 2.4 -68.172874450683594 3.2 -66.602699279785156 4 -63.93937683105468
		 4.8 -60.850200653076179 5.6 -57.649745941162109 6.4 -54.453224182128906 7.2 -51.531173706054688
		 8 -49.283981323242195 8.8 -47.686103820800781 9.6 -46.391914367675781 10.4 -45.172496795654304
		 11.2 -43.926120758056641 12 -42.605880737304687 12.8 -41.380306243896477 13.6 -40.443050384521491
		 14.4 -39.920730590820312 15.2 -39.834827423095703 16 -39.966960906982422 16.8 -40.105319976806641
		 17.6 -40.208621978759766 18.4 -40.324012756347656 19.2 -40.453731536865234 20 -40.554164886474616
		 20.8 -40.619064331054688 21.6 -40.676479339599609 22.4 -40.734882354736328 23.2 -40.776512145996094
		 24 -40.777256011962891 24.8 -40.768516540527344 25.6 -40.799942016601563 26.4 -40.839767456054688
		 27.2 -40.842025756835938 28 -40.807270050048835 28.8 -40.736915588378906 29.6 -40.655986785888672
		 30.4 -40.610469818115234 31.2 -40.574932098388672 32 -40.488651275634766 32.8 -40.375328063964858
		 33.6 -40.292949676513679 34.4 -40.238887786865234 35.2 -40.376655578613281 36 -41.131736755371094
		 36.8 -42.580421447753913 37.6 -44.392822265625 38.4 -46.594524383544922 39.2 -49.283111572265625
		 40 -52.065574645996094 40.8 -54.65472412109375 41.6 -57.172492980957031 42.4 -59.771228790283203
		 43.2 -62.394107818603509 44 -64.686782836914062 44.8 -66.304397583007813 45.6 -67.235511779785156
		 46.4 -67.552818298339844 47.2 -67.669120788574219 48 -68.250686645507813 48.8 -69.330146789550781
		 49.6 -70.337982177734375;
createNode animCurveTA -n "Bip01_R_UpperArm_rotateZ6";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 63 ".ktv[0:62]"  0 23.860876083374023 0.8 22.896266937255859
		 1.6 20.33543586730957 2.4 15.989062309265137 3.2 10.948994636535645 4 6.0259857177734375
		 4.8 1.7089122533798218 5.6 -2.074146032333374 6.4 -6.0786685943603516 7.2 -10.455878257751465
		 8 -15.128429412841797 8.8 -19.845541000366211 9.6 -24.611129760742188 10.4 -29.580471038818363
		 11.2 -34.424537658691406 12 -38.839443206787109 12.8 -42.443103790283203 13.6 -44.717853546142578
		 14.4 -45.731658935546875 15.2 -46.050323486328125 16 -46.242187500000007 16.8 -46.565559387207031
		 17.6 -46.947441101074219 18.4 -47.296566009521491 19.2 -47.700626373291016 20 -48.238834381103523
		 20.8 -48.751106262207031 21.6 -49.102565765380859 22.4 -49.360527038574219 23.2 -49.536796569824219
		 24 -49.605167388916016 24.8 -49.668045043945313 25.6 -49.793361663818359 26.4 -49.891689300537116
		 27.2 -49.886756896972656 28 -49.785865783691406 28.8 -49.579303741455078 29.6 -49.29241943359375
		 30.4 -49.010871887207031 31.2 -48.704586029052734 32 -48.273784637451165 32.8 -47.777442932128906
		 33.6 -47.314662933349609 34.4 -46.7890625 35.2 -46.029148101806641 36 -44.951267242431641
		 36.8 -43.253013610839837 37.6 -40.42451477050782 38.4 -36.285007476806641 39.2 -30.704532623291016
		 40 -24.255500793457031 40.8 -19.025386810302734 41.6 -15.889092445373535 42.4 -13.369985580444336
		 43.2 -10.477829933166504 44 -7.2985715866088876 44.8 -4.0565972328186035 45.6 -1.4149115085601809
		 46.4 -0.33164691925048828 47.2 -0.80248361825942993 48 -1.4585882425308228 48.8 -1.1074619293212893
		 49.6 0.4134419858455658;
createNode animCurveTA -n "Bip01_R_Forearm_rotateX6";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 63 ".ktv[0:62]"  0 -0.27327930927276611 0.8 -0.26802635192871094
		 1.6 -0.25633329153060913 2.4 -0.25952538847923279 3.2 -0.3120342493057251 4 -0.44052550196647655
		 4.8 -0.66237664222717296 5.6 -0.986427903175354 6.4 -1.3854700326919556 7.2 -1.7850725650787354
		 8 -2.1293997764587398 8.8 -2.419980525970459 9.6 -2.6737368106842041 10.4 -2.8980927467346191
		 11.2 -3.0893778800964355 12 -3.2340981960296631 12.8 -3.3354969024658203 13.6 -3.4140925407409668
		 14.4 -3.479095458984375 15.2 -3.5252668857574463 16 -3.5500361919403076 16.8 -3.5559666156768799
		 17.6 -3.5563948154449463 18.4 -3.5580682754516602 19.2 -3.5608038902282715 20 -3.5612714290618896
		 20.8 -3.5608646869659428 21.6 -3.5628693103790283 22.4 -3.5640411376953129 23.2 -3.5658385753631592
		 24 -3.5646436214447026 24.8 -3.5666840076446533 25.6 -3.5686392784118652 26.4 -3.5695593357086186
		 27.2 -3.570772647857666 28 -3.5719611644744873 28.8 -3.573460817337037 29.6 -3.5753147602081299
		 30.4 -3.5766918659210205 31.2 -3.5765552520751962 32 -3.575553178787231 32.8 -3.5730972290039062
		 33.6 -3.5711512565612793 34.4 -3.5709466934204106 35.2 -3.5689539909362793 36 -3.5562787055969238
		 36.8 -3.5214612483978276 37.6 -3.4602916240692139 38.4 -3.3767802715301518 39.2 -3.2708661556243896
		 40 -3.1306090354919434 40.8 -2.9369792938232422 41.6 -2.6902492046356201 42.4 -2.4183502197265625
		 43.2 -2.1407790184020996 44 -1.8561788797378544 44.8 -1.5675355195999146 45.6 -1.2841981649398804
		 46.4 -1.0068973302841189 47.2 -0.74448549747467041 48 -0.52377426624298096 48.8 -0.36120980978012091
		 49.6 -0.25208607316017151;
createNode animCurveTA -n "Bip01_R_Forearm_rotateY6";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 63 ".ktv[0:62]"  0 1.1659513711929319 0.8 1.1596920490264893
		 1.6 1.1381751298904419 2.4 1.1378062963485718 3.2 1.2162501811981199 4 1.3879547119140625
		 4.8 1.6187368631362915 5.6 1.8585259914398196 6.4 2.048478364944458 7.2 2.150748491287231
		 8 2.1773350238800049 8.8 2.1563174724578857 9.6 2.1038682460784912 10.4 2.0298953056335449
		 11.2 1.9453973770141602 12 1.8658639192581177 12.8 1.7991223335266109 13.6 1.7406836748123169
		 14.4 1.6903547048568726 15.2 1.6548688411712649 16 1.6403939723968506 16.8 1.634649395942688
		 17.6 1.6316647529602053 18.4 1.627905011177063 19.2 1.6252008676528933 20 1.6255329847335815
		 20.8 1.6260807514190674 21.6 1.6237643957138062 22.4 1.6192913055419922 23.2 1.6165404319763184
		 24 1.6114597320556641 24.8 1.608644485473633 25.6 1.6086257696151731 26.4 1.6099762916564939
		 27.2 1.6101856231689451 28 1.6081187725067141 28.8 1.6051201820373535 29.6 1.6030389070510864
		 30.4 1.6022757291793823 31.2 1.6027933359146118 32 1.602681636810303 32.8 1.6032472848892212
		 33.6 1.6035815477371216 34.4 1.6034722328186035 35.2 1.6068626642227173 36 1.6216139793395996
		 36.8 1.6550842523574829 37.6 1.7059317827224734 38.4 1.7671921253204346 39.2 1.8368412256240847
		 40 1.9175797700881962 40.8 2.0076224803924561 41.6 2.0902864933013916 42.4 2.1455278396606445
		 43.2 2.1666431427001953 44 2.1523840427398682 44.8 2.0999600887298584 45.6 2.0077338218688965
		 46.4 1.8713500499725344 47.2 1.6902924776077273 48 1.486733078956604 48.8 1.2949596643447876
		 49.6 1.1381934881210327;
createNode animCurveTA -n "Bip01_R_Forearm_rotateZ6";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 63 ".ktv[0:62]"  0 -22.658750534057617 0.8 -22.446252822875977
		 1.6 -21.800380706787109 2.4 -21.834495544433594 3.2 -24.326456069946289 4 -29.993896484375004
		 4.8 -38.457130432128906 5.6 -49.117561340332031 6.4 -60.802013397216797 7.2 -71.6978759765625
		 8 -80.808349609375 8.8 -88.485504150390639 9.6 -95.305953979492188 10.4 -101.52171325683594
		 11.2 -107.02565002441406 12 -111.37007904052734 12.8 -114.56192779541016 13.6 -117.13572692871094
		 14.4 -119.29946899414062 15.2 -120.8349075317383 16 -121.58804321289062 16.8 -121.80341339111328
		 17.6 -121.85894012451173 18.4 -121.95320129394531 19.2 -122.05364227294922 20 -122.0583419799805
		 20.8 -122.04096984863281 21.6 -122.11974334716797 22.4 -122.21628570556641 23.2 -122.29839324951172
		 24 -122.35883331298827 24.8 -122.44811248779298 25.6 -122.48794555664064 26.4 -122.48372650146484
		 27.2 -122.50492858886722 28 -122.56404876708986 28.8 -122.6446838378906 29.6 -122.71724700927736
		 30.4 -122.75786590576172 31.2 -122.74655151367187 32 -122.72816467285158 32.8 -122.66880798339844
		 33.6 -122.62413787841795 34.4 -122.62209320068359 35.2 -122.52457427978516 36 -122.01892089843751
		 36.8 -120.75172424316406 37.6 -118.65740203857423 38.4 -115.92784881591798 39.2 -112.58583068847656
		 40 -108.31702423095706 40.8 -102.68083953857422 41.6 -95.805465698242202 42.4 -88.469635009765625
		 43.2 -81.106170654296875 44 -73.566383361816406 44.8 -65.80609130859375 45.6 -57.92680358886718
		 46.4 -49.759464263916016 47.2 -41.336338043212891 48 -33.408565521240241 48.8 -26.778900146484375
		 49.6 -21.75030517578125;
createNode animCurveTA -n "Bip01_L_Clavicle_rotateX6";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 63 ".ktv[0:62]"  0 -0.13209463655948639 0.8 -0.14120332896709442
		 1.6 -0.16125565767288208 2.4 -0.16792720556259155 3.2 -0.1894889771938324 4 -0.20016229152679443
		 4.8 -0.21266570687294009 5.6 -0.22255697846412659 6.4 -0.24239473044872284 7.2 -0.26201611757278442
		 8 -0.27153989672660828 8.8 -0.28429520130157471 9.6 -0.29678377509117126 10.4 -0.30575373768806458
		 11.2 -0.32189437747001648 12 -0.33473354578018188 12.8 -0.34779143333435059 13.6 -0.35146772861480713
		 14.4 -0.36334794759750366 15.2 -0.37361183762550354 16 -0.37627708911895752 16.8 -0.38392394781112671
		 17.6 -0.38912051916122442 18.4 -0.39279165863990784 19.2 -0.39529228210449219 20 -0.39667060971260071
		 20.8 -0.39663195610046392 21.6 -0.39615359902381897 22.4 -0.39314869046211243 23.2 -0.3916068971157074
		 24 -0.38537138700485229 24.8 -0.38273811340332031 25.6 -0.37338271737098694 26.4 -0.36978980898857128
		 27.2 -0.35760232806205755 28 -0.35383576154708862 28.8 -0.3412342369556427 29.6 -0.32770437002182007
		 30.4 -0.32294276356697083 31.2 -0.30609959363937378 32 -0.30110114812850958 32.8 -0.28662002086639404
		 33.6 -0.27432733774185181 34.4 -0.26216563582420349 35.2 -0.24773417413234708 36 -0.24125686287879947
		 36.8 -0.23483790457248685 37.6 -0.22240404784679413 38.4 -0.21353976428508761 39.2 -0.20633308589458463
		 40 -0.20030772686004639 40.8 -0.19544847309589383 41.6 -0.19152660667896271 42.4 -0.19080255925655365
		 43.2 -0.18998576700687408 44 -0.19052092730998993 44.8 -0.19384191930294037 45.6 -0.19527235627174375
		 46.4 -0.20093752443790436 47.2 -0.20759601891040799 48 -0.21668463945388791 48.8 -0.21968038380146029
		 49.6 -0.23052650690078733;
createNode animCurveTA -n "Bip01_L_Clavicle_rotateY6";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 63 ".ktv[0:62]"  0 -78.3927001953125 0.8 -78.375572204589844
		 1.6 -78.337814331054702 2.4 -78.325210571289063 3.2 -78.284553527832031 4 -78.264549255371108
		 4.8 -78.240936279296875 5.6 -78.222297668457017 6.4 -78.184761047363281 7.2 -78.14776611328125
		 8 -78.129737854003906 8.8 -78.105552673339844 9.6 -78.08197021484375 10.4 -78.065025329589844
		 11.2 -78.03448486328125 12 -78.010162353515625 12.8 -77.985313415527344 13.6 -77.978363037109375
		 14.4 -77.955924987792983 15.2 -77.936279296875 16 -77.931320190429673 16.8 -77.916801452636719
		 17.6 -77.906906127929673 18.4 -77.899955749511719 19.2 -77.895210266113281 20 -77.892593383789062
		 20.8 -77.892654418945313 21.6 -77.893638610839844 22.4 -77.899269104003906 23.2 -77.902206420898438
		 24 -77.914093017578125 24.8 -77.918998718261719 25.6 -77.936805725097685 26.4 -77.943679809570327
		 27.2 -77.966743469238281 28 -77.973960876464844 28.8 -77.997734069824219 29.6 -78.023384094238281
		 30.4 -78.032455444335938 31.2 -78.064323425292983 32 -78.073753356933594 32.8 -78.10113525390625
		 33.6 -78.124465942382812 34.4 -78.147415161132812 35.2 -78.174659729003906 36 -78.186904907226562
		 36.8 -78.19903564453125 37.6 -78.222511291503906 38.4 -78.239303588867188 39.2 -78.252822875976563
		 40 -78.26416015625 40.8 -78.273368835449219 41.6 -78.280776977539063 42.4 -78.282073974609375
		 43.2 -78.28363037109375 44 -78.28271484375 44.8 -78.27640533447267 45.6 -78.273750305175781
		 46.4 -78.262985229492188 47.2 -78.25042724609375 48 -78.233329772949219 48.8 -78.227638244628906
		 49.6 -78.207160949707031;
createNode animCurveTA -n "Bip01_L_Clavicle_rotateZ6";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 63 ".ktv[0:62]"  0 173.29234313964844 0.8 173.29409790039065
		 1.6 173.29791259765625 2.4 173.29917907714844 3.2 173.30328369140625 4 173.30534362792969
		 4.8 173.30769348144531 5.6 173.3095703125 6.4 173.31327819824219 7.2 173.31698608398438
		 8 173.31874084472656 8.8 173.32109069824219 9.6 173.32342529296875 10.4 173.32511901855469
		 11.2 173.32810974121094 12 173.33045959472656 12.8 173.33282470703125 13.6 173.33349609375
		 14.4 173.33572387695312 15.2 173.33753967285156 16 173.33805847167969 16.8 173.33944702148435
		 17.6 173.34037780761719 18.4 173.34104919433594 19.2 173.34150695800781 20 173.34176635742187
		 20.8 173.34173583984375 21.6 173.34169006347656 22.4 173.34112548828125 23.2 173.34083557128906
		 24 173.3397216796875 24.8 173.33921813964844 25.6 173.3375244140625 26.4 173.33688354492188
		 27.2 173.33464050292969 28 173.333984375 28.8 173.33163452148437 29.6 173.32914733886719
		 30.4 173.32827758789062 31.2 173.32514953613281 32 173.32420349121094 32.8 173.32151794433594
		 33.6 173.31927490234375 34.4 173.31698608398438 35.2 173.31427001953125 36 173.31306457519531
		 36.8 173.31185913085935 37.6 173.30950927734375 38.4 173.30787658691406 39.2 173.30647277832031
		 40 173.30532836914062 40.8 173.30441284179687 41.6 173.30368041992187 42.4 173.30351257324219
		 43.2 173.30337524414062 44 173.30351257324219 44.8 173.30410766601562 45.6 173.30441284179687
		 46.4 173.30546569824219 47.2 173.30671691894531 48 173.30844116210937 48.8 173.30900573730472
		 49.6 173.31101989746094;
createNode animCurveTA -n "Bip01_L_UpperArm_rotateX6";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 63 ".ktv[0:62]"  0 18.24528694152832 0.8 18.155494689941406
		 1.6 18.360336303710937 2.4 19.2977294921875 3.2 20.473119735717773 4 21.2413330078125
		 4.8 21.851541519165039 5.6 22.642364501953129 6.4 23.350786209106445 7.2 23.786088943481445
		 8 24.166463851928711 8.8 24.692481994628903 9.6 25.466217041015625 10.4 26.46624755859375
		 11.2 27.336042404174805 12 27.633831024169918 12.8 27.496477127075195 13.6 27.7587890625
		 14.4 28.589742660522461 15.2 28.961929321289063 16 28.738162994384759 16.8 28.85882568359375
		 17.6 29.29896545410157 18.4 29.507194519042969 19.2 29.605487823486325 20 29.848953247070313
		 20.8 30.067710876464847 21.6 30.022125244140625 22.4 29.821331024169925 23.2 29.570022583007813
		 24 29.259624481201168 24.8 29.068235397338871 25.6 29.087284088134755 26.4 29.134538650512699
		 27.2 28.940719604492191 28 28.465362548828125 28.8 27.897270202636719 29.6 27.360334396362305
		 30.4 27.044174194335938 31.2 26.812263488769531 32 26.123214721679688 32.8 25.180160522460937
		 33.6 24.613458633422852 34.4 24.349443435668945 35.2 24.093805313110352 36 23.688528060913089
		 36.8 22.72403717041016 37.6 21.170022964477539 38.4 19.979444503784183 39.2 19.656377792358398
		 40 19.504064559936523 40.8 19.225917816162109 41.6 19.037130355834961 42.4 18.848188400268558
		 43.2 18.886955261230469 44 19.400583267211911 44.8 20.072399139404297 45.6 20.62913703918457
		 46.4 21.131687164306641 47.2 21.572210311889648 48 21.726083755493168 48.8 21.529531478881836
		 49.6 21.179450988769531;
createNode animCurveTA -n "Bip01_L_UpperArm_rotateY6";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 63 ".ktv[0:62]"  0 76.179435729980469 0.8 76.145103454589844
		 1.6 76.130378723144531 2.4 76.195465087890625 3.2 76.31864166259767 4 76.437263488769531
		 4.8 76.501579284667969 5.6 76.517005920410156 6.4 76.4947509765625 7.2 76.420814514160156
		 8 76.297775268554687 8.8 76.1923828125 9.6 76.180862426757812 10.4 76.2200927734375
		 11.2 76.207099914550781 12 76.159217834472656 12.8 76.139862060546875 13.6 76.107185363769531
		 14.4 76.023109436035156 15.2 75.959220886230469 16 75.941421508789063 16.8 75.904190063476562
		 17.6 75.853225708007813 18.4 75.833114624023438 19.2 75.826057434082031 20 75.7991943359375
		 20.8 75.773643493652358 21.6 75.792060852050781 22.4 75.844161987304688 23.2 75.911979675292983
		 24 75.978927612304688 24.8 75.987571716308594 25.6 75.923103332519531 26.4 75.846626281738281
		 27.2 75.819839477539063 28 75.844650268554688 28.8 75.887039184570327 29.6 75.928970336914063
		 30.4 75.941764831542983 31.2 75.943351745605469 32 76.003616333007813 32.8 76.0665283203125
		 33.6 76.037017822265625 34.4 75.971588134765625 35.2 75.96067047119142 36 75.946441650390625
		 36.8 75.853767395019531 37.6 75.78826904296875 38.4 75.844497680664062 39.2 76.038932800292983
		 40 76.349906921386719 40.8 76.542213439941406 41.6 76.477302551269531 42.4 76.324935913085938
		 43.2 76.134109497070327 44 75.869041442871094 44.8 75.618232727050781 45.6 75.345054626464844
		 46.4 74.926307678222656 47.2 74.471221923828125 48 74.183609008789063 48.8 74.085563659667969
		 49.6 74.11810302734375;
createNode animCurveTA -n "Bip01_L_UpperArm_rotateZ6";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 63 ".ktv[0:62]"  0 -22.303958892822266 0.8 -22.417377471923828
		 1.6 -22.16438102722168 2.4 -21.026556015014648 3.2 -19.622074127197266 4 -18.725833892822266
		 4.8 -17.99940299987793 5.6 -17.022514343261719 6.4 -16.124036788940433 7.2 -15.531355857849126
		 8 -14.976230621337894 8.8 -14.242410659790041 9.6 -13.244724273681641 10.4 -11.993514060974119
		 11.2 -10.87130737304688 12 -10.425195693969728 12.8 -10.52348804473877 13.6 -10.119584083557127
		 14.4 -8.9861135482788086 15.2 -8.4252405166625977 16 -8.6210851669311523 16.8 -8.3844022750854492
		 17.6 -7.7501735687255868 18.4 -7.421605587005617 19.2 -7.2399020195007333 20 -6.8731203079223633
		 20.8 -6.542457103729248 21.6 -6.5716552734375 22.4 -6.8231654167175293 23.2 -7.1546883583068848
		 24 -7.566810131072998 24.8 -7.8092427253723171 25.6 -7.7577214241027832 26.4 -7.6680712699890137
		 27.2 -7.9098358154296893 28 -8.5378341674804687 28.8 -9.3001880645751953 29.6 -10.031634330749512
		 30.4 -10.479958534240724 31.2 -10.82162380218506 32 -11.759980201721191 32.8 -13.007956504821776
		 33.6 -13.733405113220217 34.4 -14.058483123779297 35.2 -14.394955635070801 36 -14.89595890045166
		 36.8 -16.022327423095703 37.6 -17.886159896850586 38.4 -19.349531173706055 39.2 -19.770967483520508
		 40 -20.001590728759769 40.8 -20.301219940185547 41.6 -20.348340988159183 42.4 -20.329154968261719
		 43.2 -19.973970413208008 44 -18.950700759887695 44.8 -17.686800003051758 45.6 -16.50971794128418
		 46.4 -15.280841827392578 47.2 -14.059914588928226 48 -13.243877410888672 48.8 -12.937918663024904
		 49.6 -12.870932579040527;
createNode animCurveTA -n "Bip01_L_Forearm_rotateX6";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 63 ".ktv[0:62]"  0 -0.036618199199438095 0.8 -0.03683098778128624
		 1.6 -0.036758139729499817 2.4 -0.03599100187420845 3.2 -0.035214010626077652 4 -0.035149745643138885
		 4.8 -0.034917622804641724 5.6 -0.034012313932180405 6.4 -0.033263534307479858 7.2 -0.032747272402048111
		 8 -0.031326334923505783 8.8 -0.029089543968439099 9.6 -0.027163088321685791 10.4 -0.025342522189021114
		 11.2 -0.022611843422055244 12 -0.019987845793366432 12.8 -0.019145062193274495 13.6 -0.018801279366016388
		 14.4 -0.017374401912093163 15.2 -0.017165966331958771 16 -0.019208822399377823 16.8 -0.020530786365270615
		 17.6 -0.019879607483744621 18.4 -0.018064392730593681 19.2 -0.01469304319471121 20 -0.0093111163005232811
		 20.8 -0.0036443334538489576 21.6 -0.00068494526203721762 22.4 -0.0018358581000939012
		 23.2 -0.0068771648220717907 24 -0.01347504835575819 24.8 -0.017635991796851158 25.6 -0.018232898786664009
		 26.4 -0.016913438215851784 27.2 -0.015329497866332531 28 -0.014919149689376356 28.8 -0.016336929053068161
		 29.6 -0.018780315294861794 30.4 -0.020898396149277691 31.2 -0.023270061239600185
		 32 -0.026958860456943512 32.8 -0.029505409300327301 33.6 -0.029105858877301216 34.4 -0.028109254315495491
		 35.2 -0.028451031073927879 36 -0.029285624623298645 36.8 -0.030192932114005089 37.6 -0.03180164098739624
		 38.4 -0.033391494303941727 39.2 -0.03431098535656929 40 -0.0346684530377388 40.8 -0.033686526119709015
		 41.6 -0.031782668083906174 42.4 -0.030723582953214645 43.2 -0.029734937474131588
		 44 -0.027274025604128841 44.8 -0.024015147238969803 45.6 -0.020830441266298298 46.4 -0.017241448163986206
		 47.2 -0.012579835020005705 48 -0.0070690503343939781 48.8 -0.002555077662691474 49.6 -0.00089874572586268197;
createNode animCurveTA -n "Bip01_L_Forearm_rotateY6";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 63 ".ktv[0:62]"  0 -0.373058021068573 0.8 -0.37077540159225464
		 1.6 -0.37146025896072382 2.4 -0.37947198748588562 3.2 -0.3872683346271516 4 -0.38785254955291748
		 4.8 -0.39009317755699158 5.6 -0.39867913722991943 6.4 -0.40502199530601501 7.2 -0.40663138031959534
		 8 -0.41044396162033081 8.8 -0.4173833429813385 9.6 -0.42425686120986938 10.4 -0.43245127797126776
		 11.2 -0.44106075167655945 12 -0.44262626767158514 12.8 -0.43718844652175903 13.6 -0.44016185402870178
		 14.4 -0.45462501049041742 15.2 -0.45935457944869984 16 -0.45284470915794367 16.8 -0.45665591955184937
		 17.6 -0.46914038062095648 18.4 -0.47561818361282349 19.2 -0.47810712456703186 20 -0.48396795988082891
		 20.8 -0.48922282457351679 21.6 -0.48653191328048706 22.4 -0.48007705807685846 23.2 -0.4740930199623109
		 24 -0.46845138072967529 24.8 -0.4683412909507752 25.6 -0.47426253557205195 26.4 -0.47960317134857178
		 27.2 -0.47920256853103638 28 -0.47279384732246399 28.8 -0.46317654848098749 29.6 -0.45270076394081116
		 30.4 -0.4479464590549469 31.2 -0.44676688313484192 32 -0.43532848358154297 32.8 -0.41985154151916509
		 33.6 -0.41582620143890386 34.4 -0.41885167360305786 35.2 -0.42213413119316107 36 -0.42728030681610102
		 36.8 -0.4284186065196991 37.6 -0.41793793439865112 38.4 -0.40442177653312678 39.2 -0.39590579271316528
		 40 -0.39246088266372681 40.8 -0.40166366100311279 41.6 -0.41856768727302551 42.4 -0.4274688065052033
		 43.2 -0.43550041317939758 44 -0.45454651117324835 44.8 -0.47793823480606079 45.6 -0.49872687458992004
		 46.4 -0.51783883571624756 47.2 -0.53471201658248901 48 -0.54779696464538574 48.8 -0.5565916895866394
		 49.6 -0.5597386360168457;
createNode animCurveTA -n "Bip01_L_Forearm_rotateZ6";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 63 ".ktv[0:62]"  0 -0.091859854757785797 0.8 -0.03118707612156868
		 1.6 -0.0493796207010746 2.4 -0.26159194111824036 3.2 -0.46788069605827332 4 -0.48353302478790272
		 4.8 -0.54281270503997803 5.6 -0.77029186487197876 6.4 -0.93872278928756714 7.2 -0.98248326778411865
		 8 -1.086950421333313 8.8 -1.275734543800354 9.6 -1.4621999263763428 10.4 -1.6830861568450928
		 11.2 -1.9186474084854128 12 -1.9705814123153687 12.8 -1.8327800035476691 13.6 -1.9113969802856448
		 14.4 -2.2934167385101318 15.2 -2.417614221572876 16 -2.2393548488616943 16.8 -2.3329825401306152
		 17.6 -2.6607885360717773 18.4 -2.8371081352233887 19.2 -2.916661262512207 20 -3.0954422950744629
		 20.8 -3.2627072334289551 21.6 -3.2110013961791992 22.4 -3.0378251075744629 23.2 -2.8547346591949463
		 24 -2.673497200012207 24.8 -2.6497132778167725 25.6 -2.8007903099060059 26.4 -2.9451980590820313
		 27.2 -2.9416334629058838 28 -2.7768261432647705 28.8 -2.5203909873962398 29.6 -2.2373025417327881
		 30.4 -2.1047265529632568 31.2 -2.0646152496337891 32 -1.7524557113647461 32.8 -1.3389908075332642
		 33.6 -1.2353378534317017 34.4 -1.3178853988647461 35.2 -1.4023289680480957 36 -1.5341227054595947
		 36.8 -1.5606286525726318 37.6 -1.2814726829528809 38.4 -0.92256981134414651 39.2 -0.69705855846405029
		 40 -0.60588258504867554 40.8 -0.84959763288497925 41.6 -1.2980327606201172 42.4 -1.5339841842651367
		 43.2 -1.7475557327270508 44 -2.2536008358001709 44.8 -2.876012802124023 45.6 -3.430281400680542
		 46.4 -3.9420809745788579 47.2 -4.3997015953063965 48 -4.7618956565856934 48.8 -5.0092768669128418
		 49.6 -5.0980501174926758;
createNode animCurveTA -n "Bip01_L_Thigh_rotateX6";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 63 ".ktv[0:62]"  0 -0.1797705739736557 0.8 -0.17925147712230682
		 1.6 -0.18304905295372009 2.4 -0.186081662774086 3.2 -0.19416861236095428 4 -0.1919829398393631
		 4.8 -0.19168241322040561 5.6 -0.19515214860439301 6.4 -0.19772030413150787 7.2 -0.20045238733291623
		 8 -0.20113539695739743 8.8 -0.20438656210899353 9.6 -0.20340302586555481 10.4 -0.21001464128494263
		 11.2 -0.21138067543506625 12 -0.21474114060401919 12.8 -0.21378490328788757 13.6 -0.21463185548782349
		 14.4 -0.21427668631076813 15.2 -0.21607984602451324 16 -0.22356574237346649 16.8 -0.22326521575450897
		 17.6 -0.22424875199794769 18.4 -0.22471320629119873 19.2 -0.22744528949260712 20 -0.23121555149555206
		 20.8 -0.23394761979579928 21.6 -0.23728075623512268 22.4 -0.24165208637714383 23.2 -0.238155022263527
		 24 -0.23695291578769684 24.8 -0.23924785852432251 25.6 -0.24189797043800348 26.4 -0.25039473176002502
		 27.2 -0.25449284911155701 28 -0.257088303565979 28.8 -0.25069525837898254 29.6 -0.25203397870063782
		 30.4 -0.25017616152763367 31.2 -0.25714296102523804 32 -0.26410973072052002 32.8 -0.26547577977180481
		 33.6 -0.26536649465560913 34.4 -0.26558506488800049 35.2 -0.26498401165008545 36 -0.2664320170879364
		 36.8 -0.26033946871757507 37.6 -0.26044875383377075 38.4 -0.25585886836051941 39.2 -0.23881071805953979
		 40 -0.22411215305328369 40.8 -0.20949554443359375 41.6 -0.20755577087402344 42.4 -0.20845736563205719
		 43.2 -0.20684543251991272 44 -0.20810219645500183 44.8 -0.20616242289543152 45.6 -0.20777434110641479
		 46.4 -0.21034249663352972 47.2 -0.21056106686592105 48 -0.20758309960365295 48.8 -0.20821148157119751
		 49.6 -0.21299260854721069;
createNode animCurveTA -n "Bip01_L_Thigh_rotateY6";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 63 ".ktv[0:62]"  0 -180.27983093261719 0.8 -180.29045104980469
		 1.6 -180.26734924316409 2.4 -180.20588684082031 3.2 -180.14370727539062 4 -180.156005859375
		 4.8 -180.22741699218753 5.6 -180.27235412597656 6.4 -180.24835205078125 7.2 -180.19720458984375
		 8 -180.13499450683594 8.8 -180.07035827636719 9.6 -180.00582885742187 10.4 -179.94947814941406
		 11.2 -179.888916015625 12 -179.85102844238281 12.8 -179.89717102050781 13.6 -179.99703979492187
		 14.4 -180.02047729492187 15.2 -179.89640808105469 16 -179.71177673339844 16.8 -179.59431457519531
		 17.6 -179.56097412109375 18.4 -179.5494384765625 19.2 -179.523193359375 20 -179.48492431640625
		 20.8 -179.45236206054687 21.6 -179.43740844726562 22.4 -179.42832946777344 23.2 -179.43592834472656
		 24 -179.44337463378906 24.8 -179.40982055664062 25.6 -179.35275268554687 26.4 -179.3175048828125
		 27.2 -179.30413818359375 28 -179.28462219238281 28.8 -179.26324462890628 29.6 -179.26068115234375
		 30.4 -179.27703857421875 31.2 -179.30087280273435 32 -179.3145751953125 32.8 -179.31571960449219
		 33.6 -179.30609130859375 34.4 -179.30126953125 35.2 -179.33219909667969 36 -179.39373779296875
		 36.8 -179.43089294433594 37.6 -179.45970153808594 38.4 -179.46699523925781 39.2 -179.40675354003906
		 40 -179.38223266601565 40.8 -179.49359130859375 41.6 -179.63349914550781 42.4 -179.7265625
		 43.2 -179.81382751464844 44 -179.89582824707031 44.8 -179.92622375488284 45.6 -179.896240234375
		 46.4 -179.86195373535156 47.2 -179.91224670410156 48 -180.04176330566406 48.8 -180.26596069335935
		 49.6 -180.62648010253903;
createNode animCurveTA -n "Bip01_L_Thigh_rotateZ6";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 63 ".ktv[0:62]"  0 1.2912198305130005 0.8 1.2154867649078369
		 1.6 1.2402119636535645 2.4 1.3576229810714722 3.2 1.54691481590271 4 1.7554131746292114
		 4.8 1.9296785593032839 5.6 2.0678806304931641 6.4 2.1173584461212158 7.2 2.0971002578735352
		 8 2.1118261814117432 8.8 2.1465506553649902 9.6 2.2025036811828613 10.4 2.2941100597381592
		 11.2 2.4095675945281982 12 2.4922266006469727 12.8 2.6854116916656494 13.6 2.9692878723144531
		 14.4 3.2277696132659912 15.2 3.2952382564544682 16 3.2359249591827393 16.8 3.3304955959320068
		 17.6 3.5522172451019287 18.4 3.6537139415740958 19.2 3.6736307144165044 20 3.7499103546142578
		 20.8 3.9060072898864746 21.6 4.079658031463623 22.4 4.2135705947875985 23.2 4.2914347648620605
		 24 4.3183732032775879 24.8 4.3736567497253418 25.6 4.5384964942932129 26.4 4.734154224395752
		 27.2 4.7149477005004883 28 4.5073099136352539 28.8 4.3610072135925293 29.6 4.4198288917541513
		 30.4 4.6492958068847665 31.2 4.8674521446228027 32 4.9219708442687988 32.8 4.8182883262634277
		 33.6 4.7249879837036133 34.4 4.6497330665588379 35.2 4.5381960868835449 36 4.3624415397644043
		 36.8 4.2330641746520996 37.6 4.0744123458862305 38.4 3.4664847850799561 39.2 2.1641452312469482
		 40 0.7139732837677002 40.8 -0.26263442635536194 41.6 -0.66651713848114025 42.4 -0.80866700410842896
		 43.2 -0.91926145553588867 44 -0.99062323570251476 44.8 -1.0348281860351565 45.6 -1.0262768268585205
		 46.4 -0.97018736600875843 47.2 -0.85981148481369019 48 -0.75642973184585571 48.8 -0.63895052671432495
		 49.6 -0.44153073430061346;
createNode animCurveTA -n "Bip01_L_Calf_rotateX6";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 63 ".ktv[0:62]"  0 -0.060619886964559555 0.8 -0.056753545999526984
		 1.6 -0.055359270423650742 2.4 -0.05731454864144326 3.2 -0.060981225222349174 4 -0.063699424266815186
		 4.8 -0.064832963049411774 5.6 -0.064455606043338776 6.4 -0.063010260462760925 7.2 -0.06045247241854667
		 8 -0.058497145771980286 8.8 -0.057374153286218643 9.6 -0.056351833045482629 10.4 -0.056246951222419746
		 11.2 -0.05629093945026397 12 -0.055906768888235085 12.8 -0.056567575782537467 13.6 -0.058516662567853921
		 14.4 -0.06053788959980011 15.2 -0.060155332088470459 16 -0.057986956089735024 16.8 -0.059204936027526862
		 17.6 -0.062135316431522362 18.4 -0.061267800629138947 19.2 -0.0585043765604496 20 -0.057638023048639291
		 20.8 -0.059531893581151962 21.6 -0.06135169044137001 22.4 -0.061936765909194953 23.2 -0.061436094343662262
		 24 -0.060348618775606155 24.8 -0.060089245438575745 25.6 -0.06283955276012422 26.4 -0.0662364661693573
		 27.2 -0.064622379839420319 28 -0.058208309113979347 28.8 -0.05329627916216851 29.6 -0.053852397948503494
		 30.4 -0.058956310153007507 31.2 -0.063679315149784088 32 -0.063442565500736237 32.8 -0.060064841061830521
		 33.6 -0.057679355144500746 34.4 -0.057202797383069992 35.2 -0.056600615382194526
		 36 -0.053288552910089493 36.8 -0.051480662077665329 37.6 -0.05360175296664238 38.4 -0.056262232363224023
		 39.2 -0.056277420371770859 40 -0.055590696632862098 40.8 -0.055531892925500877 41.6 -0.055033158510923386
		 42.4 -0.054163660854101181 43.2 -0.054201748222112656 44 -0.055549070239067078 44.8 -0.056981008499860777
		 45.6 -0.056994389742612839 46.4 -0.056368429213762283 47.2 -0.056530389934778214
		 48 -0.056546326726675034 48.8 -0.056493811309337623 49.6 -0.056712366640567773;
createNode animCurveTA -n "Bip01_L_Calf_rotateY6";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 63 ".ktv[0:62]"  0 -1.0541598796844482 0.8 -1.0540069341659546
		 1.6 -1.0539346933364868 2.4 -1.054057240486145 3.2 -1.0542247295379641 4 -1.0543478727340698
		 4.8 -1.0544116497039795 5.6 -1.0543453693389893 6.4 -1.0542778968811035 7.2 -1.0541902780532837
		 8 -1.0540733337402344 8.8 -1.0540242195129397 9.6 -1.0539692640304563 10.4 -1.0539811849594116
		 11.2 -1.0539495944976809 12 -1.0539580583572388 12.8 -1.0539506673812866 13.6 -1.0540207624435425
		 14.4 -1.0541056394577026 15.2 -1.0540947914123535 16 -1.054023265838623 16.8 -1.0540925264358521
		 17.6 -1.0542143583297727 18.4 -1.0541772842407229 19.2 -1.0540658235549929 20 -1.0540262460708618
		 20.8 -1.0541281700134275 21.6 -1.0542130470275879 22.4 -1.0542329549789429 23.2 -1.0542153120040894
		 24 -1.0541855096817017 24.8 -1.0541713237762451 25.6 -1.0542948246002195 26.4 -1.0544188022613523
		 27.2 -1.0543851852416992 28 -1.0541305541992187 28.8 -1.0538845062255859 29.6 -1.0539014339447019
		 30.4 -1.0541328191757202 31.2 -1.0543259382247925 32 -1.0542991161346436 32.8 -1.0541952848434448
		 33.6 -1.0540939569473269 34.4 -1.0540788173675537 35.2 -1.0540560483932495 36 -1.0538889169692991
		 36.8 -1.0537855625152588 37.6 -1.0539153814315796 38.4 -1.0540310144424438 39.2 -1.0540459156036377
		 40 -1.0539932250976562 40.8 -1.0539823770523071 41.6 -1.0539346933364868 42.4 -1.0538672208786013
		 43.2 -1.0538724660873413 44 -1.0539283752441406 44.8 -1.0540353059768677 45.6 -1.0540418624877932
		 46.4 -1.0540444850921633 47.2 -1.0540661811828611 48 -1.054073691368103 48.8 -1.0540629625320437
		 49.6 -1.0540280342102053;
createNode animCurveTA -n "Bip01_L_Calf_rotateZ6";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 63 ".ktv[0:62]"  0 -0.027279984205961227 0.8 -0.30510476231575012
		 1.6 -0.39667409658432007 2.4 -0.29078215360641479 3.2 -0.047725021839141853 4 0.12923148274421692
		 4.8 0.19124846160411835 5.6 0.2070876806974411 6.4 0.11767636239528656 7.2 -0.070872306823730469
		 8 -0.17991042137145996 8.8 -0.25496101379394531 9.6 -0.31909599900245672 10.4 -0.33825165033340454
		 11.2 -0.3085496723651886 12 -0.3548770546913147 12.8 -0.27823051810264587 13.6 -0.13172568380832672
		 14.4 0.010378381237387655 15.2 -0.025206465274095535 16 -0.18839807808399203 16.8 -0.11752037703990938
		 17.6 0.083799727261066437 18.4 0.023944038897752758 19.2 -0.16962690651416781 20 -0.22977344691753385
		 20.8 -0.11470328271389008 21.6 0.00040178874041885138 22.4 0.047432184219360352 23.2 0.010338916443288326
		 24 -0.081866435706615476 24.8 -0.095548421144485474 25.6 0.087007217109203339 26.4 0.32225912809371948
		 27.2 0.18900978565216064 28 -0.25744256377220154 28.8 -0.57816833257675171 29.6 -0.53265494108200073
		 30.4 -0.17833435535430908 31.2 0.14507438242435455 32 0.14103874564170835 32.8 -0.1195562109351158
		 33.6 -0.28232333064079285 34.4 -0.32136073708534241 35.2 -0.36646601557731634 36 -0.58480256795883179
		 36.8 -0.6952425241470338 37.6 -0.57174563407897949 38.4 -0.38398456573486328 39.2 -0.39318615198135376
		 40 -0.42219868302345281 40.8 -0.41760668158531195 41.6 -0.43264284729957586 42.4 -0.4670427143573761
		 43.2 -0.46886882185935974 44 -0.36989086866378784 44.8 -0.30500143766403198 45.6 -0.31201070547103882
		 46.4 -0.378673255443573 47.2 -0.38231846690177917 48 -0.38756179809570313 48.8 -0.38474532961845398
		 49.6 -0.33116045594215393;
createNode animCurveTA -n "Bip01_L_Foot_rotateY6";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 63 ".ktv[0:62]"  0 -0.24157142639160156 0.8 -0.16936208307743073
		 1.6 -0.15204024314880371 2.4 -0.22717675566673276 3.2 -0.23355785012245175 4 -0.20185606181621552
		 4.8 -0.20020173490047455 5.6 -0.20240198075771332 6.4 -0.26999333500862122 7.2 -0.34069189429283142
		 8 -0.3726029098033905 8.8 -0.41486376523971558 9.6 -0.43115806579589838 10.4 -0.46856915950775146
		 11.2 -0.50597834587097168 12 -0.54049849510192871 12.8 -0.53918111324310303 13.6 -0.5451696515083313
		 14.4 -0.56384080648422241 15.2 -0.62228715419769287 16 -0.65993231534957886 16.8 -0.6980971097946167
		 17.6 -0.70623338222503662 18.4 -0.70248597860336304 19.2 -0.72605878114700317 20 -0.75950002670288075
		 20.8 -0.82180559635162354 21.6 -0.85489088296890259 22.4 -0.87436628341674805 23.2 -0.76612085103988636
		 24 -0.65033811330795299 24.8 -0.74946403503417969 25.6 -0.89672833681106556 26.4 -0.93754160404205333
		 27.2 -0.92424631118774414 28 -0.96709561347961426 28.8 -1.0456520318984983 29.6 -1.0744116306304934
		 30.4 -0.98710465431213379 31.2 -0.94409483671188354 32 -1.0221573114395142 32.8 -1.221680760383606
		 33.6 -1.3491573333740234 34.4 -1.4168444871902466 35.2 -1.5202615261077881 36 -1.5839768648147583
		 36.8 -1.6947706937789917 37.6 -1.7249242067337036 38.4 -1.5806431770324707 39.2 -1.1606332063674929
		 40 -0.70068204402923584 40.8 -0.35857373476028442 41.6 -0.23398111760616308 42.4 -0.23863060772418976
		 43.2 -0.28116178512573242 44 -0.31970158219337469 44.8 -0.34502547979354858 45.6 -0.34552314877510071
		 46.4 -0.36809372901916504 47.2 -0.43865269422531128 48 -0.53111684322357178 48.8 -0.59710961580276489
		 49.6 -0.54655981063842773;
createNode animCurveTA -n "Bip01_L_Foot_rotateZ6";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 63 ".ktv[0:62]"  0 0.92824047803878773 0.8 1.1004201173782349
		 1.6 1.242924690246582 2.4 1.3189982175827026 3.2 1.3394674062728882 4 1.3030956983566284
		 4.8 1.2250518798828125 5.6 1.0944333076477053 6.4 0.95823460817337047 7.2 0.93194651603698742
		 8 0.96445000171661377 8.8 0.98219233751297008 9.6 0.95892661809921254 10.4 0.84817266464233387
		 11.2 0.72545647621154785 12 0.64794892072677612 12.8 0.51489514112472534 13.6 0.28732219338417053
		 14.4 0.16056770086288452 15.2 0.2592091858386994 16 0.43906956911087042 16.8 0.38386711478233337
		 17.6 0.26637789607048035 18.4 0.36046504974365234 19.2 0.46843546628952032 20 0.45083761215209961
		 20.8 0.3847995400428772 21.6 0.32116496562957764 22.4 0.33372452855110168 23.2 0.47503039240837097
		 24 0.61152184009552002 24.8 0.56025898456573497 25.6 0.36203941702842712 26.4 0.227309450507164
		 27.2 0.35384750366210938 28 0.63091391324996948 28.8 0.75250321626663197 29.6 0.66745454072952271
		 30.4 0.47289803624153137 31.2 0.29221495985984802 32 0.2436799556016922 32.8 0.35327282547950745
		 33.6 0.44348117709159846 34.4 0.39497712254524231 35.2 0.2915675938129425 36 0.26934546232223511
		 36.8 0.25113120675086975 37.6 0.18802003562450409 38.4 0.32916370034217834 39.2 0.7691381573677063
		 40 1.145878791809082 40.8 1.2251582145690918 41.6 1.1162773370742798 42.4 1.0366722345352173
		 43.2 0.93248194456100475 44 0.8295522928237915 44.8 0.92179095745086659 45.6 1.2354475259780884
		 46.4 1.5247435569763184 47.2 1.5968587398529053 48 1.5503338575363159 48.8 1.4533380270004272
		 49.6 1.352121114730835;
createNode animCurveTA -n "Bip01_L_Foot_rotateX6";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 63 ".ktv[0:62]"  0 -6.4667301177978516 0.8 -6.5917139053344727
		 1.6 -6.5829839706420898 2.4 -6.5362753868103027 3.2 -6.5184907913208008 4 -6.5505976676940918
		 4.8 -6.5808858871459961 5.6 -6.5624728202819824 6.4 -6.4440855979919434 7.2 -6.2884678840637207
		 8 -6.203423023223877 8.8 -6.1589040756225586 9.6 -6.0978002548217773 10.4 -6.0312080383300781
		 11.2 -5.9659042358398437 12 -5.8979854583740234 12.8 -5.9000329971313477 13.6 -5.8780393600463867
		 14.4 -5.8414983749389648 15.2 -5.7453665733337402 16 -5.6388916969299316 16.8 -5.5947799682617187
		 17.6 -5.5769939422607431 18.4 -5.5634207725524902 19.2 -5.5367093086242676 20 -5.4508152008056641
		 20.8 -5.332984447479248 21.6 -5.2627873420715332 22.4 -5.2871131896972656 23.2 -5.4611868858337402
		 24 -5.6345443725585938 24.8 -5.4796900749206543 25.6 -5.1910481452941903 26.4 -5.1092987060546884
		 27.2 -5.129234790802002 28 -5.0614180564880371 28.8 -4.8984022140502939 29.6 -4.8582258224487305
		 30.4 -4.9848260879516602 31.2 -5.085568904876709 32 -4.8985767364501953 32.8 -4.5468292236328125
		 33.6 -4.3084464073181152 34.4 -4.1010589599609375 35.2 -3.9429566860198975 36 -3.8100483417510982
		 36.8 -3.6254189014434819 37.6 -3.5450878143310547 38.4 -3.8996052742004399 39.2 -4.666438102722168
		 40 -5.578730583190918 40.8 -6.2699785232543945 41.6 -6.5289077758789062 42.4 -6.5082998275756836
		 43.2 -6.4083051681518555 44 -6.3058714866638184 44.8 -6.2801346778869629 45.6 -6.3229799270629883
		 46.4 -6.2789192199707031 47.2 -6.1506142616271973 48 -5.9560098648071289 48.8 -5.8257436752319336
		 49.6 -5.9003505706787109;
createNode animCurveTA -n "Bip01_R_Thigh_rotateX6";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 63 ".ktv[0:62]"  0 0.24117396771907809 0.8 0.24058656394481659
		 1.6 0.23849652707576749 2.4 0.23633819818496704 3.2 0.23483555018901822 4 0.23245865106582639
		 4.8 0.2356278598308563 5.6 0.23478090763092041 6.4 0.23102430999279028 7.2 0.2287430167198182
		 8 0.22080634534358975 8.8 0.21793766319751745 9.6 0.20991902053356171 10.4 0.20404505729675293
		 11.2 0.1996190994977951 12 0.19937321543693545 12.8 0.19582150876522064 13.6 0.19772030413150787
		 14.4 0.19643622636795044 15.2 0.18665540218353271 16 0.17298136651515961 16.8 0.15962150692939758
		 17.6 0.15460816025733948 18.4 0.15489502251148224 19.2 0.15392513573169708 20 0.14796921610832214
		 20.8 0.14840634167194366 21.6 0.14658951759338379 22.4 0.143925741314888 23.2 0.14268264174461365
		 24 0.13843426108360293 24.8 0.14167177677154541 25.6 0.13715019822120669 26.4 0.13803811371326449
		 27.2 0.13747803866863251 28 0.1401418149471283 28.8 0.14064724743366239 29.6 0.14105705916881561
		 30.4 0.14119365811347959 31.2 0.14311978220939636 32 0.14784626662731171 32.8 0.15332408249378204
		 33.6 0.15780468285083771 34.4 0.15929366648197174 35.2 0.16585063934326172 36 0.17165631055831909
		 36.8 0.17706581950187683 37.6 0.18095901608467105 38.4 0.18456536531448364 39.2 0.19016611576080319
		 40 0.19819842278957367 40.8 0.19654551148414612 41.6 0.19987864792346952 42.4 0.19960543513298037
		 43.2 0.19699630141258245 44 0.19593079388141632 44.8 0.19364951550960541 45.6 0.19457842409610748
		 46.4 0.1925976574420929 47.2 0.19135457277297976 48 0.19121795892715457 48.8 0.19146385788917544
		 49.6 0.18912792205810547;
createNode animCurveTA -n "Bip01_R_Thigh_rotateZ6";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 63 ".ktv[0:62]"  0 4.0412449836730957 0.8 4.1016378402709961
		 1.6 4.223597526550293 2.4 4.3446965217590332 3.2 4.4206347465515137 4 4.5381960868835449
		 4.8 4.729987621307373 5.6 4.9280357360839844 6.4 5.0362262725830078 7.2 5.0514984130859375
		 8 5.066948413848877 8.8 5.119499683380127 9.6 5.2246437072753906 10.4 5.3557968139648438
		 11.2 5.4613232612609863 12 5.5602927207946777 12.8 5.7506227493286133 13.6 6.0071239471435547
		 14.4 6.1968255043029785 15.2 6.2101850509643564 16 6.1594367027282715 16.8 6.2176985740661621
		 17.6 6.3714594841003427 18.4 6.5165057182312012 19.2 6.6147918701171875 20 6.6751847267150879
		 20.8 6.7579116821289062 21.6 6.9164948463439941 22.4 7.1144747734069824 23.2 7.2480869293212891
		 24 7.2580180168151855 24.8 7.181041717529296 25.6 7.1452655792236328 26.4 7.2058081626892099
		 27.2 7.2752437591552734 28 7.34474802017212 28.8 7.5195322036743164 29.6 7.7018847465515137
		 30.4 7.7522506713867179 31.2 7.7738614082336461 32 7.8008131980895996 32.8 7.7202987670898438
		 33.6 7.5987491607665998 34.4 7.5971369743347168 35.2 7.6047186851501456 36 7.493072509765625
		 36.8 7.3512773513793954 37.6 7.036501407623291 38.4 6.0037903785705566 39.2 4.0139651298522958
		 40 1.730032205581665 40.8 0.13064785301685333 41.6 -0.53010463714599609 42.4 -0.67965841293334972
		 43.2 -0.68572366237640381 44 -0.64097225666046143 44.8 -0.6281588077545166 45.6 -0.68285495042800903
		 46.4 -0.68960320949554443 47.2 -0.64053511619567871 48 -0.60075610876083374 48.8 -0.50059819221496582
		 49.6 -0.35402235388755798;
createNode animCurveTA -n "Bip01_R_Thigh_rotateY6";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 63 ".ktv[0:62]"  0 180.60348510742187 0.8 180.61732482910156
		 1.6 180.65177917480469 2.4 180.71414184570312 3.2 180.7791748046875 4 180.78681945800781
		 4.8 180.71969604492187 5.6 180.68064880371097 6.4 180.7040100097656 7.2 180.76115417480469
		 8 180.83531188964847 8.8 180.92529296875 9.6 181.01416015625003 10.4 181.0874938964844
		 11.2 181.15266418457031 12 181.17965698242187 12.8 181.13681030273435 13.6 181.05165100097656
		 14.4 181.0372314453125 15.2 181.15390014648435 16 181.32228088378903 16.8 181.43724060058591
		 17.6 181.48248291015625 18.4 181.50164794921881 19.2 181.52656555175784 20 181.55708312988281
		 20.8 181.56906127929687 21.6 181.57392883300781 22.4 181.5892639160156 23.2 181.60687255859375
		 24 181.61643981933591 24.8 181.65232849121097 25.6 181.70234680175781 26.4 181.73588562011719
		 27.2 181.74856567382812 28 181.76039123535156 28.8 181.76362609863281 29.6 181.7503662109375
		 30.4 181.73255920410156 31.2 181.70683288574224 32 181.6710205078125 32.8 181.64566040039065
		 33.6 181.64093017578125 34.4 181.62173461914065 35.2 181.563232421875 36 181.48207092285156
		 36.8 181.41757202148435 37.6 181.3516845703125 38.4 181.29521179199219 39.2 181.29733276367188
		 40 181.26963806152344 40.8 181.11354064941409 41.6 180.936279296875 42.4 180.81651306152344
		 43.2 180.70166015625 44 180.5975646972656 44.8 180.54605102539065 45.6 180.55227661132812
		 46.4 180.55720520019531 47.2 180.47315979003903 48 180.30047607421875 48.8 180.03643798828125
		 49.6 179.65174865722656;
createNode animCurveTA -n "Bip01_R_Calf_rotateX6";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 63 ".ktv[0:62]"  0 0.062431126832962043 0.8 0.061442971229553216
		 1.6 0.061909291893243797 2.4 0.062781378626823425 3.2 0.062402497977018363 4 0.061718214303255081
		 4.8 0.062171246856451035 5.6 0.063059285283088684 6.4 0.062245585024356849 7.2 0.059850748628377921
		 8 0.057724852114915848 8.8 0.056515935808420181 9.6 0.05629350990056993 10.4 0.056523378938436522
		 11.2 0.056081738322973251 12 0.05535439029335977 12.8 0.055761609226465225 13.6 0.056750003248453147
		 14.4 0.0567127838730812 15.2 0.055052634328603745 16 0.053187582641839981 16.8 0.053127050399780273
		 17.6 0.054254535585641861 18.4 0.054817937314510352 19.2 0.054547939449548721 20 0.053479254245758057
		 20.8 0.052785821259021759 21.6 0.054261516779661179 22.4 0.05704887211322783 23.2 0.058552600443363197
		 24 0.057276137173175812 24.8 0.053819041699171073 25.6 0.051395013928413391 26.4 0.051590435206890113
		 27.2 0.052302949130535126 28 0.053977157920598984 28.8 0.058702997863292694 29.6 0.063157491385936737
		 30.4 0.063589408993721008 31.2 0.062637165188789368 32 0.06181001290678978 32.8 0.058655388653278351
		 33.6 0.055882327258586877 34.4 0.057817649096250534 35.2 0.060872003436088562 36 0.060373988002538674
		 36.8 0.058485846966505051 37.6 0.057158775627613075 38.4 0.052052631974220269 39.2 0.04036900773644448
		 40 0.025924690067768097 40.8 0.015705626457929611 41.6 0.012310423888266088 42.4 0.013438945636153219
		 43.2 0.017289105802774429 44 0.023206412792205811 44.8 0.027748679742217064 45.6 0.028452469035983079
		 46.4 0.028185788542032245 47.2 0.028792031109333038 48 0.029420541599392891 48.8 0.030879227444529533
		 49.6 0.032803952693939209;
createNode animCurveTA -n "Bip01_R_Calf_rotateZ6";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 63 ".ktv[0:62]"  0 -0.05368659645318985 0.8 -0.13105550408363342
		 1.6 -0.1093145087361336 2.4 -0.054729536175727844 3.2 -0.07547219842672348 4 -0.1179480776190758
		 4.8 -0.10335285216569901 5.6 -0.055992599576711655 6.4 -0.10407310724258424 7.2 -0.26935920119285583
		 8 -0.40108460187911982 8.8 -0.47393837571144104 9.6 -0.48671808838844299 10.4 -0.47112822532653809
		 11.2 -0.48462200164794922 12 -0.5408971905708313 12.8 -0.52322280406951904 13.6 -0.46898296475410461
		 14.4 -0.46904569864273066 15.2 -0.57353007793426514 16 -0.68573474884033214 16.8 -0.67838954925537109
		 17.6 -0.59265196323394775 18.4 -0.54491907358169556 19.2 -0.5714719295501709 20 -0.65253716707229614
		 20.8 -0.70679223537445068 21.6 -0.61336588859558105 22.4 -0.42235973477363586 23.2 -0.33555865287780762
		 24 -0.41990232467651367 24.8 -0.6324615478515625 25.6 -0.76602399349212635 26.4 -0.7412840723991394
		 27.2 -0.69444125890731812 28 -0.59238147735595703 28.8 -0.27094092965126038 29.6 0.037713665515184402
		 30.4 0.06125043332576751 31.2 -0.0040169931016862392 32 -0.057810850441455841 32.8 -0.25864741206169128
		 33.6 -0.44541394710540766 34.4 -0.29673460125923157 35.2 -0.066201090812683105 36 -0.093075267970561981
		 36.8 -0.19764129817485809 37.6 -0.27698197960853577 38.4 -0.65373653173446655 39.2 -1.4861941337585447
		 40 -2.4929797649383545 40.8 -3.180509090423584 41.6 -3.3940408229827881 42.4 -3.288529634475708
		 43.2 -2.9961643218994141 44 -2.5681607723236084 44.8 -2.2280547618865967 45.6 -2.1480081081390381
		 46.4 -2.1392104625701904 47.2 -2.0925664901733398 48 -2.0558946132659912 48.8 -1.9455755949020388
		 49.6 -1.8421354293823244;
createNode animCurveTA -n "Bip01_R_Calf_rotateY6";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 63 ".ktv[0:62]"  0 1.0543361902236938 0.8 1.0543050765991211
		 1.6 1.0543384552001951 2.4 1.0543755292892456 3.2 1.0543608665466309 4 1.0543270111083984
		 4.8 1.05436110496521 5.6 1.0544114112854004 6.4 1.0543707609176636 7.2 1.0542707443237305
		 8 1.0541630983352659 8.8 1.0541009902954102 9.6 1.0540815591812134 10.4 1.0541017055511477
		 11.2 1.054054856300354 12 1.0540305376052856 12.8 1.0540615320205688 13.6 1.0541222095489504
		 14.4 1.0541149377822876 15.2 1.0540302991867063 16 1.0539203882217407 16.8 1.0539031028747561
		 17.6 1.0539486408233645 18.4 1.0539635419845581 19.2 1.0539594888687136 20 1.0539172887802124
		 20.8 1.05389404296875 21.6 1.0539755821228027 22.4 1.0541043281555176 23.2 1.0541919469833374
		 24 1.0541322231292725 24.8 1.0539427995681765 25.6 1.0537819862365725 26.4 1.0537782907485962
		 27.2 1.0538215637207031 28 1.0539165735244751 28.8 1.054135322570801 29.6 1.0543150901794434
		 30.4 1.0543379783630371 31.2 1.0543015003204346 32 1.0542675256729126 32.8 1.0541104078292849
		 33.6 1.0539836883544922 34.4 1.0540505647659302 35.2 1.0541598796844482 36 1.0541298389434814
		 36.8 1.054023265838623 37.6 1.0539491176605225 38.4 1.0537377595901487 39.2 1.0531233549118044
		 40 1.0521671772003174 40.8 1.0513348579406738 41.6 1.0510209798812866 42.4 1.0510822534561155
		 43.2 1.0513743162155151 44 1.051827073097229 44.8 1.0521343946456907 45.6 1.0521507263183596
		 46.4 1.0520951747894287 47.2 1.0521324872970581 48 1.0521845817565918 48.8 1.0522809028625488
		 49.6 1.0524449348449707;
createNode animCurveTA -n "Bip01_R_Foot_rotateX6";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 63 ".ktv[0:62]"  0 11.064865112304687 0.8 11.136499404907228
		 1.6 11.226291656494141 2.4 11.28602123260498 3.2 11.283879280090332 4 11.263833045959473
		 4.8 11.25908374786377 5.6 11.25966739654541 6.4 11.32436466217041 7.2 11.41270732879639
		 8 11.566999435424805 8.8 11.720243453979492 9.6 11.872377395629885 10.4 11.964347839355469
		 11.2 11.946179389953612 12 11.806726455688477 12.8 11.733952522277832 13.6 11.766014099121094
		 14.4 11.80013942718506 15.2 11.756061553955078 16 11.677745819091797 16.8 11.631694793701172
		 17.6 11.626339912414553 18.4 11.588596343994141 19.2 11.551859855651855 20 11.454962730407717
		 20.8 11.316554069519045 21.6 11.235922813415527 22.4 11.296348571777344 23.2 11.360260009765623
		 24 11.397595405578612 24.8 11.531192779541016 25.6 11.731819152832031 26.4 11.81102466583252
		 27.2 11.798806190490724 28 11.849815368652344 28.8 11.864922523498535 29.6 11.777562141418455
		 30.4 11.722021102905272 31.2 11.738866806030272 32 11.768479347229004 32.8 11.879186630249023
		 33.6 12.038527488708496 34.4 12.159269332885742 35.2 12.254276275634766 36 12.29653263092041
		 36.8 12.383241653442385 37.6 12.308472633361816 38.4 11.825679779052734 39.2 11.068618774414064
		 40 10.403958320617676 40.8 9.9515113830566406 41.6 9.7649955749511719 42.4 9.7977561950683594
		 43.2 9.9053592681884766 44 10.059232711791992 44.8 10.168523788452148 45.6 10.208122253417969
		 46.4 10.294496536254885 47.2 10.378183364868164 48 10.385994911193848 48.8 10.43459510803223
		 49.6 10.379908561706545;
createNode animCurveTA -n "Bip01_R_Foot_rotateY6";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 63 ".ktv[0:62]"  0 -2.260185718536377 0.8 -2.302464485168457
		 1.6 -2.3673017024993896 2.4 -2.4006528854370117 3.2 -2.3899862766265869 4 -2.3861215114593506
		 4.8 -2.3684184551239018 5.6 -2.3704686164855957 6.4 -2.4218509197235107 7.2 -2.4779632091522217
		 8 -2.4999074935913086 8.8 -2.6554334163665771 9.6 -2.7480528354644775 10.4 -2.784343004226685
		 11.2 -2.7926914691925049 12 -2.7059860229492187 12.8 -2.6837303638458252 13.6 -2.6972522735595703
		 14.4 -2.6936721801757813 15.2 -2.6890530586242676 16 -2.6383383274078369 16.8 -2.6076805591583252
		 17.6 -2.608417272567749 18.4 -2.5905411243438721 19.2 -2.5505115985870361 20 -2.4988436698913574
		 20.8 -2.4355554580688477 21.6 -2.387500524520874 22.4 -2.4153294563293457 23.2 -2.4510684013366699
		 24 -2.473496675491333 24.8 -2.5448050498962398 25.6 -2.6658895015716553 26.4 -2.700728178024292
		 27.2 -2.6882789134979248 28 -2.707612276077271 28.8 -2.7237143516540527 29.6 -2.6612403392791748
		 30.4 -2.6286995410919189 31.2 -2.6430482864379883 32 -2.6679441928863525 32.8 -2.7244532108306885
		 33.6 -2.8230648040771484 34.4 -2.8834180831909184 35.2 -2.9042694568634033 36 -2.9262349605560303
		 36.8 -2.9630050659179687 37.6 -2.9226632118225098 38.4 -2.6578536033630371 39.2 -2.2699868679046631
		 40 -1.9140340089797971 40.8 -1.6756399869918823 41.6 -1.56983482837677 42.4 -1.566648006439209
		 43.2 -1.6153131723403933 44 -1.6785228252410889 44.8 -1.722792387008667 45.6 -1.7441442012786863
		 46.4 -1.7827640771865845 47.2 -1.8106951713562012 48 -1.836723804473877 48.8 -1.8267583847045896
		 49.6 -1.8146027326583871;
createNode animCurveTA -n "Bip01_R_Foot_rotateZ6";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 63 ".ktv[0:62]"  0 4.4232044219970703 0.8 4.5637831687927246
		 1.6 4.6819329261779785 2.4 4.768376350402832 3.2 4.822535514831543 4 4.8638582229614267
		 4.8 4.9094195365905762 5.6 4.9763741493225098 6.4 5.0798869132995605 7.2 5.1791090965270996
		 8 5.2402973175048828 8.8 5.3171796798706055 9.6 5.3906431198120117 10.4 5.3851923942565927
		 11.2 5.3587603569030762 12 5.3814816474914551 12.8 5.4429636001586914 13.6 5.5370979309082031
		 14.4 5.5723109245300293 15.2 5.4879460334777832 16 5.4006009101867676 16.8 5.3209948539733887
		 17.6 5.1754903793334961 18.4 5.0978221893310547 19.2 5.1007838249206543 20 5.2030544281005859
		 20.8 5.2910699844360352 21.6 5.2446026802062988 22.4 5.1501674652099609 23.2 5.185772418975831
		 24 5.3027529716491699 24.8 5.316314697265625 25.6 5.1929817199707031 26.4 5.1021332740783691
		 27.2 5.1595230102539062 28 5.1624484062194824 28.8 4.914860725402832 29.6 4.6657652854919434
		 30.4 4.6718940734863281 31.2 4.6998634338378906 32 4.6676101684570313 32.8 4.8099150657653809
		 33.6 4.9415316581726074 34.4 4.7370595932006836 35.2 4.4099111557006836 36 4.2449522018432617
		 36.8 4.1581339836120605 37.6 4.0945839881896973 38.4 4.2998828887939453 39.2 4.8866887092590332
		 40 5.4185142517089844 40.8 5.4917917251586914 41.6 5.2732396125793457 42.4 4.9875011444091797
		 43.2 4.5828042030334473 44 4.0538463592529297 44.8 3.5710229873657227 45.6 3.2336122989654541
		 46.4 3.0300495624542236 47.2 2.9686627388000488 48 2.9459171295166016 48.8 2.8737473487854004
		 49.6 2.8603549003601074;
select -ne :time1;
	setAttr ".o" 24.2;
	setAttr ".unw" 24.2;
select -ne :renderPartition;
	setAttr -s 126 ".st";
select -ne :initialShadingGroup;
	setAttr ".ro" yes;
select -ne :initialParticleSE;
	setAttr ".ro" yes;
select -ne :defaultShaderList1;
	setAttr -s 64 ".s";
select -ne :defaultTextureList1;
	setAttr -s 31 ".tx";
select -ne :postProcessList1;
	setAttr -s 2 ".p";
select -ne :defaultRenderUtilityList1;
	setAttr -s 31 ".u";
select -ne :defaultRenderingList1;
select -ne :renderGlobalsList1;
select -ne :defaultResolution;
	setAttr ".pa" 1;
select -ne :hardwareRenderGlobals;
	setAttr ".ctrs" 256;
	setAttr ".btrs" 512;
select -ne :hardwareRenderingGlobals;
	setAttr ".otfna" -type "stringArray" 18 "NURBS Curves" "NURBS Surfaces" "Polygons" "Subdiv Surfaces" "Particles" "Fluids" "Image Planes" "UI:" "Lights" "Cameras" "Locators" "Joints" "IK Handles" "Deformers" "Motion Trails" "Components" "Misc. UI" "Ornaments"  ;
	setAttr ".otfva" -type "Int32Array" 18 0 1 1 1 1 1
		 1 0 0 0 0 0 0 0 0 0 0 0 ;
select -ne :defaultHardwareRenderGlobals;
	setAttr ".fn" -type "string" "im";
	setAttr ".res" -type "string" "ntsc_4d 646 485 1.333";
select -ne :hyperGraphLayout;
	setAttr -s 4 ".hyp";
	setAttr ".hyp[1].isc" yes;
	setAttr ".hyp[2].isc" yes;
	setAttr ".hyp[3].isc" yes;
	setAttr ".hyp[4].isc" yes;
connectAttr "Bip01_Spine_translateX6.o" "|Stand_Wave_3|Bip01_Spine.tx";
connectAttr "Bip01_Spine_translateY6.o" "|Stand_Wave_3|Bip01_Spine.ty";
connectAttr "Bip01_Spine_translateZ6.o" "|Stand_Wave_3|Bip01_Spine.tz";
connectAttr "Bip01_Spine_rotateX6.o" "|Stand_Wave_3|Bip01_Spine.rx";
connectAttr "Bip01_Spine_rotateY6.o" "|Stand_Wave_3|Bip01_Spine.ry";
connectAttr "Bip01_Spine_rotateZ6.o" "|Stand_Wave_3|Bip01_Spine.rz";
connectAttr "Bip01_Spine1_rotateX6.o" "|Stand_Wave_3|Bip01_Spine|Bip01_Spine1.rx"
		;
connectAttr "Bip01_Spine1_rotateY6.o" "|Stand_Wave_3|Bip01_Spine|Bip01_Spine1.ry"
		;
connectAttr "Bip01_Spine1_rotateZ6.o" "|Stand_Wave_3|Bip01_Spine|Bip01_Spine1.rz"
		;
connectAttr "|Stand_Wave_3|Bip01_Spine.s" "|Stand_Wave_3|Bip01_Spine|Bip01_Spine1.is"
		;
connectAttr "Bip01_Spine2_rotateX6.o" "|Stand_Wave_3|Bip01_Spine|Bip01_Spine1|Bip01_Spine2.rx"
		;
connectAttr "Bip01_Spine2_rotateY6.o" "|Stand_Wave_3|Bip01_Spine|Bip01_Spine1|Bip01_Spine2.ry"
		;
connectAttr "Bip01_Spine2_rotateZ6.o" "|Stand_Wave_3|Bip01_Spine|Bip01_Spine1|Bip01_Spine2.rz"
		;
connectAttr "|Stand_Wave_3|Bip01_Spine|Bip01_Spine1.s" "|Stand_Wave_3|Bip01_Spine|Bip01_Spine1|Bip01_Spine2.is"
		;
connectAttr "|Stand_Wave_3|Bip01_Spine|Bip01_Spine1|Bip01_Spine2.s" "|Stand_Wave_3|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck.is"
		;
connectAttr "|Stand_Wave_3|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck.s" "|Stand_Wave_3|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_Head.is"
		;
connectAttr "|Stand_Wave_3|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_Head.s" "|Stand_Wave_3|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_Head|_notused_Bip01_HeadNub.is"
		;
connectAttr "Bip01_R_Clavicle_rotateX6.o" "|Stand_Wave_3|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_R_Clavicle.rx"
		;
connectAttr "Bip01_R_Clavicle_rotateY6.o" "|Stand_Wave_3|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_R_Clavicle.ry"
		;
connectAttr "Bip01_R_Clavicle_rotateZ6.o" "|Stand_Wave_3|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_R_Clavicle.rz"
		;
connectAttr "|Stand_Wave_3|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck.s" "|Stand_Wave_3|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_R_Clavicle.is"
		;
connectAttr "Bip01_R_UpperArm_rotateX6.o" "|Stand_Wave_3|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_R_Clavicle|Bip01_R_UpperArm.rx"
		;
connectAttr "Bip01_R_UpperArm_rotateY6.o" "|Stand_Wave_3|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_R_Clavicle|Bip01_R_UpperArm.ry"
		;
connectAttr "Bip01_R_UpperArm_rotateZ6.o" "|Stand_Wave_3|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_R_Clavicle|Bip01_R_UpperArm.rz"
		;
connectAttr "|Stand_Wave_3|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_R_Clavicle.s" "|Stand_Wave_3|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_R_Clavicle|Bip01_R_UpperArm.is"
		;
connectAttr "Bip01_R_Forearm_rotateX6.o" "|Stand_Wave_3|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_R_Clavicle|Bip01_R_UpperArm|Bip01_R_Forearm.rx"
		;
connectAttr "Bip01_R_Forearm_rotateY6.o" "|Stand_Wave_3|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_R_Clavicle|Bip01_R_UpperArm|Bip01_R_Forearm.ry"
		;
connectAttr "Bip01_R_Forearm_rotateZ6.o" "|Stand_Wave_3|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_R_Clavicle|Bip01_R_UpperArm|Bip01_R_Forearm.rz"
		;
connectAttr "|Stand_Wave_3|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_R_Clavicle|Bip01_R_UpperArm.s" "|Stand_Wave_3|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_R_Clavicle|Bip01_R_UpperArm|Bip01_R_Forearm.is"
		;
connectAttr "|Stand_Wave_3|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_R_Clavicle|Bip01_R_UpperArm|Bip01_R_Forearm.s" "|Stand_Wave_3|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_R_Clavicle|Bip01_R_UpperArm|Bip01_R_Forearm|Bip01_R_Hand.is"
		;
connectAttr "|Stand_Wave_3|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_R_Clavicle|Bip01_R_UpperArm|Bip01_R_Forearm|Bip01_R_Hand.s" "|Stand_Wave_3|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_R_Clavicle|Bip01_R_UpperArm|Bip01_R_Forearm|Bip01_R_Hand|Bip01_R_Finger0.is"
		;
connectAttr "|Stand_Wave_3|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_R_Clavicle|Bip01_R_UpperArm|Bip01_R_Forearm|Bip01_R_Hand|Bip01_R_Finger0.s" "|Stand_Wave_3|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_R_Clavicle|Bip01_R_UpperArm|Bip01_R_Forearm|Bip01_R_Hand|Bip01_R_Finger0|Bip01_R_Finger01.is"
		;
connectAttr "|Stand_Wave_3|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_R_Clavicle|Bip01_R_UpperArm|Bip01_R_Forearm|Bip01_R_Hand|Bip01_R_Finger0|Bip01_R_Finger01.s" "|Stand_Wave_3|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_R_Clavicle|Bip01_R_UpperArm|Bip01_R_Forearm|Bip01_R_Hand|Bip01_R_Finger0|Bip01_R_Finger01|_notused_Bip01_R_Finger0Nub.is"
		;
connectAttr "|Stand_Wave_3|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_R_Clavicle|Bip01_R_UpperArm|Bip01_R_Forearm|Bip01_R_Hand.s" "|Stand_Wave_3|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_R_Clavicle|Bip01_R_UpperArm|Bip01_R_Forearm|Bip01_R_Hand|Bip01_R_Finger2.is"
		;
connectAttr "|Stand_Wave_3|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_R_Clavicle|Bip01_R_UpperArm|Bip01_R_Forearm|Bip01_R_Hand|Bip01_R_Finger2.s" "|Stand_Wave_3|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_R_Clavicle|Bip01_R_UpperArm|Bip01_R_Forearm|Bip01_R_Hand|Bip01_R_Finger2|Bip01_R_Finger21.is"
		;
connectAttr "|Stand_Wave_3|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_R_Clavicle|Bip01_R_UpperArm|Bip01_R_Forearm|Bip01_R_Hand|Bip01_R_Finger2|Bip01_R_Finger21.s" "|Stand_Wave_3|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_R_Clavicle|Bip01_R_UpperArm|Bip01_R_Forearm|Bip01_R_Hand|Bip01_R_Finger2|Bip01_R_Finger21|_notused_Bip01_R_Finger2Nub.is"
		;
connectAttr "|Stand_Wave_3|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_R_Clavicle|Bip01_R_UpperArm|Bip01_R_Forearm|Bip01_R_Hand.s" "|Stand_Wave_3|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_R_Clavicle|Bip01_R_UpperArm|Bip01_R_Forearm|Bip01_R_Hand|Bip01_R_Finger1.is"
		;
connectAttr "|Stand_Wave_3|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_R_Clavicle|Bip01_R_UpperArm|Bip01_R_Forearm|Bip01_R_Hand|Bip01_R_Finger1.s" "|Stand_Wave_3|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_R_Clavicle|Bip01_R_UpperArm|Bip01_R_Forearm|Bip01_R_Hand|Bip01_R_Finger1|Bip01_R_Finger11.is"
		;
connectAttr "|Stand_Wave_3|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_R_Clavicle|Bip01_R_UpperArm|Bip01_R_Forearm|Bip01_R_Hand|Bip01_R_Finger1|Bip01_R_Finger11.s" "|Stand_Wave_3|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_R_Clavicle|Bip01_R_UpperArm|Bip01_R_Forearm|Bip01_R_Hand|Bip01_R_Finger1|Bip01_R_Finger11|_notused_Bip01_R_Finger1Nub.is"
		;
connectAttr "|Stand_Wave_3|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_R_Clavicle|Bip01_R_UpperArm|Bip01_R_Forearm|Bip01_R_Hand.s" "|Stand_Wave_3|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_R_Clavicle|Bip01_R_UpperArm|Bip01_R_Forearm|Bip01_R_Hand|Bip01_R_Finger3.is"
		;
connectAttr "|Stand_Wave_3|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_R_Clavicle|Bip01_R_UpperArm|Bip01_R_Forearm|Bip01_R_Hand|Bip01_R_Finger3.s" "|Stand_Wave_3|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_R_Clavicle|Bip01_R_UpperArm|Bip01_R_Forearm|Bip01_R_Hand|Bip01_R_Finger3|Bip01_R_Finger31.is"
		;
connectAttr "|Stand_Wave_3|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_R_Clavicle|Bip01_R_UpperArm|Bip01_R_Forearm|Bip01_R_Hand|Bip01_R_Finger3|Bip01_R_Finger31.s" "|Stand_Wave_3|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_R_Clavicle|Bip01_R_UpperArm|Bip01_R_Forearm|Bip01_R_Hand|Bip01_R_Finger3|Bip01_R_Finger31|_notused_Bip01_R_Finger3Nub.is"
		;
connectAttr "|Stand_Wave_3|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_R_Clavicle|Bip01_R_UpperArm|Bip01_R_Forearm|Bip01_R_Hand.s" "|Stand_Wave_3|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_R_Clavicle|Bip01_R_UpperArm|Bip01_R_Forearm|Bip01_R_Hand|Bip01_R_Finger4.is"
		;
connectAttr "|Stand_Wave_3|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_R_Clavicle|Bip01_R_UpperArm|Bip01_R_Forearm|Bip01_R_Hand|Bip01_R_Finger4.s" "|Stand_Wave_3|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_R_Clavicle|Bip01_R_UpperArm|Bip01_R_Forearm|Bip01_R_Hand|Bip01_R_Finger4|Bip01_R_Finger41.is"
		;
connectAttr "|Stand_Wave_3|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_R_Clavicle|Bip01_R_UpperArm|Bip01_R_Forearm|Bip01_R_Hand|Bip01_R_Finger4|Bip01_R_Finger41.s" "|Stand_Wave_3|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_R_Clavicle|Bip01_R_UpperArm|Bip01_R_Forearm|Bip01_R_Hand|Bip01_R_Finger4|Bip01_R_Finger41|_notused_Bip01_R_Finger4Nub.is"
		;
connectAttr "Bip01_L_Clavicle_rotateX6.o" "|Stand_Wave_3|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_L_Clavicle.rx"
		;
connectAttr "Bip01_L_Clavicle_rotateY6.o" "|Stand_Wave_3|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_L_Clavicle.ry"
		;
connectAttr "Bip01_L_Clavicle_rotateZ6.o" "|Stand_Wave_3|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_L_Clavicle.rz"
		;
connectAttr "|Stand_Wave_3|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck.s" "|Stand_Wave_3|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_L_Clavicle.is"
		;
connectAttr "Bip01_L_UpperArm_rotateX6.o" "|Stand_Wave_3|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_L_Clavicle|Bip01_L_UpperArm.rx"
		;
connectAttr "Bip01_L_UpperArm_rotateY6.o" "|Stand_Wave_3|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_L_Clavicle|Bip01_L_UpperArm.ry"
		;
connectAttr "Bip01_L_UpperArm_rotateZ6.o" "|Stand_Wave_3|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_L_Clavicle|Bip01_L_UpperArm.rz"
		;
connectAttr "|Stand_Wave_3|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_L_Clavicle.s" "|Stand_Wave_3|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_L_Clavicle|Bip01_L_UpperArm.is"
		;
connectAttr "Bip01_L_Forearm_rotateX6.o" "|Stand_Wave_3|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_L_Clavicle|Bip01_L_UpperArm|Bip01_L_Forearm.rx"
		;
connectAttr "Bip01_L_Forearm_rotateY6.o" "|Stand_Wave_3|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_L_Clavicle|Bip01_L_UpperArm|Bip01_L_Forearm.ry"
		;
connectAttr "Bip01_L_Forearm_rotateZ6.o" "|Stand_Wave_3|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_L_Clavicle|Bip01_L_UpperArm|Bip01_L_Forearm.rz"
		;
connectAttr "|Stand_Wave_3|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_L_Clavicle|Bip01_L_UpperArm.s" "|Stand_Wave_3|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_L_Clavicle|Bip01_L_UpperArm|Bip01_L_Forearm.is"
		;
connectAttr "|Stand_Wave_3|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_L_Clavicle|Bip01_L_UpperArm|Bip01_L_Forearm.s" "|Stand_Wave_3|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_L_Clavicle|Bip01_L_UpperArm|Bip01_L_Forearm|Bip01_L_Hand.is"
		;
connectAttr "|Stand_Wave_3|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_L_Clavicle|Bip01_L_UpperArm|Bip01_L_Forearm|Bip01_L_Hand.s" "|Stand_Wave_3|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_L_Clavicle|Bip01_L_UpperArm|Bip01_L_Forearm|Bip01_L_Hand|Bip01_L_Finger0.is"
		;
connectAttr "|Stand_Wave_3|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_L_Clavicle|Bip01_L_UpperArm|Bip01_L_Forearm|Bip01_L_Hand|Bip01_L_Finger0.s" "|Stand_Wave_3|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_L_Clavicle|Bip01_L_UpperArm|Bip01_L_Forearm|Bip01_L_Hand|Bip01_L_Finger0|Bip01_L_Finger01.is"
		;
connectAttr "|Stand_Wave_3|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_L_Clavicle|Bip01_L_UpperArm|Bip01_L_Forearm|Bip01_L_Hand|Bip01_L_Finger0|Bip01_L_Finger01.s" "|Stand_Wave_3|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_L_Clavicle|Bip01_L_UpperArm|Bip01_L_Forearm|Bip01_L_Hand|Bip01_L_Finger0|Bip01_L_Finger01|_notused_Bip01_L_Finger0Nub.is"
		;
connectAttr "|Stand_Wave_3|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_L_Clavicle|Bip01_L_UpperArm|Bip01_L_Forearm|Bip01_L_Hand.s" "|Stand_Wave_3|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_L_Clavicle|Bip01_L_UpperArm|Bip01_L_Forearm|Bip01_L_Hand|Bip01_L_Finger2.is"
		;
connectAttr "|Stand_Wave_3|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_L_Clavicle|Bip01_L_UpperArm|Bip01_L_Forearm|Bip01_L_Hand|Bip01_L_Finger2.s" "|Stand_Wave_3|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_L_Clavicle|Bip01_L_UpperArm|Bip01_L_Forearm|Bip01_L_Hand|Bip01_L_Finger2|Bip01_L_Finger21.is"
		;
connectAttr "|Stand_Wave_3|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_L_Clavicle|Bip01_L_UpperArm|Bip01_L_Forearm|Bip01_L_Hand|Bip01_L_Finger2|Bip01_L_Finger21.s" "|Stand_Wave_3|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_L_Clavicle|Bip01_L_UpperArm|Bip01_L_Forearm|Bip01_L_Hand|Bip01_L_Finger2|Bip01_L_Finger21|_notused_Bip01_L_Finger2Nub.is"
		;
connectAttr "|Stand_Wave_3|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_L_Clavicle|Bip01_L_UpperArm|Bip01_L_Forearm|Bip01_L_Hand.s" "|Stand_Wave_3|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_L_Clavicle|Bip01_L_UpperArm|Bip01_L_Forearm|Bip01_L_Hand|Bip01_L_Finger1.is"
		;
connectAttr "|Stand_Wave_3|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_L_Clavicle|Bip01_L_UpperArm|Bip01_L_Forearm|Bip01_L_Hand|Bip01_L_Finger1.s" "|Stand_Wave_3|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_L_Clavicle|Bip01_L_UpperArm|Bip01_L_Forearm|Bip01_L_Hand|Bip01_L_Finger1|Bip01_L_Finger11.is"
		;
connectAttr "|Stand_Wave_3|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_L_Clavicle|Bip01_L_UpperArm|Bip01_L_Forearm|Bip01_L_Hand|Bip01_L_Finger1|Bip01_L_Finger11.s" "|Stand_Wave_3|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_L_Clavicle|Bip01_L_UpperArm|Bip01_L_Forearm|Bip01_L_Hand|Bip01_L_Finger1|Bip01_L_Finger11|_notused_Bip01_L_Finger1Nub.is"
		;
connectAttr "|Stand_Wave_3|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_L_Clavicle|Bip01_L_UpperArm|Bip01_L_Forearm|Bip01_L_Hand.s" "|Stand_Wave_3|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_L_Clavicle|Bip01_L_UpperArm|Bip01_L_Forearm|Bip01_L_Hand|Bip01_L_Finger3.is"
		;
connectAttr "|Stand_Wave_3|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_L_Clavicle|Bip01_L_UpperArm|Bip01_L_Forearm|Bip01_L_Hand|Bip01_L_Finger3.s" "|Stand_Wave_3|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_L_Clavicle|Bip01_L_UpperArm|Bip01_L_Forearm|Bip01_L_Hand|Bip01_L_Finger3|Bip01_L_Finger31.is"
		;
connectAttr "|Stand_Wave_3|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_L_Clavicle|Bip01_L_UpperArm|Bip01_L_Forearm|Bip01_L_Hand|Bip01_L_Finger3|Bip01_L_Finger31.s" "|Stand_Wave_3|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_L_Clavicle|Bip01_L_UpperArm|Bip01_L_Forearm|Bip01_L_Hand|Bip01_L_Finger3|Bip01_L_Finger31|_notused_Bip01_L_Finger3Nub.is"
		;
connectAttr "|Stand_Wave_3|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_L_Clavicle|Bip01_L_UpperArm|Bip01_L_Forearm|Bip01_L_Hand.s" "|Stand_Wave_3|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_L_Clavicle|Bip01_L_UpperArm|Bip01_L_Forearm|Bip01_L_Hand|Bip01_L_Finger4.is"
		;
connectAttr "|Stand_Wave_3|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_L_Clavicle|Bip01_L_UpperArm|Bip01_L_Forearm|Bip01_L_Hand|Bip01_L_Finger4.s" "|Stand_Wave_3|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_L_Clavicle|Bip01_L_UpperArm|Bip01_L_Forearm|Bip01_L_Hand|Bip01_L_Finger4|Bip01_L_Finger41.is"
		;
connectAttr "|Stand_Wave_3|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_L_Clavicle|Bip01_L_UpperArm|Bip01_L_Forearm|Bip01_L_Hand|Bip01_L_Finger4|Bip01_L_Finger41.s" "|Stand_Wave_3|Bip01_Spine|Bip01_Spine1|Bip01_Spine2|Bip01_Neck|Bip01_L_Clavicle|Bip01_L_UpperArm|Bip01_L_Forearm|Bip01_L_Hand|Bip01_L_Finger4|Bip01_L_Finger41|_notused_Bip01_L_Finger4Nub.is"
		;
connectAttr "Bip01_L_Thigh_rotateX6.o" "|Stand_Wave_3|Bip01_Spine|Bip01_L_Thigh.rx"
		;
connectAttr "Bip01_L_Thigh_rotateY6.o" "|Stand_Wave_3|Bip01_Spine|Bip01_L_Thigh.ry"
		;
connectAttr "Bip01_L_Thigh_rotateZ6.o" "|Stand_Wave_3|Bip01_Spine|Bip01_L_Thigh.rz"
		;
connectAttr "|Stand_Wave_3|Bip01_Spine.s" "|Stand_Wave_3|Bip01_Spine|Bip01_L_Thigh.is"
		;
connectAttr "Bip01_L_Calf_rotateX6.o" "|Stand_Wave_3|Bip01_Spine|Bip01_L_Thigh|Bip01_L_Calf.rx"
		;
connectAttr "Bip01_L_Calf_rotateY6.o" "|Stand_Wave_3|Bip01_Spine|Bip01_L_Thigh|Bip01_L_Calf.ry"
		;
connectAttr "Bip01_L_Calf_rotateZ6.o" "|Stand_Wave_3|Bip01_Spine|Bip01_L_Thigh|Bip01_L_Calf.rz"
		;
connectAttr "|Stand_Wave_3|Bip01_Spine|Bip01_L_Thigh.s" "|Stand_Wave_3|Bip01_Spine|Bip01_L_Thigh|Bip01_L_Calf.is"
		;
connectAttr "Bip01_L_Foot_rotateY6.o" "|Stand_Wave_3|Bip01_Spine|Bip01_L_Thigh|Bip01_L_Calf|Bip01_L_Foot.ry"
		;
connectAttr "Bip01_L_Foot_rotateZ6.o" "|Stand_Wave_3|Bip01_Spine|Bip01_L_Thigh|Bip01_L_Calf|Bip01_L_Foot.rz"
		;
connectAttr "Bip01_L_Foot_rotateX6.o" "|Stand_Wave_3|Bip01_Spine|Bip01_L_Thigh|Bip01_L_Calf|Bip01_L_Foot.rx"
		;
connectAttr "|Stand_Wave_3|Bip01_Spine|Bip01_L_Thigh|Bip01_L_Calf.s" "|Stand_Wave_3|Bip01_Spine|Bip01_L_Thigh|Bip01_L_Calf|Bip01_L_Foot.is"
		;
connectAttr "|Stand_Wave_3|Bip01_Spine|Bip01_L_Thigh|Bip01_L_Calf|Bip01_L_Foot.s" "|Stand_Wave_3|Bip01_Spine|Bip01_L_Thigh|Bip01_L_Calf|Bip01_L_Foot|Bip01_L_Toe0.is"
		;
connectAttr "|Stand_Wave_3|Bip01_Spine|Bip01_L_Thigh|Bip01_L_Calf|Bip01_L_Foot|Bip01_L_Toe0.s" "|Stand_Wave_3|Bip01_Spine|Bip01_L_Thigh|Bip01_L_Calf|Bip01_L_Foot|Bip01_L_Toe0|_notused_Bip01_L_Toe0Nub.is"
		;
connectAttr "Bip01_R_Thigh_rotateX6.o" "|Stand_Wave_3|Bip01_Spine|Bip01_R_Thigh.rx"
		;
connectAttr "Bip01_R_Thigh_rotateZ6.o" "|Stand_Wave_3|Bip01_Spine|Bip01_R_Thigh.rz"
		;
connectAttr "Bip01_R_Thigh_rotateY6.o" "|Stand_Wave_3|Bip01_Spine|Bip01_R_Thigh.ry"
		;
connectAttr "|Stand_Wave_3|Bip01_Spine.s" "|Stand_Wave_3|Bip01_Spine|Bip01_R_Thigh.is"
		;
connectAttr "Bip01_R_Calf_rotateX6.o" "|Stand_Wave_3|Bip01_Spine|Bip01_R_Thigh|Bip01_R_Calf.rx"
		;
connectAttr "Bip01_R_Calf_rotateZ6.o" "|Stand_Wave_3|Bip01_Spine|Bip01_R_Thigh|Bip01_R_Calf.rz"
		;
connectAttr "Bip01_R_Calf_rotateY6.o" "|Stand_Wave_3|Bip01_Spine|Bip01_R_Thigh|Bip01_R_Calf.ry"
		;
connectAttr "|Stand_Wave_3|Bip01_Spine|Bip01_R_Thigh.s" "|Stand_Wave_3|Bip01_Spine|Bip01_R_Thigh|Bip01_R_Calf.is"
		;
connectAttr "Bip01_R_Foot_rotateX6.o" "|Stand_Wave_3|Bip01_Spine|Bip01_R_Thigh|Bip01_R_Calf|Bip01_R_Foot.rx"
		;
connectAttr "Bip01_R_Foot_rotateY6.o" "|Stand_Wave_3|Bip01_Spine|Bip01_R_Thigh|Bip01_R_Calf|Bip01_R_Foot.ry"
		;
connectAttr "Bip01_R_Foot_rotateZ6.o" "|Stand_Wave_3|Bip01_Spine|Bip01_R_Thigh|Bip01_R_Calf|Bip01_R_Foot.rz"
		;
connectAttr "|Stand_Wave_3|Bip01_Spine|Bip01_R_Thigh|Bip01_R_Calf.s" "|Stand_Wave_3|Bip01_Spine|Bip01_R_Thigh|Bip01_R_Calf|Bip01_R_Foot.is"
		;
connectAttr "|Stand_Wave_3|Bip01_Spine|Bip01_R_Thigh|Bip01_R_Calf|Bip01_R_Foot.s" "|Stand_Wave_3|Bip01_Spine|Bip01_R_Thigh|Bip01_R_Calf|Bip01_R_Foot|Bip01_R_Toe0.is"
		;
connectAttr "|Stand_Wave_3|Bip01_Spine|Bip01_R_Thigh|Bip01_R_Calf|Bip01_R_Foot|Bip01_R_Toe0.s" "|Stand_Wave_3|Bip01_Spine|Bip01_R_Thigh|Bip01_R_Calf|Bip01_R_Foot|Bip01_R_Toe0|_notused_Bip01_R_Toe0Nub.is"
		;
// End of Animation v5 - Stand Wave 3.ma
