
@script ExecuteInEditMode

@script RequireComponent (Camera)
@script AddComponentMenu ("Image Effects/NFAA")

class NFAA extends PostEffectsBase {
	
	public var edgeDetectHqShader : Shader;
	private var _edgeDetectHqMaterial : Material = null;	
	public var sensitivityDepth : float = 100.0f;


	function CreateMaterials () 
	{
	_edgeDetectHqMaterial = CheckShaderAndCreateMaterial(edgeDetectHqShader,_edgeDetectHqMaterial);	
	}
	
	function Start () 
	{
		CreateMaterials();
		CheckSupport(false);
	}
	


	function OnRenderImage (source : RenderTexture, destination : RenderTexture)
	{	
		CreateMaterials ();

			_edgeDetectHqMaterial.SetFloat("_texWidth", Screen.width);
		_edgeDetectHqMaterial.SetFloat("_texHeight", Screen.height);
		_edgeDetectHqMaterial.SetFloat ("sensitivity", sensitivityDepth);
		Graphics.Blit (source, destination, _edgeDetectHqMaterial);	

		
			}
	}
	


