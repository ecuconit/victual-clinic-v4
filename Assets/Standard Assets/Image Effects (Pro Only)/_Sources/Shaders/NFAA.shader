
Shader "Hidden/NFAA" {
Properties {
	_MainTex ("Base (RGB)", 2D) = "white" {}
		_BlurTex ("Base (RGB)", 2D) = "white" {}

}

SubShader {
	Pass {
		ZTest Always Cull Off ZWrite Off
		Fog { Mode off }

CGPROGRAM



#pragma vertex vert
#pragma fragment frag
#pragma fragmentoption ARB_precision_hint_fastest 

#include "UnityCG.cginc"

uniform sampler2D _MainTex;


sampler2D _CameraDepthNormalsTexture;
uniform float4 _MainTex_TexelSize;
uniform float _debug;
uniform float sensitivity; 
uniform float _texWidth; 
uniform float _texHeight; 


struct v2f {
	float4 pos : POSITION;
	float4 uv1 : TEXCOORD0;
};

v2f vert( appdata_img v )
{
	v2f o;
	o.pos = mul (UNITY_MATRIX_MVP, v.vertex);
	float2 uv = v.texcoord.xy;
	o.uv1.xy = uv;
	
	#if SHADER_API_D3D9
	if (_MainTex_TexelSize.y < 0)
		uv.y = 1-uv.y;
	#endif

	return o;
}



half4 frag (v2f i) : COLOR
{
float2 vPixelViewport = float2( 1.0f / _texWidth, 1.0f / _texHeight );
half4 center = tex2D(_CameraDepthNormalsTexture, i.uv1.xy);
float Depth = DecodeFloatRG (center.zw);
float2 upOffset = float2( 0.0f,vPixelViewport.y)*0.009;
float2 rightOffset = float2(vPixelViewport.x, 0.0f )*0.009;





float topHeight = Luminance( tex2D( _MainTex, i.uv1.xy+upOffset).rgb );
float bottomHeight = Luminance( tex2D( _MainTex, i.uv1.xy-upOffset).rgb );
float rightHeight = Luminance( tex2D( _MainTex, i.uv1.xy+rightOffset).rgb );
float leftHeight = Luminance( tex2D( _MainTex, i.uv1.xy-rightOffset).rgb );
float leftTopHeight =Luminance( tex2D( _MainTex, i.uv1.xy-rightOffset+upOffset).rgb );
float leftBottomHeight = Luminance( tex2D( _MainTex, i.uv1.xy-rightOffset-upOffset).rgb );
float rightBottomHeight = Luminance( tex2D( _MainTex, i.uv1.xy+rightOffset+upOffset).rgb );
float rightTopHeight = Luminance( tex2D(_MainTex, i.uv1.xy+rightOffset-upOffset).rgb );

//float sum0 = rightTopHeight+ topHeight + rightBottomHeight;
//float sum1 = leftTopHeight + bottomHeight + leftBottomHeight;
//float sum2 = leftTopHeight + leftHeight + rightTopHeight;
//float sum3 = leftBottomHeight + rightHeight + rightBottomHeight ;

float sum0 = rightTopHeight+ bottomHeight + leftTopHeight;
float sum1 = leftBottomHeight + topHeight + rightBottomHeight;
float sum2 = leftTopHeight + rightHeight + leftBottomHeight;
float sum3 = rightBottomHeight + leftHeight + rightTopHeight ;


float vec1 = (sum0 - sum1) * sensitivity.x;
float vec2 = (sum3 - sum2) * sensitivity.x;

float2 Normal = float2( vec1, vec2) *vPixelViewport*(2-Depth);
//Normal.xy = clamp(Normal,-float2(1.0,1.0)*0.01,float2(1.0,1.0)*0.01);
float4 Scene0 = tex2D( _MainTex, i.uv1.xy );
float4 Scene1 = tex2D(  _MainTex, i.uv1.xy + Normal.xy );
float4 Scene2 = tex2D(  _MainTex, i.uv1.xy - Normal.xy );
float4 Scene3 = tex2D(  _MainTex, i.uv1.xy + float2(Normal.x, -Normal.y) );
float4 Scene4 = tex2D(  _MainTex, i.uv1.xy - float2(Normal.x, -Normal.y) );


// Final color
float4 o_Color = (Scene0 + Scene1 + Scene2 + Scene3 + Scene4) * 0.2;

// using vec1 and vec2 for the debug output as Normal won't display anything (due to the pixel scale applied to it).
return o_Color;
}
ENDCG
	}
}

Fallback off

}