using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class RailDriver : MonoBehaviour 
{
	#region Public Config Variables
	public Vector2 buttonSize = new Vector2 (150, 22);
	public int border = 7;
	public int headerHeight = 15;
	public float PlayerMovmentSpeed = 10.0f;  // the speed of the player movment
	public float PlayerMovmentStartDelay = 0.5f; //time in seconds from when you call MovePlayerTo() and the player starts moving
	public float PlayerMovmentLookTime = 1.1f; //time in seconds the player takes to face the new path
	public float updateInterval = 0.5f;
	#endregion
	
	#region Private Data Holder Variables
	private List<iTweenPath> pathes = new List<iTweenPath> ();
	private Rect displayWindow = new Rect (20, 20, 120, 50);
	private Rect containerSize = new Rect (0, 0, 0, 0);
	
	// private bool showFPS = true;
	private string fpsDisplay;
	// private Rect bottomRight = new Rect ();
	private float accum = 0.0f; // FPS accumulated over the interval
	private float frames = 0f; // Frames drawn over the interval
	private float timeleft; // Left time for current interval
	#endregion
	
	#region Object Initialization
	/// <summary>
	/// Start this instance
	/// </summary>
	void Start () 
	{
		pathes.AddRange (gameObject.GetComponents <iTweenPath> ());
		// Initialize container size
		displayWindow.width = containerSize.width = border * 2 + buttonSize.x;
		displayWindow.height = containerSize.height = pathes.Count * (border + buttonSize.y) + border + headerHeight;
		// bottomRight = new Rect (Screen.width - buttonSize.x - border, Screen.height - buttonSize.y - border, buttonSize.x, buttonSize.y);
		
		// Initialize FPS timer
		timeleft = updateInterval;  
	}
	#endregion
	
	#region Frame Update
	/// <summary>
	/// On GUI update
	/// </summary>
	void OnGUI ()
	{
		displayWindow = GUI.Window (3, displayWindow, DrawDisplay, fpsDisplay + " : Rails");
		// GUI.Label (bottomRight, fpsDisplay);
	}
	
	/// <summary>
	/// On frame update, calculate FPS
	/// </summary>
	void Update ()
	{
		timeleft -= Time.deltaTime;
		accum += Time.timeScale/Time.deltaTime;
		++frames;
		
		// Interval ended - update GUI text and start new interval
		if( timeleft <= 0.0f)
		{
			// display two fractional digits (f2 format)
			fpsDisplay = (accum/frames).ToString("f2") + " FPS";
			timeleft = updateInterval;
			accum = 0.0f;
			frames = 0f;
		}
	}
	
	/// <summary>
	/// Draws the display window
	/// </summary>
	/// <param name='windowID'>Current window ID</param>
	private void DrawDisplay (int windowID)
	{
		GUI.BeginGroup (containerSize, "");
		int i = headerHeight + border;
		// Draw a button for each button
		foreach (iTweenPath path in pathes)
		{
			if (GUI.Button (new Rect (border, i, buttonSize.x, buttonSize.y), path.pathName))
			{
				Debug.Log (path.pathName);
				iTween.MoveTo(gameObject,iTween.Hash("path", iTweenPath.GetPath(path.pathName),"delay",PlayerMovmentStartDelay,"looktime", PlayerMovmentLookTime,"orienttopath",true,"speed",PlayerMovmentSpeed,"easetype","easeInOutSine","oncomplete","onEnterPreceptorsOffice"));
			}
			i += (int)buttonSize.y + border;
		}
		GUI.EndGroup ();
		GUI.DragWindow();
	}
	#endregion
}