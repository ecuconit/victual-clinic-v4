using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;

public static class Global
{
	#region Global Variables
    public static AnimationManager AnimationManagerRef { get; set; }
	public static string LongString = "A woman who had her phone stolen while vacationing in Ibiza was given an excellent opportunity to name and shame her victim, but then went and kinda spoiled it by being sorta racist.\n" +
		"\n" + "The unnamed victim says she \"hopelessly drunk\" and skinny dipping with some friends when someone strolled up to their deck chairs and stole all their stuff.\n" +
		"\n" + "\"Everything was gone. Money, purses, smartphones, passports, birthday presents and clothes,\" she writes.\n" +
		"\n" + "After returning home to Germany, the woman noticed that, though her iPhone had been stolen, someone was still uploading photos to her Dropbox camera upload folder: The thief.";
	#endregion

	#region Global Data Holder
	private static int maxLineCount = 500;
	private static List<DisplayText> debugMessages = new List<DisplayText> ();	
	#endregion
	
	#region Public Getter for Global Data Holder	
	/// <summary>
	/// Gets or sets the debug messages
	/// </summary>
	/// <value>A list of debug strings</value>
	public static List<DisplayText> DebugMessages
	{
		get { return debugMessages; }
	}
	#endregion

	#region Public Static Debugger Method
	/// <summary>
	/// Global dump method, for testing and logging purpose
	/// </summary>	
	/// <param name="callerObject">Which generic object is calling the dump method <see cref="Object"/></param>	
	public static void ToDebug (string message)
	{
		// Call worker method to add to display
		WriteToDebug (message);
	}
	
	/// <summary>
	/// Global dump method, for testing and logging purpose
	/// </summary>	
	/// <param name="callerObject">Which generic object is calling the dump method <see cref="Object"/></param>	
	/// <param name="message">What message to display <see cref="System.String"/></param>
	public static void ToDebug (object callerObject, string message)
	{
		// Call worker method to add to display
		WriteToDebug (string.Format ("[{0}] - {1}", callerObject.ToString (), message));
	}
	
	/// <summary>
	/// Global dump method, for testing and logging purpose
	/// </summary>	
	/// <param name="callerObject">Which generic object is calling the dump method <see cref="Object"/></param>	
	/// <param name="message">What message to display <see cref="System.String"/></param>
	/// <param name="messageType">What type of message it is</param>
	public static void ToDebug (object callerObject, string message, LogMessageType messageType)
	{
		// Call worker method to add to display
		WriteToDebug (string.Format ("[{0}] - {1}", callerObject.ToString (), message), messageType);
	}
	
	/// <summary>
	/// The actual writting to message method
	/// </summary>
	/// <param name="message">What message to display <see cref="System.String"/></param>
	private static void WriteToDebug (string message)
	{
		// Convert and display, using default message type
		WriteToDebug (message, LogMessageType.DisplayMessage);
	}
	
	/// <summary>
	/// The actual writting to message method
	/// </summary>
	/// <param name="message">What message to display <see cref="System.String"/></param>
	/// <param name="messageType">What type of message it is</param>
	private static void WriteToDebug (string message, LogMessageType messageType)
	{
		// Convert and display, using long display
		Debug.Log (string.Format ("[{0}] [{1}]: {2}", messageType, DateTime.Now.ToString ("u"), message));
		
		// Add to buffer, using short hand
		debugMessages.Add (new DisplayText (string.Format (" >> [{0}]: {1}", DateTime.Now.ToString ("G"), message), messageType));		
		
		// Trim from beginning if necessary
		if (debugMessages.Count >= maxLineCount)
			debugMessages.RemoveRange (0, debugMessages.Count - maxLineCount);
	}
	
	/// <summary>
	/// Inject the specified message into debug messages
	/// </summary>
	/// <param name='message'>What message to inject</param>
	public static void Inject (DisplayText message)
	{
		debugMessages.Add (message);
	}
	#endregion
}

/// <summary>
/// What type of message it is to log
/// </summary>
public enum LogMessageType
{
	TestMessage,
	DisplayMessage,
	StateChangeMessage,
	VerificationError,
	ValidationError,	
	Exception
}