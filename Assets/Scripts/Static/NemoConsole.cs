using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using System.IO;
using System.Linq;
#if UNITY_EDITOR
using UnityEditor;
#endif
// #if UNITY_STANDALONE_WIN
// using System.Runtime.InteropServices; // For windows form, need System.Windows.Forms.dll from C:\Program Files (x86)\Unity\Editor\Data\Mono\lib\mono\2.0
// #endif

public class NemoConsole : MonoBehaviour
{
	#region Public Configuration Variable
	public GUISkin skin;
	public GUISkin dialogSkin;
	public float fpsUpdate = 0.5f; // how long it takes to update
	
	public int displayLayer = -1;
	
	public LogMessageType debugMessageType = LogMessageType.TestMessage;
	public Color defaultColor = Color.white;
	public Color testMessage = Color.blue;
	public Color displayMessage = Color.white;
	public Color stateChangeMessage = Color.green;
	public Color verificationError = Color.red;
	public Color validationError = Color.magenta;
	public Color exception = Color.yellow;
	#endregion

	#region Private Data Holder Variables
	private bool initialized = false; // Whether the object has been initialzied or not
	private bool show = false; // Whether to display GUI or not
	private bool resize = false;

	private Environment environment = Environment.UnityEditor; // Release environment

	private int frameCount = 0;
	private float accu = 0.0f;
	private float nextUpdate = 0.0f;
	private float fps = 0.0f;
	private string fpsDisplay = "0";

	private string inputString = "";
	private List<string> previousInputs = new List<string>();
	private bool returnKey = false;
	private bool escapeKey = false;
	private bool backKey = false;
	private Rect containerSize;
	private Vector2 scrollPosition = Vector2.zero;
	private Vector2 screenSize = new Vector2 ();

	private List<GameObject> objects = new List<GameObject>();
	private List<GameObject> currentObject = new List<GameObject>();
	private List<Component> components = new List<Component>();
	private List<Component> currentComponent = new List<Component>();

	private Dictionary<string, DisplayText[]> helpMenu = new Dictionary<string, DisplayText[]>();
	
	private string defaultPath;
	// private bool haltForBrowser = false;
	// private FileBrowser browser;
	
	// [DllImport("user32.dll")]
	// private static extern void SaveFileDialog(); //in your case : OpenFileDialog
	#endregion

	#region Object Initializer
	/// <summary>
	/// Awake this instance
	/// </summary>
	void Awake()
	{
		// Assume true until proven false
		bool verified = true;

		// Verify components
		if (skin == null)
		{
			Global.ToDebug(this, "[Debug Console] needs a GUI skin", LogMessageType.VerificationError);
			verified = false;
		}
		
		// Initialize screen size buffer
		screenSize.x = Screen.width;
		screenSize.y = Screen.height;
		
		// Check for platform
		if ((Application.platform == RuntimePlatform.WindowsEditor) || (Application.platform == RuntimePlatform.WindowsEditor))
			environment = Environment.UnityEditor;			
		else if ((Application.platform == RuntimePlatform.WindowsWebPlayer) || (Application.platform == RuntimePlatform.OSXWebPlayer))
			environment = Environment.WebPlayer;
		else if ((Application.platform == RuntimePlatform.WindowsPlayer) || (Application.platform == RuntimePlatform.OSXPlayer) || (Application.platform == RuntimePlatform.OSXDashboardPlayer))
			environment = Environment.StandAlone;
		else if ((Application.platform == RuntimePlatform.XBOX360) || (Application.platform == RuntimePlatform.PS3) ||
			(Application.platform == RuntimePlatform.IPhonePlayer) || (Application.platform == RuntimePlatform.Android))
			environment = Environment.Mobile;
		else if ((Application.platform == RuntimePlatform.NaCl) || (Application.platform == RuntimePlatform.FlashPlayer))
			environment = Environment.Native;
		
		// Initialize file dialog
		if ((environment == Environment.UnityEditor) || (environment == Environment.StandAlone))
		{			
			// Generate default save path
			defaultPath = Application.dataPath + "/ScreenCap";
			
			// Construct and initialize file browser object
			// browser = new FileBrowser (dialogSkin, new Vector2 (400, 300), ".png", displayLayer);
			// browser.Initialization (defaultPath, false);
		}

		// Build help menu
		helpMenu = BuildHelpMenu();
		 		
		// Initialization
		Initialization(skin);
		
		initialized = verified;
	}

	/// <summary>
	/// Start this instance
	/// </summary>
	void Start()
	{		
	}

	/// <summary>
	/// Initialization the GUI with specified style
	/// </summary>
	/// <param name='guiSkinStyle'>What GUI style to use</param>
	public void Initialization(GUISkin guiSkinStyle)
	{
		// Buffer style
		skin = guiSkinStyle;

		// Calculate necessary components
		containerSize = new Rect(skin.box.margin.left, skin.box.margin.top, Screen.width - skin.box.margin.left - skin.box.margin.right, Screen.height - skin.box.margin.top - skin.box.margin.bottom);
	}
	#endregion

	#region GUI Update
	/// <summary>
	/// Draw the GUI display
	/// </summary>
	void OnGUI()
	{
		// Simple verification
		if ((!initialized) || (!show))
			return;
		
		// Check for resize signal
		if (resize)			
			Initialization (skin);
		
		// Toggles GUI availablilty 
		// GUI.enabled = !haltForBrowser;
		
		// Start a container at top level
		GUI.depth = displayLayer;
		GUILayout.BeginArea(containerSize, skin.box);
		GUILayout.BeginVertical();
		{
			#region Top Row
			// Top row
			GUILayout.BeginHorizontal();
			{
				// Draw label
				GUILayout.Label("Debug Window - FPS " + fpsDisplay, skin.GetStyle("Header"));

				// Draw close button
				GUI.SetNextControlName("buttonClose");
				if (GUILayout.Button("", skin.GetStyle("CloseButton")))
				{
					// Toggle flag and immediate return
					show = false;
					ToggleControls (show);
					return;
				}
			}
			GUILayout.EndHorizontal();
			#endregion

			#region Middle Row
			// Middle row
			{
				// Begin scroll region for text display to take up rest of space
				GUI.SetNextControlName("scrollviewDebugMessages");
				scrollPosition = GUILayout.BeginScrollView(scrollPosition, skin.scrollView, GUILayout.MaxWidth(Screen.width), GUILayout.MaxHeight(Screen.height));

				// Draw a label for each text
				for (int i = Global.DebugMessages.Count - 1; i >= 0; --i)
				{
					// Set color based on message type
					GUI.contentColor = GetColor(Global.DebugMessages[i].Type);
					GUILayout.Label(Global.DebugMessages[i].Text, skin.label);
				}

				// Set back to default color
				GUI.contentColor = defaultColor;

				GUILayout.EndScrollView();
			}
			#endregion

			#region Bottom Row
			// Bottom row
			GUILayout.BeginHorizontal();
			{
				// Print button
				GUI.SetNextControlName("buttonPrint");
				if (GUILayout.Button("PRT", skin.button))
				{
					Global.Inject(new DisplayText ("Screen capping the screen", debugMessageType));
					if ((environment == Environment.UnityEditor) || (environment == Environment.StandAlone))
					{
						// Enable prompt
						// haltForBrowser = true;						
					}
					else
						// Going directly to screen cap save
						StartCoroutine(ScreenAndSave()); // ^o^
				}

				// Pause button
				GUI.SetNextControlName("buttonPause");
				if (GUILayout.Button("||", skin.button))
				{
					// Toggle resource manager status
					ToggleControls (null);
				}

				// Check for return key before textfield eat the key event
				returnKey = (Event.current.Equals(Event.KeyboardEvent("return")));
				escapeKey = (Event.current.Equals(Event.KeyboardEvent("escape")));
				backKey = (Event.current.Equals(Event.KeyboardEvent("page up")));

				// Check for command roll back
				if ((backKey) && (GUI.GetNameOfFocusedControl() == "textCommandInput"))
				{
					// Double check for existence of previous command
					if (previousInputs.Count > 0)
					{
						// Pop from the bottom
						inputString = previousInputs[previousInputs.Count - 1];
						previousInputs.RemoveAt(previousInputs.Count - 1);
					}
					else
						inputString = "";
				}

				// Get and display input
				GUI.SetNextControlName("textCommandInput");
				inputString = GUILayout.TextField(inputString, skin.textField);

				// Execute button or return key
				GUI.SetNextControlName("buttonExecute");
				if ((GUILayout.Button("EXE", skin.button)) || ((returnKey) && (GUI.GetNameOfFocusedControl() == "textCommandInput")))
				{
					Global.Inject (new DisplayText ("Executing command...", debugMessageType));
					StartCoroutine(ParseAndExecute());
				}

				// Clean button or escape key
				GUI.SetNextControlName("buttonClean");
				if ((GUILayout.Button("CLS", skin.button)) || ((escapeKey) && (GUI.GetNameOfFocusedControl() == "textCommandInput")))
				{
					Global.DebugMessages.Clear();
					inputString = "";
					Global.Inject(new DisplayText ("Clearing buffered messages", debugMessageType));
				}
			}
			GUILayout.EndHorizontal();
			#endregion
		}
		GUILayout.EndVertical();
		GUILayout.EndArea();

		// If there isn't a currently selected control, focus it on text input
		if (GUI.GetNameOfFocusedControl() == "")
			GUI.FocusControl("textCommandInput");
		
		// Cancel the signal
		resize = false;
		
		// Display pop-up prompt if necessary
		// if (haltForBrowser)
		// 	// Draw the file prompt
		// 	haltForBrowser = !browser.Draw (resize);
	}

	/// <summary>
	/// Frame update handler
	/// </summary>
	void Update()
	{
		// Simple verification
		if (!initialized)
			return;

		// Check for inputs
		if (Input.GetKey(KeyCode.LeftShift) && Input.GetKeyUp(KeyCode.BackQuote))
		{
			// Toggle GUI show
			show = !show;

			// Toggle resource manager, automatically pause game
			ToggleControls (show);
		}

		// If the data is not being shown, don't do the rest
		if (!show)
			return;

		// Calculate FPS
		accu += Time.deltaTime;
		++frameCount;
		if (accu > nextUpdate)
		{
			// Update next update
			nextUpdate = fpsUpdate;
			fps = frameCount / accu;
			fpsDisplay = fps.ToString("f2");

			// Reset update
			accu = 0;
			frameCount = 0;
			
			// Take this chance to detect screen resize, instead of doing this every single frame
			if ((screenSize.x != Screen.width) || (screenSize.y != Screen.height))
			{
				// Screen resized, initialize
				screenSize.x = Screen.width;
				screenSize.y = Screen.height;
				
				// Re-initialize style
				resize = true;
			}
		}
	}
	#endregion

	#region Helper Method
	/// <summary>
	/// Toggles the buffered controls, generally used to pause resource manager
	/// </summary>
	/// <param name='active'>Set flag to active flag; null to just toggle</param>
	private void ToggleControls (bool? active)
	{
		/*
		if (active == null)
			Global.RMReference.Paused = !Global.RMReference.Paused;
		else
			Global.RMReference.Paused = (active == true);		
		Global.Inject(new DisplayText ("[Resource Manager] is now: " + (Global.RMReference.Paused ? "Paused" : "Unpaused"), debugMessageType));
		*/
	}
	
	/// <summary>
	/// Gets a specified text color
	/// </summary>
	/// <returns>The color to use for message type</returns>
	/// <param name='type'>What type of message it is</param>
	private Color GetColor(LogMessageType type)
	{
		switch (type)
		{
			case LogMessageType.TestMessage:
				return testMessage;
			case LogMessageType.DisplayMessage:
				return displayMessage;
			case LogMessageType.StateChangeMessage:
				return stateChangeMessage;
			case LogMessageType.VerificationError:
				return verificationError;
			case LogMessageType.ValidationError:
				return validationError;
			case LogMessageType.Exception:
				return exception;
			default:
				return defaultColor;
		}
	}

	/// <summary>
	/// Construct the help menu
	/// </summary>
	/// <returns>Menu string</returns>
	private Dictionary<string, DisplayText[]> BuildHelpMenu()
	{
		// Create the menu in reverse order, so that it would be displayed in correct order
		Dictionary<string, DisplayText[]> result = new Dictionary<string, DisplayText[]>();
		DisplayText[]

		temp = new DisplayText[] {
		new DisplayText (
			"/component, /c := Manipulate components in buffered game objects\n" +
			"/c -?          Display help menu for component command\n" +
			"/c -l          List all components in the buffer\n" +
			"/c -a          Use all components in buffer\n" +
			"/c -g [i]      Get the i-th components in buffer\n" +
			"/c -s [s]      Send message to buffered components\n" +
			"/c -s [s] [o]  Send message to buffered components by inject [o]\n" +
			"/c -s [s] [o..]Send message to buffered components by inject list of [o]\n" +
			"/c -b [s]      Broadcast message to buffered components\n" +
			"/c -b [s] [o]  Broadcast message to buffered components by inject [o]\n" +
			"/c -f [s]      Get the value of a field [s] of the buffered components\n" +
			"/c -f [s] [o]  Set the value [o] to a field [s] of buffered components\n" +
			"/c -p [s]      Get the value of property [s] of the buffered components\n" +
			"/c -p [s] [o]  Set the value [o] to property [s] of buffered components (WIP)",
		debugMessageType)};
		result.Add("component", temp);

		temp = new DisplayText[] {
		new DisplayText (
			"/object, /o    := Manipulate game objects in buffered list\n" +
			"/o -?          Display help menu for object command\n" +
			"/o -l          List all game objects in the buffer\n" +
			"/o -a          Use all game objects in buffer\n" +
			"/o -g [i]      Get the i-th game object in buffer\n" +
			"/o -c [s]      Get component named [s] in current game object", 
		debugMessageType)};
		result.Add("object", temp);

		temp = new DisplayText[] {
		new DisplayText (
			"/find, /f      := Find game objects in current scene\n" +
			"/f [s1].[s2]   Find the first component [s2] of first game object [s1]\n" +
			"                  Such as: MainCamera.GameController\n" +
			"/f -?          Display help menu for find command\n" +
			"/f -i [s]      Find all game objects with ID of [s]\n" +
			"/f -t [s]      Find all game objects with tag of [s]", 
		debugMessageType)};
		result.Add("find", temp);

		temp = new DisplayText[] {
		new DisplayText (
			"/echo, /e      := Echoes the message back to console\n" +
			"/e [s]         Writes the string [s] back to console", 
		debugMessageType)};
		result.Add("echo", temp);

		temp = new DisplayText[] {
		new DisplayText (
			"/help, /h, /?  := Display ExecuteHelp message\n" +
			"/h             Display help menu for all commands\n" +
			"/h [s]         Display help menu for a specific command [s]", 
		debugMessageType)};
		result.Add("help", temp);

		temp = new DisplayText[] {
		new DisplayText (
			"Help menu, helps you to navitage this debugger console\n" +
			" - Things in [UPPER CASE] are the physical key you press\n" +
			"    - [ESC]          Escape key to clear screen\n" +
			"    - [RETURN]       Return key to enter a command\n" + 
			"    - [PAGE UP]      Page up key to use previous command\n" + 
			" - [lower case], are for command\n" + 
			"    - pause, p       Pause/Resume the resource manager\n" +
			"    - cls, c         Same as escape key, to clear screen\n" +
			" - Unless they are [braces], which they are data\n" + 
			"    - [i] = integer, specifically integer, can't be a float\n" +
			"    - [o] = object (any datatype, includes Single family), delimited by space\n" +
			"    - [o..] = multiple objects of any datatype\n" +
			"    - [s] = string, if contains space, use quote delimited enclosure: \"abc def\"\n" +
			"       - \"([i] ..)\" = for a number (float) set, such as Vector2, Vector3, and Quaternion\n" +
			"       - \"<[o] ..>\" = for a list, a list of typed object, including list and array\n" +
			"          - For complex list, you can nest like: \"[\"([i] ..)\" ..]\" = list of sets\n" +
			"       - \"[[o] ..]\" = for an array, an array of typed object, including list and array\n" +
			"          - For complex array, you can nest like: \"[\"<[o] ..>\" ..]\" = array or lists\n" +
			"          - ex. \"< \"(  1.2 2.3 3.4  )\" \"( 4.5 5.6 6.7 )\" \"( 7.8 8.9 9.0 )\" >\" = array of Vector3\n" +
			" - Method ~= function & Property ~= getter/setter & Field ~= variable",
		debugMessageType)};
		result.Add("instr", temp);

		return result;
	}

	/// <summary>
	/// Resets the command buffer objects
	/// </summary>
	private void ResetBuffers()
	{
		objects.Clear();
		currentObject.Clear();
		components.Clear();
		currentComponent.Clear();
	}	

	#region File Browser
	
	#endregion
	#endregion

	#region Execute Command Method
	#region Execute Command Shell
	/// <summary>
	/// Parses the input text and execute the command if possible
	/// </summary>
	/// <returns>Enumerator flag</returns>
	private IEnumerator ParseAndExecute()
	{
		// First return
		yield return new WaitForEndOfFrame();

		// Check for simple case
		string cleaned = inputString.Trim();
		if (cleaned == string.Empty)
		{
			Global.ToDebug(this, "No input given.", LogMessageType.VerificationError);
			return true;
		}

		// Parse through input
		List<string> parsed = TextParser.SplitString(cleaned);

		// Verify for root command
		if (parsed.Count < 1)
			Global.ToDebug(this, "No command given.", LogMessageType.VerificationError);

		// Check for root command
		bool returnFlag = false;
		switch (parsed[0])
		{
			case "c":
			case "cls":
				// Other command, clean display
				Global.DebugMessages.Clear();
				inputString = "";
				Global.Inject(new DisplayText ("Clearing buffered messages", debugMessageType));
				break;
			case "p":
			case "pause":
				// Other command, toggle the pause
				ToggleControls (null);
				break;
			case "/e":
			case "/echo":
				// Echo the rest
				parsed.RemoveAt(0);
				returnFlag = ExecuteEcho(parsed);
				break;
			case "/f":
			case "/find":
				// Find game object(s)
				parsed.RemoveAt(0);
				returnFlag = ExecuteFind(parsed);
				break;
			case "/o":
			case "/object":
				// Manipulate game object(s)
				parsed.RemoveAt(0);
				returnFlag = ExecuteObject(parsed);
				break;
			case "/c":
			case "/component":
				// Manipulate game component from script
				parsed.RemoveAt(0);
				returnFlag = ExecuteComponent(parsed);
				break;
			case "/?":
			case "/h":
			case "/help":
				// Help menu
				parsed.RemoveAt(0);
				returnFlag = ExecuteHelp(parsed);
				break;
			default:
				Global.ToDebug(this, "Command [" + parsed[0] + "] is not a recognized command.", LogMessageType.VerificationError);
				break;
		}

		// Check to see whether the return flag indicates command was executed successfully
		if (returnFlag)
		{
			// Legit command, push it to buffer
			previousInputs.Add(inputString);
		}
	}
	#endregion

	#region Echo Command
	/// <summary>
	/// Executes the echo command
	/// </summary>
	/// <returns>Whether the command was executed successfully or not</returns>
	/// <param name='remainder'>Rest of the command line</param>
	private bool ExecuteEcho(List<string> remainder)
	{
		// Simple verification
		if ((remainder == null) || (remainder.Count <= 0))
		{
			Global.ToDebug(this, "Nothing to echo, please input .", LogMessageType.VerificationError);
			return false;
		}

		// Echo the remainder
		string result = string.Empty;
		foreach (string s in remainder)
			result += s + " ";
		Global.Inject(new DisplayText ("Echoing: \"" + result + "\"", debugMessageType));

		return true;
	}
	#endregion

	#region Help Command
	/// <summary>
	/// Executes the help menu command
	/// </summary>
	/// <returns>Whether the command was executed successfully or not</returns>
	/// <param name='remainder'>Rest of the command line</param>
	private bool ExecuteHelp(List<string> remainder)
	{
		// Take care of easy case, write everything
		if ((remainder != null) && (remainder.Count > 0))
		{
			// Has something, trying to find out what it is
			switch (remainder[0])
			{
				case "/e":
				case "/echo":
					// Print echo menu
					foreach (DisplayText menu in helpMenu["echo"])
						Global.Inject(menu);
					return true;
				case "/f":
				case "/find":
					// Print find menu
					foreach (DisplayText menu in helpMenu["find"])
						Global.Inject(menu);
					return true;
				case "/o":
				case "/object":
					// Print object menu
					foreach (DisplayText menu in helpMenu["object"])
						Global.Inject(menu);
					return true;
				case "/c":
				case "/component":
					// Print component menu
					foreach (DisplayText menu in helpMenu["component"])
						Global.Inject(menu);
					return true;
				case "/h":
				case "/help":
					// Print help menu
					foreach (DisplayText menu in helpMenu["help"])
						Global.Inject(menu);
					return true;
				default:
					// Fall to default
					Global.Inject(new DisplayText ("Cannot determine the input flag \"" + remainder[0] + "\", printing all menu items.", debugMessageType));
					break;
			}
		}

		// Simple case, all else
		foreach (string key in helpMenu.Keys)
			foreach (DisplayText menu in helpMenu[key])
				Global.Inject(menu);
		return true;
	}
	#endregion

	#region Find Command
	/// <summary>
	/// Executes the find object command
	/// </summary>
	/// <returns>Whether the command was executed successfully or not</returns>
	/// <param name='remainder'>Rest of the command line</param>
	private bool ExecuteFind(List<string> remainder)
	{
		// Simple verification
		if ((remainder == null) || (remainder.Count < 1))
		{
			Global.ToDebug(this, "Unknown input paramesters, please use input flag \"-?\" or \"/help\" command for help", LogMessageType.VerificationError);
			return false;
		}
		
		// Check for simple case
		if (remainder.Count == 1)
		{
			string[] splitCommand = remainder[0].Split ('.');
			if (splitCommand.Length > 1)
			{
				// Check for [GameObject].[Component]
				GameObject gameID = GameObject.Find (splitCommand[0]);
				if (gameID != null)
				{
					// Found a gameobject, attempt to get component
					Component gameComponent = gameID.GetComponent (splitCommand[1]);
					
					// Buffer the object and command
					ResetBuffers ();
					objects.Add (gameID);
					currentObject.Add (gameID);
					components.Add (gameComponent);
					currentComponent.Add (gameComponent);
					
					// Done, inject message
					Global.Inject (new DisplayText (gameComponent.ToString () + " was found and add to buffer.", debugMessageType));
					return true;
				}
			}
		}

		// Has enough command item, trying to find out what to do
		string result = string.Empty;
		switch (remainder[0])
		{
			case "-i":
				// Verify parameter
				if ((remainder[1] == string.Empty) || (remainder.Count < 2))
				{
					Global.ToDebug(this, "Find gameobject by ID needs an ID parameter.", LogMessageType.VerificationError);
					return false;
				}

				// Find gameobject by id
				ResetBuffers();

				// Find all of them
				string toSearch = remainder[1].ToUpper();
				foreach (GameObject x in GameObject.FindObjectsOfType(typeof(GameObject)) as GameObject[])
					if (x.name.ToUpper() == toSearch)
						objects.Add(x);
				currentObject.AddRange(objects);
				result = "Found " + objects.Count + " game objects by ID.";
				break;
			case "-t":
				// Verify parameter
				if ((remainder[1] == string.Empty) || (remainder.Count < 2))
				{
					Global.ToDebug(this, "Find gameobject by tag needs a tag parameter.", LogMessageType.VerificationError);
					return false;
				}

				// Find gameobject by tag
				List<GameObject> temp = new List<GameObject> ();
				try
				{
					// Attempt to fetch by tag
					temp.AddRange (GameObject.FindGameObjectsWithTag(remainder[1]));
					if (temp == null)
					{
						Global.ToDebug (this, "No game object found with the given tag.", LogMessageType.VerificationError);
						return false;
					}
				}
				catch (Exception error)
				{
					Global.ToDebug (this, "Error finding game objects by tag.  Message: " + error.Message, LogMessageType.Exception);
					return false;
				}
			
				// Verified, used it
				ResetBuffers();
				objects.AddRange(temp);
				currentObject.AddRange(objects);
				result = "Found " + objects.Count + " game objects by tag.";
				break;
			case "-?":
			case "/?":
				// Display
				foreach (DisplayText menu in helpMenu["find"])
					Global.Inject(menu);
				return true;
			default:
				// Unknown flag, treat it as shorthand
				Global.ToDebug(this, "Unknown input flag, please use input flag \"-?\" or \"/help\" command for help.", LogMessageType.VerificationError);
				return false;
		}

		// Inject result
		Global.Inject(new DisplayText(result, debugMessageType));
		return true;
	}
	#endregion

	#region Object Command
	/// <summary>
	/// Executes the object manipulation command
	/// </summary>
	/// <returns>Whether the command was executed successfully or not</returns>
	/// <param name='remainder'>Rest of the command line</param>
	private bool ExecuteObject(List<string> remainder)
	{
		// Simple verification
		if ((remainder == null) || (remainder.Count < 1))
		{
			Global.ToDebug(this, "Unknown input paramesters, please use input flag \"-?\" or \"/help\" command for help", LogMessageType.VerificationError);
			return false;
		}
		
		// Has enough command item, trying to find out what to do
		string result = string.Empty;
		switch (remainder[0])
		{
			case "-l":
				// List out all the game objects and current
				result = "There are " + objects.Count + " object(s) in the buffer:\n";
				if (objects.Count > 0)
				{
					for (int i = 0; i < objects.Count; ++i)
						result += "   " + i.ToString ("D3") + " : [" + objects[i].name + "]\n";
				}
				result += "\nCurrently using " + currentObject.Count + " object(s) in the buffer:\n";
				if (currentObject.Count > 0)
				{
					for (int i = 0; i < currentObject.Count; ++i)
						result += "   " + i.ToString("D3") + " : [" + currentObject[i].name + "]\n";
				}
				break;
			case "-a":
				// Use all of the buffered objects
				currentObject.Clear();
				currentObject.AddRange(objects);
				result = "Using all " + currentObject.Count + " game object(s).";
				break;
			case "-g":
				// Get i-th item out of the buffer
				if (remainder.Count < 2)
				{
					Global.ToDebug (this, "Not enough parameter to complete the command, please use input flag \"-?\" or \"/help\" command for help", LogMessageType.VerificationError);
					return false;
				}
			
				int index = -1;
				if (int.TryParse(remainder[1], out index))
				{
					// Succeess, check for out of bound
					if ((index < 0) || (index >= objects.Count))
					{
						Global.ToDebug(this, "Index provided is out of bound.  Can't obtain " + index + " out of " + objects.Count + " item(s).", LogMessageType.VerificationError);
						return false;
					}
					else
					{
						// In bound, use it
						currentObject.Clear();
						currentObject.Add(objects[index]);
						result = "Now using " + index + "-th object: " + currentObject[0].name;
					}
				}
				else
				{
					// Fail to parse string
					Global.ToDebug(this, "Failed to determine line index, needed an integer and was given \"" + remainder[1] + "\".", LogMessageType.VerificationError);
					return false;
				}
				break;
			case "-?":
			case "/?":
				// Display
				foreach (DisplayText menu in helpMenu["object"])
					Global.Inject(menu);
				return true;
			case "-c":
				// Get component from curent objects
				if (currentObject.Count < 1)
				{
					Global.ToDebug(this, "There isn't a game object in current buffer, please use \"/f\" to find objects and \"/o\" to indicate which object to use.", LogMessageType.VerificationError);
					return false;
				}
				if (remainder.Count < 2)
				{
					Global.ToDebug (this, "Not enough parameter to complete the command, please use input flag \"-?\" or \"/help\" command for help", LogMessageType.VerificationError);
					return false;
				}
			

				// Find all components of the given mae from buffered object
				components.Clear();
				foreach (GameObject x in currentObject)
				{
					// Fetch the component, add to the list if found
					Component c = (Component)x.GetComponent(remainder[1]);
					if (c != null)
						components.Add(c);
				}
				// Add to current component buffer
				currentComponent.Clear();
				currentComponent.AddRange(components);
				result = "Found " + components.Count + " components in " + currentObject.Count + " game object(s).";
				break;
			default:
				// Unknown flag
				Global.ToDebug(this, "Unknown input flag, please use input flag \"-?\" or \"/help\" command for help.", LogMessageType.VerificationError);
				return false;
		}

		// Inject result
		Global.Inject(new DisplayText(result, debugMessageType));
		return true;
	}
	#endregion

	#region Component Command
	#region Component Command Shell
	/// <summary>
	/// Executes the component manipulation command
	/// </summary>
	/// <returns>Whether the command was executed successfully or not</returns>
	/// <param name='remainder'>Rest of the command line</param>
	private bool ExecuteComponent(List<string> remainder)
	{
		// Simple verification 
		if ((remainder == null) || (remainder.Count < 1))
		{
			Global.ToDebug(this, "Unknown input paramesters, please use input flag \"-?\" or \"/help\" command for help", LogMessageType.VerificationError);
			return false;
		}

		// Has enough command item, trying to find out what to do
		string result = string.Empty;
		List<object> converted;
		switch (remainder[0])
		{
			case "-l":
				// List out all the components in buffer and current
				result = "There are " + components.Count + " components(s) in the buffer:\n";
				if (components.Count > 0)
				{
					for (int i = 0; i < components.Count; ++i)
						result += "   " + i.ToString("D3") + " : [" + components[i].ToString () + "]\n";
				}
				result += "\nCurrently using " + currentComponent.Count + " components(s) in the buffer:\n";
				if (currentComponent.Count > 0)
				{
					for (int i = 0; i < currentComponent.Count; ++i)
						result += "   " + i.ToString("D3") + " : [" + currentComponent[i].ToString() + "]\n";
				}
				Global.Inject(new DisplayText(result, debugMessageType));
				break;
			case "-a":
				// Use all of the buffered components
				currentComponent.Clear();
				currentComponent.AddRange(components);
				result = "Using all " + currentComponent.Count + " component(s).";
				break;
			case "-g":
				// Get i-th item out of the buffer
				if (remainder.Count < 2)
				{
					Global.ToDebug (this, "Not enough parameter to complete the command, please use input flag \"-?\" or \"/help\" command for help", LogMessageType.VerificationError);
					return false;
				}
			
				int index = -1;
				if (int.TryParse(remainder[1], out index))
				{
					// Succeess, check for out of bound
					if ((index < 0) || (index >= components.Count))
					{
						Global.ToDebug(this, "Index provided is out of bound.  Can't obtain " + index + " out of " + components.Count + " item(s).", LogMessageType.VerificationError);
						return false;
					}
					else
					{
						// In bound, use it
						currentComponent.Clear();
						currentComponent.Add(components[index]);
						result = "Now using " + index + "-th component: " + currentComponent[0].ToString ();
					}
				}
				else
				{
					// Fail to parse string
					Global.ToDebug(this, "Failed to determine line index, needed an integer and was given \"" + remainder[1] + "\".", LogMessageType.VerificationError);
					return false;
				}
				break;
			case "-b":				
			case "-s":
				// Differentiate type, treat them as the same
				if (remainder.Count < 2)
				{
					Global.ToDebug (this, "Not enough parameter to complete the command, please use input flag \"-?\" or \"/help\" command for help", LogMessageType.VerificationError);
					return false;
				}
				converted = TextParser.ConvertStringsToSymbols (remainder.GetRange (2, remainder.Count - 2));
				CallMethod (remainder[0] == "-b", remainder[1], converted);
				break;
			case "-p":
				// Calling for property 
				if (remainder.Count < 2)
				{
					Global.ToDebug (this, "Not enough parameter to complete the command, please use input flag \"-?\" or \"/help\" command for help", LogMessageType.VerificationError);
					return false;
				}
				converted = TextParser.ConvertStringsToSymbols (remainder.GetRange (2, remainder.Count - 2));
				CallProperty (remainder[1], converted);
				break;
			case "-f":
				// Calling for field
				if (remainder.Count < 2)
				{
					Global.ToDebug (this, "Not enough parameter to complete the command, please use input flag \"-?\" or \"/help\" command for help", LogMessageType.VerificationError);
					return false;
				}
				converted = TextParser.ConvertStringsToSymbols (remainder.GetRange (2, remainder.Count - 2));
				CallField (remainder[1], converted);
				break;
			case "-?":
			case "/?":
				// Display
				foreach (DisplayText menu in helpMenu["component"])
					Global.Inject(menu);
				return true;
			default:
				// Unknown flag
				Global.ToDebug(this, "Unknown input flag, please use input flag \"-?\" or \"/help\" command for help.", LogMessageType.VerificationError);
				return false;

		}
		return true;
	}
	#endregion
	
	#region Sending Message
	/// <summary>
	/// Sends the message to 
	/// </summary>
	/// <returns>Whether the command is executed correctly or not</returns>
	/// <param name='cascade'>Whether to cascade the message to child objects or not</param>
	/// <param name='parameters'>Parameters to be sent to the method call</param>
	private bool CallMethod(bool cascade, string methodName, List<object> parameters)
	{
		// Simple verification
		if ((currentObject.Count < 1) || (currentComponent.Count < 1) || (methodName == string.Empty))
		{
			Global.Inject(new DisplayText ("No object in buffer or no component in buffer, please see /help menu for more detail.", debugMessageType));
			return false;
		}
		
		try
		{
			// Check for lack of parameter
			if ((parameters == null) || (parameters.Count < 1))
				// No input parameter
				return CallMethod (cascade, methodName);
			else if (parameters.Count == 1)
				// One input parameter
				return CallMethod (cascade, methodName, parameters[0]);
			
			// All if not 0 or 1 parameter, send them all
			foreach (Component component in currentComponent)
			{
				try
				{
					// Reflect the object and get the method
					Type type = Type.GetType (component.GetType ().ToString(), true);
					// MethodBase method = type.GetMethod(methodName, BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.IgnoreCase);
					// Could use BindingFlags.GetField & BindingFlags.GetProperty
					
					// Method check
					MethodInfo[] methods = type.GetMethods (BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.IgnoreCase);
					if (methods.Length <= 0)
					{
						Global.Inject(new DisplayText ("Method invocation failed, no method found.", debugMessageType));
						return true;
					}
					// Verify method overload	
					ParameterInfo[] methodParams = null;
					bool currentFail = false, first = true;
					string methodDump = string.Empty;
					foreach (MethodInfo method in methods)
					{
						// Simple case, name match
						if (method.Name != methodName)
							continue;
						
						// Parameter check
						currentFail = false;
						methodParams = method.GetParameters ();
						if (methodParams.Length > parameters.Count)
						{
							// Insufficient parameters
							// Global.Inject(new DisplayText ("Method invocation has less than needed number of paramters.  Expecting: " + methodParams.Length, debugMessageType));
							// return true;
							currentFail = true;
						}
						else
						{
							// Carefully check for every parameter type
							for (int i= 0; i < methodParams.Length; ++i)
							{
								if (methodParams[i].ParameterType != parameters[i].GetType ())
								{
									// Method mismatch, wrong type
									// Global.Inject(new DisplayText ("Type mismatch, parameter #" + i + " (" + methodParams[i].Name + ")is expecting expecting [" +
									// 	methodParams[i].ParameterType.ToString () + "] but [" + parameters[i].GetType().ToString () + 
									// 	"] was given.", debugMessageType));
									// return true;
									currentFail = true;
									break;
								}
							}
						}
						
						// Current method hit
						if (!currentFail)
						{
							// Current method matches, invoke the reflected method in current object
							object result = method.Invoke ((object)component, parameters.ToArray ());
							if (result != null)
								Global.Inject(new DisplayText ("Method invocation was completed successfully.  Object returned: " + result.ToString (), debugMessageType));
							else
								Global.Inject(new DisplayText ("Method invocation was completed successfully with no return variable.", debugMessageType));
							return true;
						}
						else
						{
							// Not the match, dump method message
							methodDump += " -> " + method.Name + "(";
							first = true;
							foreach (ParameterInfo methodParam in methodParams)
							{
								methodDump += (first ? "" : ", ") + methodParam.ParameterType.ToString () + " " + methodParam.Name;
								first = false;
							}
							// Trim the last comma;
							methodDump += ")\n";
						}
					}
					
					// Gotten this far still haven't found a matching method, dump error message
					methodDump += "User input:\n";
					methodDump += " -> " + methodName;
					first = true;
					foreach (object x in parameters) 
					{
						methodDump += (first ? "(" : ", ") + x.GetType ().ToString () + " " + (x.GetType() == typeof (string) ? "\"" + x.ToString () + "\"" :  x.ToString ());
						first = false;
					}
					methodDump += ")";
					Global.Inject(new DisplayText ("Method not found.  Perhaps parameter mismatch.\n" + methodDump, debugMessageType));
					
					/*
					// Get the constructor and create an instance of MagicClass
			        Type magicType = Type.GetType(component);
			        ConstructorInfo magicConstructor = magicType.GetConstructor(Type.EmptyTypes);
			        object magicClassObject = magicConstructor.Invoke(new object[]{});
			
			        // Get the ItsMagic method and invoke with a parameter value of 100
			
			        MethodInfo magicMethod = magicType.GetMethod("ItsMagic");
			        object magicValue = magicMethod.Invoke(magicClassObject, new object[]{100});
					*/
				}
				catch (Exception error)
				{
					// Failure to invoke the method
					Global.ToDebug (this, "Method invoke error, fail to call [" + methodName + "] in [" + component.ToString () + "].  Error message: " + error.Message, LogMessageType.Exception);
					return false;
				}
			}
		
		}
		catch (Exception error)
		{
			Global.ToDebug(this, "Error calling method in components.  Message: " + error.Message, LogMessageType.Exception);
			return false;
		}

		return true;
	}
	
	/// <summary>
	/// Sends the message to 
	/// </summary>
	/// <returns>Whether the command is executed correctly or not</returns>
	/// <param name='cascade'>Whether to cascade the message to child objects or not</param>
	private bool CallMethod (bool cascade, string methodName)
	{
		// Assume verified
		/*
		// Simple verification
		if ((currentObject.Count < 1) || (currentComponent.Count < 1) || (methodName == string.Empty))
		{
			Global.Inject(new DisplayText ("No object in buffer or no component in buffer, please see /help menu for more detail.", debugMessageType));
			return false;
		}
		*/
			
		try
		{
			// Appy to all current game objects
			if (cascade)
				foreach (Component x in currentComponent)
				{
					Global.Inject(new DisplayText ("Broadcasting message to [" + x.ToString() + "] for no input parameter.", debugMessageType));
					x.BroadcastMessage(methodName, SendMessageOptions.DontRequireReceiver);
				}
			else
				foreach (Component x in currentComponent)
				{
					Global.Inject(new DisplayText ("Sending message to [" + x.ToString() + "] for no input parameter.", debugMessageType));
					x.SendMessage(methodName, SendMessageOptions.RequireReceiver);
				}
		}
		catch (Exception error)
		{
			Global.ToDebug (this, "Error calling method in components.  Message: " + error.Message, LogMessageType.Exception);
			return false;
		}
		
		return true;
	}
	
	/// <summary>
	/// Sends the message to 
	/// </summary>
	/// <returns>Whether the command is executed correctly or not</returns>
	/// <param name='cascade'>Whether to cascade the message to child objects or not</param>
	/// <param name='methodName'>Which method to call</param>
	/// <param name='parameters'>Generic input parameter</param>
	private bool CallMethod (bool cascade, string methodName, object parameter)
	{
		// Assume verified
		/*
		// Simple verification
		if ((currentObject.Count < 1) || (currentComponent.Count < 1) || (methodName == string.Empty))
		{
			Global.Inject(new DisplayText ("No object in buffer or no component in buffer, please see /help menu for more detail.", debugMessageType));
			return false;
		}
		*/
			
		try
		{
			// Appy to all current game objects
			if (cascade)
				foreach (Component x in currentComponent)
				{
					Global.Inject(new DisplayText ("Broadcasting message to [" + x.ToString() + "] for no input parameter.", debugMessageType));
					x.BroadcastMessage(methodName, parameter, SendMessageOptions.DontRequireReceiver);
				}
			else
				foreach (Component x in currentComponent)
				{
					Global.Inject(new DisplayText ("Sending message to [" + x.ToString() + "] for no input parameter.", debugMessageType));
					x.SendMessage(methodName, parameter, SendMessageOptions.RequireReceiver);
				}
		}
		catch (Exception error)
		{
			Global.ToDebug (this, "Error calling method in components.  Message: " + error.Message, LogMessageType.Exception);
			return false;
		}
		
		return true;
	}
	#endregion
	
	#region Call Property
	/// <summary>
	/// Gets/Sends the data of property 
	/// </summary>
	/// <returns>Whether the command is executed correctly or not</returns>
	/// <param name='propertyName'>Which property to call</param>
	/// <param name='parameters'>Parameters to be sent to the method call</param>
	private bool CallProperty(string propertyName, List<object> parameters)	
	{
		// Simple verification
		if ((currentObject.Count < 1) || (currentComponent.Count < 1) || (propertyName == string.Empty))
		{
			Global.Inject(new DisplayText ("No object in buffer or no component in buffer, please see /help menu for more detail.", debugMessageType));
			return false;
		}
		
		try
		{
			// Check for call type
			bool setValue = (parameters.Count > 0);
								
			// Go through each buffered component
			foreach (Component component in currentComponent)
			{
				try
				{
					// Reflect the object and get the method
					PropertyInfo hit = null;
					#region property check
					Type type = Type.GetType (component.GetType ().ToString(), true);
					// hit = type.Getproperty (propertyName);
					// property check
					PropertyInfo[] properties = type.GetProperties (BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.IgnoreCase);
					if (properties.Length <= 0)
					{
						Global.Inject(new DisplayText ("property invocation failed, no properties found.", debugMessageType));
						return true;
					}
					
					// Look for property
					string propertyDump = string.Empty;
					bool first = true;
					foreach (PropertyInfo property in properties)
					{
						// Simple case, name match
						if (!property.Name.Equals (propertyName))
						{
							// Name check failed, throw to dhump
							propertyDump += " -> " + property.Name + " (" + property.GetType ().ToString () + ")\n";
							continue;
						}
						
						// Hit, continue
						hit = property;
						break;
					}
					#endregion
					
					// Gotten this far still haven't found a matching property, dump error message
					if (hit == null)
					{
						propertyDump += "User input:\n";
						propertyDump += " -> " + propertyName;
						if (parameters.Count > 0)
						{
							first = true;
							foreach (object x in parameters) 
							{
								propertyDump += (first ? "(" : ", ") + x.GetType ().ToString () + " " + (x.GetType() == typeof (string) ? "\"" + x.ToString () + "\"" :  x.ToString ());
								first = false;
							}
							propertyDump += ")";
						}
						Global.Inject(new DisplayText ("property not found.  Perhaps type mismatch.\n" + propertyDump, debugMessageType));
						// Global.Inject(new DisplayText ("property not found.  Perhaps type mismatch.", debugMessageType));
						continue;
					}
					
					#region Check for execution type 
					// Send the first item to property, shift responsibility to user
					if (setValue)
					{
						// Use the first parameter
						if (hit.PropertyType == parameters[0].GetType())
						{
							hit.SetValue ((object) component, parameters[0], null);
							Global.Inject(new DisplayText ("property invoke completed, .", debugMessageType));
						}
						else
							Global.ToDebug(this, "Parameter mismatch on first item.  Expected [" + hit.PropertyType + "] but provided parameter was [" + parameters[0].GetType() + "]", LogMessageType.VerificationError);
					}
					#endregion
					
					// Throw back the value
					object result = hit.GetValue ((object) component, null);
					if (result != null)
					{
						string resultValues = "";
						first = true;
						if (result is IList)
						{
							foreach (object item in (IList) result)
							{
								resultValues += (first ? "" : ", ") + item.ToString ();
								first = false;
							}
						}
						else if (result is Array)
						{
							foreach (object item in (Array) result)
							{
								resultValues += (first ? "" : ", ") + item.ToString ();
								first = false;
							}
						}
						else
							resultValues = result.ToString ();
						
						// Unified result injection value
						Global.Inject(new DisplayText (component.ToString () + "." + propertyName + ": " + resultValues, debugMessageType));
					}
					else
						Global.ToDebug (this, "property call failed.", LogMessageType.VerificationError);
				}
				catch (Exception error)
				{
					// Failure to invoke the method
					Global.ToDebug (this, "property invoke error, fail to call [" + propertyName + "] in [" + component.ToString () + "].  Error message: " + error.Message, LogMessageType.Exception);
					return false;
				}
			}	
		}
		catch (Exception error)
		{
			Global.ToDebug(this, "Error calling property in components.  Message: " + error.Message, LogMessageType.Exception);
			return false;
		}

		return true;
	}
	#endregion
	
	#region Call Field
	/// <summary>
	/// Gets/Sends the data of field
	/// </summary>
	/// <returns>Whether the command is executed correctly or not</returns>
	/// <param name='fieldName'>Which field to call</param>
	/// <param name='parameters'>Parameters to be sent to the method call</param>
	private bool CallField(string fieldName, List<object> parameters)
	{
		// Simple verification
		if ((currentObject.Count < 1) || (currentComponent.Count < 1) || (fieldName == string.Empty))
		{
			Global.Inject(new DisplayText ("No object in buffer or no component in buffer, please see /help menu for more detail.", debugMessageType));
			return false;
		}
		
		try
		{
			// Check for call type
			bool setValue = (parameters.Count > 0);
			/*
			object list = null;
			// Don't need to parse if there isn't any variable
			if (setValue)
				list = TextParser.ConvertToList (parameters);
			*/
			
			// Type checkType = null;
			// Using a slow and stupid way to create buffer list
			/*
			Dictionary<Type, object> buffer = new Dictionary<Type, object> ();
			buffer.Add (typeof (string), new List<string> ());
			buffer.Add (typeof (int), new List<int> ());
			buffer.Add (typeof (float), new List<float> ());
			buffer.Add (typeof (double), new List<double> ());
			buffer.Add (typeof (DateTime), new List<DateTime> ());
			buffer.Add (typeof (Vector2), new List<Vector2> ());
			buffer.Add (typeof (Vector3), new List<Vector3> ());
			buffer.Add (typeof (Quaternion), new List<Quaternion> ());
			*/
			
			/*
			if ((parameters != null) && (parameters.Count > 0))
			{
				// Set to set mode
				getParameter = false;
				
				// Check for array/list type
				if (parameters.Count > 1)
				{
					// Check to see if the input may be a list
					checkType = parameters[0].GetType ();
					same = true; // Assume to be the same until prove false
					foreach (object parameter in parameters)
					{	
						// Different type
						if (parameter.GetType() != checkType)
						{
							same = false;
							break;
						}
						
						// Same type, add to list
						#region Add to buffer
						if (checkType == typeof (string))
						{
							(buffer[checkType] as List<string>).Add ((string) parameter);
						} 
						else if (checkType == typeof (int))
						{
							(buffer[checkType] as List<int>).Add ((int) parameter);
						} 
						else if (checkType == typeof (float))
						{
							(buffer[checkType] as List<float>).Add ((float) parameter);
						} 
						else if (checkType == typeof (double))
						{
							(buffer[checkType] as List<double>).Add ((double) parameter);
						} 
						else if (checkType == typeof (DateTime))
						{
							(buffer[checkType] as List<DateTime>).Add ((DateTime) parameter);
						} 
						else if (checkType == typeof (Vector2))
						{
							(buffer[checkType] as List<Vector2>).Add ((Vector2) parameter);
						} 
						else if (checkType == typeof (Vector3))
						{
							(buffer[checkType] as List<Vector3>).Add ((Vector3) parameter);
						} 
						else if (checkType == typeof (Quaternion))
						{
							(buffer[checkType] as List<Quaternion>).Add ((Quaternion) parameter);
						}
						else
							// Unknown type, throw to generic list
							(buffer[checkType] as List<object>).Add (parameter);
						#endregion 
					}
					
					// All items in array are of the same type
					if (same)
					{
						// Check for number type, force to be Single if that's the case
						if ((checkType == typeof (float)) || (checkType == typeof (double)))
							checkType = typeof (System.Single);
						
						// Convert into an array
						tempArray = System.Array.CreateInstance (checkType, parameters.Count);
						System.Array.Copy (parameters.ToArray (), tempArray, parameters.Count);
					}
				}
			}	
			*/
					
			// Go through each buffered component
			foreach (Component component in currentComponent)
			{
				try
				{
					// Reflect the object and get the method
					FieldInfo hit = null;
					#region field check
					Type type = Type.GetType (component.GetType ().ToString(), true);
					// hit = type.GetField (fieldName);
					// field check
					FieldInfo[] properties = type.GetFields (BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.IgnoreCase);
					if (properties.Length <= 0)
					{
						Global.Inject(new DisplayText ("field invocation failed, no properties found.", debugMessageType));
						return true;
					}
					
					// Look for field
					string fieldDump = string.Empty;
					bool first = true;
					foreach (FieldInfo field in properties)
					{
						// Simple case, name match
						if (!field.Name.Equals (fieldName))
						{
							// Name check failed, throw to dhump
							fieldDump += " -> " + field.Name + " (" + field.GetType ().ToString () + ")\n";
							continue;
						}
						
						// Hit, continue
						hit = field;
						break;
					}
					#endregion
					
					// Gotten this far still haven't found a matching field, dump error message
					if (hit == null)
					{
						fieldDump += "User input:\n";
						fieldDump += " -> " + fieldName;
						if (parameters.Count > 0)
						{
							first = true;
							foreach (object x in parameters) 
							{
								fieldDump += (first ? "(" : ", ") + x.GetType ().ToString () + " " + (x.GetType() == typeof (string) ? "\"" + x.ToString () + "\"" :  x.ToString ());
								first = false;
							}
							fieldDump += ")";
						}
						Global.Inject(new DisplayText ("field not found.  Perhaps type mismatch.\n" + fieldDump, debugMessageType));
						// Global.Inject(new DisplayText ("field not found.  Perhaps type mismatch.", debugMessageType));
						continue;
					}
					
					#region Check for execution type 
					/*
					// If list exist, assume it's setter type					
					if (list != null)
					{
						try
						{
							// Try to use list type
							hit.SetValue ((object) component, list);
						}
						catch
						{
							// Failed, try array
							// Type checkType = parameters[0].GetType (); 
							// if ((checkType == typeof (float)) || (checkType == typeof (double)))
							// 	checkType = typeof (System.Single);
							// System.Array tempArray = System.Array.CreateInstance (checkType, parameters.Count);
							// System.Array.Copy (parameters.ToArray (), tempArray, parameters.Count);
							// hit.SetValue ((object) component, tempArray);
							// hit.SetValue ((object) component, TextParser.ConvertToArray (parameters));
						}
					}
					*/
					// Send the first item to field, shift responsibility to user
					if (setValue)
					{
						// Use the first parameter
						if (hit.FieldType == parameters[0].GetType())
						{
							hit.SetValue ((object) component, parameters[0]);
							Global.Inject(new DisplayText ("field invoke completed, .", debugMessageType));
						}
						else
							Global.ToDebug(this, "Parameter mismatch on first item.  Expected [" + hit.FieldType + "] but provided parameter was [" + parameters[0].GetType() + "]", LogMessageType.VerificationError);
					}
					#endregion
					
					// Throw back the value
					object result = hit.GetValue ((object) component);
					if (result != null)
					{
						string resultValues = "";
						first = true;
						if (result is Array)
						{
							foreach (object item in (Array) result)
							{
								resultValues += (first ? "\"[" : " ") + item.ToString ();
								first = false;
							}
							resultValues += "]\"";
						}
						else if (result is IList)
						{
							foreach (object item in (IList) result)
							{
								resultValues += (first ? "\"<" : " ") + item.ToString ();
								first = false;
							}
							resultValues += ">\"";
						}
						else
							resultValues = result.ToString ();
						
						// Unified result injection value
						Global.Inject(new DisplayText (component.ToString () + "." + fieldName + ": " + resultValues, debugMessageType));
					}
					else
						Global.ToDebug (this, "Field call failed.", LogMessageType.VerificationError);
				}
				catch (Exception error)
				{
					// Failure to invoke the method
					Global.ToDebug (this, "field invoke error, fail to call [" + fieldName + "] in [" + component.ToString () + "].  Error message: " + error.Message, LogMessageType.Exception);
					return false;
				}
			}	
		}
		catch (Exception error)
		{
			Global.ToDebug(this, "Error calling field in components.  Message: " + error.Message, LogMessageType.Exception);
			return false;
		}

		return true;
	}
	
	public static List<T> CreateList<T> (T t)
	{
		return new List<T>();
	}
	#endregion
	#endregion
	#endregion

	#region Screen Capture Method
	/// <summary>
	/// Capture the screen and prompt save
	/// </summary>
	/// <returns>Enumerator flag</returns>
	private IEnumerator ScreenAndSave()
	{
		// First return, wait for graphics to render ^o^
		yield return new WaitForEndOfFrame();
		string fileName = "ScreenCap." + DateTime.Now.ToString ("yyyy.MM.dd-HH.mm.ss");
		string fileExtension = "png";

		// Create a texture to pass to encoding
        Texture2D texture = new Texture2D(Screen.width, Screen.height, TextureFormat.RGB24, false);
 
        // Put buffer into texture
        texture.ReadPixels(new Rect(0, 0, Screen.width, Screen.height), 0, 0);
        texture.Apply();
 
        // Split the process up--ReadPixels() and the GetPixels() call inside of the encoder are both pretty heavy
        yield return 0;
 
		// Prompt instruction
		switch (environment)
		{
		case Environment.StandAlone:
			// Prompt
			/*
			System.Windows.Forms.SaveFileDialog save = new System.Windows.Forms.SaveFileDialog ();
			save.Filter = "Image File | *.png | All Files | *.*";
   			save.DefaultExt = "png";
			
			// Save the image
			if (save.ShowDialog () == DialogResult.OK)
			{
				try
				{
					// Create a writer, so the script is webplayer friendly
					BinaryWriter writer = new BinaryWriter (File.OpenWrite (save.FileName));
					
					// Write data
					writer.Write (texture.EncodeToPNG());
					writer.Flush ();
					writer.Close ();
				}
				catch (Exception error)
				{
					Global.ToDebug (this, "Error occurred while writing screen cap file to: " + path + ".\r\nError Message: " + error.Message + ".\r\nTrace: " + error.StackTrace, LogMessageType.Exception);
				}
			}
			*/
			try
			{
				// Create the folder if not already exist
				if (Directory.Exists (defaultPath))
					Directory.CreateDirectory (defaultPath);
				
				// Create a writer, so the script is webplayer friendly
				BinaryWriter writer = new BinaryWriter (File.OpenWrite (defaultPath + "/" + fileName + "." + fileExtension));
				
				// Write data
				writer.Write (texture.EncodeToPNG());
				writer.Flush ();
				writer.Close ();
				
				// write confirmation
				Global.Inject(new DisplayText ("Screen capping completed, file saved to: " + defaultPath + "/" + fileName + "." + fileExtension, debugMessageType));
			}
			catch (Exception error)
			{
				Global.ToDebug (this, "Error occurred while writing screen cap file to: " + defaultPath + "/" + fileName + "." + fileExtension + 
					".\r\nError Message: " + error.Message + 
					".\r\nTrace: " + error.StackTrace, LogMessageType.Exception);
			}
			break;
		case Environment.WebPlayer:
			// Convert data to string
			string data = System.Convert.ToBase64String(texture.EncodeToPNG());
			
			// Write to document
			// Application.ExternalEval("document.location.href='data:octet-stream;base64," + data + "'");
			Application.ExternalEval("document.location.href='data:octet-stream;base64," + data + "'");
			break;
		case Environment.UnityEditor:
			#if UNITY_EDITOR
			// Display instruction message
	        if (EditorUtility.DisplayDialog ("Save Screen Cap as PNG", "Select where to save the screen capture file.", "Okay", "Cancel"))
			{
				// Create the folder if not already exist
				if (Directory.Exists (defaultPath))
					Directory.CreateDirectory (defaultPath);
				
				// Get file save path
				string path = EditorUtility.SaveFilePanel ("Save Screen Cap as PNG", defaultPath, fileName, fileExtension);
				
				// Selection verification
				if (path.Length != 0)
				{
					// Save the image
					try
					{
						// Simple case, doesn't work with webplayer, can't even compile
						// File.WriteAllBytes(path, texture.EncodeToPNG());
						
						// Create a writer, so the script is webplayer friendly
						BinaryWriter writer = new BinaryWriter (File.OpenWrite (path));
						
						// Write data
						writer.Write (texture.EncodeToPNG());
						writer.Flush ();
						writer.Close ();
						
						// write confirmation
						Global.Inject(new DisplayText ("Screen capping completed, file saved to: " + path, debugMessageType));
					}
					catch (Exception error)
					{
						Global.ToDebug (this, "Error occurred while writing screen cap file to: " + path + ".\r\nError Message: " + error.Message + ".\r\nTrace: " + error.StackTrace, LogMessageType.Exception);
					}
				}
				else
					Global.Inject(new DisplayText ("Screen Cap cancelled.", debugMessageType));
			}
			else
				Global.Inject(new DisplayText ("Screen Cap cancelled.", debugMessageType));
			#else
				Global.ToDebug (this, "System is flagged as [Unity Editor] but compiler doesn't think so.  Current platform: " + Application.platform.ToString (), debugMessageType);
			#endif
			break;
		default:
			Global.Inject(new DisplayText ("Screen capture method is not currently being supported on your platform.  Current platform: " + Application.platform.ToString (), debugMessageType));
			break;
		}
        // Added by Karl. - Tell unity to delete the texture, by default it seems to keep hold of it and memory crashes will occur after too many screenshots.
        DestroyObject( texture );
 		return true;
	}

	/// <summary>
	/// Capture the screen
	/// </summary>
	/// <returns>Screen texture</returns>
	/// <param name='srcCamera'>Which camera to use</param>
	/// <param name='width'>Background width</param>
	/// <param name='height'>Background height</param>
	private static Texture2D ScreenShoot(Camera srcCamera, int width, int height)
	{
		// Local buffer
		RenderTexture renderTexture = new RenderTexture(width, height, 0);
		Texture2D targetTexture = new Texture2D(width, height, TextureFormat.RGB24, false);

		// Perform screen capture
		srcCamera.targetTexture = renderTexture;
		srcCamera.Render();

		// Adds texture to top
		RenderTexture.active = renderTexture;
		targetTexture.ReadPixels(new Rect(0, 0, width, height), 0, 0);
		targetTexture.Apply();
		srcCamera.targetTexture = null;
		RenderTexture.active = null;

		// Reset the camera and return
		srcCamera.ResetAspect();
		return targetTexture;
	}

	/// <summary>
	/// Lerps the texture
	/// </summary>
	/// <param name='alphaTexture'>Texture with alpha channel</param>
	/// <param name='texture'>Texture to be overlayed with alpha texture</param>
	private static void LerpTexture(Texture2D alphaTexture, ref Texture2D texture)
	{
		// Buffer the two color channels
		Color[] bgColors = alphaTexture.GetPixels();
		Color[] tarCols = texture.GetPixels();

		// Overlay the two images based on alpha channel
		for (var i = 0; i < tarCols.Length; i++)
			tarCols[i] = (bgColors[i].a > 0.99f ? bgColors[i] : Color.Lerp(tarCols[i], bgColors[i], bgColors[i].a));

		// Overwrite the target
		texture.SetPixels(tarCols);

		// Reapply the texture by reference
		texture.Apply();
	}
	#endregion
}

#region Helper Object/Enum
/// <summary>
/// Buffer object for debug text display
/// </summary>
public class DisplayText
{
	#region Public
	public string Text { get; set; }
	public LogMessageType Type { get; set; }
	#endregion

	#region Object Initialization
	/// <summary>
	/// Initializes a new instance of the <see cref="DisplayText"/> class.
	/// </summary>
	public DisplayText()
	{ }

	/// <summary>
	/// Initializes a new instance of the <see cref="DisplayText"/> class.
	/// </summary>
	/// <param name='text'>Text to display</param>
	public DisplayText(string text)
	{
		Text = text;
		Type = LogMessageType.DisplayMessage;
	}

	/// <summary>
	/// Initializes a new instance of the <see cref="DisplayText"/> class.
	/// </summary>
	/// <param name='text'>Text to display</param>
	/// <param name='type'>Type of message</param>
	public DisplayText(string text, LogMessageType type)
	{
		Text = text;
		Type = type;
	}
	#endregion
}

/// <summary>
/// Release environment
/// </summary>
public enum Environment
{
	UnityEditor,
	Mobile,
	WebPlayer,
	StandAlone,
	Native
}
#endregion