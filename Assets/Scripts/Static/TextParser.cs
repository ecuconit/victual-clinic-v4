using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;

public static class TextParser
{
	#region Private Data Holder
	private static bool hyphenates = true;
	private static int lineSize = 50;
	#endregion
	
	#region Public Static methods
	#region Cut String Methods
	/// <summary>
	/// Cut a string into several smaller strings by calling worker method
	/// </summary>
	/// <returns>A string containing series of substrings whose length is less than lineSize and separated by '\n'</returns>
	/// <param name='stringtoCut'>The string to be cut</param>
	/// <param name='maxLineSize'>Overload the maximum line size</param>
	public static string CutStringToSize(string stringtoCut, int maxLineSize)
	{
		// Buffer configuration
		int tempLineSize = lineSize;
		lineSize = maxLineSize;
		
		// Cut the line
		string result = CutStringToSize(stringtoCut);
		
		// Restore configuration
		lineSize = tempLineSize;
		
		return result;
	}
	
	/// <summary>
	/// Cut a string into several smaller strings by calling worker method
	/// </summary>
	/// <returns>A string containing series of substrings whose length is less than lineSize and separated by '\n'</returns>
	/// <param name='stringtoCut'>The string to be cut</param>
	/// <param name='toHyphenate'>Overload whether to hyphenate or not</param>
	public static string CutStringToSize(string stringtoCut, bool toHyphenate)
	{
		// Buffer configuration
		bool tempHyphenates = hyphenates;
		hyphenates = toHyphenate;
		
		// Cut the line
		string result = CutStringToSize(stringtoCut);
		
		// Restore configuration
		hyphenates = tempHyphenates;
		
		return result;
	}
	
	/// <summary>
	/// Cut a string into several smaller strings by calling worker method
	/// </summary>
	/// <returns>A string containing series of substrings whose length is less than lineSize and separated by '\n'</returns>
	/// <param name='stringtoCut'>The string to be cut</param>
	/// <param name='maxLineSize'>Overload the maximum line size</param>
	/// <param name='toHyphenate'>Overload whether to hyphenate or not</param>
	public static string CutStringToSize(string stringtoCut, int maxLineSize, bool toHyphenate)
	{
		// Buffer configuration
		int tempLineSize = lineSize;
		bool tempHyphenates = hyphenates;
		lineSize = maxLineSize;
		hyphenates = toHyphenate;
		
		// Cut the line
		string result = CutStringToSize(stringtoCut);
		
		// Restore configuration
		lineSize = tempLineSize;
		hyphenates = tempHyphenates;
		
		return result;
	}
	
	/// <summary>
	/// Cut a string into several smaller strings recursively 
	/// </summary>
	/// <returns>A string containing series of substrings whose length is less than lineSize and separated by '\n'</returns>
	/// <param name='stringtoCut'>The string to be cut</param>
	public static string CutStringToSize(string stringtoCut)
	{
		// Check for simple case
		if(stringtoCut.Length > lineSize)
		{
			int lineBreak;
			string firstline, rest;
			bool hyphenated = false;
			
			// DebugConsole.Log ("Trying to parse \"" + stringtoCut + "\".");
			// lineBreak = stringtoCut.LastIndexOf(' ', lineSize - lineSize/4,  lineSize/4);
			lineBreak = stringtoCut.IndexOf ('\n', 0, lineSize);
			if (lineBreak < 0)
				lineBreak = stringtoCut.LastIndexOf(' ', lineSize, lineSize / 4);
			
			// Double check to make sure line is long enough
			if (lineBreak < 0)
			{
				// If hyphantes, remove additional character
				if (hyphenates)
				{
					lineBreak = lineSize - 1;
					hyphenated = true;
				}
				else
				{
					lineBreak = lineSize;
				}
			}
			
			firstline = stringtoCut.Substring(0,lineBreak) + ( hyphenated ? "-" : "" );
			rest = stringtoCut.Substring(lineBreak).Trim ();
			
			return firstline + "\n" + CutStringToSize(rest);
		}
		else
			return stringtoCut;
	}
	#endregion
	
	#region String Split Methods
	/// <summary>
	/// Counts the quote level, left to increase and right to decrease
	/// </summary>
	/// <returns>Number of unclosed quotes</returns>
	/// <param name='toCount'>What string to count</param>
	private static int CountQuote (string toCount)
	{
		int quoteCount = 0;
		// char nextChar;
		
		// First, count forward
		for (int i = 0; i < toCount.Length; ++i)
		{
			// Check for easy case first, ignore spaces
			if (toCount[i] == ' ')
				continue;
			
			// Found a quote, enough space afterward
			if ((toCount[i] == '\"') && (i < toCount.Length - 1))
			{
				// Increment
				++quoteCount;
				switch (toCount[i + 1])
				{
				case '(':
				case '[':
				case '<':
					++i; // Compound object found, skip next character
					break;
				case ' ':
					// Space, let the loop do the trick
					break;
				default:
					// Something else, treat it as a string quote, ignore everything afterward, end the loop
					i = toCount.Length;
					continue;
				}
				continue;
			}
			
			// Bail out on the first sign of trouble
			break;
		}
		
		// Second, count backward
		for (int i = toCount.Length - 1; i >= 0; --i)
		{
			// Check for easy case first, ignore spaces
			if (toCount[i] == ' ')
				continue;
			
			// Found a quote, enough space afterward
			if ((toCount[i] == '\"') && (i > 0))
			{
				// Increment
				--quoteCount;
				switch (toCount[i - 1])
				{
				case ')':
				case ']':
				case '>':
					--i; // Compound object found, skip next character
					break;
				case ' ':
					// Space, let the loop do the trick
					break;
				default:
					// Something else, treat it as a string quote, ignore everything afterward, end the loop
					i = -1;
					continue;
				}
				continue;
			}
			
			// Bail out on the first sign of troublebreak;
			break;
		}
		
		return quoteCount;
	}
		
	/// <summary>
	/// Splits the string into list of strings
	/// </summary>
	/// <returns>A list of substring separated by space, empty if nothing is entered</returns>
	/// <param name='stringtoSplit'>What string to separate</param>
	public static List<string> SplitString (string stringtoSplit)
	{	
		// Create result list
		// List<string> temp = new List<string> ();
		List<string> result = new List<string> ();
		string cleaned = string.Empty;
		// string buffer = string.Empty;
		int quoteLevel = 0, lastQuoteLevel;
		
		// Check for simple case first
		if ((stringtoSplit == null) || (stringtoSplit.Equals (string.Empty)))
			return result;
		foreach (string segment in stringtoSplit.Split (' '))
		{
			// Clean the segment
			cleaned = segment.Trim ();
			
			// Simple check
			if (cleaned.Equals (string.Empty))
				continue;
			
			// Update quote levels
			lastQuoteLevel = quoteLevel;
			quoteLevel += CountQuote (cleaned);
			
			// Check for unlikely case
			if (quoteLevel < 0)
				quoteLevel = 0;
			
			// Check for quote
			if ((lastQuoteLevel == 0) && (quoteLevel > 0))
			{
				// Start of quote
				result.Add (cleaned.Substring (1, cleaned.Length - 1));
			}			
			// Check for
			else if ((lastQuoteLevel > 0) && (quoteLevel == 0))
			{
				// Closing quote, concatnate the last item
				result[result.Count - 1] += (" " + cleaned.Substring (0, cleaned.Length - 1));
			}
			else if ((lastQuoteLevel > 0) && (quoteLevel > 0))
			{
				// Still in quote, add to the last item
				result[result.Count - 1] += (" " + cleaned);
			}
			else
			{
				// Not in quote, just add a new item
				result.Add (cleaned);
			}
		}
		
		return result;
		
		/*
		// Simple split
		temp.AddRange (cleaned.Split (' '));
		
		// Parse
		foreach (string s in temp)
		{
			buffer = s.Trim ();
			
			// Simple check
			if (buffer.Equals (string.Empty))
			{
				// Empty string, do nothing
				continue;
			}
			
			
			// Check for start quote
			else if ((!inQuote) && (s[0] == '\"'))
			{
				// Line start and end with quote
				if (s[s.Length - 1] == '\"')
				{
					// Allow even the empty string
					result.Add (s.Substring (1, s.Length - 2));
					
					continue;
				}
				
				// Start collection
				buffer = s;
				inQuote = true;
			}
			// Check for end quote			
			else if ((inQuote) && (s[s.Length - 1] == '\"'))
			{
				// Add the last string and remove the quotes
				buffer += " " + s;
				inQuote = false;
				result.Add (buffer.Substring (1, buffer.Length - 2));
			}			
			else if (inQuote)
			{
				// Still in quotes, buffer everything
				buffer += " " + s;
			}
			else 
			{
				// Other string, just use it
				result.Add (s);
			}
		}
		
		return result;
		*/
	}
	
	/*
	public static List<string> SplitString(string aString)
	{
		List<string> aStringList; 
		int index2 = aString.LastIndexOf('"');  //index of last char '"'  
		int index1 = aString.IndexOf('"');	  //index of first char '"'  

		// Handle the situation 1: no char '"' in aString or only one char '"' 
		// just split the aString between blank char ' '
		if (index2 == index1)
		{
			string[] stringArray = aString.Split(' ');

			aStringList = new List<string>(stringArray);			   
		}

	   	// Handle the situation 2: more than one char '"' in aString
		// First split aString into 3 substring by the first and last char '"'
		// split the substring_1 and substring_3 by char ' '
		else
		{
			string substring_1 = aString.Substring(0, index1);
			string substring_2 = aString.Substring(index1 + 1, index2 -index1 -1);
			string substring_3 = aString.Substring(index2 + 1);

			string[] stringArray1 = substring_1.Split(' ');
			string[] stringArray3 = substring_3.Split(' ');

			aStringList = new List<string>(stringArray1);
			aStringList.Add(substring_2);
			aStringList.AddRange(stringArray3);			  
		}

		//Remove some blank string
		while (aStringList.Contains("") == true)
		{
			aStringList.Remove("");
		} 

		return aStringList;
	}
	*/
	
	/// <summary>
	/// Splits the string to list of objects
	/// </summary>
	/// <returns>An array of objects representing float, integer, boolean, datetime or string
	/// </returns>
	/// <param name='stringtoSplit'>What string to separate</param>
	public static List<object> ConvertStringsToSymbols (string[] stringstoParse)
	{
		return ConvertStringsToSymbols (new List<string> (stringstoParse));	
	}
	
	/// <summary>
	/// Splits the string to list of objects
	/// </summary>
	/// <returns>A list of objects representing float, integer, boolean, datetime or string
	/// </returns>
	/// <param name='stringtoSplit'>What string to separate</param>
	public static List<object> ConvertStringsToSymbols (List<string> stringstoParse)
	{
		// Create result list
		List<object> result = new List<object> ();
		Single tempSingle; // Use in place of float and double
		// float tempFloat;
		int tempInt;
		bool tempBool;
		DateTime tempDateTime;
		
		// string tempString;
		string trimmed;
		List<string>stringList;
		// string[]stringArray;
		// Single[] tempNumbers;
		List<object> tempList;		
		
		// Parse through each object
		foreach (string symbol in stringstoParse)
		{
			// Trim the string
			trimmed = symbol.Trim ();
			
			try
			{
				// Use integer if possible
				if (int.TryParse (trimmed, out tempInt))
					result.Add (tempInt);
				// Check for float data type and use it if possible
				else if (Single.TryParse (trimmed, out tempSingle))
					result.Add (tempSingle);
				// Use boolean if possible
				else if (bool.TryParse (trimmed, out tempBool))
					result.Add (tempBool);
				// Use datetime if possible
				else if (DateTime.TryParse (trimmed, out tempDateTime))
					result.Add (tempDateTime);
				// Default as string
				else
				{
					// Check for sets
					if ((trimmed[0] == '(') && (trimmed[trimmed.Length - 1] == ')'))
					{
						// Check for components
						// stringArray = trimmed.Substring (1, trimmed.Length - 2).Split (',');
						// tempList = ConvertStringsToSymbols (stringArray);
						stringList = SplitString (trimmed.Substring (1, trimmed.Length - 2));
						tempList = ConvertStringsToSymbols (stringList);
						
						// Compound into complex object based on length
						if (tempList.Count == 1)
							result.Add ((Single) tempList[0]);
						else if (tempList.Count == 2)
							result.Add (new Vector2 ((Single)tempList[0], (Single)tempList[1]));
						else if (tempList.Count == 3)
							result.Add (new Vector3 ((Single)tempList[0], (Single)tempList[1], (Single)tempList[2]));
						else if (tempList.Count == 4)
							result.Add (new Quaternion ((Single)tempList[0], (Single)tempList[1], (Single)tempList[2], (Single)tempList[3]));
					}
					// Check for list
					else if ((trimmed[0] == '<') && (trimmed[trimmed.Length - 1] == '>'))
					{
						// Check for components
						stringList = SplitString (trimmed.Substring (1, trimmed.Length - 2));
						tempList = ConvertStringsToSymbols (stringList);
						
						// Check for parsed data, prepare for list
						result.Add (ConvertToList (tempList)); 
					}
					// Check for array
					else if ((trimmed[0] == '[') && (trimmed[trimmed.Length - 1] == ']'))
					{
						// Check for components
						stringList = SplitString (trimmed.Substring (1, trimmed.Length - 2));
						tempList = ConvertStringsToSymbols (stringList);
						
						// Check for parsed data, prepare for list
						result.Add (ConvertToArray (tempList)); 
					}
					// Nah, this is just a normal string
					else
					{
						result.Add (symbol);
					}
				}
			}
			catch 
			{
				// In case any error occurred
				return null;
			}
		}
		
		return result;
	}
	
	/// <summary>
	/// Splits the string to list of objects
	/// </summary>
	/// <returns>A list of objects representing float, integer, boolean, datetime or string
	/// </returns>
	/// <param name='stringtoSplit'>What string to separate</param>
	public static List<object> SplitStringsToSymbols (string stringtoSplit)
	{
		return ConvertStringsToSymbols (SplitString (stringtoSplit));
	}
	
	/// <summary>
	/// Converts a list of generic object into an objects of typed list using LINQ
	/// </summary>
	/// <returns>The list equivalent of the array, NULL if type differs</returns>
	/// <param name='parameters'>A list of typed objects, had to be the same for this to work</param>	
	public static IList ConvertToList (List<object> parameters)
	{
		// Simple verification
		if ((parameters == null) || (parameters.Count <= 0))
			return null;
		
		// Type check and create a generic typed list
		bool same = true;
		Type checkType = parameters[0].GetType (); 
		var genericType = typeof(List<>).MakeGenericType (checkType);
		var list = Activator.CreateInstance (genericType);
		MethodInfo addMethod = list.GetType().GetMethod("Add");
		
		foreach (object parameter in parameters)
		{
			if (parameter.GetType() != checkType)
			{
				same = false;
				break;
			}
			addMethod.Invoke(list, new object[] { parameter });
		}
				
		// Type verfication check
		if (!same)
			return null;
		
		// Return data
		return (IList)list;
	}
	
	/*  // Outdated method, now using purely dynamic
	/// <summary>
	/// Converts a list of generic object into an objects of typed list
	/// </summary>
	/// <returns>The list equivalent of the array, NULL if type differs</returns>
	/// <param name='parameters'>A list of typed objects, had to be the same for this to work</param>
	public static object ConvertToList (List<object> parameters)
	{
		// Preset to first item, if things are different then there is no use trying
		Type checkType = parameters[0].GetType (); 
		// Assume to be the same until prove false
		bool same = true; 
		// Predeterined buffer
		Dictionary<Type, object> buffer = new Dictionary<Type, object> (); 
		buffer.Add (typeof (string), new List<string> ());
		buffer.Add (typeof (int), new List<int> ());
		buffer.Add (typeof (float), new List<float> ());
		buffer.Add (typeof (double), new List<double> ());
		buffer.Add (typeof (DateTime), new List<DateTime> ());
		buffer.Add (typeof (Vector2), new List<Vector2> ());
		buffer.Add (typeof (Vector3), new List<Vector3> ());
		buffer.Add (typeof (Quaternion), new List<Quaternion> ());
		
		// Check for each object
		foreach (object parameter in parameters)
		{	
			// Check for different type
			if (parameter.GetType() != checkType)
			{
				same = false;
				break;
			}
			
			// Same type, add to list
			#region Add to buffer
			if (checkType == typeof (string))
			{
				(buffer[checkType] as List<string>).Add ((string) parameter);
			} 
			else if (checkType == typeof (int))
			{
				(buffer[checkType] as List<int>).Add ((int) parameter);
			} 
			else if (checkType == typeof (float))
			{
				(buffer[checkType] as List<float>).Add ((float) parameter);
			} 
			else if (checkType == typeof (double))
			{
				(buffer[checkType] as List<double>).Add ((double) parameter);
			} 
			else if (checkType == typeof (DateTime))
			{
				(buffer[checkType] as List<DateTime>).Add ((DateTime) parameter);
			} 
			else if (checkType == typeof (Vector2))
			{
				(buffer[checkType] as List<Vector2>).Add ((Vector2) parameter);
			} 
			else if (checkType == typeof (Vector3))
			{
				(buffer[checkType] as List<Vector3>).Add ((Vector3) parameter);
			} 
			else if (checkType == typeof (Quaternion))
			{
				(buffer[checkType] as List<Quaternion>).Add ((Quaternion) parameter);
			}
			else
				// Unknown type, throw to generic list
				(buffer[checkType] as List<object>).Add (parameter);
			#endregion 
		}
		
		// All items are of the same type, return the buffered list
		if (same)
			return buffer[checkType];
		
		// Return error flag
		return null;
	}
	*/
	
	/// <summary>
	/// Converts a list of generic objec	t into a typed array
	/// </summary>
	/// <returns>The list equivalent of the array, NULL if type differs</returns>
	/// <param name='parameters'>A list of typed objects, had to be the same for this to work</param>
	public static Array ConvertToArray (List<object> parameters)
	{
		try
		{
			// Force and try
			Type checkType = parameters[0].GetType (); 
			if ((checkType == typeof (float)) || (checkType == typeof (double)))
				checkType = typeof (System.Single);
			System.Array tempArray = System.Array.CreateInstance (checkType, parameters.Count);
			System.Array.Copy (parameters.ToArray (), tempArray, parameters.Count);
			return tempArray;
		}
		catch
		{
			// If failed, return null
			return null;
		}
	}
	#endregion
	
	/// <summary>
	/// Determine how many lines there are in this given string
	/// </summary>
	/// <returns>Number of lines</returns>
	/// <param name='toParse'>What string to parse</param>
	public static int CountLines(string toParse)
	{
		return toParse.Split ('\n').Length;
	}
	#endregion
}