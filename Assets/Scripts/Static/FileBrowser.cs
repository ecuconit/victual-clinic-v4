using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.IO;

public class FileBrowser
{
	#region
	#endregion
	
	#region Private Data Holder
	private GUISkin skin;
	private List<string> fileTypes = new List<string> ();
	private int depth = -2;
	
	private bool openFile;
	private DirectoryInfo currentDir;
	private FileInfo currentFile;
	private DirectoryInfo[] displayFolders;
	private FileInfo[] displayFiles;
	
	private string title;
	private string selectedFile;
	
	// Format buffering
	private Rect container;
	private Rect dragRegion;
	private GUIStyle containerStyle;
	private GUIStyle titleStyle;
	private GUIStyle closeIconStyle;
	private GUIStyle submitStyle;
	#endregion
	
	#region Public Getter & Setter
	/// <summary>
	/// Sets a list of usable file format filter
	/// </summary>
	/// <value>The file formats delimited by "|"</value>
	public string Filter
	{
		set 			
		{
			// Reset file filter type
			fileTypes.Clear ();
			
			if ((value != null) && (!value.Equals (string.Empty)))
			{
				int space;
				string tempString;				
				foreach (string fileType in value.Split ('|'))
				{
					// Trim the front
					tempString = fileType.TrimStart(' ', '.');
					
					// Cut the back, if necessary
					space = tempString.IndexOf (' ');
					if (space > 0)
						tempString = tempString.Substring (0, space);
					
					// Add to result
					fileTypes.Add (tempString);
				}
			}
		}
	}
	
	/// <summary>
	/// Gets the selected file path
	/// </summary>
	/// <value>The selected file path</value>
	public string SelectedFile
	{
		get 			
		{ 
			if (openFile)
			{
				// File open mode, check for file existence
				
				// Simple verification
				if ((currentFile == null) || (currentFile.Exists))
					return null;
				
				// Return the selected file path
				return currentFile.FullName;
			}
			else
			{
				// File save mode, doesn't need to exist
				return selectedFile;
			}
		}				
	}
	#endregion
	
	#region Object Constructor
	/// <summary>
	/// Initializes a new instance of the <see cref="FileBrowser"/> class.
	/// </summary>
	/// <param name='skinStyle'>GUI skin style collection</param>
	/// <param name='size'>How big the window is</param>
	/// <param name='fileType'>Default file type string</param>
	/// <param name='parentLayer'>Parent layer depth</param>
	public FileBrowser (GUISkin skinStyle, Vector2 size, string fileType, int parentLayer)
	{
		// Buffer input variables
		skin = skinStyle;
		this.Filter = fileType;
		depth = parentLayer - 1;
		
		// Fetch interpreted data, for quick access
		containerStyle = skin.box;
		container.width = size.x;
		container.height = size.y;
		titleStyle = skin.GetStyle ("Title");
		closeIconStyle = skin.GetStyle ("CloseButton");	
		submitStyle = skin.GetStyle ("SubmitButton");
		
		// Initialize the GUI elements
		InitializeGUI ();
	}
	
	/// <summary>
	/// Initializes the GUI
	/// </summary>
	/// <param name='startDir'>Starting direction for display</param>
	/// <param name='openMode'>Set the current display mode to open if true; false for save</param>
	public void Initialization (string startDir, bool openMode)
	{
		// Save option, if set to open mode need to verify file existence
		openFile = openMode;
		title = (openFile ? "Open File Dialog" : "Save File Dialog");
	
		// Initialize the GUI elements
		// InitializeGUI ();
	}
	
	/// <summary>
	/// Initializes the GUI elements
	/// </summary>
	private void InitializeGUI ()
	{
		// Check for screen overflow
		if (container.width > Screen.width - containerStyle.margin.left - containerStyle.margin.right)
			container.width = Screen.width - containerStyle.margin.left - containerStyle.margin.right;
		if (container.height > Screen.height - containerStyle.margin.top - containerStyle.margin.bottom)
			container.height = Screen.height - containerStyle.margin.top - containerStyle.margin.bottom;		
		container = new Rect ((Screen.width - container.width) / 2, (Screen.height - container.height) / 2, 
			container.width, container.height);
		dragRegion = new Rect (0, 0, container.width, titleStyle.fixedHeight);
		
		selectedFile = "";		
	}
	#endregion
	
	#region Frame Update
	/// <summary>
	/// Draw the display
	/// </summary>
	/// <returns>Whether the object has indicated to be ending or not</returns>
	/// <param name='delta'>How long it took since the last frame update</param>
	/// <param name='resize'>Whether or not there has been a resize event</param>
	public bool FrameUpdate (float delta, bool resize)
	{
		// Take care of the time, if necessary
		// So far nothing is time sensitive, do nothing
		
		// Forward the draw event
		return Draw (resize);
	}
	
	/// <summary>
	/// Draw the display
	/// </summary>
	/// <returns>Whether the object has indicated to be ending or not</returns>
	/// <param name='resize'>Whether or not there has been a resize event</param>
	public bool Draw (bool resize)
	{
		// If resize event is called, re-initialize GUI elements
		if (resize)
			InitializeGUI ();
		
		// Draw the container
		bool returnFlag = false;
		GUI.depth = depth;
		GUI.enabled = true;
		GUILayout.BeginArea (container, containerStyle);
		GUILayout.BeginVertical ();
		
		// Draw top bar
		GUILayout.BeginHorizontal ();
		{
			GUILayout.Label (title, titleStyle);
			if (GUILayout.Button ("", this.closeIconStyle))
				returnFlag = true;
		}
		GUILayout.EndHorizontal ();
		
		// Draw content region
		GUILayout.BeginHorizontal ();
		{
		}
		GUILayout.EndHorizontal ();
		
		// Draw input area
		GUILayout.BeginHorizontal ();
		{
			selectedFile = GUILayout.TextField (selectedFile);
			if (GUILayout.Button ("Okay"))
				Global.ToDebug ("Okay");
		}
		GUILayout.EndHorizontal ();
		
		GUILayout.EndVertical ();
		GUILayout.EndArea ();
		GUI.enabled = false;
		
		return returnFlag;
	}
	#endregion
}
