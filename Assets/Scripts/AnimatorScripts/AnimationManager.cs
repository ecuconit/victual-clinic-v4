using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;

public class AnimationManager : MonoBehaviour 
{
	#region Public Config Variable
	public bool debug = true;
	
	public List<AnimationController> characters;
	
	// public Debugger debugger;
	
	#endregion
	
	#region Private Data Holder Variable
	private Dictionary<CharacterType, List<AnimationController>> buffer = new Dictionary<CharacterType, List<AnimationController>> ();
	private List<AnimationController> currentCharacters = new List<AnimationController> ();
	
	// private bool displayDebugger = false;
	
	private KeyCode lastKey;
	private List<KeyCode> keys;
	#endregion
	
	#region Object Constructor 
	/// <summary>
	/// Awake this instance
	/// </summary>
	void Awake ()
	{
		// Buffer to global
		Global.AnimationManagerRef = this;	
		
		// Add each character to buffer
		foreach (AnimationController character in characters)	
			AddCharacter (character);
	}
	
	/// <summary>
	/// Start this instance
	/// </summary>
	void Start () 
	{
		// Initialize the animation controller
		SetCharacter (CharacterType.Receptionist);
		// Play the animation
		SetAction (ActionType.Wave, 3.0f);
				
		/*
		// Initialize keycodes		
		keys = new List<KeyCode> ();
		for (int i = 0; i < 12; ++i)
			keys.Add (KeyCode.None);
		*/
	}
	#endregion
	
	#region GUI Update
	public Vector2 buttonSize = new Vector2 (100, 18);
	public int margin = 5;
	
	private Rect typeWindow = new Rect (10, 10, 100, 50);
	private Rect poseWindow = new Rect (130, 10, 100, 50);
	private Rect actionWindow = new Rect (250, 10, 100, 50);
	
	/// <summary>
	/// On GUI update generate tester script
	/// </summary>
	void OnGUI ()
	{
		/*
		// Fetch key input
		Event e = Event.current;
		if ((e.isKey) && (e.keyCode != KeyCode.None) && (e.keyCode != lastKey))
		{
			// Update reference code
			lastKey = e.keyCode;
			
			// Check for code input
			if (lastKey == KeyCode.Return)
			{
				// Check for codes
				for (int i = 1; i <= toggleDebug.Length; ++i)
				{
					// Check for list difference from the back
					if (keys[keys.Count - i] != toggleDebug[toggleDebug.Length - i])
						break;
					// Verified
					debug = !debug;
					debugger.enabled = debug;
				}
			}
			else
			{
				// Add to queue
				keys.Add (lastKey);
				keys.RemoveAt(0);
			}
		}		
		*/
		
		// Check for simple case first
		if (!debug)
			return;
		
		// Draw character list window
		typeWindow = GUI.Window (0, typeWindow, DoTypeWindow, "Character");
			
		// Draw pose window
		// poseWindow = GUI.Window (1, poseWindow, DoPoseWindow, "Pose");
		
		// Draw action window
		actionWindow = GUI.Window (2, actionWindow, DoActionWindow, "Action");
		
		/*
		// Check for keyboard input
		if (Input.GetKeyUp (KeyCode.Slash))
			displayDebugger = !displayDebugger;
		
		// Draw debugger window
		if (displayDebugger)
			debugger.Draw ();
		*/
	}
	
	/// <summary>
	/// Fixeds the update
	/// </summary>
	void FixedUpdate() 
	{
		// Loop through each character to update the animation
		foreach (AnimationController character in currentCharacters)
			character.AnimationUpdate ();
	}
	
	#region Create Tester Windows
	/// <summary>
	/// Draw the character type window
	/// </summary>
	/// <param name='windowID'>Which window ID it is</param>
	private void DoTypeWindow (int windowID)
	{
		// Setup height index
		int heightIndex = (int)buttonSize.y;
		
		// Get each value
		foreach (int flag in Enum.GetValues (typeof (CharacterType)))
		{
			// Check for button click
			if (GUI.Button(new Rect(margin, heightIndex + margin, buttonSize.x, buttonSize.y), Enum.GetName (typeof (CharacterType), (CharacterType)flag)))
				SetCharacter ((CharacterType)flag);
			
			heightIndex += (int)buttonSize.y + margin;
		}
		
		// Set window size
		typeWindow.width = buttonSize.x + margin * 2;
		typeWindow.height = heightIndex + margin;
		
		// Draw draggable window
		GUI.DragWindow (new Rect (0, 0, typeWindow.width, typeWindow.height));
	}
	
	/// <summary>
	/// Draw the pose type window
	/// </summary>
	/// <param name='windowID'>Which window ID it is</param>
	private void DoPoseWindow (int windowID)
	{
		// Setup height index
		int heightIndex = (int)buttonSize.y;
		
		// Get each value
		foreach (int flag in Enum.GetValues (typeof (PositionType)))
		{
			// Check for button click
			if (GUI.Button(new Rect(margin, heightIndex + margin, buttonSize.x, buttonSize.y), Enum.GetName (typeof (PositionType), (PositionType)flag)))
				SetPosition ((PositionType)flag);
			
			heightIndex += (int)buttonSize.y + margin;
		}
		
		// Set window size
		poseWindow.width = buttonSize.x + margin * 2;
		poseWindow.height = heightIndex + margin;
		
		// Draw draggable window
		GUI.DragWindow (new Rect (0, 0, typeWindow.width, typeWindow.height));
	}
	
	/// <summary>
	/// Draw the action type window
	/// </summary>
	/// <param name='windowID'>Which window ID it is</param>
	private void DoActionWindow (int windowID)
	{
		// Setup height index
		int heightIndex = (int)buttonSize.y;
		
		// Get each value
		foreach (int flag in Enum.GetValues (typeof (ActionType)))
		{
			// Check for button click
			if (GUI.Button(new Rect(margin, heightIndex + margin, buttonSize.x, buttonSize.y), Enum.GetName (typeof (ActionType), (ActionType)flag)))
				SetAction ((ActionType)flag);
			
			heightIndex += (int)buttonSize.y + margin;
		}
		
		// Set window size
		actionWindow.width = buttonSize.x + margin * 2;
		actionWindow.height = heightIndex + margin;
		
		// Draw draggable window
		GUI.DragWindow (new Rect (0, 0, typeWindow.width, typeWindow.height));
	}
	#endregion	
	#endregion
	
	#region Animation Setter
	/// <summary>
	/// Sets the character position
	/// </summary>
	/// <param name='type'>Which character type to apply the animation</param>
	public void SetCharacter (CharacterType type)
	{
		// Buffer current type if possible
		if (!buffer.ContainsKey (type))
			buffer.Add (type, new List<AnimationController> ());
		currentCharacters = buffer[type];
		Global.ToDebug (this, "Set current control character to: [" + type + "]", LogMessageType.DisplayMessage);
	}
	
	/// <summary>
	/// Sets the character position
	/// </summary>
	/// <param name='type'>Which character type to apply the animation</param>
	/// <param name='setTo'>What position to set to</param>
	public void SetPosition (CharacterType type, PositionType setTo)
	{
		// Set current type
		SetCharacter (type);
		
		// Set position
		SetPosition (setTo);
	}

	/// <summary>
	/// Sets the character position
	/// </summary>
	/// <param name='setTo'>What position to set to</param>
	public void SetPosition (PositionType setTo)
	{
		// Loop through each character to apply the animation
		foreach (AnimationController character in currentCharacters)
			character.SetPosition (setTo);		
		Global.ToDebug (this, "Set current control position to: [" + setTo + "]", LogMessageType.DisplayMessage);
	}
	
	/// <summary>
	/// Sets the action in current position
	/// </summary>
	/// <param name='type'>Which character type to apply the animation</param>
	/// <param name='setTo'>What action to perform</param>
	public void SetAction (CharacterType type, ActionType setTo)
	{
		// Set current type
		SetCharacter (type);
		
		// Set position
		SetAction (setTo);
				
	}
	
	/// <summary>
	/// Sets the action in current position
	/// </summary>
	/// <param name='setTo'>What action to perform</param>
	public void SetAction (ActionType setTo)
	{
		// Loop through each character to apply the animation
		foreach (AnimationController character in currentCharacters)
			character.SetAction (setTo);
		
		Global.ToDebug (this, "Set current control action to: [" + setTo + "]", LogMessageType.DisplayMessage);
	}
	
	/// <summary>
	/// Sets the action in current position
	/// </summary>
	/// <param name='type'>Which character type to apply the animation</param>
	/// <param name='setTo'>What action to perform</param>
	/// <param name='duration'>Only perform the animation for given amount of time</param>
	public void SetAction (CharacterType type, ActionType setTo, float duration)
	{
		// Set current type
		SetCharacter (type);
		
		// Set position
		SetAction (setTo, duration);
	}
	
	/// <summary>
	/// Sets the action in current position
	/// </summary>
	/// <param name='setTo'>What action to perform</param>
	/// <param name='duration'>Only perform the animation for given amount of time</param>
	public void SetAction (ActionType setTo, float duration)
	{
		// Loop through each character to apply the animation
		foreach (AnimationController character in currentCharacters)
			character.SetAction (setTo, duration);
		Global.ToDebug (this, "Set current control action to: [" + setTo + "] for duration of [" + duration + "]", LogMessageType.DisplayMessage);
	}
	#endregion
	
	#region Helper Method
	/// <summary>
	/// Adds a character to buffer
	/// </summary>
	/// <returns>Whether the character was added correctly or not</returns>
	/// <param name='character'>Which character object to add</param>
	public bool AddCharacter (AnimationController character)
	{
		// Add the key of not already exist
		if (!buffer.ContainsKey (character.Character))
			buffer.Add (character.Character, new List<AnimationController> ());
		
		// Add to buffer
		buffer[character.Character].Add (character);
		return true;
	}
	#endregion
}
