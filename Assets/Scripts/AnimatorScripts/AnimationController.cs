using UnityEngine;
using System.Collections;

#region Type Enumerator
/// <summary>
/// Type of action flag
/// </summary>
public enum ActionType
{
	Idle = 0,
	Talk = 1,
	Wave = 2,
	Point = 3
}

/// <summary>
/// Type of position flag
/// </summary>
public enum PositionType
{
	Sit = 0,
	Stand = 1,
	Lay = 2,
	Tub = 3,
	SitHold = 4
}

/// <summary>
/// Type of character flag
/// </summary>
public enum CharacterType
{
	Receptionist = 0,
	Preceptor = 1,
	Patient = 2,
	Partner = 3
}
#endregion

public class AnimationController : MonoBehaviour 
{
	#region Public Config Variable
	public CharacterType character;
	public PositionType currentPosition;
	#endregion
	
	#region Private Data Holder Variable
	private bool initialized;
	private Animator animator;
	private float timeStamp;
	
	private ActionType currentAction;
	private float currentRandom;
	#endregion
	
	#region Hash ID
	private int action;
	// private int position;
	private int random;
	#endregion
	
	#region Public Getter & Setters
	/// <summary>
	/// Gets the character type 
	/// </summary>
	/// <value>
	/// The character type flag
	/// </value>
	public CharacterType Character
	{
		get { return character; }
	}
	
	/*
	/// <summary>
	/// Gets or sets the character pose
	/// </summary>
	/// <value>
	/// The pose flag
	/// </value>
	public PositionType Position
	{
		get { return currentPosition; }
		set { SetPosition (value); }
	}
	*/
	#endregion
	
	#region Object Constructor 
	/// <summary>
	/// Awake this instance, verify configuration
	/// </summary>
	void Awake ()
	{
		initialized = true;
	}
	
	/// <summary>
	/// Start this instance
	/// </summary>
	void Start () 
	{
		// Local buffer
		animator = gameObject.GetComponent<Animator> ();
		
		// Hash ID buffer
	 	action = Animator.StringToHash("Action");
		// position = Animator.StringToHash("Position");
		random = Animator.StringToHash("Random");
		
		// Initialize stances
		// SetPosition (currentPosition);
       	
		// Make sure object is verified
		if (!initialized)
			return;
	}
	#endregion
	
	#region Update Method
	/// <summary>
	/// Animations the update animation
	/// </summary>
	public void AnimationUpdate ()
	{
		// Updates the current values
		animator.SetFloat (random, currentRandom);
		animator.SetInteger (action, (int)currentAction);
	}
	#endregion
	
	#region Animation Setter
	/// <summary>
	/// Sets the character position
	/// </summary>
	/// <param name='setTo'>What position to set to</param>
	public void SetPosition (PositionType setTo)
	{
		/*
		// Set parameters to trigger state transition
		animator.SetFloat (random, GetRandom());
		animator.SetInteger (position, (int)setTo);
		*/
		
		// Local buffer
		currentPosition = setTo;
		currentAction = ActionType.Idle;
		// Global.ToDebug (this, "Pose set to: " + setTo.ToString (), LogMessageType.DisplayMessage);
	}
	
	/// <summary>
	/// Sets the action in current position
	/// </summary>
	/// <param name='setTo'>What action to perform</param>
	public void SetAction (ActionType setTo)
	{
		/*
		// Set parameters to trigger state transition
		animator.SetFloat (random, GetRandom());
		animator.SetInteger (action, (int)setTo);
		*/
			
		// Buffer and display
		currentAction = setTo;
		// Global.ToDebug (this, "Action set to: " + setTo.ToString (), LogMessageType.DisplayMessage);
	}
	
	/// <summary>
	/// Sets the action in current position
	/// </summary>
	/// <param name='setTo'>What action to perform</param>
	/// <param name='duration'>Only perform the animation for given amount of time</param>
	public void SetAction (ActionType setTo, float duration)
	{
		/*
		// Call the worker method
		SetAction (setTo);
		*/
			
		// Delay to random
		currentAction = setTo;
		StartCoroutine(DelayThenDefault (duration, Time.time));
	}
	#endregion
	
	#region Helper Method
	/// <summary>
	/// Get a random value
	/// </summary>
	/// <returns>A random value between 0.0 ~ 1.0</returns>
	private float GetRandom ()
	{
		currentRandom = UnityEngine.Random.value;
		return currentRandom;
	}
	
	/// <summary>
	/// Get a random value
	/// </summary>	
	/// <returns>A random value between 1 ~ max</returns>
	/// <param name='max'>Maximum amount to return</param>
	private int GetRandom (int max)
	{
		currentRandom = Mathf.CeilToInt(UnityEngine.Random.value * max);
		return (int)currentRandom;
	}
	
	/// <summary>
	/// Wait then set to default idle animation
	/// </summary>
	/// <returns>Return flag</returns>
	/// <param name='duration'>Amount of delay</param>
	/// <param name='startTime'>Starting timestamp</param>
	private IEnumerator DelayThenDefault (float duration, float startTime)
	{
		// Buffer time stamp and call delay
		timeStamp = Time.time;
		yield return new WaitForSeconds(duration);
		
		// Check to be sure that the session is still valid
		if (timeStamp == startTime)
		{
			SetPosition (currentPosition);
		}
	}
	#endregion
}
