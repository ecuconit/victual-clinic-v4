using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public enum InventoryItemType {MedicalHistory, RegistrationForm, VitialSigns};

public class InventoryManager : MonoBehaviour
{
    public List<InventoryItem> Items;
    public wwwread XMLReader;

    public GUIStyle RegistrationFormStyle = new GUIStyle();
    public Texture2D[] RegistrationFormTextures = new Texture2D[2];

    void Start()
    {
        XMLReader = GetComponent<wwwread>();

        Items = new List<InventoryItem>();

        //AddInventoryItem(InventoryItemType.MedicalHistory);
        //AddInventoryItem(InventoryItemType.RegistrationForm);
    }

    public void AddInventoryItem(InventoryItemType itype)
    {
        switch (itype)
        {
            case InventoryItemType.MedicalHistory:
                Items.Add(new InventoryItem("Medical History", itype));
                break;
            case InventoryItemType.RegistrationForm:
                Items.Add(new RegistrationForm(RegistrationFormStyle, RegistrationFormTextures, XMLReader));
                break;
            case InventoryItemType.VitialSigns:
                Items.Add(new InventoryItem("Vitial Signs", itype));
                break;
        }
    }

    public void LoadFromWeb()
    {
        foreach (InventoryItem item in Items)
        {
            item.LoadFromWeb();
        }
    }
}