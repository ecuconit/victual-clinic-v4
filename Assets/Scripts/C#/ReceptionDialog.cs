using UnityEngine;
using System;
using System.Collections;
using System.IO;
using System.Collections.Generic;
using VirtualClinic.WebSite.Serializable;

[System.Serializable]
public class ReceptionQuestionWindow : PlayerQuestionWindow
{
	#region Private Data Holder
	private Rect introBar;
	private string parsedIntroText;	
	#endregion
	
	#region Object Constructor
	/// <summary>
	/// Uses the default base constructor, nothing new need to be added
	/// </summary>
	/// <param name='options'>Display options</param>
	/// <param name='questions'>List of questions</param>
	/// <param name='answers'>List of answers</param>
	/// <param name='interview'>Interview controller reference</param>
	public ReceptionQuestionWindow (StepOptions options, Dictionary<long, InterviewQuestion> questions, List<long> answers, InterviewController interview) 
		: base(-1, "Welcome", options, interview.GUIComponents, 6, questions, answers, interview.Inventory) { }
	#endregion
	
	#region Override Method
	/// <summary>
	/// Overwrite on how questions are drawn
	/// </summary>
	/// <returns>Return flag</returns>
	/// <param name='currentHeight'>Current height index</param>
	protected override SelectionReturn DrawQuestions (float currentHeight)
	{
		// Perform transition
		SelectionReturn returnFlag = null;
		questionBox.y = Screen.height - questionBox.height + currentHeight;
		
		// Draws the box
		GUILayout.BeginArea (questionBox, boxStyle);		
		
		// Draw title bar
		GUILayout.Label("Welcome to Virtual Clinic", headerStyle);
		GUILayout.Space (lineoffset * 0.25f);
		
		// Draw body
		scrollViewVector = GUILayout.BeginScrollView (scrollViewVector);
		GUILayout.BeginVertical ();
		{
			// Draw the okay button
			if (DrawMovetoNextStep3 ())
			{
				// Create return flag and retract the window
				returnFlag = new SelectionReturn(AllSelections);
				targetPlayerDialogHeight = ScrollViewHeight;
			}
		}
		GUILayout.EndVertical ();
		GUILayout.EndScrollView ();
		GUILayout.EndArea ();
		
		return returnFlag;
		
		/*
		// Start a scroll view
		ScrollViewLineOffset = 0;
		scrollViewVector = GUI.BeginScrollView (new Rect (20, Screen.height - currentHeight + lineoffset * 2, Screen.width - 55, ScrollViewHeight), 
			scrollViewVector, new Rect (0, 0, 400, (lineoffset * Questions.Count) + ScrollViewOffset)); //(lineoffset*Questions.Count) + ScrollViewOffset)		
		int i = 0;
		
		// Draw each question
		string questionText;
		int additionalLines;
		foreach (InterviewQuestion question in Questions.Values) {
			
			// Check to see if the option was already selected, disable click if already selected
			if (SelectedAnswers.Contains (question.Response.ID))
				GUI.enabled = false;
			
			// Generate display text
			questionText = CutStringToSize (question.Text);
			additionalLines = CountLines (questionText);
			if (GUI.Button (new Rect (0, (lineoffset * i), Screen.width - 50, lineoffset + lineoffset * additionalLines), questionText, PlayerDialogTextStyle)) {
				AllSelections [question.Response.ID] = true;
				return new SelectionReturn (question.ID, question.ResponseDisplayType);
			}
			GUI.enabled = true;
			
			if (additionalLines > 0)
				ScrollViewLineOffset += lineoffset * additionalLines;
			i += additionalLines;
			++i;
		}
		// Get the total number of additional lines
		totalAdditionalLines = i;
		
		// Draw the proceed button
		if (DrawMovetoNextStep2 (currentPlayerDialogHeight, totalAdditionalLines)) {
			return new SelectionReturn (AllSelections);
		}
		ScrollViewLineOffset += lineoffset;		
		GUI.EndScrollView ();
		
		// Cap the view offset to view line offset
		if (ScrollViewLineOffset > ScrollViewOffset)
			ScrollViewOffset = ScrollViewLineOffset;
		
		return null;
		*/
	}
	#endregion
}

public class ReceptionDialog : MonoBehaviour
{
	public Dictionary<long, InterviewQuestion> Questions;
	public List<long> SelectedQuestions;
	public List<long> SelectedAnswers;
	ReceptionQuestionWindow QuestionSelection;
	PatientResponseWindow Response;
	// bool drawResponse = true;
	SelectionReturn selection;
	InterviewController interview;
	// float startResponseHeight;
	// float currentResponseHeight;
	// float targetResponseHeight = 0;
	float lineoffset;
	// GUIStyle PlayerDialogTextStyle;
	// GUIStyle ReceptionistResponseTextStyle;
	wwwread SeverConnection;
	string ResponseText;
	public string ResponseText1 = "Response 1 this should be loaded from web";
	public string ResponseText2 = "Response 2 this should be loaded from web";
	public string AudioURL1;
	public string AudioURL2;
	GameObject ResponseAudioPlayer;
	bool AudioStarted = false;
	bool BothAudioPlayed = false;

	public float shadowOffset = 1f;
	private GUIStyle playerTextStyle;
	private GUIStyle patientTextStyle;
	private Color patientTextColor;
	private GUIStyle patientBackgroundStyle;
	private Color patientBackgroundColor;
	private float responseBarHeight;
	private Rect responseBar;
	private Rect[] responseBarShadow;
	private float currentAlpha = 0.0f;
	private float targetAlpha = 0.0f;
	private float backgroundMaxAlpha = 0.60f;
	private float alphaTransitionSpeed = 5.0f;
	private float alphaThreshold = 0.15f;

	//InterviewQuestion testquestion = new InterviewQuestion(1,1,"Hello",true,"Hi! How are you today?","",0,false,"");
	//InterviewQuestion testquestion2 = new InterviewQuestion(2,2,"I'm fine thanks, and you?",true,"Oh you know, busy busy here in the virtual clinic","",0,false,"");

	#region Object Initialization
	/// <summary>
	/// Start this instance
	/// </summary>
	void Start ()
	{
		interview = GetComponent<InterviewController> ();
		// animation.Play(AnimationType.Wave_Stand);

		Questions = new Dictionary<long, InterviewQuestion> ();
		SelectedAnswers = new List<long> ();
		SelectedQuestions = new List<long> ();
		QuestionSelection = new ReceptionQuestionWindow (new StepOptions (0, 0, StepQuestionSelectionType.SingleClickSelectionButton), Questions, SelectedAnswers, interview);
		//Response = new PatientResponseWindow(ReceptionistResponseTextStyle,Questions ); 
		//AddQuestion(testquestion);

		// PlayerDialogTextStyle = interview.GUIComponents.GetStyle (InterviewStyle.PlayerText);
		// ReceptionistResponseTextStyle = interview.GUIComponents.GetStyle (InterviewStyle.ResponseText);
		playerTextStyle = interview.GUIComponents.GetStyle(InterviewStyle.PlayerText);
		patientTextStyle = interview.GUIComponents.GetStyle(InterviewStyle.ResponseText2);
		patientTextColor = patientTextStyle.normal.textColor;
		patientBackgroundStyle = new GUIStyle(patientTextStyle);
		patientBackgroundColor = new Color(0f, 0f, 0f, backgroundMaxAlpha);
		patientBackgroundStyle.normal.textColor = patientBackgroundColor;
		patientBackgroundStyle.hover.textColor = patientBackgroundColor;
		
		//Fake response
		// lineoffset = PlayerDialogTextStyle.fontSize + 10;
		// startResponseHeight = 0 - lineoffset * 2;
		// currentResponseHeight = startResponseHeight;

		ResponseText = ResponseText1;		
		ResponseAudioPlayer = new GameObject ("Receptionist Audio Playback");
		ResponseAudioPlayer.AddComponent <AudioSource>();
		ResponseAudioPlayer.GetComponent<AudioSource>().volume = 10.0f;
		ResponseAudioPlayer.GetComponent<AudioSource>().spatialBlend = 0;
	}
	
	#region Data Parser
	/// <summary>
	/// Inits the interview steps
	/// </summary>
	public void InitStep ()
	{
		QuestionSelection.initQuestions ();
	}

	/// <summary>
	/// Adds an interview question to buffer
	/// </summary>
	/// <param name='question'>Which question to add</param>
	public void AddQuestion (InterviewQuestion question)
	{
		Questions.Add (question.ID, question);
		QuestionSelection.initQuestions ();
	}

	/// <summary>
	/// Initialize display text and audio stream
	/// </summary>
	/// <param name='T1'>Display text 1</param>
	/// <param name='A1'>Display text 2</param>
	/// <param name='T2'>Audio 1</param>
	/// <param name='A2'>Audio 2</param>
	public void SetTextAndAudio (string T1, string A1, string T2, string A2)
	{
		ResponseText1 = T1;
		ResponseText2 = T2;
		AudioURL1 = A1;
		AudioURL2 = A2;
		
		// Initialize GUI elements		
		ResponseText = ResponseText1;
		InitializeGUI();		
	}
	#endregion
	
	#region Initialize GUI Positions
	private Rect currentDisplay;
	private string parsedResponseText;
	
	/// <summary>
	/// Initializes the GUI element positions
	/// </summary>
	private void InitializeGUI ()
	{
		// Calculate the necessity response header, fit to font width, assume font width = fontSize * 2
		int characterCount = (Screen.width - patientTextStyle.margin.left - patientTextStyle.margin.right -
			patientTextStyle.padding.left - patientTextStyle.padding.right) / patientTextStyle.fontSize * 2;
		lineoffset = patientTextStyle.fontSize + 10;

		// Calculate the response header
		parsedResponseText = TextParser.CutStringToSize(ResponseText, characterCount);
		responseBarHeight = (lineoffset * TextParser.CountLines(parsedResponseText) + 10);
		responseBar = new Rect(0, QuestionSelection.GetCurrentTop - responseBarHeight, Screen.width, responseBarHeight);
		responseBarShadow = new Rect[] { new Rect(responseBar), new Rect(responseBar), new Rect(responseBar), new Rect(responseBar) };
		for (int i = 0; i < responseBarShadow.Length; ++i)
		{
			responseBarShadow[i].x += (i % 2 == 0 ? shadowOffset : -shadowOffset);
			responseBarShadow[i].y += (i / 2 >= 1 ? shadowOffset : -shadowOffset);
		}
		targetAlpha = 1.0f;
	}
	#endregion
	#endregion

	#region Frame Update
	/// <summary>
	/// Draw the receptionist dialog
	/// </summary>
	/// <returns>Whether the form has ended or not</returns>
	/// <param name='resize'>Whether the GUI has been resized or not</param>
	public bool Draw(bool resize)
	{
		// If marked to redraw, reinitialize GUI elements
		if (resize)
			InitializeGUI();

		// Draw the questions, not really necessary since receptionst doesn't have an associated question set
		selection = QuestionSelection.Draw();

		// Newer style, precalculate location and size
		// currentDisplay.y = currentResponseHeight = iTween.FloatUpdate (currentResponseHeight, targetResponseHeight, 3f);
		// GUI.Button (currentDisplay, parsedResponseText, ReceptionistResponseTextStyle); // Top bar, response


		// Check for bottom
		if ((AudioStarted) && (currentAlpha < alphaThreshold))
		{
			// Bottom hit, something new to display, show it
			Global.AnimationManagerRef.SetAction(ActionType.Talk);
			AudioStarted = false;
			ResponseText = ResponseText2;

			// Reinitialize GUI based on text change
			InitializeGUI();
			StartCoroutine(PlayAudioResponse(AudioURL2));
		}

		// Advance alpha
		patientTextColor.a = currentAlpha = iTween.FloatUpdate(currentAlpha, targetAlpha, alphaTransitionSpeed);
		patientBackgroundColor.a = currentAlpha * backgroundMaxAlpha;
		patientBackgroundStyle.hover.textColor = patientBackgroundStyle.normal.textColor = patientBackgroundColor;
		patientTextStyle.hover.textColor = patientTextStyle.normal.textColor = patientTextColor;
		// Draw shadow
		foreach (Rect shadow in responseBarShadow)
			GUI.Label(shadow, parsedResponseText, patientBackgroundStyle);
		// Draw the text on top
		GUI.Label(responseBar, parsedResponseText, patientTextStyle);

		// If player clicked through the to next, proceed to next step
		if (selection.SelectionType == SelectionReturnType.StepFinished)
		{
			Destroy(ResponseAudioPlayer);
			return false;
		}

		// If has played but stopped, load the next text
		if ((AudioStarted) && (!BothAudioPlayed) && (!ResponseAudioPlayer.GetComponent<AudioSource>().isPlaying))
		{
			targetAlpha = 0.0f;
		}

		return true;
	}
	#endregion

	#region Helper Method
	/// <summary>
	/// Counts the lines of given string
	/// </summary>
	/// <returns>Number of lines</returns>
	/// <param name='stringtoCount'>What string to count</param>
	int CountLines (string stringtoCount)
	{
		int lines = 0;
		for (int i = 0; i < stringtoCount.Length; ++i) {
			if (stringtoCount [i] == '\n')
				lines++;
		}
		return lines;
	}

	/// <summary>
	/// Plays an audio clip determined by preset public URL
	/// </summary>
	public void StartAudioPlayback ()
	{
		// Play the animation 
		Global.AnimationManagerRef.SetAction (ActionType.Talk, 2.5f);
		StartCoroutine (PlayAudioResponse (AudioURL1));
		// animation.Play(AnimationType.Talk_Stand);		
	}

	/// <summary>
	/// Plays an audio clip
	/// </summary>
	/// <returns>Coroutine response</returns>
	/// <param name='sourceURL'>Audio URL</param>
	private IEnumerator PlayAudioResponse (string sourceURL)
	{
		//Debug.Log("Trying to Play audio " + "https://zhuoffice2.dyndns.org" + sourceURL);
		WWW stream;

		//Debug.Log("Connecting to stream...");

		if (sourceURL != null && sourceURL.Length > 10) {
			//"https://zhuoffice2.dyndns.org" 
			stream = new WWW (sourceURL);
			yield return stream;
		} else {
			return false;
		}

		ResponseAudioPlayer.GetComponent<AudioSource>().clip = stream.audioClip;
		ResponseAudioPlayer.GetComponent<AudioSource>().Play ();

		AudioStarted = true;
		if (sourceURL == AudioURL2) {
			BothAudioPlayed = true;
		}
		//animation.Play(AnimationType.Talk_Stand);

		// Update animation to be timed
		Global.AnimationManagerRef.SetAction (ActionType.Talk, stream.audioClip.length);
	}
	#endregion

	//	void OnGUI()
	//	{
	//		GUILayout.Label(ResponseText1);
	//		GUILayout.Label(ResponseText2);
	//		GUILayout.Label(AudioURL1);
	//		GUILayout.Label(AudioURL2);
	//	}
}

/* Code grave yard
			if( selection.SelectionType == SelectionReturnType.AnswerSelected)
			{	
				SelectedQuestions.Add(selection.SelectedAnswer);
				SelectedAnswers.Add(Questions[selection.SelectedAnswer].AnswerID);	
			}
			
			if( selection.SelectionType == SelectionReturnType.SelectionChanged)
			{	
				drawResponse = false;
			}	

			if(drawResponse)
			{
				Response.Draw(selection.SelectedAnswer);
				if(selection.SelectedAnswer == 1)
				{
					Debug.Log("selected hello");
					AddQuestion(testquestion2);
				}
			}	

	/// <summary>
	/// Initializes the GUI element positions
	/// </summary>
	private void InitializeGUI ()
	{
		// Resize current respond text
		int characterCount = (Screen.width - ReceptionistResponseTextStyle.margin.left - ReceptionistResponseTextStyle.margin.right - 
			ReceptionistResponseTextStyle.padding.left - ReceptionistResponseTextStyle.padding.right) / ReceptionistResponseTextStyle.fontSize * 2;
		lineoffset = ReceptionistResponseTextStyle.fontSize + 10;	
		
		parsedResponseText = TextParser.CutStringToSize (ResponseText, characterCount);
		currentDisplay = new Rect (0, startResponseHeight, Screen.width, lineoffset * TextParser.CountLines (parsedResponseText) + 10);
	}
 * 
 
   /// <summary>
	/// Draw the receptionist dialog
	/// </summary>
	/// <returns>Whether the form has ended or not</returns>
	/// <param name='resize'>Whether the GUI has been resized or not</param>
	public bool Draw (bool resize)
	{
		// If marked to redraw, reinitialize GUI elements
		if (resize)
			InitializeGUI ();
		
		// Draw the questions, not really necessary since receptionst doesn't have an associated question set
		selection = QuestionSelection.Draw ();
		
		//// Older style, uses local calculation
		//lineoffset = ReceptionistResponseTextStyle.fontSize + 10;
		//currentResponseHeight = iTween.FloatUpdate(currentResponseHeight, targetResponseHeight, 3f);
		
		//// Display the preset response text defined by T1 of SetTextAndAudio method
		//GUI.Button(new Rect(0, currentResponseHeight, Screen.width, lineoffset * 2 + lineoffset * CountLines(ResponseText)), ResponseText, ReceptionistResponseTextStyle);
		
		// Newer style, precalculate location and size
		currentDisplay.y = currentResponseHeight = iTween.FloatUpdate (currentResponseHeight, targetResponseHeight, 3f);
		GUI.Button (currentDisplay, parsedResponseText, ReceptionistResponseTextStyle); // Top bar, response
			
		// If player clicked through the to next, proceed to next step
		if (selection.SelectionType == SelectionReturnType.StepFinished) {
			Destroy (ResponseAudioPlayer);
			return false;
		}
		if (AudioStarted && !BothAudioPlayed) {
			// If audio stopped, load the next text
			if (!ResponseAudioPlayer.audio.isPlaying) {
				// Play the animation
				Global.AnimationManagerRef.SetAction (ActionType.Talk);
				AudioStarted = false;
				ResponseText = ResponseText2;
				
				// Reinitialize GUI based on text change
				InitializeGUI (); 
				StartCoroutine (PlayAudioResponse (AudioURL2));
				// animation.Play(AnimationType.Talk_Stand);				
			}
		}
		// Both audio has played, manually switching to idle
		if (BothAudioPlayed) {
			// Global.AnimationManagerRef.SetAction (ActionType.Idle);
			
			//	if(ResponseAudioPlayer.audio.isPlaying)
			//		animation.Play(AnimationType.Talk_Stand);
		}

		return true;
	}
*/