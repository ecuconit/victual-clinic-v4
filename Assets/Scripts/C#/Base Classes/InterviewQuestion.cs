using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using VirtualClinic.WebSite.Serializable;

[System.Serializable]
public class InterviewQuestion
{
	public long ID;
	public string Text;
	public bool IsCorrectToAsk;
	public string Rationale;
	public QuestionResponseDisplayType? ResponseDisplayType;
	public InterviewResponse Response;

	public InterviewQuestion()
	{
		ID = -1;
		Text = "No Question Text Provided";
		IsCorrectToAsk = true;
		Rationale = "No Rationale Provided";
		ResponseDisplayType = QuestionResponseDisplayType.NoResponse;
		Response = null;	
	}
	
	public InterviewQuestion(long id, string text, bool correct, string rationale,QuestionResponseDisplayType type,InterviewResponse response)
	{
		ID = id;
		Text = text;
		IsCorrectToAsk = correct;
		Rationale = rationale;
		ResponseDisplayType = type;
		Response = response;	
	}
}

[System.Serializable]
public class InterviewResponse
{
    public long ID;
    public string Text;
    public bool HasVoiceAudio;
    public string VoiceAudioUrl;
    public string[] Multimedia;

    public InterviewResponse()
    {
        ID = -1;
        Text = " ... ";
        HasVoiceAudio = false;
        VoiceAudioUrl = null;
        Multimedia = null;
    }

    public InterviewResponse(long id, string text, bool needaudio, string audioURL, string[] media)
    {
        ID = id;
        Text = text;
        HasVoiceAudio = needaudio;
        VoiceAudioUrl = audioURL;
        Multimedia = media;
    }
}