using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.IO;

public enum MediaContentType {image, audio, video};

public class MediaContent
{
    public string url;
    public MediaContentType Type;
    public Texture2D texture;
    public AudioClip audioClip;

    public MediaContent(string address)
    {
        url = address;
    }
}

public class MultimediaWindow : MonoBehaviour
{
    public string ServerAddress;
    public GameObject Media;
    static MultimediaWindow instance;
    public bool windowOpen = false;
    public bool windowClosed = false;
    static bool opening = false;

    public GUIStyle CloseButtonStyle;
    public Rect CloseButtonRect;
    public Rect NextButtonRect;
    public GUIStyle NextButtonStyle;
    public Rect PrevButtonRect;
    public GUIStyle PrevButtonStyle;
    public Rect WindowTitleRect;
    public GUIStyle WindowTitleStyle;

    public Texture2D audioIcon;
    public Rect PlayButtonRect;
    public GUIStyle PlayButtonStyle;
    public Rect StopButtonRect;
    public GUIStyle StopButtonStyle;

    public List<MediaContent> MediaItem;

    public int CurrentMediaItem = 0;

    public GameObject MediaAudioPlayer;

    // Use this for initialization
    void Start()
    {
        instance = GetComponent<MultimediaWindow>();
        windowOpen = false;
        windowClosed = false;

        MediaAudioPlayer = new GameObject();
        MediaAudioPlayer.AddComponent<AudioSource>();
        MediaAudioPlayer.GetComponent<AudioSource>().volume = 10.0f;
        MediaAudioPlayer.GetComponent<AudioSource>().spatialBlend = 0;
    }

    public static void Draw(string[] mediaURLs)
    {
        if (instance.windowClosed)
            return;
        if (!instance.windowOpen)
        {
            if (!opening)
                instance.LoadContent(mediaURLs);
            instance.Openwindow();

        }
        else
        {

            //Media Items
            switch (instance.MediaItem[instance.CurrentMediaItem].Type)
            {
                case MediaContentType.image:
                    if (instance.Media.GetComponent<Renderer>().material.mainTexture != instance.MediaItem[instance.CurrentMediaItem].texture)
                        instance.Media.GetComponent<Renderer>().material.mainTexture = instance.MediaItem[instance.CurrentMediaItem].texture;
                    break;

                case MediaContentType.audio:
                    if (instance.Media.GetComponent<Renderer>().material.mainTexture != instance.audioIcon)
                        instance.Media.GetComponent<Renderer>().material.mainTexture = instance.audioIcon;
                    instance.MediaAudioPlayer.GetComponent<AudioSource>().clip = instance.MediaItem[instance.CurrentMediaItem].audioClip;
                    if (instance.MediaAudioPlayer.GetComponent<AudioSource>().isPlaying)
                        GUI.enabled = false;
                    if (GUI.Button(instance.PlayButtonRect, "", instance.PlayButtonStyle))
                        instance.MediaAudioPlayer.GetComponent<AudioSource>().Play();
                    GUI.enabled = true;
                    if (GUI.Button(instance.StopButtonRect, "", instance.StopButtonStyle))
                        instance.MediaAudioPlayer.GetComponent<AudioSource>().Stop();
                    break;
            }

            //Title and gui Buttons
            GUI.Label(instance.WindowTitleRect, "Item " + (instance.CurrentMediaItem + 1) + " of " + instance.MediaItem.Count, instance.WindowTitleStyle);
            if (GUI.Button(instance.CloseButtonRect, "", instance.CloseButtonStyle))
            {
                instance.Closewindow();
                instance.MediaAudioPlayer.GetComponent<AudioSource>().Stop();
            }


            if ((instance.CurrentMediaItem + 1) < instance.MediaItem.Count)
            {
                if (GUI.Button(instance.NextButtonRect, "", instance.NextButtonStyle))
                {
                    instance.MediaAudioPlayer.GetComponent<AudioSource>().Stop();
                    instance.CurrentMediaItem++;
                }

            }
            if (instance.CurrentMediaItem > 0 && instance.MediaItem.Count > 1)
            {
                if (GUI.Button(instance.PrevButtonRect, "", instance.PrevButtonStyle))
                {
                    instance.MediaAudioPlayer.GetComponent<AudioSource>().Stop();
                    instance.CurrentMediaItem--;
                }

            }

        }
    }

    public void Openwindow()
    {
        if (!opening)
        {
            iTweenEvent.GetEvent(gameObject, "open").Play();
            opening = true;
        }
    }

    public void Closewindow()
    {
        windowClosed = true;
        iTweenEvent.GetEvent(gameObject, "close").Play();
    }

    public void SetOpen()
    {
        windowOpen = true;
        opening = false;
    }

    void LoadContent(string[] mediaURLs)
    {
        MediaItem = new List<MediaContent>();
        foreach (string url in mediaURLs)
        {
            MediaContent item = new MediaContent(url);
            StartCoroutine(LoadItemFromServer(item));
            MediaItem.Add(item);
        }
    }

    public IEnumerator LoadItemFromServer(MediaContent item)
    {
        string e = Path.GetExtension(item.url);
        if (e == ".png" || e == ".jpg")
        {
            item.Type = MediaContentType.image;
            WWW pic = new WWW(ServerAddress + item.url);
            yield return pic;
            item.texture = pic.texture;
        }
        if (e == ".ogg")
        {
            item.Type = MediaContentType.audio;
            WWW sound = new WWW(ServerAddress + item.url);
            yield return sound;
            item.audioClip = sound.audioClip;
        }
    }
}