using UnityEngine;
using System;
using System.Collections;

[System.Serializable]
public class InterviewWindow
{
    protected bool hyphenates = false;
    protected float lineoffset;
    protected int MaxNumOfChars;
	
	/// <summary>
	/// Cuts the size of the string to list of strings limited by lineSize
	/// </summary>
	/// <returns>Parsed string delimited by /n, each line is no longer than lineSize</returns>
	/// <param name='stringtoCut'>What string to cut</param>
	/// <param name='lineSize'>Custom line size</param>
	public string CutStringToSize(string stringtoCut, int lineSize)
    {
		// Buffer the current max line size
		int currentMax = MaxNumOfChars;
		MaxNumOfChars = lineSize;
		string result = CutStringToSize(stringtoCut);
		MaxNumOfChars = currentMax;
		return result;		
	} 
     
    /// <summary>
	/// Cuts the size of the string to list of strings limited by MaxNumOfChars
	/// </summary>
	/// <returns>Parsed string delimited by /n, each line is no longer than lineSize</returns>
	/// <param name='stringtoCut'>What string to cut</param>
	public string CutStringToSize(string stringtoCut)
    {
		// Check for simple case, see if something needs to be done
        if (stringtoCut.Length > MaxNumOfChars)
        {
            //DebugConsole.Log("found a long string. Resizing");
            int lineBreak = -1;
            string firstline, rest;
            
            // DebugConsole.Log ("Trying to parse \"" + stringtoCut + "\".");
			
			// Try to cut by line break
            lineBreak = stringtoCut.IndexOf('\n', 0, MaxNumOfChars);
            if (lineBreak < 0)
				// Line break not found, try space
				lineBreak = stringtoCut.LastIndexOf(' ', MaxNumOfChars, MaxNumOfChars / 3);
			if (lineBreak > 0)
			{
				// Cut by linebreak/space found, use it
				firstline = stringtoCut.Substring(0, lineBreak);
				rest = stringtoCut.Substring(lineBreak).Trim();
				return firstline + "\n" + CutStringToSize(rest);
			}
			
			// Line too long, try to hyphenate
			lineBreak = MaxNumOfChars;
			if (hyphenates)
			{
            	firstline = stringtoCut.Substring(0, lineBreak - 1) + "-";
				rest = stringtoCut.Substring(lineBreak - 1).Trim();
			}
			else
			{
				firstline = stringtoCut.Substring(0, lineBreak);
				rest = stringtoCut.Substring(lineBreak).Trim();
            }
            return firstline + "\n" + CutStringToSize(rest);
        }
        else
			// Line is already short, nothing to do
            return stringtoCut;
    }
	
	/// <summary>
	/// Counts the number of lines in a given string
	/// </summary>
	/// <returns>Number of the lines</returns>
	/// <param name='stringtoCount'>What string to count</param>
    public int CountLines(string stringtoCount)
    {
		/*
        int lines = 0;
        for (int i = 0; i < stringtoCount.Length; ++i)
        {
            if (stringtoCount[i] == '\n')
                lines++;
        }
        return lines;
		*/
		return stringtoCount.Split ('\n').Length - 1;
    }
}