using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using VirtualClinic.WebSite.Serializable;

public enum StepReturnType { Unifinished, PlayAudio, SaveAndQuit, Continue, Save };

[System.Serializable]
public class StepReturn
{
    public StepReturnType Type;
    public SerializableDictionary<long, bool> Selections;
    public string AudioURL;
    public float Score;

    public StepReturn()
    {
        Type = StepReturnType.Unifinished;
        Selections = null;
        AudioURL = null;
        Score = 0;
    }

    public StepReturn(string URL)
    {
        Type = StepReturnType.PlayAudio;
        Selections = null;
        AudioURL = URL;
        Score = 0;
    }

    public StepReturn(StepReturnType type, SerializableDictionary<long, bool> selections, float score)
    {
        Type = type;
        Selections = selections;
        Score = score;
    }
}