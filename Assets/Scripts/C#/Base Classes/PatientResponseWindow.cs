using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

public class PatientResponseWindow : InterviewWindow
{
	#region Private Data Holder
	public Dictionary<long, InterviewQuestion> Questions;
	// float responseHeight;
	// float currentResponseHeight;
	// float targetResponseHeight = 0;

	public string AudioURL;
	public string ResponseText = "";
	public float shadowOffset = 1f;

	private PlayerQuestionWindow questionWindowRef;
	private GUIStyle patientTextStyle;
	private Color patientTextColor;
	private GUIStyle patientBackgroundStyle;
	private Color patientBackgroundColor;
	private string parsedResponseText;
	private float responseBarHeight;
	private Rect responseBar;
	private Rect[] responseBarShadow;
	private long currentQuestionID = 0;
	private float currentAlpha = 0.0f;
	private float targetAlpha = 0.0f;
	private float backgroundMaxAlpha = 0.60f;
	private float alphaTransitionSpeed = 5.0f;
	private float alphaThreshold = 0.15f;
	#endregion

	#region Object Constructor
	/// <summary>
	/// Initializes the response window
	/// </summary>
	/// <param name='styles'>What style to use for this window</param>
	/// <param name='questions'>List of questions</param>
	public PatientResponseWindow(InterviewStyleCollection styles, Dictionary<long, InterviewQuestion> questions, PlayerQuestionWindow questionWindow)
	{
		// Buffer attributes
		patientTextStyle = styles.GetStyle(InterviewStyle.ResponseText2);
		patientTextColor = patientTextStyle.normal.textColor;
		patientBackgroundStyle = new GUIStyle(patientTextStyle);
		patientBackgroundColor = new Color(0f, 0f, 0f, backgroundMaxAlpha);
		patientBackgroundStyle.normal.textColor = patientBackgroundColor;
		patientBackgroundStyle.hover.textColor = patientBackgroundColor;
		Questions = questions;
		questionWindowRef = questionWindow;

		// Initialize values
		// InitializeGUI ();
	}

	/// <summary>
	/// Initializes the GUI objects
	/// </summary>
	public void InitializeGUI()
	{
		// Calculate the necessity response header, fit to font width, assume font width = fontSize * 2
		int characterCount = (Screen.width - patientTextStyle.margin.left - patientTextStyle.margin.right -
			patientTextStyle.padding.left - patientTextStyle.padding.right) / patientTextStyle.fontSize * 2;
		lineoffset = patientTextStyle.fontSize + 10;

		// Test for response text
		if ((ResponseText == null) || (ResponseText.Trim().Equals(string.Empty)) || (ResponseText.Trim().Equals("...")))
			ResponseText = "...<i>[No Response]</i>...";
		Global.ToDebug(this, "Loading text onto response window: " + ResponseText, LogMessageType.DisplayMessage);

		// Calculate the response header
		parsedResponseText = TextParser.CutStringToSize(ResponseText, characterCount);
		responseBarHeight = (lineoffset * TextParser.CountLines(parsedResponseText) + 10);
		responseBar = new Rect(0, questionWindowRef.GetCurrentTop - responseBarHeight, Screen.width, responseBarHeight);
		responseBarShadow = new Rect[] { new Rect(responseBar), new Rect(responseBar), new Rect(responseBar), new Rect(responseBar) };
		for (int i = 0; i < responseBarShadow.Length; ++i)
		{
			responseBarShadow[i].x += (i % 2 == 0 ? shadowOffset : -shadowOffset);
			responseBarShadow[i].y += (i / 2 >= 1 ? shadowOffset : -shadowOffset);
		}
		targetAlpha = 1.0f;
	}
	#endregion

	#region Frame Update
	/// <summary>
	/// Draw the response window
	/// </summary>
	/// <param name='questionID'>Which question to draw response for</param>
	/// <param name='resize'>Whether the object has been resized since the call</param>
	public string Draw(long questionID, bool resize)
	{
		// Simple verification
		if (resize)
			InitializeGUI();
		AudioURL = null;

		// Check for bottom
		if ((currentAlpha < alphaThreshold) && (currentQuestionID > 0))
		{
			// Bottom hit, something new to display, show it
			ResponseText = Questions[currentQuestionID].Response.Text;
			AudioURL = Questions[currentQuestionID].Response.VoiceAudioUrl;
			InitializeGUI();
		}

		// Detect change
		if ((questionID != 0) && (questionID != currentQuestionID))
		{
			// Update buffer
			currentQuestionID = questionID;

			// Set to fade out
			targetAlpha = 0.0f;
		}
		else if (questionID == -1)
		{
			// End
			targetAlpha = 0.0f;
		}

		// Advance alpha
		patientTextColor.a = currentAlpha = iTween.FloatUpdate(currentAlpha, targetAlpha, alphaTransitionSpeed);
		patientBackgroundColor.a = currentAlpha * backgroundMaxAlpha;
		patientBackgroundStyle.hover.textColor = patientBackgroundStyle.normal.textColor = patientBackgroundColor;
		patientTextStyle.hover.textColor = patientTextStyle.normal.textColor = patientTextColor;
		// Draw shadow
		foreach (Rect shadow in responseBarShadow)
			GUI.Label(shadow, parsedResponseText, patientBackgroundStyle);
		// Draw the text on top
		GUI.Label(responseBar, parsedResponseText, patientTextStyle);

		return AudioURL;
	}
	#endregion

	#region Public Interface Method
	/*
	/// <summary>
	/// Draws multi-media window, no longer being used
	/// </summary>
	/// <param name='ID'>Multi-media ID</param>
	public void MultimediaWin(int ID)
	{
		foreach (string file in Questions[CurrentlySelected].Response.Multimedia)
		{
			GUILayout.Label(file);
		}
	}
	*/
	#endregion
}

/*********************************************************
******************** CODE GRAVEYARD **********************

	/// <summary>
	/// Draw the response window
	/// </summary>
	/// <param name='questionID'>Which question to draw response for</param>
	/// <param name='resize'>Whether the object has been resized since the call</param>
	public string Draw(long questionID, bool resize)
	{
		// Simple verification
		if (resize)
			InitializeGUI();
		AudioURL = null;

		// Detect change
		if ((questionID != 0) && (questionID != currentQuestionID))
		{
			// Update buffer
			currentQuestionID = questionID;

			// Selection has changed, alter
			try
			{
				ResponseText = Questions[currentQuestionID].Response.Text;
				AudioURL = Questions[currentQuestionID].Response.VoiceAudioUrl;
				InitializeGUI();
			}
			catch (Exception error)
			{
				Global.ToDebug(this, "Error in question fetching.  Message: " + (error.Message ?? "Unknown Error"), LogMessageType.Exception);
			}

			// Set to fade out
		}

		// Draw shadow
		foreach (Rect shadow in responseBarShadow)
			GUI.Label(shadow, parsedResponseText, patientBackgroundStyle);
		// Draw the text on top
		GUI.Label(responseBar, parsedResponseText, patientTextStyle);

		return AudioURL;
	}
 * 
	/// <summary>
	/// Draw the response window
	/// </summary>
	/// <param name='questionID'>Which question to draw response for</param>
	/// <param name='resize'>Whether the object has been resized since the call</param>
	public string Draw(long questionID, bool resize)
	{
		if (resize)
			InitializeGUI ();
			
		// ^o^
		AudioURL = null;
		// Once bar have been moved out of view...
		if(currentResponseHeight < startResponseHeight + 0.9f)
		{
			// Assume going out of view until proven otherwise
			targetResponseHeight = 0;
			
			// If there are something to display, set it up for display
			if(CurrentlySelected != 0)
			{
				ResponseText = Questions[CurrentlySelected].Response.Text;
				// ResponseText = CutStringToSize(ResponseText);
				InitializeGUI (); // Format the response text
				
				AudioURL = Questions[CurrentlySelected].Response.VoiceAudioUrl;
			}
		}
		// Something has been povided
		if(questionID != 0)
		{
			// Something new, move bar out of view
			if(questionID != CurrentlySelected)
			{
				targetResponseHeight = -responseBar.height; // = (0 - lineoffset * 2);
			}
			// Move onto next section, withdraw the bar and no longer need it
			if(questionID == -1)
			{
				targetResponseHeight = -responseBar.height; // = (0 - lineoffset * 2);
				CurrentlySelected = 0;
			}
			else
			{
				// Update currently selected item
				CurrentlySelected = questionID;
			}
		}
		// Something selected, move bar into view
		if(CurrentlySelected != 0)
		{

			// lineoffset = patientTextStyle.fontSize + 10;
			currentResponseHeight = iTween.FloatUpdate(currentResponseHeight, targetResponseHeight, 3f);
	
			// GUI.Button( new Rect(0,currentResponseHeight,Screen.width,lineoffset * 2 + lineoffset * CountLines(ResponseText) ),ResponseText,patientTextStyle);
			responseBar.y = currentResponseHeight;
			GUI.Button(responseBar, parsedResponseText, patientTextStyle);
		}
		
		// // Multi-media component no longer used
		// if(Questions[CurrentlySelected].Response.Multimedia != null && Questions[CurrentlySelected].Response.Multimedia.Length > 0)
		// {
		// 	MultimediaWindow.Draw(Questions[CurrentlySelected].Response.Multimedia);
		// }

		return AudioURL;
	}

	public PatientResponseWindow(GUIStyle patienttextstyle,Dictionary<int,InterviewQuestion>  questions )
	{
		patientTextStyle = patienttextstyle;
		Questions = questions;
		lineoffset = patientTextStyle.fontSize + 10;
		startResponseHeight = 0 - lineoffset * 2;
		currentResponseHeight = startResponseHeight;		
	}



**********************************************************/
