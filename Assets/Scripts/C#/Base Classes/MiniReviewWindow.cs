using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using VirtualClinic.WebSite.Serializable;

public class MiniReviewWindow : InterviewWindow
{
	#region Public Config Variables
	public string Name;
	public float targetReviewHeight = Screen.height - 775;
	public string Comments = "none";
	#endregion
	
	#region Private Data Holder Variables
	// float currentReviewHeight = -775;
	// int buttonoffset = 673;
	// float TextWidth = 630;
	// float TempTextHeight = 0;
	// float totalTextHeight = 0;
	GUIStyle ReviewWindowStyle;
	GUIStyle ReviewTextStyle;
	GUIStyle correct;
	GUIStyle missed;
	GUIStyle incorrect;
	GUIStyle ReviewButtonStyle;
	public Dictionary<long, InterviewQuestion> Questions;
	public List<long> SelectedQuestions;
	Vector2 scrollViewVector = Vector2.zero;
	// float MissedQuestionsStartHeight;
	// float IncorectQuestionsStartHeight;
	Texture2D CorrectIcon;
	Texture2D MissedIcon;
	Texture2D IncorrectIcon;
	Texture2D NextIcon;
	// StepNames StepName;
	
	private GUIStyle headerStyle;
	
	private Rect container;
	private string headerText;	
	#endregion
	
	#region Object Constructor
	/// <summary>
	/// Initialize the review window
	/// </summary>
	/// <param name='name'>Name of the review window</param>
	/// <param name='styles'>Display style for interview review</param>
	/// <param name='questions'>List of questions</param>
	/// <param name='selectedquestions'>List of selected questions</param>
	public MiniReviewWindow(string name, InterviewStyleCollection styles, Dictionary<long, InterviewQuestion> questions, List<long> selectedquestions)
	{
		Name = name;
		Questions = questions;
		SelectedQuestions = selectedquestions;

		ReviewWindowStyle = styles.GetStyle(InterviewStyle.ReviewWindow);
		ReviewTextStyle = styles.GetStyle(InterviewStyle.ReviewText);
		ReviewButtonStyle = styles.GetStyle(InterviewStyle.ReviewButton);
		correct = styles.GetStyle(InterviewStyle.ReviewCorrect);
		missed = styles.GetStyle(InterviewStyle.ReviewMissed);
		incorrect = styles.GetStyle(InterviewStyle.ReviewIncorrect);
		lineoffset = ReviewTextStyle.fontSize + 10;
		CorrectIcon = styles.GetIcon(InterviewIcon.Correct);
		MissedIcon = styles.GetIcon(InterviewIcon.Missed);
		IncorrectIcon = styles.GetIcon(InterviewIcon.Incorrect);
		NextIcon = styles.GetIcon(InterviewIcon.Next);
		MaxNumOfChars = 80;
		
		// Construct a style for the header by stripping its background
		headerStyle = new GUIStyle (ReviewWindowStyle);
		headerStyle.normal.background = null;
		headerStyle.hover.background = null;
		headerStyle.padding = new RectOffset (8, 8, 5, 5);
		headerStyle.margin = new RectOffset ();
		
		// Initialize GUI elements
		InitializeGUI ();
	}
	
	/// <summary>
	/// Initializes the GUI objects
	/// </summary>
	public void InitializeGUI ()
	{
		// ^o^
		// Calculate the main container size
		Vector2 containerSize = new Vector2 (Mathf.Min (ReviewWindowStyle.normal.background.width, Screen.width - ReviewWindowStyle.margin.left - ReviewWindowStyle.margin.right), 
			Mathf.Min (ReviewWindowStyle.normal.background.height, Screen.height - ReviewWindowStyle.margin.top - ReviewWindowStyle.margin.bottom));
		container = new Rect ((Screen.width - containerSize.x) / 2, - containerSize.y, containerSize.x, containerSize.y);
		targetReviewHeight = (Screen.height - containerSize.y) / 2;
		
		headerText = "Step Review: " + Name;
	}
	#endregion
	
	#region Public Interface Method
	/// <summary>
	/// Whether or not this interview step has any questions answered
	/// </summary>
	/// <returns>True if it has questions answered</returns>
	public bool AnsweredAnyCorrect()
	{
		foreach (int questionID in SelectedQuestions)
		{
			if (Questions[questionID].IsCorrectToAsk)
				return true;
		}
		return false;
	}

	/// <summary>
	/// Whether or not this interview step has any incorrect questions
	/// </summary>
	/// <returns>True if it has incorrect questions</returns>
	public bool AnsweredAnyIncorrect()
	{
		foreach (int questionID in SelectedQuestions)
		{
			if (Questions[questionID].IsCorrectToAsk == false)
				return true;
		}
		return false;
	}
	
	/// <summary>
	/// Whether or not this interview step has any missed questions
	/// </summary>
	/// <returns>True if it has missed questions</returns>
	public bool MissedAnyCorrect()
	{
		foreach (InterviewQuestion question in Questions.Values)
		{
			if (question.IsCorrectToAsk && SelectedQuestions.Contains(question.ID) == false)
				return true;
		}
		return false;
	}
	
	/// <summary>
	/// Assign a comment to this review
	/// </summary>
	/// <param name='comment'>What comment to assign</param>
	public void AddComment(string comment)
	{
		Comments = comment;
	}
	#endregion
	
	#region Frame Update
	/// <summary>
	/// Draw the review window
	/// </summary>
	/// <param name='score'>Gained score</param>
	/// <param name='resize'>Whether the object has been resized since the call</param>
	public int Draw(float score, bool resize)
	{
		if (resize)
			InitializeGUI ();
		
		// Use iTween to draw sliding animation
		int returnFlag = 0;
		container.y = iTween.FloatUpdate(container.y, targetReviewHeight, 3f);
		GUILayout.BeginArea (container, ReviewWindowStyle);
		
		// Draw header
		GUILayout.Label(headerText, headerStyle);
		GUILayout.Space (10.0f);
		
		// Draw body
		scrollViewVector = GUILayout.BeginScrollView (scrollViewVector);
		GUILayout.BeginVertical ();
		{
			// Draw correct questions
			if (AnsweredAnyCorrect())
			{
				// Draw header, center it
				GUILayout.BeginHorizontal();
				GUILayout.FlexibleSpace ();				
				GUILayout.Label (new GUIContent ("Correctly Asked Questions", CorrectIcon), ReviewTextStyle);
				GUILayout.FlexibleSpace ();
				GUILayout.EndHorizontal();
				
				// Check for each question from selected questions
				foreach (long questionID in SelectedQuestions)
				{
					// Check to see if it's correct
					if (Questions[questionID].IsCorrectToAsk)
					{
						// Draw the correct question
						GUILayout.Label (Questions[questionID].Text, correct);
						// Draw the rationale, if having any
						if ((Questions[questionID].Rationale != null) && (Questions[questionID].Rationale != string.Empty))
							GUILayout.Label (Questions[questionID].Rationale, ReviewTextStyle);
					}
				}
				
				// Spacer
				GUILayout.Space (15.0f);
			}
			
			// Draw missed questions
			if (MissedAnyCorrect())
			{
				// Draw header, center it
				GUILayout.BeginHorizontal();
				GUILayout.FlexibleSpace ();				
				GUILayout.Label (new GUIContent ("Missed Questions", MissedIcon), ReviewTextStyle);
				GUILayout.FlexibleSpace ();
				GUILayout.EndHorizontal();
				
				// Check for all the questions and check to see which ones are missed
				foreach (InterviewQuestion question in Questions.Values)
				{
					// Check to see if it's correct
					if ((question.IsCorrectToAsk) && (!SelectedQuestions.Contains(question.ID)))
					{
						// Draw the correct question
						GUILayout.Label (question.Text, missed);
						// Draw the rationale, if having any
						if ((question.Rationale != null) && (question.Rationale != string.Empty))
							GUILayout.Label (question.Rationale, ReviewTextStyle);
					}
				}
				
				// Spacer
				GUILayout.Space (15.0f);
			}
		
			// Draw incorrect questions
			if (AnsweredAnyIncorrect())
			{
				// Draw header, center it
				GUILayout.BeginHorizontal();
				GUILayout.FlexibleSpace ();				
				GUILayout.Label (new GUIContent ("Incorrectly Asked Questions", IncorrectIcon), ReviewTextStyle);
				GUILayout.FlexibleSpace ();
				GUILayout.EndHorizontal();
				
				// Check for all the questions and check to see which ones are incorrect
				foreach (InterviewQuestion question in Questions.Values)
				{
					// Check to see if it's correct
					if ((!question.IsCorrectToAsk) && (SelectedQuestions.Contains(question.ID)))
					{
						// Draw the correct question
						GUILayout.Label (question.Text, incorrect);
						// Draw the rationale, if having any
						if ((question.Rationale != null) && (question.Rationale != string.Empty))
							GUILayout.Label (question.Rationale, ReviewTextStyle);
					}
				}
				
				// Spacer
				GUILayout.Space (15.0f);
			}
		}
		GUILayout.EndVertical ();		
		GUILayout.EndScrollView ();
		
		GUILayout.FlexibleSpace ();				
		
		// Draw the save and continue button
		GUILayout.BeginHorizontal();
		// Disabled until debug ^o^							
		GUI.enabled = false;
		if (GUILayout.Button ("Save and continue later", ReviewButtonStyle, GUILayout.ExpandHeight (true)))
		{				
			Global.ToDebug (this, "Save and Quit was seleced by player", LogMessageType.DisplayMessage);
			returnFlag = 2;
		}
		GUI.enabled = true;
		GUILayout.FlexibleSpace ();				
		if (GUILayout.Button(new GUIContent ("Continue on to next Step", NextIcon), ReviewButtonStyle, GUILayout.ExpandHeight (true)))
		{
			Global.ToDebug (this, "Continue to next step.  Current step: " + this.Name, LogMessageType.DisplayMessage);
			returnFlag = 1;
		}
		GUILayout.EndHorizontal();		
		GUILayout.EndArea ();
		
		// Return selected flag
		return returnFlag;
		
		/*
		// Slide into view
		currentReviewHeight = iTween.FloatUpdate(currentReviewHeight, targetReviewHeight, 3f);
		TempTextHeight = 0;
		
		// Header
		GUI.Label(new Rect(150, currentReviewHeight, 690, 750), "Step " + Name + " Review", ReviewWindowStyle);
		
		// Body content
		scrollViewVector = GUI.BeginScrollView(new Rect(180, currentReviewHeight + 55, TextWidth, 600), scrollViewVector, new Rect(0, 0, TextWidth - 20, totalTextHeight));
		int i = 3;
		
		// Draw correct questions ^o^
		if (AnsweredAnyCorrect())
		{
			GUI.Label(new Rect(0, 20, TextWidth, lineoffset + lineoffset), "Correctly Asked Questions", ReviewTextStyle);
			GUI.Label(new Rect(170, 0, 48, 48), CorrectIcon);

			foreach (long questionID in SelectedQuestions)
			{
				if (Questions[questionID].IsCorrectToAsk)
				{
					string questionText = CutStringToSize(Questions[questionID].Text);
					string RationaleText = CutStringToSize(Questions[questionID].Rationale);
					int additionalLines = CountLines(questionText);
					int additionalLines2 = CountLines(RationaleText);

					GUI.Label(new Rect(0, (lineoffset * i) - lineoffset, TextWidth, lineoffset + lineoffset + lineoffset * additionalLines), questionText, correct);
					GUI.Label(new Rect(0, (lineoffset * i) + (additionalLines * lineoffset), TextWidth, lineoffset + lineoffset + lineoffset * additionalLines2), RationaleText, ReviewTextStyle);
					i += 2;
					i += additionalLines + additionalLines2;

					TempTextHeight += (2 * lineoffset) + additionalLines * lineoffset + additionalLines2 * lineoffset;
				}
			}
			TempTextHeight += lineoffset * 3;
			if (TempTextHeight > totalTextHeight)
			{
				totalTextHeight = TempTextHeight;
			}
		}
		
		// Draw missed questions
		if (MissedAnyCorrect())
		{
			MissedQuestionsStartHeight = TempTextHeight;
			GUI.Label(new Rect(0, MissedQuestionsStartHeight + 20, TextWidth, lineoffset + lineoffset), "Missed Questions", ReviewTextStyle);
			GUI.Label(new Rect(200, MissedQuestionsStartHeight, 48, 48), MissedIcon);
			i = 3;

			foreach (InterviewQuestion question in Questions.Values)
			{
				if (question.IsCorrectToAsk && SelectedQuestions.Contains(question.ID) == false)
				{
					string questionText = CutStringToSize(question.Text);
					string RationaleText = CutStringToSize(question.Rationale);
					int additionalLines = CountLines(questionText);
					int additionalLines2 = CountLines(RationaleText);
					GUI.Label(new Rect(0, MissedQuestionsStartHeight + (lineoffset * i) - lineoffset, TextWidth, lineoffset + lineoffset + lineoffset * additionalLines), questionText, missed);
					GUI.Label(new Rect(0, MissedQuestionsStartHeight + (lineoffset * i) + (additionalLines * lineoffset), TextWidth, lineoffset + lineoffset + lineoffset * additionalLines2), RationaleText, ReviewTextStyle);
					i += 2;
					i += additionalLines + additionalLines2;
					TempTextHeight += (2 * lineoffset) + additionalLines * lineoffset + additionalLines2 * lineoffset;
				}
			}

			TempTextHeight += lineoffset * 3;
			if (TempTextHeight > totalTextHeight)
			{
				totalTextHeight = TempTextHeight;
			}
		}
		
		// Draw incorrect questions
		if (AnsweredAnyIncorrect())
		{
			IncorectQuestionsStartHeight = TempTextHeight;
			GUI.Label(new Rect(0, IncorectQuestionsStartHeight + 20, TextWidth, lineoffset + lineoffset), "Incorrectly Asked Questions", ReviewTextStyle);
			GUI.Label(new Rect(160, IncorectQuestionsStartHeight, 48, 48), IncorrectIcon);
			i = 3;

			foreach (InterviewQuestion question in Questions.Values)
			{
				if (question.IsCorrectToAsk == false && SelectedQuestions.Contains(question.ID))
				{

					string questionText = CutStringToSize(question.Text);
					string RationaleText = CutStringToSize(question.Rationale);
					int additionalLines = CountLines(questionText);
					int additionalLines2 = CountLines(RationaleText);
					GUI.Label(new Rect(0, IncorectQuestionsStartHeight + (lineoffset * i) - lineoffset, TextWidth, lineoffset + lineoffset + lineoffset * additionalLines), questionText, incorrect);
					GUI.Label(new Rect(0, IncorectQuestionsStartHeight + (lineoffset * i) + (additionalLines * lineoffset), TextWidth, lineoffset + lineoffset + lineoffset * additionalLines2), RationaleText, ReviewTextStyle);
					i += 2;
					i += additionalLines + additionalLines2;
					TempTextHeight += (2 * lineoffset) + additionalLines * lineoffset + additionalLines2 * lineoffset;
				}
			}
			TempTextHeight += lineoffset * 2;
			if (TempTextHeight > totalTextHeight)
			{
				totalTextHeight = TempTextHeight;
			}
		}
		
		// Score and comment display
		TempTextHeight += lineoffset * 2;
		GUI.Label(new Rect(0, TempTextHeight, TextWidth, lineoffset + lineoffset), "Step Score: " + score.ToString("f2") + "%", ReviewTextStyle);
		TempTextHeight += lineoffset * 2;

		GUI.Label(new Rect(0, TempTextHeight, TextWidth, lineoffset + lineoffset), "Preceptor's Comments", ReviewTextStyle);
		TempTextHeight += lineoffset;

		string commentText = CutStringToSize(Comments);

		int additionalLines3 = CountLines(commentText);
		GUI.Label(new Rect(0, TempTextHeight, TextWidth, lineoffset + lineoffset + lineoffset * additionalLines3), commentText, ReviewTextStyle);
		TempTextHeight += (2 * lineoffset) + additionalLines3 * lineoffset;

		if (TempTextHeight > totalTextHeight)
		{
			totalTextHeight = TempTextHeight;
		}
		GUI.EndScrollView();
		
		// Draw the proceed button
		if (StepName == StepNames.Plan)
		{
			// If final step, draw button to end interview.  This shouldn't be used by now as the case structure is now dynamic
			if (GUI.Button(new Rect(180 + 160, currentReviewHeight + buttonoffset, 310, 50), "Finish Interview", ReviewButtonStyle))
			{
				return 1;
			}
		}
		else
		{
			// Save and continue button
			if (GUI.Button(new Rect(180, currentReviewHeight + buttonoffset, 310, 50), "Save and continue later", ReviewButtonStyle))
			{				
				DebugConsole.Log("Save and Quit was seleced by player1");
				return 2;
			}
			
			// Proceed to next step button
			if (GUI.Button(new Rect(180 + 320, currentReviewHeight + buttonoffset, 310, 50), "Continue on to next Step", ReviewButtonStyle))
			{
				return 1;
			}
			GUI.Label(new Rect(180 + 320 + 270, currentReviewHeight + buttonoffset + 10, 32, 32), NextIcon);
		}
		
		// Nothing has been selected
		return 0;
		*/
	}
	#endregion
}

/*********************************************************
******************** CODE GRAVEYARD **********************
	//old constructor
	public MiniReviewWindow(StepNames step,Dictionary<int,InterviewQuestion> questions,List<int> selectedquestions,  GUIStyle minireviewwindow,GUIStyle minireviewtext,GUIStyle minireviewcorrect,GUIStyle minireviewmissed,GUIStyle minireviewincorrect,GUIStyle minireviewbutton,Texture2D correcticon,Texture2D missedicon,Texture2D incorrecticon, Texture2D nexticon )
	{
		StepName = step;
		Questions = questions;
		SelectedQuestions = selectedquestions;
		ReviewWindowStyle = minireviewwindow;
		ReviewTextStyle = minireviewtext;
		ReviewButtonStyle = minireviewbutton;
		correct = minireviewcorrect;
		missed = minireviewmissed;
		incorrect = minireviewincorrect;
		lineoffset = ReviewTextStyle.fontSize + 10;
		CorrectIcon = correcticon;
		MissedIcon = missedicon;
		IncorrectIcon = incorrecticon;
		NextIcon = nexticon;
	}

**********************************************************/