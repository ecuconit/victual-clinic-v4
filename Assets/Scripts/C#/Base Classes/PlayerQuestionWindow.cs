using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using VirtualClinic.WebSite.Serializable;

[System.Serializable]
public abstract class PlayerQuestionWindow : InterviewWindow 
{
	public int StepIndex;
	public string Name;

	protected float currentPlayerDialogHeight = 0;
	protected float targetPlayerDialogHeight;
	protected float ScrollViewHeight;
	// protected float ScrollViewOffset;
	// protected float ScrollViewLineOffset;
	protected int MaxQuestionInView;
	
	// protected GUIStyle DialogBoxStyle;
	// protected GUIStyle InventoryHeaderStyle;
	// protected GUIStyle PlayerDialogTextStyle;
	public Dictionary<long,InterviewQuestion> Questions;
	public SerializableDictionary<long,bool> AllSelections = null;
	public List<long> SelectedAnswers; 
	
	public Texture2D NextIcon;
	
	protected Vector2 scrollViewVector = Vector2.zero;
	// protected Vector2 InventoryscrollVector = Vector2.zero;
	// protected StepNames StepName;
	// protected InventoryManager Inventory;
	// protected int totalAdditionalLines;
	public StepOptions Options;
	
	protected GUIStyle boxStyle;
	protected GUIStyle headerStyle;
	protected GUIStyle questionStyle;
	protected string titleString = string.Empty;
	protected Rect questionBox;
	protected SelectionReturn defaultAnswer = new SelectionReturn ();

	#region Public Getter
	/// <summary>
	/// Gets the current top of the display block of the question window
	/// </summary>
	/// <value>Distance between top of the screen and top of the question window</value>
	public float GetCurrentTop
	{
		get { return Screen.height - ScrollViewHeight; }
	}
	#endregion
	
	#region Object Initialization
	/// <summary>
	/// Base object constructor
	/// </summary>
	/// <param name='index'>Step index</param>
	/// <param name='name'>Name of the question window</param>
	/// <param name='options'>Display option</param>
	/// <param name='styles'>Collection of styles to be used</param>
	/// <param name='maxquestions'>Maximum number of questions to display in view</param>
	/// <param name='questions'>List of questions to display</param>
	/// <param name='answers'>List of selected answer in the step</param>
	/// <param name='inventory'>Reference to inventory manager</param>
	public PlayerQuestionWindow(int index, string name, StepOptions options,InterviewStyleCollection styles,int maxquestions ,Dictionary<long,InterviewQuestion>  questions,List<long> answers, InventoryManager inventory)
	{
		StepIndex = index;
		Name = name;
		Options = options;
		// DialogBoxStyle = styles.GetStyle(InterviewStyle.DialogBox);
		// PlayerDialogTextStyle = styles.GetStyle(InterviewStyle.PlayerText);
		Questions = questions;
		SelectedAnswers = answers;
		// lineoffset = PlayerDialogTextStyle.fontSize + 10;
		// targetPlayerDialogHeight = (Questions.Count * lineoffset) + lineoffset;
		NextIcon = styles.GetIcon(InterviewIcon.Next);
		MaxQuestionInView = maxquestions; 
		// ScrollViewHeight = lineoffset*MaxQuestionInView;
		// ScrollViewOffset = 0;
		// ScrollViewLineOffset = 0;
		// Inventory = inventory;
		
		// InventoryHeaderStyle = new GUIStyle(DialogBoxStyle);
		// InventoryHeaderStyle.padding.left = 10;
		// InventoryHeaderStyle.padding.top = 0;
		// InventoryHeaderStyle.normal.background = null;

		// MaxNumOfChars = 85;
		// totalAdditionalLines = 0;
		
		boxStyle = new GUIStyle (styles.GetStyle(InterviewStyle.DialogBox));		
		headerStyle = new GUIStyle (styles.GetStyle(InterviewStyle.DialogBox));
		questionStyle = new GUIStyle (styles.GetStyle(InterviewStyle.PlayerText));
		headerStyle.normal.background = null;
		headerStyle.margin = questionStyle.margin;
		headerStyle.padding = questionStyle.padding;	
		
		// Initialize the GUI elements
		currentPlayerDialogHeight = Screen.height;
		targetPlayerDialogHeight = 0;
		InitializeGUI ();
	}
	
	/// <summary>
	/// Initialize the questions
	/// </summary>
	public void initQuestions()
	{
		if(AllSelections == null)
		{
			AllSelections = new SerializableDictionary<long,bool>();
			foreach(InterviewQuestion question in Questions.Values)
			{
				AllSelections.Add(question.Response.ID,false);
			}
		}
		InitializeGUI ();
	}	
	
	#region Initialize GUI Positions
	/// <summary>
	/// Initializes the GUI element positions
	/// </summary>
	public void InitializeGUI ()
	{
		// Construct title string for display
		titleString = Name;
		if (Options.MaxSelectedQuestions == 0 && Options.MinSelectedQuestions == 0)
		{
			// Select any amount
			titleString += " - Choose all that apply";
		}
		else if (Options.MinSelectedQuestions == Options.MaxSelectedQuestions)
		{
			// Choose exactly x number of item(s)
			titleString += " - Choose the " + Options.MaxSelectedQuestions + "selections that most apply";
		}
		else if (Options.MinSelectedQuestions > 0)
		{
			// Select at least certain number
			titleString += " - Choose at least " + Options.MinSelectedQuestions;
			if (Options.MaxSelectedQuestions > 0)
				titleString += " but no more than " + Options.MaxSelectedQuestions;
			titleString += " selections that most apply";
		}
		else
			// Has max but no min
			titleString += " - Choose no more than " + Options.MaxSelectedQuestions + " slections that apply";
		
		// ^o^, No longer needed, to be removed later
		// MaxNumOfChars = 85;
		// MaxNumOfChars = (Screen.width - questionStyle.margin.left - questionStyle.margin.right - 
		//	questionStyle.padding.left - questionStyle.padding.right) / questionStyle.fontSize * 2;
		
		// Set scroll view height
		lineoffset = (questionStyle.fontSize * 1.5f) + questionStyle.padding.top + questionStyle.padding.bottom; // + questionStyle.margin.top + questionStyle.margin.bottom;
		int lineCount = (int)(Mathf.Min (MaxQuestionInView, Questions.Count) + 2.25f);
		ScrollViewHeight = lineoffset * lineCount + (lineCount - 1) * questionStyle.margin.top +
			boxStyle.margin.top + boxStyle.margin.bottom + boxStyle.padding.top + boxStyle.padding.bottom - questionStyle.margin.top - questionStyle.margin.bottom;
		// currentPlayerDialogHeight = ScrollViewHeight;
		// targetPlayerDialogHeight = 0;
		questionBox = new Rect(0, currentPlayerDialogHeight, Screen.width, ScrollViewHeight);
		
		MaxNumOfChars = (int) (questionBox.width - boxStyle.padding.left - boxStyle.padding.right) / questionStyle.fontSize * 2;
	}
	#endregion
	#endregion

	#region Frame Update
	/// <summary>
	/// Draw the receptionist dialog
	/// </summary>
	/// <param name='resize'>Whether the GUI has been resized or not</param>
	public SelectionReturn Draw()
	{
		SelectionReturn selection = null;
		
		/*
		// lineoffset = PlayerDialogTextStyle.fontSize + 10;
		if(targetPlayerDialogHeight > 0)
		{
			targetPlayerDialogHeight = Mathf.Min(((CountTotalLines() * lineoffset) + lineoffset*3) ,((MaxQuestionInView * lineoffset) + lineoffset*2 + 4));
		}
		currentPlayerDialogHeight = iTween.FloatUpdate(currentPlayerDialogHeight, targetPlayerDialogHeight, 2.0f);
		*/
		currentPlayerDialogHeight = iTween.FloatUpdate(currentPlayerDialogHeight, targetPlayerDialogHeight, 2.0f);
		
		/*
		string title = Name;
		if(Options.MaxSelectedQuestions == 0 && Options.MinSelectedQuestions == 0)
		{
			title += " - Choose all that apply";
		}
		else
		if(Options.MinSelectedQuestions == Options.MaxSelectedQuestions)
		{
			title += " - Choose the " + Options.MaxSelectedQuestions + "selections that most apply";
		}
		else
		if(Options.MinSelectedQuestions > 0)
		{
			title += " - Choose at least " + Options.MinSelectedQuestions;
			if(Options.MaxSelectedQuestions > 0)
			{
				title += " but no more than " + Options.MaxSelectedQuestions;
			}
			title += " selections that most apply";
		}
		else
		{
			title += " - Choose no more than " + Options.MaxSelectedQuestions + " slections that apply";
		}
		GUI.Label( new Rect(0,Screen.height-currentPlayerDialogHeight +lineoffset/2,Screen.width,Mathf.Max( (Mathf.Min(((CountTotalLines() * lineoffset) + lineoffset*3) ,((MaxQuestionInView * lineoffset) + lineoffset*2+4))) ,lineoffset*6)),title,DialogBoxStyle);
		*/
		
		// Draw the interview questions if there isn't already one
		if(selection == null)
			selection = DrawQuestions(currentPlayerDialogHeight);
		
		// Create the return type based on the selections made
		if(selection == null)
			return defaultAnswer;
		else
			return selection;
	}
	
	#region Helper Methods
	/// <summary>
	/// Counts the total lines for each question
	/// </summary>
	/// <returns>Total number of lines</returns>
	protected int CountTotalLines()
	{
		int lines = 0;
		foreach (InterviewQuestion question in Questions.Values)
		{
			string questionText = CutStringToSize("   " + question.Text);
			int additionalLines = CountLines(questionText);
			lines += additionalLines;
			++lines;
		}
		return lines;
	}
	#endregion	
	
	#region GUI Element Helper
	/*
	/// <summary>
	/// Draws the inventory icon - No longer used
	/// </summary>
	/// <returns>Return flag</returns>
	/// <param name='currentHeight'>Current GUI element height index</param>	
	protected SelectionReturn DrawInventory(float currentHeight)
	{
		GUI.Label( new Rect(800,Screen.height-currentHeight + lineoffset ,160,lineoffset),"Check Reports", InventoryHeaderStyle);
		InventoryscrollVector = GUI.BeginScrollView (new Rect (800, Screen.height-currentHeight + lineoffset*2, 190, Mathf.Min( ScrollViewHeight,(lineoffset*Questions.Count)) ), InventoryscrollVector,new Rect (0, 0, 160, (lineoffset*Inventory.Items.Count)));				
		int i = 0; bool resetquestions = true;
		foreach(InventoryItem item in Inventory.Items)
		{
			if(GUI.Button(new Rect(0,(lineoffset*i),Screen.width,lineoffset) ,item.ItemName,PlayerDialogTextStyle))
			{
				
				item.IsSelected = !item.IsSelected;
			}
			if(item.IsSelected)
			{
				targetPlayerDialogHeight = -1;
				resetquestions = false;
			}
			++i;
		}	
		GUI.EndScrollView();
		if(resetquestions && targetPlayerDialogHeight < 0)
		{
			targetPlayerDialogHeight = Mathf.Min(((Questions.Count * lineoffset) + lineoffset*3) ,((MaxQuestionInView * lineoffset) + lineoffset*3));
		}
		return null;	
	}
	*/
		
	/// <summary>
	/// Draws the questions, no longer being used, each child object should have their own
	/// </summary>
	/// <returns>Return flag</returns>
	/// <param name='currentHeight'>Current GUI element height index</param>
	protected abstract SelectionReturn DrawQuestions(float currentHeight);
	/*
	protected virtual SelectionReturn DrawQuestions(float currentHeight)
	{
		DebugConsole.LogError("You should no longer be using a PlayerQuestionWindow class directly and instead using a class the inherits from it and overrieds the drawquestions function");
		return null;
	}
	*/	
	
	/*
	/// <summary>
	/// Generic draws the button to move onto next step
	/// </summary>
	/// <returns>Whether or not the button has been pressed</returns>
	/// <param name='currentHeight'>Height index</param>
	protected bool DrawMovetoNextStep(float currentHeight)
	{
		
		if(GUI.Button( new Rect(20,Screen.height-currentHeight + ((Mathf.Min(((Questions.Count * lineoffset) + lineoffset*2) ,((MaxQuestionInView * lineoffset) + lineoffset*2))) ),Screen.width-50,lineoffset),"	  Move on to next section",PlayerDialogTextStyle))
		{
			targetPlayerDialogHeight = 0;
			return true;
		}
		GUI.Label( new Rect(30,Screen.height-currentHeight +( (Mathf.Min(((Questions.Count * lineoffset) + (lineoffset*2)-2) ,((MaxQuestionInView * lineoffset) + (lineoffset*2)-2))) ),32,32),NextIcon);	
		return false;
	}	
	
	/// <summary>
	/// Generic draws the button to move onto next step
	/// </summary>
	/// <returns>Whether or not the button has been pressed</returns>
	/// <param name='currentHeight'>Height index</param>
	/// <param name='Lines'>Number of lines</param>
	protected bool DrawMovetoNextStep2(float currentHeight, int Lines)
	{
					//new Rect(0,(lineoffset*i),Screen.width-50,lineoffset + lineoffset*additionalLines),questionText,PlayerDialogTextStyle))
					//(Questions.Count * lineoffset) + (lineoffset)
		if(GUI.Button( new Rect(0,(Lines * lineoffset),Screen.width-50,lineoffset),"	  Move on to next section",PlayerDialogTextStyle))
		{
			targetPlayerDialogHeight = 0;
			return true;
		}
		GUI.Label( new Rect(12,(Lines * lineoffset)-2,32,32),NextIcon);	
		return false;
	}
	*/
		
	/// <summary>
	/// Draws the button to move to next step
	/// </summary>
	/// <returns>Whether the button is pressed or not</returns>
	protected bool DrawMovetoNextStep3 ()
	{
		if (GUILayout.Button (new GUIContent ("Move on to next section", NextIcon), questionStyle))
		{
			targetPlayerDialogHeight = 0;
			return true;
		}
		return false;
	}
	#endregion
	#endregion
}

/*********************************************************
******************** CODE GRAVEYARD **********************
	//old Constructor
	public PlayerQuestionWindow(GUIStyle boxstyle, GUIStyle playertextstyle,Dictionary<int,InterviewQuestion>  questions,List<long> answers, Texture2D nexticon, StepNames stepname, InventoryManager inventory, int MaxQuestions )
	{
		DialogBoxStyle = boxstyle;
		PlayerDialogTextStyle = playertextstyle;
		Questions = questions;
		SelectedAnswers = answers;
		lineoffset = PlayerDialogTextStyle.fontSize + 10;
		targetPlayerDialogHeight = (Questions.Count * lineoffset) + lineoffset;
		NextIcon = nexticon;
		MaxQuestionInView = MaxQuestions; 
		ScrollViewHeight = lineoffset*MaxQuestionInView;
		ScrollViewOffset = 0;
		ScrollViewLineOffset = 0;
		StepName = stepname;
		Inventory = inventory;
		
		InventoryHeaderStyle = new GUIStyle(DialogBoxStyle);
		InventoryHeaderStyle.padding.left = 10;
		InventoryHeaderStyle.padding.top = 0;
		InventoryHeaderStyle.normal.background = null;

		totalAdditionalLines = 0;
	}
	
**********************************************************/