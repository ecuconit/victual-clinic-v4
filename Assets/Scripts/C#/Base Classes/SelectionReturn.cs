using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using VirtualClinic.WebSite.Serializable;

public enum SelectionReturnType { NoSelection, AnswerSelected, SelectionChanged, StepFinished, InventorySelected };

[System.Serializable]
public class SelectionReturn
{
    public SerializableDictionary<long, bool> AllSelections;
    public long SelectedAnswer;
    public SelectionReturnType SelectionType;
    public QuestionResponseDisplayType? ResponseDisplayType;

    public SelectionReturn()
    {
        AllSelections = null;
        SelectedAnswer = 0;
        SelectionType = SelectionReturnType.NoSelection;
        ResponseDisplayType = null;
    }

    public SelectionReturn(long selectionid, QuestionResponseDisplayType? response)
    {
        AllSelections = null;
        SelectedAnswer = selectionid;
        SelectionType = SelectionReturnType.AnswerSelected;
        ResponseDisplayType = response;
    }

    public SelectionReturn(long selectionid, bool change, QuestionResponseDisplayType? response)
    {
        AllSelections = null;
        SelectedAnswer = selectionid;
        SelectionType = SelectionReturnType.SelectionChanged;
        ResponseDisplayType = response;
    }

    public SelectionReturn(SelectionReturnType selectiontype, long selectionid, QuestionResponseDisplayType? response)
    {
        AllSelections = null;
        SelectedAnswer = selectionid;
        SelectionType = selectiontype;
        ResponseDisplayType = response;
    }

    public SelectionReturn(SerializableDictionary<long, bool> allselections)
    {
        AllSelections = allselections;
        SelectedAnswer = -1;
        SelectionType = SelectionReturnType.StepFinished;
        ResponseDisplayType = QuestionResponseDisplayType.NoResponse;
    }
}