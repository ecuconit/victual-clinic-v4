using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public enum InterviewIcon
{
    Next = 0,
    Correct = 1,
    Missed = 2,
    Incorrect = 3
}

public enum InterviewStyle
{
    DialogBox = 0,
	PlayerText = 1,
	ResponseText = 2,
	ResponseText2 = 3,
	ReviewWindow = 4,
	ReviewText = 5,
	ReviewCorrect = 6,
	ReviewMissed = 7,
	ReviewIncorrect = 8,
	ReviewButton = 9,
	CheckBox = 10
}

[System.Serializable]
public class InterviewStyleCollection
{
    public List<Texture2D> Icons;
    public List<GUIStyle> Styles;

    public Texture2D GetIcon(InterviewIcon icon)
    {
        if ((int)icon < Icons.Count)
            return Icons[(int)icon];
        else
            Global.ToDebug(this, "One of the Required Interview Icons are Missing. Check the GUIStuff values of Interview Controller and make them match up with the enum in InterviewStyleCollection script", LogMessageType.VerificationError);
        return new Texture2D(64, 64);
    }

    public GUIStyle GetStyle(InterviewStyle style)
    {
        if ((int)style < Styles.Count)
            return Styles[(int)style];
        else
			Global.ToDebug(this, "One of the Required Interview Styles are Missing. Check the GUIStuff values of Interview Controller and make them match up with the enum in InterviewStyleCollection script", LogMessageType.VerificationError);
        return new GUIStyle();
    }
}