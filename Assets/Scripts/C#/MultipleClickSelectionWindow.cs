using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using VirtualClinic.WebSite.Serializable;

[System.Serializable]
public class MultipleClickSelectionWindow : PlayerQuestionWindow
{
    GUIStyle CheckBox;

    public MultipleClickSelectionWindow(int index, string name, StepOptions options, InterviewStyleCollection styles, int maxquestions, Dictionary<long, InterviewQuestion> questions, List<long> answers, InventoryManager inventory)
        : base(index, name, options, styles, maxquestions, questions, answers, inventory)
    {
        CheckBox = styles.GetStyle(InterviewStyle.CheckBox);
    }

    /// <summary>
	/// Draws the question window
	/// </summary>
	/// <returns>Selected item</returns>
	/// <param name='currentHeight'>Current display height</param>
    protected override SelectionReturn DrawQuestions(float currentHeight)
    {
		// Perform transition
		SelectionReturn returnFlag = null;
		questionBox.y = Screen.height - questionBox.height + currentHeight;
		
		// Draws the box
		GUILayout.BeginArea (questionBox, boxStyle);		
		
		// Draw title bar
		GUILayout.Label(titleString, headerStyle);
		GUILayout.Space (lineoffset * 0.25f);
		
		// Draw body
		scrollViewVector = GUILayout.BeginScrollView (scrollViewVector);
		GUILayout.BeginVertical ();
		{
			// Draw each button
			foreach (InterviewQuestion question in Questions.Values)
			{
				// if (GUILayout.Toggle (AllSelections[question.Response.ID], question.Text, CheckBox))
				if (GUILayout.Button (new GUIContent (question.Text, (AllSelections[question.Response.ID] ? CheckBox.onNormal.background : CheckBox.normal.background)), questionStyle))
				{
					AllSelections[question.Response.ID] = !AllSelections[question.Response.ID];
                	returnFlag = new SelectionReturn(question.ID, AllSelections[question.Response.ID], question.ResponseDisplayType);
				}
			}
			
			// Draw the okay button
			if (DrawMovetoNextStep3 ())
			{
				// Create return flag and retract the window
				returnFlag = new SelectionReturn(AllSelections);
				targetPlayerDialogHeight = ScrollViewHeight;
			}
		}
		GUILayout.EndVertical ();
		GUILayout.EndScrollView ();
		GUILayout.EndArea ();
		
		return returnFlag;
		
		/*
		ScrollViewLineOffset = 0;
        scrollViewVector = GUI.BeginScrollView(new Rect(20, Screen.height - currentHeight + lineoffset * 2, Screen.width - 55, ScrollViewHeight), 
			scrollViewVector, new Rect(0, 0, 400, (lineoffset * Questions.Count) + ScrollViewOffset));//(lineoffset*Questions.Count) + ScrollViewOffset)		
        int i = 0;

        foreach (InterviewQuestion question in Questions.Values)
        {
            string questionText = CutStringToSize("   " + question.Text);
            int additionalLines = CountLines(questionText);
            if (GUI.Button(new Rect(0, (lineoffset * i), Screen.width - 50, lineoffset + lineoffset * additionalLines), questionText, PlayerDialogTextStyle))
            {
                AllSelections[question.Response.ID] = !AllSelections[question.Response.ID];
                return new SelectionReturn(question.ID, AllSelections[question.Response.ID], question.ResponseDisplayType);
            }
            GUI.Toggle(new Rect(0, (lineoffset * i) + 1.5f, 10, 10), AllSelections[question.Response.ID], "", CheckBox);
            ScrollViewLineOffset += lineoffset * additionalLines;
            i += additionalLines;
            ++i;
        }

        totalAdditionalLines = i;

        if (DrawMovetoNextStep2(currentPlayerDialogHeight, totalAdditionalLines))
        {
            return new SelectionReturn(AllSelections);
        }
        ScrollViewLineOffset += lineoffset;
        GUI.EndScrollView();
        if (ScrollViewLineOffset > ScrollViewOffset)
        {
            ScrollViewOffset = ScrollViewLineOffset;
        }
        return null;
        */
	}
}