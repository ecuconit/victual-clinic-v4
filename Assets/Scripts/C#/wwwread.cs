using UnityEngine;
using System;
using System.Collections;
using System.IO;
using System.Text;
using System.Xml.Serialization;
using System.Collections.Generic;
using VirtualClinic.WebSite.Serializable;

public class wwwread : MonoBehaviour
{
    InterviewController interviewGUI;
    ComputerScreenInterface computerscreenGUI;

    public DiseaseCase Case;
    public DiseaseCase TempCase;
    public DiseaseCaseResponse TempCaseResponse;
    public DiseaseCaseResponse PlayerResponse;
    WWW XMLCaseFile;
    WWW XMLResponseUpload;

    public bool UseFullAddress = false;
    public bool FirstCaseLoaded = false;

    public string ServerAddress = "https://150.216.217.218:442";
    public string GetDiseaseCaseServerCommand = "/API/GetDiseaseCase?id=";
    public string GetCaseResponseServerCommand = "/API/GetDiseaseCaseResponse?id=";
    public int PreSelectedCaseNumber;
    public bool UseDebugTestCases = false;
    public List<int> DebugTestCases;
    List<int> AvailableCasesIDS;
    public DiseaseCase[] AvailableCases;

    string AvailableCasesString;

    // int fakeQuestionID = 1;

    void Awake()
    {
        computerscreenGUI = GetComponent<ComputerScreenInterface>();
        interviewGUI = GetComponent<InterviewController>();
        if (UseDebugTestCases)
        {
            AvailableCasesIDS = DebugTestCases;
        }
        else
        {
            AvailableCasesIDS = new List<int>();
            PreSelectedCaseNumber = CheckForPreSelectedCase();
            if (PreSelectedCaseNumber != -1)
            {
                AvailableCasesIDS.Add(PreSelectedCaseNumber);
            }
            StartCoroutine(CheckServerForMoreAvailableCases());
        }
    }

    public IEnumerator LoadAvaiableCases()
    {
        for (int id = 0; id < AvailableCasesIDS.Count; ++id)
        {
            TempCase = new DiseaseCase();
            TempCaseResponse = new DiseaseCaseResponse();
            yield return StartCoroutine(RetriveDataFromServer(GetDiseaseCaseServerCommand, AvailableCasesIDS[id]));
            yield return StartCoroutine(RetriveDataFromServer(GetCaseResponseServerCommand, AvailableCasesIDS[id]));
            computerscreenGUI.LoadCaseCallBack(TempCase, TempCaseResponse, PreSelectedCaseNumber);

            if (id == 0)
            {
                FirstCaseLoaded = true;
            }
        }

        //	if(PreSelectedCaseNumber != -1)
        //	{
        //		Case = AvailableCases[0];
        //	}

        computerscreenGUI.AllCasesLoaded = true;
    }

    IEnumerator RetriveDataFromServer(string ServerCommand, int casenumber)
    {
        // DebugConsole.Log("trying to load case " + casenumber + " from web");
		Global.ToDebug (this, "Trying to load case " + casenumber + " from web", LogMessageType.DisplayMessage);
        if (UseFullAddress)
            XMLCaseFile = new WWW(ServerAddress + ServerCommand + casenumber);
        else
            XMLCaseFile = new WWW(ServerCommand + casenumber);
        yield return XMLCaseFile;

        MemoryStream memStream = new MemoryStream();

        memStream.Write(XMLCaseFile.bytes, 0, XMLCaseFile.bytes.Length);
        memStream.Flush();
        memStream.Seek(0, SeekOrigin.Begin);


        if (ServerCommand == GetDiseaseCaseServerCommand)
        {
            XmlSerializer serializer = new XmlSerializer(typeof(DiseaseCase));
            TempCase = (DiseaseCase)serializer.Deserialize(memStream);
        }
        else
        {
            if (ServerCommand == GetCaseResponseServerCommand)
            {
                XmlSerializer serializer = new XmlSerializer(typeof(DiseaseCaseResponse));
                TempCaseResponse = (DiseaseCaseResponse)serializer.Deserialize(memStream);
            }
        }
    }

    public IEnumerator InitializePlayerResponse()
    {
        // DebugConsole.Log("Calling InitializePlayerResponse on the server for case " + Case.ID);
        long CaseNumber = Case.ID;
		
		Global.ToDebug (this, "Calling InitializePlayerResponse on the server for case " + CaseNumber, LogMessageType.DisplayMessage);
        if (UseFullAddress)
            XMLCaseFile = new WWW(ServerAddress + "/API/InitializeResponseForDiseaseCase/" + CaseNumber);
        else
            XMLCaseFile = new WWW("/API/InitializeResponseForDiseaseCase/" + CaseNumber);
        yield return XMLCaseFile;

        MemoryStream memStream = new MemoryStream();

        memStream.Write(XMLCaseFile.bytes, 0, XMLCaseFile.bytes.Length);
        memStream.Flush();
        memStream.Seek(0, SeekOrigin.Begin);

        XmlSerializer serializer = new XmlSerializer(typeof(DiseaseCaseResponse));
        PlayerResponse = (DiseaseCaseResponse)serializer.Deserialize(memStream);
    }

    public IEnumerator UploadPlayerResponse()
    {
        // DebugConsole.Log("Uploading Selections For Step to Server");
        System.Xml.Serialization.XmlSerializer SerializedResponse;
		Global.ToDebug (this, "Prepare to upload selections to server.", LogMessageType.DisplayMessage);

        using (MemoryStream ms = new MemoryStream())
        {
            System.Xml.XmlTextWriter xWriter = new System.Xml.XmlTextWriter(ms, Encoding.UTF8);

            SerializedResponse = new System.Xml.Serialization.XmlSerializer(PlayerResponse.GetType());
            SerializedResponse.Serialize(xWriter, PlayerResponse);
            xWriter.Flush();

            xWriter.Close();
            ms.Close();

            try
            {
				Global.ToDebug (this, "Uploading Selections For Step to Server", LogMessageType.DisplayMessage);        
                if (UseFullAddress)
                {
                    XMLResponseUpload = new WWW(ServerAddress + "/API/ContinueToSaveDiseaseCaseResponse", ms.ToArray());
                }
                else
                {
                    XMLResponseUpload = new WWW("/API/ContinueToSaveDiseaseCaseResponse", ms.ToArray());
                }
            }
            catch (Exception error)
            {
                Global.ToDebug (this, "Saving error: " + error.ToString(), LogMessageType.Exception);
                // DebugConsole.Log("Saving error: " + error.ToString(), DebugConsole.MessageTypes.Error);
            }

            yield return XMLResponseUpload;
        }
    }

    public IEnumerator SaveFinalScore(decimal finalScore)
    {
        // DebugConsole.Log("Saving final score to server");
		Global.ToDebug (this, "Preparing to save final score to server.");

        System.Xml.Serialization.XmlSerializer SerializedResponse;

        using (MemoryStream ms = new MemoryStream())
        {
            System.Xml.XmlTextWriter xWriter = new System.Xml.XmlTextWriter(ms, Encoding.UTF8);

            SerializedResponse = new System.Xml.Serialization.XmlSerializer(PlayerResponse.GetType());
            SerializedResponse.Serialize(xWriter, PlayerResponse);
            xWriter.Flush();

            xWriter.Close();
            ms.Close();
			
			Global.ToDebug (this, "Saving final score to server", LogMessageType.DisplayMessage);
            if (UseFullAddress)
            {
                XMLResponseUpload = new WWW(ServerAddress + "/API/ContinueToSaveDiseaseCaseResponse?finalScore=" + finalScore, ms.ToArray());
                Global.ToDebug(this, "Savinvg to: " + ServerAddress + "/API/ContinueToSaveDiseaseCaseResponse?finalScore=" + finalScore, LogMessageType.DisplayMessage);
            }
            else
            {
                XMLResponseUpload = new WWW("/API/ContinueToSaveDiseaseCaseResponse?finalScore=" + finalScore, ms.ToArray());
                Global.ToDebug (this, "Savinvg to: " + ServerAddress + "/API/ContinueToSaveDiseaseCaseResponse?finalScore=" + finalScore, LogMessageType.DisplayMessage);
            }
            yield return XMLResponseUpload;
        }
    }

    public void LoadCaseToGUI()
    {
        interviewGUI.Reset();
        for (int i = 0; i < Case.Steps.Length; ++i)
        {
            LoadStepToGUI(i);
        }
    }

    private void LoadStepToGUI(int stepnumber)
    {
        InterviewStep Step = Case.Steps[stepnumber];

        Global.ToDebug (this, "Loading Step " + Step.StepName, LogMessageType.DisplayMessage);
        interviewGUI.AddStep(stepnumber, Step.StepName.ToString());

        Global.ToDebug (this, "Setting Options for Step " + Step.StepName, LogMessageType.DisplayMessage);
        interviewGUI.SetUpStep(stepnumber, new StepOptions(Step.MaxSelectedQuestions, Step.MaxSelectedQuestions, Step.QuestionSelectionType));

        Global.ToDebug (this, "Adding Questions for Step " + Step.StepName, LogMessageType.DisplayMessage);
        foreach (Question q in Step.Questions)
        {
            InterviewQuestion TempQuestion = new InterviewQuestion();
            InterviewResponse TempResponse = new InterviewResponse();

            TempQuestion.ID = q.ID;
            TempQuestion.Text = q.Text ?? TempQuestion.Text;
            TempQuestion.IsCorrectToAsk = q.IsCorrectToAsk;
            TempQuestion.Rationale = q.Rationale ?? TempQuestion.Rationale;
            TempQuestion.ResponseDisplayType = q.ResponseDisplayType;

            TempResponse.ID = q.Answer.ID;
            TempResponse.Text = q.Answer.Text ?? TempResponse.Text;
            TempResponse.HasVoiceAudio = q.Answer.NeedToGenerateVoiceForText;
            TempResponse.VoiceAudioUrl = q.Answer.TextVoiceMP3Url ?? TempResponse.VoiceAudioUrl;
            TempResponse.Multimedia = q.Answer.MultimediaFiles ?? TempResponse.Multimedia;

            if (q.Answer.MultimediaFiles != null)
            {
                foreach (string file in q.Answer.MultimediaFiles)
                {
                    Debug.Log(file);
                }
            }

            TempQuestion.Response = TempResponse;

            interviewGUI.AddQuestion(stepnumber, TempQuestion);
        }

        interviewGUI.InitStep(stepnumber);

        if (Step.PreceptorComments != null)
        {
            if (Step.PreceptorComments.VoiceText != null)
                interviewGUI.AddComment(stepnumber, Step.PreceptorComments.VoiceText);
        }
    }

    int CheckForPreSelectedCase()
    {
        if (Application.srcValue.Length > 0)
        {
            int IDPosition = Application.srcValue.IndexOf("=");
            string PreSelectedId = Application.srcValue.Substring(IDPosition + 1);
            return System.Convert.ToInt32(PreSelectedId);
        }
        else
        {
            if (UseDebugTestCases)
            {
                return DebugTestCases[0];
            }
            return -1;
        }

    }

    IEnumerator CheckServerForMoreAvailableCases()
    {
        WWW AvailableCasesServerResponse;
        // DebugConsole.Log("Trying to check server for all Available Cases");
		Global.ToDebug (this, "Trying to check server for all Available Cases", LogMessageType.DisplayMessage);
        if (UseFullAddress)
		{
			Global.ToDebug (this, "Contacting: " + ServerAddress + "/API/GetAssignedDiseaseCases", LogMessageType.DisplayMessage);
			AvailableCasesServerResponse = new WWW(ServerAddress + "/API/GetAssignedDiseaseCases");
		}
        else
		{
			Global.ToDebug (this, "Contacting: http://localhost/API/GetAssignedDiseaseCases", LogMessageType.DisplayMessage);
			AvailableCasesServerResponse = new WWW("/API/GetAssignedDiseaseCases");
		}
        yield return AvailableCasesServerResponse;

        AvailableCasesString = AvailableCasesServerResponse.text;
		Global.ToDebug (this, "Check for availability of [" + PreSelectedCaseNumber + "] in [" + AvailableCasesString + "]", LogMessageType.DisplayMessage);

        if (AvailableCasesString.Length > 0)
        {
			Global.ToDebug (this, "Cases found, parsing for match from: " + AvailableCasesString, LogMessageType.DisplayMessage);
			string[] CaseIDStrings = AvailableCasesString.Split(' ');
            foreach (string id in CaseIDStrings)
            {
                int ID = System.Convert.ToInt32(id);
                if (ID != PreSelectedCaseNumber)
                {
                    AvailableCasesIDS.Add(ID);
                }
            }
        }
		else
			Global.ToDebug (this, "No available case, please contact the system administrator about case releases.", LogMessageType.DisplayMessage);

		if ((AvailableCasesIDS == null) || (AvailableCasesIDS.Count <= 0))
			Global.ToDebug (this, "Current case [" + PreSelectedCaseNumber + "] is not a part of released case, please contact your professor for more information.", LogMessageType.DisplayMessage);
    }

    //	void OnGUI()
    //	{
    //		GUILayout.Label(Case.ReceptionistStep.Text1);
    //	}
}

/*********************************************************
******************** CODE GRAVEYARD **********************


**********************************************************/