using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using VirtualClinic.WebSite.Serializable;

public class ComputerScreenInterface : MonoBehaviour
{
	#region Public Config Variables
	// public Texture2D BackGround;
	// public Texture2D HeaderButtonBackGround;
	public Texture2D DefaultMale;
	public Texture2D DefaultFemale;
	public GUIStyle HeaderButtonStyle = new GUIStyle ();

	public bool AllCasesLoaded = false;
	public bool AllowRestart = true;
	public Texture2D SelectCaseBackGround;
	public Texture2D SelectCaseEvenBack;
	public Texture2D SelectCaseHoverBack;
	public Texture2D SelectCaseSelectedBack;
	public GUIStyle CaseSelectText = new GUIStyle ();
	public GUIStyle HealthHistoryText = new GUIStyle ();
	public GUIStyle VitalSignsText = new GUIStyle ();
	public GUIStyle VitalSignsText2 = new GUIStyle ();
	public GUIStyle VitalSignsInfo = new GUIStyle ();
	public GUIStyle VitalSignsHeading = new GUIStyle ();
	public GUIStyle StartInterviewButton = new GUIStyle ();
	public GUIStyle ContInterviewButton = new GUIStyle ();
	public GUIStyle RestartInterviewButton = new GUIStyle ();
	public GUIStyle VirticalScrollBarStyle = new GUIStyle ();
	public GUIStyle CaseSelectEven;
	public GUIStyle CaseSelectOdd;
	public GUIStyle CaseRoot;
	public List<DiseaseCase> AvailableCases;
	public List<DiseaseCaseResponse> CaseResponses;
	public int Selectedtab;
	public GUIStyle backgroundContainer;
	public int spacerSpace = 2;
	#endregion
	
	#region Private Data Holder Variables
	wwwread CaseLoader;
	// float SelectCaselineoffset;
	// float HealthHistoryoffset;
	// float HealhistoryScrollOffset = 0;
	// float Registrationoffset;
	// float RegistrationScrollOffset = 0;
	
	// GUIStyle CaseSelectEven;
	// GUIStyle CaseSelectOdd;
	GUIStyle CaseSelectSelectedBack;
	GUIStyle CaseSelectSelectedText;
	// Vector2 HealthHistoryScroll = Vector2.zero;
	// Vector2 RegistrationScroll = Vector2.zero;

	// string[] HeaderButtonNames = { "Case Info", "Registration", "Health History", "		   " }; // Last page no longer used
	string[] HeaderButtonNames = { "Case Info", "Registration", "Health History"};
	string[] RegistrationItems = { "Name", "SSN", "BirthDate", "Home Phone", "Address", "City", "State", "Zip", "Employer", "Occupation", "Business Adress", "Business Phone", "Insurance Company", "Contract Number", "Group Number", "Subscriber Number", "Referrer", "Emergancy Contact", "Emergancy Phone", };	
	private Rect backgroundPosition;
	private Rect toolBarPosition;
	private Rect bodyContainer;
	private Rect innerContainer;
	private List<string> registrationItems;
	private List<string> registrationValues;
	private int registrationColumnWidth = 300;
	private Vector2 bodyScrollView;
	private int healthHistoryColumnWidth = 300;	
	#endregion
	
	#region Object Constructor
	/// <summary>
	/// Start this instance
	/// </summary>
	void Start ()
	{
		Selectedtab = 0;
		AvailableCases = new List<DiseaseCase> ();
		CaseResponses = new List<DiseaseCaseResponse> ();
		CaseLoader = GetComponent<wwwread> ();
		StartCoroutine (CaseLoader.LoadAvaiableCases ());

		// SelectCaselineoffset = CaseSelectText.fontSize + 10;
		// HealthHistoryoffset = HealthHistoryText.fontSize + 10;
		// Registrationoffset = SelectCaselineoffset + 4;
		
		/*
		CaseSelectEven = new GUIStyle(CaseSelectText);
		CaseSelectEven.normal.background = SelectCaseEvenBack;
		CaseSelectEven.hover.background = SelectCaseHoverBack;
		CaseSelectEven.fontSize = 20;
		CaseSelectEven.alignment = TextAnchor.MiddleLeft;

		CaseSelectOdd = new GUIStyle(CaseSelectText);
		CaseSelectOdd.hover.background = SelectCaseHoverBack;
		CaseSelectOdd.fontSize = 20;
		CaseSelectOdd.alignment = TextAnchor.MiddleLeft;
		*/
		
		CaseSelectSelectedBack = new GUIStyle (CaseSelectText);
		CaseSelectSelectedBack.normal.background = SelectCaseSelectedBack;
		CaseSelectSelectedBack.hover.background = SelectCaseSelectedBack;

		CaseSelectSelectedText = new GUIStyle (CaseSelectText);
		CaseSelectSelectedText.normal.textColor = Color.white;
	}
	
	/// <summary>
	/// Loads the case call back data
	/// </summary>
	/// <param name='loadedcase'>Which case was loaded</param>
	/// <param name='loadedResponse'>Which response was loaded</param>
	/// <param name='PreSelected'>Preselected case ID</param>
	public void LoadCaseCallBack (DiseaseCase loadedcase, DiseaseCaseResponse loadedResponse, int PreSelected)
	{
		DebugConsole.Log ("Loaded Case: " + loadedcase.Name + " to case selection page. Status: " + loadedResponse.Status);

		AvailableCases.Add (loadedcase);
		CaseResponses.Add (loadedResponse);

		if (loadedcase.ID == PreSelected) {
			DebugConsole.Log ("Case Was PreSelected By Website BackEnd");
			CaseLoader.Case = loadedcase;
			CaseLoader.PlayerResponse = loadedResponse;
			Selectedtab = 0;
		}
		
		// Initialize GUI elements
		InitializeGUI ();
	}
	
	#region Initialize GUI Positions
	/// <summary>
	/// Initializes the GUI element positions
	/// </summary>
	private void InitializeGUI ()
	{
		// Calculate background and toolbar containers
		// backgroundPosition = new Rect(0, -3, Screen.width + 5, Screen.height + 5);
		backgroundPosition = new Rect (0, 0, Screen.width, Screen.height);
		// toolBarPosition = new Rect(200, -12, 800, 100);
		toolBarPosition = new Rect (100, 0, Screen.width - 100 * 2, HeaderButtonStyle.onNormal.background.height * ((float)Screen.height / (float)backgroundContainer.normal.background.height));
		
		// Draw Preceptor containers
		float containerTop = toolBarPosition.y + toolBarPosition.height + backgroundContainer.padding.top;
		bodyContainer = new Rect (backgroundContainer.padding.left, containerTop,
			Screen.width - backgroundContainer.padding.left - backgroundContainer.padding.right, Screen.height - containerTop - backgroundContainer.padding.bottom);
		innerContainer = new Rect (0, 0, bodyContainer.width, bodyContainer.height);
		
		// Pre-define registration items
		registrationItems = new List<string> ();
		registrationValues = new List<string> ();
		string spacer = new string (' ', spacerSpace);
		if ((CaseLoader != null) && (CaseLoader.Case != null)) {
			int max = int.MinValue;
			foreach (string item in RegistrationItems)
				max = Mathf.Max (max, item.Length);
			max += spacerSpace + 1;
			for (int i = 0; i < RegistrationItems.Length; ++i) {
				/*
				registrationItems.Add (RegistrationItems[i] + ":" + new string (' ', max - RegistrationItems[i].Length) + GetRegistrationItem (RegistrationItems[i]));
				*/
				registrationItems.Add (spacer + RegistrationItems [i] + ":");
				registrationValues.Add (GetRegistrationItem (RegistrationItems [i]));
			}
			registrationColumnWidth = (int)(max * CaseSelectEven.fontSize / 1.4f);
		}
		
		// Pre-define health history items
		healthHistoryColumnWidth = (int)(HealthHistoryHeaderCount (CaseLoader.Case.HealthHistoryItem.Children, 0) * CaseSelectEven.fontSize / 1.4f);		
	}
	#endregion
	#endregion

	#region Frame Update
	/// <summary>
	/// Generic interface draw method
	/// </summary>
	/// <returns>Vital signs selection return flag</returns>
	/// <param name='resize'>Screen resize signal</param>
	public int Draw (bool resize)
	{
		// Reinitialize the GUI elements on resize signal
		if (resize)
			InitializeGUI ();
		
		// Drawing the background and top toolbar
		int returnFlag = 0;
		DrawHeader ();
		
		GUILayout.BeginArea (bodyContainer);
		bodyScrollView = GUILayout.BeginScrollView (bodyScrollView);		
		switch (Selectedtab) {
		case 0:
				// Draw the first tab, case nfo (vital sign)
			returnFlag = DrawVitalSigns ();
			break;
		case 1:
				// Draw the registration form
			DrawRegistration ();
			break;
		case 2:
			DrawHealthHistory ();
			break;
		case 3:
				// No longer being used, case selection is moved to web portion
				// DrawSelectCase();
			break;
		}
		GUILayout.EndScrollView ();
		GUILayout.EndArea ();
		
		return returnFlag;
	}
	
	#region Header
	/// <summary>
	/// Draws the tab header
	/// </summary>
	void DrawHeader ()
	{
		//GUI.Label( new Rect(0,-3, 1030, 790),BackGround);
		
		// Draw the background for full screen
		// GUI.Label(new Rect(0, -3, Screen.width + 5, Screen.height + 5), BackGround);
		GUI.Label (backgroundPosition, "", backgroundContainer);
		
		/*
		// Unnecessary to draw secondary icon just for currently selected item, Toolbar.OnNormal does that for you 
		switch (Selectedtab)
		{
			case 0:
				GUI.Label(new Rect(205, -10, 200, 100), HeaderButtonBackGround);
				break;
			case 1:
				GUI.Label(new Rect(405, -10, 200, 100), HeaderButtonBackGround);
				break;
			case 2:
				GUI.Label(new Rect(607, -10, 210, 100), HeaderButtonBackGround);
				break;
			case 3:
				//GUI.Label(new Rect(805,-10, 200, 100), HeaderButtonBackGround);	
				break;
		}
		*/

		// Draw the selecion tab
		int oldSelectedTab = Selectedtab;
		Selectedtab = GUI.Toolbar (toolBarPosition, Selectedtab, HeaderButtonNames, HeaderButtonStyle);
		
		// Simple verification
		if ((Selectedtab < 0) || (Selectedtab > 2))
			Selectedtab = oldSelectedTab;
	}
	#endregion
	
	#region Page 1 - Case Info
	/// <summary>
	/// Draws the case info (vital signs) tab
	/// </summary>
	/// <returns>Tab index</returns>
	int DrawVitalSigns ()
	{
		// Load data from case
		int returnFlag = 0;
		Patient patient = CaseLoader.Case.Patient;
		VitaSign patientinfo = CaseLoader.Case.VitaSign;
		
		// Container
		GUILayout.BeginArea (innerContainer);
		GUILayout.BeginHorizontal ();
		GUILayout.FlexibleSpace (); // Left spacer
		
		GUILayout.BeginHorizontal ();
		{		
			GUILayout.FlexibleSpace (); // Left spacer
			
			// Left column, picture and such
			GUILayout.BeginVertical ();
			{
				GUILayout.FlexibleSpace (); // Top spacer
			
				// Patient portrait 
				if (patient.Sex == "Male")
					GUILayout.Label (DefaultMale, VitalSignsText2); // Use part 2 for center
				else
					GUILayout.Label (DefaultFemale, VitalSignsText2); // Use part 2 for center
				
				// Patient bio
				GUILayout.Label (patient.FirstName + " " + patient.LastName, VitalSignsText2);
				GUILayout.Label (patient.MartialStatus.ToString (), VitalSignsText2);
				GUILayout.Label (patient.Race.ToString (), VitalSignsText2);
				GUILayout.Label (patient.Sex.ToString (), VitalSignsText2);
				
				GUILayout.FlexibleSpace (); // Bottom spacer			
			}
			GUILayout.EndVertical ();
			
			GUILayout.Space (35.0f); // Middle spacer
			
			// Right column, detail and buttons
			GUILayout.BeginVertical (GUILayout.ExpandWidth (true), GUILayout.ExpandHeight (true));
			{
				GUILayout.FlexibleSpace (); // Top spacer
			
				// Title
				GUILayout.Label ("Vitals taken today:", VitalSignsHeading);
				
				GUILayout.Space (20.0f); // Title spacer
				
				// Innter column split
				GUILayout.BeginHorizontal ();
				{
					// Inner left column
					GUILayout.BeginVertical ();
					{
						// Draw label
						GUILayout.Label ("Height:", VitalSignsText);
						GUILayout.Label ("Weight:", VitalSignsText);
						GUILayout.Label ("BMI:", VitalSignsText);
						GUILayout.Label ("BPSystolic:", VitalSignsText);
						GUILayout.Label ("BPDiastolic:", VitalSignsText);
						GUILayout.Label ("Pulse:", VitalSignsText);
						GUILayout.Label ("Temperature:", VitalSignsText);
						GUILayout.Label ("Respiration:", VitalSignsText);
					}
					GUILayout.EndVertical ();
					
					GUILayout.Space (15.0f); // Middle spacer
					
					// Inner right column
					GUILayout.BeginVertical ();		
					{
						// Draw content
						GUILayout.Label (patientinfo.Height.ToString ("F") + "'", VitalSignsInfo);
						GUILayout.Label (patientinfo.Weight.ToString ("F") + " lb", VitalSignsInfo);
						GUILayout.Label (patientinfo.BMI.ToString ("F"), VitalSignsInfo);
						GUILayout.Label (patientinfo.BPSystolic.ToString () + " mm Hg", VitalSignsInfo);
						GUILayout.Label (patientinfo.BPDiastolic.ToString () + " mm Hg", VitalSignsInfo);
						GUILayout.Label (patientinfo.Pulse.ToString () + " BPM", VitalSignsInfo);
						GUILayout.Label (patientinfo.Temperature.ToString ("F") + " F", VitalSignsInfo);
						GUILayout.Label (patientinfo.Respiration.ToString () + " BPM", VitalSignsInfo);
					}
					GUILayout.EndVertical ();		
				}
				GUILayout.EndHorizontal ();
				
				// Draw the control buttons
				if (CaseLoader.PlayerResponse.Status == CaseResponseStatus.Unstarted) {
					GUILayout.BeginHorizontal ();	
					{
						GUILayout.FlexibleSpace (); // Left spacer
						if (GUILayout.Button ("", StartInterviewButton)) {
							Selectedtab = 0;
							returnFlag = 1;
						}
						GUILayout.FlexibleSpace (); // Right spacer
					}
					GUILayout.EndHorizontal ();
				} else if (CaseLoader.PlayerResponse.Status == CaseResponseStatus.Started) {
					if (AllowRestart) {
						GUILayout.BeginHorizontal ();	
						{
							GUILayout.FlexibleSpace (); // Left spacer
							// Disabled until debug ^o^
							GUI.enabled = false;
							if (GUILayout.Button ("", ContInterviewButton)) {
								Selectedtab = 0;
								returnFlag = 2;
							}
							GUI.enabled = true;
							GUILayout.FlexibleSpace (); // Right spacer
						}
						GUILayout.EndHorizontal ();
						
						GUILayout.BeginHorizontal ();	
						{
							GUILayout.FlexibleSpace (); // Left spacer
							if (GUILayout.Button ("", RestartInterviewButton)) {
								Selectedtab = 0;
								returnFlag = 1;
							}
							GUILayout.FlexibleSpace (); // Right spacer
						}
						GUILayout.EndHorizontal ();
					} else {
						GUILayout.BeginHorizontal ();	
						{							
							GUILayout.FlexibleSpace (); // Left spacer
							// Disabled until debug ^o^
							GUI.enabled = false;
							if (GUILayout.Button ("", ContInterviewButton)) {
								Selectedtab = 0;
								returnFlag = 2;
							}
							GUI.enabled = true;
							GUILayout.FlexibleSpace (); // Right spacer
						}
						GUILayout.EndHorizontal ();
					}
				}
				
				GUILayout.FlexibleSpace (); // Bottom spacer			
			}
			GUILayout.EndVertical ();
			
			GUILayout.FlexibleSpace (); // Right spacer			
		}
		GUILayout.EndHorizontal ();		

		GUILayout.FlexibleSpace (); // Right spacer
		GUILayout.EndHorizontal ();		
		GUILayout.EndArea ();
	
		/*
		GUI.Label(new Rect(450, 120, 500, 100), "Vitals taken today:", VitalSignsHeading);

		//GUI.Label(new Rect(400,250, 500, 30), 		 "ID: "+patientinfo.ID, VitalSignsText);
		GUI.Label(new Rect(500, 200 + 30, 200, 30), "Height: ", VitalSignsText);
		GUI.Label(new Rect(700, 200 + 30, 500, 30), "  " + patientinfo.Height, VitalSignsInfo);

		GUI.Label(new Rect(500, 200 + 30 * 2, 200, 30), "Weight: ", VitalSignsText);
		GUI.Label(new Rect(700, 200 + 30 * 2, 500, 30), "  " + patientinfo.Weight, VitalSignsInfo);

		GUI.Label(new Rect(500, 200 + 30 * 3, 200, 30), "BMI: ", VitalSignsText);
		GUI.Label(new Rect(700, 200 + 30 * 3, 500, 30), "  " + patientinfo.BMI, VitalSignsInfo);

		GUI.Label(new Rect(500, 200 + 30 * 4, 200, 30), "BPSystolic: ", VitalSignsText);
		GUI.Label(new Rect(700, 200 + 30 * 4, 500, 30), "  " + patientinfo.BPSystolic, VitalSignsInfo);

		GUI.Label(new Rect(500, 200 + 30 * 5, 200, 30), "BPDiastolic:  ", VitalSignsText);
		GUI.Label(new Rect(700, 200 + 30 * 5, 500, 30), "  " + patientinfo.BPDiastolic, VitalSignsInfo);

		GUI.Label(new Rect(500, 200 + 30 * 6, 200, 30), "Pulse: ", VitalSignsText);
		GUI.Label(new Rect(700, 200 + 30 * 6, 500, 30), "  " + patientinfo.Pulse, VitalSignsInfo);

		GUI.Label(new Rect(500, 200 + 30 * 7, 200, 30), "Temperature: ", VitalSignsText);
		GUI.Label(new Rect(700, 200 + 30 * 7, 500, 30), "  " + patientinfo.Temperature, VitalSignsInfo);

		GUI.Label(new Rect(500, 200 + 30 * 8, 200, 30), "Respiration: ", VitalSignsText);
		GUI.Label(new Rect(700, 200 + 30 * 8, 500, 30), "  " + patientinfo.Respiration, VitalSignsInfo);


		//----
		GUI.Label(new Rect(15, 480, 500, 35), patient.FirstName + " " + patient.LastName, VitalSignsText2);
		GUI.Label(new Rect(15, 480 + 35 * 2, 500, 35), "" + patient.MartialStatus, VitalSignsText2);
		GUI.Label(new Rect(15, 480 + 35, 500, 35), "" + patient.Race, VitalSignsText2);
		GUI.Label(new Rect(15, 480 + 35 * 3, 500, 35), "" + patient.Sex, VitalSignsText2);

		if (patient.Sex == "Male")
		{
			GUI.Label(new Rect(50, 200, 350, 350), DefaultMale, VitalSignsText);
		}
		else
		{
			GUI.Label(new Rect(50, 200, 350, 350), DefaultFemale, VitalSignsText);
		}

		switch (CaseLoader.PlayerResponse.Status)
		{
			case CaseResponseStatus.Unstarted:
				if (GUI.Button(new Rect(480, 550, 464, 166), "", StartInterviewButton))
				{
					Selectedtab = 0;
					return 1;
				}
				break;
			case CaseResponseStatus.Started:
				if (AllowRestart)
				{
					if (GUI.Button(new Rect(530, 480, 380, 124), "", ContInterviewButton))
					{
						Selectedtab = 0;
						return 2;
					}
					if (GUI.Button(new Rect(530, 610, 380, 124), "", RestartInterviewButton))
					{
						Selectedtab = 0;
						return 1;
					}
				}
				else
				{
					if (GUI.Button(new Rect(480, 550, 464, 166), "", ContInterviewButton))
					{
						Selectedtab = 0;
						return 2;
					}
				}
				break;
		}
		*/
			
		return returnFlag;
	}
	#endregion
	
	#region Page 2 - Registration
	/// <summary>
	/// Draws the registration tab
	/// </summary>
	void DrawRegistration ()
	{
		/*
		// First alternative, works, but not on things that are too long
		if (CaseLoader.Case != null)
		{
			/// Draw the labels
			GUILayout.BeginVertical ();
			for (int i = 0; i < registrationItems.Count; ++i)
				GUILayout.Label(registrationItems[i], (i % 2 == 0 ? CaseSelectEven : CaseSelectOdd));			
			GUILayout.EndVertical ();
		}
		*/
		
		// First alternative, works, but not on things that are too long
		if (CaseLoader.Case != null) {
			/// Draw the labels
			GUIStyle current;
			GUILayout.BeginVertical ();			
			for (int i = 0; i < registrationItems.Count; ++i) {
				current = ((i % 2 == 0) ? CaseSelectEven : CaseSelectOdd);
				
				GUILayout.BeginHorizontal ();
				GUILayout.Label (registrationItems [i], current, GUILayout.Width (registrationColumnWidth), GUILayout.ExpandHeight (true));
				GUILayout.Label (registrationValues [i], current);
				GUILayout.EndHorizontal ();
			}
			GUILayout.EndVertical ();
		}
		
		/*
		if (CaseLoader.Case != null)
		{
			RegistrationScroll = GUI.BeginScrollView(new Rect(15, 90, Screen.width - 30, Screen.height - 90), RegistrationScroll, new Rect(0, 0, Screen.width - 50, RegistrationScrollOffset));

			int i = 0;
			foreach (string item in RegistrationItems)
			{
				if (i % 2 == 0)
				{
					GUI.Label(new Rect(0, Registrationoffset * i, Screen.width, Registrationoffset), "", CaseSelectEven);
				}
				else
				{
					GUI.Label(new Rect(0, Registrationoffset * i, Screen.width, Registrationoffset), "", CaseSelectOdd);

				}
				GUI.Label(new Rect(0, Registrationoffset * i, 200, Registrationoffset), item + ":", CaseSelectText);
				DrawRegistrationItem(item, Registrationoffset * i);

				i++;
			}

			GUI.EndScrollView();
		}
		*/
	}
	
	/// <summary>
	/// Gets the registration item by name
	/// </summary>
	/// <returns>Patient registration data</returns>
	/// <param name='itemName'>Registration field name</param>
	private string GetRegistrationItem (string itemName)
	{
		// Load patient information
		Patient patient = CaseLoader.Case.Patient;
		string itemvalue = "";
		
		// Get item value from patient data
		switch (itemName) {
		case "Name":
			itemvalue = patient.FirstName + "  " + patient.Initial + "  " + patient.LastName;
			break;
		case "SSN":
			itemvalue = patient.SSN;
			break;
		case "BirthDate":
			itemvalue = patient.DOB.ToShortDateString ();
			break;
		case "Home Phone":
			itemvalue = patient.HomePhone;
			break;
		case "Address":
			itemvalue = patient.AddressLine1;
			break;
		case "City":
			itemvalue = patient.AddressCity;
			break;
		case "State":
			itemvalue = patient.AddressState;
			break;
		case "Zip":
			itemvalue = patient.AddressZip;
			break;
		case "Employer":
			itemvalue = (patient.Employer != null) ? patient.Employer.Name : "";
			break;
		case "Occupation":
			itemvalue = patient.Occupation;
			break;
		case "Business Adress":
			itemvalue = (patient.Employer != null) ? patient.Employer.AddressLine1 : "";
			break;
		case "Business Phone":
			itemvalue = patient.BusinessPhone;
			break;
		case "Insurance Company":
			{
				if (patient.InsurancePolicy != null) {
					if (patient.InsurancePolicy.Company != null) {
						itemvalue = patient.InsurancePolicy.Company.Name;
					} else
						itemvalue = "";
				} else
					itemvalue = "";
			}
			break;
		case "Contract Number":
			itemvalue = (patient.InsurancePolicy != null) ? ((patient.InsurancePolicy.Company != null) ? (patient.InsurancePolicy.Company.Phone) : "") : "";
			break;
		case "Group Number":
			itemvalue = patient.InsurancePolicy.GroupNo;
			break;
		case "Subscriber Number":
			itemvalue = patient.InsurancePolicy.ContractNo;
			break;
		case "Referrer":
			itemvalue = patient.EmergencyContact.FirstName + " " + patient.EmergencyContact.LastName;
			break;
		case "Emergancy Contact":
			itemvalue = patient.EmergencyContact.FirstName + " " + patient.EmergencyContact.LastName;
			break;
		case "Emergancy Phone":
			itemvalue = patient.EmergencyContact.Phone;
			break;
		}
		
		return itemvalue;
	}
	
	/*
	/// <summary>
	/// Draws each registration item
	/// </summary>
	/// <param name='ItemName'>Item to draw</param>
	/// <param name='offset'>x Offset</param>
	void DrawRegistrationItem(string ItemName, float offset)
	{
		Patient patient = CaseLoader.Case.Patient;
		string itemvalue = "";
		switch (ItemName)
		{
			case "Name":
				itemvalue = patient.FirstName + "  " + patient.Initial + "  " + patient.LastName;
				break;
			case "SSN":
				itemvalue = patient.SSN;
				break;
			case "BirthDate":
				itemvalue = patient.DOB.ToShortDateString();
				break;
			case "Home Phone":
				itemvalue = patient.HomePhone;
				break;
			case "Address":
				itemvalue = patient.AddressLine1;
				break;
			case "City":
				itemvalue = patient.AddressCity;
				break;
			case "State":
				itemvalue = patient.AddressState;
				break;
			case "Zip":
				itemvalue = patient.AddressZip;
				break;
			case "Employer":
				itemvalue = (patient.Employer != null) ? patient.Employer.Name : "";
				break;
			case "Occupation":
				itemvalue = patient.Occupation;
				break;
			case "Business Adress":
				itemvalue = (patient.Employer != null) ? patient.Employer.AddressLine1 : "";
				break;
			case "Business Phone":
				itemvalue = patient.BusinessPhone;
				break;
			case "Insurance Company":
				{
					if (patient.InsurancePolicy != null)
					{
						if (patient.InsurancePolicy.Company != null)
						{
							itemvalue = patient.InsurancePolicy.Company.Name;
						}
						else
							itemvalue = "";
					}
					else
						itemvalue = "";
				}
				break;
			case "Contract Number":
				itemvalue = (patient.InsurancePolicy != null) ? ((patient.InsurancePolicy.Company != null) ? (patient.InsurancePolicy.Company.Phone) : "") : "";
				break;
			case "Group Number":
				itemvalue = patient.InsurancePolicy.GroupNo;
				break;
			case "Subscriber Number":
				itemvalue = patient.InsurancePolicy.ContractNo;
				break;
			case "Referrer":
				itemvalue = patient.EmergencyContact.FirstName + " " + patient.EmergencyContact.LastName;
				break;
			case "Emergancy Contact":
				itemvalue = patient.EmergencyContact.FirstName + " " + patient.EmergencyContact.LastName;
				break;
			case "Emergancy Phone":
				itemvalue = patient.EmergencyContact.Phone;
				break;
		}
		GUI.Label(new Rect(0, offset, Screen.width, Registrationoffset), itemvalue, CaseSelectText);
	}
	*/
	#endregion
	
	#region Page 3 - Health History
	/// <summary>
	/// Draws the health history tab
	/// </summary>
	void DrawHealthHistory ()
	{
		if (CaseLoader.Case != null) {
			/// Draw the labels
			GUILayout.BeginVertical ();			
			HealthHistoryTree (CaseLoader.Case.HealthHistoryItem.Children, 0);				
			GUILayout.EndVertical ();
		}
		/*
		if (CaseLoader.Case != null)
		{
			HealthHistoryItem[] healthhistory = CaseLoader.Case.HealthHistoryItem.Children;
			HealthHistoryScroll = GUI.BeginScrollView(new Rect(15, 85, Screen.width - 30, Screen.height - 90), HealthHistoryScroll, new Rect(0, 0, Screen.width - 50, HealhistoryScrollOffset));
			HealhistoryScrollOffset = DrawHealthHistoryTreeLevel(healthhistory, 1, 0);
			GUI.EndScrollView();
		}
		*/
	}
	
	/// <summary>
	/// Get the longest string length (including tab space) for the tree
	/// </summary>
	/// <param name='history'>Which health history item to draw</param>
	/// <param name='level'>Tree level</param>
	private int HealthHistoryHeaderCount (HealthHistoryItem[] history, int level)
	{
		// Longest string
		int max = 0;
		
		// Check for all the sibilings on the level
		for (int i = 0; i < history.Length; ++i) {
			// Get the maximum character length
			max = Mathf.Max (max, history [i].Title.Length + 1 + level * spacerSpace);
		
			// Check for all the child object of the siblings
			if (history [i].Children != null)
				max = Mathf.Max (max, HealthHistoryHeaderCount (history [i].Children, level + 1));
		}
		
		return max;
	}
	
	/// <summary>
	/// Draw each health history item in form of tree
	/// </summary>
	/// <param name='history'>Which health history item to draw</param>
	/// <param name='level'>Tree level</param>
	private void HealthHistoryTree (HealthHistoryItem[] history, int level)
	{
		// Draw each item
		for (int i = 0; i < history.Length; ++i) {
			// Decide what style to use
			GUIStyle current = (level == 0 ? CaseRoot : (i % 2 == 0 ? CaseSelectEven : CaseSelectOdd));
			
			// Draw the content
			GUILayout.BeginHorizontal ();
			GUILayout.Label (new string (' ', spacerSpace * level) + history [i].Title + ":", current, 
			                 GUILayout.Width (healthHistoryColumnWidth), GUILayout.ExpandHeight (true)); // precalculate the registrationColumnWidth
			GUILayout.Label (history [i].Content, current);
			GUILayout.EndHorizontal ();
			
			// Draw its child
			if (history [i].Children != null)
				HealthHistoryTree (history [i].Children, level + 1);
		}
	}
	
	/*
	/// <summary>
	/// Draws the health history tree level
	/// </summary>
	/// <returns>Current level</returns>
	/// <param name='history'>History items</param>
	/// <param name='level'>Current level</param>
	/// <param name='offset'>X offset</param>
	float DrawHealthHistoryTreeLevel(HealthHistoryItem[] history, int level, float offset)
	{
		GUIStyle even = new GUIStyle(HealthHistoryText);
		if (level > 1)
		{
			even.normal.background = SelectCaseEvenBack;
		}
		else
		{
			even.normal.background = SelectCaseHoverBack;
		}
		GUIStyle[] Levelstyle = { even, HealthHistoryText };
		int Style = 0;
		foreach (HealthHistoryItem item in history)
		{
			//Debug.Log(child.Title + "[" + (child.Content != null ? child.Content : "") + "]");

			if (item.Content != null)
			{
				int additionalLines = CountLines(item.Content);
				GUI.Label(new Rect(((level - 1) * 20), offset, Screen.width - 32, HealthHistoryoffset * additionalLines), "", Levelstyle[level == 1 ? 0 : Style]);
				GUI.Label(new Rect(((level - 1) * 20), offset, 200, HealthHistoryoffset * additionalLines), "" + item.Title, HealthHistoryText);
				GUI.Label(new Rect(200 + (((level - 1) * 20)), offset, 750, HealthHistoryoffset * additionalLines), "" + item.Content, HealthHistoryText);
				offset += HealthHistoryoffset * (additionalLines - 1);
			}
			else
			{
				GUI.Label(new Rect(((level - 1) * 20), offset, Screen.width - 32, HealthHistoryoffset), "", Levelstyle[level == 1 ? 0 : Style]);
				GUI.Label(new Rect(((level - 1) * 20), offset, 200, HealthHistoryoffset), "" + item.Title, HealthHistoryText);
				GUI.Label(new Rect(200 + ((level - 1) * 20), offset, 750, HealthHistoryoffset), "" + item.Content, HealthHistoryText);
			}

			Style = (Style == 1 ? 0 : 1);
			offset += HealthHistoryoffset + 1;

			if (item.Children != null)
			{
				offset = DrawHealthHistoryTreeLevel(item.Children, level + 1, offset);
			}
		}
		return offset;
	}
	*/
	#endregion
	
	#region Page 4 - Case Selection - Not Being Used
	/*
	/// <summary>
	/// Draws page #4, the select case window, no longer used
	/// </summary>
	void DrawSelectCase()
	{
		//GUI.Label(new Rect(100,200, 1000, 15), AvailableCases[0].Name );
		float offset = 0;

		for (int i = 0; i < AvailableCases.Count; i++)
		{
			if (CaseLoader.Case == AvailableCases[i])
			{
				if (GUI.Button(new Rect(15, 120 + offset, Screen.width - 32, SelectCaselineoffset), "", CaseSelectSelectedBack))
				{
					CaseLoader.Case = AvailableCases[i];
				}
				GUI.Label(new Rect(10, 120 + offset, 100, SelectCaselineoffset), "" + AvailableCases[i].ID, CaseSelectSelectedText);
				GUI.Label(new Rect(115, 120 + offset, 440, SelectCaselineoffset), AvailableCases[i].Name, CaseSelectSelectedText);
				GUI.Label(new Rect(455, 120 + offset, 440, SelectCaselineoffset), "" + CaseResponses[i].Status, CaseSelectSelectedText);
			}
			else
			{
				if (i % 2 == 0)
				{
					if (GUI.Button(new Rect(15, 120 + offset, Screen.width - 32, SelectCaselineoffset), "", CaseSelectEven))
					{
						CaseLoader.Case = AvailableCases[i];
					}
					GUI.Label(new Rect(10, 120 + offset, 100, SelectCaselineoffset), "" + AvailableCases[i].ID, CaseSelectText);
					GUI.Label(new Rect(115, 120 + offset, 440, SelectCaselineoffset), AvailableCases[i].Name, CaseSelectText);
					GUI.Label(new Rect(455, 120 + offset, 440, SelectCaselineoffset), "" + CaseResponses[i].Status, CaseSelectText);
				}
				else
				{
					if (GUI.Button(new Rect(15, 120 + offset, Screen.width - 32, SelectCaselineoffset), "", CaseSelectOdd))
					{
						CaseLoader.Case = AvailableCases[i];
					}
					GUI.Label(new Rect(10, 120 + offset, 100, SelectCaselineoffset), "" + AvailableCases[i].ID, CaseSelectText);
					GUI.Label(new Rect(115, 120 + offset, 440, SelectCaselineoffset), AvailableCases[i].Name, CaseSelectText);
					GUI.Label(new Rect(455, 120 + offset, 440, SelectCaselineoffset), "" + CaseResponses[i].Status, CaseSelectText);
				}
			}
			offset += SelectCaselineoffset;
		}
		if (!AllCasesLoaded)
		{
			GUI.Label(new Rect(15, 120 + offset, Screen.width - 32, SelectCaselineoffset), "", CaseSelectEven);
			GUI.Label(new Rect(115, 120 + offset, 440, SelectCaselineoffset), "Loading...", CaseSelectText);
		}

		GUI.Label(new Rect(0, -3, Screen.width + 5, Screen.height + 5), SelectCaseBackGround);
	}
	*/
	#endregion
	#endregion
	
	#region Helper Method
	/// <summary>
	/// Counts the lines
	/// </summary>
	/// <returns>Number of lines</returns>
	/// <param name='stringtoCount'>What string to count</param>
	int CountLines (string stringtoCount)
	{
		int lines = 1;
		for (int i = 0; i < stringtoCount.Length; ++i) {
			if (stringtoCount [i] == '\n')
				lines++;
		}

		if (lines > 1)
			return lines;
		return lines + stringtoCount.Length / 130;
	}
	#endregion
}