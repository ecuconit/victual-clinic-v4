using UnityEngine;
using System.Collections;

public class TitleScreen : MonoBehaviour 
{
	#region Public Config Variable
	public Texture2D titleLogo;
	public GUIStyle StartButtonStyle = new GUIStyle();
	#endregion
	
	#region Private Data Holder Variables
	private wwwread SeverConnection;
	// private bool verified = false;
	
	#region GUI Position Holder
	private Rect logoPosition;
	private Rect startButtonPosition;
	#endregion
	#endregion
		
	#region Object Initialization
	/// <summary>
	/// Start this instance
	/// </summary>
	void Start () 
	{
		// Grab the server connection object and save to buffer
		SeverConnection = GetComponent<wwwread>();
		
		// Object verificaiton
		// verified = true;
		if (titleLogo == null)			
		{
			Global.ToDebug (this, "[Title Logo] has not been provided to [Title Screen].", LogMessageType.VerificationError);
			// verified = false;
		}
		if (StartButtonStyle == null)
		{
			Global.ToDebug (this, "[Start Button Style] has not been provided to [Title Screen].", LogMessageType.VerificationError);
			// verified = false;
		}		
		
		// Initialize GUI elements
		InitializeGUI ();
	}
	
	#region Initialize GUI Positions
	/// <summary>
	/// Initializes the GUI element positions
	/// </summary>
	private void InitializeGUI ()
	{
		// logoPosition = new Rect(0,-3, Screen.width+ 5, Screen.height + 5); // Original hardcode
		// Calculate size, capping by max size
		Vector2 titleCardSize = new Vector2 ((float)titleLogo.width, (float)titleLogo.height);
		Vector2 screenSize = new Vector2 (Mathf.Min ((float)titleLogo.width, (float)Screen.width), Mathf.Min ((float)titleLogo.height, (float)Screen.height));
		if ((titleCardSize.x / titleCardSize.y) > (screenSize.x / screenSize.y))
		{
			// Title card is fixed by width, scale to height ratio
			titleCardSize /= titleCardSize.x;
			titleCardSize *= screenSize.x;
		}
		else
		{
			// Title card is fixed by height, scale to width ratio
			titleCardSize /= titleCardSize.y;
			titleCardSize *= screenSize.y;
		}
		logoPosition = new Rect((Screen.width - titleCardSize.x) / 2, (Screen.height - titleCardSize.y) / 2, titleCardSize.x, titleCardSize.y);
			
		// startButtonPosition = new Rect((Screen.width / 2) - 145 , 550, 308, 153); // Original hardcode
		// Offset button size by ratio
		// float ratio = (StartButtonStyle.normal.background.height / 1024.0f) * titleCardSize.y / StartButtonStyle.normal.background.height; =>
		float ratio = titleCardSize.y / titleLogo.height;
		Vector2 startButtonSize = new Vector2 (StartButtonStyle.normal.background.width * ratio, StartButtonStyle.normal.background.height * ratio);
		startButtonPosition = new Rect((Screen.width - startButtonSize.x) / 2, logoPosition.y + (550.0f / titleLogo.height * logoPosition.height), 
			startButtonSize.x, startButtonSize.y); // Original offset was 550 px off top of the background
	}
	#endregion
	#endregion
	
	#region Frame Update
	/// <summary>
	/// Draw the title screen
	/// </summary>
	/// <returns>Whether the form has ended or not</returns>
	/// <param name='resize'>Whether the GUI has been resized or not</param>
	public bool Draw(bool redraw)
	{
		// If marked to redraw, reinitialize GUI elements
		if (redraw)
			InitializeGUI ();
		
		GUI.Label(logoPosition, titleLogo);		
		
		
		// Check to make sure case is loaded before enabling the button
		if(!SeverConnection.FirstCaseLoaded)
			GUI.enabled = false;
		else
			GUI.enabled = true;
		
		// If the start button is pressed, send back okay
		if(GUI.Button(startButtonPosition, "", StartButtonStyle))
			return false;
		
		return true;
	}
	#endregion
}
