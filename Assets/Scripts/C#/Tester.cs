using UnityEngine;
using System.Collections;

public class Tester : MonoBehaviour 
{
	public string toTest = "Poor weight gain, pica, and anemia View the nutritional consult videos (copy and paste the URL into your browser window): Healthy Eating During Pregnancy (16 min.) http://con.ecu.edu/Mediasite/Play/0ec1586cbd9a4aa5bcd11457f3a53cac1d and Caloric Needs and Weight Gain in Pregnancy (14 min.) http://con.ecu.edu/Mediasite/Play/7eec35e883264288aece46222e4484721d *You will need to sign in with your ECU user ID and PW to view the videos	";
	
	// Use this for initialization
	void Start () 
	{
		InterviewWindow tester = new InterviewWindow ();
		string result = tester.CutStringToSize (toTest, 50);
		int lineCount = tester.CountLines (result);
		Global.ToDebug (this, result, LogMessageType.DisplayMessage);		
		Global.ToDebug (this, "Line count: " + lineCount, LogMessageType.DisplayMessage);
	}
}
