using UnityEngine;
using System;
using System.Collections;

public class PlayerController : MonoBehaviour
{
	#region Public Config Variables
	public GUIStyle GoFullScreenButton = new GUIStyle();
	public GUIStyle CloseFullScreenButton = new GUIStyle();
	
	public bool ShowFullScreenButon = true;
	PlayerPositions CurrentLocation;
	public float PlayerMovmentSpeed = 3.0f;  // the speed of the player movment, default is 5.0f
	public float PlayerMovmentStartDelay = 0.5f; //time in seconds from when you call MovePlayerTo() and the player starts moving
	public float PlayerMovmentLookTime = 2.5f; //time in seconds the player takes to face the new path, default is 1.5f
	public PlayerPositions PlayerStartLocation;
	
	public bool FinishedCase;
	
	// How often to check for GUI update
	public float resizeCheckTime = 0.5f;
	
	public Animator[] notToAnime;
	// public AnimationController oldBlackWoman80, hispanicMale, lumbeeFemale, white25yo, black17yo, avatar111, avatar111Mother, loggerMan, loggerWife;
	public AnimationController lumbeeFemale, lumbeeMale, oldBlackWoman80, loggerMan, loggerWife, shauna,
		avatar19, obeseBlackWoman, avatar2, avatar9, black17yo;
	#endregion
	
	#region Private Data Holder Variables
	// Hold each GUI update sub-controller
	InterviewController interview;
	wwwread SeverConnection;
	ComputerScreenInterface computerscreen;
	TitleScreen titlescreen;
	ReceptionDialog receptionist;
	PreceptorDialog preceptor;
	
	bool LoadSavedResponses;
	
	// Footing to be used for rails to distinguish between each room
	private string railFooting = "";
	
	// For GUI element check
	private bool resized;	
	private float accu = 0.0f;
	private float nextCheck = 0.0f;
	private Vector2 screenSize = Vector2.zero;	
	private PlayerPositions lastPosition = PlayerPositions.OutsideClinic;
	#endregion
	
	#region Object Initialization
	/// <summary>
	/// Start this instance
	/// </summary>
	void Start()
	{
		FinishedCase = false;
		LoadSavedResponses = false;
		
		interview = GetComponent<InterviewController>();
		SeverConnection = GetComponent<wwwread>();
		computerscreen = GetComponent<ComputerScreenInterface>();
		titlescreen = GetComponent<TitleScreen>();
		receptionist = GetComponent<ReceptionDialog>();
		preceptor = GetComponent<PreceptorDialog>();
		
		switch (PlayerStartLocation)
		{
		case PlayerPositions.inMotion:
		{
			DebugConsole.LogError("You cannot manualy set the player's state to inMotion, you must start the player at a specific location");
			return;
		}
		case PlayerPositions.OutsideClinic:
		{
			CurrentLocation = PlayerPositions.OutsideClinic;
			
			break;
		}
		case PlayerPositions.ReceptionDesk:
		{
			gameObject.transform.position = new Vector3(1.012727f, 4.0f, 13.03424f);
			gameObject.transform.Rotate(new Vector3(0, 341.9735f, 0));
			onEnterReceptionDesk();
			break;
		}
		case PlayerPositions.ComputerRoom:
		{
			gameObject.transform.position = new Vector3(-16.7775f, 4.0f, 18.13648f);
			gameObject.transform.Rotate(new Vector3(0, 186.9735f, 0));
			onEnterComputerRoom();
			break;
		}
			/*
            // On entering examination room, don't fix the camera, since there are now multiple examination rooms
            case PlayerPositions.ExamRoom:
            {
                gameObject.transform.position = new Vector3(9.535616f,4.0f,51.7618f);
                gameObject.transform.Rotate(new Vector3(2.916191f,111.3183f,0));
                onEnterExamRoom();
                break;	
            }
            */
		case PlayerPositions.PreceptorsOffice:
		{
			gameObject.transform.position = new Vector3(-9.881224f, 4.0f, 49.99445f);
			gameObject.transform.Rotate(new Vector3(0, 249.4731f, 0));
			onEnterPreceptorsOffice();
			break;
		}
		}
	}
	#endregion
	
	#region Helper Method
	/// <summary>
	/// Moves the player to destination by rail then raise the rail completion event
	/// </summary>
	/// <param name='NewPosition'>Where to move to</param>
	public void MovePlayerTo(PlayerPositions NewPosition)
	{
		if (CurrentLocation == PlayerPositions.inMotion)
		{
			DebugConsole.LogError("You cannot reset change the player's location while already in motion");
			return;
		}
		
		switch (NewPosition)
		{
		case PlayerPositions.inMotion:
		{
			DebugConsole.LogError("You cannot manualy set the player's state to inMotion, you must give MovePlayerTo() a specific location");
			return;
		}
		case PlayerPositions.ReceptionDesk:
		{
			if (CurrentLocation != PlayerPositions.OutsideClinic)
			{
				DebugConsole.LogError("There is currently no path defined for moving to " + NewPosition + " from " + CurrentLocation);
				return;
			}
			CurrentLocation = PlayerPositions.inMotion;
			iTween.MoveTo(gameObject, iTween.Hash("path", iTweenPath.GetPath("Reception1"), "delay", PlayerMovmentStartDelay, "looktime", PlayerMovmentLookTime, "orienttopath", true, "speed", PlayerMovmentSpeed, "easetype", "EaseInOutSine", "oncomplete", "onEnterReceptionDesk"));
			break;
		}
		case PlayerPositions.ComputerRoom:
		{
			if (CurrentLocation == PlayerPositions.ReceptionDesk)
			{
				CurrentLocation = PlayerPositions.inMotion;
				iTween.MoveTo(gameObject, iTween.Hash("path", iTweenPath.GetPath("Computer1"), "delay", PlayerMovmentStartDelay, "looktime", PlayerMovmentLookTime, "orienttopath", true, "speed", PlayerMovmentSpeed, "easetype", "EaseInOutSine", "oncomplete", "onEnterComputerRoom"));
				break;
			}
			else if (CurrentLocation == PlayerPositions.PreceptorsOffice)
			{
				CurrentLocation = PlayerPositions.inMotion;
				iTween.MoveTo(gameObject, iTween.Hash("path", iTweenPath.GetPath("Computer2"), "delay", PlayerMovmentStartDelay, "looktime", PlayerMovmentLookTime, "orienttopath", true, "speed", PlayerMovmentSpeed, "easetype", "EaseInOutSine", "oncomplete", "onEnterComputerRoom"));
				break;
			}
			else if (CurrentLocation == PlayerPositions.ExamRoom)
			{
				CurrentLocation = PlayerPositions.inMotion;
				// Select rail based on rail's footing
				iTween.MoveTo(gameObject, iTween.Hash("path", iTweenPath.GetPath("Computer3" + railFooting), "delay", PlayerMovmentStartDelay, "looktime", PlayerMovmentLookTime, "orienttopath", true, "speed", PlayerMovmentSpeed, "easetype", "EaseInOutSine", "oncomplete", "onEnterComputerRoom"));
				break;
			}
			else
			{
				DebugConsole.LogError("There is currently no path defined for moving  " + NewPosition + " from " + CurrentLocation);
				return;
			}
		}
		case PlayerPositions.ExamRoom:
		{
			if (CurrentLocation != PlayerPositions.ComputerRoom)
			{
				DebugConsole.LogError("There is currently no path defined for moving  " + NewPosition + " from " + CurrentLocation);
				return;
			}
			CurrentLocation = PlayerPositions.inMotion;
			// Select rail based on rail's footing
			iTween.MoveTo(gameObject, iTween.Hash("path", iTweenPath.GetPath("Exam1" + railFooting), "delay", PlayerMovmentStartDelay, "looktime", PlayerMovmentLookTime, "orienttopath", true, "speed", PlayerMovmentSpeed, "easetype", "EaseInOutSine", "oncomplete", "onEnterExamRoom"));
			break;
		}
		case PlayerPositions.PreceptorsOffice:
		{
			if (CurrentLocation == PlayerPositions.ExamRoom)
			{
				CurrentLocation = PlayerPositions.inMotion;
				// Select rail based on rail's footing
				iTween.MoveTo(gameObject, iTween.Hash("path", iTweenPath.GetPath("Doctor1" + railFooting), "delay", PlayerMovmentStartDelay, "looktime", PlayerMovmentLookTime, "orienttopath", true, "speed", PlayerMovmentSpeed, "easetype", "EaseInOutSine", "oncomplete", "onEnterPreceptorsOffice"));
				break;
			}
			else if (CurrentLocation == PlayerPositions.ReceptionDesk)
			{
				CurrentLocation = PlayerPositions.inMotion;
				iTween.MoveTo(gameObject, iTween.Hash("path", iTweenPath.GetPath("Doctor2"), "delay", PlayerMovmentStartDelay, "looktime", PlayerMovmentLookTime, "orienttopath", true, "speed", PlayerMovmentSpeed, "easetype", "EaseInOutSine", "oncomplete", "onEnterPreceptorsOffice"));
				break;
			}
			else
			{
				DebugConsole.LogError("There is currently no path defined for moving  " + NewPosition + " from " + CurrentLocation);
				return;
			}
		}
		}
	}
	
	#region Rail Completion Event
	/***********************************************************************************************************	
		Enter functions: Each off these functions will be called exactly one time the moment the
		Player enters any of the possible position states.
	************************************************************************************************************/
	/// <summary>
	/// On entering receptionist room, display the instruction
	/// </summary>
	void onEnterReceptionDesk()
	{
		DebugConsole.Log("Player is at the ReceptionDesk");
		
		receptionist.SetTextAndAudio(SeverConnection.Case.ReceptionistStep.Text1,
		                             SeverConnection.Case.ReceptionistStep.Text1MP3Url,
		                             SeverConnection.Case.ReceptionistStep.Text2,
		                             SeverConnection.Case.ReceptionistStep.Text2MP3Url);
		
		receptionist.StartAudioPlayback();
		
		CurrentLocation = PlayerPositions.ReceptionDesk;
		
		// Set rail footing, done by hard coding ^o^
		railFooting = ""; // In this batch, all use exam room 1
		Global.ToDebug(this, "Switching to case #" + SeverConnection.Case.ID, LogMessageType.DisplayMessage);

		switch (SeverConnection.Case.ID) {
			case 149:
				// Use lumbee woman
				Global.AnimationManagerRef.AddCharacter(lumbeeFemale);
				GameObject.Destroy(oldBlackWoman80.gameObject);
				GameObject.Destroy(loggerMan.gameObject); GameObject.Destroy(loggerWife.gameObject);
				GameObject.Destroy(shauna.gameObject);
				GameObject.Destroy(avatar19.gameObject);
				GameObject.Destroy(obeseBlackWoman.gameObject);				
				GameObject.Destroy(avatar2.gameObject);
				GameObject.Destroy(avatar9.gameObject);
				GameObject.Destroy(black17yo.gameObject);
				break;
			case 171:
				// Use logger man and logger wife
				Global.AnimationManagerRef.AddCharacter(loggerMan);
				GameObject.Destroy(lumbeeFemale.gameObject); GameObject.Destroy(lumbeeMale.gameObject);
				GameObject.Destroy(oldBlackWoman80.gameObject);
				GameObject.Destroy(shauna.gameObject);
				GameObject.Destroy(avatar19.gameObject);
				GameObject.Destroy(obeseBlackWoman.gameObject);				
				GameObject.Destroy(avatar2.gameObject);
				GameObject.Destroy(avatar9.gameObject);
				GameObject.Destroy(black17yo.gameObject);
				break;
			case 172:
				// Use Shauna
				Global.AnimationManagerRef.AddCharacter(shauna);
				GameObject.Destroy(lumbeeFemale.gameObject); GameObject.Destroy(lumbeeMale.gameObject);
				GameObject.Destroy(oldBlackWoman80.gameObject);
				GameObject.Destroy(loggerMan.gameObject); GameObject.Destroy(loggerWife.gameObject);				
				GameObject.Destroy(avatar19.gameObject);
				GameObject.Destroy(obeseBlackWoman.gameObject);
				GameObject.Destroy(avatar2.gameObject);
				GameObject.Destroy(avatar9.gameObject);
				GameObject.Destroy(black17yo.gameObject);
				break;
			case 173:
				// Use avatar 19
				Global.AnimationManagerRef.AddCharacter(avatar19);
				GameObject.Destroy(oldBlackWoman80.gameObject);	
				GameObject.Destroy(lumbeeFemale.gameObject); GameObject.Destroy(lumbeeMale.gameObject);
				GameObject.Destroy(loggerMan.gameObject); GameObject.Destroy(loggerWife.gameObject);
				GameObject.Destroy(shauna.gameObject);
				GameObject.Destroy(obeseBlackWoman.gameObject);	
				GameObject.Destroy(avatar2.gameObject);
				GameObject.Destroy(avatar9.gameObject);
				GameObject.Destroy(black17yo.gameObject);
				break;
			case 174:
				// Use obese black woman
				Global.AnimationManagerRef.AddCharacter(obeseBlackWoman);
				GameObject.Destroy(oldBlackWoman80.gameObject);	
				GameObject.Destroy(lumbeeFemale.gameObject); GameObject.Destroy(lumbeeMale.gameObject);
				GameObject.Destroy(loggerMan.gameObject); GameObject.Destroy(loggerWife.gameObject);
				GameObject.Destroy(shauna.gameObject);
				GameObject.Destroy(avatar19.gameObject);
				GameObject.Destroy(avatar2.gameObject);
				GameObject.Destroy(avatar9.gameObject);
				GameObject.Destroy(black17yo.gameObject);
				break;
			case 175:
				// Use avatar 2
				Global.AnimationManagerRef.AddCharacter(avatar2);
				GameObject.Destroy(lumbeeFemale.gameObject); GameObject.Destroy(lumbeeMale.gameObject);
				GameObject.Destroy(oldBlackWoman80.gameObject);
				GameObject.Destroy(loggerMan.gameObject); GameObject.Destroy(loggerWife.gameObject);
				GameObject.Destroy(shauna.gameObject);
				GameObject.Destroy(avatar19.gameObject);
				GameObject.Destroy(obeseBlackWoman.gameObject);				
				GameObject.Destroy(avatar9.gameObject);
				GameObject.Destroy(black17yo.gameObject);
				break;
			case 176:
				// Use temp user controller
				Global.AnimationManagerRef.AddCharacter(avatar9);
				GameObject.Destroy(lumbeeFemale.gameObject); GameObject.Destroy(lumbeeMale.gameObject);
				GameObject.Destroy(oldBlackWoman80.gameObject);
				GameObject.Destroy(loggerMan.gameObject); GameObject.Destroy(loggerWife.gameObject);
				GameObject.Destroy(shauna.gameObject);
				GameObject.Destroy(avatar19.gameObject);
				GameObject.Destroy(obeseBlackWoman.gameObject);
				GameObject.Destroy(avatar2.gameObject);
				GameObject.Destroy(black17yo.gameObject);
				break;
			case 111:
				// Use 17 years old black pregnant
				Global.AnimationManagerRef.AddCharacter(black17yo);
				GameObject.Destroy(lumbeeFemale.gameObject); GameObject.Destroy(lumbeeMale.gameObject);
				GameObject.Destroy(loggerMan.gameObject); GameObject.Destroy(loggerWife.gameObject);
				GameObject.Destroy(shauna.gameObject);
				GameObject.Destroy(avatar19.gameObject);
				GameObject.Destroy(obeseBlackWoman.gameObject);
				GameObject.Destroy(avatar2.gameObject);
				GameObject.Destroy(avatar9.gameObject);
				GameObject.Destroy(oldBlackWoman80.gameObject);
				break;
			case 164:
			case 120:
			default:
				// Use default model
				Global.AnimationManagerRef.AddCharacter(oldBlackWoman80);
				GameObject.Destroy(lumbeeFemale.gameObject); GameObject.Destroy(lumbeeMale.gameObject);
				GameObject.Destroy(loggerMan.gameObject); GameObject.Destroy(loggerWife.gameObject);
				GameObject.Destroy(shauna.gameObject);
				GameObject.Destroy(avatar19.gameObject);
				GameObject.Destroy(obeseBlackWoman.gameObject);				
				GameObject.Destroy(avatar2.gameObject);
				GameObject.Destroy(avatar9.gameObject);
				GameObject.Destroy(black17yo.gameObject);
				break;
		}
		/*
		switch (SeverConnection.Case.ID) {
			case 148:
				// Use Hispanic man
				Global.AnimationManagerRef.AddCharacter(hispanicMale);
				GameObject.Destroy(oldBlackWoman80.gameObject);
				// GameObject.Destroy(hispanicMale.gameObject);
				GameObject.Destroy(lumbeeFemale.gameObject);
				GameObject.Destroy(white25yo.gameObject);
				GameObject.Destroy(black17yo.gameObject);
				GameObject.Destroy(avatar111.gameObject);
				GameObject.Destroy(avatar111Mother.gameObject);
				GameObject.Destroy(loggerMan.gameObject);
				GameObject.Destroy(loggerWife.gameObject);
				break;
			case 149:
				// Use Lumbee woman
				Global.AnimationManagerRef.AddCharacter(lumbeeFemale);
				GameObject.Destroy(oldBlackWoman80.gameObject);
				GameObject.Destroy(hispanicMale.gameObject);
				// GameObject.Destroy(lumbeeFemale.gameObject);
				GameObject.Destroy(white25yo.gameObject);
				GameObject.Destroy(black17yo.gameObject);
				GameObject.Destroy(avatar111.gameObject);
				GameObject.Destroy(avatar111Mother.gameObject);
				GameObject.Destroy(loggerMan.gameObject);
				GameObject.Destroy(loggerWife.gameObject);
				break;
			case 169:
				// Use white 25 years old
				Global.AnimationManagerRef.AddCharacter(white25yo);
				GameObject.Destroy(oldBlackWoman80.gameObject);
				GameObject.Destroy(hispanicMale.gameObject);
				GameObject.Destroy(lumbeeFemale.gameObject);
				// GameObject.Destroy(white25yo.gameObject);
				GameObject.Destroy(black17yo.gameObject);
				GameObject.Destroy(avatar111.gameObject);
				GameObject.Destroy(avatar111Mother.gameObject);
				GameObject.Destroy(loggerMan.gameObject);
				GameObject.Destroy(loggerWife.gameObject);
				break;
			case 113:
				// Use hispanic mother and child
				Global.AnimationManagerRef.AddCharacter(avatar111Mother);
				GameObject.Destroy(oldBlackWoman80.gameObject);
				GameObject.Destroy(hispanicMale.gameObject);
				GameObject.Destroy(lumbeeFemale.gameObject);
				GameObject.Destroy(white25yo.gameObject);
				GameObject.Destroy(black17yo.gameObject);
				// GameObject.Destroy(avatar111.gameObject);
				// GameObject.Destroy(avatar111Mother.gameObject);
				GameObject.Destroy(loggerMan.gameObject);
				GameObject.Destroy(loggerWife.gameObject);
				break;
			case 111:
				// Use black 17 years old
				Global.AnimationManagerRef.AddCharacter(black17yo);
				GameObject.Destroy(oldBlackWoman80.gameObject);
				GameObject.Destroy(hispanicMale.gameObject);
				GameObject.Destroy(lumbeeFemale.gameObject);
				GameObject.Destroy(white25yo.gameObject);
				// GameObject.Destroy(black17yo.gameObject);
				GameObject.Destroy(avatar111.gameObject);
				GameObject.Destroy(avatar111Mother.gameObject);
				GameObject.Destroy(loggerMan.gameObject);
				GameObject.Destroy(loggerWife.gameObject);
				break;
			case 171:
				// Use logger man and logger wife
				Global.AnimationManagerRef.AddCharacter(loggerMan);
				GameObject.Destroy(oldBlackWoman80.gameObject);
				GameObject.Destroy(hispanicMale.gameObject);
				GameObject.Destroy(lumbeeFemale.gameObject);
				GameObject.Destroy(white25yo.gameObject);
				GameObject.Destroy(black17yo.gameObject);
				GameObject.Destroy(avatar111.gameObject);
				GameObject.Destroy(avatar111Mother.gameObject);
				// GameObject.Destroy(loggerMan.gameObject);
				// GameObject.Destroy(loggerWife.gameObject);
				break;
			case 120:
			default:
				// Use default model
				Global.AnimationManagerRef.AddCharacter(oldBlackWoman80);
				// GameObject.Destroy(oldBlackWoman80.gameObject);
				GameObject.Destroy(hispanicMale.gameObject);
				GameObject.Destroy(lumbeeFemale.gameObject);
				GameObject.Destroy(white25yo.gameObject);
				GameObject.Destroy(black17yo.gameObject);
				GameObject.Destroy(avatar111.gameObject);
				GameObject.Destroy(avatar111Mother.gameObject);
				GameObject.Destroy(loggerMan.gameObject);
				GameObject.Destroy(loggerWife.gameObject);
				break;
		}
		*/
		/*
		switch (SeverConnection.Case.ID)
		{
			case 130:
				// GameObject.Destroy (oldWhiteWoman.gameObject);
				GameObject.Destroy(oldBlackWoman80.gameObject);
				GameObject.Destroy(oldBlackWoman84.gameObject);
				Global.AnimationManagerRef.AddCharacter(oldWhiteWoman);

				Global.ToDebug(this, "Use old white woman", LogMessageType.DisplayMessage);
				break;
			case 131:
				GameObject.Destroy(oldWhiteWoman.gameObject);
				// GameObject.Destroy (oldBlackWoman80.gameObject);
				GameObject.Destroy(oldBlackWoman84.gameObject);
				Global.AnimationManagerRef.AddCharacter(oldBlackWoman80);
				
				Global.ToDebug(this, "Use old black woman 80", LogMessageType.DisplayMessage);
				break;
			default:
			case 142:
				GameObject.Destroy(oldWhiteWoman.gameObject);
				GameObject.Destroy(oldBlackWoman80.gameObject);
				// GameObject.Destroy (oldBlackWoman84.gameObject);
				Global.AnimationManagerRef.AddCharacter(oldBlackWoman84);

				Global.ToDebug(this, "Use old black woman 84", LogMessageType.DisplayMessage);				
				break;
		}
		*/

		/*
		switch (SeverConnection.Case.ID)
		{
		default:
		case 120:
		case 140:
			GameObject.Destroy (oldWhiteFarmer.gameObject);
			GameObject.Destroy (avatar2.gameObject);
			GameObject.Destroy (avatar14.gameObject);
			GameObject.Destroy (avatar15.gameObject);
			GameObject.Destroy (avatar21.gameObject);
			GameObject.Destroy (lumbeeTeen.gameObject);
			// GameObject.Destroy (oldBlackLady.gameObject);
			Global.AnimationManagerRef.AddCharacter (oldBlackLady);
			break;
		case 135:
		case 153:
			// Use Lumbee teen
			GameObject.Destroy (oldWhiteFarmer.gameObject);
			GameObject.Destroy (avatar2.gameObject);
			GameObject.Destroy (avatar14.gameObject);
			GameObject.Destroy (avatar15.gameObject);
			GameObject.Destroy (avatar21.gameObject);
			// GameObject.Destroy (lumbeeTeen.gameObject);
			GameObject.Destroy (oldBlackLady.gameObject);
			Global.AnimationManagerRef.AddCharacter (lumbeeTeen);
			break;
		case 154:
			// Use Pregnant Asian with drug and domestic abuse
			// GameObject.Destroy (asianPregnantAbuse.gameObject);
			GameObject.Destroy (avatar2.gameObject);
			GameObject.Destroy (avatar14.gameObject);
			GameObject.Destroy (avatar15.gameObject);
			GameObject.Destroy (avatar21.gameObject);
			GameObject.Destroy (lumbeeTeen.gameObject);
			Global.AnimationManagerRef.AddCharacter (asianPregnantAbuse);
			break;
		case 155:
			// Use Old White Farmer
			// GameObject.Destroy (oldWhiteFarmer.gameObject);
			GameObject.Destroy (avatar2.gameObject);
			GameObject.Destroy (avatar14.gameObject);
			GameObject.Destroy (avatar15.gameObject);
			GameObject.Destroy (avatar21.gameObject);
			GameObject.Destroy (lumbeeTeen.gameObject);
			GameObject.Destroy (oldBlackLady.gameObject);
			Global.AnimationManagerRef.AddCharacter (oldWhiteFarmer);
			break;
		case 156:
			// Use Caucasian model
			GameObject.Destroy (oldWhiteFarmer.gameObject);
			GameObject.Destroy (avatar2.gameObject);
			GameObject.Destroy (avatar14.gameObject);
			// GameObject.Destroy (avatar15.gameObject);
			GameObject.Destroy (avatar21.gameObject);
			GameObject.Destroy (lumbeeTeen.gameObject);
			GameObject.Destroy (oldBlackLady.gameObject);
			Global.AnimationManagerRef.AddCharacter (avatar15);
			break;
		case 157:
			// Use African American woman holding a baby
			GameObject.Destroy (oldWhiteFarmer.gameObject);
			// GameObject.Destroy (avatar2.gameObject);
			GameObject.Destroy (avatar14.gameObject);
			GameObject.Destroy (avatar15.gameObject);
			GameObject.Destroy (avatar21.gameObject);
			GameObject.Destroy (lumbeeTeen.gameObject);
			GameObject.Destroy (oldBlackLady.gameObject);
			Global.AnimationManagerRef.AddCharacter (avatar2);
			break;
		case 158:
			// Use Caucasian obese model
			GameObject.Destroy (oldWhiteFarmer.gameObject);
			GameObject.Destroy (avatar2.gameObject);
			// GameObject.Destroy (avatar14.gameObject);
			GameObject.Destroy (avatar15.gameObject);
			GameObject.Destroy (avatar21.gameObject);
			GameObject.Destroy (lumbeeTeen.gameObject);
			Global.AnimationManagerRef.AddCharacter (avatar14);
			break;
		case 159:
			// Use Hispanic model
			GameObject.Destroy (oldWhiteFarmer.gameObject);
			GameObject.Destroy (avatar2.gameObject);
			GameObject.Destroy (avatar14.gameObject);
			GameObject.Destroy (avatar15.gameObject);
			// GameObject.Destroy (avatar21.gameObject);
			GameObject.Destroy (lumbeeTeen.gameObject);
			GameObject.Destroy (oldBlackLady.gameObject);
			Global.AnimationManagerRef.AddCharacter (avatar21);
			break;		
		}
		*/
		
		// Disable animation engine for all
		foreach (Animator ani in notToAnime)
		{
			Global.ToDebug(this, "Disabling " + ani.gameObject.name + " animator.", LogMessageType.TestMessage);
			ani.StopPlayback ();
			ani.enabled = false;
		}
		
		/*
		switch (SeverConnection.Case.ID)
		{
		case 148:
			// Use Hispanic male model
			GameObject.Destroy (avatar13.gameObject);
			GameObject.Destroy (avatar20.gameObject);
			Global.AnimationManagerRef.AddCharacter (hispanicMale);
			break;
		case 135:
			GameObject.Destroy (hispanicMale.gameObject);
			GameObject.Destroy (avatar20.gameObject);
			break;
		case 146:
			GameObject.Destroy (hispanicMale.gameObject);
			GameObject.Destroy (avatar13.gameObject);
			break;
		case 147:
			railFooting = "_3";
			break;
		case 149:
			Global.AnimationManagerRef.AddCharacter (lumbeeFemale);
			railFooting = "_2";
			break;
		case 152:
			railFooting = "_4";
			break;
		default:
			// Use default case and model
			GameObject.Destroy (avatar13.gameObject);
			GameObject.Destroy (avatar20.gameObject);
			Global.AnimationManagerRef.AddCharacter (lumbeeFemale);
			railFooting = "_2";
			break;
		}
		*/
		
		/*
		if (SeverConnection.Case.ID == 148)
		{
			// Use hispanic man
			GameObject.Destroy (obeseBlackWoman.gameObject);
			Global.AnimationManagerRef.AddCharacter (hispanicMan);
			railFooting = "_2";
		}		
		else
		{
			// Use default model
			GameObject.Destroy (hispanicMan.gameObject);
			Global.AnimationManagerRef.AddCharacter (obeseBlackWoman);
		}
		*/
		
		/*
		if (SeverConnection.Case.ID == 120)
		{
			// Use obese black woman
			GameObject.Destroy (oldBlackWoman.gameObject);
			Global.AnimationManagerRef.AddCharacter (obeseBlackWoman);
		}
		else
		{
			// Use default model
			GameObject.Destroy (obeseBlackWoman.gameObject);
			Global.AnimationManagerRef.AddCharacter (oldBlackWoman);
		}
		*/
		
		/*
		if (SeverConnection.Case.ID == 130)
		{
			// Use old white woman
			// GameObject.Destroy (oldBlackWoman.gameObject);
			GameObject.Destroy (pregnantBlackWoman.gameObject);
			GameObject.Destroy (pregnantWhiteWoman.gameObject);
			GameObject.Destroy (whiteMan.gameObject);
			Global.AnimationManagerRef.AddCharacter (oldWhiteWoman);
			railFooting = "_2";
		}
		else if (SeverConnection.Case.ID == 143)
		{
			// Pregnant black woman
			GameObject.Destroy (oldBlackWoman.gameObject);
			Global.AnimationManagerRef.AddCharacter (pregnantBlackWoman);
			GameObject.Destroy (pregnantWhiteWoman.gameObject);
			GameObject.Destroy (whiteMan.gameObject);
			// GameObject.Destroy (oldWhiteWoman.gameObject);
		}
		else if (SeverConnection.Case.ID == 144)
		{
			// White couple
			GameObject.Destroy (oldBlackWoman.gameObject);
			GameObject.Destroy (pregnantBlackWoman.gameObject);
			Global.AnimationManagerRef.AddCharacter (pregnantWhiteWoman);
			Global.AnimationManagerRef.AddCharacter (whiteMan);
			// GameObject.Destroy (oldWhiteWoman.gameObject);
		}
		else
		{
			// Use old black woman by default
			Global.AnimationManagerRef.AddCharacter (oldBlackWoman);
			GameObject.Destroy (pregnantBlackWoman.gameObject);
			GameObject.Destroy (pregnantWhiteWoman.gameObject);
			GameObject.Destroy (whiteMan.gameObject);
			// GameObject.Destroy (oldWhiteWoman.gameObject);
		}
		*/
		
		/*
		if (SeverConnection.Case.ID == 120)
		{
			railFooting = "";
			
			// Add old black woman reference 
			Global.AnimationManagerRef.AddCharacter (oldBlackWoman);
		
			// Take out black woman
			GameObject.Destroy (blackWoman.gameObject);	
			
			// Initialize exam room 2
			GameObject.Destroy (asianWoman.gameObject);	
			GameObject.Destroy (asianWomanPartner.gameObject);	
			
			// Display message
			Global.ToDebug (this, "Exam Room #1 initialized for case #120", LogMessageType.DisplayMessage);
		}
		else if (SeverConnection.Case.ID == 87)
		{
			railFooting = "_2";
			
			// Add old white man reference
			Global.AnimationManagerRef.AddCharacter (oldWhiteMan);
		
			// Take out asian woman and partner
			GameObject.Destroy (asianWoman.gameObject);	
			GameObject.Destroy (asianWomanPartner.gameObject);	
			
			// Initialize exam room 1
			GameObject.Destroy (blackWoman.gameObject);	
			
			Global.ToDebug (this, "Exam Room #2 initialized for case #87", LogMessageType.DisplayMessage);
		}
		else if (SeverConnection.Case.ID == 135)
		{
			railFooting = "";
			
			// Add black woman
			Global.AnimationManagerRef.AddCharacter (blackWoman);
		
			// Take out old black woman
			GameObject.Destroy (oldBlackWoman.gameObject);
			
			// Initialize exam room 2
			GameObject.Destroy (asianWoman.gameObject);	
			GameObject.Destroy (asianWomanPartner.gameObject);	
			
			// Display message
			Global.ToDebug (this, "Exam Room #1 initialized for case #135", LogMessageType.DisplayMessage);
		}
		else if (SeverConnection.Case.ID == 136)
		{			
			railFooting = "_2";	
			
			// Add asian woman and partner
			Global.AnimationManagerRef.AddCharacter (asianWoman);
			Global.AnimationManagerRef.AddCharacter (asianWomanPartner);
		
			// Take out white man reference
			GameObject.Destroy (oldWhiteMan.gameObject);
			
			// Initialize exam room 1
			GameObject.Destroy (blackWoman.gameObject);	
			
			Global.ToDebug (this, "Exam Room #2 initialized for case #136", LogMessageType.DisplayMessage);
		}
		else if (SeverConnection.Case.ID == 137)
		{
			railFooting = "_3";
			
			// Initialize both exam room 1 & 2
			GameObject.Destroy (blackWoman.gameObject);	
			GameObject.Destroy (asianWoman.gameObject);	
			GameObject.Destroy (asianWomanPartner.gameObject);	
			
			Global.ToDebug (this, "Exam Room #3 initialized for case #137", LogMessageType.DisplayMessage);
		}
		else
		{
			Global.ToDebug (this, "An unrecognized case is currently being used.  Currently dynamic model selection has yet to be implemented, only case #87 and #120's models are in use.", LogMessageType.ValidationError);
		}
		*/
		
		// Toggle the models
		
		// Case loaded, initialize preceptor
		DebugConsole.Log("Initializing preceptor...");
		interview.InitializePreceptor(SeverConnection.Case.Clinic.Preceptor.Sex == PatientSex.Male.ToString());
		
		// Load the patient and partner ^o^... actually, could be earlier
	}
	
	/// <summary>
	/// On entering computer room, this is where we will need to connect to the database and get case info
	/// </summary>
	void onEnterComputerRoom()
	{
		DebugConsole.Log("Player entered the ComputerRoom");
		CurrentLocation = PlayerPositions.ComputerRoom;
	}
	
	/// <summary>
	/// On the entering examination room, draw interview
	/// </summary>
	void onEnterExamRoom()
	{
		// Update animation flag
		Global.AnimationManagerRef.SetCharacter(CharacterType.Patient);
		
		DebugConsole.Log("Player entered the ExamRoom");
		CurrentLocation = PlayerPositions.ExamRoom;
		
		//interview.Inventory.LoadFromWeb();
		SeverConnection.LoadCaseToGUI();
		
		if (LoadSavedResponses)
			interview.LoadSavedResponses();
	}
	
	/// <summary>
	/// On the entering preceptors office, this is where all the grading will be done and sent back to the database
	/// </summary>
	void onEnterPreceptorsOffice()
	{
		try
		{
			// Update animation flag
			Global.AnimationManagerRef.SetCharacter(CharacterType.Preceptor);
			
			DebugConsole.Log("Player entered the PreceptorsOffice");
			CurrentLocation = PlayerPositions.PreceptorsOffice;
			
			if (!FinishedCase)
			{			
				preceptor.SetTextAndAudio(
					SeverConnection.Case.IntroductionVoiceText,
					SeverConnection.Case.IntroductionVoiceTextMP3Url,
					SeverConnection.Case.WrapUpVoiceText,
					SeverConnection.Case.WrapUpVoiceTextMP3Url,
					SeverConnection.Case.IntroductionText,
					SeverConnection.Case.WrapUpText
					);
				
				//finalgradescreen.WrapUpText = SeverConnection.Case.WrapUpText;
				
			}
			preceptor.StartAudioPlayback(FinishedCase);
		}
		catch (Exception error)
		{
			Global.ToDebug (this, "Error occurred while initializing preceptor text and audio.  Error Message: " + error.Message, LogMessageType.Exception);
		}
	}
	#endregion
	#endregion
	
	#region On Frame Update
	/// <summary>
	/// Overall GUI event handler
	/// </summary>
	void OnGUI()
	{
		// Check for timer
		resized = false;
		if (accu > nextCheck)
		{
			// Timer reinitialize
			accu -= resizeCheckTime;
			nextCheck = resizeCheckTime;
			
			// Check for resize
			if ((screenSize.x != Screen.width) || (screenSize.y != Screen.height))
			{
				// Update screen size
				screenSize.x = Screen.width;
				screenSize.y = Screen.height;
				resized = true;
			}
		}
		// Update accumulator
		accu += Time.deltaTime;
		
		try
		{
			// If position changed, force resize recalculate 
			if (lastPosition != CurrentLocation)
			{
				lastPosition = CurrentLocation;
				resized = true;
			}
			
			// Split GUI update to each sub-controller
			switch (CurrentLocation)
			{
			case PlayerPositions.inMotion:
			{
				break;
			}
			case PlayerPositions.OutsideClinic:
			{
				OnOutsideClinicGUI(resized);
				break;
			}
			case PlayerPositions.ReceptionDesk:
			{
				OnReceptionDeskGUI(resized);
				break;
			}
			case PlayerPositions.ComputerRoom:
			{
				OnComputerRoomGUI(resized);
				break;
			}
			case PlayerPositions.ExamRoom:
			{
				OnExamRoomGUI(resized);
				break;
			}
			case PlayerPositions.PreceptorsOffice:
			{
				OnPreceptorsOfficeGUI(resized);
				break;
			}
			}
			//if(ShowFullScreenButon)
			//{
			//	DrawFullScreenButton();
			//}
		}
		catch (Exception error)
		{
			Global.ToDebug (this, "Unhandled exception: " + error.Message + ".\nStack: " + error.StackTrace, LogMessageType.Exception);
		}
	}
	
	void DrawFullScreenButton()
	{
		bool toggleFullScreen = false;
		if (!Screen.fullScreen)
		{
			if (GUI.Button(new Rect((Screen.width) - 24, 0, 24, 24), "", GoFullScreenButton))
			{
				toggleFullScreen = true;
			}
		}
		else
		{
			if (GUI.Button(new Rect((Screen.width) - 24, 0, 24, 24), "", CloseFullScreenButton))
			{
				toggleFullScreen = true;
			}
		}
		if (toggleFullScreen)
		{
			Screen.fullScreen = !Screen.fullScreen;
		}
	}
	
	#region GUI Update Sub-Control
	/***********************************************************************************************************	
	GUI functions: Each off these functions work exactly like the standard Unity OnGUI function except 
	they are only called if the player is in the corresponding position
		
	To Move the player to a new room call MovePlayerTo(PlayerPositions.Position) 
	where PlayerPositions.Position is a value defined in the enum above
	************************************************************************************************************/
	
	/// <summary>
	/// When the player is outside of clinic, draw the title screen and start button
	/// </summary>
	/// <param name='resize'>Whether the GUI has been resized or not</param>
	void OnOutsideClinicGUI(bool resize)
	{
		// Draw the title screen until form has been confirmed to proceed
		if (!titlescreen.Draw(resize))
			MovePlayerTo(PlayerPositions.ReceptionDesk);
	}
	
	/// <summary>
	/// GUI update for receptionist desk, draw instruction
	/// </summary>
	/// <param name='resize'>Whether the controller is calling for GUI update</param>
	void OnReceptionDeskGUI(bool resize)
	{
		// Draw the receptionist welcome message window until confirmed to proceed
		if (!receptionist.Draw(resize))
			MovePlayerTo(PlayerPositions.PreceptorsOffice);
	}
	
	/// <summary>
	/// GUI update for computer room, displays the forms
	/// </summary>
	/// <param name='resize'>Whether the controller is calling for GUI update</param>
	void OnComputerRoomGUI(bool resize)
	{
		switch (computerscreen.Draw(resize))
		{
		case 1: //Case not yet started or player selected restart			
			StartCoroutine(SeverConnection.InitializePlayerResponse());
			/*
                if(SeverConnection.Case.Patient.Sex == "Male")
                    interview.LoadPatientModel(PatientSex.Male);
                else
                    interview.LoadPatientModel(PatientSex.Female);
                */
			MovePlayerTo(PlayerPositions.ExamRoom);
			break;
		case 2: //Continuing save case interview 
			/*
                if(SeverConnection.Case.Patient.Sex == "Male")				
                    interview.LoadPatientModel(PatientSex.Male);
                else
                    interview.LoadPatientModel(PatientSex.Female);
                */
			LoadSavedResponses = true;
			MovePlayerTo(PlayerPositions.ExamRoom);
			break;
		}
	}
	
	/// <summary>
	/// GUI update for examination room, for interview
	/// </summary>
	/// <param name='resize'>Whether the controller is calling for GUI update</param>
	void OnExamRoomGUI(bool resize)
	{
		switch (interview.Draw(resize))
		{
		case 0:
			break;
		case 1:
			FinishedCase = true;
			MovePlayerTo(PlayerPositions.PreceptorsOffice);
			break;
		case 2:
			StartCoroutine(SeverConnection.LoadAvaiableCases());
			MovePlayerTo(PlayerPositions.ComputerRoom);
			break;
		}
	}
	
	/// <summary>
	/// GUI update for preceptor office for either review (case finished) or preview (case not finished)
	/// </summary>
	/// <param name='resize'>Whether the controller is calling for GUI update</param>
	void OnPreceptorsOfficeGUI(bool resize)
	{
		/*
        // Depends on whether the case is finished or not, travel to computer room or examination room
        if(FinishedCase)
            if(!preceptor.Draw(FinishedCase, resize))
                MovePlayerTo(PlayerPositions.ComputerRoom);	
        else
            if (!preceptor.Draw(FinishedCase, resize)) 
                MovePlayerTo(PlayerPositions.ComputerRoom);
        */
		// ^o^ To update
		if (!preceptor.Draw(FinishedCase, resize))
			MovePlayerTo(PlayerPositions.ComputerRoom);
	}
	#endregion
	#endregion
}

#region Helper Enumerator
/// <summary>
/// Player positions flag, used to indicate where the player is
/// </summary>
public enum PlayerPositions 
{ 
	OutsideClinic, 
	ReceptionDesk, 
	ComputerRoom, 
	ExamRoom, 
	PreceptorsOffice, 
	inMotion
};
#endregion