using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using VirtualClinic.WebSite.Serializable;

/// <summary>
/// Object to hold each interview step option, used to determine maximum/minimum item selection as well as form type
/// </summary>
[System.Serializable]
public class StepOptions
{
	public int	MaxSelectedQuestions;
	public int	MinSelectedQuestions;
	public StepQuestionSelectionType QuestionSelectionType;

	public StepOptions(int max, int min, StepQuestionSelectionType type)
	{
		MaxSelectedQuestions = max;
		MinSelectedQuestions = min;
		QuestionSelectionType = type;
	}
}

/// <summary>
/// Data holder for an interview step
/// </summary>
[System.Serializable]
public class GUIInterviewStep 
{
	#region Public Config Variables
	public int StepIndex;
	public string Name;
	public float StepScore = 0;
	public Dictionary<long,InterviewQuestion> Questions;
	public List<long> SelectedQuestions;
	public List<long> SelectedAnswers;
	public SerializableDictionary<long,bool> AllSelections;
	public StepOptions Options;
	public QuestionResponseDisplayType? CurrentResponseDisplayType;
	public bool drawReview = false;
	#endregion
	
	#region Private Data Holder
	SelectionReturn selection;
	PlayerQuestionWindow QuestionSelection;
	PatientResponseWindow Response;
	MiniReviewWindow Review; 
	#endregion

	#region Constructor
	/// <summary>
	/// Default constructor
	/// </summary>
	/// <param name='index'>Current interview step index</param>
	/// <param name='name'>Name of the step</param>
	public GUIInterviewStep(int index, string name)
	{
		StepIndex = index;
		Name = name;
		Questions = new Dictionary<long,InterviewQuestion>();
		SelectedAnswers = new List<long>();
		SelectedQuestions = new List<long>();
	}
	
	#region Public Setup Method
	/// <summary>
	/// Initialize the step
	/// </summary>
	public void InitStep()
	{
		// Initialize the questions
		QuestionSelection.initQuestions();
		// QuestionSelection.InitializeGUI (); // Re-initialize the GUI elements
	}
	
	/// <summary>
	/// Adding a comment to current step
	/// </summary>
	/// <param name='comment'>What comment to add</param>
	public void AddComment(string comment)
	{
		Review.AddComment(comment);
	}

	/// <summary>
	/// Adding a question to the step
	/// </summary>
	/// <param name='question'>What question to add</param>
	public void AddQuestion(InterviewQuestion question)
	{
		Questions.Add(question.ID,question);
	}
	
	/// <summary>
	/// Initialize interview step option style
	/// </summary>
	/// <param name='options'>Interview display option</param>
	/// <param name='styles'>List of GUI styles to be used to draw patient response and review</param>
	/// <param name='maxquestioninview'>Maximum display questions</param>
	/// <param name='inventory'>Inventory manager reference</param>
	public void SetUpStepOptions(StepOptions options, InterviewStyleCollection styles, int maxquestioninview, InventoryManager inventory)
	{
		Options = options;

		// Construct question window based on question type, whether it be single select (immediate return) or multiple select
		switch(Options.QuestionSelectionType)
		{
			case StepQuestionSelectionType.SingleClickSelectionButton:
				QuestionSelection = new SingleClickSelectionWindow(StepIndex,Name,Options,styles,maxquestioninview,Questions, SelectedAnswers,inventory);
			break;
			case StepQuestionSelectionType.MultipleClickSelectionButton:
				QuestionSelection = new MultipleClickSelectionWindow(StepIndex,Name,Options,styles,maxquestioninview,Questions, SelectedAnswers,inventory);
			break;
		}

		// Pass along question window reference to response window
		Response = new PatientResponseWindow(styles, Questions, QuestionSelection); 
		Review = new MiniReviewWindow(Name,styles,Questions,SelectedQuestions);
	}
	#endregion

	/// <summary>
	/// Initializes the GUI objects
	/// </summary>
	public void InitializeGUI ()
	{
		// ^o^
		// Resize components
		QuestionSelection.InitializeGUI ();
		Response.InitializeGUI ();
		Review.InitializeGUI ();		
	}
	#endregion
	
	#region Public Interface Methods
	/// <summary>
	/// Calculates the step score based on number of correct questions out of total
	/// </summary>
	public void CalculateStepScore()
	{
		float count = 0;
		float numberCorrect = 0;
		foreach(InterviewQuestion question in Questions.Values)
		{
			if(AllSelections[question.Response.ID] == question.IsCorrectToAsk)
				numberCorrect++;
			++count;
		}	
		DebugConsole.Log (string.Format ("Step Score: ({0} / {1}) * 100 = {2:f2}", numberCorrect, count, ((numberCorrect/count) * 100)));
		
		StepScore = (numberCorrect/count) * 100;		
	}	

	/// <summary>
	/// Gets the total number of questions
	/// </summary>
	/// <returns>Number of questions</returns>
	public float GetTotalQuectionCount()
	{
		return Questions.Count;
	}

	/// <summary>
	/// This function is a little less descriptive then most of mine.
	/// I ususally like to use very descriptive variable names so my code is quite 
	/// readable without comments. But here I'm storing all the values into one of Unity's 
	/// built in types and I have no control over the names. 
	///
	/// So for the Vector3 the values are as follows:
	///	x = Total Correct (The user made the correct selection or no)
	///	y = Total Missed (the question was correct to ask but the user did not select it)
	///	z = Total Incorrect (the question was not correct but the user selected it)
	/// </summary>
	/// <returns>A compound object representing number of questions that are correct and incorrect</returns>
	public Vector3 GetTotals()
	{
		float Correct = 0;
		float Missed = 0;
		float Incorrect = 0;
		foreach(InterviewQuestion question in Questions.Values)
		{
			if(AllSelections[question.Response.ID] == question.IsCorrectToAsk)
				Correct++;
			else
			{
				if(question.IsCorrectToAsk)
					Missed++;
				else
					Incorrect++;
			}
		}
		return new Vector3(Correct,Missed,Incorrect);
	}
	#endregion
		
	#region Frame Update
	/// <summary>
	/// Draw the receptionist dialog
	/// </summary>
	/// <param name='resize'>Whether the GUI has been resized or not</param>
	public StepReturn Draw (bool resize)
	{	
		// Uniformed resize handler
		if (resize)
			InitializeGUI ();
		
		selection = QuestionSelection.Draw();
		
		if(selection.SelectionType == SelectionReturnType.StepFinished)
		{
			DebugConsole.Log("Calculating step score");
			AllSelections = selection.AllSelections;
			CalculateStepScore();
			DebugConsole.Log("Step [" + Name + "] Finished Score = " + StepScore.ToString ("f2") + "%");
			drawReview = true;
			
			Review.InitializeGUI ();
			
			//Here is where I need to save the completed step to the server.
			return new StepReturn(StepReturnType.Save,AllSelections,StepScore);
		}
		if(drawReview)
		{
			int returnFlag = Review.Draw(StepScore, resize);
			switch(returnFlag)
			{
				case 1:
					return new StepReturn(StepReturnType.Continue,AllSelections,StepScore);
				case 2:
					DebugConsole.Log("Save and Quit was seleced by player");
					return new StepReturn(StepReturnType.SaveAndQuit,AllSelections,StepScore);
			}
		}

		if(selection.SelectionType != SelectionReturnType.StepFinished)
		{
			if( selection.SelectionType == SelectionReturnType.AnswerSelected)
			{	
				SelectedQuestions.Add(selection.SelectedAnswer);
				SelectedAnswers.Add(Questions[selection.SelectedAnswer].Response.ID);	
			}
			
			if( selection.SelectionType == SelectionReturnType.SelectionChanged)
			{	
				if(SelectedQuestions.Contains(selection.SelectedAnswer))
				{
					SelectedQuestions.Remove(selection.SelectedAnswer);
				}
				else
				{
					SelectedQuestions.Add(selection.SelectedAnswer);
				}
				if(SelectedAnswers.Contains(Questions[selection.SelectedAnswer].Response.ID))
				{
					SelectedAnswers.Remove(Questions[selection.SelectedAnswer].Response.ID);
				}
				else
				{
					SelectedAnswers.Add(Questions[selection.SelectedAnswer].Response.ID);	
				}
			}	

			CurrentResponseDisplayType = selection.ResponseDisplayType ?? CurrentResponseDisplayType;

			if(!drawReview)
			{
				switch(CurrentResponseDisplayType)
				{
					case QuestionResponseDisplayType.NoResponse:
					break;
					case QuestionResponseDisplayType.ImmediateResponse:
						string audioURL = Response.Draw(selection.SelectedAnswer, resize);
						if ( audioURL != null)
							return new StepReturn(audioURL);
					break;
				}	
			}		
		}
		return new StepReturn();
	}
	#endregion
}

/*********************************************************
******************** CODE GRAVEYARD **********************
	public GUIInterviewStep(StepNames name, StepNames next, GUIStyle boxstyle, GUIStyle playertextstyle,GUIStyle patienttextstyle,Texture2D nexticon, GUIStyle minireviewwindow,GUIStyle minireviewtext,GUIStyle minireviewcorrect,GUIStyle minireviewmissed,GUIStyle minireviewincorrect,GUIStyle minireviewbutton,Texture2D correcticon,Texture2D missedicon,Texture2D incorrecticon, InventoryManager inventory )
	{
		StepName = name;
		NextStepName = next;
		Questions = new Dictionary<int,InterviewQuestion>();
		SelectedAnswers = new List<long>();
		SelectedQuestions = new List<int>();
		QuestionSelection = new PlayerQuestionWindow(boxstyle,playertextstyle,Questions, SelectedAnswers, nexticon, StepName, inventory,6);
		Response = new PatientResponseWindow(patienttextstyle,Questions ); 
		Review = new MiniReviewWindow(StepName,Questions,SelectedQuestions,minireviewwindow,minireviewtext,minireviewcorrect,minireviewmissed,minireviewincorrect,minireviewbutton,correcticon,missedicon,incorrecticon,nexticon);
	}

	public GUIInterviewStep(StepNames name, GUIStyle boxstyle, GUIStyle playertextstyle,GUIStyle patienttextstyle,Texture2D nexticon, GUIStyle minireviewwindow,GUIStyle minireviewtext,GUIStyle minireviewcorrect,GUIStyle minireviewmissed,GUIStyle minireviewincorrect,GUIStyle minireviewbutton,Texture2D correcticon,Texture2D missedicon,Texture2D incorrecticon, InventoryManager inventory )
	{
		StepName = name;
		NextStepName = 0;
		Questions = new Dictionary<int,InterviewQuestion>();
		SelectedAnswers = new List<long>();
		SelectedQuestions = new List<int>();
		QuestionSelection = new PlayerQuestionWindow(boxstyle,playertextstyle,Questions, SelectedAnswers, nexticon, StepName, inventory,6);
		Response = new PatientResponseWindow(patienttextstyle,Questions ); 
		Review = new MiniReviewWindow(StepName,Questions,SelectedQuestions,minireviewwindow,minireviewtext,minireviewcorrect,minireviewmissed,minireviewincorrect,minireviewbutton,correcticon,missedicon,incorrecticon,nexticon);
	}	


**********************************************************/