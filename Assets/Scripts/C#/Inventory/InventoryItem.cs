using UnityEngine;
using System.Collections;

public class InventoryItem
{
    public bool IsSelected;
    public string ItemName;
    public InventoryItemType Itemtype;
    
    public InventoryItem(string iname, InventoryItemType itype)
    {
        IsSelected = false;
        ItemName = iname;
        Itemtype = itype;
    }

    public virtual void ShowItem()
    {
        if (IsSelected)
        {
            string label = "This a default Inventory Item display. \n The virtual function ShowItem() needs to be overridden for this InventoryItemType";
            if (GUI.Button(new Rect(Screen.width / 2 - 250, Screen.height / 2 - 250, 500, 500), label))
            {
                IsSelected = !IsSelected;
            }
        }
    }

    public virtual void LoadFromWeb()
    {
        Debug.Log("This is the virtual base function for an InventoryItems Load From Web /n if this item needs to load information form web pleasae redine this function");
    }
}