using UnityEngine;
using System.Collections;
using VirtualClinic.WebSite.Serializable;

public class RegistrationForm : InventoryItem
{
    GUIStyle RegistrationFormStyle;
    public Texture2D[] Textures;
    public float targetHeight = Screen.height - 613;
    float currentHeight = Screen.height;
    int CurrentPage = 0;

    wwwread WebLoader;
    //Set this to 0 for the default font otherwise adjust it to move all text up or down for a custom font
    public float fontOffset = -6;

    public float[] Page1LineHeights = { 63,
										138,
										172,
										195,
										218,
										240,
										263,
										285,
										307,
										388,
										418,
										442,
										467,
										486,
										512,
										535,
										559,
										582};

    public float[] Page2LineHeights = { 160,
										183,
										207,
										230,
										252,
										275,
										299,
										376,
										412,
										490};

    //******************************************************************
    //Form Items
    //******************************************************************
    //Patient Information
    public string Date = "08/11/1985";
    public string HomePhone = "564654";
    public string LastName = "Doe";
    public string FirstName = "Jane";
    public string Initial = "";
    public string SSN = "959632156";
    public string PAddress = "5414 Big Truck Road";
    public string PCity = "Greenville";
    public string PState = "NC";
    public string PZip = "27858";
    public string Age = "49";
    public string Birthdate = "08/11/1985";
    public string EmployeedBy = "herp derp inc";
    public string Occupation = "herp derper";
    public string BusinessAdress = "5414 Big Truck Road";
    public string BusinessPhone = "564654";
    public string Referrer = "Uncle Buck";
    public string EmergancyContact = "Uncle Buck";
    public string EmergancyPhone = "564654";
    //Primary Insurance
    public string ILastName = "Houes";
    public string IFirstName = "Blues";
    public string IInitial = "D";
    public string Relation = "father";
    public string IBirthDate = "08/11/1985";
    public string ISSN = "959632156";
    public string IAddress = "5414 Big Truck Road";
    public string IPhone = "564654";
    public string ICity = "Greenville";
    public string IState = "NC";
    public string IZip = "27858";
    public string IEmployedby = "herp derp inc";
    public string IOccupation = "herp derper";
    public string IBusinessAdress = "5414 Big Truck Road";
    public string IBusinessPhone = "564654";
    public string InsuranceCompany = "HerpDerping Insureance";
    public string ContractNo = "9199034561";
    public string GroupNo = "456123";
    public string SubscriberNo = "123456";
    public string otherDependents = "Little Tommy Bradshaw";
    //Additional Insurance
    public string SubscriberName = "Jack Daniels";
    public string ARelation = "Uncle";
    public string ABirthdate = "08/11/1985";
    public string Aaddress = "5414 Big Fish Drive";
    public string APhone = "9199034561";
    public string ACity = "Greenville";
    public string AState = "NC";
    public string AZip = "27858";
    public string AEmployeedBy = "Marks Fish Farm";
    public string ABusinessPhone = "9199034561";
    public string AInsuranceCompany = "Smelly Health Care Protection";
    public string ASSN = "959632156";
    public string AContractNo = "9199034561";
    public string AGroupNo = "5464154";
    public string ASubscriberNo = "87835741";
    public string AotherDependents = "Jilly Jane Martin";
    //Release
    public string RInsuranceCompany = "Smelly Health";
    public string DR = "White";
    public string Responsible = "Jack Daniels";
    public string Relationship = "Uncle";
    public string RDate = "08/11/1985";

    public RegistrationForm(GUIStyle style, Texture2D[] textures, wwwread loader)
        : base("Registration Form", InventoryItemType.RegistrationForm)
    {
        RegistrationFormStyle = style;
        Textures = textures;
        WebLoader = loader;

        for (int i = 0; i < Page1LineHeights.Length; i++)
        {
            Page1LineHeights[i] += fontOffset;
        }
        for (int i = 0; i < Page2LineHeights.Length; i++)
        {
            Page2LineHeights[i] += fontOffset;
        }
    }

    public override void LoadFromWeb()
    {
        Debug.Log("trying to load RegistrationFrom From DataBase");
        Patient patient = WebLoader.Case.Patient;

        //Date = WebLoader.Case.OnsetDate.ToShortDateString();
        HomePhone = patient.HomePhone;
        LastName = patient.LastName;
        FirstName = patient.FirstName;
        Initial = patient.Initial;
        SSN = patient.SSN;
        PAddress = patient.AddressLine1;
        PCity = patient.AddressCity;
        PState = patient.AddressState;
        PZip = patient.AddressZip;
        Age = "";
        Birthdate = patient.DOB.ToShortDateString();
        EmployeedBy = (patient.Employer != null) ? patient.Employer.Name : "";
        Occupation = patient.Occupation;
        BusinessAdress = (patient.Employer != null) ? patient.Employer.AddressLine1 : "";
        BusinessPhone = patient.BusinessPhone;
        Referrer = patient.EmergencyContact.FirstName + " " + patient.EmergencyContact.LastName;
        EmergancyContact = patient.EmergencyContact.FirstName + " " + patient.EmergencyContact.LastName;
        EmergancyPhone = patient.EmergencyContact.Phone;
        //Primary Insurance
        ILastName = LastName;
        IFirstName = FirstName;
        IInitial = Initial;
        Relation = "";
        IBirthDate = Birthdate;
        ISSN = SSN;
        IAddress = PAddress;
        IPhone = HomePhone;
        ICity = PCity;
        IState = PState;
        IZip = PZip;
        IEmployedby = EmployeedBy;
        IOccupation = Occupation;
        IBusinessAdress = BusinessAdress;
        IBusinessPhone = BusinessPhone;
        InsuranceCompany = (patient.InsurancePolicy != null) ? ((patient.InsurancePolicy.Company != null) ? (patient.InsurancePolicy.Company.Name) : "") : "";
        ContractNo = (patient.InsurancePolicy != null) ? ((patient.InsurancePolicy.Company != null) ? (patient.InsurancePolicy.Company.Phone) : "") : "";
        GroupNo = (patient.InsurancePolicy != null) ? ((patient.InsurancePolicy.Company != null) ? (patient.InsurancePolicy.GroupNo) : "") : "";
        SubscriberNo = (patient.InsurancePolicy != null) ? ((patient.InsurancePolicy.Company != null) ? (patient.InsurancePolicy.ContractNo) : "") : "";
        otherDependents = "";
        //Additional Insurance
        SubscriberName = "";
        ARelation = "";
        ABirthdate = "";
        Aaddress = "";
        APhone = "";
        ACity = "";
        AState = "";
        AZip = "";
        AEmployeedBy = "";
        ABusinessPhone = "";
        AInsuranceCompany = "";
        ASSN = "";
        AContractNo = "";
        AGroupNo = "";
        ASubscriberNo = "";
        AotherDependents = "";
        //Release
        RInsuranceCompany = InsuranceCompany;
        DR = WebLoader.Case.Clinic.Preceptor.FirstName + " " + WebLoader.Case.Clinic.Preceptor.LastName;
        Responsible = FirstName + " " + LastName;
        Relationship = "";
        RDate = Date;
    }

    public void DrawNextPageButton()
    {
        if (GUI.Button(new Rect(855, 0, 60, 50), " ", RegistrationFormStyle))
        {
            if (CurrentPage == 0)
                CurrentPage++;
            else
            {
                CurrentPage = 0;
                targetHeight = Screen.height + 1;
            }
        }
        if (currentHeight > Screen.height)
        {
            currentHeight = Screen.height;
            targetHeight = Screen.height - 613;
            IsSelected = !IsSelected;
        }
    }

    public void DrawFormEntries(int page)
    {
        if (page == 0) //First page
        {
            //Patient Information
            DrawFormText(Date, 75, Page1LineHeights[0]);
            DrawFormText(HomePhone, 665, Page1LineHeights[0]);
            DrawFormText(LastName, 83, Page1LineHeights[1]);
            DrawFormText(FirstName, 340, Page1LineHeights[1]);
            DrawFormText(Initial, 540, Page1LineHeights[1]);
            DrawFormText(SSN, 690, Page1LineHeights[1]);
            DrawFormText(PAddress, 100, Page1LineHeights[2]);
            DrawFormText(PCity, 75, Page1LineHeights[3]);
            DrawFormText(PState, 520, Page1LineHeights[3]);
            DrawFormText(PZip, 695, Page1LineHeights[3]);
            DrawFormText(Age, 184, Page1LineHeights[4]);
            DrawFormText(Birthdate, 310, Page1LineHeights[4]);
            DrawFormText(EmployeedBy, 190, Page1LineHeights[5]);
            DrawFormText(Occupation, 660, Page1LineHeights[5]);
            DrawFormText(BusinessAdress, 160, Page1LineHeights[6]);
            DrawFormText(BusinessPhone, 665, Page1LineHeights[6]);
            DrawFormText(Referrer, 285, Page1LineHeights[7]);
            DrawFormText(EmergancyContact, 324, Page1LineHeights[8]);
            DrawFormText(EmergancyPhone, 675, Page1LineHeights[8]);
            //Primary Insurance
            DrawFormText(ILastName, 245, Page1LineHeights[9]);
            DrawFormText(IFirstName, 588, Page1LineHeights[9]);
            DrawFormText(IInitial, 790, Page1LineHeights[9]);
            DrawFormText(Relation, 160, Page1LineHeights[10]);
            DrawFormText(IBirthDate, 490, Page1LineHeights[10]);
            DrawFormText(ISSN, 690, Page1LineHeights[10]);
            DrawFormText(IAddress, 250, Page1LineHeights[11]);
            DrawFormText(IPhone, 670, Page1LineHeights[11]);
            DrawFormText(ICity, 88, Page1LineHeights[12]);
            DrawFormText(IState, 520, Page1LineHeights[12]);
            DrawFormText(IZip, 700, Page1LineHeights[12]);
            DrawFormText(IEmployedby, 260, Page1LineHeights[13]);
            DrawFormText(IOccupation, 645, Page1LineHeights[13]);
            DrawFormText(IBusinessAdress, 155, Page1LineHeights[14]);
            DrawFormText(IBusinessPhone, 665, Page1LineHeights[14]);
            DrawFormText(InsuranceCompany, 170, Page1LineHeights[15]);
            DrawFormText(ContractNo, 110, Page1LineHeights[16]);
            DrawFormText(GroupNo, 380, Page1LineHeights[16]);
            DrawFormText(SubscriberNo, 635, Page1LineHeights[16]);
            DrawFormText(otherDependents, 375, Page1LineHeights[17]);
        }
        else //Second page
        {
            //Additional Insurance
            DrawFormText(SubscriberName, 155, Page2LineHeights[0]);
            DrawFormText(ARelation, 570, Page2LineHeights[0]);
            DrawFormText(ABirthdate, 755, Page2LineHeights[0]);
            DrawFormText(Aaddress, 245, Page2LineHeights[1]);
            DrawFormText(APhone, 665, Page2LineHeights[1]);
            DrawFormText(ACity, 75, Page2LineHeights[2]);
            DrawFormText(AState, 520, Page2LineHeights[2]);
            DrawFormText(AZip, 700, Page2LineHeights[2]);
            DrawFormText(AEmployeedBy, 255, Page2LineHeights[3]);
            DrawFormText(ABusinessPhone, 660, Page2LineHeights[3]);
            DrawFormText(AInsuranceCompany, 170, Page2LineHeights[4]);
            DrawFormText(ASSN, 685, Page2LineHeights[4]);
            DrawFormText(AContractNo, 110, Page2LineHeights[5]);
            DrawFormText(AGroupNo, 380, Page2LineHeights[5]);
            DrawFormText(ASubscriberNo, 635, Page2LineHeights[5]);
            DrawFormText(AotherDependents, 370, Page2LineHeights[6]);
            //Release
            DrawFormText(RInsuranceCompany, 550, Page2LineHeights[7]);
            DrawFormText(DR, 210, Page2LineHeights[8]);
            DrawFormText(Responsible, 140, Page2LineHeights[9]);
            DrawFormText(Relationship, 450, Page2LineHeights[9]);
            DrawFormText(RDate, 710, Page2LineHeights[9]);
        }
    }

    void DrawFormText(string text, float x, float y)
    {
        GUI.Label(new Rect(x, y, Screen.width, Screen.height), text, RegistrationFormStyle);
    }

    public override void ShowItem()
    {
        if (IsSelected)
        {
            currentHeight = iTween.FloatUpdate(currentHeight, targetHeight, 5.0f);

            GUI.BeginGroup(new Rect(50, currentHeight, Screen.width, Screen.height), Textures[CurrentPage], RegistrationFormStyle);

            DrawNextPageButton();
            DrawFormEntries(CurrentPage);

            GUI.EndGroup();
        }
    }
}