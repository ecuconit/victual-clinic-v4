using UnityEngine;
using System;
using System.Collections;
using System.IO;
using System.Collections.Generic;

using VirtualClinic.WebSite.Serializable;

public enum PatientSex {Male, Female};

///This loads and controls all the gui elements for the interview section. It is 
///also the section that records the player's selections ingame.
public class InterviewController : MonoBehaviour 
{
	#region Public Interface Variables
	// public GameObject PatientModel;
	// AnimationManager PatientAnimation;

	public AnimationController femalePreceptor;
	public AnimationController malePreceptor;
	
	// Static prefab refrence removed
	// public GameObject[] MalePrefabs;
	// public GameObject[] FemalePrefabs;
	// public string StartStepName = "ChiefComplaint";
	// public string CurrentStepName;
	public InventoryManager Inventory;
	
	public int MaxQuestionsInView;
	public InterviewStyleCollection GUIComponents;
	
	public int CurrentStepIndex;
	public List<GUIInterviewStep> Steps;
	public Dictionary<int, SerializableDictionary<long,bool> > AllSelections = new Dictionary<int, SerializableDictionary<long,bool> > ();
	
	public GUIStyle VirticalScrollBarStyle = new GUIStyle();
	#endregion
	
	#region Private Dataholder Variables
	wwwread SeverConnection;
	GameObject ResponseAudioPlayer;

	/*
	GUIInterviewStep ChiefComplaint; 	
	GUIInterviewStep HPI;            		
	GUIInterviewStep ReivewOfSystem; 	
	GUIInterviewStep PhysicalExam;   		
	GUIInterviewStep LabTest;               
	GUIInterviewStep DifferentialDiagnosis; 
	GUIInterviewStep Impression;		 
	GUIInterviewStep Plan;       		   
	*/
	
	float TotalQuestions;
	float TotalCorrect;
	float TotalMissed;
	float TotalIncorrect;

	StepReturn stepResults;
	PreceptorDialog preceptor;

	bool FinalGradeTransmited;
	#endregion
	
	#region Object Constructor
	/// <summary>
	/// Start this instance
	/// </summary>	
	void Start() 
	{
		
		CurrentStepIndex = 0;
		Inventory = GetComponent<InventoryManager>();
		SeverConnection = GetComponent<wwwread>();
		preceptor = GetComponent<PreceptorDialog>(); 
		
		Reset();
		
		ResponseAudioPlayer = new GameObject();
		ResponseAudioPlayer.AddComponent<AudioSource>();
		ResponseAudioPlayer.GetComponent<AudioSource>().volume = 10.0f;
		ResponseAudioPlayer.GetComponent<AudioSource>().spatialBlend = 0;	
	}
	
	#region Setup Methods
	/// <summary>
	/// Reset the form
	/// </summary>
	public void Reset()
	{
		CurrentStepIndex = 0;
		Steps.Clear();
		AllSelections.Clear();

		TotalQuestions = 0;
		TotalCorrect = 0;
		TotalMissed = 0;
		TotalIncorrect = 0;

		FinalGradeTransmited = false;
	}

	/// <summary>
	/// Loads the saved responses from previous session
	/// </summary>
	public void LoadSavedResponses()
	{
		DebugConsole.Log("Loading Saved Selections From Server:");
		SerializableDictionary<long,bool> TempSavedSelections;
		foreach(System.Collections.Generic.KeyValuePair<int,VirtualClinic.WebSite.Serializable.SerializableDictionary<long,bool>> savedSelections in SeverConnection.PlayerResponse.InterviewQuestionResponse)
		{
			TempSavedSelections = savedSelections.Value;
			if(TempSavedSelections.Count == 0)
			{
				DebugConsole.LogWarning("     No saved selection found for step " + savedSelections.Key);
				CurrentStepIndex = savedSelections.Key;
				break;
			}
			else
			{
				DebugConsole.Log("     Saved selections found for step " + savedSelections.Key);
				Steps[savedSelections.Key].AllSelections = TempSavedSelections;
				AllSelections.Add(savedSelections.Key,TempSavedSelections);
			}
		}
		//CurrentStepName = StepNames.Plan;
		//GUIInterviewSteps[CurrentStepName].drawReview = true;
	}
	
	/// <summary>
	/// Initializes an interview step
	/// </summary>
	/// <param name='stepindex'>Which step to initialize</param>
	public void InitStep(int stepindex)
	{
		Steps[stepindex].InitStep();
	}	
	
	/// <summary>
	/// Add a step to interview
	/// </summary>
	/// <param name='stepindex'>Index number for the step</param>
	/// <param name='name'>Name of the step</param>
	public void AddStep(int stepindex, string name)
	{
		if (stepindex == Steps.Count)
			Steps.Add(new GUIInterviewStep(stepindex, name));
		else
			DebugConsole.LogWarning("You are not allowed to add steps out of order! No step " + name + " was not added!");
	}
	
	/// <summary>
	/// Sets up step.
	/// </summary>
	/// <param name='stepindex'>
	/// Stepindex.
	/// </param>
	/// <param name='options'>
	/// Options.
	/// </param>
	public void SetUpStep(int stepindex, StepOptions options)
	{
		Steps[stepindex].SetUpStepOptions(options, GUIComponents, MaxQuestionsInView, Inventory);
	}
	
	/// <summary>
	/// Adds a question to the step
	/// </summary>
	/// <param name='stepindex'>Which step to add to</param>
	/// <param name='question'>What question to add</param>
	public void AddQuestion(int stepindex, InterviewQuestion question)
	{
		Steps[stepindex].AddQuestion(question);
	}
	
	/// <summary>
	/// Adds a comment to the step
	/// </summary>
	/// <param name='stepindex'>Which step to add to</param>
	/// <param name='comment'>What comment to add</param>
	public void AddComment(int stepindex, string comment)
	{
		Steps[stepindex].AddComment(comment);
	}	
	#endregion
	
	/// <summary>
	/// Initializes the GUI objects
	/// </summary>
	private void InitializeGUI ()
	{
		// ^o^
		foreach (GUIInterviewStep step in Steps)
		{
			// Initialize call to all objects
			step.InitializeGUI ();
		}
	}
	#endregion

	#region Helper Methods
	/// <summary>
	/// Calculates the final grade based on number of items correct and incorrect
	/// </summary>
	public void CalculateFinalGrade()
	{
		DebugConsole.Log ("Calculating final grade");
		
		FinalGradeTransmited = true;
		foreach(GUIInterviewStep Step in Steps)
		{
			TotalQuestions += Step.GetTotalQuectionCount();
			Vector3 Totals = Step.GetTotals();
			TotalCorrect += Totals.x;
			TotalMissed += Totals.y;
			TotalIncorrect += Totals.z;
		}
		DebugConsole.Log("Total questions = " + TotalQuestions);
		DebugConsole.Log("Total correct = " + TotalCorrect);
		DebugConsole.Log("Total missed = " + TotalMissed);
		DebugConsole.Log("Total incorrect = " + TotalIncorrect);
		DebugConsole.Log("Final Score = " + (TotalCorrect/TotalQuestions)*100);

		preceptor.SetTotals(TotalQuestions, new Vector3((float)TotalCorrect,(float)TotalMissed,(float)TotalIncorrect));
		
		try
		{
			DebugConsole.Log ("Final Score Saving completed");
			SeverConnection.PlayerResponse.FinalScore = (decimal)((TotalCorrect/TotalQuestions)*100.0f);
			StartCoroutine(SeverConnection.SaveFinalScore((decimal)((TotalCorrect/TotalQuestions)*100.0f)));			
		}
		catch (Exception error)
		{
			DebugConsole.Log ("Final Score Saving Error: [" + error.Message + "]");
		}
	}
	
	/// <summary>
	/// Loads a random patient model based on gender, no longer used 
	/// </summary>
	/// <param name='Sex'>Patient gender</param>
	public void LoadPatientModel(PatientSex Sex)
	{		
		// Load the patient's model ^o^ ^o^
		/*
		Destroy(PatientModel);
		if(Sex == PatientSex.Male)
		{
			PatientModel = (GameObject)Instantiate(MalePrefabs[Random.Range(0, MalePrefabs.Length)]);
		}
		else
		{   
			PatientModel = (GameObject)Instantiate(FemalePrefabs[Random.Range(0, FemalePrefabs.Length-1)]);
		}
		PatientAnimation = PatientModel.GetComponent<AnimationManager>();
		*/
	}
	
	/// <summary>
	/// Initializes the preceptor
	/// </summary>
	/// <param name='gender'>Gender of the preceptor in format of string</param>
	public void InitializePreceptor (bool isMale)
	{
		if (isMale)
		{
			DebugConsole.Log ("Preceptor is male.");
			Global.AnimationManagerRef.AddCharacter (malePreceptor);
			GameObject.Destroy (femalePreceptor.gameObject);
		}
		else
		{
			DebugConsole.Log ("Preceptor is female.");
			Global.AnimationManagerRef.AddCharacter (femalePreceptor);
			GameObject.Destroy (malePreceptor.gameObject);
		}
	}
	
	/// <summary>
	/// Plays the audio response.
	/// </summary>
	/// <returns>Coroutine return code</returns>
	/// <param name='sourceURL'>Audio URL</param>
	private IEnumerator PlayAudioResponse(string sourceURL)
	{
		//DebugConsole.Log("Trying to Play audio " + "https://zhuoffice2.dyndns.org" + sourceURL);
		WWW stream;

		//DebugConsole.Log("Connecting to stream...");

		if (sourceURL != null && sourceURL.Length > 10)
		{
			//"https://zhuoffice2.dyndns.org" 
			stream = new WWW(sourceURL);
			Global.ToDebug("Playing: " + sourceURL); // ^o^
			yield return stream;
		}
		else
		{
			return false;
		}

		ResponseAudioPlayer.GetComponent<AudioSource>().clip = stream.audioClip;

		ResponseAudioPlayer.GetComponent<AudioSource>().Play();

		// Update animation to be timed 
		Global.AnimationManagerRef.SetAction(ActionType.Talk, stream.audioClip.length);
	}
	#endregion
	
	#region Frame Update
	/// <summary>
	/// Draw the receptionist dialog
	/// </summary>
	/// <param name='resize'>Whether the GUI has been resized or not</param>
	public int Draw(bool resize)
	{
		// Uniformal GUI re-initialization
		if (resize)
			InitializeGUI ();
		
		GUI.skin.verticalScrollbarThumb = VirticalScrollBarStyle;
		stepResults = Steps[CurrentStepIndex].Draw(false); // No longer need to be forwarded, generic object took care of this
		if(stepResults.Type == StepReturnType.Continue)
		{
			if(!FinalGradeTransmited)
			{
				CurrentStepIndex++;
				if(CurrentStepIndex >= Steps.Count)
				{
					/*here is where I need to calculate the final score*/
					CalculateFinalGrade();

					return 1;
				}					
			}
			else
				return 1;
		}

		if(stepResults.Type == StepReturnType.Save)
		{
			if(!FinalGradeTransmited)
			{
				AllSelections.Add(CurrentStepIndex,stepResults.Selections);

				//Adding my collect data into a database ready data struct
				SeverConnection.PlayerResponse.InterviewQuestionResponse[CurrentStepIndex] = stepResults.Selections;
				//Uplaod selections to server
				
				if(CurrentStepIndex >= Steps.Count)
				{
					/*here is where I need to calculate the final score*/
					CalculateFinalGrade();
				}
				else
				{
					DebugConsole.Log ("Saving step");
					StartCoroutine(SeverConnection.UploadPlayerResponse());
				}
			}

		}

		if(stepResults.Type == StepReturnType.SaveAndQuit)
		{
			return 2;
		}
		
		if(stepResults.Type == StepReturnType.PlayAudio)
		{
			// PatientAnimation.Play(AnimationType.Talk_Sit);
			// Play the animation
			Global.AnimationManagerRef.SetAction (ActionType.Talk);
			
			StartCoroutine (PlayAudioResponse(stepResults.AudioURL));
		}		

		//foreach(InventoryItem Item in Inventory.Items)
		//{
		//	Item.ShowItem();
		//}		
			
		
		return 0;	
	}
	#endregion	
}

/*********************************************************
******************** CODE GRAVEYARD **********************

	public Texture2D NextIcon;
	public Texture2D CorrectIcon;
	public Texture2D MissedIcon;
	public Texture2D IncorrectIcon;
	
	public GUIStyle DialogBoxStyle = new GUIStyle();
	public GUIStyle PlayerDialogTextStyle = new GUIStyle();
	public GUIStyle PatientResponseTextStyle = new GUIStyle();
	public GUIStyle MiniReviewWindowStyle = new GUIStyle();
	public GUIStyle MiniReviewTextStyle = new GUIStyle();
	public GUIStyle MiniReviewCorrectStyle = new GUIStyle();
	public GUIStyle MiniReviewMissedStyle = new GUIStyle();
	public GUIStyle MiniReviewIncorrectStyle = new GUIStyle();
	public GUIStyle MiniReviewButtonStyle = new GUIStyle();

	public void Reset()
	{
		CurrentStepIndex = 0;
		CurrentStepName = StartStepName;
		GUIInterviewSteps.Clear();
		AllSelections.Clear();
		
		ChiefComplaint 		  = new GUIInterviewStep(StepNames.ChiefComplaint,StepNames.HPI,DialogBoxStyle,PlayerDialogTextStyle,PatientResponseTextStyle,NextIcon,MiniReviewWindowStyle,MiniReviewTextStyle,MiniReviewCorrectStyle,MiniReviewMissedStyle,MiniReviewIncorrectStyle,MiniReviewButtonStyle,CorrectIcon,MissedIcon,IncorrectIcon, Inventory);
		HPI 				  = new GUIInterviewStep(StepNames.HPI,StepNames.ReviewOfSystem,DialogBoxStyle,PlayerDialogTextStyle,PatientResponseTextStyle,NextIcon,MiniReviewWindowStyle,MiniReviewTextStyle,MiniReviewCorrectStyle,MiniReviewMissedStyle,MiniReviewIncorrectStyle,MiniReviewButtonStyle,CorrectIcon,MissedIcon,IncorrectIcon, Inventory);
		ReivewOfSystem		  = new GUIInterviewStep(StepNames.ReviewOfSystem,StepNames.PhysicalExam,DialogBoxStyle,PlayerDialogTextStyle,PatientResponseTextStyle,NextIcon,MiniReviewWindowStyle,MiniReviewTextStyle,MiniReviewCorrectStyle,MiniReviewMissedStyle,MiniReviewIncorrectStyle,MiniReviewButtonStyle,CorrectIcon,MissedIcon,IncorrectIcon, Inventory);
		PhysicalExam		  = new GUIInterviewStep(StepNames.PhysicalExam,StepNames.LabTest,DialogBoxStyle,PlayerDialogTextStyle,PatientResponseTextStyle,NextIcon,MiniReviewWindowStyle,MiniReviewTextStyle,MiniReviewCorrectStyle,MiniReviewMissedStyle,MiniReviewIncorrectStyle,MiniReviewButtonStyle,CorrectIcon,MissedIcon,IncorrectIcon, Inventory);
		LabTest				  = new GUIInterviewStep(StepNames.LabTest,StepNames.DifferentialDiagnosis,DialogBoxStyle,PlayerDialogTextStyle,PatientResponseTextStyle,NextIcon,MiniReviewWindowStyle,MiniReviewTextStyle,MiniReviewCorrectStyle,MiniReviewMissedStyle,MiniReviewIncorrectStyle,MiniReviewButtonStyle,CorrectIcon,MissedIcon,IncorrectIcon, Inventory);
		DifferentialDiagnosis = new GUIInterviewStep(StepNames.DifferentialDiagnosis,StepNames.Impression,DialogBoxStyle,PlayerDialogTextStyle,PatientResponseTextStyle,NextIcon,MiniReviewWindowStyle,MiniReviewTextStyle,MiniReviewCorrectStyle,MiniReviewMissedStyle,MiniReviewIncorrectStyle,MiniReviewButtonStyle,CorrectIcon,MissedIcon,IncorrectIcon, Inventory);
		Impression			  = new GUIInterviewStep(StepNames.Impression,StepNames.Plan,DialogBoxStyle,PlayerDialogTextStyle,PatientResponseTextStyle,NextIcon,MiniReviewWindowStyle,MiniReviewTextStyle,MiniReviewCorrectStyle,MiniReviewMissedStyle,MiniReviewIncorrectStyle,MiniReviewButtonStyle,CorrectIcon,MissedIcon,IncorrectIcon, Inventory);
		Plan				  = new GUIInterviewStep(StepNames.Plan,DialogBoxStyle,PlayerDialogTextStyle,PatientResponseTextStyle,NextIcon,MiniReviewWindowStyle,MiniReviewTextStyle,MiniReviewCorrectStyle,MiniReviewMissedStyle,MiniReviewIncorrectStyle,MiniReviewButtonStyle,CorrectIcon,MissedIcon,IncorrectIcon, Inventory);
		
		GUIInterviewSteps.Add(ChiefComplaint.StepName, ChiefComplaint);
		GUIInterviewSteps.Add(HPI.StepName, HPI);
		GUIInterviewSteps.Add(ReivewOfSystem.StepName, ReivewOfSystem);
		GUIInterviewSteps.Add(PhysicalExam.StepName, PhysicalExam);
		GUIInterviewSteps.Add(LabTest.StepName, LabTest);
		GUIInterviewSteps.Add(DifferentialDiagnosis.StepName, DifferentialDiagnosis);
		GUIInterviewSteps.Add(Impression.StepName, Impression);
		GUIInterviewSteps.Add(Plan.StepName, Plan);	

		TotalQuestions = 0;
		TotalCorrect = 0;
		TotalMissed = 0;
		TotalIncorrect = 0;

		FinalGradeTransmited = false;
	}

	public int Draw()
	{
		GUI.skin.verticalScrollbarThumb = VirticalScrollBarStyle;
		stepResults = GUIInterviewSteps[CurrentStepName].Draw();
		if(stepResults.Type == StepReturnType.Continue)
		{
			if(!FinalGradeTransmited)
			{
				if(GUIInterviewSteps[CurrentStepName].NextStepName == 0)
				{
					//here is where I need to calculate the final score
					CalculateFinalGrade();

					return 1;
				}
				else
					CurrentStepName = GUIInterviewSteps[CurrentStepName].NextStepName;
			}
			else
				return 1;
		}

		if(stepResults.Type == StepReturnType.Save)
		{
			if(!FinalGradeTransmited)
			{
				AllSelections.Add(CurrentStepName,stepResults.Selections);

				//Adding my collect data into a database ready data struct
				SeverConnection.PlayerResponse.InterviewQuestionResponse[(int)CurrentStepName] = stepResults.Selections;
				//Uplaod selections to server
				
				if(GUIInterviewSteps[CurrentStepName].NextStepName == 0)
				{
					//here is where I need to calculate the final score
					CalculateFinalGrade();
				}
				else
				{
					StartCoroutine(SeverConnection.UploadPlayerResponse());
				}
			}

		}

		if(stepResults.Type == StepReturnType.SaveAndQuit)
		{
			return 2;
		}
		
		if(stepResults.Type == StepReturnType.PlayAudio)
		{
			PatientAnimation.Play(AnimationType.Talk_Sit);
			StartCoroutine (PlayAudioResponse(stepResults.AudioURL));
		}		

		//foreach(InventoryItem Item in Inventory.Items)
		//{
		//	Item.ShowItem();
		//}		
			
		
		return 0;
	}

**********************************************************/


