using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class PreceptorDialog : MonoBehaviour
{
	#region Public Config Variable
	// public AnimationManager animation;
	public float waveDuration = 2.0f;
	float TotalQuestions = 100;
	float TotalCorrect = 93.7659254f;
	// float TotalMissed;
	// float TotalIncorrect;
	float FinalGrade;
	int MaxNumOfChars = 110;
	public Dictionary<long, InterviewQuestion> Questions;
	public List<long> SelectedQuestions;
	public List<long> SelectedAnswers;
	// PlayerQuestionWindow QuestionSelection;
	PatientResponseWindow Response;
	// bool drawResponse = true;
	SelectionReturn selection;
	InterviewController interview;
	// Texture2D nexticon;
	// GUIStyle DialogBoxStyle;
	GUIStyle PlayerDialogTextStyle;
	GUIStyle PreceptorResponseTextStyle;
	float startResponseHeight;
	float currentResponseHeight;
	float targetResponseHeight = 0;
	float lineoffset;
	// string ResponseText;
	public string IntroductionVoiceText = "Response 1 this should be loaded from web";
	public string IntroductionText = "Response 1 this should be loaded from web";
	public string WrapUpVoiceText = "Response 2 this should be loaded from web";
	public string WrapUpText = "Response 2 this should be loaded from web";
	public string IntroductionVoiceTextMP3Url;
	public string WrapUpVoiceTextMP3Url;
	GameObject ResponseAudioPlayer;
	// bool AudioStarted = false;
	// bool BothAudioPlayed = false;
	// bool Finished;
	wwwread SeverConnection;
	public Vector2 PositionTester = new Vector2 ((Screen.width / 2), (Screen.height / 2));
	public Vector2 SizeTester = new Vector2 (100, 100);
	public GUIStyle BackGroundStyle;
	public GUIStyle Heading;
	public GUIStyle BasicText;
	public GUIStyle OkButton;
	public GUIStyle Grade;
	public GUIStyle VirticalScrollBarStyle;
	public GUIStyle CheckBox;
	public Texture2D BackGround;
	#endregion
	
	#region Private Data Holder
	private Rect introContainer;
	private Rect wrapupContainer;
	private Rect finalGradeContainer;
	Vector2 scrollPosition = Vector2.zero;
	private Rect introBar;
	private Rect wrapupBar;
	private string parsedIntroText;
	private string parsedWrapupText;	
	#endregion
	
	//InterviewQuestion testquestion = new InterviewQuestion(1,1,"Hello",true,"Hi! How are you today?","",0,false,"");
	//InterviewQuestion testquestion2 = new InterviewQuestion(2,2,"I'm fine thanks, and you?",true,"Oh you know, busy busy here in the virtual clinic","",0,false,"");
	
	#region Object Initialization
	/// <summary>
	/// Start this instance
	/// </summary>
	void Start ()
	{
		// Finished = false;
		interview = GetComponent<InterviewController> ();
		// animation.Play(AnimationType.Wave_Stand);
		// Play the animation
		// Global.AnimationManagerRef.SetAction (ActionType.Wave, waveDuration);

		// DialogBoxStyle = interview.GUIComponents.GetStyle (InterviewStyle.DialogBox);
		PlayerDialogTextStyle = interview.GUIComponents.GetStyle (InterviewStyle.PlayerText);
		PreceptorResponseTextStyle = interview.GUIComponents.GetStyle (InterviewStyle.ResponseText);
		// nexticon = interview.GUIComponents.GetIcon (InterviewIcon.Next);

		Questions = new Dictionary<long, InterviewQuestion> ();
		SelectedAnswers = new List<long> ();
		SelectedQuestions = new List<long> ();
		//QuestionSelection = new PlayerQuestionWindow(" ",interview.GUIComponents,interview.GUIComponents,interview.MaxQuestionsInView,Questions, SelectedAnswers,interview.Inventory);

		//Response = new PatientResponseWindow(PreceptorResponseTextStyle,Questions ); 
		//AddQuestion(testquestion);

		//Fake response
		lineoffset = PlayerDialogTextStyle.fontSize + 10;
		startResponseHeight = 0 - lineoffset * 2;
		currentResponseHeight = startResponseHeight;
        
		// ResponseText = IntroductionVoiceText;

		ResponseAudioPlayer = new GameObject ();
		ResponseAudioPlayer.AddComponent <AudioSource>();
		ResponseAudioPlayer.GetComponent<AudioSource>().volume = 10.0f;
		ResponseAudioPlayer.GetComponent<AudioSource>().spatialBlend = 0;
	}
	
	/// <summary>
	/// Sets the text and audio
	/// </summary>
	/// <param name='T1'>Introduction text for audio</param>
	/// <param name='A1'>Introduction audio URL</param>
	/// <param name='T2'>Wrapup text for audio</param>
	/// <param name='A2'>Wrapus audio URL</param>
	/// <param name='FT1'>Introduction text for display</param>
	/// <param name='FT2'>Introduction audio for display</param>
	public void SetTextAndAudio (string T1, string A1, string T2, string A2, string FT1, string FT2)
	{
		IntroductionVoiceText = T1;
		WrapUpVoiceText = T2;
		IntroductionVoiceTextMP3Url = A1;
		WrapUpVoiceTextMP3Url = A2;

		IntroductionText = FT1;
		WrapUpText = FT2;
		
		// Initialize GUI layouts
		InitializeGUI ();
		
		// ResponseText = IntroductionVoiceText;
	}
	
	/*
	/// <summary>
	/// Initialize current step
	/// </summary>
	public void InitStep ()
	{
		QuestionSelection.initQuestions ();
	}
	*/
	
	#region Initialize GUI Positions
	/// <summary>
	/// Initializes the GUI element positions
	/// </summary>
	private void InitializeGUI ()
	{
		// Calculate the necessity response header, fit to font width, assume font width = fontSize * 2
		int characterCount = (Screen.width - PreceptorResponseTextStyle.margin.left - PreceptorResponseTextStyle.margin.right - 
			PreceptorResponseTextStyle.padding.left - PreceptorResponseTextStyle.padding.right) / PreceptorResponseTextStyle.fontSize * 2;
		lineoffset = PreceptorResponseTextStyle.fontSize + 10;	
		
		// Calculate the response header
		// IntroductionText // WrapUpText // IntroductionVoiceText // WrapUpVoiceText
		parsedIntroText = TextParser.CutStringToSize (IntroductionText, characterCount);
		parsedWrapupText = TextParser.CutStringToSize (WrapUpText, characterCount);
		introBar = new Rect (0, startResponseHeight, Screen.width, lineoffset * TextParser.CountLines (parsedIntroText) + 10);
		wrapupBar = new Rect (introBar.x, introBar.y, introBar.width, lineoffset * TextParser.CountLines (parsedWrapupText) + 10);
		
		// Calculate the GUI rectangles
		Vector2 temp = new Vector2 (350, 360);
		finalGradeContainer = new Rect (BackGroundStyle.margin.left, (Screen.height - wrapupBar.height - temp.x) / 2 + wrapupBar.height, temp.x, temp.y); // Dock on left
		temp = new Vector2 (Mathf.Max (finalGradeContainer.xMax + BackGroundStyle.margin.left + BackGroundStyle.margin.right, Screen.width / 2), introBar.height);
		introContainer = new Rect (temp.x, temp.y, Screen.width - temp.x - BackGroundStyle.margin.right, Screen.height - temp.y - BackGroundStyle.margin.bottom);
		temp = new Vector2 (Mathf.Max (finalGradeContainer.xMax + BackGroundStyle.margin.left + BackGroundStyle.margin.right, Screen.width / 2), wrapupBar.height);
		wrapupContainer = new Rect (temp.x, temp.y, Screen.width - temp.x - BackGroundStyle.margin.right, Screen.height - temp.y - BackGroundStyle.margin.bottom);
	}
	#endregion
	#endregion
    
	#region Frame Update
	/// <summary>
	/// Draw the preceptor window
	/// </summary>
	/// <param name='Casefinished'>Whether the case is finished or not.  If so, draw Introduction; if not, draw Wrap Up</param>
	/// <param name='resize'>Screen resize signal</param>
	public bool Draw (bool Casefinished, bool resize)
	{
		// If call for GUI resize, use it
		if (resize)
			InitializeGUI ();
		
		bool returnFlag = true;
		GUI.skin.verticalScrollbarThumb = VirticalScrollBarStyle;
		GUI.skin.toggle = CheckBox;
		if (!Casefinished) {
			// Draw the static response with drop down animation
			introBar.y = currentResponseHeight = iTween.FloatUpdate (currentResponseHeight, targetResponseHeight, 3f);
			GUI.Button (introBar, parsedIntroText, PreceptorResponseTextStyle); // Top bar, response
			
			// Draw container
			GUILayout.BeginArea (introContainer);
			GUILayout.BeginVertical (BackGroundStyle);
			
			// Draw title bar
			GUILayout.Label ("Introduction", Heading);
			
			// Draw middle
			scrollPosition = GUILayout.BeginScrollView (scrollPosition);
			// Draw text area
			GUILayout.TextArea (IntroductionText, BasicText);			
			GUILayout.EndScrollView ();
			
			// Draw okay button, center it
			GUILayout.BeginHorizontal ();
			GUILayout.Label ("");
			if (GUILayout.Button ("", OkButton)) {
				//Destroy(ResponseAudioPlayer);
				// AudioStarted = false; ^o^
				returnFlag = false;
				ResponseAudioPlayer.GetComponent<AudioSource>().Stop();
			}
			GUILayout.Label ("");
			GUILayout.EndHorizontal ();
			
			GUILayout.EndVertical ();
			GUILayout.EndArea ();
			/*
            //PositionTester.x,PositionTester.y,SizeTester.x,SizeTester.y
            GUI.Label(new Rect(480, 120, 600, 600), BackGround, BackGroundStyle);
            GUI.Label(new Rect(500, 160, 465, 100), "Introduction", Heading); // Header
            scrollPosition = GUI.BeginScrollView(new Rect(520, 220, 440, 420), scrollPosition, new Rect(0, 0, 420, 600));
            GUI.TextArea(new Rect(0, 0, 420, 600), IntroductionText, BasicText); // Container
            GUI.EndScrollView();

            if (GUI.Button(new Rect(665, 640, 150, 50), "", OkButton))
            {
                //Destroy(ResponseAudioPlayer);
                AudioStarted = false;
                return false;
            }
			*/
		} else {
			// Draw the static response with drop down animation
			wrapupBar.y = currentResponseHeight = iTween.FloatUpdate (currentResponseHeight, targetResponseHeight, 3f);
			GUI.Button (wrapupBar, parsedWrapupText, PreceptorResponseTextStyle); // Top bar, response
			
			// Draw right container
			GUILayout.BeginArea (wrapupContainer);
			GUILayout.BeginVertical (BackGroundStyle);
			
			// Draw title bar
			GUILayout.Label ("Case Wrap Up", Heading);
			
			// Draw middle
			scrollPosition = GUILayout.BeginScrollView (scrollPosition);
			// Draw text area
			GUILayout.TextArea (WrapUpText, BasicText);			
			GUILayout.EndScrollView ();
			
			GUILayout.EndVertical ();
			GUILayout.EndArea ();
			
			// Draw left container
			GUILayout.BeginArea (finalGradeContainer);
			GUILayout.BeginVertical (BackGroundStyle);
			
			// Draw Final grade
			GUILayout.Label ("Final Grade", Heading);
			
			// Draw okay button, center it
			GUILayout.BeginHorizontal ();
			GUILayout.Label ("");
			GUILayout.Label (FinalGrade + "%", Grade);			
			GUILayout.Label ("");
			GUILayout.EndHorizontal ();			
			
			// Draw instruction
			GUILayout.Label ("Please use the link\n on the page above\nto return to the\n case selection.", Heading);
			
			GUILayout.EndVertical ();
			GUILayout.EndArea ();
			
			/*
            ResponseText = WrapUpVoiceText;
            lineoffset = PreceptorResponseTextStyle.fontSize + 10;
            currentResponseHeight = iTween.FloatUpdate(currentResponseHeight, targetResponseHeight, 3f);

            GUI.Button(new Rect(0, currentResponseHeight, Screen.width, lineoffset * 2 + lineoffset * CountLines(ResponseText)), ResponseText, PreceptorResponseTextStyle);
            
            GUI.Label(new Rect(610, 160, 420, 600), BackGround, BackGroundStyle);
            GUI.Label(new Rect(610, 180, 420, 100), "Case WrapUp", Heading);
            
            scrollPosition = GUI.BeginScrollView(new Rect(640, 220, 350, 600), scrollPosition, new Rect(0, 0, 350, 600));
            GUI.TextArea(new Rect(0, 0, 350, 600), WrapUpText, BasicText);
            GUI.EndScrollView();

            GUI.Label(new Rect(20, 200, 420, 420), BackGround, BackGroundStyle);
            GUI.Label(new Rect(20, 220, 350, 100), "Final Grade", Heading);
            GUI.Label(new Rect(80, 300, 500, 200), FinalGrade + "%", Grade);
            GUI.Label(new Rect(0, 460, 400, 300), "Please use the link\n on the page above\nto return to the\n case selection.", Heading);
			*/

		}
		return returnFlag;
	}
	#endregion 
	
	#region Helper Method
	/// <summary>
	/// Cuts the string into multiple smaller strings, each limit by preset size limit
	/// </summary>
	/// <returns>String cut into substring, each delimited by /n</returns>
	/// <param name='stringtoCut'>What string to cut</param>
	string CutStringToSize (string stringtoCut)
	{
		if (stringtoCut.Length > MaxNumOfChars) {
			DebugConsole.Log ("found a long string. Resizing");
			int lineBreak;
			string firstline, rest;

			lineBreak = stringtoCut.LastIndexOf (' ', MaxNumOfChars - MaxNumOfChars / 4, MaxNumOfChars / 4);
			firstline = stringtoCut.Substring (0, lineBreak);
			rest = stringtoCut.Substring (lineBreak);
			return firstline + "\n" + CutStringToSize (rest);
		} else
			return stringtoCut;
	}
	
	/// <summary>
	/// Counts the number of lines
	/// </summary>
	/// <returns>Number of lines in given string - 1</returns>
	/// <param name='stringtoCount'>What string to count</param>
	int CountLines (string stringtoCount)
	{
		int lines = 0;
		for (int i = 0; i < stringtoCount.Length; ++i) {
			if (stringtoCount [i] == '\n')
				lines++;
		}
		return lines;
	}
	
	/// <summary>
	/// Adds the interview question to list
	/// </summary>
	/// <param name='question'>What question to add</param>
	public void AddQuestion (InterviewQuestion question)
	{
		Questions.Add (question.ID, question);
		// QuestionSelection.initQuestions ();
	}

	/// <summary>
	/// Plays an audio
	/// </summary>
	/// <param name='Casefinished'>Whether the case is finished or not.  If yes, play wrapup audio; if not, play introduction</param>
	public void StartAudioPlayback (bool Casefinished)
	{
		if (!Casefinished) {
			// Play the animation
			Global.AnimationManagerRef.SetAction (ActionType.Talk);
			StartCoroutine (PlayAudioResponse (IntroductionVoiceTextMP3Url));
			// animation.Play(AnimationType.Talk_Stand);
		} else {
			// Play the animation
			Global.AnimationManagerRef.SetAction (ActionType.Talk);
			StartCoroutine (PlayAudioResponse (WrapUpVoiceTextMP3Url));
			// animation.Play(AnimationType.Talk_Stand);			
		}
	}
	
	/// <summary>
	/// Plaies the audio response
	/// </summary>
	/// <returns>Coroutine return flag</returns>
	/// <param name='sourceURL'>Audio URL</param>
	private IEnumerator PlayAudioResponse (string sourceURL)
	{
		//DebugConsole.Log("Trying to Play audio " + "https://zhuoffice2.dyndns.org" + sourceURL);
		WWW stream;

		//DebugConsole.Log("Connecting to stream...");
		if (sourceURL != null && sourceURL.Length > 10) {
			//"https://zhuoffice2.dyndns.org" 
			//stream = new WWW("https://zhuoffice2.dyndns.org" + sourceURL);
			stream = new WWW (sourceURL);
			yield return stream;
		} else {
			return false;
		}

		ResponseAudioPlayer.GetComponent<AudioSource>().clip = stream.audioClip;
		ResponseAudioPlayer.GetComponent<AudioSource>().Play ();
		// AudioStarted = true;
		//animation.Play(AnimationType.Talk_Stand);

		// Update animation to be timed 
		Global.AnimationManagerRef.SetAction (ActionType.Talk, stream.audioClip.length);
	}
	
	/// <summary>
	/// Calculate the total score
	/// </summary>
	/// <param name='totalQ'>Total number of questions</param>
	/// <param name='totals'>Total selection in sets, nmber of correct, incorrect, and missed</param>
	public void SetTotals (float totalQ, Vector3 totals)
	{
		// Record how many questions there were, how many are correct, and how many aren't
		TotalQuestions = totalQ;
		TotalCorrect = totals.x;
		// TotalIncorrect = totals.z;
		// TotalMissed = totals.y;

		FinalGrade = Mathf.Round ((TotalCorrect / TotalQuestions) * 10000) / 100;
	}
	#endregion

	//	void OnGUI()
	//	{
	//		GUILayout.Label(IntroductionVoiceText);
	//		GUILayout.Label(WrapUpVoiceText);
	//		GUILayout.Label(IntroductionVoiceTextMP3Url);
	//		GUILayout.Label(WrapUpVoiceTextMP3Url);
	//	}
}